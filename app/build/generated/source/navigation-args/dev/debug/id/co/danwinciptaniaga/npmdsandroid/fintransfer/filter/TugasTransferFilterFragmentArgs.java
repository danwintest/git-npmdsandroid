package id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class TugasTransferFilterFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private TugasTransferFilterFragmentArgs() {
  }

  private TugasTransferFilterFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static TugasTransferFilterFragmentArgs fromBundle(@NonNull Bundle bundle) {
    TugasTransferFilterFragmentArgs __result = new TugasTransferFilterFragmentArgs();
    bundle.setClassLoader(TugasTransferFilterFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("filterField")) {
      TugasTransferBrowseAndroidFilter filterField;
      if (Parcelable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class) || Serializable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class)) {
        filterField = (TugasTransferBrowseAndroidFilter) bundle.get("filterField");
      } else {
        throw new UnsupportedOperationException(TugasTransferBrowseAndroidFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("filterField", filterField);
    } else {
      __result.arguments.put("filterField", null);
    }
    if (bundle.containsKey("isGrouping")) {
      boolean isGrouping;
      isGrouping = bundle.getBoolean("isGrouping");
      __result.arguments.put("isGrouping", isGrouping);
    } else {
      throw new IllegalArgumentException("Required argument \"isGrouping\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public TugasTransferBrowseAndroidFilter getFilterField() {
    return (TugasTransferBrowseAndroidFilter) arguments.get("filterField");
  }

  @SuppressWarnings("unchecked")
  public boolean getIsGrouping() {
    return (boolean) arguments.get("isGrouping");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("filterField")) {
      TugasTransferBrowseAndroidFilter filterField = (TugasTransferBrowseAndroidFilter) arguments.get("filterField");
      if (Parcelable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class) || filterField == null) {
        __result.putParcelable("filterField", Parcelable.class.cast(filterField));
      } else if (Serializable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class)) {
        __result.putSerializable("filterField", Serializable.class.cast(filterField));
      } else {
        throw new UnsupportedOperationException(TugasTransferBrowseAndroidFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("filterField", null);
    }
    if (arguments.containsKey("isGrouping")) {
      boolean isGrouping = (boolean) arguments.get("isGrouping");
      __result.putBoolean("isGrouping", isGrouping);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    TugasTransferFilterFragmentArgs that = (TugasTransferFilterFragmentArgs) object;
    if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
      return false;
    }
    if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
      return false;
    }
    if (arguments.containsKey("isGrouping") != that.arguments.containsKey("isGrouping")) {
      return false;
    }
    if (getIsGrouping() != that.getIsGrouping()) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
    result = 31 * result + (getIsGrouping() ? 1 : 0);
    return result;
  }

  @Override
  public String toString() {
    return "TugasTransferFilterFragmentArgs{"
        + "filterField=" + getFilterField()
        + ", isGrouping=" + getIsGrouping()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(TugasTransferFilterFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(boolean isGrouping) {
      this.arguments.put("isGrouping", isGrouping);
    }

    @NonNull
    public TugasTransferFilterFragmentArgs build() {
      TugasTransferFilterFragmentArgs result = new TugasTransferFilterFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setFilterField(@Nullable TugasTransferBrowseAndroidFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @NonNull
    public Builder setIsGrouping(boolean isGrouping) {
      this.arguments.put("isGrouping", isGrouping);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public TugasTransferBrowseAndroidFilter getFilterField() {
      return (TugasTransferBrowseAndroidFilter) arguments.get("filterField");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsGrouping() {
      return (boolean) arguments.get("isGrouping");
    }
  }
}
