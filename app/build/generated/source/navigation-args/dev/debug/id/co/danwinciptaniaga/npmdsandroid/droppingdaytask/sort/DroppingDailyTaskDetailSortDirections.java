package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.sort;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class DroppingDailyTaskDetailSortDirections {
  private DroppingDailyTaskDetailSortDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
