package id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class BookingViewDetailTransferFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private BookingViewDetailTransferFragmentArgs() {
  }

  private BookingViewDetailTransferFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static BookingViewDetailTransferFragmentArgs fromBundle(@NonNull Bundle bundle) {
    BookingViewDetailTransferFragmentArgs __result = new BookingViewDetailTransferFragmentArgs();
    bundle.setClassLoader(BookingViewDetailTransferFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("bookingId")) {
      UUID bookingId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        bookingId = (UUID) bundle.get("bookingId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("bookingId", bookingId);
    } else {
      __result.arguments.put("bookingId", null);
    }
    if (bundle.containsKey("isHasCreatePermission")) {
      boolean isHasCreatePermission;
      isHasCreatePermission = bundle.getBoolean("isHasCreatePermission");
      __result.arguments.put("isHasCreatePermission", isHasCreatePermission);
    } else {
      throw new IllegalArgumentException("Required argument \"isHasCreatePermission\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public UUID getBookingId() {
    return (UUID) arguments.get("bookingId");
  }

  @SuppressWarnings("unchecked")
  public boolean getIsHasCreatePermission() {
    return (boolean) arguments.get("isHasCreatePermission");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("bookingId")) {
      UUID bookingId = (UUID) arguments.get("bookingId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || bookingId == null) {
        __result.putParcelable("bookingId", Parcelable.class.cast(bookingId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("bookingId", Serializable.class.cast(bookingId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("bookingId", null);
    }
    if (arguments.containsKey("isHasCreatePermission")) {
      boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
      __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    BookingViewDetailTransferFragmentArgs that = (BookingViewDetailTransferFragmentArgs) object;
    if (arguments.containsKey("bookingId") != that.arguments.containsKey("bookingId")) {
      return false;
    }
    if (getBookingId() != null ? !getBookingId().equals(that.getBookingId()) : that.getBookingId() != null) {
      return false;
    }
    if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
      return false;
    }
    if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getBookingId() != null ? getBookingId().hashCode() : 0);
    result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
    return result;
  }

  @Override
  public String toString() {
    return "BookingViewDetailTransferFragmentArgs{"
        + "bookingId=" + getBookingId()
        + ", isHasCreatePermission=" + getIsHasCreatePermission()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(BookingViewDetailTransferFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public BookingViewDetailTransferFragmentArgs build() {
      BookingViewDetailTransferFragmentArgs result = new BookingViewDetailTransferFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setBookingId(@Nullable UUID bookingId) {
      this.arguments.put("bookingId", bookingId);
      return this;
    }

    @NonNull
    public Builder setIsHasCreatePermission(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getBookingId() {
      return (UUID) arguments.get("bookingId");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }
  }
}
