package id.co.danwinciptaniaga.npmdsandroid.ui.bankacct;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class BankAcctFormFragmentDirections {
  private BankAcctFormFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
