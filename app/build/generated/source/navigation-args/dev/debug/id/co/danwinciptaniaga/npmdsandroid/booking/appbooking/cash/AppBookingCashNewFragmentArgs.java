package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class AppBookingCashNewFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AppBookingCashNewFragmentArgs() {
  }

  private AppBookingCashNewFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AppBookingCashNewFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AppBookingCashNewFragmentArgs __result = new AppBookingCashNewFragmentArgs();
    bundle.setClassLoader(AppBookingCashNewFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("appPBookingId")) {
      UUID appPBookingId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        appPBookingId = (UUID) bundle.get("appPBookingId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("appPBookingId", appPBookingId);
    } else {
      __result.arguments.put("appPBookingId", null);
    }
    if (bundle.containsKey("appOperation")) {
      String appOperation;
      appOperation = bundle.getString("appOperation");
      if (appOperation == null) {
        throw new IllegalArgumentException("Argument \"appOperation\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("appOperation", appOperation);
    } else {
      throw new IllegalArgumentException("Required argument \"appOperation\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("bookingType")) {
      String bookingType;
      bookingType = bundle.getString("bookingType");
      if (bookingType == null) {
        throw new IllegalArgumentException("Argument \"bookingType\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("bookingType", bookingType);
    } else {
      throw new IllegalArgumentException("Required argument \"bookingType\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("isHasCreatePermission")) {
      boolean isHasCreatePermission;
      isHasCreatePermission = bundle.getBoolean("isHasCreatePermission");
      __result.arguments.put("isHasCreatePermission", isHasCreatePermission);
    } else {
      throw new IllegalArgumentException("Required argument \"isHasCreatePermission\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public UUID getAppPBookingId() {
    return (UUID) arguments.get("appPBookingId");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getAppOperation() {
    return (String) arguments.get("appOperation");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getBookingType() {
    return (String) arguments.get("bookingType");
  }

  @SuppressWarnings("unchecked")
  public boolean getIsHasCreatePermission() {
    return (boolean) arguments.get("isHasCreatePermission");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("appPBookingId")) {
      UUID appPBookingId = (UUID) arguments.get("appPBookingId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || appPBookingId == null) {
        __result.putParcelable("appPBookingId", Parcelable.class.cast(appPBookingId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("appPBookingId", Serializable.class.cast(appPBookingId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("appPBookingId", null);
    }
    if (arguments.containsKey("appOperation")) {
      String appOperation = (String) arguments.get("appOperation");
      __result.putString("appOperation", appOperation);
    }
    if (arguments.containsKey("bookingType")) {
      String bookingType = (String) arguments.get("bookingType");
      __result.putString("bookingType", bookingType);
    }
    if (arguments.containsKey("isHasCreatePermission")) {
      boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
      __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AppBookingCashNewFragmentArgs that = (AppBookingCashNewFragmentArgs) object;
    if (arguments.containsKey("appPBookingId") != that.arguments.containsKey("appPBookingId")) {
      return false;
    }
    if (getAppPBookingId() != null ? !getAppPBookingId().equals(that.getAppPBookingId()) : that.getAppPBookingId() != null) {
      return false;
    }
    if (arguments.containsKey("appOperation") != that.arguments.containsKey("appOperation")) {
      return false;
    }
    if (getAppOperation() != null ? !getAppOperation().equals(that.getAppOperation()) : that.getAppOperation() != null) {
      return false;
    }
    if (arguments.containsKey("bookingType") != that.arguments.containsKey("bookingType")) {
      return false;
    }
    if (getBookingType() != null ? !getBookingType().equals(that.getBookingType()) : that.getBookingType() != null) {
      return false;
    }
    if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
      return false;
    }
    if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getAppPBookingId() != null ? getAppPBookingId().hashCode() : 0);
    result = 31 * result + (getAppOperation() != null ? getAppOperation().hashCode() : 0);
    result = 31 * result + (getBookingType() != null ? getBookingType().hashCode() : 0);
    result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AppBookingCashNewFragmentArgs{"
        + "appPBookingId=" + getAppPBookingId()
        + ", appOperation=" + getAppOperation()
        + ", bookingType=" + getBookingType()
        + ", isHasCreatePermission=" + getIsHasCreatePermission()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AppBookingCashNewFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull String appOperation, @NonNull String bookingType,
        boolean isHasCreatePermission) {
      if (appOperation == null) {
        throw new IllegalArgumentException("Argument \"appOperation\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("appOperation", appOperation);
      if (bookingType == null) {
        throw new IllegalArgumentException("Argument \"bookingType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingType", bookingType);
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public AppBookingCashNewFragmentArgs build() {
      AppBookingCashNewFragmentArgs result = new AppBookingCashNewFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setAppPBookingId(@Nullable UUID appPBookingId) {
      this.arguments.put("appPBookingId", appPBookingId);
      return this;
    }

    @NonNull
    public Builder setAppOperation(@NonNull String appOperation) {
      if (appOperation == null) {
        throw new IllegalArgumentException("Argument \"appOperation\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("appOperation", appOperation);
      return this;
    }

    @NonNull
    public Builder setBookingType(@NonNull String bookingType) {
      if (bookingType == null) {
        throw new IllegalArgumentException("Argument \"bookingType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingType", bookingType);
      return this;
    }

    @NonNull
    public Builder setIsHasCreatePermission(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getAppPBookingId() {
      return (UUID) arguments.get("appPBookingId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getAppOperation() {
      return (String) arguments.get("appOperation");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getBookingType() {
      return (String) arguments.get("bookingType");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }
  }
}
