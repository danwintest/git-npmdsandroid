package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class DroppingDailyTaskWfFragmentDirections {
  private DroppingDailyTaskWfFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
