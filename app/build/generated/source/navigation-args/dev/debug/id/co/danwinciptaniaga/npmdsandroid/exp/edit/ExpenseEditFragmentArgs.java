package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class ExpenseEditFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private ExpenseEditFragmentArgs() {
  }

  private ExpenseEditFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static ExpenseEditFragmentArgs fromBundle(@NonNull Bundle bundle) {
    ExpenseEditFragmentArgs __result = new ExpenseEditFragmentArgs();
    bundle.setClassLoader(ExpenseEditFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("expenseId")) {
      UUID expenseId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        expenseId = (UUID) bundle.get("expenseId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("expenseId", expenseId);
    } else {
      __result.arguments.put("expenseId", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public UUID getExpenseId() {
    return (UUID) arguments.get("expenseId");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("expenseId")) {
      UUID expenseId = (UUID) arguments.get("expenseId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || expenseId == null) {
        __result.putParcelable("expenseId", Parcelable.class.cast(expenseId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("expenseId", Serializable.class.cast(expenseId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("expenseId", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    ExpenseEditFragmentArgs that = (ExpenseEditFragmentArgs) object;
    if (arguments.containsKey("expenseId") != that.arguments.containsKey("expenseId")) {
      return false;
    }
    if (getExpenseId() != null ? !getExpenseId().equals(that.getExpenseId()) : that.getExpenseId() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getExpenseId() != null ? getExpenseId().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "ExpenseEditFragmentArgs{"
        + "expenseId=" + getExpenseId()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(ExpenseEditFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public ExpenseEditFragmentArgs build() {
      ExpenseEditFragmentArgs result = new ExpenseEditFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setExpenseId(@Nullable UUID expenseId) {
      this.arguments.put("expenseId", expenseId);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getExpenseId() {
      return (UUID) arguments.get("expenseId");
    }
  }
}
