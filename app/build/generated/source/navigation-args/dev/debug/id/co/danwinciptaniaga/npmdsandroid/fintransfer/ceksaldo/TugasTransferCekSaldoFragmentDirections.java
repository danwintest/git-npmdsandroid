package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class TugasTransferCekSaldoFragmentDirections {
  private TugasTransferCekSaldoFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
