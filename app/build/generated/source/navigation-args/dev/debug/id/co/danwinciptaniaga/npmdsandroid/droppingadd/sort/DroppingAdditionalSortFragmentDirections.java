package id.co.danwinciptaniaga.npmdsandroid.droppingadd.sort;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class DroppingAdditionalSortFragmentDirections {
  private DroppingAdditionalSortFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
