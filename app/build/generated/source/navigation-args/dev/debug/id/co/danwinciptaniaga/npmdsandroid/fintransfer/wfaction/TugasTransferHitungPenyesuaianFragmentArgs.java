package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.math.BigDecimal;
import java.util.HashMap;

public class TugasTransferHitungPenyesuaianFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private TugasTransferHitungPenyesuaianFragmentArgs() {
  }

  private TugasTransferHitungPenyesuaianFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static TugasTransferHitungPenyesuaianFragmentArgs fromBundle(@NonNull Bundle bundle) {
    TugasTransferHitungPenyesuaianFragmentArgs __result = new TugasTransferHitungPenyesuaianFragmentArgs();
    bundle.setClassLoader(TugasTransferHitungPenyesuaianFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("penyesuaianPercentage")) {
      BigDecimal penyesuaianPercentage;
      if (Parcelable.class.isAssignableFrom(BigDecimal.class) || Serializable.class.isAssignableFrom(BigDecimal.class)) {
        penyesuaianPercentage = (BigDecimal) bundle.get("penyesuaianPercentage");
      } else {
        throw new UnsupportedOperationException(BigDecimal.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (penyesuaianPercentage == null) {
        throw new IllegalArgumentException("Argument \"penyesuaianPercentage\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("penyesuaianPercentage", penyesuaianPercentage);
    } else {
      throw new IllegalArgumentException("Required argument \"penyesuaianPercentage\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public BigDecimal getPenyesuaianPercentage() {
    return (BigDecimal) arguments.get("penyesuaianPercentage");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("penyesuaianPercentage")) {
      BigDecimal penyesuaianPercentage = (BigDecimal) arguments.get("penyesuaianPercentage");
      if (Parcelable.class.isAssignableFrom(BigDecimal.class) || penyesuaianPercentage == null) {
        __result.putParcelable("penyesuaianPercentage", Parcelable.class.cast(penyesuaianPercentage));
      } else if (Serializable.class.isAssignableFrom(BigDecimal.class)) {
        __result.putSerializable("penyesuaianPercentage", Serializable.class.cast(penyesuaianPercentage));
      } else {
        throw new UnsupportedOperationException(BigDecimal.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    TugasTransferHitungPenyesuaianFragmentArgs that = (TugasTransferHitungPenyesuaianFragmentArgs) object;
    if (arguments.containsKey("penyesuaianPercentage") != that.arguments.containsKey("penyesuaianPercentage")) {
      return false;
    }
    if (getPenyesuaianPercentage() != null ? !getPenyesuaianPercentage().equals(that.getPenyesuaianPercentage()) : that.getPenyesuaianPercentage() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getPenyesuaianPercentage() != null ? getPenyesuaianPercentage().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "TugasTransferHitungPenyesuaianFragmentArgs{"
        + "penyesuaianPercentage=" + getPenyesuaianPercentage()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(TugasTransferHitungPenyesuaianFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull BigDecimal penyesuaianPercentage) {
      if (penyesuaianPercentage == null) {
        throw new IllegalArgumentException("Argument \"penyesuaianPercentage\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("penyesuaianPercentage", penyesuaianPercentage);
    }

    @NonNull
    public TugasTransferHitungPenyesuaianFragmentArgs build() {
      TugasTransferHitungPenyesuaianFragmentArgs result = new TugasTransferHitungPenyesuaianFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setPenyesuaianPercentage(@NonNull BigDecimal penyesuaianPercentage) {
      if (penyesuaianPercentage == null) {
        throw new IllegalArgumentException("Argument \"penyesuaianPercentage\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("penyesuaianPercentage", penyesuaianPercentage);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public BigDecimal getPenyesuaianPercentage() {
      return (BigDecimal) arguments.get("penyesuaianPercentage");
    }
  }
}
