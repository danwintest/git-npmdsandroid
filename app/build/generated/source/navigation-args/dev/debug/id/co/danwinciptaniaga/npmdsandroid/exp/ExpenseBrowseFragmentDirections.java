package id.co.danwinciptaniaga.npmdsandroid.exp;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class ExpenseBrowseFragmentDirections {
  private ExpenseBrowseFragmentDirections() {
  }

  @NonNull
  public static ActionExpenseEditFragment actionExpenseEditFragment() {
    return new ActionExpenseEditFragment();
  }

  @NonNull
  public static ActionExpenseFilterFragment actionExpenseFilterFragment() {
    return new ActionExpenseFilterFragment();
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionExpenseEditFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionExpenseEditFragment() {
    }

    @NonNull
    public ActionExpenseEditFragment setExpenseId(@Nullable UUID expenseId) {
      this.arguments.put("expenseId", expenseId);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("expenseId")) {
        UUID expenseId = (UUID) arguments.get("expenseId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || expenseId == null) {
          __result.putParcelable("expenseId", Parcelable.class.cast(expenseId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("expenseId", Serializable.class.cast(expenseId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("expenseId", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_expenseEditFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getExpenseId() {
      return (UUID) arguments.get("expenseId");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionExpenseEditFragment that = (ActionExpenseEditFragment) object;
      if (arguments.containsKey("expenseId") != that.arguments.containsKey("expenseId")) {
        return false;
      }
      if (getExpenseId() != null ? !getExpenseId().equals(that.getExpenseId()) : that.getExpenseId() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getExpenseId() != null ? getExpenseId().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionExpenseEditFragment(actionId=" + getActionId() + "){"
          + "expenseId=" + getExpenseId()
          + "}";
    }
  }

  public static class ActionExpenseFilterFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionExpenseFilterFragment() {
    }

    @NonNull
    public ActionExpenseFilterFragment setFilterField(@Nullable ExpenseBrowseFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("filterField")) {
        ExpenseBrowseFilter filterField = (ExpenseBrowseFilter) arguments.get("filterField");
        if (Parcelable.class.isAssignableFrom(ExpenseBrowseFilter.class) || filterField == null) {
          __result.putParcelable("filterField", Parcelable.class.cast(filterField));
        } else if (Serializable.class.isAssignableFrom(ExpenseBrowseFilter.class)) {
          __result.putSerializable("filterField", Serializable.class.cast(filterField));
        } else {
          throw new UnsupportedOperationException(ExpenseBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("filterField", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_expenseFilterFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ExpenseBrowseFilter getFilterField() {
      return (ExpenseBrowseFilter) arguments.get("filterField");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionExpenseFilterFragment that = (ActionExpenseFilterFragment) object;
      if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
        return false;
      }
      if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionExpenseFilterFragment(actionId=" + getActionId() + "){"
          + "filterField=" + getFilterField()
          + "}";
    }
  }
}
