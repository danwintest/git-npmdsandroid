package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalData;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class DroppingAdditionalBrowseFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private DroppingAdditionalBrowseFragmentArgs() {
  }

  private DroppingAdditionalBrowseFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static DroppingAdditionalBrowseFragmentArgs fromBundle(@NonNull Bundle bundle) {
    DroppingAdditionalBrowseFragmentArgs __result = new DroppingAdditionalBrowseFragmentArgs();
    bundle.setClassLoader(DroppingAdditionalBrowseFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("droppingAdditionalData")) {
      DroppingAdditionalData droppingAdditionalData;
      if (Parcelable.class.isAssignableFrom(DroppingAdditionalData.class) || Serializable.class.isAssignableFrom(DroppingAdditionalData.class)) {
        droppingAdditionalData = (DroppingAdditionalData) bundle.get("droppingAdditionalData");
      } else {
        throw new UnsupportedOperationException(DroppingAdditionalData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("droppingAdditionalData", droppingAdditionalData);
    } else {
      __result.arguments.put("droppingAdditionalData", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public DroppingAdditionalData getDroppingAdditionalData() {
    return (DroppingAdditionalData) arguments.get("droppingAdditionalData");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("droppingAdditionalData")) {
      DroppingAdditionalData droppingAdditionalData = (DroppingAdditionalData) arguments.get("droppingAdditionalData");
      if (Parcelable.class.isAssignableFrom(DroppingAdditionalData.class) || droppingAdditionalData == null) {
        __result.putParcelable("droppingAdditionalData", Parcelable.class.cast(droppingAdditionalData));
      } else if (Serializable.class.isAssignableFrom(DroppingAdditionalData.class)) {
        __result.putSerializable("droppingAdditionalData", Serializable.class.cast(droppingAdditionalData));
      } else {
        throw new UnsupportedOperationException(DroppingAdditionalData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("droppingAdditionalData", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    DroppingAdditionalBrowseFragmentArgs that = (DroppingAdditionalBrowseFragmentArgs) object;
    if (arguments.containsKey("droppingAdditionalData") != that.arguments.containsKey("droppingAdditionalData")) {
      return false;
    }
    if (getDroppingAdditionalData() != null ? !getDroppingAdditionalData().equals(that.getDroppingAdditionalData()) : that.getDroppingAdditionalData() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getDroppingAdditionalData() != null ? getDroppingAdditionalData().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "DroppingAdditionalBrowseFragmentArgs{"
        + "droppingAdditionalData=" + getDroppingAdditionalData()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(DroppingAdditionalBrowseFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public DroppingAdditionalBrowseFragmentArgs build() {
      DroppingAdditionalBrowseFragmentArgs result = new DroppingAdditionalBrowseFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setDroppingAdditionalData(
        @Nullable DroppingAdditionalData droppingAdditionalData) {
      this.arguments.put("droppingAdditionalData", droppingAdditionalData);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public DroppingAdditionalData getDroppingAdditionalData() {
      return (DroppingAdditionalData) arguments.get("droppingAdditionalData");
    }
  }
}
