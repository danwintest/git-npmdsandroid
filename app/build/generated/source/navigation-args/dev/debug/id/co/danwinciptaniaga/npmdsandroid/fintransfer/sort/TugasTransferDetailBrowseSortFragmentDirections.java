package id.co.danwinciptaniaga.npmdsandroid.fintransfer.sort;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class TugasTransferDetailBrowseSortFragmentDirections {
  private TugasTransferDetailBrowseSortFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
