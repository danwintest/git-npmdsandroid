package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class AppBookingTransferRekeningFormArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AppBookingTransferRekeningFormArgs() {
  }

  private AppBookingTransferRekeningFormArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AppBookingTransferRekeningFormArgs fromBundle(@NonNull Bundle bundle) {
    AppBookingTransferRekeningFormArgs __result = new AppBookingTransferRekeningFormArgs();
    bundle.setClassLoader(AppBookingTransferRekeningFormArgs.class.getClassLoader());
    if (bundle.containsKey("outletId")) {
      UUID outletId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        outletId = (UUID) bundle.get("outletId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("outletId", outletId);
    } else {
      throw new IllegalArgumentException("Required argument \"outletId\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("mode")) {
      String mode;
      mode = bundle.getString("mode");
      if (mode == null) {
        throw new IllegalArgumentException("Argument \"mode\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("mode", mode);
    } else {
      throw new IllegalArgumentException("Required argument \"mode\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("appBookingId")) {
      UUID appBookingId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        appBookingId = (UUID) bundle.get("appBookingId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("appBookingId", appBookingId);
    } else {
      __result.arguments.put("appBookingId", null);
    }
    if (bundle.containsKey("bookingId")) {
      UUID bookingId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        bookingId = (UUID) bundle.get("bookingId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("bookingId", bookingId);
    } else {
      __result.arguments.put("bookingId", null);
    }
    if (bundle.containsKey("companyId")) {
      UUID companyId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        companyId = (UUID) bundle.get("companyId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (companyId == null) {
        throw new IllegalArgumentException("Argument \"companyId\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("companyId", companyId);
    } else {
      throw new IllegalArgumentException("Required argument \"companyId\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("fromBookingStatus")) {
      boolean fromBookingStatus;
      fromBookingStatus = bundle.getBoolean("fromBookingStatus");
      __result.arguments.put("fromBookingStatus", fromBookingStatus);
    } else {
      throw new IllegalArgumentException("Required argument \"fromBookingStatus\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("consumerName")) {
      String consumerName;
      consumerName = bundle.getString("consumerName");
      __result.arguments.put("consumerName", consumerName);
    } else {
      throw new IllegalArgumentException("Required argument \"consumerName\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("accName")) {
      String accName;
      accName = bundle.getString("accName");
      __result.arguments.put("accName", accName);
    } else {
      throw new IllegalArgumentException("Required argument \"accName\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("accNo")) {
      String accNo;
      accNo = bundle.getString("accNo");
      __result.arguments.put("accNo", accNo);
    } else {
      throw new IllegalArgumentException("Required argument \"accNo\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("bankId")) {
      UUID bankId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        bankId = (UUID) bundle.get("bankId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("bankId", bankId);
    } else {
      throw new IllegalArgumentException("Required argument \"bankId\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("otherBankName")) {
      String otherBankName;
      otherBankName = bundle.getString("otherBankName");
      __result.arguments.put("otherBankName", otherBankName);
    } else {
      throw new IllegalArgumentException("Required argument \"otherBankName\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("validStatus")) {
      boolean validStatus;
      validStatus = bundle.getBoolean("validStatus");
      __result.arguments.put("validStatus", validStatus);
    } else {
      throw new IllegalArgumentException("Required argument \"validStatus\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public UUID getOutletId() {
    return (UUID) arguments.get("outletId");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getMode() {
    return (String) arguments.get("mode");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public UUID getAppBookingId() {
    return (UUID) arguments.get("appBookingId");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public UUID getBookingId() {
    return (UUID) arguments.get("bookingId");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public UUID getCompanyId() {
    return (UUID) arguments.get("companyId");
  }

  @SuppressWarnings("unchecked")
  public boolean getFromBookingStatus() {
    return (boolean) arguments.get("fromBookingStatus");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getConsumerName() {
    return (String) arguments.get("consumerName");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getAccName() {
    return (String) arguments.get("accName");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getAccNo() {
    return (String) arguments.get("accNo");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public UUID getBankId() {
    return (UUID) arguments.get("bankId");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getOtherBankName() {
    return (String) arguments.get("otherBankName");
  }

  @SuppressWarnings("unchecked")
  public boolean getValidStatus() {
    return (boolean) arguments.get("validStatus");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("outletId")) {
      UUID outletId = (UUID) arguments.get("outletId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || outletId == null) {
        __result.putParcelable("outletId", Parcelable.class.cast(outletId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("outletId", Serializable.class.cast(outletId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("mode")) {
      String mode = (String) arguments.get("mode");
      __result.putString("mode", mode);
    }
    if (arguments.containsKey("appBookingId")) {
      UUID appBookingId = (UUID) arguments.get("appBookingId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || appBookingId == null) {
        __result.putParcelable("appBookingId", Parcelable.class.cast(appBookingId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("appBookingId", Serializable.class.cast(appBookingId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("appBookingId", null);
    }
    if (arguments.containsKey("bookingId")) {
      UUID bookingId = (UUID) arguments.get("bookingId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || bookingId == null) {
        __result.putParcelable("bookingId", Parcelable.class.cast(bookingId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("bookingId", Serializable.class.cast(bookingId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("bookingId", null);
    }
    if (arguments.containsKey("companyId")) {
      UUID companyId = (UUID) arguments.get("companyId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || companyId == null) {
        __result.putParcelable("companyId", Parcelable.class.cast(companyId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("companyId", Serializable.class.cast(companyId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("fromBookingStatus")) {
      boolean fromBookingStatus = (boolean) arguments.get("fromBookingStatus");
      __result.putBoolean("fromBookingStatus", fromBookingStatus);
    }
    if (arguments.containsKey("consumerName")) {
      String consumerName = (String) arguments.get("consumerName");
      __result.putString("consumerName", consumerName);
    }
    if (arguments.containsKey("accName")) {
      String accName = (String) arguments.get("accName");
      __result.putString("accName", accName);
    }
    if (arguments.containsKey("accNo")) {
      String accNo = (String) arguments.get("accNo");
      __result.putString("accNo", accNo);
    }
    if (arguments.containsKey("bankId")) {
      UUID bankId = (UUID) arguments.get("bankId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || bankId == null) {
        __result.putParcelable("bankId", Parcelable.class.cast(bankId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("bankId", Serializable.class.cast(bankId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("otherBankName")) {
      String otherBankName = (String) arguments.get("otherBankName");
      __result.putString("otherBankName", otherBankName);
    }
    if (arguments.containsKey("validStatus")) {
      boolean validStatus = (boolean) arguments.get("validStatus");
      __result.putBoolean("validStatus", validStatus);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AppBookingTransferRekeningFormArgs that = (AppBookingTransferRekeningFormArgs) object;
    if (arguments.containsKey("outletId") != that.arguments.containsKey("outletId")) {
      return false;
    }
    if (getOutletId() != null ? !getOutletId().equals(that.getOutletId()) : that.getOutletId() != null) {
      return false;
    }
    if (arguments.containsKey("mode") != that.arguments.containsKey("mode")) {
      return false;
    }
    if (getMode() != null ? !getMode().equals(that.getMode()) : that.getMode() != null) {
      return false;
    }
    if (arguments.containsKey("appBookingId") != that.arguments.containsKey("appBookingId")) {
      return false;
    }
    if (getAppBookingId() != null ? !getAppBookingId().equals(that.getAppBookingId()) : that.getAppBookingId() != null) {
      return false;
    }
    if (arguments.containsKey("bookingId") != that.arguments.containsKey("bookingId")) {
      return false;
    }
    if (getBookingId() != null ? !getBookingId().equals(that.getBookingId()) : that.getBookingId() != null) {
      return false;
    }
    if (arguments.containsKey("companyId") != that.arguments.containsKey("companyId")) {
      return false;
    }
    if (getCompanyId() != null ? !getCompanyId().equals(that.getCompanyId()) : that.getCompanyId() != null) {
      return false;
    }
    if (arguments.containsKey("fromBookingStatus") != that.arguments.containsKey("fromBookingStatus")) {
      return false;
    }
    if (getFromBookingStatus() != that.getFromBookingStatus()) {
      return false;
    }
    if (arguments.containsKey("consumerName") != that.arguments.containsKey("consumerName")) {
      return false;
    }
    if (getConsumerName() != null ? !getConsumerName().equals(that.getConsumerName()) : that.getConsumerName() != null) {
      return false;
    }
    if (arguments.containsKey("accName") != that.arguments.containsKey("accName")) {
      return false;
    }
    if (getAccName() != null ? !getAccName().equals(that.getAccName()) : that.getAccName() != null) {
      return false;
    }
    if (arguments.containsKey("accNo") != that.arguments.containsKey("accNo")) {
      return false;
    }
    if (getAccNo() != null ? !getAccNo().equals(that.getAccNo()) : that.getAccNo() != null) {
      return false;
    }
    if (arguments.containsKey("bankId") != that.arguments.containsKey("bankId")) {
      return false;
    }
    if (getBankId() != null ? !getBankId().equals(that.getBankId()) : that.getBankId() != null) {
      return false;
    }
    if (arguments.containsKey("otherBankName") != that.arguments.containsKey("otherBankName")) {
      return false;
    }
    if (getOtherBankName() != null ? !getOtherBankName().equals(that.getOtherBankName()) : that.getOtherBankName() != null) {
      return false;
    }
    if (arguments.containsKey("validStatus") != that.arguments.containsKey("validStatus")) {
      return false;
    }
    if (getValidStatus() != that.getValidStatus()) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getOutletId() != null ? getOutletId().hashCode() : 0);
    result = 31 * result + (getMode() != null ? getMode().hashCode() : 0);
    result = 31 * result + (getAppBookingId() != null ? getAppBookingId().hashCode() : 0);
    result = 31 * result + (getBookingId() != null ? getBookingId().hashCode() : 0);
    result = 31 * result + (getCompanyId() != null ? getCompanyId().hashCode() : 0);
    result = 31 * result + (getFromBookingStatus() ? 1 : 0);
    result = 31 * result + (getConsumerName() != null ? getConsumerName().hashCode() : 0);
    result = 31 * result + (getAccName() != null ? getAccName().hashCode() : 0);
    result = 31 * result + (getAccNo() != null ? getAccNo().hashCode() : 0);
    result = 31 * result + (getBankId() != null ? getBankId().hashCode() : 0);
    result = 31 * result + (getOtherBankName() != null ? getOtherBankName().hashCode() : 0);
    result = 31 * result + (getValidStatus() ? 1 : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AppBookingTransferRekeningFormArgs{"
        + "outletId=" + getOutletId()
        + ", mode=" + getMode()
        + ", appBookingId=" + getAppBookingId()
        + ", bookingId=" + getBookingId()
        + ", companyId=" + getCompanyId()
        + ", fromBookingStatus=" + getFromBookingStatus()
        + ", consumerName=" + getConsumerName()
        + ", accName=" + getAccName()
        + ", accNo=" + getAccNo()
        + ", bankId=" + getBankId()
        + ", otherBankName=" + getOtherBankName()
        + ", validStatus=" + getValidStatus()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AppBookingTransferRekeningFormArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull UUID outletId, @NonNull String mode, @NonNull UUID companyId,
        boolean fromBookingStatus, @Nullable String consumerName, @Nullable String accName,
        @Nullable String accNo, @Nullable UUID bankId, @Nullable String otherBankName,
        boolean validStatus) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      if (mode == null) {
        throw new IllegalArgumentException("Argument \"mode\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("mode", mode);
      if (companyId == null) {
        throw new IllegalArgumentException("Argument \"companyId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyId", companyId);
      this.arguments.put("fromBookingStatus", fromBookingStatus);
      this.arguments.put("consumerName", consumerName);
      this.arguments.put("accName", accName);
      this.arguments.put("accNo", accNo);
      this.arguments.put("bankId", bankId);
      this.arguments.put("otherBankName", otherBankName);
      this.arguments.put("validStatus", validStatus);
    }

    @NonNull
    public AppBookingTransferRekeningFormArgs build() {
      AppBookingTransferRekeningFormArgs result = new AppBookingTransferRekeningFormArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setOutletId(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      return this;
    }

    @NonNull
    public Builder setMode(@NonNull String mode) {
      if (mode == null) {
        throw new IllegalArgumentException("Argument \"mode\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("mode", mode);
      return this;
    }

    @NonNull
    public Builder setAppBookingId(@Nullable UUID appBookingId) {
      this.arguments.put("appBookingId", appBookingId);
      return this;
    }

    @NonNull
    public Builder setBookingId(@Nullable UUID bookingId) {
      this.arguments.put("bookingId", bookingId);
      return this;
    }

    @NonNull
    public Builder setCompanyId(@NonNull UUID companyId) {
      if (companyId == null) {
        throw new IllegalArgumentException("Argument \"companyId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyId", companyId);
      return this;
    }

    @NonNull
    public Builder setFromBookingStatus(boolean fromBookingStatus) {
      this.arguments.put("fromBookingStatus", fromBookingStatus);
      return this;
    }

    @NonNull
    public Builder setConsumerName(@Nullable String consumerName) {
      this.arguments.put("consumerName", consumerName);
      return this;
    }

    @NonNull
    public Builder setAccName(@Nullable String accName) {
      this.arguments.put("accName", accName);
      return this;
    }

    @NonNull
    public Builder setAccNo(@Nullable String accNo) {
      this.arguments.put("accNo", accNo);
      return this;
    }

    @NonNull
    public Builder setBankId(@Nullable UUID bankId) {
      this.arguments.put("bankId", bankId);
      return this;
    }

    @NonNull
    public Builder setOtherBankName(@Nullable String otherBankName) {
      this.arguments.put("otherBankName", otherBankName);
      return this;
    }

    @NonNull
    public Builder setValidStatus(boolean validStatus) {
      this.arguments.put("validStatus", validStatus);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getOutletId() {
      return (UUID) arguments.get("outletId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getMode() {
      return (String) arguments.get("mode");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getAppBookingId() {
      return (UUID) arguments.get("appBookingId");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getBookingId() {
      return (UUID) arguments.get("bookingId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getCompanyId() {
      return (UUID) arguments.get("companyId");
    }

    @SuppressWarnings("unchecked")
    public boolean getFromBookingStatus() {
      return (boolean) arguments.get("fromBookingStatus");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getConsumerName() {
      return (String) arguments.get("consumerName");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getAccName() {
      return (String) arguments.get("accName");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getAccNo() {
      return (String) arguments.get("accNo");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getBankId() {
      return (UUID) arguments.get("bankId");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getOtherBankName() {
      return (String) arguments.get("otherBankName");
    }

    @SuppressWarnings("unchecked")
    public boolean getValidStatus() {
      return (boolean) arguments.get("validStatus");
    }
  }
}
