package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class DroppingDailyTaskFilterFragmentDirections {
  private DroppingDailyTaskFilterFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
