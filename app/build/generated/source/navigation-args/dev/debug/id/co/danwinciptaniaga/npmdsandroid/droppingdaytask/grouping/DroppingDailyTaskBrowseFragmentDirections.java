package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.math.BigDecimal;
import java.util.HashMap;

public class DroppingDailyTaskBrowseFragmentDirections {
  private DroppingDailyTaskBrowseFragmentDirections() {
  }

  @NonNull
  public static ActionFilter actionFilter(boolean isGrouping) {
    return new ActionFilter(isGrouping);
  }

  @NonNull
  public static ActionSort actionSort() {
    return new ActionSort();
  }

  @NonNull
  public static ActionDetail actionDetail(@NonNull DroppingDailyTaskBrowseFilter filterData,
      @NonNull BigDecimal totalAmt) {
    return new ActionDetail(filterData, totalAmt);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionFilter implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionFilter(boolean isGrouping) {
      this.arguments.put("isGrouping", isGrouping);
    }

    @NonNull
    public ActionFilter setFilterField(@Nullable DroppingDailyTaskBrowseFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @NonNull
    public ActionFilter setIsGrouping(boolean isGrouping) {
      this.arguments.put("isGrouping", isGrouping);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("filterField")) {
        DroppingDailyTaskBrowseFilter filterField = (DroppingDailyTaskBrowseFilter) arguments.get("filterField");
        if (Parcelable.class.isAssignableFrom(DroppingDailyTaskBrowseFilter.class) || filterField == null) {
          __result.putParcelable("filterField", Parcelable.class.cast(filterField));
        } else if (Serializable.class.isAssignableFrom(DroppingDailyTaskBrowseFilter.class)) {
          __result.putSerializable("filterField", Serializable.class.cast(filterField));
        } else {
          throw new UnsupportedOperationException(DroppingDailyTaskBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("filterField", null);
      }
      if (arguments.containsKey("isGrouping")) {
        boolean isGrouping = (boolean) arguments.get("isGrouping");
        __result.putBoolean("isGrouping", isGrouping);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_filter;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public DroppingDailyTaskBrowseFilter getFilterField() {
      return (DroppingDailyTaskBrowseFilter) arguments.get("filterField");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsGrouping() {
      return (boolean) arguments.get("isGrouping");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionFilter that = (ActionFilter) object;
      if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
        return false;
      }
      if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
        return false;
      }
      if (arguments.containsKey("isGrouping") != that.arguments.containsKey("isGrouping")) {
        return false;
      }
      if (getIsGrouping() != that.getIsGrouping()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
      result = 31 * result + (getIsGrouping() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionFilter(actionId=" + getActionId() + "){"
          + "filterField=" + getFilterField()
          + ", isGrouping=" + getIsGrouping()
          + "}";
    }
  }

  public static class ActionSort implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionSort() {
    }

    @NonNull
    public ActionSort setSortField(@Nullable String sortField) {
      this.arguments.put("sortField", sortField);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("sortField")) {
        String sortField = (String) arguments.get("sortField");
        __result.putString("sortField", sortField);
      } else {
        __result.putString("sortField", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_sort;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getSortField() {
      return (String) arguments.get("sortField");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionSort that = (ActionSort) object;
      if (arguments.containsKey("sortField") != that.arguments.containsKey("sortField")) {
        return false;
      }
      if (getSortField() != null ? !getSortField().equals(that.getSortField()) : that.getSortField() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getSortField() != null ? getSortField().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionSort(actionId=" + getActionId() + "){"
          + "sortField=" + getSortField()
          + "}";
    }
  }

  public static class ActionDetail implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionDetail(@NonNull DroppingDailyTaskBrowseFilter filterData,
        @NonNull BigDecimal totalAmt) {
      if (filterData == null) {
        throw new IllegalArgumentException("Argument \"filterData\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("filterData", filterData);
      if (totalAmt == null) {
        throw new IllegalArgumentException("Argument \"totalAmt\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("totalAmt", totalAmt);
    }

    @NonNull
    public ActionDetail setFilterData(@NonNull DroppingDailyTaskBrowseFilter filterData) {
      if (filterData == null) {
        throw new IllegalArgumentException("Argument \"filterData\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("filterData", filterData);
      return this;
    }

    @NonNull
    public ActionDetail setTotalAmt(@NonNull BigDecimal totalAmt) {
      if (totalAmt == null) {
        throw new IllegalArgumentException("Argument \"totalAmt\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("totalAmt", totalAmt);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("filterData")) {
        DroppingDailyTaskBrowseFilter filterData = (DroppingDailyTaskBrowseFilter) arguments.get("filterData");
        if (Parcelable.class.isAssignableFrom(DroppingDailyTaskBrowseFilter.class) || filterData == null) {
          __result.putParcelable("filterData", Parcelable.class.cast(filterData));
        } else if (Serializable.class.isAssignableFrom(DroppingDailyTaskBrowseFilter.class)) {
          __result.putSerializable("filterData", Serializable.class.cast(filterData));
        } else {
          throw new UnsupportedOperationException(DroppingDailyTaskBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("totalAmt")) {
        BigDecimal totalAmt = (BigDecimal) arguments.get("totalAmt");
        if (Parcelable.class.isAssignableFrom(BigDecimal.class) || totalAmt == null) {
          __result.putParcelable("totalAmt", Parcelable.class.cast(totalAmt));
        } else if (Serializable.class.isAssignableFrom(BigDecimal.class)) {
          __result.putSerializable("totalAmt", Serializable.class.cast(totalAmt));
        } else {
          throw new UnsupportedOperationException(BigDecimal.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_detail;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public DroppingDailyTaskBrowseFilter getFilterData() {
      return (DroppingDailyTaskBrowseFilter) arguments.get("filterData");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public BigDecimal getTotalAmt() {
      return (BigDecimal) arguments.get("totalAmt");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionDetail that = (ActionDetail) object;
      if (arguments.containsKey("filterData") != that.arguments.containsKey("filterData")) {
        return false;
      }
      if (getFilterData() != null ? !getFilterData().equals(that.getFilterData()) : that.getFilterData() != null) {
        return false;
      }
      if (arguments.containsKey("totalAmt") != that.arguments.containsKey("totalAmt")) {
        return false;
      }
      if (getTotalAmt() != null ? !getTotalAmt().equals(that.getTotalAmt()) : that.getTotalAmt() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getFilterData() != null ? getFilterData().hashCode() : 0);
      result = 31 * result + (getTotalAmt() != null ? getTotalAmt().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionDetail(actionId=" + getActionId() + "){"
          + "filterData=" + getFilterData()
          + ", totalAmt=" + getTotalAmt()
          + "}";
    }
  }
}
