package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class AppBookingMultiActionFragmentDirections {
  private AppBookingMultiActionFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
