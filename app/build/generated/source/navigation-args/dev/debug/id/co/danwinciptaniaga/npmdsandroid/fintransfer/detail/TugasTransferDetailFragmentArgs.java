package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class TugasTransferDetailFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private TugasTransferDetailFragmentArgs() {
  }

  private TugasTransferDetailFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static TugasTransferDetailFragmentArgs fromBundle(@NonNull Bundle bundle) {
    TugasTransferDetailFragmentArgs __result = new TugasTransferDetailFragmentArgs();
    bundle.setClassLoader(TugasTransferDetailFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("filterData")) {
      TugasTransferBrowseAndroidFilter filterData;
      if (Parcelable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class) || Serializable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class)) {
        filterData = (TugasTransferBrowseAndroidFilter) bundle.get("filterData");
      } else {
        throw new UnsupportedOperationException(TugasTransferBrowseAndroidFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (filterData == null) {
        throw new IllegalArgumentException("Argument \"filterData\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("filterData", filterData);
    } else {
      throw new IllegalArgumentException("Required argument \"filterData\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public TugasTransferBrowseAndroidFilter getFilterData() {
    return (TugasTransferBrowseAndroidFilter) arguments.get("filterData");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("filterData")) {
      TugasTransferBrowseAndroidFilter filterData = (TugasTransferBrowseAndroidFilter) arguments.get("filterData");
      if (Parcelable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class) || filterData == null) {
        __result.putParcelable("filterData", Parcelable.class.cast(filterData));
      } else if (Serializable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class)) {
        __result.putSerializable("filterData", Serializable.class.cast(filterData));
      } else {
        throw new UnsupportedOperationException(TugasTransferBrowseAndroidFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    TugasTransferDetailFragmentArgs that = (TugasTransferDetailFragmentArgs) object;
    if (arguments.containsKey("filterData") != that.arguments.containsKey("filterData")) {
      return false;
    }
    if (getFilterData() != null ? !getFilterData().equals(that.getFilterData()) : that.getFilterData() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getFilterData() != null ? getFilterData().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "TugasTransferDetailFragmentArgs{"
        + "filterData=" + getFilterData()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(TugasTransferDetailFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull TugasTransferBrowseAndroidFilter filterData) {
      if (filterData == null) {
        throw new IllegalArgumentException("Argument \"filterData\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("filterData", filterData);
    }

    @NonNull
    public TugasTransferDetailFragmentArgs build() {
      TugasTransferDetailFragmentArgs result = new TugasTransferDetailFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setFilterData(@NonNull TugasTransferBrowseAndroidFilter filterData) {
      if (filterData == null) {
        throw new IllegalArgumentException("Argument \"filterData\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("filterData", filterData);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public TugasTransferBrowseAndroidFilter getFilterData() {
      return (TugasTransferBrowseAndroidFilter) arguments.get("filterData");
    }
  }
}
