package id.co.danwinciptaniaga.npmdsandroid;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;

public class MobileNavigationDirections {
  private MobileNavigationDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return new ActionOnlyNavDirections(R.id.nav_logout);
  }
}
