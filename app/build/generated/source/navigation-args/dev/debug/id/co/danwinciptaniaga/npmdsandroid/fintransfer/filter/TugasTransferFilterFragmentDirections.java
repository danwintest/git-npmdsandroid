package id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class TugasTransferFilterFragmentDirections {
  private TugasTransferFilterFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
