package id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class SelectBookingFragmentDirections {
  private SelectBookingFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
