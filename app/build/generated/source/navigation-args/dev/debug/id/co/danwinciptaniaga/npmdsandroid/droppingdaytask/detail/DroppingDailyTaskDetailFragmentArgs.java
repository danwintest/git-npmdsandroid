package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.math.BigDecimal;
import java.util.HashMap;

public class DroppingDailyTaskDetailFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private DroppingDailyTaskDetailFragmentArgs() {
  }

  private DroppingDailyTaskDetailFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static DroppingDailyTaskDetailFragmentArgs fromBundle(@NonNull Bundle bundle) {
    DroppingDailyTaskDetailFragmentArgs __result = new DroppingDailyTaskDetailFragmentArgs();
    bundle.setClassLoader(DroppingDailyTaskDetailFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("filterData")) {
      DroppingDailyTaskBrowseFilter filterData;
      if (Parcelable.class.isAssignableFrom(DroppingDailyTaskBrowseFilter.class) || Serializable.class.isAssignableFrom(DroppingDailyTaskBrowseFilter.class)) {
        filterData = (DroppingDailyTaskBrowseFilter) bundle.get("filterData");
      } else {
        throw new UnsupportedOperationException(DroppingDailyTaskBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (filterData == null) {
        throw new IllegalArgumentException("Argument \"filterData\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("filterData", filterData);
    } else {
      throw new IllegalArgumentException("Required argument \"filterData\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("totalAmt")) {
      BigDecimal totalAmt;
      if (Parcelable.class.isAssignableFrom(BigDecimal.class) || Serializable.class.isAssignableFrom(BigDecimal.class)) {
        totalAmt = (BigDecimal) bundle.get("totalAmt");
      } else {
        throw new UnsupportedOperationException(BigDecimal.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (totalAmt == null) {
        throw new IllegalArgumentException("Argument \"totalAmt\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("totalAmt", totalAmt);
    } else {
      throw new IllegalArgumentException("Required argument \"totalAmt\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public DroppingDailyTaskBrowseFilter getFilterData() {
    return (DroppingDailyTaskBrowseFilter) arguments.get("filterData");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public BigDecimal getTotalAmt() {
    return (BigDecimal) arguments.get("totalAmt");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("filterData")) {
      DroppingDailyTaskBrowseFilter filterData = (DroppingDailyTaskBrowseFilter) arguments.get("filterData");
      if (Parcelable.class.isAssignableFrom(DroppingDailyTaskBrowseFilter.class) || filterData == null) {
        __result.putParcelable("filterData", Parcelable.class.cast(filterData));
      } else if (Serializable.class.isAssignableFrom(DroppingDailyTaskBrowseFilter.class)) {
        __result.putSerializable("filterData", Serializable.class.cast(filterData));
      } else {
        throw new UnsupportedOperationException(DroppingDailyTaskBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("totalAmt")) {
      BigDecimal totalAmt = (BigDecimal) arguments.get("totalAmt");
      if (Parcelable.class.isAssignableFrom(BigDecimal.class) || totalAmt == null) {
        __result.putParcelable("totalAmt", Parcelable.class.cast(totalAmt));
      } else if (Serializable.class.isAssignableFrom(BigDecimal.class)) {
        __result.putSerializable("totalAmt", Serializable.class.cast(totalAmt));
      } else {
        throw new UnsupportedOperationException(BigDecimal.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    DroppingDailyTaskDetailFragmentArgs that = (DroppingDailyTaskDetailFragmentArgs) object;
    if (arguments.containsKey("filterData") != that.arguments.containsKey("filterData")) {
      return false;
    }
    if (getFilterData() != null ? !getFilterData().equals(that.getFilterData()) : that.getFilterData() != null) {
      return false;
    }
    if (arguments.containsKey("totalAmt") != that.arguments.containsKey("totalAmt")) {
      return false;
    }
    if (getTotalAmt() != null ? !getTotalAmt().equals(that.getTotalAmt()) : that.getTotalAmt() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getFilterData() != null ? getFilterData().hashCode() : 0);
    result = 31 * result + (getTotalAmt() != null ? getTotalAmt().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "DroppingDailyTaskDetailFragmentArgs{"
        + "filterData=" + getFilterData()
        + ", totalAmt=" + getTotalAmt()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(DroppingDailyTaskDetailFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull DroppingDailyTaskBrowseFilter filterData,
        @NonNull BigDecimal totalAmt) {
      if (filterData == null) {
        throw new IllegalArgumentException("Argument \"filterData\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("filterData", filterData);
      if (totalAmt == null) {
        throw new IllegalArgumentException("Argument \"totalAmt\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("totalAmt", totalAmt);
    }

    @NonNull
    public DroppingDailyTaskDetailFragmentArgs build() {
      DroppingDailyTaskDetailFragmentArgs result = new DroppingDailyTaskDetailFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setFilterData(@NonNull DroppingDailyTaskBrowseFilter filterData) {
      if (filterData == null) {
        throw new IllegalArgumentException("Argument \"filterData\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("filterData", filterData);
      return this;
    }

    @NonNull
    public Builder setTotalAmt(@NonNull BigDecimal totalAmt) {
      if (totalAmt == null) {
        throw new IllegalArgumentException("Argument \"totalAmt\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("totalAmt", totalAmt);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public DroppingDailyTaskBrowseFilter getFilterData() {
      return (DroppingDailyTaskBrowseFilter) arguments.get("filterData");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public BigDecimal getTotalAmt() {
      return (BigDecimal) arguments.get("totalAmt");
    }
  }
}
