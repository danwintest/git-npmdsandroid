package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class DroppingAdditionalDetailEditFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private DroppingAdditionalDetailEditFragmentArgs() {
  }

  private DroppingAdditionalDetailEditFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static DroppingAdditionalDetailEditFragmentArgs fromBundle(@NonNull Bundle bundle) {
    DroppingAdditionalDetailEditFragmentArgs __result = new DroppingAdditionalDetailEditFragmentArgs();
    bundle.setClassLoader(DroppingAdditionalDetailEditFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("droppingDetailData")) {
      DroppingDetailFormData droppingDetailData;
      if (Parcelable.class.isAssignableFrom(DroppingDetailFormData.class) || Serializable.class.isAssignableFrom(DroppingDetailFormData.class)) {
        droppingDetailData = (DroppingDetailFormData) bundle.get("droppingDetailData");
      } else {
        throw new UnsupportedOperationException(DroppingDetailFormData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("droppingDetailData", droppingDetailData);
    } else {
      __result.arguments.put("droppingDetailData", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public DroppingDetailFormData getDroppingDetailData() {
    return (DroppingDetailFormData) arguments.get("droppingDetailData");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("droppingDetailData")) {
      DroppingDetailFormData droppingDetailData = (DroppingDetailFormData) arguments.get("droppingDetailData");
      if (Parcelable.class.isAssignableFrom(DroppingDetailFormData.class) || droppingDetailData == null) {
        __result.putParcelable("droppingDetailData", Parcelable.class.cast(droppingDetailData));
      } else if (Serializable.class.isAssignableFrom(DroppingDetailFormData.class)) {
        __result.putSerializable("droppingDetailData", Serializable.class.cast(droppingDetailData));
      } else {
        throw new UnsupportedOperationException(DroppingDetailFormData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("droppingDetailData", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    DroppingAdditionalDetailEditFragmentArgs that = (DroppingAdditionalDetailEditFragmentArgs) object;
    if (arguments.containsKey("droppingDetailData") != that.arguments.containsKey("droppingDetailData")) {
      return false;
    }
    if (getDroppingDetailData() != null ? !getDroppingDetailData().equals(that.getDroppingDetailData()) : that.getDroppingDetailData() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getDroppingDetailData() != null ? getDroppingDetailData().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "DroppingAdditionalDetailEditFragmentArgs{"
        + "droppingDetailData=" + getDroppingDetailData()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(DroppingAdditionalDetailEditFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public DroppingAdditionalDetailEditFragmentArgs build() {
      DroppingAdditionalDetailEditFragmentArgs result = new DroppingAdditionalDetailEditFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setDroppingDetailData(@Nullable DroppingDetailFormData droppingDetailData) {
      this.arguments.put("droppingDetailData", droppingDetailData);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public DroppingDetailFormData getDroppingDetailData() {
      return (DroppingDetailFormData) arguments.get("droppingDetailData");
    }
  }
}
