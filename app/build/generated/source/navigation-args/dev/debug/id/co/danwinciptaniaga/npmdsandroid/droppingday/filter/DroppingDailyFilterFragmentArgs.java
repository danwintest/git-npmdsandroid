package id.co.danwinciptaniaga.npmdsandroid.droppingday.filter;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class DroppingDailyFilterFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private DroppingDailyFilterFragmentArgs() {
  }

  private DroppingDailyFilterFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static DroppingDailyFilterFragmentArgs fromBundle(@NonNull Bundle bundle) {
    DroppingDailyFilterFragmentArgs __result = new DroppingDailyFilterFragmentArgs();
    bundle.setClassLoader(DroppingDailyFilterFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("filterField")) {
      DroppingBrowseFilter filterField;
      if (Parcelable.class.isAssignableFrom(DroppingBrowseFilter.class) || Serializable.class.isAssignableFrom(DroppingBrowseFilter.class)) {
        filterField = (DroppingBrowseFilter) bundle.get("filterField");
      } else {
        throw new UnsupportedOperationException(DroppingBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("filterField", filterField);
    } else {
      __result.arguments.put("filterField", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public DroppingBrowseFilter getFilterField() {
    return (DroppingBrowseFilter) arguments.get("filterField");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("filterField")) {
      DroppingBrowseFilter filterField = (DroppingBrowseFilter) arguments.get("filterField");
      if (Parcelable.class.isAssignableFrom(DroppingBrowseFilter.class) || filterField == null) {
        __result.putParcelable("filterField", Parcelable.class.cast(filterField));
      } else if (Serializable.class.isAssignableFrom(DroppingBrowseFilter.class)) {
        __result.putSerializable("filterField", Serializable.class.cast(filterField));
      } else {
        throw new UnsupportedOperationException(DroppingBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("filterField", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    DroppingDailyFilterFragmentArgs that = (DroppingDailyFilterFragmentArgs) object;
    if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
      return false;
    }
    if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "DroppingDailyFilterFragmentArgs{"
        + "filterField=" + getFilterField()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(DroppingDailyFilterFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public DroppingDailyFilterFragmentArgs build() {
      DroppingDailyFilterFragmentArgs result = new DroppingDailyFilterFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setFilterField(@Nullable DroppingBrowseFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public DroppingBrowseFilter getFilterField() {
      return (DroppingBrowseFilter) arguments.get("filterField");
    }
  }
}
