package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class AppBookingTransferRekeningFormDirections {
  private AppBookingTransferRekeningFormDirections() {
  }

  @NonNull
  public static ActionPilihRekening actionPilihRekening(@NonNull UUID outletId,
      @NonNull String trxType) {
    return new ActionPilihRekening(outletId, trxType);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionPilihRekening implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionPilihRekening(@NonNull UUID outletId, @NonNull String trxType) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      if (trxType == null) {
        throw new IllegalArgumentException("Argument \"trxType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("trxType", trxType);
    }

    @NonNull
    public ActionPilihRekening setOutletId(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      return this;
    }

    @NonNull
    public ActionPilihRekening setTrxType(@NonNull String trxType) {
      if (trxType == null) {
        throw new IllegalArgumentException("Argument \"trxType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("trxType", trxType);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("outletId")) {
        UUID outletId = (UUID) arguments.get("outletId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || outletId == null) {
          __result.putParcelable("outletId", Parcelable.class.cast(outletId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("outletId", Serializable.class.cast(outletId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("trxType")) {
        String trxType = (String) arguments.get("trxType");
        __result.putString("trxType", trxType);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_pilih_rekening;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getOutletId() {
      return (UUID) arguments.get("outletId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getTrxType() {
      return (String) arguments.get("trxType");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionPilihRekening that = (ActionPilihRekening) object;
      if (arguments.containsKey("outletId") != that.arguments.containsKey("outletId")) {
        return false;
      }
      if (getOutletId() != null ? !getOutletId().equals(that.getOutletId()) : that.getOutletId() != null) {
        return false;
      }
      if (arguments.containsKey("trxType") != that.arguments.containsKey("trxType")) {
        return false;
      }
      if (getTrxType() != null ? !getTrxType().equals(that.getTrxType()) : that.getTrxType() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getOutletId() != null ? getOutletId().hashCode() : 0);
      result = 31 * result + (getTrxType() != null ? getTrxType().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionPilihRekening(actionId=" + getActionId() + "){"
          + "outletId=" + getOutletId()
          + ", trxType=" + getTrxType()
          + "}";
    }
  }
}
