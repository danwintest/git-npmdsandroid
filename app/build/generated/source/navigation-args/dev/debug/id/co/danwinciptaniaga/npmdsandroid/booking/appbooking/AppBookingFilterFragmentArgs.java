package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBrowseFilter;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class AppBookingFilterFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AppBookingFilterFragmentArgs() {
  }

  private AppBookingFilterFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AppBookingFilterFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AppBookingFilterFragmentArgs __result = new AppBookingFilterFragmentArgs();
    bundle.setClassLoader(AppBookingFilterFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("filterField")) {
      AppBookingBrowseFilter filterField;
      if (Parcelable.class.isAssignableFrom(AppBookingBrowseFilter.class) || Serializable.class.isAssignableFrom(AppBookingBrowseFilter.class)) {
        filterField = (AppBookingBrowseFilter) bundle.get("filterField");
      } else {
        throw new UnsupportedOperationException(AppBookingBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("filterField", filterField);
    } else {
      __result.arguments.put("filterField", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public AppBookingBrowseFilter getFilterField() {
    return (AppBookingBrowseFilter) arguments.get("filterField");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("filterField")) {
      AppBookingBrowseFilter filterField = (AppBookingBrowseFilter) arguments.get("filterField");
      if (Parcelable.class.isAssignableFrom(AppBookingBrowseFilter.class) || filterField == null) {
        __result.putParcelable("filterField", Parcelable.class.cast(filterField));
      } else if (Serializable.class.isAssignableFrom(AppBookingBrowseFilter.class)) {
        __result.putSerializable("filterField", Serializable.class.cast(filterField));
      } else {
        throw new UnsupportedOperationException(AppBookingBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("filterField", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AppBookingFilterFragmentArgs that = (AppBookingFilterFragmentArgs) object;
    if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
      return false;
    }
    if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AppBookingFilterFragmentArgs{"
        + "filterField=" + getFilterField()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AppBookingFilterFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public AppBookingFilterFragmentArgs build() {
      AppBookingFilterFragmentArgs result = new AppBookingFilterFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setFilterField(@Nullable AppBookingBrowseFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public AppBookingBrowseFilter getFilterField() {
      return (AppBookingBrowseFilter) arguments.get("filterField");
    }
  }
}
