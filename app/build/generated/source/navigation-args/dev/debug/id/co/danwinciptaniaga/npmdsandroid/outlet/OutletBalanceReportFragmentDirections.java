package id.co.danwinciptaniaga.npmdsandroid.outlet;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class OutletBalanceReportFragmentDirections {
  private OutletBalanceReportFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
