package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class TugasTransferCekSaldoFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private TugasTransferCekSaldoFragmentArgs() {
  }

  private TugasTransferCekSaldoFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static TugasTransferCekSaldoFragmentArgs fromBundle(@NonNull Bundle bundle) {
    TugasTransferCekSaldoFragmentArgs __result = new TugasTransferCekSaldoFragmentArgs();
    bundle.setClassLoader(TugasTransferCekSaldoFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("companyId")) {
      UUID companyId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        companyId = (UUID) bundle.get("companyId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (companyId == null) {
        throw new IllegalArgumentException("Argument \"companyId\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("companyId", companyId);
    } else {
      throw new IllegalArgumentException("Required argument \"companyId\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("companyName")) {
      String companyName;
      companyName = bundle.getString("companyName");
      if (companyName == null) {
        throw new IllegalArgumentException("Argument \"companyName\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("companyName", companyName);
    } else {
      throw new IllegalArgumentException("Required argument \"companyName\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("companyCode")) {
      String companyCode;
      companyCode = bundle.getString("companyCode");
      if (companyCode == null) {
        throw new IllegalArgumentException("Argument \"companyCode\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("companyCode", companyCode);
    } else {
      throw new IllegalArgumentException("Required argument \"companyCode\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public UUID getCompanyId() {
    return (UUID) arguments.get("companyId");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getCompanyName() {
    return (String) arguments.get("companyName");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getCompanyCode() {
    return (String) arguments.get("companyCode");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("companyId")) {
      UUID companyId = (UUID) arguments.get("companyId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || companyId == null) {
        __result.putParcelable("companyId", Parcelable.class.cast(companyId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("companyId", Serializable.class.cast(companyId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("companyName")) {
      String companyName = (String) arguments.get("companyName");
      __result.putString("companyName", companyName);
    }
    if (arguments.containsKey("companyCode")) {
      String companyCode = (String) arguments.get("companyCode");
      __result.putString("companyCode", companyCode);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    TugasTransferCekSaldoFragmentArgs that = (TugasTransferCekSaldoFragmentArgs) object;
    if (arguments.containsKey("companyId") != that.arguments.containsKey("companyId")) {
      return false;
    }
    if (getCompanyId() != null ? !getCompanyId().equals(that.getCompanyId()) : that.getCompanyId() != null) {
      return false;
    }
    if (arguments.containsKey("companyName") != that.arguments.containsKey("companyName")) {
      return false;
    }
    if (getCompanyName() != null ? !getCompanyName().equals(that.getCompanyName()) : that.getCompanyName() != null) {
      return false;
    }
    if (arguments.containsKey("companyCode") != that.arguments.containsKey("companyCode")) {
      return false;
    }
    if (getCompanyCode() != null ? !getCompanyCode().equals(that.getCompanyCode()) : that.getCompanyCode() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getCompanyId() != null ? getCompanyId().hashCode() : 0);
    result = 31 * result + (getCompanyName() != null ? getCompanyName().hashCode() : 0);
    result = 31 * result + (getCompanyCode() != null ? getCompanyCode().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "TugasTransferCekSaldoFragmentArgs{"
        + "companyId=" + getCompanyId()
        + ", companyName=" + getCompanyName()
        + ", companyCode=" + getCompanyCode()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(TugasTransferCekSaldoFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull UUID companyId, @NonNull String companyName,
        @NonNull String companyCode) {
      if (companyId == null) {
        throw new IllegalArgumentException("Argument \"companyId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyId", companyId);
      if (companyName == null) {
        throw new IllegalArgumentException("Argument \"companyName\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyName", companyName);
      if (companyCode == null) {
        throw new IllegalArgumentException("Argument \"companyCode\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyCode", companyCode);
    }

    @NonNull
    public TugasTransferCekSaldoFragmentArgs build() {
      TugasTransferCekSaldoFragmentArgs result = new TugasTransferCekSaldoFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setCompanyId(@NonNull UUID companyId) {
      if (companyId == null) {
        throw new IllegalArgumentException("Argument \"companyId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyId", companyId);
      return this;
    }

    @NonNull
    public Builder setCompanyName(@NonNull String companyName) {
      if (companyName == null) {
        throw new IllegalArgumentException("Argument \"companyName\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyName", companyName);
      return this;
    }

    @NonNull
    public Builder setCompanyCode(@NonNull String companyCode) {
      if (companyCode == null) {
        throw new IllegalArgumentException("Argument \"companyCode\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyCode", companyCode);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getCompanyId() {
      return (UUID) arguments.get("companyId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getCompanyName() {
      return (String) arguments.get("companyName");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getCompanyCode() {
      return (String) arguments.get("companyCode");
    }
  }
}
