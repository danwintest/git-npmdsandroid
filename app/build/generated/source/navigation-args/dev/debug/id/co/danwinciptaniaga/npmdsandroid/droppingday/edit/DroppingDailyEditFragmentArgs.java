package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class DroppingDailyEditFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private DroppingDailyEditFragmentArgs() {
  }

  private DroppingDailyEditFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static DroppingDailyEditFragmentArgs fromBundle(@NonNull Bundle bundle) {
    DroppingDailyEditFragmentArgs __result = new DroppingDailyEditFragmentArgs();
    bundle.setClassLoader(DroppingDailyEditFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("droppingId")) {
      UUID droppingId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        droppingId = (UUID) bundle.get("droppingId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("droppingId", droppingId);
    } else {
      __result.arguments.put("droppingId", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public UUID getDroppingId() {
    return (UUID) arguments.get("droppingId");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("droppingId")) {
      UUID droppingId = (UUID) arguments.get("droppingId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || droppingId == null) {
        __result.putParcelable("droppingId", Parcelable.class.cast(droppingId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("droppingId", Serializable.class.cast(droppingId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("droppingId", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    DroppingDailyEditFragmentArgs that = (DroppingDailyEditFragmentArgs) object;
    if (arguments.containsKey("droppingId") != that.arguments.containsKey("droppingId")) {
      return false;
    }
    if (getDroppingId() != null ? !getDroppingId().equals(that.getDroppingId()) : that.getDroppingId() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getDroppingId() != null ? getDroppingId().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "DroppingDailyEditFragmentArgs{"
        + "droppingId=" + getDroppingId()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(DroppingDailyEditFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public DroppingDailyEditFragmentArgs build() {
      DroppingDailyEditFragmentArgs result = new DroppingDailyEditFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setDroppingId(@Nullable UUID droppingId) {
      this.arguments.put("droppingId", droppingId);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getDroppingId() {
      return (UUID) arguments.get("droppingId");
    }
  }
}
