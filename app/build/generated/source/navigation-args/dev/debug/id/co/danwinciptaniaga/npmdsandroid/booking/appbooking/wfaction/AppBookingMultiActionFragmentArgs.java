package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class AppBookingMultiActionFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AppBookingMultiActionFragmentArgs() {
  }

  private AppBookingMultiActionFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AppBookingMultiActionFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AppBookingMultiActionFragmentArgs __result = new AppBookingMultiActionFragmentArgs();
    bundle.setClassLoader(AppBookingMultiActionFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("dataList")) {
      String dataList;
      dataList = bundle.getString("dataList");
      if (dataList == null) {
        throw new IllegalArgumentException("Argument \"dataList\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("dataList", dataList);
    } else {
      throw new IllegalArgumentException("Required argument \"dataList\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("decision")) {
      String decision;
      decision = bundle.getString("decision");
      if (decision == null) {
        throw new IllegalArgumentException("Argument \"decision\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("decision", decision);
    } else {
      throw new IllegalArgumentException("Required argument \"decision\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getDataList() {
    return (String) arguments.get("dataList");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getDecision() {
    return (String) arguments.get("decision");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("dataList")) {
      String dataList = (String) arguments.get("dataList");
      __result.putString("dataList", dataList);
    }
    if (arguments.containsKey("decision")) {
      String decision = (String) arguments.get("decision");
      __result.putString("decision", decision);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AppBookingMultiActionFragmentArgs that = (AppBookingMultiActionFragmentArgs) object;
    if (arguments.containsKey("dataList") != that.arguments.containsKey("dataList")) {
      return false;
    }
    if (getDataList() != null ? !getDataList().equals(that.getDataList()) : that.getDataList() != null) {
      return false;
    }
    if (arguments.containsKey("decision") != that.arguments.containsKey("decision")) {
      return false;
    }
    if (getDecision() != null ? !getDecision().equals(that.getDecision()) : that.getDecision() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getDataList() != null ? getDataList().hashCode() : 0);
    result = 31 * result + (getDecision() != null ? getDecision().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AppBookingMultiActionFragmentArgs{"
        + "dataList=" + getDataList()
        + ", decision=" + getDecision()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AppBookingMultiActionFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull String dataList, @NonNull String decision) {
      if (dataList == null) {
        throw new IllegalArgumentException("Argument \"dataList\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("dataList", dataList);
      if (decision == null) {
        throw new IllegalArgumentException("Argument \"decision\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("decision", decision);
    }

    @NonNull
    public AppBookingMultiActionFragmentArgs build() {
      AppBookingMultiActionFragmentArgs result = new AppBookingMultiActionFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setDataList(@NonNull String dataList) {
      if (dataList == null) {
        throw new IllegalArgumentException("Argument \"dataList\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("dataList", dataList);
      return this;
    }

    @NonNull
    public Builder setDecision(@NonNull String decision) {
      if (decision == null) {
        throw new IllegalArgumentException("Argument \"decision\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("decision", decision);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getDataList() {
      return (String) arguments.get("dataList");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getDecision() {
      return (String) arguments.get("decision");
    }
  }
}
