package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.math.BigDecimal;
import java.util.HashMap;

public class TugasTransferWfFragmentDirections {
  private TugasTransferWfFragmentDirections() {
  }

  @NonNull
  public static ActionHitungPenyesuaian actionHitungPenyesuaian(
      @NonNull BigDecimal penyesuaianPercentage) {
    return new ActionHitungPenyesuaian(penyesuaianPercentage);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionHitungPenyesuaian implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionHitungPenyesuaian(@NonNull BigDecimal penyesuaianPercentage) {
      if (penyesuaianPercentage == null) {
        throw new IllegalArgumentException("Argument \"penyesuaianPercentage\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("penyesuaianPercentage", penyesuaianPercentage);
    }

    @NonNull
    public ActionHitungPenyesuaian setPenyesuaianPercentage(
        @NonNull BigDecimal penyesuaianPercentage) {
      if (penyesuaianPercentage == null) {
        throw new IllegalArgumentException("Argument \"penyesuaianPercentage\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("penyesuaianPercentage", penyesuaianPercentage);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("penyesuaianPercentage")) {
        BigDecimal penyesuaianPercentage = (BigDecimal) arguments.get("penyesuaianPercentage");
        if (Parcelable.class.isAssignableFrom(BigDecimal.class) || penyesuaianPercentage == null) {
          __result.putParcelable("penyesuaianPercentage", Parcelable.class.cast(penyesuaianPercentage));
        } else if (Serializable.class.isAssignableFrom(BigDecimal.class)) {
          __result.putSerializable("penyesuaianPercentage", Serializable.class.cast(penyesuaianPercentage));
        } else {
          throw new UnsupportedOperationException(BigDecimal.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_hitung_penyesuaian;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public BigDecimal getPenyesuaianPercentage() {
      return (BigDecimal) arguments.get("penyesuaianPercentage");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionHitungPenyesuaian that = (ActionHitungPenyesuaian) object;
      if (arguments.containsKey("penyesuaianPercentage") != that.arguments.containsKey("penyesuaianPercentage")) {
        return false;
      }
      if (getPenyesuaianPercentage() != null ? !getPenyesuaianPercentage().equals(that.getPenyesuaianPercentage()) : that.getPenyesuaianPercentage() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getPenyesuaianPercentage() != null ? getPenyesuaianPercentage().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionHitungPenyesuaian(actionId=" + getActionId() + "){"
          + "penyesuaianPercentage=" + getPenyesuaianPercentage()
          + "}";
    }
  }
}
