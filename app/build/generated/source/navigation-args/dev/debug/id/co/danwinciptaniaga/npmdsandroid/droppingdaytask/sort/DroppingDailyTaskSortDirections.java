package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.sort;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class DroppingDailyTaskSortDirections {
  private DroppingDailyTaskSortDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
