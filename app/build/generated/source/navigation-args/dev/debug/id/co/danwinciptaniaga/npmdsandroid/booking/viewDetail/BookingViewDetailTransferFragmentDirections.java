package id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class BookingViewDetailTransferFragmentDirections {
  private BookingViewDetailTransferFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
