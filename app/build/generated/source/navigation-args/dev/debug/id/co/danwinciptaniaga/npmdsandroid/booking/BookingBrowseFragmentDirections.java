package id.co.danwinciptaniaga.npmdsandroid.booking;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class BookingBrowseFragmentDirections {
  private BookingBrowseFragmentDirections() {
  }

  @NonNull
  public static ActionBookingViewDetailFragment actionBookingViewDetailFragment(
      boolean isHasCreatePermission) {
    return new ActionBookingViewDetailFragment(isHasCreatePermission);
  }

  @NonNull
  public static ActionBookingViewDetailTransferFragment actionBookingViewDetailTransferFragment(
      boolean isHasCreatePermission) {
    return new ActionBookingViewDetailTransferFragment(isHasCreatePermission);
  }

  @NonNull
  public static ActionBookingPengajuanBatalCash actionBookingPengajuanBatalCash(
      @NonNull UUID bookingId, boolean isHasCreatePermission) {
    return new ActionBookingPengajuanBatalCash(bookingId, isHasCreatePermission);
  }

  @NonNull
  public static ActionBookingPengajuanBatalTransfer actionBookingPengajuanBatalTransfer(
      @NonNull UUID bookingId, boolean isHasCreatePermission) {
    return new ActionBookingPengajuanBatalTransfer(bookingId, isHasCreatePermission);
  }

  @NonNull
  public static ActionBookingPengajuanEditCash actionBookingPengajuanEditCash(
      @NonNull UUID bookingId, boolean isHasCreatePermission) {
    return new ActionBookingPengajuanEditCash(bookingId, isHasCreatePermission);
  }

  @NonNull
  public static ActionBookingPengajuanEditTransfer actionBookingPengajuanEditTransfer(
      @NonNull UUID bookingId, boolean isHasCreatePermission) {
    return new ActionBookingPengajuanEditTransfer(bookingId, isHasCreatePermission);
  }

  @NonNull
  public static ActionFilter actionFilter() {
    return new ActionFilter();
  }

  @NonNull
  public static ActionSort actionSort() {
    return new ActionSort();
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionBookingViewDetailFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionBookingViewDetailFragment(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public ActionBookingViewDetailFragment setBookingId(@Nullable UUID bookingId) {
      this.arguments.put("bookingId", bookingId);
      return this;
    }

    @NonNull
    public ActionBookingViewDetailFragment setIsHasCreatePermission(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("bookingId")) {
        UUID bookingId = (UUID) arguments.get("bookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || bookingId == null) {
          __result.putParcelable("bookingId", Parcelable.class.cast(bookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("bookingId", Serializable.class.cast(bookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("bookingId", null);
      }
      if (arguments.containsKey("isHasCreatePermission")) {
        boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
        __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_bookingViewDetailFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getBookingId() {
      return (UUID) arguments.get("bookingId");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionBookingViewDetailFragment that = (ActionBookingViewDetailFragment) object;
      if (arguments.containsKey("bookingId") != that.arguments.containsKey("bookingId")) {
        return false;
      }
      if (getBookingId() != null ? !getBookingId().equals(that.getBookingId()) : that.getBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
        return false;
      }
      if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getBookingId() != null ? getBookingId().hashCode() : 0);
      result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionBookingViewDetailFragment(actionId=" + getActionId() + "){"
          + "bookingId=" + getBookingId()
          + ", isHasCreatePermission=" + getIsHasCreatePermission()
          + "}";
    }
  }

  public static class ActionBookingViewDetailTransferFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionBookingViewDetailTransferFragment(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public ActionBookingViewDetailTransferFragment setBookingId(@Nullable UUID bookingId) {
      this.arguments.put("bookingId", bookingId);
      return this;
    }

    @NonNull
    public ActionBookingViewDetailTransferFragment setIsHasCreatePermission(
        boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("bookingId")) {
        UUID bookingId = (UUID) arguments.get("bookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || bookingId == null) {
          __result.putParcelable("bookingId", Parcelable.class.cast(bookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("bookingId", Serializable.class.cast(bookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("bookingId", null);
      }
      if (arguments.containsKey("isHasCreatePermission")) {
        boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
        __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_bookingViewDetailTransferFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getBookingId() {
      return (UUID) arguments.get("bookingId");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionBookingViewDetailTransferFragment that = (ActionBookingViewDetailTransferFragment) object;
      if (arguments.containsKey("bookingId") != that.arguments.containsKey("bookingId")) {
        return false;
      }
      if (getBookingId() != null ? !getBookingId().equals(that.getBookingId()) : that.getBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
        return false;
      }
      if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getBookingId() != null ? getBookingId().hashCode() : 0);
      result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionBookingViewDetailTransferFragment(actionId=" + getActionId() + "){"
          + "bookingId=" + getBookingId()
          + ", isHasCreatePermission=" + getIsHasCreatePermission()
          + "}";
    }
  }

  public static class ActionBookingPengajuanBatalCash implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionBookingPengajuanBatalCash(@NonNull UUID bookingId,
        boolean isHasCreatePermission) {
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingId", bookingId);
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public ActionBookingPengajuanBatalCash setBookingId(@NonNull UUID bookingId) {
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingId", bookingId);
      return this;
    }

    @NonNull
    public ActionBookingPengajuanBatalCash setIsHasCreatePermission(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("bookingId")) {
        UUID bookingId = (UUID) arguments.get("bookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || bookingId == null) {
          __result.putParcelable("bookingId", Parcelable.class.cast(bookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("bookingId", Serializable.class.cast(bookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("isHasCreatePermission")) {
        boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
        __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_booking_pengajuan_batal_cash;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getBookingId() {
      return (UUID) arguments.get("bookingId");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionBookingPengajuanBatalCash that = (ActionBookingPengajuanBatalCash) object;
      if (arguments.containsKey("bookingId") != that.arguments.containsKey("bookingId")) {
        return false;
      }
      if (getBookingId() != null ? !getBookingId().equals(that.getBookingId()) : that.getBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
        return false;
      }
      if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getBookingId() != null ? getBookingId().hashCode() : 0);
      result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionBookingPengajuanBatalCash(actionId=" + getActionId() + "){"
          + "bookingId=" + getBookingId()
          + ", isHasCreatePermission=" + getIsHasCreatePermission()
          + "}";
    }
  }

  public static class ActionBookingPengajuanBatalTransfer implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionBookingPengajuanBatalTransfer(@NonNull UUID bookingId,
        boolean isHasCreatePermission) {
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingId", bookingId);
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public ActionBookingPengajuanBatalTransfer setBookingId(@NonNull UUID bookingId) {
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingId", bookingId);
      return this;
    }

    @NonNull
    public ActionBookingPengajuanBatalTransfer setIsHasCreatePermission(
        boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("bookingId")) {
        UUID bookingId = (UUID) arguments.get("bookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || bookingId == null) {
          __result.putParcelable("bookingId", Parcelable.class.cast(bookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("bookingId", Serializable.class.cast(bookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("isHasCreatePermission")) {
        boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
        __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_booking_pengajuan_batal_transfer;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getBookingId() {
      return (UUID) arguments.get("bookingId");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionBookingPengajuanBatalTransfer that = (ActionBookingPengajuanBatalTransfer) object;
      if (arguments.containsKey("bookingId") != that.arguments.containsKey("bookingId")) {
        return false;
      }
      if (getBookingId() != null ? !getBookingId().equals(that.getBookingId()) : that.getBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
        return false;
      }
      if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getBookingId() != null ? getBookingId().hashCode() : 0);
      result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionBookingPengajuanBatalTransfer(actionId=" + getActionId() + "){"
          + "bookingId=" + getBookingId()
          + ", isHasCreatePermission=" + getIsHasCreatePermission()
          + "}";
    }
  }

  public static class ActionBookingPengajuanEditCash implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionBookingPengajuanEditCash(@NonNull UUID bookingId, boolean isHasCreatePermission) {
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingId", bookingId);
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public ActionBookingPengajuanEditCash setBookingId(@NonNull UUID bookingId) {
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingId", bookingId);
      return this;
    }

    @NonNull
    public ActionBookingPengajuanEditCash setIsHasCreatePermission(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("bookingId")) {
        UUID bookingId = (UUID) arguments.get("bookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || bookingId == null) {
          __result.putParcelable("bookingId", Parcelable.class.cast(bookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("bookingId", Serializable.class.cast(bookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("isHasCreatePermission")) {
        boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
        __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_booking_pengajuan_edit_cash;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getBookingId() {
      return (UUID) arguments.get("bookingId");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionBookingPengajuanEditCash that = (ActionBookingPengajuanEditCash) object;
      if (arguments.containsKey("bookingId") != that.arguments.containsKey("bookingId")) {
        return false;
      }
      if (getBookingId() != null ? !getBookingId().equals(that.getBookingId()) : that.getBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
        return false;
      }
      if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getBookingId() != null ? getBookingId().hashCode() : 0);
      result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionBookingPengajuanEditCash(actionId=" + getActionId() + "){"
          + "bookingId=" + getBookingId()
          + ", isHasCreatePermission=" + getIsHasCreatePermission()
          + "}";
    }
  }

  public static class ActionBookingPengajuanEditTransfer implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionBookingPengajuanEditTransfer(@NonNull UUID bookingId,
        boolean isHasCreatePermission) {
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingId", bookingId);
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public ActionBookingPengajuanEditTransfer setBookingId(@NonNull UUID bookingId) {
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingId", bookingId);
      return this;
    }

    @NonNull
    public ActionBookingPengajuanEditTransfer setIsHasCreatePermission(
        boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("bookingId")) {
        UUID bookingId = (UUID) arguments.get("bookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || bookingId == null) {
          __result.putParcelable("bookingId", Parcelable.class.cast(bookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("bookingId", Serializable.class.cast(bookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("isHasCreatePermission")) {
        boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
        __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_booking_pengajuan_edit_transfer;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getBookingId() {
      return (UUID) arguments.get("bookingId");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionBookingPengajuanEditTransfer that = (ActionBookingPengajuanEditTransfer) object;
      if (arguments.containsKey("bookingId") != that.arguments.containsKey("bookingId")) {
        return false;
      }
      if (getBookingId() != null ? !getBookingId().equals(that.getBookingId()) : that.getBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
        return false;
      }
      if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getBookingId() != null ? getBookingId().hashCode() : 0);
      result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionBookingPengajuanEditTransfer(actionId=" + getActionId() + "){"
          + "bookingId=" + getBookingId()
          + ", isHasCreatePermission=" + getIsHasCreatePermission()
          + "}";
    }
  }

  public static class ActionFilter implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionFilter() {
    }

    @NonNull
    public ActionFilter setFilterField(@Nullable BookingBrowseFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("filterField")) {
        BookingBrowseFilter filterField = (BookingBrowseFilter) arguments.get("filterField");
        if (Parcelable.class.isAssignableFrom(BookingBrowseFilter.class) || filterField == null) {
          __result.putParcelable("filterField", Parcelable.class.cast(filterField));
        } else if (Serializable.class.isAssignableFrom(BookingBrowseFilter.class)) {
          __result.putSerializable("filterField", Serializable.class.cast(filterField));
        } else {
          throw new UnsupportedOperationException(BookingBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("filterField", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_filter;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public BookingBrowseFilter getFilterField() {
      return (BookingBrowseFilter) arguments.get("filterField");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionFilter that = (ActionFilter) object;
      if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
        return false;
      }
      if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionFilter(actionId=" + getActionId() + "){"
          + "filterField=" + getFilterField()
          + "}";
    }
  }

  public static class ActionSort implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionSort() {
    }

    @NonNull
    public ActionSort setSortField(@Nullable String sortField) {
      this.arguments.put("sortField", sortField);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("sortField")) {
        String sortField = (String) arguments.get("sortField");
        __result.putString("sortField", sortField);
      } else {
        __result.putString("sortField", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_sort;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getSortField() {
      return (String) arguments.get("sortField");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionSort that = (ActionSort) object;
      if (arguments.containsKey("sortField") != that.arguments.containsKey("sortField")) {
        return false;
      }
      if (getSortField() != null ? !getSortField().equals(that.getSortField()) : that.getSortField() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getSortField() != null ? getSortField().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionSort(actionId=" + getActionId() + "){"
          + "sortField=" + getSortField()
          + "}";
    }
  }
}
