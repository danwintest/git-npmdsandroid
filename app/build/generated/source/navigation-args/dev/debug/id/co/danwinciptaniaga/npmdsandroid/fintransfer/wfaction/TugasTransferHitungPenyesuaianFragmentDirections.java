package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class TugasTransferHitungPenyesuaianFragmentDirections {
  private TugasTransferHitungPenyesuaianFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
