package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class OutletStockByDayReportFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private OutletStockByDayReportFragmentArgs() {
  }

  private OutletStockByDayReportFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static OutletStockByDayReportFragmentArgs fromBundle(@NonNull Bundle bundle) {
    OutletStockByDayReportFragmentArgs __result = new OutletStockByDayReportFragmentArgs();
    bundle.setClassLoader(OutletStockByDayReportFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("outletId")) {
      UUID outletId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        outletId = (UUID) bundle.get("outletId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("outletId", outletId);
    } else {
      throw new IllegalArgumentException("Required argument \"outletId\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public UUID getOutletId() {
    return (UUID) arguments.get("outletId");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("outletId")) {
      UUID outletId = (UUID) arguments.get("outletId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || outletId == null) {
        __result.putParcelable("outletId", Parcelable.class.cast(outletId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("outletId", Serializable.class.cast(outletId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    OutletStockByDayReportFragmentArgs that = (OutletStockByDayReportFragmentArgs) object;
    if (arguments.containsKey("outletId") != that.arguments.containsKey("outletId")) {
      return false;
    }
    if (getOutletId() != null ? !getOutletId().equals(that.getOutletId()) : that.getOutletId() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getOutletId() != null ? getOutletId().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "OutletStockByDayReportFragmentArgs{"
        + "outletId=" + getOutletId()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(OutletStockByDayReportFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
    }

    @NonNull
    public OutletStockByDayReportFragmentArgs build() {
      OutletStockByDayReportFragmentArgs result = new OutletStockByDayReportFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setOutletId(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getOutletId() {
      return (UUID) arguments.get("outletId");
    }
  }
}
