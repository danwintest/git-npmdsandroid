package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class DroppingAdditionalDetailEditFragmentDirections {
  private DroppingAdditionalDetailEditFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
