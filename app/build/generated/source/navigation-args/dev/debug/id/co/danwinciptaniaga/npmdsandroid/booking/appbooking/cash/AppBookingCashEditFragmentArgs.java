package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class AppBookingCashEditFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AppBookingCashEditFragmentArgs() {
  }

  private AppBookingCashEditFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AppBookingCashEditFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AppBookingCashEditFragmentArgs __result = new AppBookingCashEditFragmentArgs();
    bundle.setClassLoader(AppBookingCashEditFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("bookingId")) {
      UUID bookingId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        bookingId = (UUID) bundle.get("bookingId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("bookingId", bookingId);
    } else {
      throw new IllegalArgumentException("Required argument \"bookingId\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("isHasCreatePermission")) {
      boolean isHasCreatePermission;
      isHasCreatePermission = bundle.getBoolean("isHasCreatePermission");
      __result.arguments.put("isHasCreatePermission", isHasCreatePermission);
    } else {
      throw new IllegalArgumentException("Required argument \"isHasCreatePermission\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public UUID getBookingId() {
    return (UUID) arguments.get("bookingId");
  }

  @SuppressWarnings("unchecked")
  public boolean getIsHasCreatePermission() {
    return (boolean) arguments.get("isHasCreatePermission");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("bookingId")) {
      UUID bookingId = (UUID) arguments.get("bookingId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || bookingId == null) {
        __result.putParcelable("bookingId", Parcelable.class.cast(bookingId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("bookingId", Serializable.class.cast(bookingId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("isHasCreatePermission")) {
      boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
      __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AppBookingCashEditFragmentArgs that = (AppBookingCashEditFragmentArgs) object;
    if (arguments.containsKey("bookingId") != that.arguments.containsKey("bookingId")) {
      return false;
    }
    if (getBookingId() != null ? !getBookingId().equals(that.getBookingId()) : that.getBookingId() != null) {
      return false;
    }
    if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
      return false;
    }
    if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getBookingId() != null ? getBookingId().hashCode() : 0);
    result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AppBookingCashEditFragmentArgs{"
        + "bookingId=" + getBookingId()
        + ", isHasCreatePermission=" + getIsHasCreatePermission()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AppBookingCashEditFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull UUID bookingId, boolean isHasCreatePermission) {
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingId", bookingId);
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public AppBookingCashEditFragmentArgs build() {
      AppBookingCashEditFragmentArgs result = new AppBookingCashEditFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setBookingId(@NonNull UUID bookingId) {
      if (bookingId == null) {
        throw new IllegalArgumentException("Argument \"bookingId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingId", bookingId);
      return this;
    }

    @NonNull
    public Builder setIsHasCreatePermission(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getBookingId() {
      return (UUID) arguments.get("bookingId");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }
  }
}
