package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class DroppingAdditionalBrowseFragmentDirections {
  private DroppingAdditionalBrowseFragmentDirections() {
  }

  @NonNull
  public static ActionDroppingAdditionalEditFragment actionDroppingAdditionalEditFragment() {
    return new ActionDroppingAdditionalEditFragment();
  }

  @NonNull
  public static ActionFilterFragment actionFilterFragment() {
    return new ActionFilterFragment();
  }

  @NonNull
  public static ActionSortFragment actionSortFragment() {
    return new ActionSortFragment();
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionDroppingAdditionalEditFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionDroppingAdditionalEditFragment() {
    }

    @NonNull
    public ActionDroppingAdditionalEditFragment setDroppingId(@Nullable UUID droppingId) {
      this.arguments.put("droppingId", droppingId);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("droppingId")) {
        UUID droppingId = (UUID) arguments.get("droppingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || droppingId == null) {
          __result.putParcelable("droppingId", Parcelable.class.cast(droppingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("droppingId", Serializable.class.cast(droppingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("droppingId", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_droppingAdditionalEditFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getDroppingId() {
      return (UUID) arguments.get("droppingId");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionDroppingAdditionalEditFragment that = (ActionDroppingAdditionalEditFragment) object;
      if (arguments.containsKey("droppingId") != that.arguments.containsKey("droppingId")) {
        return false;
      }
      if (getDroppingId() != null ? !getDroppingId().equals(that.getDroppingId()) : that.getDroppingId() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getDroppingId() != null ? getDroppingId().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionDroppingAdditionalEditFragment(actionId=" + getActionId() + "){"
          + "droppingId=" + getDroppingId()
          + "}";
    }
  }

  public static class ActionFilterFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionFilterFragment() {
    }

    @NonNull
    public ActionFilterFragment setFilterField(@Nullable DroppingBrowseFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("filterField")) {
        DroppingBrowseFilter filterField = (DroppingBrowseFilter) arguments.get("filterField");
        if (Parcelable.class.isAssignableFrom(DroppingBrowseFilter.class) || filterField == null) {
          __result.putParcelable("filterField", Parcelable.class.cast(filterField));
        } else if (Serializable.class.isAssignableFrom(DroppingBrowseFilter.class)) {
          __result.putSerializable("filterField", Serializable.class.cast(filterField));
        } else {
          throw new UnsupportedOperationException(DroppingBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("filterField", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_filterFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public DroppingBrowseFilter getFilterField() {
      return (DroppingBrowseFilter) arguments.get("filterField");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionFilterFragment that = (ActionFilterFragment) object;
      if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
        return false;
      }
      if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionFilterFragment(actionId=" + getActionId() + "){"
          + "filterField=" + getFilterField()
          + "}";
    }
  }

  public static class ActionSortFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionSortFragment() {
    }

    @NonNull
    public ActionSortFragment setSortField(@Nullable String sortField) {
      this.arguments.put("sortField", sortField);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("sortField")) {
        String sortField = (String) arguments.get("sortField");
        __result.putString("sortField", sortField);
      } else {
        __result.putString("sortField", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_sortFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getSortField() {
      return (String) arguments.get("sortField");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionSortFragment that = (ActionSortFragment) object;
      if (arguments.containsKey("sortField") != that.arguments.containsKey("sortField")) {
        return false;
      }
      if (getSortField() != null ? !getSortField().equals(that.getSortField()) : that.getSortField() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getSortField() != null ? getSortField().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionSortFragment(actionId=" + getActionId() + "){"
          + "sortField=" + getSortField()
          + "}";
    }
  }
}
