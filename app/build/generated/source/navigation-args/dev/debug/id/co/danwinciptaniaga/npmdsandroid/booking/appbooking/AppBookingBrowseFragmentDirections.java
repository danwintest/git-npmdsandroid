package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class AppBookingBrowseFragmentDirections {
  private AppBookingBrowseFragmentDirections() {
  }

  @NonNull
  public static ActionEditCashFragment actionEditCashFragment(@NonNull String appOperation,
      @NonNull String bookingType, boolean isHasCreatePermission) {
    return new ActionEditCashFragment(appOperation, bookingType, isHasCreatePermission);
  }

  @NonNull
  public static ActionEditTransferFragment actionEditTransferFragment(@NonNull String appOperation,
      @NonNull String bookingType, @Nullable String txnMode, boolean isHasCreatePermission) {
    return new ActionEditTransferFragment(appOperation, bookingType, txnMode, isHasCreatePermission);
  }

  @NonNull
  public static ActionFilter actionFilter() {
    return new ActionFilter();
  }

  @NonNull
  public static ActionSort actionSort() {
    return new ActionSort();
  }

  @NonNull
  public static ActionWf actionWf(@NonNull String dataList, @NonNull String decision) {
    return new ActionWf(dataList, decision);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionEditCashFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionEditCashFragment(@NonNull String appOperation, @NonNull String bookingType,
        boolean isHasCreatePermission) {
      if (appOperation == null) {
        throw new IllegalArgumentException("Argument \"appOperation\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("appOperation", appOperation);
      if (bookingType == null) {
        throw new IllegalArgumentException("Argument \"bookingType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingType", bookingType);
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public ActionEditCashFragment setAppPBookingId(@Nullable UUID appPBookingId) {
      this.arguments.put("appPBookingId", appPBookingId);
      return this;
    }

    @NonNull
    public ActionEditCashFragment setAppOperation(@NonNull String appOperation) {
      if (appOperation == null) {
        throw new IllegalArgumentException("Argument \"appOperation\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("appOperation", appOperation);
      return this;
    }

    @NonNull
    public ActionEditCashFragment setBookingType(@NonNull String bookingType) {
      if (bookingType == null) {
        throw new IllegalArgumentException("Argument \"bookingType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingType", bookingType);
      return this;
    }

    @NonNull
    public ActionEditCashFragment setIsHasCreatePermission(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("appPBookingId")) {
        UUID appPBookingId = (UUID) arguments.get("appPBookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || appPBookingId == null) {
          __result.putParcelable("appPBookingId", Parcelable.class.cast(appPBookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("appPBookingId", Serializable.class.cast(appPBookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("appPBookingId", null);
      }
      if (arguments.containsKey("appOperation")) {
        String appOperation = (String) arguments.get("appOperation");
        __result.putString("appOperation", appOperation);
      }
      if (arguments.containsKey("bookingType")) {
        String bookingType = (String) arguments.get("bookingType");
        __result.putString("bookingType", bookingType);
      }
      if (arguments.containsKey("isHasCreatePermission")) {
        boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
        __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_EditCashFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getAppPBookingId() {
      return (UUID) arguments.get("appPBookingId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getAppOperation() {
      return (String) arguments.get("appOperation");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getBookingType() {
      return (String) arguments.get("bookingType");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionEditCashFragment that = (ActionEditCashFragment) object;
      if (arguments.containsKey("appPBookingId") != that.arguments.containsKey("appPBookingId")) {
        return false;
      }
      if (getAppPBookingId() != null ? !getAppPBookingId().equals(that.getAppPBookingId()) : that.getAppPBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("appOperation") != that.arguments.containsKey("appOperation")) {
        return false;
      }
      if (getAppOperation() != null ? !getAppOperation().equals(that.getAppOperation()) : that.getAppOperation() != null) {
        return false;
      }
      if (arguments.containsKey("bookingType") != that.arguments.containsKey("bookingType")) {
        return false;
      }
      if (getBookingType() != null ? !getBookingType().equals(that.getBookingType()) : that.getBookingType() != null) {
        return false;
      }
      if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
        return false;
      }
      if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getAppPBookingId() != null ? getAppPBookingId().hashCode() : 0);
      result = 31 * result + (getAppOperation() != null ? getAppOperation().hashCode() : 0);
      result = 31 * result + (getBookingType() != null ? getBookingType().hashCode() : 0);
      result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionEditCashFragment(actionId=" + getActionId() + "){"
          + "appPBookingId=" + getAppPBookingId()
          + ", appOperation=" + getAppOperation()
          + ", bookingType=" + getBookingType()
          + ", isHasCreatePermission=" + getIsHasCreatePermission()
          + "}";
    }
  }

  public static class ActionEditTransferFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionEditTransferFragment(@NonNull String appOperation, @NonNull String bookingType,
        @Nullable String txnMode, boolean isHasCreatePermission) {
      if (appOperation == null) {
        throw new IllegalArgumentException("Argument \"appOperation\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("appOperation", appOperation);
      if (bookingType == null) {
        throw new IllegalArgumentException("Argument \"bookingType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingType", bookingType);
      this.arguments.put("txnMode", txnMode);
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public ActionEditTransferFragment setAppPBookingId(@Nullable UUID appPBookingId) {
      this.arguments.put("appPBookingId", appPBookingId);
      return this;
    }

    @NonNull
    public ActionEditTransferFragment setAppOperation(@NonNull String appOperation) {
      if (appOperation == null) {
        throw new IllegalArgumentException("Argument \"appOperation\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("appOperation", appOperation);
      return this;
    }

    @NonNull
    public ActionEditTransferFragment setBookingType(@NonNull String bookingType) {
      if (bookingType == null) {
        throw new IllegalArgumentException("Argument \"bookingType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingType", bookingType);
      return this;
    }

    @NonNull
    public ActionEditTransferFragment setTxnMode(@Nullable String txnMode) {
      this.arguments.put("txnMode", txnMode);
      return this;
    }

    @NonNull
    public ActionEditTransferFragment setIsHasCreatePermission(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("appPBookingId")) {
        UUID appPBookingId = (UUID) arguments.get("appPBookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || appPBookingId == null) {
          __result.putParcelable("appPBookingId", Parcelable.class.cast(appPBookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("appPBookingId", Serializable.class.cast(appPBookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("appPBookingId", null);
      }
      if (arguments.containsKey("appOperation")) {
        String appOperation = (String) arguments.get("appOperation");
        __result.putString("appOperation", appOperation);
      }
      if (arguments.containsKey("bookingType")) {
        String bookingType = (String) arguments.get("bookingType");
        __result.putString("bookingType", bookingType);
      }
      if (arguments.containsKey("txnMode")) {
        String txnMode = (String) arguments.get("txnMode");
        __result.putString("txnMode", txnMode);
      }
      if (arguments.containsKey("isHasCreatePermission")) {
        boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
        __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_EditTransferFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getAppPBookingId() {
      return (UUID) arguments.get("appPBookingId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getAppOperation() {
      return (String) arguments.get("appOperation");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getBookingType() {
      return (String) arguments.get("bookingType");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTxnMode() {
      return (String) arguments.get("txnMode");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionEditTransferFragment that = (ActionEditTransferFragment) object;
      if (arguments.containsKey("appPBookingId") != that.arguments.containsKey("appPBookingId")) {
        return false;
      }
      if (getAppPBookingId() != null ? !getAppPBookingId().equals(that.getAppPBookingId()) : that.getAppPBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("appOperation") != that.arguments.containsKey("appOperation")) {
        return false;
      }
      if (getAppOperation() != null ? !getAppOperation().equals(that.getAppOperation()) : that.getAppOperation() != null) {
        return false;
      }
      if (arguments.containsKey("bookingType") != that.arguments.containsKey("bookingType")) {
        return false;
      }
      if (getBookingType() != null ? !getBookingType().equals(that.getBookingType()) : that.getBookingType() != null) {
        return false;
      }
      if (arguments.containsKey("txnMode") != that.arguments.containsKey("txnMode")) {
        return false;
      }
      if (getTxnMode() != null ? !getTxnMode().equals(that.getTxnMode()) : that.getTxnMode() != null) {
        return false;
      }
      if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
        return false;
      }
      if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getAppPBookingId() != null ? getAppPBookingId().hashCode() : 0);
      result = 31 * result + (getAppOperation() != null ? getAppOperation().hashCode() : 0);
      result = 31 * result + (getBookingType() != null ? getBookingType().hashCode() : 0);
      result = 31 * result + (getTxnMode() != null ? getTxnMode().hashCode() : 0);
      result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionEditTransferFragment(actionId=" + getActionId() + "){"
          + "appPBookingId=" + getAppPBookingId()
          + ", appOperation=" + getAppOperation()
          + ", bookingType=" + getBookingType()
          + ", txnMode=" + getTxnMode()
          + ", isHasCreatePermission=" + getIsHasCreatePermission()
          + "}";
    }
  }

  public static class ActionFilter implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionFilter() {
    }

    @NonNull
    public ActionFilter setFilterField(@Nullable AppBookingBrowseFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("filterField")) {
        AppBookingBrowseFilter filterField = (AppBookingBrowseFilter) arguments.get("filterField");
        if (Parcelable.class.isAssignableFrom(AppBookingBrowseFilter.class) || filterField == null) {
          __result.putParcelable("filterField", Parcelable.class.cast(filterField));
        } else if (Serializable.class.isAssignableFrom(AppBookingBrowseFilter.class)) {
          __result.putSerializable("filterField", Serializable.class.cast(filterField));
        } else {
          throw new UnsupportedOperationException(AppBookingBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("filterField", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_filter;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public AppBookingBrowseFilter getFilterField() {
      return (AppBookingBrowseFilter) arguments.get("filterField");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionFilter that = (ActionFilter) object;
      if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
        return false;
      }
      if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionFilter(actionId=" + getActionId() + "){"
          + "filterField=" + getFilterField()
          + "}";
    }
  }

  public static class ActionSort implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionSort() {
    }

    @NonNull
    public ActionSort setSortField(@Nullable String sortField) {
      this.arguments.put("sortField", sortField);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("sortField")) {
        String sortField = (String) arguments.get("sortField");
        __result.putString("sortField", sortField);
      } else {
        __result.putString("sortField", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_sort;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getSortField() {
      return (String) arguments.get("sortField");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionSort that = (ActionSort) object;
      if (arguments.containsKey("sortField") != that.arguments.containsKey("sortField")) {
        return false;
      }
      if (getSortField() != null ? !getSortField().equals(that.getSortField()) : that.getSortField() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getSortField() != null ? getSortField().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionSort(actionId=" + getActionId() + "){"
          + "sortField=" + getSortField()
          + "}";
    }
  }

  public static class ActionWf implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionWf(@NonNull String dataList, @NonNull String decision) {
      if (dataList == null) {
        throw new IllegalArgumentException("Argument \"dataList\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("dataList", dataList);
      if (decision == null) {
        throw new IllegalArgumentException("Argument \"decision\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("decision", decision);
    }

    @NonNull
    public ActionWf setDataList(@NonNull String dataList) {
      if (dataList == null) {
        throw new IllegalArgumentException("Argument \"dataList\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("dataList", dataList);
      return this;
    }

    @NonNull
    public ActionWf setDecision(@NonNull String decision) {
      if (decision == null) {
        throw new IllegalArgumentException("Argument \"decision\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("decision", decision);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("dataList")) {
        String dataList = (String) arguments.get("dataList");
        __result.putString("dataList", dataList);
      }
      if (arguments.containsKey("decision")) {
        String decision = (String) arguments.get("decision");
        __result.putString("decision", decision);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_wf;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getDataList() {
      return (String) arguments.get("dataList");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getDecision() {
      return (String) arguments.get("decision");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionWf that = (ActionWf) object;
      if (arguments.containsKey("dataList") != that.arguments.containsKey("dataList")) {
        return false;
      }
      if (getDataList() != null ? !getDataList().equals(that.getDataList()) : that.getDataList() != null) {
        return false;
      }
      if (arguments.containsKey("decision") != that.arguments.containsKey("decision")) {
        return false;
      }
      if (getDecision() != null ? !getDecision().equals(that.getDecision()) : that.getDecision() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getDataList() != null ? getDataList().hashCode() : 0);
      result = 31 * result + (getDecision() != null ? getDecision().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionWf(actionId=" + getActionId() + "){"
          + "dataList=" + getDataList()
          + ", decision=" + getDecision()
          + "}";
    }
  }
}
