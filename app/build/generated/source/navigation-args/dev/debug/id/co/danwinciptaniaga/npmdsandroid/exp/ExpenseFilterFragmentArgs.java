package id.co.danwinciptaniaga.npmdsandroid.exp;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseFilter;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class ExpenseFilterFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private ExpenseFilterFragmentArgs() {
  }

  private ExpenseFilterFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static ExpenseFilterFragmentArgs fromBundle(@NonNull Bundle bundle) {
    ExpenseFilterFragmentArgs __result = new ExpenseFilterFragmentArgs();
    bundle.setClassLoader(ExpenseFilterFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("filterField")) {
      ExpenseBrowseFilter filterField;
      if (Parcelable.class.isAssignableFrom(ExpenseBrowseFilter.class) || Serializable.class.isAssignableFrom(ExpenseBrowseFilter.class)) {
        filterField = (ExpenseBrowseFilter) bundle.get("filterField");
      } else {
        throw new UnsupportedOperationException(ExpenseBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("filterField", filterField);
    } else {
      __result.arguments.put("filterField", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public ExpenseBrowseFilter getFilterField() {
    return (ExpenseBrowseFilter) arguments.get("filterField");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("filterField")) {
      ExpenseBrowseFilter filterField = (ExpenseBrowseFilter) arguments.get("filterField");
      if (Parcelable.class.isAssignableFrom(ExpenseBrowseFilter.class) || filterField == null) {
        __result.putParcelable("filterField", Parcelable.class.cast(filterField));
      } else if (Serializable.class.isAssignableFrom(ExpenseBrowseFilter.class)) {
        __result.putSerializable("filterField", Serializable.class.cast(filterField));
      } else {
        throw new UnsupportedOperationException(ExpenseBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("filterField", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    ExpenseFilterFragmentArgs that = (ExpenseFilterFragmentArgs) object;
    if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
      return false;
    }
    if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "ExpenseFilterFragmentArgs{"
        + "filterField=" + getFilterField()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(ExpenseFilterFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public ExpenseFilterFragmentArgs build() {
      ExpenseFilterFragmentArgs result = new ExpenseFilterFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setFilterField(@Nullable ExpenseBrowseFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ExpenseBrowseFilter getFilterField() {
      return (ExpenseBrowseFilter) arguments.get("filterField");
    }
  }
}
