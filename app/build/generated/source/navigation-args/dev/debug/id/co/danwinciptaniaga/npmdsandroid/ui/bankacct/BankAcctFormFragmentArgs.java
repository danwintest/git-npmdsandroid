package id.co.danwinciptaniaga.npmdsandroid.ui.bankacct;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class BankAcctFormFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private BankAcctFormFragmentArgs() {
  }

  private BankAcctFormFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static BankAcctFormFragmentArgs fromBundle(@NonNull Bundle bundle) {
    BankAcctFormFragmentArgs __result = new BankAcctFormFragmentArgs();
    bundle.setClassLoader(BankAcctFormFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("bankAccountData")) {
      BankAccountData bankAccountData;
      if (Parcelable.class.isAssignableFrom(BankAccountData.class) || Serializable.class.isAssignableFrom(BankAccountData.class)) {
        bankAccountData = (BankAccountData) bundle.get("bankAccountData");
      } else {
        throw new UnsupportedOperationException(BankAccountData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("bankAccountData", bankAccountData);
    } else {
      __result.arguments.put("bankAccountData", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public BankAccountData getBankAccountData() {
    return (BankAccountData) arguments.get("bankAccountData");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("bankAccountData")) {
      BankAccountData bankAccountData = (BankAccountData) arguments.get("bankAccountData");
      if (Parcelable.class.isAssignableFrom(BankAccountData.class) || bankAccountData == null) {
        __result.putParcelable("bankAccountData", Parcelable.class.cast(bankAccountData));
      } else if (Serializable.class.isAssignableFrom(BankAccountData.class)) {
        __result.putSerializable("bankAccountData", Serializable.class.cast(bankAccountData));
      } else {
        throw new UnsupportedOperationException(BankAccountData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("bankAccountData", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    BankAcctFormFragmentArgs that = (BankAcctFormFragmentArgs) object;
    if (arguments.containsKey("bankAccountData") != that.arguments.containsKey("bankAccountData")) {
      return false;
    }
    if (getBankAccountData() != null ? !getBankAccountData().equals(that.getBankAccountData()) : that.getBankAccountData() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getBankAccountData() != null ? getBankAccountData().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "BankAcctFormFragmentArgs{"
        + "bankAccountData=" + getBankAccountData()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(BankAcctFormFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public BankAcctFormFragmentArgs build() {
      BankAcctFormFragmentArgs result = new BankAcctFormFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setBankAccountData(@Nullable BankAccountData bankAccountData) {
      this.arguments.put("bankAccountData", bankAccountData);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public BankAccountData getBankAccountData() {
      return (BankAccountData) arguments.get("bankAccountData");
    }
  }
}
