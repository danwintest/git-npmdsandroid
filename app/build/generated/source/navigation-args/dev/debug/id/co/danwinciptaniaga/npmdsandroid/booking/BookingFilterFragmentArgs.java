package id.co.danwinciptaniaga.npmdsandroid.booking;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseFilter;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class BookingFilterFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private BookingFilterFragmentArgs() {
  }

  private BookingFilterFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static BookingFilterFragmentArgs fromBundle(@NonNull Bundle bundle) {
    BookingFilterFragmentArgs __result = new BookingFilterFragmentArgs();
    bundle.setClassLoader(BookingFilterFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("filterField")) {
      BookingBrowseFilter filterField;
      if (Parcelable.class.isAssignableFrom(BookingBrowseFilter.class) || Serializable.class.isAssignableFrom(BookingBrowseFilter.class)) {
        filterField = (BookingBrowseFilter) bundle.get("filterField");
      } else {
        throw new UnsupportedOperationException(BookingBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("filterField", filterField);
    } else {
      __result.arguments.put("filterField", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public BookingBrowseFilter getFilterField() {
    return (BookingBrowseFilter) arguments.get("filterField");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("filterField")) {
      BookingBrowseFilter filterField = (BookingBrowseFilter) arguments.get("filterField");
      if (Parcelable.class.isAssignableFrom(BookingBrowseFilter.class) || filterField == null) {
        __result.putParcelable("filterField", Parcelable.class.cast(filterField));
      } else if (Serializable.class.isAssignableFrom(BookingBrowseFilter.class)) {
        __result.putSerializable("filterField", Serializable.class.cast(filterField));
      } else {
        throw new UnsupportedOperationException(BookingBrowseFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("filterField", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    BookingFilterFragmentArgs that = (BookingFilterFragmentArgs) object;
    if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
      return false;
    }
    if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "BookingFilterFragmentArgs{"
        + "filterField=" + getFilterField()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(BookingFilterFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public BookingFilterFragmentArgs build() {
      BookingFilterFragmentArgs result = new BookingFilterFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setFilterField(@Nullable BookingBrowseFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public BookingBrowseFilter getFilterField() {
      return (BookingBrowseFilter) arguments.get("filterField");
    }
  }
}
