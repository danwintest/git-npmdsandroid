package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalData;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class DroppingAdditionalEditFragmentDirections {
  private DroppingAdditionalEditFragmentDirections() {
  }

  @NonNull
  public static ActionEditAltAccount actionEditAltAccount() {
    return new ActionEditAltAccount();
  }

  @NonNull
  public static ActionDroppingAdditionalDetailEditFragment actionDroppingAdditionalDetailEditFragment(
      ) {
    return new ActionDroppingAdditionalDetailEditFragment();
  }

  @NonNull
  public static BackToBrowse backToBrowse() {
    return new BackToBrowse();
  }

  @NonNull
  public static ActionWfHistory actionWfHistory(@NonNull WfType wfType, @NonNull UUID entityId) {
    return new ActionWfHistory(wfType, entityId);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionEditAltAccount implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionEditAltAccount() {
    }

    @NonNull
    public ActionEditAltAccount setBankAccountData(@Nullable BankAccountData bankAccountData) {
      this.arguments.put("bankAccountData", bankAccountData);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("bankAccountData")) {
        BankAccountData bankAccountData = (BankAccountData) arguments.get("bankAccountData");
        if (Parcelable.class.isAssignableFrom(BankAccountData.class) || bankAccountData == null) {
          __result.putParcelable("bankAccountData", Parcelable.class.cast(bankAccountData));
        } else if (Serializable.class.isAssignableFrom(BankAccountData.class)) {
          __result.putSerializable("bankAccountData", Serializable.class.cast(bankAccountData));
        } else {
          throw new UnsupportedOperationException(BankAccountData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("bankAccountData", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_edit_alt_account;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public BankAccountData getBankAccountData() {
      return (BankAccountData) arguments.get("bankAccountData");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionEditAltAccount that = (ActionEditAltAccount) object;
      if (arguments.containsKey("bankAccountData") != that.arguments.containsKey("bankAccountData")) {
        return false;
      }
      if (getBankAccountData() != null ? !getBankAccountData().equals(that.getBankAccountData()) : that.getBankAccountData() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getBankAccountData() != null ? getBankAccountData().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionEditAltAccount(actionId=" + getActionId() + "){"
          + "bankAccountData=" + getBankAccountData()
          + "}";
    }
  }

  public static class ActionDroppingAdditionalDetailEditFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionDroppingAdditionalDetailEditFragment() {
    }

    @NonNull
    public ActionDroppingAdditionalDetailEditFragment setDroppingDetailData(
        @Nullable DroppingDetailFormData droppingDetailData) {
      this.arguments.put("droppingDetailData", droppingDetailData);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("droppingDetailData")) {
        DroppingDetailFormData droppingDetailData = (DroppingDetailFormData) arguments.get("droppingDetailData");
        if (Parcelable.class.isAssignableFrom(DroppingDetailFormData.class) || droppingDetailData == null) {
          __result.putParcelable("droppingDetailData", Parcelable.class.cast(droppingDetailData));
        } else if (Serializable.class.isAssignableFrom(DroppingDetailFormData.class)) {
          __result.putSerializable("droppingDetailData", Serializable.class.cast(droppingDetailData));
        } else {
          throw new UnsupportedOperationException(DroppingDetailFormData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("droppingDetailData", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_droppingAdditionalDetailEditFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public DroppingDetailFormData getDroppingDetailData() {
      return (DroppingDetailFormData) arguments.get("droppingDetailData");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionDroppingAdditionalDetailEditFragment that = (ActionDroppingAdditionalDetailEditFragment) object;
      if (arguments.containsKey("droppingDetailData") != that.arguments.containsKey("droppingDetailData")) {
        return false;
      }
      if (getDroppingDetailData() != null ? !getDroppingDetailData().equals(that.getDroppingDetailData()) : that.getDroppingDetailData() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getDroppingDetailData() != null ? getDroppingDetailData().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionDroppingAdditionalDetailEditFragment(actionId=" + getActionId() + "){"
          + "droppingDetailData=" + getDroppingDetailData()
          + "}";
    }
  }

  public static class BackToBrowse implements NavDirections {
    private final HashMap arguments = new HashMap();

    private BackToBrowse() {
    }

    @NonNull
    public BackToBrowse setDroppingAdditionalData(
        @Nullable DroppingAdditionalData droppingAdditionalData) {
      this.arguments.put("droppingAdditionalData", droppingAdditionalData);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("droppingAdditionalData")) {
        DroppingAdditionalData droppingAdditionalData = (DroppingAdditionalData) arguments.get("droppingAdditionalData");
        if (Parcelable.class.isAssignableFrom(DroppingAdditionalData.class) || droppingAdditionalData == null) {
          __result.putParcelable("droppingAdditionalData", Parcelable.class.cast(droppingAdditionalData));
        } else if (Serializable.class.isAssignableFrom(DroppingAdditionalData.class)) {
          __result.putSerializable("droppingAdditionalData", Serializable.class.cast(droppingAdditionalData));
        } else {
          throw new UnsupportedOperationException(DroppingAdditionalData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("droppingAdditionalData", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.backToBrowse;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public DroppingAdditionalData getDroppingAdditionalData() {
      return (DroppingAdditionalData) arguments.get("droppingAdditionalData");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      BackToBrowse that = (BackToBrowse) object;
      if (arguments.containsKey("droppingAdditionalData") != that.arguments.containsKey("droppingAdditionalData")) {
        return false;
      }
      if (getDroppingAdditionalData() != null ? !getDroppingAdditionalData().equals(that.getDroppingAdditionalData()) : that.getDroppingAdditionalData() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getDroppingAdditionalData() != null ? getDroppingAdditionalData().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "BackToBrowse(actionId=" + getActionId() + "){"
          + "droppingAdditionalData=" + getDroppingAdditionalData()
          + "}";
    }
  }

  public static class ActionWfHistory implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionWfHistory(@NonNull WfType wfType, @NonNull UUID entityId) {
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("wfType", wfType);
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("entityId", entityId);
    }

    @NonNull
    public ActionWfHistory setWfType(@NonNull WfType wfType) {
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("wfType", wfType);
      return this;
    }

    @NonNull
    public ActionWfHistory setEntityId(@NonNull UUID entityId) {
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("entityId", entityId);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("wfType")) {
        WfType wfType = (WfType) arguments.get("wfType");
        if (Parcelable.class.isAssignableFrom(WfType.class) || wfType == null) {
          __result.putParcelable("wfType", Parcelable.class.cast(wfType));
        } else if (Serializable.class.isAssignableFrom(WfType.class)) {
          __result.putSerializable("wfType", Serializable.class.cast(wfType));
        } else {
          throw new UnsupportedOperationException(WfType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("entityId")) {
        UUID entityId = (UUID) arguments.get("entityId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || entityId == null) {
          __result.putParcelable("entityId", Parcelable.class.cast(entityId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("entityId", Serializable.class.cast(entityId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_wfHistory;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public WfType getWfType() {
      return (WfType) arguments.get("wfType");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getEntityId() {
      return (UUID) arguments.get("entityId");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionWfHistory that = (ActionWfHistory) object;
      if (arguments.containsKey("wfType") != that.arguments.containsKey("wfType")) {
        return false;
      }
      if (getWfType() != null ? !getWfType().equals(that.getWfType()) : that.getWfType() != null) {
        return false;
      }
      if (arguments.containsKey("entityId") != that.arguments.containsKey("entityId")) {
        return false;
      }
      if (getEntityId() != null ? !getEntityId().equals(that.getEntityId()) : that.getEntityId() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getWfType() != null ? getWfType().hashCode() : 0);
      result = 31 * result + (getEntityId() != null ? getEntityId().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionWfHistory(actionId=" + getActionId() + "){"
          + "wfType=" + getWfType()
          + ", entityId=" + getEntityId()
          + "}";
    }
  }
}
