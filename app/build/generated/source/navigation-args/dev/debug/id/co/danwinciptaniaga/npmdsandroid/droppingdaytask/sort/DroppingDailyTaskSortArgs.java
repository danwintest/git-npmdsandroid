package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.sort;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class DroppingDailyTaskSortArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private DroppingDailyTaskSortArgs() {
  }

  private DroppingDailyTaskSortArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static DroppingDailyTaskSortArgs fromBundle(@NonNull Bundle bundle) {
    DroppingDailyTaskSortArgs __result = new DroppingDailyTaskSortArgs();
    bundle.setClassLoader(DroppingDailyTaskSortArgs.class.getClassLoader());
    if (bundle.containsKey("sortField")) {
      String sortField;
      sortField = bundle.getString("sortField");
      __result.arguments.put("sortField", sortField);
    } else {
      __result.arguments.put("sortField", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getSortField() {
    return (String) arguments.get("sortField");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("sortField")) {
      String sortField = (String) arguments.get("sortField");
      __result.putString("sortField", sortField);
    } else {
      __result.putString("sortField", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    DroppingDailyTaskSortArgs that = (DroppingDailyTaskSortArgs) object;
    if (arguments.containsKey("sortField") != that.arguments.containsKey("sortField")) {
      return false;
    }
    if (getSortField() != null ? !getSortField().equals(that.getSortField()) : that.getSortField() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getSortField() != null ? getSortField().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "DroppingDailyTaskSortArgs{"
        + "sortField=" + getSortField()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(DroppingDailyTaskSortArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public DroppingDailyTaskSortArgs build() {
      DroppingDailyTaskSortArgs result = new DroppingDailyTaskSortArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setSortField(@Nullable String sortField) {
      this.arguments.put("sortField", sortField);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getSortField() {
      return (String) arguments.get("sortField");
    }
  }
}
