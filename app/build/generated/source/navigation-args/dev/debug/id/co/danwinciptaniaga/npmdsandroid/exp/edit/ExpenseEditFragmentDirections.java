package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class ExpenseEditFragmentDirections {
  private ExpenseEditFragmentDirections() {
  }

  @NonNull
  public static BackToBrowse backToBrowse() {
    return new BackToBrowse();
  }

  @NonNull
  public static ActionExpenseEditSelectBooking actionExpenseEditSelectBooking(
      @NonNull UUID outletId) {
    return new ActionExpenseEditSelectBooking(outletId);
  }

  @NonNull
  public static ActionExpenseWfHistory actionExpenseWfHistory(@NonNull WfType wfType,
      @NonNull UUID entityId) {
    return new ActionExpenseWfHistory(wfType, entityId);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class BackToBrowse implements NavDirections {
    private final HashMap arguments = new HashMap();

    private BackToBrowse() {
    }

    @NonNull
    public BackToBrowse setExpenseData(@Nullable ExpenseData expenseData) {
      this.arguments.put("expenseData", expenseData);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("expenseData")) {
        ExpenseData expenseData = (ExpenseData) arguments.get("expenseData");
        if (Parcelable.class.isAssignableFrom(ExpenseData.class) || expenseData == null) {
          __result.putParcelable("expenseData", Parcelable.class.cast(expenseData));
        } else if (Serializable.class.isAssignableFrom(ExpenseData.class)) {
          __result.putSerializable("expenseData", Serializable.class.cast(expenseData));
        } else {
          throw new UnsupportedOperationException(ExpenseData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("expenseData", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.backToBrowse;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ExpenseData getExpenseData() {
      return (ExpenseData) arguments.get("expenseData");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      BackToBrowse that = (BackToBrowse) object;
      if (arguments.containsKey("expenseData") != that.arguments.containsKey("expenseData")) {
        return false;
      }
      if (getExpenseData() != null ? !getExpenseData().equals(that.getExpenseData()) : that.getExpenseData() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getExpenseData() != null ? getExpenseData().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "BackToBrowse(actionId=" + getActionId() + "){"
          + "expenseData=" + getExpenseData()
          + "}";
    }
  }

  public static class ActionExpenseEditSelectBooking implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionExpenseEditSelectBooking(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
    }

    @NonNull
    public ActionExpenseEditSelectBooking setOutletId(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("outletId")) {
        UUID outletId = (UUID) arguments.get("outletId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || outletId == null) {
          __result.putParcelable("outletId", Parcelable.class.cast(outletId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("outletId", Serializable.class.cast(outletId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_expenseEditSelectBooking;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getOutletId() {
      return (UUID) arguments.get("outletId");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionExpenseEditSelectBooking that = (ActionExpenseEditSelectBooking) object;
      if (arguments.containsKey("outletId") != that.arguments.containsKey("outletId")) {
        return false;
      }
      if (getOutletId() != null ? !getOutletId().equals(that.getOutletId()) : that.getOutletId() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getOutletId() != null ? getOutletId().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionExpenseEditSelectBooking(actionId=" + getActionId() + "){"
          + "outletId=" + getOutletId()
          + "}";
    }
  }

  public static class ActionExpenseWfHistory implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionExpenseWfHistory(@NonNull WfType wfType, @NonNull UUID entityId) {
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("wfType", wfType);
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("entityId", entityId);
    }

    @NonNull
    public ActionExpenseWfHistory setWfType(@NonNull WfType wfType) {
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("wfType", wfType);
      return this;
    }

    @NonNull
    public ActionExpenseWfHistory setEntityId(@NonNull UUID entityId) {
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("entityId", entityId);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("wfType")) {
        WfType wfType = (WfType) arguments.get("wfType");
        if (Parcelable.class.isAssignableFrom(WfType.class) || wfType == null) {
          __result.putParcelable("wfType", Parcelable.class.cast(wfType));
        } else if (Serializable.class.isAssignableFrom(WfType.class)) {
          __result.putSerializable("wfType", Serializable.class.cast(wfType));
        } else {
          throw new UnsupportedOperationException(WfType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("entityId")) {
        UUID entityId = (UUID) arguments.get("entityId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || entityId == null) {
          __result.putParcelable("entityId", Parcelable.class.cast(entityId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("entityId", Serializable.class.cast(entityId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_expenseWfHistory;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public WfType getWfType() {
      return (WfType) arguments.get("wfType");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getEntityId() {
      return (UUID) arguments.get("entityId");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionExpenseWfHistory that = (ActionExpenseWfHistory) object;
      if (arguments.containsKey("wfType") != that.arguments.containsKey("wfType")) {
        return false;
      }
      if (getWfType() != null ? !getWfType().equals(that.getWfType()) : that.getWfType() != null) {
        return false;
      }
      if (arguments.containsKey("entityId") != that.arguments.containsKey("entityId")) {
        return false;
      }
      if (getEntityId() != null ? !getEntityId().equals(that.getEntityId()) : that.getEntityId() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getWfType() != null ? getWfType().hashCode() : 0);
      result = 31 * result + (getEntityId() != null ? getEntityId().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionExpenseWfHistory(actionId=" + getActionId() + "){"
          + "wfType=" + getWfType()
          + ", entityId=" + getEntityId()
          + "}";
    }
  }
}
