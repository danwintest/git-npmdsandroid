package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class AppBookingTransferRekeningListFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AppBookingTransferRekeningListFragmentArgs() {
  }

  private AppBookingTransferRekeningListFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AppBookingTransferRekeningListFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AppBookingTransferRekeningListFragmentArgs __result = new AppBookingTransferRekeningListFragmentArgs();
    bundle.setClassLoader(AppBookingTransferRekeningListFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("outletId")) {
      UUID outletId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        outletId = (UUID) bundle.get("outletId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("outletId", outletId);
    } else {
      throw new IllegalArgumentException("Required argument \"outletId\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("trxType")) {
      String trxType;
      trxType = bundle.getString("trxType");
      if (trxType == null) {
        throw new IllegalArgumentException("Argument \"trxType\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("trxType", trxType);
    } else {
      throw new IllegalArgumentException("Required argument \"trxType\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public UUID getOutletId() {
    return (UUID) arguments.get("outletId");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getTrxType() {
    return (String) arguments.get("trxType");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("outletId")) {
      UUID outletId = (UUID) arguments.get("outletId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || outletId == null) {
        __result.putParcelable("outletId", Parcelable.class.cast(outletId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("outletId", Serializable.class.cast(outletId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("trxType")) {
      String trxType = (String) arguments.get("trxType");
      __result.putString("trxType", trxType);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AppBookingTransferRekeningListFragmentArgs that = (AppBookingTransferRekeningListFragmentArgs) object;
    if (arguments.containsKey("outletId") != that.arguments.containsKey("outletId")) {
      return false;
    }
    if (getOutletId() != null ? !getOutletId().equals(that.getOutletId()) : that.getOutletId() != null) {
      return false;
    }
    if (arguments.containsKey("trxType") != that.arguments.containsKey("trxType")) {
      return false;
    }
    if (getTrxType() != null ? !getTrxType().equals(that.getTrxType()) : that.getTrxType() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getOutletId() != null ? getOutletId().hashCode() : 0);
    result = 31 * result + (getTrxType() != null ? getTrxType().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AppBookingTransferRekeningListFragmentArgs{"
        + "outletId=" + getOutletId()
        + ", trxType=" + getTrxType()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AppBookingTransferRekeningListFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull UUID outletId, @NonNull String trxType) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      if (trxType == null) {
        throw new IllegalArgumentException("Argument \"trxType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("trxType", trxType);
    }

    @NonNull
    public AppBookingTransferRekeningListFragmentArgs build() {
      AppBookingTransferRekeningListFragmentArgs result = new AppBookingTransferRekeningListFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setOutletId(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      return this;
    }

    @NonNull
    public Builder setTrxType(@NonNull String trxType) {
      if (trxType == null) {
        throw new IllegalArgumentException("Argument \"trxType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("trxType", trxType);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getOutletId() {
      return (UUID) arguments.get("outletId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getTrxType() {
      return (String) arguments.get("trxType");
    }
  }
}
