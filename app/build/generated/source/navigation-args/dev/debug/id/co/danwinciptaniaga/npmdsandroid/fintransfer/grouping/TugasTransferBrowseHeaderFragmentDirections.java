package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class TugasTransferBrowseHeaderFragmentDirections {
  private TugasTransferBrowseHeaderFragmentDirections() {
  }

  @NonNull
  public static ActionFilter actionFilter(boolean isGrouping) {
    return new ActionFilter(isGrouping);
  }

  @NonNull
  public static ActionSort actionSort() {
    return new ActionSort();
  }

  @NonNull
  public static ActionDetail actionDetail(@NonNull TugasTransferBrowseAndroidFilter filterData) {
    return new ActionDetail(filterData);
  }

  @NonNull
  public static ActionCekSaldo actionCekSaldo(@NonNull UUID companyId, @NonNull String companyName,
      @NonNull String companyCode) {
    return new ActionCekSaldo(companyId, companyName, companyCode);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionFilter implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionFilter(boolean isGrouping) {
      this.arguments.put("isGrouping", isGrouping);
    }

    @NonNull
    public ActionFilter setFilterField(@Nullable TugasTransferBrowseAndroidFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @NonNull
    public ActionFilter setIsGrouping(boolean isGrouping) {
      this.arguments.put("isGrouping", isGrouping);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("filterField")) {
        TugasTransferBrowseAndroidFilter filterField = (TugasTransferBrowseAndroidFilter) arguments.get("filterField");
        if (Parcelable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class) || filterField == null) {
          __result.putParcelable("filterField", Parcelable.class.cast(filterField));
        } else if (Serializable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class)) {
          __result.putSerializable("filterField", Serializable.class.cast(filterField));
        } else {
          throw new UnsupportedOperationException(TugasTransferBrowseAndroidFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("filterField", null);
      }
      if (arguments.containsKey("isGrouping")) {
        boolean isGrouping = (boolean) arguments.get("isGrouping");
        __result.putBoolean("isGrouping", isGrouping);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_filter;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public TugasTransferBrowseAndroidFilter getFilterField() {
      return (TugasTransferBrowseAndroidFilter) arguments.get("filterField");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsGrouping() {
      return (boolean) arguments.get("isGrouping");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionFilter that = (ActionFilter) object;
      if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
        return false;
      }
      if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
        return false;
      }
      if (arguments.containsKey("isGrouping") != that.arguments.containsKey("isGrouping")) {
        return false;
      }
      if (getIsGrouping() != that.getIsGrouping()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
      result = 31 * result + (getIsGrouping() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionFilter(actionId=" + getActionId() + "){"
          + "filterField=" + getFilterField()
          + ", isGrouping=" + getIsGrouping()
          + "}";
    }
  }

  public static class ActionSort implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionSort() {
    }

    @NonNull
    public ActionSort setSortField(@Nullable String sortField) {
      this.arguments.put("sortField", sortField);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("sortField")) {
        String sortField = (String) arguments.get("sortField");
        __result.putString("sortField", sortField);
      } else {
        __result.putString("sortField", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_sort;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getSortField() {
      return (String) arguments.get("sortField");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionSort that = (ActionSort) object;
      if (arguments.containsKey("sortField") != that.arguments.containsKey("sortField")) {
        return false;
      }
      if (getSortField() != null ? !getSortField().equals(that.getSortField()) : that.getSortField() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getSortField() != null ? getSortField().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionSort(actionId=" + getActionId() + "){"
          + "sortField=" + getSortField()
          + "}";
    }
  }

  public static class ActionDetail implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionDetail(@NonNull TugasTransferBrowseAndroidFilter filterData) {
      if (filterData == null) {
        throw new IllegalArgumentException("Argument \"filterData\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("filterData", filterData);
    }

    @NonNull
    public ActionDetail setFilterData(@NonNull TugasTransferBrowseAndroidFilter filterData) {
      if (filterData == null) {
        throw new IllegalArgumentException("Argument \"filterData\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("filterData", filterData);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("filterData")) {
        TugasTransferBrowseAndroidFilter filterData = (TugasTransferBrowseAndroidFilter) arguments.get("filterData");
        if (Parcelable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class) || filterData == null) {
          __result.putParcelable("filterData", Parcelable.class.cast(filterData));
        } else if (Serializable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class)) {
          __result.putSerializable("filterData", Serializable.class.cast(filterData));
        } else {
          throw new UnsupportedOperationException(TugasTransferBrowseAndroidFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_detail;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public TugasTransferBrowseAndroidFilter getFilterData() {
      return (TugasTransferBrowseAndroidFilter) arguments.get("filterData");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionDetail that = (ActionDetail) object;
      if (arguments.containsKey("filterData") != that.arguments.containsKey("filterData")) {
        return false;
      }
      if (getFilterData() != null ? !getFilterData().equals(that.getFilterData()) : that.getFilterData() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getFilterData() != null ? getFilterData().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionDetail(actionId=" + getActionId() + "){"
          + "filterData=" + getFilterData()
          + "}";
    }
  }

  public static class ActionCekSaldo implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionCekSaldo(@NonNull UUID companyId, @NonNull String companyName,
        @NonNull String companyCode) {
      if (companyId == null) {
        throw new IllegalArgumentException("Argument \"companyId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyId", companyId);
      if (companyName == null) {
        throw new IllegalArgumentException("Argument \"companyName\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyName", companyName);
      if (companyCode == null) {
        throw new IllegalArgumentException("Argument \"companyCode\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyCode", companyCode);
    }

    @NonNull
    public ActionCekSaldo setCompanyId(@NonNull UUID companyId) {
      if (companyId == null) {
        throw new IllegalArgumentException("Argument \"companyId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyId", companyId);
      return this;
    }

    @NonNull
    public ActionCekSaldo setCompanyName(@NonNull String companyName) {
      if (companyName == null) {
        throw new IllegalArgumentException("Argument \"companyName\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyName", companyName);
      return this;
    }

    @NonNull
    public ActionCekSaldo setCompanyCode(@NonNull String companyCode) {
      if (companyCode == null) {
        throw new IllegalArgumentException("Argument \"companyCode\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyCode", companyCode);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("companyId")) {
        UUID companyId = (UUID) arguments.get("companyId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || companyId == null) {
          __result.putParcelable("companyId", Parcelable.class.cast(companyId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("companyId", Serializable.class.cast(companyId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("companyName")) {
        String companyName = (String) arguments.get("companyName");
        __result.putString("companyName", companyName);
      }
      if (arguments.containsKey("companyCode")) {
        String companyCode = (String) arguments.get("companyCode");
        __result.putString("companyCode", companyCode);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_cek_saldo;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getCompanyId() {
      return (UUID) arguments.get("companyId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getCompanyName() {
      return (String) arguments.get("companyName");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getCompanyCode() {
      return (String) arguments.get("companyCode");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionCekSaldo that = (ActionCekSaldo) object;
      if (arguments.containsKey("companyId") != that.arguments.containsKey("companyId")) {
        return false;
      }
      if (getCompanyId() != null ? !getCompanyId().equals(that.getCompanyId()) : that.getCompanyId() != null) {
        return false;
      }
      if (arguments.containsKey("companyName") != that.arguments.containsKey("companyName")) {
        return false;
      }
      if (getCompanyName() != null ? !getCompanyName().equals(that.getCompanyName()) : that.getCompanyName() != null) {
        return false;
      }
      if (arguments.containsKey("companyCode") != that.arguments.containsKey("companyCode")) {
        return false;
      }
      if (getCompanyCode() != null ? !getCompanyCode().equals(that.getCompanyCode()) : that.getCompanyCode() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getCompanyId() != null ? getCompanyId().hashCode() : 0);
      result = 31 * result + (getCompanyName() != null ? getCompanyName().hashCode() : 0);
      result = 31 * result + (getCompanyCode() != null ? getCompanyCode().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionCekSaldo(actionId=" + getActionId() + "){"
          + "companyId=" + getCompanyId()
          + ", companyName=" + getCompanyName()
          + ", companyCode=" + getCompanyCode()
          + "}";
    }
  }
}
