package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class AppBookingTransferRekeningListFragmentDirections {
  private AppBookingTransferRekeningListFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
