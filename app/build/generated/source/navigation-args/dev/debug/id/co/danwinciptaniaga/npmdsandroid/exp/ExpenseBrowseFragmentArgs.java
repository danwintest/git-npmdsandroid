package id.co.danwinciptaniaga.npmdsandroid.exp;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class ExpenseBrowseFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private ExpenseBrowseFragmentArgs() {
  }

  private ExpenseBrowseFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static ExpenseBrowseFragmentArgs fromBundle(@NonNull Bundle bundle) {
    ExpenseBrowseFragmentArgs __result = new ExpenseBrowseFragmentArgs();
    bundle.setClassLoader(ExpenseBrowseFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("expenseData")) {
      ExpenseData expenseData;
      if (Parcelable.class.isAssignableFrom(ExpenseData.class) || Serializable.class.isAssignableFrom(ExpenseData.class)) {
        expenseData = (ExpenseData) bundle.get("expenseData");
      } else {
        throw new UnsupportedOperationException(ExpenseData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("expenseData", expenseData);
    } else {
      __result.arguments.put("expenseData", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public ExpenseData getExpenseData() {
    return (ExpenseData) arguments.get("expenseData");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("expenseData")) {
      ExpenseData expenseData = (ExpenseData) arguments.get("expenseData");
      if (Parcelable.class.isAssignableFrom(ExpenseData.class) || expenseData == null) {
        __result.putParcelable("expenseData", Parcelable.class.cast(expenseData));
      } else if (Serializable.class.isAssignableFrom(ExpenseData.class)) {
        __result.putSerializable("expenseData", Serializable.class.cast(expenseData));
      } else {
        throw new UnsupportedOperationException(ExpenseData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    } else {
      __result.putSerializable("expenseData", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    ExpenseBrowseFragmentArgs that = (ExpenseBrowseFragmentArgs) object;
    if (arguments.containsKey("expenseData") != that.arguments.containsKey("expenseData")) {
      return false;
    }
    if (getExpenseData() != null ? !getExpenseData().equals(that.getExpenseData()) : that.getExpenseData() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getExpenseData() != null ? getExpenseData().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "ExpenseBrowseFragmentArgs{"
        + "expenseData=" + getExpenseData()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(ExpenseBrowseFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public ExpenseBrowseFragmentArgs build() {
      ExpenseBrowseFragmentArgs result = new ExpenseBrowseFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setExpenseData(@Nullable ExpenseData expenseData) {
      this.arguments.put("expenseData", expenseData);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ExpenseData getExpenseData() {
      return (ExpenseData) arguments.get("expenseData");
    }
  }
}
