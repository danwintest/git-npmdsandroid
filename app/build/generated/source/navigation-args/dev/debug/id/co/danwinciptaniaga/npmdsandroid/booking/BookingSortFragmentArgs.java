package id.co.danwinciptaniaga.npmdsandroid.booking;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class BookingSortFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private BookingSortFragmentArgs() {
  }

  private BookingSortFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static BookingSortFragmentArgs fromBundle(@NonNull Bundle bundle) {
    BookingSortFragmentArgs __result = new BookingSortFragmentArgs();
    bundle.setClassLoader(BookingSortFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("sortField")) {
      String sortField;
      sortField = bundle.getString("sortField");
      __result.arguments.put("sortField", sortField);
    } else {
      __result.arguments.put("sortField", null);
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getSortField() {
    return (String) arguments.get("sortField");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("sortField")) {
      String sortField = (String) arguments.get("sortField");
      __result.putString("sortField", sortField);
    } else {
      __result.putString("sortField", null);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    BookingSortFragmentArgs that = (BookingSortFragmentArgs) object;
    if (arguments.containsKey("sortField") != that.arguments.containsKey("sortField")) {
      return false;
    }
    if (getSortField() != null ? !getSortField().equals(that.getSortField()) : that.getSortField() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getSortField() != null ? getSortField().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "BookingSortFragmentArgs{"
        + "sortField=" + getSortField()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(BookingSortFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder() {
    }

    @NonNull
    public BookingSortFragmentArgs build() {
      BookingSortFragmentArgs result = new BookingSortFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setSortField(@Nullable String sortField) {
      this.arguments.put("sortField", sortField);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getSortField() {
      return (String) arguments.get("sortField");
    }
  }
}
