package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class TugasTransferDetailFragmentDirections {
  private TugasTransferDetailFragmentDirections() {
  }

  @NonNull
  public static ActionFilter actionFilter(boolean isGrouping) {
    return new ActionFilter(isGrouping);
  }

  @NonNull
  public static ActionSort actionSort() {
    return new ActionSort();
  }

  @NonNull
  public static ActionWf actionWf(@NonNull String dataList, @Nullable String decision,
      boolean isAuto) {
    return new ActionWf(dataList, decision, isAuto);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionFilter implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionFilter(boolean isGrouping) {
      this.arguments.put("isGrouping", isGrouping);
    }

    @NonNull
    public ActionFilter setFilterField(@Nullable TugasTransferBrowseAndroidFilter filterField) {
      this.arguments.put("filterField", filterField);
      return this;
    }

    @NonNull
    public ActionFilter setIsGrouping(boolean isGrouping) {
      this.arguments.put("isGrouping", isGrouping);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("filterField")) {
        TugasTransferBrowseAndroidFilter filterField = (TugasTransferBrowseAndroidFilter) arguments.get("filterField");
        if (Parcelable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class) || filterField == null) {
          __result.putParcelable("filterField", Parcelable.class.cast(filterField));
        } else if (Serializable.class.isAssignableFrom(TugasTransferBrowseAndroidFilter.class)) {
          __result.putSerializable("filterField", Serializable.class.cast(filterField));
        } else {
          throw new UnsupportedOperationException(TugasTransferBrowseAndroidFilter.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("filterField", null);
      }
      if (arguments.containsKey("isGrouping")) {
        boolean isGrouping = (boolean) arguments.get("isGrouping");
        __result.putBoolean("isGrouping", isGrouping);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_filter;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public TugasTransferBrowseAndroidFilter getFilterField() {
      return (TugasTransferBrowseAndroidFilter) arguments.get("filterField");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsGrouping() {
      return (boolean) arguments.get("isGrouping");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionFilter that = (ActionFilter) object;
      if (arguments.containsKey("filterField") != that.arguments.containsKey("filterField")) {
        return false;
      }
      if (getFilterField() != null ? !getFilterField().equals(that.getFilterField()) : that.getFilterField() != null) {
        return false;
      }
      if (arguments.containsKey("isGrouping") != that.arguments.containsKey("isGrouping")) {
        return false;
      }
      if (getIsGrouping() != that.getIsGrouping()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getFilterField() != null ? getFilterField().hashCode() : 0);
      result = 31 * result + (getIsGrouping() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionFilter(actionId=" + getActionId() + "){"
          + "filterField=" + getFilterField()
          + ", isGrouping=" + getIsGrouping()
          + "}";
    }
  }

  public static class ActionSort implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionSort() {
    }

    @NonNull
    public ActionSort setSortField(@Nullable String sortField) {
      this.arguments.put("sortField", sortField);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("sortField")) {
        String sortField = (String) arguments.get("sortField");
        __result.putString("sortField", sortField);
      } else {
        __result.putString("sortField", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_sort;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getSortField() {
      return (String) arguments.get("sortField");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionSort that = (ActionSort) object;
      if (arguments.containsKey("sortField") != that.arguments.containsKey("sortField")) {
        return false;
      }
      if (getSortField() != null ? !getSortField().equals(that.getSortField()) : that.getSortField() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getSortField() != null ? getSortField().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionSort(actionId=" + getActionId() + "){"
          + "sortField=" + getSortField()
          + "}";
    }
  }

  public static class ActionWf implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionWf(@NonNull String dataList, @Nullable String decision, boolean isAuto) {
      if (dataList == null) {
        throw new IllegalArgumentException("Argument \"dataList\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("dataList", dataList);
      this.arguments.put("decision", decision);
      this.arguments.put("isAuto", isAuto);
    }

    @NonNull
    public ActionWf setDataList(@NonNull String dataList) {
      if (dataList == null) {
        throw new IllegalArgumentException("Argument \"dataList\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("dataList", dataList);
      return this;
    }

    @NonNull
    public ActionWf setDecision(@Nullable String decision) {
      this.arguments.put("decision", decision);
      return this;
    }

    @NonNull
    public ActionWf setIsAuto(boolean isAuto) {
      this.arguments.put("isAuto", isAuto);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("dataList")) {
        String dataList = (String) arguments.get("dataList");
        __result.putString("dataList", dataList);
      }
      if (arguments.containsKey("decision")) {
        String decision = (String) arguments.get("decision");
        __result.putString("decision", decision);
      }
      if (arguments.containsKey("isAuto")) {
        boolean isAuto = (boolean) arguments.get("isAuto");
        __result.putBoolean("isAuto", isAuto);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_wf;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getDataList() {
      return (String) arguments.get("dataList");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getDecision() {
      return (String) arguments.get("decision");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsAuto() {
      return (boolean) arguments.get("isAuto");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionWf that = (ActionWf) object;
      if (arguments.containsKey("dataList") != that.arguments.containsKey("dataList")) {
        return false;
      }
      if (getDataList() != null ? !getDataList().equals(that.getDataList()) : that.getDataList() != null) {
        return false;
      }
      if (arguments.containsKey("decision") != that.arguments.containsKey("decision")) {
        return false;
      }
      if (getDecision() != null ? !getDecision().equals(that.getDecision()) : that.getDecision() != null) {
        return false;
      }
      if (arguments.containsKey("isAuto") != that.arguments.containsKey("isAuto")) {
        return false;
      }
      if (getIsAuto() != that.getIsAuto()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getDataList() != null ? getDataList().hashCode() : 0);
      result = 31 * result + (getDecision() != null ? getDecision().hashCode() : 0);
      result = 31 * result + (getIsAuto() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionWf(actionId=" + getActionId() + "){"
          + "dataList=" + getDataList()
          + ", decision=" + getDecision()
          + ", isAuto=" + getIsAuto()
          + "}";
    }
  }
}
