package id.co.danwinciptaniaga.npmdsandroid.wf;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavArgs;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class WfHistoryFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private WfHistoryFragmentArgs() {
  }

  private WfHistoryFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static WfHistoryFragmentArgs fromBundle(@NonNull Bundle bundle) {
    WfHistoryFragmentArgs __result = new WfHistoryFragmentArgs();
    bundle.setClassLoader(WfHistoryFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("wfType")) {
      WfType wfType;
      if (Parcelable.class.isAssignableFrom(WfType.class) || Serializable.class.isAssignableFrom(WfType.class)) {
        wfType = (WfType) bundle.get("wfType");
      } else {
        throw new UnsupportedOperationException(WfType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("wfType", wfType);
    } else {
      throw new IllegalArgumentException("Required argument \"wfType\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("entityId")) {
      UUID entityId;
      if (Parcelable.class.isAssignableFrom(UUID.class) || Serializable.class.isAssignableFrom(UUID.class)) {
        entityId = (UUID) bundle.get("entityId");
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("entityId", entityId);
    } else {
      throw new IllegalArgumentException("Required argument \"entityId\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public WfType getWfType() {
    return (WfType) arguments.get("wfType");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public UUID getEntityId() {
    return (UUID) arguments.get("entityId");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("wfType")) {
      WfType wfType = (WfType) arguments.get("wfType");
      if (Parcelable.class.isAssignableFrom(WfType.class) || wfType == null) {
        __result.putParcelable("wfType", Parcelable.class.cast(wfType));
      } else if (Serializable.class.isAssignableFrom(WfType.class)) {
        __result.putSerializable("wfType", Serializable.class.cast(wfType));
      } else {
        throw new UnsupportedOperationException(WfType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("entityId")) {
      UUID entityId = (UUID) arguments.get("entityId");
      if (Parcelable.class.isAssignableFrom(UUID.class) || entityId == null) {
        __result.putParcelable("entityId", Parcelable.class.cast(entityId));
      } else if (Serializable.class.isAssignableFrom(UUID.class)) {
        __result.putSerializable("entityId", Serializable.class.cast(entityId));
      } else {
        throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    WfHistoryFragmentArgs that = (WfHistoryFragmentArgs) object;
    if (arguments.containsKey("wfType") != that.arguments.containsKey("wfType")) {
      return false;
    }
    if (getWfType() != null ? !getWfType().equals(that.getWfType()) : that.getWfType() != null) {
      return false;
    }
    if (arguments.containsKey("entityId") != that.arguments.containsKey("entityId")) {
      return false;
    }
    if (getEntityId() != null ? !getEntityId().equals(that.getEntityId()) : that.getEntityId() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getWfType() != null ? getWfType().hashCode() : 0);
    result = 31 * result + (getEntityId() != null ? getEntityId().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "WfHistoryFragmentArgs{"
        + "wfType=" + getWfType()
        + ", entityId=" + getEntityId()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(WfHistoryFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull WfType wfType, @NonNull UUID entityId) {
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("wfType", wfType);
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("entityId", entityId);
    }

    @NonNull
    public WfHistoryFragmentArgs build() {
      WfHistoryFragmentArgs result = new WfHistoryFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setWfType(@NonNull WfType wfType) {
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("wfType", wfType);
      return this;
    }

    @NonNull
    public Builder setEntityId(@NonNull UUID entityId) {
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("entityId", entityId);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public WfType getWfType() {
      return (WfType) arguments.get("wfType");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getEntityId() {
      return (UUID) arguments.get("entityId");
    }
  }
}
