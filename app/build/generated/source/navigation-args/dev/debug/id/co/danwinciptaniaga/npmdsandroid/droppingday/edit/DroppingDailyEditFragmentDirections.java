package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class DroppingDailyEditFragmentDirections {
  private DroppingDailyEditFragmentDirections() {
  }

  @NonNull
  public static ActionEditAltAccount actionEditAltAccount() {
    return new ActionEditAltAccount();
  }

  @NonNull
  public static ActionWfHistory actionWfHistory(@NonNull WfType wfType, @NonNull UUID entityId) {
    return new ActionWfHistory(wfType, entityId);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionEditAltAccount implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionEditAltAccount() {
    }

    @NonNull
    public ActionEditAltAccount setBankAccountData(@Nullable BankAccountData bankAccountData) {
      this.arguments.put("bankAccountData", bankAccountData);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("bankAccountData")) {
        BankAccountData bankAccountData = (BankAccountData) arguments.get("bankAccountData");
        if (Parcelable.class.isAssignableFrom(BankAccountData.class) || bankAccountData == null) {
          __result.putParcelable("bankAccountData", Parcelable.class.cast(bankAccountData));
        } else if (Serializable.class.isAssignableFrom(BankAccountData.class)) {
          __result.putSerializable("bankAccountData", Serializable.class.cast(bankAccountData));
        } else {
          throw new UnsupportedOperationException(BankAccountData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("bankAccountData", null);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_edit_alt_account;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public BankAccountData getBankAccountData() {
      return (BankAccountData) arguments.get("bankAccountData");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionEditAltAccount that = (ActionEditAltAccount) object;
      if (arguments.containsKey("bankAccountData") != that.arguments.containsKey("bankAccountData")) {
        return false;
      }
      if (getBankAccountData() != null ? !getBankAccountData().equals(that.getBankAccountData()) : that.getBankAccountData() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getBankAccountData() != null ? getBankAccountData().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionEditAltAccount(actionId=" + getActionId() + "){"
          + "bankAccountData=" + getBankAccountData()
          + "}";
    }
  }

  public static class ActionWfHistory implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionWfHistory(@NonNull WfType wfType, @NonNull UUID entityId) {
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("wfType", wfType);
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("entityId", entityId);
    }

    @NonNull
    public ActionWfHistory setWfType(@NonNull WfType wfType) {
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("wfType", wfType);
      return this;
    }

    @NonNull
    public ActionWfHistory setEntityId(@NonNull UUID entityId) {
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("entityId", entityId);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("wfType")) {
        WfType wfType = (WfType) arguments.get("wfType");
        if (Parcelable.class.isAssignableFrom(WfType.class) || wfType == null) {
          __result.putParcelable("wfType", Parcelable.class.cast(wfType));
        } else if (Serializable.class.isAssignableFrom(WfType.class)) {
          __result.putSerializable("wfType", Serializable.class.cast(wfType));
        } else {
          throw new UnsupportedOperationException(WfType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("entityId")) {
        UUID entityId = (UUID) arguments.get("entityId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || entityId == null) {
          __result.putParcelable("entityId", Parcelable.class.cast(entityId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("entityId", Serializable.class.cast(entityId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_WfHistory;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public WfType getWfType() {
      return (WfType) arguments.get("wfType");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getEntityId() {
      return (UUID) arguments.get("entityId");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionWfHistory that = (ActionWfHistory) object;
      if (arguments.containsKey("wfType") != that.arguments.containsKey("wfType")) {
        return false;
      }
      if (getWfType() != null ? !getWfType().equals(that.getWfType()) : that.getWfType() != null) {
        return false;
      }
      if (arguments.containsKey("entityId") != that.arguments.containsKey("entityId")) {
        return false;
      }
      if (getEntityId() != null ? !getEntityId().equals(that.getEntityId()) : that.getEntityId() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getWfType() != null ? getWfType().hashCode() : 0);
      result = 31 * result + (getEntityId() != null ? getEntityId().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionWfHistory(actionId=" + getActionId() + "){"
          + "wfType=" + getWfType()
          + ", entityId=" + getEntityId()
          + "}";
    }
  }
}
