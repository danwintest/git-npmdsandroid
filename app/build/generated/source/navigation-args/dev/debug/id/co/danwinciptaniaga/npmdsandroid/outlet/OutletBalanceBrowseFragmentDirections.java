package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class OutletBalanceBrowseFragmentDirections {
  private OutletBalanceBrowseFragmentDirections() {
  }

  @NonNull
  public static ActionBalanceReportFragment actionBalanceReportFragment(@NonNull UUID outletId) {
    return new ActionBalanceReportFragment(outletId);
  }

  @NonNull
  public static ActionStockByDayReportFragment actionStockByDayReportFragment(
      @NonNull UUID outletId) {
    return new ActionStockByDayReportFragment(outletId);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionBalanceReportFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionBalanceReportFragment(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
    }

    @NonNull
    public ActionBalanceReportFragment setOutletId(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("outletId")) {
        UUID outletId = (UUID) arguments.get("outletId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || outletId == null) {
          __result.putParcelable("outletId", Parcelable.class.cast(outletId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("outletId", Serializable.class.cast(outletId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_balanceReportFragment;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getOutletId() {
      return (UUID) arguments.get("outletId");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionBalanceReportFragment that = (ActionBalanceReportFragment) object;
      if (arguments.containsKey("outletId") != that.arguments.containsKey("outletId")) {
        return false;
      }
      if (getOutletId() != null ? !getOutletId().equals(that.getOutletId()) : that.getOutletId() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getOutletId() != null ? getOutletId().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionBalanceReportFragment(actionId=" + getActionId() + "){"
          + "outletId=" + getOutletId()
          + "}";
    }
  }

  public static class ActionStockByDayReportFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionStockByDayReportFragment(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
    }

    @NonNull
    public ActionStockByDayReportFragment setOutletId(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("outletId")) {
        UUID outletId = (UUID) arguments.get("outletId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || outletId == null) {
          __result.putParcelable("outletId", Parcelable.class.cast(outletId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("outletId", Serializable.class.cast(outletId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_stockByDayReportFragment;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getOutletId() {
      return (UUID) arguments.get("outletId");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionStockByDayReportFragment that = (ActionStockByDayReportFragment) object;
      if (arguments.containsKey("outletId") != that.arguments.containsKey("outletId")) {
        return false;
      }
      if (getOutletId() != null ? !getOutletId().equals(that.getOutletId()) : that.getOutletId() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getOutletId() != null ? getOutletId().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionStockByDayReportFragment(actionId=" + getActionId() + "){"
          + "outletId=" + getOutletId()
          + "}";
    }
  }
}
