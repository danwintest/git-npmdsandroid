package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class AppBookingSortFragmentDirections {
  private AppBookingSortFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
