package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.UUID;

public class AppBookingTransferNewFragmentDirections {
  private AppBookingTransferNewFragmentDirections() {
  }

  @NonNull
  public static ActionWfHistory actionWfHistory(@NonNull WfType wfType, @NonNull UUID entityId) {
    return new ActionWfHistory(wfType, entityId);
  }

  @NonNull
  public static ActionTrxForm actionTrxForm(@NonNull UUID outletId, @NonNull String mode,
      @NonNull UUID companyId, boolean fromBookingStatus, @Nullable String consumerName,
      @Nullable String accName, @Nullable String accNo, @Nullable UUID bankId,
      @Nullable String otherBankName, boolean validStatus) {
    return new ActionTrxForm(outletId, mode, companyId, fromBookingStatus, consumerName, accName, accNo, bankId, otherBankName, validStatus);
  }

  @NonNull
  public static ActionTxnMode actionTxnMode(@NonNull String appOperation,
      @NonNull String bookingType, @Nullable String txnMode, boolean isHasCreatePermission) {
    return new ActionTxnMode(appOperation, bookingType, txnMode, isHasCreatePermission);
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }

  public static class ActionWfHistory implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionWfHistory(@NonNull WfType wfType, @NonNull UUID entityId) {
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("wfType", wfType);
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("entityId", entityId);
    }

    @NonNull
    public ActionWfHistory setWfType(@NonNull WfType wfType) {
      if (wfType == null) {
        throw new IllegalArgumentException("Argument \"wfType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("wfType", wfType);
      return this;
    }

    @NonNull
    public ActionWfHistory setEntityId(@NonNull UUID entityId) {
      if (entityId == null) {
        throw new IllegalArgumentException("Argument \"entityId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("entityId", entityId);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("wfType")) {
        WfType wfType = (WfType) arguments.get("wfType");
        if (Parcelable.class.isAssignableFrom(WfType.class) || wfType == null) {
          __result.putParcelable("wfType", Parcelable.class.cast(wfType));
        } else if (Serializable.class.isAssignableFrom(WfType.class)) {
          __result.putSerializable("wfType", Serializable.class.cast(wfType));
        } else {
          throw new UnsupportedOperationException(WfType.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("entityId")) {
        UUID entityId = (UUID) arguments.get("entityId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || entityId == null) {
          __result.putParcelable("entityId", Parcelable.class.cast(entityId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("entityId", Serializable.class.cast(entityId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_WfHistory;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public WfType getWfType() {
      return (WfType) arguments.get("wfType");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getEntityId() {
      return (UUID) arguments.get("entityId");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionWfHistory that = (ActionWfHistory) object;
      if (arguments.containsKey("wfType") != that.arguments.containsKey("wfType")) {
        return false;
      }
      if (getWfType() != null ? !getWfType().equals(that.getWfType()) : that.getWfType() != null) {
        return false;
      }
      if (arguments.containsKey("entityId") != that.arguments.containsKey("entityId")) {
        return false;
      }
      if (getEntityId() != null ? !getEntityId().equals(that.getEntityId()) : that.getEntityId() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getWfType() != null ? getWfType().hashCode() : 0);
      result = 31 * result + (getEntityId() != null ? getEntityId().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionWfHistory(actionId=" + getActionId() + "){"
          + "wfType=" + getWfType()
          + ", entityId=" + getEntityId()
          + "}";
    }
  }

  public static class ActionTrxForm implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionTrxForm(@NonNull UUID outletId, @NonNull String mode, @NonNull UUID companyId,
        boolean fromBookingStatus, @Nullable String consumerName, @Nullable String accName,
        @Nullable String accNo, @Nullable UUID bankId, @Nullable String otherBankName,
        boolean validStatus) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      if (mode == null) {
        throw new IllegalArgumentException("Argument \"mode\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("mode", mode);
      if (companyId == null) {
        throw new IllegalArgumentException("Argument \"companyId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyId", companyId);
      this.arguments.put("fromBookingStatus", fromBookingStatus);
      this.arguments.put("consumerName", consumerName);
      this.arguments.put("accName", accName);
      this.arguments.put("accNo", accNo);
      this.arguments.put("bankId", bankId);
      this.arguments.put("otherBankName", otherBankName);
      this.arguments.put("validStatus", validStatus);
    }

    @NonNull
    public ActionTrxForm setOutletId(@NonNull UUID outletId) {
      if (outletId == null) {
        throw new IllegalArgumentException("Argument \"outletId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("outletId", outletId);
      return this;
    }

    @NonNull
    public ActionTrxForm setMode(@NonNull String mode) {
      if (mode == null) {
        throw new IllegalArgumentException("Argument \"mode\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("mode", mode);
      return this;
    }

    @NonNull
    public ActionTrxForm setAppBookingId(@Nullable UUID appBookingId) {
      this.arguments.put("appBookingId", appBookingId);
      return this;
    }

    @NonNull
    public ActionTrxForm setBookingId(@Nullable UUID bookingId) {
      this.arguments.put("bookingId", bookingId);
      return this;
    }

    @NonNull
    public ActionTrxForm setCompanyId(@NonNull UUID companyId) {
      if (companyId == null) {
        throw new IllegalArgumentException("Argument \"companyId\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("companyId", companyId);
      return this;
    }

    @NonNull
    public ActionTrxForm setFromBookingStatus(boolean fromBookingStatus) {
      this.arguments.put("fromBookingStatus", fromBookingStatus);
      return this;
    }

    @NonNull
    public ActionTrxForm setConsumerName(@Nullable String consumerName) {
      this.arguments.put("consumerName", consumerName);
      return this;
    }

    @NonNull
    public ActionTrxForm setAccName(@Nullable String accName) {
      this.arguments.put("accName", accName);
      return this;
    }

    @NonNull
    public ActionTrxForm setAccNo(@Nullable String accNo) {
      this.arguments.put("accNo", accNo);
      return this;
    }

    @NonNull
    public ActionTrxForm setBankId(@Nullable UUID bankId) {
      this.arguments.put("bankId", bankId);
      return this;
    }

    @NonNull
    public ActionTrxForm setOtherBankName(@Nullable String otherBankName) {
      this.arguments.put("otherBankName", otherBankName);
      return this;
    }

    @NonNull
    public ActionTrxForm setValidStatus(boolean validStatus) {
      this.arguments.put("validStatus", validStatus);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("outletId")) {
        UUID outletId = (UUID) arguments.get("outletId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || outletId == null) {
          __result.putParcelable("outletId", Parcelable.class.cast(outletId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("outletId", Serializable.class.cast(outletId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("mode")) {
        String mode = (String) arguments.get("mode");
        __result.putString("mode", mode);
      }
      if (arguments.containsKey("appBookingId")) {
        UUID appBookingId = (UUID) arguments.get("appBookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || appBookingId == null) {
          __result.putParcelable("appBookingId", Parcelable.class.cast(appBookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("appBookingId", Serializable.class.cast(appBookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("appBookingId", null);
      }
      if (arguments.containsKey("bookingId")) {
        UUID bookingId = (UUID) arguments.get("bookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || bookingId == null) {
          __result.putParcelable("bookingId", Parcelable.class.cast(bookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("bookingId", Serializable.class.cast(bookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("bookingId", null);
      }
      if (arguments.containsKey("companyId")) {
        UUID companyId = (UUID) arguments.get("companyId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || companyId == null) {
          __result.putParcelable("companyId", Parcelable.class.cast(companyId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("companyId", Serializable.class.cast(companyId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("fromBookingStatus")) {
        boolean fromBookingStatus = (boolean) arguments.get("fromBookingStatus");
        __result.putBoolean("fromBookingStatus", fromBookingStatus);
      }
      if (arguments.containsKey("consumerName")) {
        String consumerName = (String) arguments.get("consumerName");
        __result.putString("consumerName", consumerName);
      }
      if (arguments.containsKey("accName")) {
        String accName = (String) arguments.get("accName");
        __result.putString("accName", accName);
      }
      if (arguments.containsKey("accNo")) {
        String accNo = (String) arguments.get("accNo");
        __result.putString("accNo", accNo);
      }
      if (arguments.containsKey("bankId")) {
        UUID bankId = (UUID) arguments.get("bankId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || bankId == null) {
          __result.putParcelable("bankId", Parcelable.class.cast(bankId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("bankId", Serializable.class.cast(bankId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("otherBankName")) {
        String otherBankName = (String) arguments.get("otherBankName");
        __result.putString("otherBankName", otherBankName);
      }
      if (arguments.containsKey("validStatus")) {
        boolean validStatus = (boolean) arguments.get("validStatus");
        __result.putBoolean("validStatus", validStatus);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_trxForm;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getOutletId() {
      return (UUID) arguments.get("outletId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getMode() {
      return (String) arguments.get("mode");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getAppBookingId() {
      return (UUID) arguments.get("appBookingId");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getBookingId() {
      return (UUID) arguments.get("bookingId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public UUID getCompanyId() {
      return (UUID) arguments.get("companyId");
    }

    @SuppressWarnings("unchecked")
    public boolean getFromBookingStatus() {
      return (boolean) arguments.get("fromBookingStatus");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getConsumerName() {
      return (String) arguments.get("consumerName");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getAccName() {
      return (String) arguments.get("accName");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getAccNo() {
      return (String) arguments.get("accNo");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getBankId() {
      return (UUID) arguments.get("bankId");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getOtherBankName() {
      return (String) arguments.get("otherBankName");
    }

    @SuppressWarnings("unchecked")
    public boolean getValidStatus() {
      return (boolean) arguments.get("validStatus");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionTrxForm that = (ActionTrxForm) object;
      if (arguments.containsKey("outletId") != that.arguments.containsKey("outletId")) {
        return false;
      }
      if (getOutletId() != null ? !getOutletId().equals(that.getOutletId()) : that.getOutletId() != null) {
        return false;
      }
      if (arguments.containsKey("mode") != that.arguments.containsKey("mode")) {
        return false;
      }
      if (getMode() != null ? !getMode().equals(that.getMode()) : that.getMode() != null) {
        return false;
      }
      if (arguments.containsKey("appBookingId") != that.arguments.containsKey("appBookingId")) {
        return false;
      }
      if (getAppBookingId() != null ? !getAppBookingId().equals(that.getAppBookingId()) : that.getAppBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("bookingId") != that.arguments.containsKey("bookingId")) {
        return false;
      }
      if (getBookingId() != null ? !getBookingId().equals(that.getBookingId()) : that.getBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("companyId") != that.arguments.containsKey("companyId")) {
        return false;
      }
      if (getCompanyId() != null ? !getCompanyId().equals(that.getCompanyId()) : that.getCompanyId() != null) {
        return false;
      }
      if (arguments.containsKey("fromBookingStatus") != that.arguments.containsKey("fromBookingStatus")) {
        return false;
      }
      if (getFromBookingStatus() != that.getFromBookingStatus()) {
        return false;
      }
      if (arguments.containsKey("consumerName") != that.arguments.containsKey("consumerName")) {
        return false;
      }
      if (getConsumerName() != null ? !getConsumerName().equals(that.getConsumerName()) : that.getConsumerName() != null) {
        return false;
      }
      if (arguments.containsKey("accName") != that.arguments.containsKey("accName")) {
        return false;
      }
      if (getAccName() != null ? !getAccName().equals(that.getAccName()) : that.getAccName() != null) {
        return false;
      }
      if (arguments.containsKey("accNo") != that.arguments.containsKey("accNo")) {
        return false;
      }
      if (getAccNo() != null ? !getAccNo().equals(that.getAccNo()) : that.getAccNo() != null) {
        return false;
      }
      if (arguments.containsKey("bankId") != that.arguments.containsKey("bankId")) {
        return false;
      }
      if (getBankId() != null ? !getBankId().equals(that.getBankId()) : that.getBankId() != null) {
        return false;
      }
      if (arguments.containsKey("otherBankName") != that.arguments.containsKey("otherBankName")) {
        return false;
      }
      if (getOtherBankName() != null ? !getOtherBankName().equals(that.getOtherBankName()) : that.getOtherBankName() != null) {
        return false;
      }
      if (arguments.containsKey("validStatus") != that.arguments.containsKey("validStatus")) {
        return false;
      }
      if (getValidStatus() != that.getValidStatus()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getOutletId() != null ? getOutletId().hashCode() : 0);
      result = 31 * result + (getMode() != null ? getMode().hashCode() : 0);
      result = 31 * result + (getAppBookingId() != null ? getAppBookingId().hashCode() : 0);
      result = 31 * result + (getBookingId() != null ? getBookingId().hashCode() : 0);
      result = 31 * result + (getCompanyId() != null ? getCompanyId().hashCode() : 0);
      result = 31 * result + (getFromBookingStatus() ? 1 : 0);
      result = 31 * result + (getConsumerName() != null ? getConsumerName().hashCode() : 0);
      result = 31 * result + (getAccName() != null ? getAccName().hashCode() : 0);
      result = 31 * result + (getAccNo() != null ? getAccNo().hashCode() : 0);
      result = 31 * result + (getBankId() != null ? getBankId().hashCode() : 0);
      result = 31 * result + (getOtherBankName() != null ? getOtherBankName().hashCode() : 0);
      result = 31 * result + (getValidStatus() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionTrxForm(actionId=" + getActionId() + "){"
          + "outletId=" + getOutletId()
          + ", mode=" + getMode()
          + ", appBookingId=" + getAppBookingId()
          + ", bookingId=" + getBookingId()
          + ", companyId=" + getCompanyId()
          + ", fromBookingStatus=" + getFromBookingStatus()
          + ", consumerName=" + getConsumerName()
          + ", accName=" + getAccName()
          + ", accNo=" + getAccNo()
          + ", bankId=" + getBankId()
          + ", otherBankName=" + getOtherBankName()
          + ", validStatus=" + getValidStatus()
          + "}";
    }
  }

  public static class ActionTxnMode implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionTxnMode(@NonNull String appOperation, @NonNull String bookingType,
        @Nullable String txnMode, boolean isHasCreatePermission) {
      if (appOperation == null) {
        throw new IllegalArgumentException("Argument \"appOperation\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("appOperation", appOperation);
      if (bookingType == null) {
        throw new IllegalArgumentException("Argument \"bookingType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingType", bookingType);
      this.arguments.put("txnMode", txnMode);
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
    }

    @NonNull
    public ActionTxnMode setAppPBookingId(@Nullable UUID appPBookingId) {
      this.arguments.put("appPBookingId", appPBookingId);
      return this;
    }

    @NonNull
    public ActionTxnMode setAppOperation(@NonNull String appOperation) {
      if (appOperation == null) {
        throw new IllegalArgumentException("Argument \"appOperation\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("appOperation", appOperation);
      return this;
    }

    @NonNull
    public ActionTxnMode setBookingType(@NonNull String bookingType) {
      if (bookingType == null) {
        throw new IllegalArgumentException("Argument \"bookingType\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("bookingType", bookingType);
      return this;
    }

    @NonNull
    public ActionTxnMode setTxnMode(@Nullable String txnMode) {
      this.arguments.put("txnMode", txnMode);
      return this;
    }

    @NonNull
    public ActionTxnMode setIsHasCreatePermission(boolean isHasCreatePermission) {
      this.arguments.put("isHasCreatePermission", isHasCreatePermission);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("appPBookingId")) {
        UUID appPBookingId = (UUID) arguments.get("appPBookingId");
        if (Parcelable.class.isAssignableFrom(UUID.class) || appPBookingId == null) {
          __result.putParcelable("appPBookingId", Parcelable.class.cast(appPBookingId));
        } else if (Serializable.class.isAssignableFrom(UUID.class)) {
          __result.putSerializable("appPBookingId", Serializable.class.cast(appPBookingId));
        } else {
          throw new UnsupportedOperationException(UUID.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      } else {
        __result.putSerializable("appPBookingId", null);
      }
      if (arguments.containsKey("appOperation")) {
        String appOperation = (String) arguments.get("appOperation");
        __result.putString("appOperation", appOperation);
      }
      if (arguments.containsKey("bookingType")) {
        String bookingType = (String) arguments.get("bookingType");
        __result.putString("bookingType", bookingType);
      }
      if (arguments.containsKey("txnMode")) {
        String txnMode = (String) arguments.get("txnMode");
        __result.putString("txnMode", txnMode);
      }
      if (arguments.containsKey("isHasCreatePermission")) {
        boolean isHasCreatePermission = (boolean) arguments.get("isHasCreatePermission");
        __result.putBoolean("isHasCreatePermission", isHasCreatePermission);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_txn_mode;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public UUID getAppPBookingId() {
      return (UUID) arguments.get("appPBookingId");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getAppOperation() {
      return (String) arguments.get("appOperation");
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getBookingType() {
      return (String) arguments.get("bookingType");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTxnMode() {
      return (String) arguments.get("txnMode");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsHasCreatePermission() {
      return (boolean) arguments.get("isHasCreatePermission");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionTxnMode that = (ActionTxnMode) object;
      if (arguments.containsKey("appPBookingId") != that.arguments.containsKey("appPBookingId")) {
        return false;
      }
      if (getAppPBookingId() != null ? !getAppPBookingId().equals(that.getAppPBookingId()) : that.getAppPBookingId() != null) {
        return false;
      }
      if (arguments.containsKey("appOperation") != that.arguments.containsKey("appOperation")) {
        return false;
      }
      if (getAppOperation() != null ? !getAppOperation().equals(that.getAppOperation()) : that.getAppOperation() != null) {
        return false;
      }
      if (arguments.containsKey("bookingType") != that.arguments.containsKey("bookingType")) {
        return false;
      }
      if (getBookingType() != null ? !getBookingType().equals(that.getBookingType()) : that.getBookingType() != null) {
        return false;
      }
      if (arguments.containsKey("txnMode") != that.arguments.containsKey("txnMode")) {
        return false;
      }
      if (getTxnMode() != null ? !getTxnMode().equals(that.getTxnMode()) : that.getTxnMode() != null) {
        return false;
      }
      if (arguments.containsKey("isHasCreatePermission") != that.arguments.containsKey("isHasCreatePermission")) {
        return false;
      }
      if (getIsHasCreatePermission() != that.getIsHasCreatePermission()) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getAppPBookingId() != null ? getAppPBookingId().hashCode() : 0);
      result = 31 * result + (getAppOperation() != null ? getAppOperation().hashCode() : 0);
      result = 31 * result + (getBookingType() != null ? getBookingType().hashCode() : 0);
      result = 31 * result + (getTxnMode() != null ? getTxnMode().hashCode() : 0);
      result = 31 * result + (getIsHasCreatePermission() ? 1 : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionTxnMode(actionId=" + getActionId() + "){"
          + "appPBookingId=" + getAppPBookingId()
          + ", appOperation=" + getAppOperation()
          + ", bookingType=" + getBookingType()
          + ", txnMode=" + getTxnMode()
          + ", isHasCreatePermission=" + getIsHasCreatePermission()
          + "}";
    }
  }
}
