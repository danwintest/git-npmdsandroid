package id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class DroppingAdditionalFilterFragmentDirections {
  private DroppingAdditionalFilterFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
