package id.co.danwinciptaniaga.npmdsandroid.droppingday.sort;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import id.co.danwinciptaniaga.npmdsandroid.MobileNavigationDirections;

public class DroppingDailyBrowseSortFragmentDirections {
  private DroppingDailyBrowseSortFragmentDirections() {
  }

  @NonNull
  public static NavDirections navLogout() {
    return MobileNavigationDirections.navLogout();
  }
}
