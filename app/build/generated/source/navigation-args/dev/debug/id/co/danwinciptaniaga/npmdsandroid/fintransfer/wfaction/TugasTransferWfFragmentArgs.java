package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class TugasTransferWfFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private TugasTransferWfFragmentArgs() {
  }

  private TugasTransferWfFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static TugasTransferWfFragmentArgs fromBundle(@NonNull Bundle bundle) {
    TugasTransferWfFragmentArgs __result = new TugasTransferWfFragmentArgs();
    bundle.setClassLoader(TugasTransferWfFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("dataList")) {
      String dataList;
      dataList = bundle.getString("dataList");
      if (dataList == null) {
        throw new IllegalArgumentException("Argument \"dataList\" is marked as non-null but was passed a null value.");
      }
      __result.arguments.put("dataList", dataList);
    } else {
      throw new IllegalArgumentException("Required argument \"dataList\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("decision")) {
      String decision;
      decision = bundle.getString("decision");
      __result.arguments.put("decision", decision);
    } else {
      throw new IllegalArgumentException("Required argument \"decision\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("isAuto")) {
      boolean isAuto;
      isAuto = bundle.getBoolean("isAuto");
      __result.arguments.put("isAuto", isAuto);
    } else {
      throw new IllegalArgumentException("Required argument \"isAuto\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public String getDataList() {
    return (String) arguments.get("dataList");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getDecision() {
    return (String) arguments.get("decision");
  }

  @SuppressWarnings("unchecked")
  public boolean getIsAuto() {
    return (boolean) arguments.get("isAuto");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("dataList")) {
      String dataList = (String) arguments.get("dataList");
      __result.putString("dataList", dataList);
    }
    if (arguments.containsKey("decision")) {
      String decision = (String) arguments.get("decision");
      __result.putString("decision", decision);
    }
    if (arguments.containsKey("isAuto")) {
      boolean isAuto = (boolean) arguments.get("isAuto");
      __result.putBoolean("isAuto", isAuto);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    TugasTransferWfFragmentArgs that = (TugasTransferWfFragmentArgs) object;
    if (arguments.containsKey("dataList") != that.arguments.containsKey("dataList")) {
      return false;
    }
    if (getDataList() != null ? !getDataList().equals(that.getDataList()) : that.getDataList() != null) {
      return false;
    }
    if (arguments.containsKey("decision") != that.arguments.containsKey("decision")) {
      return false;
    }
    if (getDecision() != null ? !getDecision().equals(that.getDecision()) : that.getDecision() != null) {
      return false;
    }
    if (arguments.containsKey("isAuto") != that.arguments.containsKey("isAuto")) {
      return false;
    }
    if (getIsAuto() != that.getIsAuto()) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getDataList() != null ? getDataList().hashCode() : 0);
    result = 31 * result + (getDecision() != null ? getDecision().hashCode() : 0);
    result = 31 * result + (getIsAuto() ? 1 : 0);
    return result;
  }

  @Override
  public String toString() {
    return "TugasTransferWfFragmentArgs{"
        + "dataList=" + getDataList()
        + ", decision=" + getDecision()
        + ", isAuto=" + getIsAuto()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(TugasTransferWfFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@NonNull String dataList, @Nullable String decision, boolean isAuto) {
      if (dataList == null) {
        throw new IllegalArgumentException("Argument \"dataList\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("dataList", dataList);
      this.arguments.put("decision", decision);
      this.arguments.put("isAuto", isAuto);
    }

    @NonNull
    public TugasTransferWfFragmentArgs build() {
      TugasTransferWfFragmentArgs result = new TugasTransferWfFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setDataList(@NonNull String dataList) {
      if (dataList == null) {
        throw new IllegalArgumentException("Argument \"dataList\" is marked as non-null but was passed a null value.");
      }
      this.arguments.put("dataList", dataList);
      return this;
    }

    @NonNull
    public Builder setDecision(@Nullable String decision) {
      this.arguments.put("decision", decision);
      return this;
    }

    @NonNull
    public Builder setIsAuto(boolean isAuto) {
      this.arguments.put("isAuto", isAuto);
      return this;
    }

    @SuppressWarnings("unchecked")
    @NonNull
    public String getDataList() {
      return (String) arguments.get("dataList");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getDecision() {
      return (String) arguments.get("decision");
    }

    @SuppressWarnings("unchecked")
    public boolean getIsAuto() {
      return (boolean) arguments.get("isAuto");
    }
  }
}
