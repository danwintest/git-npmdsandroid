/**
 * Automatically generated file. DO NOT MODIFY
 */
package id.co.danwinciptaniaga.npmdsandroid.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "id.co.danwinciptaniaga.npmdsandroid.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "dev";
  public static final int VERSION_CODE = 10041;
  public static final String VERSION_NAME = "1.0.4.8";
  // Fields from build type: debug
  public static final boolean SHOW_EDIT_PAGE_MODE = false;
  public static final boolean SIMULATE_ANDROCON_INIT_DELAY = false;
  public static final boolean USE_CAMERA_STUB = true;
}
