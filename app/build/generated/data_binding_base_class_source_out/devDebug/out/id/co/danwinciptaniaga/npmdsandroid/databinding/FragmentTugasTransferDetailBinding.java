// Generated by view binder compiler. Do not edit!
package id.co.danwinciptaniaga.npmdsandroid.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.Barrier;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewbinding.ViewBinding;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentTugasTransferDetailBinding implements ViewBinding {
  @NonNull
  private final FrameLayout rootView;

  @NonNull
  public final Barrier barrier;

  @NonNull
  public final Barrier barrier2;

  @NonNull
  public final View divStatus;

  @NonNull
  public final RecyclerView listDetail;

  @NonNull
  public final ProgressViewBinding progressWrapper;

  @NonNull
  public final SwipeRefreshLayout swipeRefresh;

  @NonNull
  public final TextView tvBank;

  @NonNull
  public final TextView tvBankVal;

  @NonNull
  public final TextView tvBatch;

  @NonNull
  public final TextView tvBatchLabel;

  @NonNull
  public final TextView tvCompanyName;

  @NonNull
  public final TextView tvCompanyNameLabel;

  @NonNull
  public final TextView tvJenisTransaksi;

  @NonNull
  public final TextView tvJenisTransaksiLabel;

  @NonNull
  public final TextView tvJenisTransfer;

  @NonNull
  public final TextView tvJenisTransferLabel;

  @NonNull
  public final TextView tvPendingTask;

  @NonNull
  public final TextView tvPendingTaskLabel;

  @NonNull
  public final TextView tvRekeningSumber;

  @NonNull
  public final TextView tvRekeningSumberLabel;

  @NonNull
  public final TextView tvTransferStatus;

  @NonNull
  public final TextView tvTransferStatusLabel;

  private FragmentTugasTransferDetailBinding(@NonNull FrameLayout rootView,
      @NonNull Barrier barrier, @NonNull Barrier barrier2, @NonNull View divStatus,
      @NonNull RecyclerView listDetail, @NonNull ProgressViewBinding progressWrapper,
      @NonNull SwipeRefreshLayout swipeRefresh, @NonNull TextView tvBank,
      @NonNull TextView tvBankVal, @NonNull TextView tvBatch, @NonNull TextView tvBatchLabel,
      @NonNull TextView tvCompanyName, @NonNull TextView tvCompanyNameLabel,
      @NonNull TextView tvJenisTransaksi, @NonNull TextView tvJenisTransaksiLabel,
      @NonNull TextView tvJenisTransfer, @NonNull TextView tvJenisTransferLabel,
      @NonNull TextView tvPendingTask, @NonNull TextView tvPendingTaskLabel,
      @NonNull TextView tvRekeningSumber, @NonNull TextView tvRekeningSumberLabel,
      @NonNull TextView tvTransferStatus, @NonNull TextView tvTransferStatusLabel) {
    this.rootView = rootView;
    this.barrier = barrier;
    this.barrier2 = barrier2;
    this.divStatus = divStatus;
    this.listDetail = listDetail;
    this.progressWrapper = progressWrapper;
    this.swipeRefresh = swipeRefresh;
    this.tvBank = tvBank;
    this.tvBankVal = tvBankVal;
    this.tvBatch = tvBatch;
    this.tvBatchLabel = tvBatchLabel;
    this.tvCompanyName = tvCompanyName;
    this.tvCompanyNameLabel = tvCompanyNameLabel;
    this.tvJenisTransaksi = tvJenisTransaksi;
    this.tvJenisTransaksiLabel = tvJenisTransaksiLabel;
    this.tvJenisTransfer = tvJenisTransfer;
    this.tvJenisTransferLabel = tvJenisTransferLabel;
    this.tvPendingTask = tvPendingTask;
    this.tvPendingTaskLabel = tvPendingTaskLabel;
    this.tvRekeningSumber = tvRekeningSumber;
    this.tvRekeningSumberLabel = tvRekeningSumberLabel;
    this.tvTransferStatus = tvTransferStatus;
    this.tvTransferStatusLabel = tvTransferStatusLabel;
  }

  @Override
  @NonNull
  public FrameLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentTugasTransferDetailBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentTugasTransferDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_tugas_transfer_detail, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentTugasTransferDetailBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.barrier;
      Barrier barrier = rootView.findViewById(id);
      if (barrier == null) {
        break missingId;
      }

      id = R.id.barrier2;
      Barrier barrier2 = rootView.findViewById(id);
      if (barrier2 == null) {
        break missingId;
      }

      id = R.id.divStatus;
      View divStatus = rootView.findViewById(id);
      if (divStatus == null) {
        break missingId;
      }

      id = R.id.listDetail;
      RecyclerView listDetail = rootView.findViewById(id);
      if (listDetail == null) {
        break missingId;
      }

      id = R.id.progress_wrapper;
      View progressWrapper = rootView.findViewById(id);
      if (progressWrapper == null) {
        break missingId;
      }
      ProgressViewBinding binding_progressWrapper = ProgressViewBinding.bind(progressWrapper);

      id = R.id.swipeRefresh;
      SwipeRefreshLayout swipeRefresh = rootView.findViewById(id);
      if (swipeRefresh == null) {
        break missingId;
      }

      id = R.id.tvBank;
      TextView tvBank = rootView.findViewById(id);
      if (tvBank == null) {
        break missingId;
      }

      id = R.id.tvBankVal;
      TextView tvBankVal = rootView.findViewById(id);
      if (tvBankVal == null) {
        break missingId;
      }

      id = R.id.tvBatch;
      TextView tvBatch = rootView.findViewById(id);
      if (tvBatch == null) {
        break missingId;
      }

      id = R.id.tvBatchLabel;
      TextView tvBatchLabel = rootView.findViewById(id);
      if (tvBatchLabel == null) {
        break missingId;
      }

      id = R.id.tvCompanyName;
      TextView tvCompanyName = rootView.findViewById(id);
      if (tvCompanyName == null) {
        break missingId;
      }

      id = R.id.tvCompanyNameLabel;
      TextView tvCompanyNameLabel = rootView.findViewById(id);
      if (tvCompanyNameLabel == null) {
        break missingId;
      }

      id = R.id.tvJenisTransaksi;
      TextView tvJenisTransaksi = rootView.findViewById(id);
      if (tvJenisTransaksi == null) {
        break missingId;
      }

      id = R.id.tvJenisTransaksiLabel;
      TextView tvJenisTransaksiLabel = rootView.findViewById(id);
      if (tvJenisTransaksiLabel == null) {
        break missingId;
      }

      id = R.id.tvJenisTransfer;
      TextView tvJenisTransfer = rootView.findViewById(id);
      if (tvJenisTransfer == null) {
        break missingId;
      }

      id = R.id.tvJenisTransferLabel;
      TextView tvJenisTransferLabel = rootView.findViewById(id);
      if (tvJenisTransferLabel == null) {
        break missingId;
      }

      id = R.id.tvPendingTask;
      TextView tvPendingTask = rootView.findViewById(id);
      if (tvPendingTask == null) {
        break missingId;
      }

      id = R.id.tvPendingTaskLabel;
      TextView tvPendingTaskLabel = rootView.findViewById(id);
      if (tvPendingTaskLabel == null) {
        break missingId;
      }

      id = R.id.tvRekeningSumber;
      TextView tvRekeningSumber = rootView.findViewById(id);
      if (tvRekeningSumber == null) {
        break missingId;
      }

      id = R.id.tvRekeningSumberLabel;
      TextView tvRekeningSumberLabel = rootView.findViewById(id);
      if (tvRekeningSumberLabel == null) {
        break missingId;
      }

      id = R.id.tvTransferStatus;
      TextView tvTransferStatus = rootView.findViewById(id);
      if (tvTransferStatus == null) {
        break missingId;
      }

      id = R.id.tvTransferStatusLabel;
      TextView tvTransferStatusLabel = rootView.findViewById(id);
      if (tvTransferStatusLabel == null) {
        break missingId;
      }

      return new FragmentTugasTransferDetailBinding((FrameLayout) rootView, barrier, barrier2,
          divStatus, listDetail, binding_progressWrapper, swipeRefresh, tvBank, tvBankVal, tvBatch,
          tvBatchLabel, tvCompanyName, tvCompanyNameLabel, tvJenisTransaksi, tvJenisTransaksiLabel,
          tvJenisTransfer, tvJenisTransferLabel, tvPendingTask, tvPendingTaskLabel,
          tvRekeningSumber, tvRekeningSumberLabel, tvTransferStatus, tvTransferStatusLabel);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
