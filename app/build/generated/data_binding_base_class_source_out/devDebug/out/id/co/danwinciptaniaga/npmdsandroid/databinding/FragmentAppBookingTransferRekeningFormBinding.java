// Generated by view binder compiler. Do not edit!
package id.co.danwinciptaniaga.npmdsandroid.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.tiper.MaterialSpinner;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentAppBookingTransferRekeningFormBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final MaterialButton btnCheck;

  @NonNull
  public final Button btnOk;

  @NonNull
  public final MaterialButton btnPilihRekening;

  @NonNull
  public final EditText etAccountName;

  @NonNull
  public final EditText etAccountNo;

  @NonNull
  public final EditText etBankManual;

  @NonNull
  public final AppCompatImageView ivDate;

  @NonNull
  public final ConstraintLayout pageContent;

  @NonNull
  public final ProgressViewBinding progressWrapper;

  @NonNull
  public final MaterialSpinner spBank;

  @NonNull
  public final Switch swLainLain;

  @NonNull
  public final TextInputLayout tilAccountNo;

  @NonNull
  public final TextInputLayout tilAccoutnName;

  @NonNull
  public final TextInputLayout tilBank;

  @NonNull
  public final TextInputLayout tilBankManual;

  @NonNull
  public final TextView tvDate;

  @NonNull
  public final TextView tvLabelLainLain;

  private FragmentAppBookingTransferRekeningFormBinding(@NonNull ConstraintLayout rootView,
      @NonNull MaterialButton btnCheck, @NonNull Button btnOk,
      @NonNull MaterialButton btnPilihRekening, @NonNull EditText etAccountName,
      @NonNull EditText etAccountNo, @NonNull EditText etBankManual,
      @NonNull AppCompatImageView ivDate, @NonNull ConstraintLayout pageContent,
      @NonNull ProgressViewBinding progressWrapper, @NonNull MaterialSpinner spBank,
      @NonNull Switch swLainLain, @NonNull TextInputLayout tilAccountNo,
      @NonNull TextInputLayout tilAccoutnName, @NonNull TextInputLayout tilBank,
      @NonNull TextInputLayout tilBankManual, @NonNull TextView tvDate,
      @NonNull TextView tvLabelLainLain) {
    this.rootView = rootView;
    this.btnCheck = btnCheck;
    this.btnOk = btnOk;
    this.btnPilihRekening = btnPilihRekening;
    this.etAccountName = etAccountName;
    this.etAccountNo = etAccountNo;
    this.etBankManual = etBankManual;
    this.ivDate = ivDate;
    this.pageContent = pageContent;
    this.progressWrapper = progressWrapper;
    this.spBank = spBank;
    this.swLainLain = swLainLain;
    this.tilAccountNo = tilAccountNo;
    this.tilAccoutnName = tilAccoutnName;
    this.tilBank = tilBank;
    this.tilBankManual = tilBankManual;
    this.tvDate = tvDate;
    this.tvLabelLainLain = tvLabelLainLain;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentAppBookingTransferRekeningFormBinding inflate(
      @NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentAppBookingTransferRekeningFormBinding inflate(
      @NonNull LayoutInflater inflater, @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_app_booking_transfer_rekening_form, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentAppBookingTransferRekeningFormBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.btnCheck;
      MaterialButton btnCheck = rootView.findViewById(id);
      if (btnCheck == null) {
        break missingId;
      }

      id = R.id.btnOk;
      Button btnOk = rootView.findViewById(id);
      if (btnOk == null) {
        break missingId;
      }

      id = R.id.btnPilihRekening;
      MaterialButton btnPilihRekening = rootView.findViewById(id);
      if (btnPilihRekening == null) {
        break missingId;
      }

      id = R.id.etAccountName;
      EditText etAccountName = rootView.findViewById(id);
      if (etAccountName == null) {
        break missingId;
      }

      id = R.id.etAccountNo;
      EditText etAccountNo = rootView.findViewById(id);
      if (etAccountNo == null) {
        break missingId;
      }

      id = R.id.etBankManual;
      EditText etBankManual = rootView.findViewById(id);
      if (etBankManual == null) {
        break missingId;
      }

      id = R.id.ivDate;
      AppCompatImageView ivDate = rootView.findViewById(id);
      if (ivDate == null) {
        break missingId;
      }

      id = R.id.pageContent;
      ConstraintLayout pageContent = rootView.findViewById(id);
      if (pageContent == null) {
        break missingId;
      }

      id = R.id.progress_wrapper;
      View progressWrapper = rootView.findViewById(id);
      if (progressWrapper == null) {
        break missingId;
      }
      ProgressViewBinding binding_progressWrapper = ProgressViewBinding.bind(progressWrapper);

      id = R.id.spBank;
      MaterialSpinner spBank = rootView.findViewById(id);
      if (spBank == null) {
        break missingId;
      }

      id = R.id.swLainLain;
      Switch swLainLain = rootView.findViewById(id);
      if (swLainLain == null) {
        break missingId;
      }

      id = R.id.tilAccountNo;
      TextInputLayout tilAccountNo = rootView.findViewById(id);
      if (tilAccountNo == null) {
        break missingId;
      }

      id = R.id.tilAccoutnName;
      TextInputLayout tilAccoutnName = rootView.findViewById(id);
      if (tilAccoutnName == null) {
        break missingId;
      }

      id = R.id.tilBank;
      TextInputLayout tilBank = rootView.findViewById(id);
      if (tilBank == null) {
        break missingId;
      }

      id = R.id.tilBankManual;
      TextInputLayout tilBankManual = rootView.findViewById(id);
      if (tilBankManual == null) {
        break missingId;
      }

      id = R.id.tvDate;
      TextView tvDate = rootView.findViewById(id);
      if (tvDate == null) {
        break missingId;
      }

      id = R.id.tvLabelLainLain;
      TextView tvLabelLainLain = rootView.findViewById(id);
      if (tvLabelLainLain == null) {
        break missingId;
      }

      return new FragmentAppBookingTransferRekeningFormBinding((ConstraintLayout) rootView,
          btnCheck, btnOk, btnPilihRekening, etAccountName, etAccountNo, etBankManual, ivDate,
          pageContent, binding_progressWrapper, spBank, swLainLain, tilAccountNo, tilAccoutnName,
          tilBank, tilBankManual, tvDate, tvLabelLainLain);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
