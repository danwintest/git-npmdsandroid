// Generated by view binder compiler. Do not edit!
package id.co.danwinciptaniaga.npmdsandroid.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentExpenseItemBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final ConstraintLayout clExpContainer;

  @NonNull
  public final View divStatus;

  @NonNull
  public final FrameLayout flExpImg;

  @NonNull
  public final ImageView ivExpImg;

  @NonNull
  public final ConstraintLayout rlExpenseItem;

  @NonNull
  public final TextView tvExpAdditionalInfo;

  @NonNull
  public final TextView tvExpAmount;

  @NonNull
  public final TextView tvExpItemNoTrx;

  @NonNull
  public final TextView tvExpOutletAndCode;

  @NonNull
  public final TextView tvExpTrxAndActDate;

  @NonNull
  public final TextView tvExpType;

  @NonNull
  public final TextView tvExpWfStatus;

  private FragmentExpenseItemBinding(@NonNull ConstraintLayout rootView,
      @NonNull ConstraintLayout clExpContainer, @NonNull View divStatus,
      @NonNull FrameLayout flExpImg, @NonNull ImageView ivExpImg,
      @NonNull ConstraintLayout rlExpenseItem, @NonNull TextView tvExpAdditionalInfo,
      @NonNull TextView tvExpAmount, @NonNull TextView tvExpItemNoTrx,
      @NonNull TextView tvExpOutletAndCode, @NonNull TextView tvExpTrxAndActDate,
      @NonNull TextView tvExpType, @NonNull TextView tvExpWfStatus) {
    this.rootView = rootView;
    this.clExpContainer = clExpContainer;
    this.divStatus = divStatus;
    this.flExpImg = flExpImg;
    this.ivExpImg = ivExpImg;
    this.rlExpenseItem = rlExpenseItem;
    this.tvExpAdditionalInfo = tvExpAdditionalInfo;
    this.tvExpAmount = tvExpAmount;
    this.tvExpItemNoTrx = tvExpItemNoTrx;
    this.tvExpOutletAndCode = tvExpOutletAndCode;
    this.tvExpTrxAndActDate = tvExpTrxAndActDate;
    this.tvExpType = tvExpType;
    this.tvExpWfStatus = tvExpWfStatus;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentExpenseItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentExpenseItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_expense_item, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentExpenseItemBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.clExpContainer;
      ConstraintLayout clExpContainer = rootView.findViewById(id);
      if (clExpContainer == null) {
        break missingId;
      }

      id = R.id.divStatus;
      View divStatus = rootView.findViewById(id);
      if (divStatus == null) {
        break missingId;
      }

      id = R.id.flExpImg;
      FrameLayout flExpImg = rootView.findViewById(id);
      if (flExpImg == null) {
        break missingId;
      }

      id = R.id.ivExpImg;
      ImageView ivExpImg = rootView.findViewById(id);
      if (ivExpImg == null) {
        break missingId;
      }

      ConstraintLayout rlExpenseItem = (ConstraintLayout) rootView;

      id = R.id.tvExpAdditionalInfo;
      TextView tvExpAdditionalInfo = rootView.findViewById(id);
      if (tvExpAdditionalInfo == null) {
        break missingId;
      }

      id = R.id.tvExpAmount;
      TextView tvExpAmount = rootView.findViewById(id);
      if (tvExpAmount == null) {
        break missingId;
      }

      id = R.id.tvExpItemNoTrx;
      TextView tvExpItemNoTrx = rootView.findViewById(id);
      if (tvExpItemNoTrx == null) {
        break missingId;
      }

      id = R.id.tvExpOutletAndCode;
      TextView tvExpOutletAndCode = rootView.findViewById(id);
      if (tvExpOutletAndCode == null) {
        break missingId;
      }

      id = R.id.tvExpTrxAndActDate;
      TextView tvExpTrxAndActDate = rootView.findViewById(id);
      if (tvExpTrxAndActDate == null) {
        break missingId;
      }

      id = R.id.tvExpType;
      TextView tvExpType = rootView.findViewById(id);
      if (tvExpType == null) {
        break missingId;
      }

      id = R.id.tvExpWfStatus;
      TextView tvExpWfStatus = rootView.findViewById(id);
      if (tvExpWfStatus == null) {
        break missingId;
      }

      return new FragmentExpenseItemBinding((ConstraintLayout) rootView, clExpContainer, divStatus,
          flExpImg, ivExpImg, rlExpenseItem, tvExpAdditionalInfo, tvExpAmount, tvExpItemNoTrx,
          tvExpOutletAndCode, tvExpTrxAndActDate, tvExpType, tvExpWfStatus);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
