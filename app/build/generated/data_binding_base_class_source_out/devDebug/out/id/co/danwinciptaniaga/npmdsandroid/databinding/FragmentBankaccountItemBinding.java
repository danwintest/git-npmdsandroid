// Generated by view binder compiler. Do not edit!
package id.co.danwinciptaniaga.npmdsandroid.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentBankaccountItemBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final ConstraintLayout clAppBookingContainer;

  @NonNull
  public final ConstraintLayout clContainer;

  @NonNull
  public final View divAppBookingStatus;

  @NonNull
  public final ImageView icValidate;

  @NonNull
  public final TextView tvAccountName;

  @NonNull
  public final TextView tvAccountNo;

  @NonNull
  public final TextView tvBank;

  @NonNull
  public final TextView tvValidateDate;

  private FragmentBankaccountItemBinding(@NonNull ConstraintLayout rootView,
      @NonNull ConstraintLayout clAppBookingContainer, @NonNull ConstraintLayout clContainer,
      @NonNull View divAppBookingStatus, @NonNull ImageView icValidate,
      @NonNull TextView tvAccountName, @NonNull TextView tvAccountNo, @NonNull TextView tvBank,
      @NonNull TextView tvValidateDate) {
    this.rootView = rootView;
    this.clAppBookingContainer = clAppBookingContainer;
    this.clContainer = clContainer;
    this.divAppBookingStatus = divAppBookingStatus;
    this.icValidate = icValidate;
    this.tvAccountName = tvAccountName;
    this.tvAccountNo = tvAccountNo;
    this.tvBank = tvBank;
    this.tvValidateDate = tvValidateDate;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentBankaccountItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentBankaccountItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_bankaccount_item, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentBankaccountItemBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      ConstraintLayout clAppBookingContainer = (ConstraintLayout) rootView;

      id = R.id.clContainer;
      ConstraintLayout clContainer = rootView.findViewById(id);
      if (clContainer == null) {
        break missingId;
      }

      id = R.id.divAppBookingStatus;
      View divAppBookingStatus = rootView.findViewById(id);
      if (divAppBookingStatus == null) {
        break missingId;
      }

      id = R.id.icValidate;
      ImageView icValidate = rootView.findViewById(id);
      if (icValidate == null) {
        break missingId;
      }

      id = R.id.tvAccountName;
      TextView tvAccountName = rootView.findViewById(id);
      if (tvAccountName == null) {
        break missingId;
      }

      id = R.id.tvAccountNo;
      TextView tvAccountNo = rootView.findViewById(id);
      if (tvAccountNo == null) {
        break missingId;
      }

      id = R.id.tvBank;
      TextView tvBank = rootView.findViewById(id);
      if (tvBank == null) {
        break missingId;
      }

      id = R.id.tvValidateDate;
      TextView tvValidateDate = rootView.findViewById(id);
      if (tvValidateDate == null) {
        break missingId;
      }

      return new FragmentBankaccountItemBinding((ConstraintLayout) rootView, clAppBookingContainer,
          clContainer, divAppBookingStatus, icValidate, tvAccountName, tvAccountNo, tvBank,
          tvValidateDate);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
