// Generated by view binder compiler. Do not edit!
package id.co.danwinciptaniaga.npmdsandroid.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentDroppingAdditionalDetailEditBinding implements ViewBinding {
  @NonNull
  private final ScrollView rootView;

  @NonNull
  public final Button btnSaveDraft;

  @NonNull
  public final TextInputEditText etAmount;

  @NonNull
  public final TextInputEditText etRemarks;

  @NonNull
  public final ImageView ivAttachment;

  @NonNull
  public final ConstraintLayout pageContent;

  @NonNull
  public final TextInputLayout tilAmount;

  @NonNull
  public final TextInputLayout tilRemarks;

  @NonNull
  public final TextView tvAttachment;

  private FragmentDroppingAdditionalDetailEditBinding(@NonNull ScrollView rootView,
      @NonNull Button btnSaveDraft, @NonNull TextInputEditText etAmount,
      @NonNull TextInputEditText etRemarks, @NonNull ImageView ivAttachment,
      @NonNull ConstraintLayout pageContent, @NonNull TextInputLayout tilAmount,
      @NonNull TextInputLayout tilRemarks, @NonNull TextView tvAttachment) {
    this.rootView = rootView;
    this.btnSaveDraft = btnSaveDraft;
    this.etAmount = etAmount;
    this.etRemarks = etRemarks;
    this.ivAttachment = ivAttachment;
    this.pageContent = pageContent;
    this.tilAmount = tilAmount;
    this.tilRemarks = tilRemarks;
    this.tvAttachment = tvAttachment;
  }

  @Override
  @NonNull
  public ScrollView getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentDroppingAdditionalDetailEditBinding inflate(
      @NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentDroppingAdditionalDetailEditBinding inflate(
      @NonNull LayoutInflater inflater, @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_dropping_additional_detail_edit, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentDroppingAdditionalDetailEditBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.btnSaveDraft;
      Button btnSaveDraft = rootView.findViewById(id);
      if (btnSaveDraft == null) {
        break missingId;
      }

      id = R.id.etAmount;
      TextInputEditText etAmount = rootView.findViewById(id);
      if (etAmount == null) {
        break missingId;
      }

      id = R.id.etRemarks;
      TextInputEditText etRemarks = rootView.findViewById(id);
      if (etRemarks == null) {
        break missingId;
      }

      id = R.id.ivAttachment;
      ImageView ivAttachment = rootView.findViewById(id);
      if (ivAttachment == null) {
        break missingId;
      }

      id = R.id.page_content;
      ConstraintLayout pageContent = rootView.findViewById(id);
      if (pageContent == null) {
        break missingId;
      }

      id = R.id.tilAmount;
      TextInputLayout tilAmount = rootView.findViewById(id);
      if (tilAmount == null) {
        break missingId;
      }

      id = R.id.tilRemarks;
      TextInputLayout tilRemarks = rootView.findViewById(id);
      if (tilRemarks == null) {
        break missingId;
      }

      id = R.id.tvAttachment;
      TextView tvAttachment = rootView.findViewById(id);
      if (tvAttachment == null) {
        break missingId;
      }

      return new FragmentDroppingAdditionalDetailEditBinding((ScrollView) rootView, btnSaveDraft,
          etAmount, etRemarks, ivAttachment, pageContent, tilAmount, tilRemarks, tvAttachment);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
