// Generated by view binder compiler. Do not edit!
package id.co.danwinciptaniaga.npmdsandroid.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewbinding.ViewBinding;
import id.co.danwinciptaniaga.npmdsandroid.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentSubstituteUserBinding implements ViewBinding {
  @NonNull
  private final FrameLayout rootView;

  @NonNull
  public final RecyclerView list;

  @NonNull
  public final ConstraintLayout pageContent;

  @NonNull
  public final View pageHeaderSeparator;

  @NonNull
  public final ProgressViewBinding progressWrapper;

  @NonNull
  public final SwipeRefreshLayout swipeRefresh;

  @NonNull
  public final TextView tvUser;

  private FragmentSubstituteUserBinding(@NonNull FrameLayout rootView, @NonNull RecyclerView list,
      @NonNull ConstraintLayout pageContent, @NonNull View pageHeaderSeparator,
      @NonNull ProgressViewBinding progressWrapper, @NonNull SwipeRefreshLayout swipeRefresh,
      @NonNull TextView tvUser) {
    this.rootView = rootView;
    this.list = list;
    this.pageContent = pageContent;
    this.pageHeaderSeparator = pageHeaderSeparator;
    this.progressWrapper = progressWrapper;
    this.swipeRefresh = swipeRefresh;
    this.tvUser = tvUser;
  }

  @Override
  @NonNull
  public FrameLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentSubstituteUserBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentSubstituteUserBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_substitute_user, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentSubstituteUserBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.list;
      RecyclerView list = rootView.findViewById(id);
      if (list == null) {
        break missingId;
      }

      id = R.id.page_content;
      ConstraintLayout pageContent = rootView.findViewById(id);
      if (pageContent == null) {
        break missingId;
      }

      id = R.id.page_header_separator;
      View pageHeaderSeparator = rootView.findViewById(id);
      if (pageHeaderSeparator == null) {
        break missingId;
      }

      id = R.id.progress_wrapper;
      View progressWrapper = rootView.findViewById(id);
      if (progressWrapper == null) {
        break missingId;
      }
      ProgressViewBinding binding_progressWrapper = ProgressViewBinding.bind(progressWrapper);

      id = R.id.swipeRefresh;
      SwipeRefreshLayout swipeRefresh = rootView.findViewById(id);
      if (swipeRefresh == null) {
        break missingId;
      }

      id = R.id.tvUser;
      TextView tvUser = rootView.findViewById(id);
      if (tvUser == null) {
        break missingId;
      }

      return new FragmentSubstituteUserBinding((FrameLayout) rootView, list, pageContent,
          pageHeaderSeparator, binding_progressWrapper, swipeRefresh, tvUser);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
