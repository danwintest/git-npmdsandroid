package dagger.hilt.internal.processedrootsentinel.codegen;

import dagger.hilt.internal.processedrootsentinel.ProcessedRootSentinel;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@ProcessedRootSentinel(
    roots = "id.co.danwinciptaniaga.npmdsandroid.SimpleTest"
)
public class _id_co_danwinciptaniaga_npmdsandroid_SimpleTest {
}
