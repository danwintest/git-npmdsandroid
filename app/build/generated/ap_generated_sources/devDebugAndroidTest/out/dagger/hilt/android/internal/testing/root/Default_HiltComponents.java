package dagger.hilt.android.internal.testing.root;

import androidx.hilt.lifecycle.ViewModelFactoryModules;
import dagger.Binds;
import dagger.Component;
import dagger.Module;
import dagger.Subcomponent;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.android.components.ServiceComponent;
import dagger.hilt.android.components.ViewComponent;
import dagger.hilt.android.components.ViewModelComponent;
import dagger.hilt.android.components.ViewWithFragmentComponent;
import dagger.hilt.android.flags.FragmentGetContextFix;
import dagger.hilt.android.flags.HiltWrapper_FragmentGetContextFix_FragmentGetContextFixModule;
import dagger.hilt.android.internal.builders.ActivityComponentBuilder;
import dagger.hilt.android.internal.builders.ActivityRetainedComponentBuilder;
import dagger.hilt.android.internal.builders.FragmentComponentBuilder;
import dagger.hilt.android.internal.builders.ServiceComponentBuilder;
import dagger.hilt.android.internal.builders.ViewComponentBuilder;
import dagger.hilt.android.internal.builders.ViewModelComponentBuilder;
import dagger.hilt.android.internal.builders.ViewWithFragmentComponentBuilder;
import dagger.hilt.android.internal.lifecycle.DefaultViewModelFactories;
import dagger.hilt.android.internal.lifecycle.HiltViewModelFactory;
import dagger.hilt.android.internal.lifecycle.HiltWrapper_DefaultViewModelFactories_ActivityModule;
import dagger.hilt.android.internal.lifecycle.HiltWrapper_HiltViewModelFactory_ActivityCreatorEntryPoint;
import dagger.hilt.android.internal.lifecycle.HiltWrapper_HiltViewModelFactory_ViewModelModule;
import dagger.hilt.android.internal.managers.ActivityComponentManager;
import dagger.hilt.android.internal.managers.FragmentComponentManager;
import dagger.hilt.android.internal.managers.HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedComponentBuilderEntryPoint;
import dagger.hilt.android.internal.managers.HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedLifecycleEntryPoint;
import dagger.hilt.android.internal.managers.HiltWrapper_ActivityRetainedComponentManager_LifecycleModule;
import dagger.hilt.android.internal.managers.ServiceComponentManager;
import dagger.hilt.android.internal.managers.ViewComponentManager;
import dagger.hilt.android.internal.modules.ApplicationContextModule;
import dagger.hilt.android.internal.modules.HiltWrapper_ActivityModule;
import dagger.hilt.android.scopes.ActivityRetainedScoped;
import dagger.hilt.android.scopes.ActivityScoped;
import dagger.hilt.android.scopes.FragmentScoped;
import dagger.hilt.android.scopes.ServiceScoped;
import dagger.hilt.android.scopes.ViewModelScoped;
import dagger.hilt.android.scopes.ViewScoped;
import dagger.hilt.components.SingletonComponent;
import dagger.hilt.internal.GeneratedComponent;
import dagger.hilt.internal.TestSingletonComponent;
import dagger.hilt.migration.DisableInstallInCheck;
import id.co.danwinciptaniaga.androcon.DownloadReceiver_GeneratedInjector;
import id.co.danwinciptaniaga.androcon.di.AndroconModule;
import id.co.danwinciptaniaga.androcon.glide.HiltWrapper_AndroconGlideModule_AndroconGlideEntryPoint;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceViewModel_HiltModules;
import id.co.danwinciptaniaga.androcon.security.ProtectedActivityViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.App_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.LaunchActivity_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.SimpleTest_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.TestAppBooking_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.auth.NpmdsAuthenticatorService_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingFilterVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash.AppBookingCashVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningFormVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningForm_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningListFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.BankAccountBrowseVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailTransferVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.DroppingAdditionalFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.DroppingAdditionalFilterVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.DroppingDailyFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.DroppingDailyFilterVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail.DroppingDailyTaskDetailBrowseVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseSortFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseEditFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseEditViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.SelectBookingFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.SelectBookingViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailBrowseVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferBrowseHeaderFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferHeaderBrowseVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.notif.NpmdsNotificationService_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceBrowseVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockByDayReportFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockByDayReportFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockReportByDayVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.BankAcctFormFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.BankAcctFormVM_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.DrawerViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.LoginViewModel_HiltModules;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfHistoryFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfHistoryVM_HiltModules;
import javax.inject.Singleton;

public final class Default_HiltComponents {
  private Default_HiltComponents() {
  }

  @Module(
      subcomponents = ServiceC.class
  )
  @DisableInstallInCheck
  abstract interface ServiceCBuilderModule {
    @Binds
    ServiceComponentBuilder bind(ServiceC.Builder builder);
  }

  @Module(
      subcomponents = ActivityRetainedC.class
  )
  @DisableInstallInCheck
  abstract interface ActivityRetainedCBuilderModule {
    @Binds
    ActivityRetainedComponentBuilder bind(ActivityRetainedC.Builder builder);
  }

  @Module(
      subcomponents = ActivityC.class
  )
  @DisableInstallInCheck
  abstract interface ActivityCBuilderModule {
    @Binds
    ActivityComponentBuilder bind(ActivityC.Builder builder);
  }

  @Module(
      subcomponents = ViewModelC.class
  )
  @DisableInstallInCheck
  abstract interface ViewModelCBuilderModule {
    @Binds
    ViewModelComponentBuilder bind(ViewModelC.Builder builder);
  }

  @Module(
      subcomponents = ViewC.class
  )
  @DisableInstallInCheck
  abstract interface ViewCBuilderModule {
    @Binds
    ViewComponentBuilder bind(ViewC.Builder builder);
  }

  @Module(
      subcomponents = FragmentC.class
  )
  @DisableInstallInCheck
  abstract interface FragmentCBuilderModule {
    @Binds
    FragmentComponentBuilder bind(FragmentC.Builder builder);
  }

  @Module(
      subcomponents = ViewWithFragmentC.class
  )
  @DisableInstallInCheck
  abstract interface ViewWithFragmentCBuilderModule {
    @Binds
    ViewWithFragmentComponentBuilder bind(ViewWithFragmentC.Builder builder);
  }

  @Component(
      modules = {
          AndroconModule.class,
          ApplicationContextModule.class,
          ActivityRetainedCBuilderModule.class,
          ServiceCBuilderModule.class,
          HiltWrapper_FragmentGetContextFix_FragmentGetContextFixModule.class,
          NpmdsModule.class
      }
  )
  @Singleton
  public abstract static class SingletonC implements FragmentGetContextFix.FragmentGetContextFixEntryPoint,
      HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedComponentBuilderEntryPoint,
      ServiceComponentManager.ServiceComponentBuilderEntryPoint,
      SingletonComponent,
      TestSingletonComponent,
      DownloadReceiver_GeneratedInjector,
      HiltWrapper_AndroconGlideModule_AndroconGlideEntryPoint,
      App_GeneratedInjector,
      SimpleTest_GeneratedInjector,
      TestAppBooking_GeneratedInjector {
  }

  @Subcomponent
  @ServiceScoped
  public abstract static class ServiceC implements ServiceComponent,
      GeneratedComponent,
      NpmdsAuthenticatorService_GeneratedInjector,
      NpmdsNotificationService_GeneratedInjector {
    @Subcomponent.Builder
    abstract interface Builder extends ServiceComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          AppBookingBrowseVM_HiltModules.KeyModule.class,
          AppBookingCashVM_HiltModules.KeyModule.class,
          AppBookingFilterVM_HiltModules.KeyModule.class,
          AppBookingMultiActionVM_HiltModules.KeyModule.class,
          AppBookingTransferRekeningFormVM_HiltModules.KeyModule.class,
          AppBookingTransferVM_HiltModules.KeyModule.class,
          BankAccountBrowseVM_HiltModules.KeyModule.class,
          BankAcctFormVM_HiltModules.KeyModule.class,
          BookingBrowseViewModel_HiltModules.KeyModule.class,
          BookingDetailTransferVM_HiltModules.KeyModule.class,
          BookingDetailVM_HiltModules.KeyModule.class,
          BookingFilterVM_HiltModules.KeyModule.class,
          ChangePasswordVM_HiltModules.KeyModule.class,
          ActivityCBuilderModule.class,
          ViewModelCBuilderModule.class,
          DrawerViewModel_HiltModules.KeyModule.class,
          DroppingAdditionalBrowseViewModel_HiltModules.KeyModule.class,
          DroppingAdditionalDetailEditViewModel_HiltModules.KeyModule.class,
          DroppingAdditionalEditViewModel_HiltModules.KeyModule.class,
          DroppingAdditionalFilterVM_HiltModules.KeyModule.class,
          DroppingDailyBrowseVM_HiltModules.KeyModule.class,
          DroppingDailyEditViewModel_HiltModules.KeyModule.class,
          DroppingDailyFilterVM_HiltModules.KeyModule.class,
          DroppingDailyTaskBrowseVM_HiltModules.KeyModule.class,
          DroppingDailyTaskDetailBrowseVM_HiltModules.KeyModule.class,
          DroppingDailyTaskFilterVM_HiltModules.KeyModule.class,
          DroppingDailyTaskWfVM_HiltModules.KeyModule.class,
          ExpenseBrowseViewModel_HiltModules.KeyModule.class,
          ExpenseEditViewModel_HiltModules.KeyModule.class,
          ExpenseFilterVM_HiltModules.KeyModule.class,
          HiltWrapper_ActivityRetainedComponentManager_LifecycleModule.class,
          LoginViewModel_HiltModules.KeyModule.class,
          OutletBalanceBrowseVM_HiltModules.KeyModule.class,
          OutletBalanceReportVM_HiltModules.KeyModule.class,
          OutletStockReportByDayVM_HiltModules.KeyModule.class,
          PreferenceViewModel_HiltModules.KeyModule.class,
          ProtectedActivityViewModel_HiltModules.KeyModule.class,
          RegisterDeviceViewModel_HiltModules.KeyModule.class,
          SelectBookingViewModel_HiltModules.KeyModule.class,
          TugasTransferCekSaldoVM_HiltModules.KeyModule.class,
          TugasTransferDetailBrowseVM_HiltModules.KeyModule.class,
          TugasTransferFilterVM_HiltModules.KeyModule.class,
          TugasTransferHeaderBrowseVM_HiltModules.KeyModule.class,
          TugasTransferWfVM_HiltModules.KeyModule.class,
          WfHistoryVM_HiltModules.KeyModule.class
      }
  )
  @ActivityRetainedScoped
  public abstract static class ActivityRetainedC implements ActivityRetainedComponent,
      ActivityComponentManager.ActivityComponentBuilderEntryPoint,
      HiltWrapper_ActivityRetainedComponentManager_ActivityRetainedLifecycleEntryPoint,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ActivityRetainedComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          FragmentCBuilderModule.class,
          ViewCBuilderModule.class,
          HiltWrapper_ActivityModule.class,
          HiltWrapper_DefaultViewModelFactories_ActivityModule.class,
          ViewModelFactoryModules.ActivityModule.class
      }
  )
  @ActivityScoped
  public abstract static class ActivityC implements ActivityComponent,
      DefaultViewModelFactories.ActivityEntryPoint,
      HiltWrapper_HiltViewModelFactory_ActivityCreatorEntryPoint,
      FragmentComponentManager.FragmentComponentBuilderEntryPoint,
      ViewComponentManager.ViewComponentBuilderEntryPoint,
      GeneratedComponent,
      LaunchActivity_GeneratedInjector,
      LandingActivity_GeneratedInjector,
      AuthenticationActivity_GeneratedInjector {
    @Subcomponent.Builder
    abstract interface Builder extends ActivityComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          AppBookingBrowseVM_HiltModules.BindsModule.class,
          AppBookingCashVM_HiltModules.BindsModule.class,
          AppBookingFilterVM_HiltModules.BindsModule.class,
          AppBookingMultiActionVM_HiltModules.BindsModule.class,
          AppBookingTransferRekeningFormVM_HiltModules.BindsModule.class,
          AppBookingTransferVM_HiltModules.BindsModule.class,
          BankAccountBrowseVM_HiltModules.BindsModule.class,
          BankAcctFormVM_HiltModules.BindsModule.class,
          BookingBrowseViewModel_HiltModules.BindsModule.class,
          BookingDetailTransferVM_HiltModules.BindsModule.class,
          BookingDetailVM_HiltModules.BindsModule.class,
          BookingFilterVM_HiltModules.BindsModule.class,
          ChangePasswordVM_HiltModules.BindsModule.class,
          DrawerViewModel_HiltModules.BindsModule.class,
          DroppingAdditionalBrowseViewModel_HiltModules.BindsModule.class,
          DroppingAdditionalDetailEditViewModel_HiltModules.BindsModule.class,
          DroppingAdditionalEditViewModel_HiltModules.BindsModule.class,
          DroppingAdditionalFilterVM_HiltModules.BindsModule.class,
          DroppingDailyBrowseVM_HiltModules.BindsModule.class,
          DroppingDailyEditViewModel_HiltModules.BindsModule.class,
          DroppingDailyFilterVM_HiltModules.BindsModule.class,
          DroppingDailyTaskBrowseVM_HiltModules.BindsModule.class,
          DroppingDailyTaskDetailBrowseVM_HiltModules.BindsModule.class,
          DroppingDailyTaskFilterVM_HiltModules.BindsModule.class,
          DroppingDailyTaskWfVM_HiltModules.BindsModule.class,
          ExpenseBrowseViewModel_HiltModules.BindsModule.class,
          ExpenseEditViewModel_HiltModules.BindsModule.class,
          ExpenseFilterVM_HiltModules.BindsModule.class,
          HiltWrapper_HiltViewModelFactory_ViewModelModule.class,
          LoginViewModel_HiltModules.BindsModule.class,
          OutletBalanceBrowseVM_HiltModules.BindsModule.class,
          OutletBalanceReportVM_HiltModules.BindsModule.class,
          OutletStockReportByDayVM_HiltModules.BindsModule.class,
          PreferenceViewModel_HiltModules.BindsModule.class,
          ProtectedActivityViewModel_HiltModules.BindsModule.class,
          RegisterDeviceViewModel_HiltModules.BindsModule.class,
          SelectBookingViewModel_HiltModules.BindsModule.class,
          TugasTransferCekSaldoVM_HiltModules.BindsModule.class,
          TugasTransferDetailBrowseVM_HiltModules.BindsModule.class,
          TugasTransferFilterVM_HiltModules.BindsModule.class,
          TugasTransferHeaderBrowseVM_HiltModules.BindsModule.class,
          TugasTransferWfVM_HiltModules.BindsModule.class,
          WfHistoryVM_HiltModules.BindsModule.class
      }
  )
  @ViewModelScoped
  public abstract static class ViewModelC implements ViewModelComponent,
      HiltViewModelFactory.ViewModelFactoriesEntryPoint,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewModelComponentBuilder {
    }
  }

  @Subcomponent
  @ViewScoped
  public abstract static class ViewC implements ViewComponent,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          ViewWithFragmentCBuilderModule.class,
          ViewModelFactoryModules.FragmentModule.class
      }
  )
  @FragmentScoped
  public abstract static class FragmentC implements FragmentComponent,
      DefaultViewModelFactories.FragmentEntryPoint,
      ViewComponentManager.ViewWithFragmentComponentBuilderEntryPoint,
      GeneratedComponent,
      BookingBrowseFragment_GeneratedInjector,
      BookingFilterFragment_GeneratedInjector,
      Abstract_AppBookingFragment_GeneratedInjector,
      AppBookingBrowseFragment_GeneratedInjector,
      AppBookingFilterFragment_GeneratedInjector,
      AppBookingTransferRekeningForm_GeneratedInjector,
      AppBookingTransferRekeningListFragment_GeneratedInjector,
      AppBookingMultiActionFragment_GeneratedInjector,
      DroppingAdditionalBrowseFragment_GeneratedInjector,
      DroppingAdditionalDetailEditFragment_GeneratedInjector,
      DroppingAdditionalEditFragment_GeneratedInjector,
      DroppingAdditionalFilterFragment_GeneratedInjector,
      DroppingDailyBrowseFragment_GeneratedInjector,
      DroppingDailyEditFragment_GeneratedInjector,
      DroppingDailyFilterFragment_GeneratedInjector,
      DroppingDailyTaskFilterFragment_GeneratedInjector,
      DroppingDailyTaskBrowseFragment_GeneratedInjector,
      DroppingDailyTaskWfFragment_GeneratedInjector,
      ExpenseBrowseFragment_GeneratedInjector,
      ExpenseFilterFragment_GeneratedInjector,
      ExpenseSortFragment_GeneratedInjector,
      ExpenseEditFragment_GeneratedInjector,
      SelectBookingFragment_GeneratedInjector,
      TugasTransferCekSaldoFragment_GeneratedInjector,
      TugasTransferDetailFragment_GeneratedInjector,
      TugasTransferFilterFragment_GeneratedInjector,
      TugasTransferBrowseHeaderFragment_GeneratedInjector,
      TugasTransferWfFragment_GeneratedInjector,
      OutletBalanceReportFilterFragment_GeneratedInjector,
      OutletBalanceReportFragment_GeneratedInjector,
      OutletStockByDayReportFilterFragment_GeneratedInjector,
      OutletStockByDayReportFragment_GeneratedInjector,
      ChangePasswordFragment_GeneratedInjector,
      PreferenceFragment_GeneratedInjector,
      BankAcctFormFragment_GeneratedInjector,
      WfHistoryFragment_GeneratedInjector {
    @Subcomponent.Builder
    abstract interface Builder extends FragmentComponentBuilder {
    }
  }

  @Subcomponent
  @ViewScoped
  public abstract static class ViewWithFragmentC implements ViewWithFragmentComponent,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewWithFragmentComponentBuilder {
    }
  }
}
