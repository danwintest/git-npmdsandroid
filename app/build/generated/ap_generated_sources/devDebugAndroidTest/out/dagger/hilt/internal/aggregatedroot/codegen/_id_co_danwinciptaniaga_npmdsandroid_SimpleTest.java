package dagger.hilt.internal.aggregatedroot.codegen;

import dagger.hilt.android.testing.HiltAndroidTest;
import dagger.hilt.internal.aggregatedroot.AggregatedRoot;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedRoot(
    root = "id.co.danwinciptaniaga.npmdsandroid.SimpleTest",
    originatingRoot = "id.co.danwinciptaniaga.npmdsandroid.SimpleTest",
    rootAnnotation = HiltAndroidTest.class
)
public class _id_co_danwinciptaniaga_npmdsandroid_SimpleTest {
}
