package dagger.hilt.android.internal.testing;

import androidx.test.core.app.ApplicationProvider;
import dagger.hilt.android.internal.Contexts;
import dagger.hilt.android.internal.modules.ApplicationContextModule;
import dagger.hilt.android.internal.testing.root.DaggerDefault_HiltComponents_SingletonC;
import java.lang.Object;

class EarlySingletonComponentCreatorImpl extends EarlySingletonComponentCreator {
  Object create() {
    return DaggerDefault_HiltComponents_SingletonC.builder()
        .applicationContextModule(
            new ApplicationContextModule(Contexts.getApplication(ApplicationProvider.getApplicationContext())))
        .build();
  }
}
