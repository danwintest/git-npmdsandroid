package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.components.SingletonComponent",
    test = "id.co.danwinciptaniaga.npmdsandroid.TestAppBooking",
    entryPoints = "id.co.danwinciptaniaga.npmdsandroid.TestAppBooking_GeneratedInjector"
)
public class _id_co_danwinciptaniaga_npmdsandroid_TestAppBooking_GeneratedInjector {
}
