// Generated by Dagger (https://dagger.dev).
package id.co.danwinciptaniaga.npmdsandroid;

import android.app.Application;
import dagger.MembersInjector;
import dagger.internal.DaggerGenerated;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.AuthService;
import id.co.danwinciptaniaga.androcon.auth.AuthorizationHeaderProvider;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCaseSync;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingListAction;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingLoadUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingProcessUC;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TestAppBooking_MembersInjector implements MembersInjector<TestAppBooking> {
  private final Provider<Application> appProvider;

  private final Provider<AndroconConfig> androconConfigProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<LoginUtil> loginUtilProvider;

  private final Provider<AuthorizationHeaderProvider> ahpProvider;

  private final Provider<AuthService> authServiceProvider;

  private final Provider<GetTokenUseCaseSync> ucGetTokenSyncProvider;

  private final Provider<AppBookingProcessUC> appBookingProcessUCProvider;

  private final Provider<AppBookingLoadUC> appBookingLoadUCProvider;

  private final Provider<AppBookingListAction> appBookingListActionProvider;

  public TestAppBooking_MembersInjector(Provider<Application> appProvider,
      Provider<AndroconConfig> androconConfigProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<LoginUtil> loginUtilProvider, Provider<AuthorizationHeaderProvider> ahpProvider,
      Provider<AuthService> authServiceProvider,
      Provider<GetTokenUseCaseSync> ucGetTokenSyncProvider,
      Provider<AppBookingProcessUC> appBookingProcessUCProvider,
      Provider<AppBookingLoadUC> appBookingLoadUCProvider,
      Provider<AppBookingListAction> appBookingListActionProvider) {
    this.appProvider = appProvider;
    this.androconConfigProvider = androconConfigProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.loginUtilProvider = loginUtilProvider;
    this.ahpProvider = ahpProvider;
    this.authServiceProvider = authServiceProvider;
    this.ucGetTokenSyncProvider = ucGetTokenSyncProvider;
    this.appBookingProcessUCProvider = appBookingProcessUCProvider;
    this.appBookingLoadUCProvider = appBookingLoadUCProvider;
    this.appBookingListActionProvider = appBookingListActionProvider;
  }

  public static MembersInjector<TestAppBooking> create(Provider<Application> appProvider,
      Provider<AndroconConfig> androconConfigProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<LoginUtil> loginUtilProvider, Provider<AuthorizationHeaderProvider> ahpProvider,
      Provider<AuthService> authServiceProvider,
      Provider<GetTokenUseCaseSync> ucGetTokenSyncProvider,
      Provider<AppBookingProcessUC> appBookingProcessUCProvider,
      Provider<AppBookingLoadUC> appBookingLoadUCProvider,
      Provider<AppBookingListAction> appBookingListActionProvider) {
    return new TestAppBooking_MembersInjector(appProvider, androconConfigProvider, appExecutorsProvider, loginUtilProvider, ahpProvider, authServiceProvider, ucGetTokenSyncProvider, appBookingProcessUCProvider, appBookingLoadUCProvider, appBookingListActionProvider);
  }

  @Override
  public void injectMembers(TestAppBooking instance) {
    injectApp(instance, appProvider.get());
    injectAndroconConfig(instance, androconConfigProvider.get());
    injectAppExecutors(instance, appExecutorsProvider.get());
    injectLoginUtil(instance, loginUtilProvider.get());
    injectAhp(instance, ahpProvider.get());
    injectAuthService(instance, authServiceProvider.get());
    injectUcGetTokenSync(instance, ucGetTokenSyncProvider.get());
    injectAppBookingProcessUC(instance, appBookingProcessUCProvider.get());
    injectAppBookingLoadUC(instance, appBookingLoadUCProvider.get());
    injectAppBookingListAction(instance, appBookingListActionProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.TestAppBooking.app")
  public static void injectApp(TestAppBooking instance, Application app) {
    instance.app = app;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.TestAppBooking.androconConfig")
  public static void injectAndroconConfig(TestAppBooking instance, AndroconConfig androconConfig) {
    instance.androconConfig = androconConfig;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.TestAppBooking.appExecutors")
  public static void injectAppExecutors(TestAppBooking instance, AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.TestAppBooking.loginUtil")
  public static void injectLoginUtil(TestAppBooking instance, LoginUtil loginUtil) {
    instance.loginUtil = loginUtil;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.TestAppBooking.ahp")
  public static void injectAhp(TestAppBooking instance, AuthorizationHeaderProvider ahp) {
    instance.ahp = ahp;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.TestAppBooking.authService")
  public static void injectAuthService(TestAppBooking instance, AuthService authService) {
    instance.authService = authService;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.TestAppBooking.ucGetTokenSync")
  public static void injectUcGetTokenSync(TestAppBooking instance,
      GetTokenUseCaseSync ucGetTokenSync) {
    instance.ucGetTokenSync = ucGetTokenSync;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.TestAppBooking.appBookingProcessUC")
  public static void injectAppBookingProcessUC(TestAppBooking instance,
      AppBookingProcessUC appBookingProcessUC) {
    instance.appBookingProcessUC = appBookingProcessUC;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.TestAppBooking.appBookingLoadUC")
  public static void injectAppBookingLoadUC(TestAppBooking instance,
      AppBookingLoadUC appBookingLoadUC) {
    instance.appBookingLoadUC = appBookingLoadUC;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.TestAppBooking.appBookingListAction")
  public static void injectAppBookingListAction(TestAppBooking instance,
      AppBookingListAction appBookingListAction) {
    instance.appBookingListAction = appBookingListAction;
  }
}
