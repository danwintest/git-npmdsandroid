package id.co.danwinciptaniaga.npmdsandroid;

import dagger.hilt.InstallIn;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.components.SingletonComponent;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = SimpleTest.class
)
@GeneratedEntryPoint
@InstallIn(SingletonComponent.class)
public interface SimpleTest_GeneratedInjector {
  void injectTest(SimpleTest simpleTest);
}
