package id.co.danwinciptaniaga.npmdsandroid;

import dagger.hilt.InstallIn;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.components.SingletonComponent;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = TestAppBooking.class
)
@GeneratedEntryPoint
@InstallIn(SingletonComponent.class)
public interface TestAppBooking_GeneratedInjector {
  void injectTest(TestAppBooking testAppBooking);
}
