package id.co.danwinciptaniaga.npmdsandroid;

import android.content.Context;
import dagger.hilt.android.internal.testing.TestApplicationComponentManager;
import dagger.hilt.android.internal.testing.TestApplicationComponentManagerHolder;
import dagger.hilt.internal.GeneratedComponentManager;
import java.lang.Object;
import java.lang.Override;

public final class CustomTestApplicationTest_Application extends AppTest implements GeneratedComponentManager<Object>, TestApplicationComponentManagerHolder {
  private TestApplicationComponentManager componentManager;

  @Override
  protected final void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    componentManager = new TestApplicationComponentManager(this);
  }

  @Override
  public final GeneratedComponentManager<Object> componentManager() {
    return componentManager;
  }

  @Override
  public final Object generatedComponent() {
    return componentManager.generatedComponent();
  }
}
