package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingMultiActionUC_Factory implements Factory<AppBookingMultiActionUC> {
  private final Provider<Application> appProvider;

  private final Provider<AppBookingService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public AppBookingMultiActionUC_Factory(Provider<Application> appProvider,
      Provider<AppBookingService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public AppBookingMultiActionUC get() {
    return newInstance(appProvider.get(), serviceProvider.get(), appExecutorsProvider.get());
  }

  public static AppBookingMultiActionUC_Factory create(Provider<Application> appProvider,
      Provider<AppBookingService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new AppBookingMultiActionUC_Factory(appProvider, serviceProvider, appExecutorsProvider);
  }

  public static AppBookingMultiActionUC newInstance(Application app, AppBookingService service,
      AppExecutors appExecutors) {
    return new AppBookingMultiActionUC(app, service, appExecutors);
  }
}
