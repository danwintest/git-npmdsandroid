package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingAdditionalEditViewModel_AssistedFactory_Factory implements Factory<DroppingAdditionalEditViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<PrepareDroppingAdditionalUseCase> prepareDroppingAdditionalUseCaseProvider;

  private final Provider<DroppingAdditionalSaveUseCase> droppingAdditionalSaveUseCaseProvider;

  private final Provider<LoadDroppingAdditionalUseCase> loadDroppingAdditionalUseCaseProvider;

  private final Provider<DroppingAdditionalSupervisorDecideUseCase> droppingAdditionalSupervisorDecideUseCaseProvider;

  private final Provider<UtilityService> utilityServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingAdditionalEditViewModel_AssistedFactory_Factory(
      Provider<Application> applicationProvider,
      Provider<PrepareDroppingAdditionalUseCase> prepareDroppingAdditionalUseCaseProvider,
      Provider<DroppingAdditionalSaveUseCase> droppingAdditionalSaveUseCaseProvider,
      Provider<LoadDroppingAdditionalUseCase> loadDroppingAdditionalUseCaseProvider,
      Provider<DroppingAdditionalSupervisorDecideUseCase> droppingAdditionalSupervisorDecideUseCaseProvider,
      Provider<UtilityService> utilityServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.prepareDroppingAdditionalUseCaseProvider = prepareDroppingAdditionalUseCaseProvider;
    this.droppingAdditionalSaveUseCaseProvider = droppingAdditionalSaveUseCaseProvider;
    this.loadDroppingAdditionalUseCaseProvider = loadDroppingAdditionalUseCaseProvider;
    this.droppingAdditionalSupervisorDecideUseCaseProvider = droppingAdditionalSupervisorDecideUseCaseProvider;
    this.utilityServiceProvider = utilityServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingAdditionalEditViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, prepareDroppingAdditionalUseCaseProvider, droppingAdditionalSaveUseCaseProvider, loadDroppingAdditionalUseCaseProvider, droppingAdditionalSupervisorDecideUseCaseProvider, utilityServiceProvider, appExecutorsProvider);
  }

  public static DroppingAdditionalEditViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<PrepareDroppingAdditionalUseCase> prepareDroppingAdditionalUseCaseProvider,
      Provider<DroppingAdditionalSaveUseCase> droppingAdditionalSaveUseCaseProvider,
      Provider<LoadDroppingAdditionalUseCase> loadDroppingAdditionalUseCaseProvider,
      Provider<DroppingAdditionalSupervisorDecideUseCase> droppingAdditionalSupervisorDecideUseCaseProvider,
      Provider<UtilityService> utilityServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingAdditionalEditViewModel_AssistedFactory_Factory(applicationProvider, prepareDroppingAdditionalUseCaseProvider, droppingAdditionalSaveUseCaseProvider, loadDroppingAdditionalUseCaseProvider, droppingAdditionalSupervisorDecideUseCaseProvider, utilityServiceProvider, appExecutorsProvider);
  }

  public static DroppingAdditionalEditViewModel_AssistedFactory newInstance(
      Provider<Application> application,
      Provider<PrepareDroppingAdditionalUseCase> prepareDroppingAdditionalUseCase,
      Provider<DroppingAdditionalSaveUseCase> droppingAdditionalSaveUseCase,
      Provider<LoadDroppingAdditionalUseCase> loadDroppingAdditionalUseCase,
      Provider<DroppingAdditionalSupervisorDecideUseCase> droppingAdditionalSupervisorDecideUseCase,
      Provider<UtilityService> utilityService, Provider<AppExecutors> appExecutors) {
    return new DroppingAdditionalEditViewModel_AssistedFactory(application, prepareDroppingAdditionalUseCase, droppingAdditionalSaveUseCase, loadDroppingAdditionalUseCase, droppingAdditionalSupervisorDecideUseCase, utilityService, appExecutors);
  }
}
