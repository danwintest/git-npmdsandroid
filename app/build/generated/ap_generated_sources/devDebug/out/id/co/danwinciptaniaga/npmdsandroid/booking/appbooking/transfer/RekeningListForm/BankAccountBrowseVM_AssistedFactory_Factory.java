package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class BankAccountBrowseVM_AssistedFactory_Factory implements Factory<BankAccountBrowseVM_AssistedFactory> {
  private final Provider<Application> appProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<BankAccountBrowseUC> loadUCProvider;

  public BankAccountBrowseVM_AssistedFactory_Factory(Provider<Application> appProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<BankAccountBrowseUC> loadUCProvider) {
    this.appProvider = appProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.loadUCProvider = loadUCProvider;
  }

  @Override
  public BankAccountBrowseVM_AssistedFactory get() {
    return newInstance(appProvider, appExecutorsProvider, loadUCProvider);
  }

  public static BankAccountBrowseVM_AssistedFactory_Factory create(
      Provider<Application> appProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<BankAccountBrowseUC> loadUCProvider) {
    return new BankAccountBrowseVM_AssistedFactory_Factory(appProvider, appExecutorsProvider, loadUCProvider);
  }

  public static BankAccountBrowseVM_AssistedFactory newInstance(Provider<Application> app,
      Provider<AppExecutors> appExecutors, Provider<BankAccountBrowseUC> loadUC) {
    return new BankAccountBrowseVM_AssistedFactory(app, appExecutors, loadUC);
  }
}
