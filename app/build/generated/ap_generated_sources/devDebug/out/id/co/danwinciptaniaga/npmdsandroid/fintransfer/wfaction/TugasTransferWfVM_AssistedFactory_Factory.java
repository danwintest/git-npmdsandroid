package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TugasTransferWfVM_AssistedFactory_Factory implements Factory<TugasTransferWfVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<TugasTransferWfFormUC> ucFormProvider;

  private final Provider<TugasTransferWfProcessUC> ucProcessProvider;

  private final Provider<TugasTransferWFGenerateOtpUC> ucGenerateOtpUCProvider;

  public TugasTransferWfVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<TugasTransferWfFormUC> ucFormProvider,
      Provider<TugasTransferWfProcessUC> ucProcessProvider,
      Provider<TugasTransferWFGenerateOtpUC> ucGenerateOtpUCProvider) {
    this.applicationProvider = applicationProvider;
    this.ucFormProvider = ucFormProvider;
    this.ucProcessProvider = ucProcessProvider;
    this.ucGenerateOtpUCProvider = ucGenerateOtpUCProvider;
  }

  @Override
  public TugasTransferWfVM_AssistedFactory get() {
    return newInstance(applicationProvider, ucFormProvider, ucProcessProvider, ucGenerateOtpUCProvider);
  }

  public static TugasTransferWfVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<TugasTransferWfFormUC> ucFormProvider,
      Provider<TugasTransferWfProcessUC> ucProcessProvider,
      Provider<TugasTransferWFGenerateOtpUC> ucGenerateOtpUCProvider) {
    return new TugasTransferWfVM_AssistedFactory_Factory(applicationProvider, ucFormProvider, ucProcessProvider, ucGenerateOtpUCProvider);
  }

  public static TugasTransferWfVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<TugasTransferWfFormUC> ucForm, Provider<TugasTransferWfProcessUC> ucProcess,
      Provider<TugasTransferWFGenerateOtpUC> ucGenerateOtpUC) {
    return new TugasTransferWfVM_AssistedFactory(application, ucForm, ucProcess, ucGenerateOtpUC);
  }
}
