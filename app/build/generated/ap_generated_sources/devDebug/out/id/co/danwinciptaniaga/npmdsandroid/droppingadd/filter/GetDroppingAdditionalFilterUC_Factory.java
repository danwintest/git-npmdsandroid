package id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetDroppingAdditionalFilterUC_Factory implements Factory<GetDroppingAdditionalFilterUC> {
  private final Provider<Application> appProvider;

  private final Provider<DroppingAdditionalService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetDroppingAdditionalFilterUC_Factory(Provider<Application> appProvider,
      Provider<DroppingAdditionalService> serviceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetDroppingAdditionalFilterUC get() {
    return newInstance(appProvider.get(), serviceProvider.get(), appExecutorsProvider.get());
  }

  public static GetDroppingAdditionalFilterUC_Factory create(Provider<Application> appProvider,
      Provider<DroppingAdditionalService> serviceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new GetDroppingAdditionalFilterUC_Factory(appProvider, serviceProvider, appExecutorsProvider);
  }

  public static GetDroppingAdditionalFilterUC newInstance(Application app,
      DroppingAdditionalService service, AppExecutors appExecutors) {
    return new GetDroppingAdditionalFilterUC(app, service, appExecutors);
  }
}
