package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetOutletBalanceReportUseCase_Factory implements Factory<GetOutletBalanceReportUseCase> {
  private final Provider<Application> applicationProvider;

  private final Provider<OutletReportService> outletReportServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetOutletBalanceReportUseCase_Factory(Provider<Application> applicationProvider,
      Provider<OutletReportService> outletReportServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.outletReportServiceProvider = outletReportServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetOutletBalanceReportUseCase get() {
    return newInstance(applicationProvider.get(), outletReportServiceProvider.get(), appExecutorsProvider.get());
  }

  public static GetOutletBalanceReportUseCase_Factory create(
      Provider<Application> applicationProvider,
      Provider<OutletReportService> outletReportServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new GetOutletBalanceReportUseCase_Factory(applicationProvider, outletReportServiceProvider, appExecutorsProvider);
  }

  public static GetOutletBalanceReportUseCase newInstance(Application application,
      OutletReportService outletReportService, AppExecutors appExecutors) {
    return new GetOutletBalanceReportUseCase(application, outletReportService, appExecutors);
  }
}
