package id.co.danwinciptaniaga.npmdsandroid.exp;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ExpenseFilterUC_Factory implements Factory<ExpenseFilterUC> {
  private final Provider<Application> appProvider;

  private final Provider<ExpenseService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public ExpenseFilterUC_Factory(Provider<Application> appProvider,
      Provider<ExpenseService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public ExpenseFilterUC get() {
    return newInstance(appProvider.get(), serviceProvider.get(), appExecutorsProvider.get());
  }

  public static ExpenseFilterUC_Factory create(Provider<Application> appProvider,
      Provider<ExpenseService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new ExpenseFilterUC_Factory(appProvider, serviceProvider, appExecutorsProvider);
  }

  public static ExpenseFilterUC newInstance(Application app, ExpenseService service,
      AppExecutors appExecutors) {
    return new ExpenseFilterUC(app, service, appExecutors);
  }
}
