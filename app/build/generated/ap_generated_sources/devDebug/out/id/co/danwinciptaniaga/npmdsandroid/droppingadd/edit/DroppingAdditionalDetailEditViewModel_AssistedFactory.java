package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingAdditionalDetailEditViewModel_AssistedFactory implements ViewModelAssistedFactory<DroppingAdditionalDetailEditViewModel> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  @Inject
  DroppingAdditionalDetailEditViewModel_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors) {
    this.application = application;
    this.appExecutors = appExecutors;
  }

  @Override
  @NonNull
  public DroppingAdditionalDetailEditViewModel create(@NonNull SavedStateHandle arg0) {
    return new DroppingAdditionalDetailEditViewModel(application.get(), appExecutors.get());
  }
}
