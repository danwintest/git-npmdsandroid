package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingAdditionalSupervisorDecideUseCase_Factory implements Factory<DroppingAdditionalSupervisorDecideUseCase> {
  private final Provider<Application> applicationProvider;

  private final Provider<DroppingAdditionalService> daServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingAdditionalSupervisorDecideUseCase_Factory(
      Provider<Application> applicationProvider,
      Provider<DroppingAdditionalService> daServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.daServiceProvider = daServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingAdditionalSupervisorDecideUseCase get() {
    return newInstance(applicationProvider.get(), daServiceProvider.get(), appExecutorsProvider.get());
  }

  public static DroppingAdditionalSupervisorDecideUseCase_Factory create(
      Provider<Application> applicationProvider,
      Provider<DroppingAdditionalService> daServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingAdditionalSupervisorDecideUseCase_Factory(applicationProvider, daServiceProvider, appExecutorsProvider);
  }

  public static DroppingAdditionalSupervisorDecideUseCase newInstance(Application application,
      DroppingAdditionalService daService, AppExecutors appExecutors) {
    return new DroppingAdditionalSupervisorDecideUseCase(application, daService, appExecutors);
  }
}
