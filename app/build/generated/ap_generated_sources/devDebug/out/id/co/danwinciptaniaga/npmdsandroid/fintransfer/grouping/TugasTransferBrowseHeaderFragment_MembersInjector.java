package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TugasTransferBrowseHeaderFragment_MembersInjector implements MembersInjector<TugasTransferBrowseHeaderFragment> {
  private final Provider<AppExecutors> appExecutorsProvider;

  public TugasTransferBrowseHeaderFragment_MembersInjector(
      Provider<AppExecutors> appExecutorsProvider) {
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<TugasTransferBrowseHeaderFragment> create(
      Provider<AppExecutors> appExecutorsProvider) {
    return new TugasTransferBrowseHeaderFragment_MembersInjector(appExecutorsProvider);
  }

  @Override
  public void injectMembers(TugasTransferBrowseHeaderFragment instance) {
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferBrowseHeaderFragment.appExecutors")
  public static void injectAppExecutors(TugasTransferBrowseHeaderFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
