package id.co.danwinciptaniaga.npmdsandroid.ui.landing;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.npmdsandroid.common.GetExtUserInfoUseCase;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DrawerViewModel_AssistedFactory implements ViewModelAssistedFactory<DrawerViewModel> {
  private final Provider<Application> application;

  private final Provider<GetExtUserInfoUseCase> getUserInfoUseCase;

  @Inject
  DrawerViewModel_AssistedFactory(Provider<Application> application,
      Provider<GetExtUserInfoUseCase> getUserInfoUseCase) {
    this.application = application;
    this.getUserInfoUseCase = getUserInfoUseCase;
  }

  @Override
  @NonNull
  public DrawerViewModel create(@NonNull SavedStateHandle arg0) {
    return new DrawerViewModel(application.get(), getUserInfoUseCase.get());
  }
}
