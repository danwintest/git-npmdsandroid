package id.co.danwinciptaniaga.npmdsandroid.ui.landing;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.security.ProtectedActivity_MembersInjector;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.BaseActivity_MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LandingActivity_MembersInjector implements MembersInjector<LandingActivity> {
  private final Provider<LoginUtil> loginUtilProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<AndroconConfig> androconConfigProvider;

  private final Provider<LoginUtil> loginUtilProvider2;

  public LandingActivity_MembersInjector(Provider<LoginUtil> loginUtilProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<AndroconConfig> androconConfigProvider,
      Provider<LoginUtil> loginUtilProvider2) {
    this.loginUtilProvider = loginUtilProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.androconConfigProvider = androconConfigProvider;
    this.loginUtilProvider2 = loginUtilProvider2;
  }

  public static MembersInjector<LandingActivity> create(Provider<LoginUtil> loginUtilProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<AndroconConfig> androconConfigProvider,
      Provider<LoginUtil> loginUtilProvider2) {
    return new LandingActivity_MembersInjector(loginUtilProvider, appExecutorsProvider, androconConfigProvider, loginUtilProvider2);
  }

  @Override
  public void injectMembers(LandingActivity instance) {
    ProtectedActivity_MembersInjector.injectLoginUtil(instance, loginUtilProvider.get());
    ProtectedActivity_MembersInjector.injectAppExecutors(instance, appExecutorsProvider.get());
    BaseActivity_MembersInjector.injectAndroconConfig(instance, androconConfigProvider.get());
    injectLoginUtil(instance, loginUtilProvider2.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity.loginUtil")
  public static void injectLoginUtil(LandingActivity instance, LoginUtil loginUtil) {
    instance.loginUtil = loginUtil;
  }
}
