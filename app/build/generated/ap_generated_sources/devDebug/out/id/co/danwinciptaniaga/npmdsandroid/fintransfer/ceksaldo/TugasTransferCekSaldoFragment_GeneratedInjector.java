package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = TugasTransferCekSaldoFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface TugasTransferCekSaldoFragment_GeneratedInjector {
  void injectTugasTransferCekSaldoFragment(
      TugasTransferCekSaldoFragment tugasTransferCekSaldoFragment);
}
