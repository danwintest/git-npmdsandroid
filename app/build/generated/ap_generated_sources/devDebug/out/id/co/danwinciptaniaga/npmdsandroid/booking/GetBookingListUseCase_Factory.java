package id.co.danwinciptaniaga.npmdsandroid.booking;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetBookingListUseCase_Factory implements Factory<GetBookingListUseCase> {
  private final Provider<BookingService> bookingServiceProvider;

  public GetBookingListUseCase_Factory(Provider<BookingService> bookingServiceProvider) {
    this.bookingServiceProvider = bookingServiceProvider;
  }

  @Override
  public GetBookingListUseCase get() {
    return newInstance(bookingServiceProvider.get());
  }

  public static GetBookingListUseCase_Factory create(
      Provider<BookingService> bookingServiceProvider) {
    return new GetBookingListUseCase_Factory(bookingServiceProvider);
  }

  public static GetBookingListUseCase newInstance(BookingService bookingService) {
    return new GetBookingListUseCase(bookingService);
  }
}
