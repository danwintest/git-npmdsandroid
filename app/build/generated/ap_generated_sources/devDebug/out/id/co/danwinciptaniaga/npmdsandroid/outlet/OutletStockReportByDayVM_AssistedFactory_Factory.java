package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class OutletStockReportByDayVM_AssistedFactory_Factory implements Factory<OutletStockReportByDayVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<GetOutletStockByDayReportUC> getOutletStockByDayReportUCProvider;

  public OutletStockReportByDayVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<GetOutletStockByDayReportUC> getOutletStockByDayReportUCProvider) {
    this.applicationProvider = applicationProvider;
    this.getOutletStockByDayReportUCProvider = getOutletStockByDayReportUCProvider;
  }

  @Override
  public OutletStockReportByDayVM_AssistedFactory get() {
    return newInstance(applicationProvider, getOutletStockByDayReportUCProvider);
  }

  public static OutletStockReportByDayVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<GetOutletStockByDayReportUC> getOutletStockByDayReportUCProvider) {
    return new OutletStockReportByDayVM_AssistedFactory_Factory(applicationProvider, getOutletStockByDayReportUCProvider);
  }

  public static OutletStockReportByDayVM_AssistedFactory newInstance(
      Provider<Application> application,
      Provider<GetOutletStockByDayReportUC> getOutletStockByDayReportUC) {
    return new OutletStockReportByDayVM_AssistedFactory(application, getOutletStockByDayReportUC);
  }
}
