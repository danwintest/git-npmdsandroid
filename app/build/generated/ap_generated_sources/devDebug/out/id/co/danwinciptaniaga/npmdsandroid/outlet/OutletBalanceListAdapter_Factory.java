package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.content.Context;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class OutletBalanceListAdapter_Factory implements Factory<OutletBalanceListAdapter> {
  private final Provider<Context> contextProvider;

  private final Provider<OutletBalanceListAdapter.Listener> listenerProvider;

  public OutletBalanceListAdapter_Factory(Provider<Context> contextProvider,
      Provider<OutletBalanceListAdapter.Listener> listenerProvider) {
    this.contextProvider = contextProvider;
    this.listenerProvider = listenerProvider;
  }

  @Override
  public OutletBalanceListAdapter get() {
    return newInstance(contextProvider.get(), listenerProvider.get());
  }

  public static OutletBalanceListAdapter_Factory create(Provider<Context> contextProvider,
      Provider<OutletBalanceListAdapter.Listener> listenerProvider) {
    return new OutletBalanceListAdapter_Factory(contextProvider, listenerProvider);
  }

  public static OutletBalanceListAdapter newInstance(Context context, Object listener) {
    return new OutletBalanceListAdapter(context, (OutletBalanceListAdapter.Listener) listener);
  }
}
