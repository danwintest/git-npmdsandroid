package id.co.danwinciptaniaga.npmdsandroid.prefs;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class ChangePasswordVM_AssistedFactory implements ViewModelAssistedFactory<ChangePasswordVM> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<ChangePasswordUC> changePasswordUC;

  @Inject
  ChangePasswordVM_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<ChangePasswordUC> changePasswordUC) {
    this.application = application;
    this.appExecutors = appExecutors;
    this.changePasswordUC = changePasswordUC;
  }

  @Override
  @NonNull
  public ChangePasswordVM create(@NonNull SavedStateHandle arg0) {
    return new ChangePasswordVM(application.get(), appExecutors.get(), changePasswordUC.get());
  }
}
