package id.co.danwinciptaniaga.npmdsandroid.droppingday.filter;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = DroppingDailyFilterFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface DroppingDailyFilterFragment_GeneratedInjector {
  void injectDroppingDailyFilterFragment(DroppingDailyFilterFragment droppingDailyFilterFragment);
}
