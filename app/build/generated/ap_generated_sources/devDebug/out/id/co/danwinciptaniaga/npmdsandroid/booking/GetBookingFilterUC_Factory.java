package id.co.danwinciptaniaga.npmdsandroid.booking;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetBookingFilterUC_Factory implements Factory<GetBookingFilterUC> {
  private final Provider<Application> appProvider;

  private final Provider<BookingService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetBookingFilterUC_Factory(Provider<Application> appProvider,
      Provider<BookingService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetBookingFilterUC get() {
    return newInstance(appProvider.get(), serviceProvider.get(), appExecutorsProvider.get());
  }

  public static GetBookingFilterUC_Factory create(Provider<Application> appProvider,
      Provider<BookingService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new GetBookingFilterUC_Factory(appProvider, serviceProvider, appExecutorsProvider);
  }

  public static GetBookingFilterUC newInstance(Application app, BookingService service,
      AppExecutors appExecutors) {
    return new GetBookingFilterUC(app, service, appExecutors);
  }
}
