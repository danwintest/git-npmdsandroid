package id.co.danwinciptaniaga.npmdsandroid.booking;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class BookingBrowseViewModel_AssistedFactory_Factory implements Factory<BookingBrowseViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<GetBookingListUseCase> getListUCProvider;

  public BookingBrowseViewModel_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<GetBookingListUseCase> getListUCProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.getListUCProvider = getListUCProvider;
  }

  @Override
  public BookingBrowseViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider, getListUCProvider);
  }

  public static BookingBrowseViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<GetBookingListUseCase> getListUCProvider) {
    return new BookingBrowseViewModel_AssistedFactory_Factory(applicationProvider, appExecutorsProvider, getListUCProvider);
  }

  public static BookingBrowseViewModel_AssistedFactory newInstance(
      Provider<Application> application, Provider<AppExecutors> appExecutors,
      Provider<GetBookingListUseCase> getListUC) {
    return new BookingBrowseViewModel_AssistedFactory(application, appExecutors, getListUC);
  }
}
