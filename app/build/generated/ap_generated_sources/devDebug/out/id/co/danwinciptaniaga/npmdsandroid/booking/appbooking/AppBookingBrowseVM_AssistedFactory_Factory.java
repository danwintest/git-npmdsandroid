package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingBrowseVM_AssistedFactory_Factory implements Factory<AppBookingBrowseVM_AssistedFactory> {
  private final Provider<Application> appProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<AppBookingBrowseUC> getListUCProvider;

  private final Provider<AppBookingListAction> appBookingListActionProvider;

  public AppBookingBrowseVM_AssistedFactory_Factory(Provider<Application> appProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<AppBookingBrowseUC> getListUCProvider,
      Provider<AppBookingListAction> appBookingListActionProvider) {
    this.appProvider = appProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.getListUCProvider = getListUCProvider;
    this.appBookingListActionProvider = appBookingListActionProvider;
  }

  @Override
  public AppBookingBrowseVM_AssistedFactory get() {
    return newInstance(appProvider, appExecutorsProvider, getListUCProvider, appBookingListActionProvider);
  }

  public static AppBookingBrowseVM_AssistedFactory_Factory create(Provider<Application> appProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<AppBookingBrowseUC> getListUCProvider,
      Provider<AppBookingListAction> appBookingListActionProvider) {
    return new AppBookingBrowseVM_AssistedFactory_Factory(appProvider, appExecutorsProvider, getListUCProvider, appBookingListActionProvider);
  }

  public static AppBookingBrowseVM_AssistedFactory newInstance(Provider<Application> app,
      Provider<AppExecutors> appExecutors, Provider<AppBookingBrowseUC> getListUC,
      Provider<AppBookingListAction> appBookingListAction) {
    return new AppBookingBrowseVM_AssistedFactory(app, appExecutors, getListUC, appBookingListAction);
  }
}
