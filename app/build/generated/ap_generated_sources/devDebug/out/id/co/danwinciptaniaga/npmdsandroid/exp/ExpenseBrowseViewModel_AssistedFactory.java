package id.co.danwinciptaniaga.npmdsandroid.exp;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class ExpenseBrowseViewModel_AssistedFactory implements ViewModelAssistedFactory<ExpenseBrowseViewModel> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<GetExpenseListUseCase> getExpenseListUseCase;

  private final Provider<ExpenseListAction> expListAction;

  @Inject
  ExpenseBrowseViewModel_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<GetExpenseListUseCase> getExpenseListUseCase,
      Provider<ExpenseListAction> expListAction) {
    this.application = application;
    this.appExecutors = appExecutors;
    this.getExpenseListUseCase = getExpenseListUseCase;
    this.expListAction = expListAction;
  }

  @Override
  @NonNull
  public ExpenseBrowseViewModel create(@NonNull SavedStateHandle arg0) {
    return new ExpenseBrowseViewModel(application.get(), appExecutors.get(),
        getExpenseListUseCase.get(), expListAction.get());
  }
}
