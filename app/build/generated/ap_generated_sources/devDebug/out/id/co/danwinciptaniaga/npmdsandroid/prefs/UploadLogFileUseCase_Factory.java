package id.co.danwinciptaniaga.npmdsandroid.prefs;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class UploadLogFileUseCase_Factory implements Factory<UploadLogFileUseCase> {
  private final Provider<Application> applicationProvider;

  private final Provider<UtilityService> utilityServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public UploadLogFileUseCase_Factory(Provider<Application> applicationProvider,
      Provider<UtilityService> utilityServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.utilityServiceProvider = utilityServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public UploadLogFileUseCase get() {
    return newInstance(applicationProvider.get(), utilityServiceProvider.get(), appExecutorsProvider.get());
  }

  public static UploadLogFileUseCase_Factory create(Provider<Application> applicationProvider,
      Provider<UtilityService> utilityServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new UploadLogFileUseCase_Factory(applicationProvider, utilityServiceProvider, appExecutorsProvider);
  }

  public static UploadLogFileUseCase newInstance(Application application,
      UtilityService utilityService, AppExecutors appExecutors) {
    return new UploadLogFileUseCase(application, utilityService, appExecutors);
  }
}
