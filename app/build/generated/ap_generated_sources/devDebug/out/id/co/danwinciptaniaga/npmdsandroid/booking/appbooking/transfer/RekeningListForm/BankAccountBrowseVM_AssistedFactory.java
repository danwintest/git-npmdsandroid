package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class BankAccountBrowseVM_AssistedFactory implements ViewModelAssistedFactory<BankAccountBrowseVM> {
  private final Provider<Application> app;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<BankAccountBrowseUC> loadUC;

  @Inject
  BankAccountBrowseVM_AssistedFactory(Provider<Application> app,
      Provider<AppExecutors> appExecutors, Provider<BankAccountBrowseUC> loadUC) {
    this.app = app;
    this.appExecutors = appExecutors;
    this.loadUC = loadUC;
  }

  @Override
  @NonNull
  public BankAccountBrowseVM create(@NonNull SavedStateHandle arg0) {
    return new BankAccountBrowseVM(app.get(), appExecutors.get(), loadUC.get());
  }
}
