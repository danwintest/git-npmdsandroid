package id.co.danwinciptaniaga.npmdsandroid.ui.login;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = AuthenticationActivity.class
)
@GeneratedEntryPoint
@InstallIn(ActivityComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface AuthenticationActivity_GeneratedInjector {
  void injectAuthenticationActivity(AuthenticationActivity authenticationActivity);
}
