package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TugasTransferWfFragment_MembersInjector implements MembersInjector<TugasTransferWfFragment> {
  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<LoginUtil> loginUtilProvider;

  public TugasTransferWfFragment_MembersInjector(Provider<AppExecutors> appExecutorsProvider,
      Provider<LoginUtil> loginUtilProvider) {
    this.appExecutorsProvider = appExecutorsProvider;
    this.loginUtilProvider = loginUtilProvider;
  }

  public static MembersInjector<TugasTransferWfFragment> create(
      Provider<AppExecutors> appExecutorsProvider, Provider<LoginUtil> loginUtilProvider) {
    return new TugasTransferWfFragment_MembersInjector(appExecutorsProvider, loginUtilProvider);
  }

  @Override
  public void injectMembers(TugasTransferWfFragment instance) {
    injectAppExecutors(instance, appExecutorsProvider.get());
    injectLoginUtil(instance, loginUtilProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfFragment.appExecutors")
  public static void injectAppExecutors(TugasTransferWfFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfFragment.loginUtil")
  public static void injectLoginUtil(TugasTransferWfFragment instance, LoginUtil loginUtil) {
    instance.loginUtil = loginUtil;
  }
}
