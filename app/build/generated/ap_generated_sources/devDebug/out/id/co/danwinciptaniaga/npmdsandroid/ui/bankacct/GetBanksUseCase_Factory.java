package id.co.danwinciptaniaga.npmdsandroid.ui.bankacct;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetBanksUseCase_Factory implements Factory<GetBanksUseCase> {
  private final Provider<Application> appProvider;

  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetBanksUseCase_Factory(Provider<Application> appProvider,
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetBanksUseCase get() {
    return newInstance(appProvider.get(), commonServiceProvider.get(), appExecutorsProvider.get());
  }

  public static GetBanksUseCase_Factory create(Provider<Application> appProvider,
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new GetBanksUseCase_Factory(appProvider, commonServiceProvider, appExecutorsProvider);
  }

  public static GetBanksUseCase newInstance(Application app, CommonService commonService,
      AppExecutors appExecutors) {
    return new GetBanksUseCase(app, commonService, appExecutors);
  }
}
