package id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetTugasTransferFilterUC_Factory implements Factory<GetTugasTransferFilterUC> {
  private final Provider<Application> appProvider;

  private final Provider<TugasTransferService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetTugasTransferFilterUC_Factory(Provider<Application> appProvider,
      Provider<TugasTransferService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetTugasTransferFilterUC get() {
    return newInstance(appProvider.get(), serviceProvider.get(), appExecutorsProvider.get());
  }

  public static GetTugasTransferFilterUC_Factory create(Provider<Application> appProvider,
      Provider<TugasTransferService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new GetTugasTransferFilterUC_Factory(appProvider, serviceProvider, appExecutorsProvider);
  }

  public static GetTugasTransferFilterUC newInstance(Application app, TugasTransferService service,
      AppExecutors appExecutors) {
    return new GetTugasTransferFilterUC(app, service, appExecutors);
  }
}
