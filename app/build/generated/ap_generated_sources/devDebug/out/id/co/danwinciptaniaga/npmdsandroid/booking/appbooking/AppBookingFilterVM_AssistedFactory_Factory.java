package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.GetBookingFilterUC;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingFilterVM_AssistedFactory_Factory implements Factory<AppBookingFilterVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<GetBookingFilterUC> ucServiceProvider;

  public AppBookingFilterVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<GetBookingFilterUC> ucServiceProvider) {
    this.applicationProvider = applicationProvider;
    this.ucServiceProvider = ucServiceProvider;
  }

  @Override
  public AppBookingFilterVM_AssistedFactory get() {
    return newInstance(applicationProvider, ucServiceProvider);
  }

  public static AppBookingFilterVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<GetBookingFilterUC> ucServiceProvider) {
    return new AppBookingFilterVM_AssistedFactory_Factory(applicationProvider, ucServiceProvider);
  }

  public static AppBookingFilterVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<GetBookingFilterUC> ucService) {
    return new AppBookingFilterVM_AssistedFactory(application, ucService);
  }
}
