package id.co.danwinciptaniaga.npmdsandroid.common;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class SubstituteUserUC_Factory implements Factory<SubstituteUserUC> {
  private final Provider<Application> applicationProvider;

  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public SubstituteUserUC_Factory(Provider<Application> applicationProvider,
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public SubstituteUserUC get() {
    return newInstance(applicationProvider.get(), commonServiceProvider.get(), appExecutorsProvider.get());
  }

  public static SubstituteUserUC_Factory create(Provider<Application> applicationProvider,
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new SubstituteUserUC_Factory(applicationProvider, commonServiceProvider, appExecutorsProvider);
  }

  public static SubstituteUserUC newInstance(Application application, CommonService commonService,
      AppExecutors appExecutors) {
    return new SubstituteUserUC(application, commonService, appExecutors);
  }
}
