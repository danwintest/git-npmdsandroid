package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import dagger.internal.Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class BankAccountBrowseUC_Factory implements Factory<BankAccountBrowseUC> {
  private final Provider<AppBookingService> serviceProvider;

  public BankAccountBrowseUC_Factory(Provider<AppBookingService> serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

  @Override
  public BankAccountBrowseUC get() {
    return newInstance(serviceProvider.get());
  }

  public static BankAccountBrowseUC_Factory create(Provider<AppBookingService> serviceProvider) {
    return new BankAccountBrowseUC_Factory(serviceProvider);
  }

  public static BankAccountBrowseUC newInstance(AppBookingService service) {
    return new BankAccountBrowseUC(service);
  }
}
