package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingAdditionalSaveUseCase_Factory implements Factory<DroppingAdditionalSaveUseCase> {
  private final Provider<Application> applicationProvider;

  private final Provider<DroppingAdditionalService> droppingAdditionalServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingAdditionalSaveUseCase_Factory(Provider<Application> applicationProvider,
      Provider<DroppingAdditionalService> droppingAdditionalServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.droppingAdditionalServiceProvider = droppingAdditionalServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingAdditionalSaveUseCase get() {
    return newInstance(applicationProvider.get(), droppingAdditionalServiceProvider.get(), appExecutorsProvider.get());
  }

  public static DroppingAdditionalSaveUseCase_Factory create(
      Provider<Application> applicationProvider,
      Provider<DroppingAdditionalService> droppingAdditionalServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingAdditionalSaveUseCase_Factory(applicationProvider, droppingAdditionalServiceProvider, appExecutorsProvider);
  }

  public static DroppingAdditionalSaveUseCase newInstance(Application application,
      DroppingAdditionalService droppingAdditionalService, AppExecutors appExecutors) {
    return new DroppingAdditionalSaveUseCase(application, droppingAdditionalService, appExecutors);
  }
}
