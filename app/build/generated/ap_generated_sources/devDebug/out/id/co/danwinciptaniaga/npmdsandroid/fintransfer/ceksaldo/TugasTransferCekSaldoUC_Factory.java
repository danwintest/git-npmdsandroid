package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TugasTransferCekSaldoUC_Factory implements Factory<TugasTransferCekSaldoUC> {
  private final Provider<Application> appProvider;

  private final Provider<TugasTransferService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public TugasTransferCekSaldoUC_Factory(Provider<Application> appProvider,
      Provider<TugasTransferService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public TugasTransferCekSaldoUC get() {
    return newInstance(appProvider.get(), serviceProvider.get(), appExecutorsProvider.get());
  }

  public static TugasTransferCekSaldoUC_Factory create(Provider<Application> appProvider,
      Provider<TugasTransferService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new TugasTransferCekSaldoUC_Factory(appProvider, serviceProvider, appExecutorsProvider);
  }

  public static TugasTransferCekSaldoUC newInstance(Application app, TugasTransferService service,
      AppExecutors appExecutors) {
    return new TugasTransferCekSaldoUC(app, service, appExecutors);
  }
}
