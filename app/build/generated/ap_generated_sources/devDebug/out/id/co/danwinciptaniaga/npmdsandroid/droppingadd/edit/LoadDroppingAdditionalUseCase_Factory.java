package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LoadDroppingAdditionalUseCase_Factory implements Factory<LoadDroppingAdditionalUseCase> {
  private final Provider<Application> appProvider;

  private final Provider<DroppingAdditionalService> droppingAdditionalServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public LoadDroppingAdditionalUseCase_Factory(Provider<Application> appProvider,
      Provider<DroppingAdditionalService> droppingAdditionalServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.droppingAdditionalServiceProvider = droppingAdditionalServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public LoadDroppingAdditionalUseCase get() {
    return newInstance(appProvider.get(), droppingAdditionalServiceProvider.get(), appExecutorsProvider.get());
  }

  public static LoadDroppingAdditionalUseCase_Factory create(Provider<Application> appProvider,
      Provider<DroppingAdditionalService> droppingAdditionalServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new LoadDroppingAdditionalUseCase_Factory(appProvider, droppingAdditionalServiceProvider, appExecutorsProvider);
  }

  public static LoadDroppingAdditionalUseCase newInstance(Application app,
      DroppingAdditionalService droppingAdditionalService, AppExecutors appExecutors) {
    return new LoadDroppingAdditionalUseCase(app, droppingAdditionalService, appExecutors);
  }
}
