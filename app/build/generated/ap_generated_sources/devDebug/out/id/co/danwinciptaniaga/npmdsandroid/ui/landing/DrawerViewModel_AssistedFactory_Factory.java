package id.co.danwinciptaniaga.npmdsandroid.ui.landing;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.npmdsandroid.common.GetExtUserInfoUseCase;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DrawerViewModel_AssistedFactory_Factory implements Factory<DrawerViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<GetExtUserInfoUseCase> getUserInfoUseCaseProvider;

  public DrawerViewModel_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<GetExtUserInfoUseCase> getUserInfoUseCaseProvider) {
    this.applicationProvider = applicationProvider;
    this.getUserInfoUseCaseProvider = getUserInfoUseCaseProvider;
  }

  @Override
  public DrawerViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, getUserInfoUseCaseProvider);
  }

  public static DrawerViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<GetExtUserInfoUseCase> getUserInfoUseCaseProvider) {
    return new DrawerViewModel_AssistedFactory_Factory(applicationProvider, getUserInfoUseCaseProvider);
  }

  public static DrawerViewModel_AssistedFactory newInstance(Provider<Application> application,
      Provider<GetExtUserInfoUseCase> getUserInfoUseCase) {
    return new DrawerViewModel_AssistedFactory(application, getUserInfoUseCase);
  }
}
