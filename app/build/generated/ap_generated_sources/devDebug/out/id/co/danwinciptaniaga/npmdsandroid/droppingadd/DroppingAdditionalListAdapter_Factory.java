package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import android.content.Context;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingAdditionalListAdapter_Factory implements Factory<DroppingAdditionalListAdapter> {
  private final Provider<DroppingAdditionalListAdapter.DadRecyclerViewAdapterListener> listenerProvider;

  private final Provider<Context> ctxProvider;

  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingAdditionalListAdapter_Factory(
      Provider<DroppingAdditionalListAdapter.DadRecyclerViewAdapterListener> listenerProvider,
      Provider<Context> ctxProvider, Provider<CommonService> commonServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.listenerProvider = listenerProvider;
    this.ctxProvider = ctxProvider;
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingAdditionalListAdapter get() {
    return newInstance(listenerProvider.get(), ctxProvider.get(), commonServiceProvider.get(), appExecutorsProvider.get());
  }

  public static DroppingAdditionalListAdapter_Factory create(
      Provider<DroppingAdditionalListAdapter.DadRecyclerViewAdapterListener> listenerProvider,
      Provider<Context> ctxProvider, Provider<CommonService> commonServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingAdditionalListAdapter_Factory(listenerProvider, ctxProvider, commonServiceProvider, appExecutorsProvider);
  }

  public static DroppingAdditionalListAdapter newInstance(Object listener, Context ctx,
      CommonService commonService, AppExecutors appExecutors) {
    return new DroppingAdditionalListAdapter((DroppingAdditionalListAdapter.DadRecyclerViewAdapterListener) listener, ctx, commonService, appExecutors);
  }
}
