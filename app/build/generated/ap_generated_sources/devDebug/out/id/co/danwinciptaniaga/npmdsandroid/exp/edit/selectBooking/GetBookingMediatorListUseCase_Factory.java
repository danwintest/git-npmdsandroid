package id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking;

import dagger.internal.Factory;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetBookingMediatorListUseCase_Factory implements Factory<GetBookingMediatorListUseCase> {
  private final Provider<ExpenseService> expenseServiceProvider;

  public GetBookingMediatorListUseCase_Factory(Provider<ExpenseService> expenseServiceProvider) {
    this.expenseServiceProvider = expenseServiceProvider;
  }

  @Override
  public GetBookingMediatorListUseCase get() {
    return newInstance(expenseServiceProvider.get());
  }

  public static GetBookingMediatorListUseCase_Factory create(
      Provider<ExpenseService> expenseServiceProvider) {
    return new GetBookingMediatorListUseCase_Factory(expenseServiceProvider);
  }

  public static GetBookingMediatorListUseCase newInstance(ExpenseService expenseService) {
    return new GetBookingMediatorListUseCase(expenseService);
  }
}
