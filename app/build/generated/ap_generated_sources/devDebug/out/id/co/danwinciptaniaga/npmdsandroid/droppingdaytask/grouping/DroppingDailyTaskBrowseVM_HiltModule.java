package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;
import javax.annotation.Generated;

@Generated("androidx.hilt.AndroidXHiltProcessor")
@Module
@InstallIn(ActivityRetainedComponent.class)
@OriginatingElement(
    topLevelClass = DroppingDailyTaskBrowseVM.class
)
public interface DroppingDailyTaskBrowseVM_HiltModule {
  @Binds
  @IntoMap
  @StringKey("id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseVM")
  ViewModelAssistedFactory<? extends ViewModel> bind(
      DroppingDailyTaskBrowseVM_AssistedFactory factory);
}
