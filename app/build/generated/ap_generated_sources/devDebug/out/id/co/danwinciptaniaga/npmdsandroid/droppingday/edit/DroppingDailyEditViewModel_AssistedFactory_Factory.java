package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyEditViewModel_AssistedFactory_Factory implements Factory<DroppingDailyEditViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<LoadDroppingDailyUseCase> loadDroppingDailyUseCaseProvider;

  private final Provider<DroppingDailySupervisorDecideUseCase> droppingDailySupervisorDecideUseCaseProvider;

  private final Provider<UtilityService> utilityServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailyEditViewModel_AssistedFactory_Factory(
      Provider<Application> applicationProvider,
      Provider<LoadDroppingDailyUseCase> loadDroppingDailyUseCaseProvider,
      Provider<DroppingDailySupervisorDecideUseCase> droppingDailySupervisorDecideUseCaseProvider,
      Provider<UtilityService> utilityServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.loadDroppingDailyUseCaseProvider = loadDroppingDailyUseCaseProvider;
    this.droppingDailySupervisorDecideUseCaseProvider = droppingDailySupervisorDecideUseCaseProvider;
    this.utilityServiceProvider = utilityServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingDailyEditViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, loadDroppingDailyUseCaseProvider, droppingDailySupervisorDecideUseCaseProvider, utilityServiceProvider, appExecutorsProvider);
  }

  public static DroppingDailyEditViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<LoadDroppingDailyUseCase> loadDroppingDailyUseCaseProvider,
      Provider<DroppingDailySupervisorDecideUseCase> droppingDailySupervisorDecideUseCaseProvider,
      Provider<UtilityService> utilityServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailyEditViewModel_AssistedFactory_Factory(applicationProvider, loadDroppingDailyUseCaseProvider, droppingDailySupervisorDecideUseCaseProvider, utilityServiceProvider, appExecutorsProvider);
  }

  public static DroppingDailyEditViewModel_AssistedFactory newInstance(
      Provider<Application> application,
      Provider<LoadDroppingDailyUseCase> loadDroppingDailyUseCase,
      Provider<DroppingDailySupervisorDecideUseCase> droppingDailySupervisorDecideUseCase,
      Provider<UtilityService> utilityService, Provider<AppExecutors> appExecutors) {
    return new DroppingDailyEditViewModel_AssistedFactory(application, loadDroppingDailyUseCase, droppingDailySupervisorDecideUseCase, utilityService, appExecutors);
  }
}
