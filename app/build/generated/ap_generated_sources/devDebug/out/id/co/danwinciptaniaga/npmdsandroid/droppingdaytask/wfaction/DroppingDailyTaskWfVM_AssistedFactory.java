package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingDailyTaskWfVM_AssistedFactory implements ViewModelAssistedFactory<DroppingDailyTaskWfVM> {
  private final Provider<Application> application;

  private final Provider<DroppingDailyTaskProcessUC> processUC;

  @Inject
  DroppingDailyTaskWfVM_AssistedFactory(Provider<Application> application,
      Provider<DroppingDailyTaskProcessUC> processUC) {
    this.application = application;
    this.processUC = processUC;
  }

  @Override
  @NonNull
  public DroppingDailyTaskWfVM create(@NonNull SavedStateHandle arg0) {
    return new DroppingDailyTaskWfVM(application.get(), processUC.get());
  }
}
