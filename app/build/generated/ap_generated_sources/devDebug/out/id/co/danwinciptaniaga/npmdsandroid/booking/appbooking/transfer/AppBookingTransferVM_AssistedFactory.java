package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingLoadUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingPrepareUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingProcessUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class AppBookingTransferVM_AssistedFactory implements ViewModelAssistedFactory<AppBookingTransferVM> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<AppBookingPrepareUC> prepareUC;

  private final Provider<AppBookingLoadUC> appBookingLoadUC;

  private final Provider<AppBookingProcessUC> processUC;

  private final Provider<UtilityService> utilityService;

  private final Provider<AppBookingService> appBookingService;

  @Inject
  AppBookingTransferVM_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<AppBookingPrepareUC> prepareUC,
      Provider<AppBookingLoadUC> appBookingLoadUC, Provider<AppBookingProcessUC> processUC,
      Provider<UtilityService> utilityService, Provider<AppBookingService> appBookingService) {
    this.application = application;
    this.appExecutors = appExecutors;
    this.prepareUC = prepareUC;
    this.appBookingLoadUC = appBookingLoadUC;
    this.processUC = processUC;
    this.utilityService = utilityService;
    this.appBookingService = appBookingService;
  }

  @Override
  @NonNull
  public AppBookingTransferVM create(@NonNull SavedStateHandle arg0) {
    return new AppBookingTransferVM(application.get(), appExecutors.get(), prepareUC.get(),
        appBookingLoadUC.get(), processUC.get(), utilityService.get(), appBookingService.get());
  }
}
