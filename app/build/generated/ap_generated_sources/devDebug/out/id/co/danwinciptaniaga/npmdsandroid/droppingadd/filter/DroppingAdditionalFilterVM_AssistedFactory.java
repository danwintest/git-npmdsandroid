package id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingAdditionalFilterVM_AssistedFactory implements ViewModelAssistedFactory<DroppingAdditionalFilterVM> {
  private final Provider<Application> app;

  private final Provider<GetDroppingAdditionalFilterUC> uc;

  @Inject
  DroppingAdditionalFilterVM_AssistedFactory(Provider<Application> app,
      Provider<GetDroppingAdditionalFilterUC> uc) {
    this.app = app;
    this.uc = uc;
  }

  @Override
  @NonNull
  public DroppingAdditionalFilterVM create(@NonNull SavedStateHandle arg0) {
    return new DroppingAdditionalFilterVM(app.get(), uc.get());
  }
}
