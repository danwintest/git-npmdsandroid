package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingMultiActionVM_AssistedFactory_Factory implements Factory<AppBookingMultiActionVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppBookingMultiActionUC> processUCProvider;

  public AppBookingMultiActionVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<AppBookingMultiActionUC> processUCProvider) {
    this.applicationProvider = applicationProvider;
    this.processUCProvider = processUCProvider;
  }

  @Override
  public AppBookingMultiActionVM_AssistedFactory get() {
    return newInstance(applicationProvider, processUCProvider);
  }

  public static AppBookingMultiActionVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<AppBookingMultiActionUC> processUCProvider) {
    return new AppBookingMultiActionVM_AssistedFactory_Factory(applicationProvider, processUCProvider);
  }

  public static AppBookingMultiActionVM_AssistedFactory newInstance(
      Provider<Application> application, Provider<AppBookingMultiActionUC> processUC) {
    return new AppBookingMultiActionVM_AssistedFactory(application, processUC);
  }
}
