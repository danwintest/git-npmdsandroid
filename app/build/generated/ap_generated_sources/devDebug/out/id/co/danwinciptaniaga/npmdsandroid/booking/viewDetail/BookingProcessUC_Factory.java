package id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class BookingProcessUC_Factory implements Factory<BookingProcessUC> {
  private final Provider<Application> appProvider;

  private final Provider<BookingService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public BookingProcessUC_Factory(Provider<Application> appProvider,
      Provider<BookingService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public BookingProcessUC get() {
    return newInstance(appProvider.get(), serviceProvider.get(), appExecutorsProvider.get());
  }

  public static BookingProcessUC_Factory create(Provider<Application> appProvider,
      Provider<BookingService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new BookingProcessUC_Factory(appProvider, serviceProvider, appExecutorsProvider);
  }

  public static BookingProcessUC newInstance(Application app, BookingService service,
      AppExecutors appExecutors) {
    return new BookingProcessUC(app, service, appExecutors);
  }
}
