package id.co.danwinciptaniaga.npmdsandroid.security;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.npmdsandroid.common.GetSubstituteUsersUC;
import id.co.danwinciptaniaga.npmdsandroid.common.SubstituteUserUC;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class SubstituteUserVM_AssistedFactory implements ViewModelAssistedFactory<SubstituteUserVM> {
  private final Provider<Application> application;

  private final Provider<GetSubstituteUsersUC> getSubstituteUsersUC;

  private final Provider<SubstituteUserUC> substituteUserUC;

  @Inject
  SubstituteUserVM_AssistedFactory(Provider<Application> application,
      Provider<GetSubstituteUsersUC> getSubstituteUsersUC,
      Provider<SubstituteUserUC> substituteUserUC) {
    this.application = application;
    this.getSubstituteUsersUC = getSubstituteUsersUC;
    this.substituteUserUC = substituteUserUC;
  }

  @Override
  @NonNull
  public SubstituteUserVM create(@NonNull SavedStateHandle arg0) {
    return new SubstituteUserVM(application.get(), getSubstituteUsersUC.get(),
        substituteUserUC.get());
  }
}
