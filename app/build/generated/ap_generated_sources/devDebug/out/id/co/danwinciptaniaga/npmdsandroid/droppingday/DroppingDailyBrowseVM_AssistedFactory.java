package id.co.danwinciptaniaga.npmdsandroid.droppingday;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingListActionUC;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingDailyBrowseVM_AssistedFactory implements ViewModelAssistedFactory<DroppingDailyBrowseVM> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<GetDroppingDailyListUC> getDroppingDailyListUC;

  private final Provider<DroppingListActionUC> droppingListActionUC;

  @Inject
  DroppingDailyBrowseVM_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<GetDroppingDailyListUC> getDroppingDailyListUC,
      Provider<DroppingListActionUC> droppingListActionUC) {
    this.application = application;
    this.appExecutors = appExecutors;
    this.getDroppingDailyListUC = getDroppingDailyListUC;
    this.droppingListActionUC = droppingListActionUC;
  }

  @Override
  @NonNull
  public DroppingDailyBrowseVM create(@NonNull SavedStateHandle arg0) {
    return new DroppingDailyBrowseVM(application.get(), appExecutors.get(),
        getDroppingDailyListUC.get(), droppingListActionUC.get());
  }
}
