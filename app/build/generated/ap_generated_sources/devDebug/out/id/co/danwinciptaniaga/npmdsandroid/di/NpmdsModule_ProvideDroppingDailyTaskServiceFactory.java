package id.co.danwinciptaniaga.npmdsandroid.di;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import id.co.danwinciptaniaga.androcon.retrofit.WebServiceGenerator;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.DroppingDailyTaskService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NpmdsModule_ProvideDroppingDailyTaskServiceFactory implements Factory<DroppingDailyTaskService> {
  private final NpmdsModule module;

  private final Provider<WebServiceGenerator> webServiceGeneratorProvider;

  public NpmdsModule_ProvideDroppingDailyTaskServiceFactory(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    this.module = module;
    this.webServiceGeneratorProvider = webServiceGeneratorProvider;
  }

  @Override
  public DroppingDailyTaskService get() {
    return provideDroppingDailyTaskService(module, webServiceGeneratorProvider.get());
  }

  public static NpmdsModule_ProvideDroppingDailyTaskServiceFactory create(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    return new NpmdsModule_ProvideDroppingDailyTaskServiceFactory(module, webServiceGeneratorProvider);
  }

  public static DroppingDailyTaskService provideDroppingDailyTaskService(NpmdsModule instance,
      WebServiceGenerator webServiceGenerator) {
    return Preconditions.checkNotNull(instance.provideDroppingDailyTaskService(webServiceGenerator), "Cannot return null from a non-@Nullable @Provides method");
  }
}
