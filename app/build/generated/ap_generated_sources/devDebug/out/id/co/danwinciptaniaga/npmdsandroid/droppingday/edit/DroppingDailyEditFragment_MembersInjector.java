package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyEditFragment_MembersInjector implements MembersInjector<DroppingDailyEditFragment> {
  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailyEditFragment_MembersInjector(Provider<CommonService> commonServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<DroppingDailyEditFragment> create(
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailyEditFragment_MembersInjector(commonServiceProvider, appExecutorsProvider);
  }

  @Override
  public void injectMembers(DroppingDailyEditFragment instance) {
    injectCommonService(instance, commonServiceProvider.get());
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditFragment.commonService")
  public static void injectCommonService(DroppingDailyEditFragment instance,
      CommonService commonService) {
    instance.commonService = commonService;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditFragment.appExecutors")
  public static void injectAppExecutors(DroppingDailyEditFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
