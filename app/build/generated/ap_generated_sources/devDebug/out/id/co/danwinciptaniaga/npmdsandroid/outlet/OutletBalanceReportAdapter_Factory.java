package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.content.Context;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class OutletBalanceReportAdapter_Factory implements Factory<OutletBalanceReportAdapter> {
  private final Provider<Context> contextProvider;

  public OutletBalanceReportAdapter_Factory(Provider<Context> contextProvider) {
    this.contextProvider = contextProvider;
  }

  @Override
  public OutletBalanceReportAdapter get() {
    return newInstance(contextProvider.get());
  }

  public static OutletBalanceReportAdapter_Factory create(Provider<Context> contextProvider) {
    return new OutletBalanceReportAdapter_Factory(contextProvider);
  }

  public static OutletBalanceReportAdapter newInstance(Context context) {
    return new OutletBalanceReportAdapter(context);
  }
}
