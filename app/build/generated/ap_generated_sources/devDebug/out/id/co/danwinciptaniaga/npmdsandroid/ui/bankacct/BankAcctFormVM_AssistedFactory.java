package id.co.danwinciptaniaga.npmdsandroid.ui.bankacct;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class BankAcctFormVM_AssistedFactory implements ViewModelAssistedFactory<BankAcctFormVM> {
  private final Provider<Application> application;

  private final Provider<GetBanksUseCase> getBanksUseCase;

  @Inject
  BankAcctFormVM_AssistedFactory(Provider<Application> application,
      Provider<GetBanksUseCase> getBanksUseCase) {
    this.application = application;
    this.getBanksUseCase = getBanksUseCase;
  }

  @Override
  @NonNull
  public BankAcctFormVM create(@NonNull SavedStateHandle arg0) {
    return new BankAcctFormVM(application.get(), getBanksUseCase.get());
  }
}
