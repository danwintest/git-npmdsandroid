package id.co.danwinciptaniaga.npmdsandroid.droppingday;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyBrowseFragment_MembersInjector implements MembersInjector<DroppingDailyBrowseFragment> {
  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailyBrowseFragment_MembersInjector(Provider<CommonService> commonServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<DroppingDailyBrowseFragment> create(
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailyBrowseFragment_MembersInjector(commonServiceProvider, appExecutorsProvider);
  }

  @Override
  public void injectMembers(DroppingDailyBrowseFragment instance) {
    injectCommonService(instance, commonServiceProvider.get());
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseFragment.commonService")
  public static void injectCommonService(DroppingDailyBrowseFragment instance,
      CommonService commonService) {
    instance.commonService = commonService;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseFragment.appExecutors")
  public static void injectAppExecutors(DroppingDailyBrowseFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
