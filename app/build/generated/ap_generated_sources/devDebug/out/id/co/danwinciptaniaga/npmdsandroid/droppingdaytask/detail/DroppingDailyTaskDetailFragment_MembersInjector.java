package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskDetailFragment_MembersInjector implements MembersInjector<DroppingDailyTaskDetailFragment> {
  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailyTaskDetailFragment_MembersInjector(
      Provider<AppExecutors> appExecutorsProvider) {
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<DroppingDailyTaskDetailFragment> create(
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailyTaskDetailFragment_MembersInjector(appExecutorsProvider);
  }

  @Override
  public void injectMembers(DroppingDailyTaskDetailFragment instance) {
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail.DroppingDailyTaskDetailFragment.appExecutors")
  public static void injectAppExecutors(DroppingDailyTaskDetailFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
