package id.co.danwinciptaniaga.npmdsandroid.prefs;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ChangePasswordFragment_MembersInjector implements MembersInjector<ChangePasswordFragment> {
  private final Provider<LoginUtil> loginUtilProvider;

  public ChangePasswordFragment_MembersInjector(Provider<LoginUtil> loginUtilProvider) {
    this.loginUtilProvider = loginUtilProvider;
  }

  public static MembersInjector<ChangePasswordFragment> create(
      Provider<LoginUtil> loginUtilProvider) {
    return new ChangePasswordFragment_MembersInjector(loginUtilProvider);
  }

  @Override
  public void injectMembers(ChangePasswordFragment instance) {
    injectLoginUtil(instance, loginUtilProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordFragment.loginUtil")
  public static void injectLoginUtil(ChangePasswordFragment instance, LoginUtil loginUtil) {
    instance.loginUtil = loginUtil;
  }
}
