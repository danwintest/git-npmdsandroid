package id.co.danwinciptaniaga.npmdsandroid.di;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import id.co.danwinciptaniaga.androcon.retrofit.WebServiceGenerator;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NpmdsModule_ProvideExpenseServiceFactory implements Factory<ExpenseService> {
  private final NpmdsModule module;

  private final Provider<WebServiceGenerator> webServiceGeneratorProvider;

  public NpmdsModule_ProvideExpenseServiceFactory(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    this.module = module;
    this.webServiceGeneratorProvider = webServiceGeneratorProvider;
  }

  @Override
  public ExpenseService get() {
    return provideExpenseService(module, webServiceGeneratorProvider.get());
  }

  public static NpmdsModule_ProvideExpenseServiceFactory create(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    return new NpmdsModule_ProvideExpenseServiceFactory(module, webServiceGeneratorProvider);
  }

  public static ExpenseService provideExpenseService(NpmdsModule instance,
      WebServiceGenerator webServiceGenerator) {
    return Preconditions.checkNotNull(instance.provideExpenseService(webServiceGenerator), "Cannot return null from a non-@Nullable @Provides method");
  }
}
