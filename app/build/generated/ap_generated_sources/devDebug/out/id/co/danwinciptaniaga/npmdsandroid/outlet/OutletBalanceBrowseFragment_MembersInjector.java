package id.co.danwinciptaniaga.npmdsandroid.outlet;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class OutletBalanceBrowseFragment_MembersInjector implements MembersInjector<OutletBalanceBrowseFragment> {
  private final Provider<AppExecutors> appExecutorsProvider;

  public OutletBalanceBrowseFragment_MembersInjector(Provider<AppExecutors> appExecutorsProvider) {
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<OutletBalanceBrowseFragment> create(
      Provider<AppExecutors> appExecutorsProvider) {
    return new OutletBalanceBrowseFragment_MembersInjector(appExecutorsProvider);
  }

  @Override
  public void injectMembers(OutletBalanceBrowseFragment instance) {
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceBrowseFragment.appExecutors")
  public static void injectAppExecutors(OutletBalanceBrowseFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
