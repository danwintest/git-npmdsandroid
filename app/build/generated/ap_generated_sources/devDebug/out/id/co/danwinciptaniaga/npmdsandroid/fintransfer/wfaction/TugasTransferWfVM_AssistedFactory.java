package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class TugasTransferWfVM_AssistedFactory implements ViewModelAssistedFactory<TugasTransferWfVM> {
  private final Provider<Application> application;

  private final Provider<TugasTransferWfFormUC> ucForm;

  private final Provider<TugasTransferWfProcessUC> ucProcess;

  private final Provider<TugasTransferWFGenerateOtpUC> ucGenerateOtpUC;

  @Inject
  TugasTransferWfVM_AssistedFactory(Provider<Application> application,
      Provider<TugasTransferWfFormUC> ucForm, Provider<TugasTransferWfProcessUC> ucProcess,
      Provider<TugasTransferWFGenerateOtpUC> ucGenerateOtpUC) {
    this.application = application;
    this.ucForm = ucForm;
    this.ucProcess = ucProcess;
    this.ucGenerateOtpUC = ucGenerateOtpUC;
  }

  @Override
  @NonNull
  public TugasTransferWfVM create(@NonNull SavedStateHandle arg0) {
    return new TugasTransferWfVM(application.get(), ucForm.get(), ucProcess.get(),
        ucGenerateOtpUC.get());
  }
}
