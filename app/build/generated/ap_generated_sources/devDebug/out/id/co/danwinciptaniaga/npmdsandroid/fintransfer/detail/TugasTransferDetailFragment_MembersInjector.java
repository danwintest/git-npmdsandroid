package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TugasTransferDetailFragment_MembersInjector implements MembersInjector<TugasTransferDetailFragment> {
  private final Provider<AppExecutors> appExecutorsProvider;

  public TugasTransferDetailFragment_MembersInjector(Provider<AppExecutors> appExecutorsProvider) {
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<TugasTransferDetailFragment> create(
      Provider<AppExecutors> appExecutorsProvider) {
    return new TugasTransferDetailFragment_MembersInjector(appExecutorsProvider);
  }

  @Override
  public void injectMembers(TugasTransferDetailFragment instance) {
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailFragment.appExecutors")
  public static void injectAppExecutors(TugasTransferDetailFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
