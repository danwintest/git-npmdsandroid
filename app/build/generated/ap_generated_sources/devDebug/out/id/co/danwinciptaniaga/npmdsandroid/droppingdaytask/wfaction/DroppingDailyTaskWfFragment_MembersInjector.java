package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskWfFragment_MembersInjector implements MembersInjector<DroppingDailyTaskWfFragment> {
  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailyTaskWfFragment_MembersInjector(Provider<AppExecutors> appExecutorsProvider) {
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<DroppingDailyTaskWfFragment> create(
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailyTaskWfFragment_MembersInjector(appExecutorsProvider);
  }

  @Override
  public void injectMembers(DroppingDailyTaskWfFragment instance) {
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfFragment.appExecutors")
  public static void injectAppExecutors(DroppingDailyTaskWfFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
