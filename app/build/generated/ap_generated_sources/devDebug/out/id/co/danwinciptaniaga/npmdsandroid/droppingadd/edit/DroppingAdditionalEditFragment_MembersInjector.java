package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingAdditionalEditFragment_MembersInjector implements MembersInjector<DroppingAdditionalEditFragment> {
  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingAdditionalEditFragment_MembersInjector(
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<DroppingAdditionalEditFragment> create(
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingAdditionalEditFragment_MembersInjector(commonServiceProvider, appExecutorsProvider);
  }

  @Override
  public void injectMembers(DroppingAdditionalEditFragment instance) {
    injectCommonService(instance, commonServiceProvider.get());
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditFragment.commonService")
  public static void injectCommonService(DroppingAdditionalEditFragment instance,
      CommonService commonService) {
    instance.commonService = commonService;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditFragment.appExecutors")
  public static void injectAppExecutors(DroppingAdditionalEditFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
