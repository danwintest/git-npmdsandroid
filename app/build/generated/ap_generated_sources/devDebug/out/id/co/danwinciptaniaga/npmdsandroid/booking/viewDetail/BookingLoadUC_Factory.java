package id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class BookingLoadUC_Factory implements Factory<BookingLoadUC> {
  private final Provider<Application> appProvider;

  private final Provider<BookingService> bookingServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public BookingLoadUC_Factory(Provider<Application> appProvider,
      Provider<BookingService> bookingServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.bookingServiceProvider = bookingServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public BookingLoadUC get() {
    return newInstance(appProvider.get(), bookingServiceProvider.get(), appExecutorsProvider.get());
  }

  public static BookingLoadUC_Factory create(Provider<Application> appProvider,
      Provider<BookingService> bookingServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new BookingLoadUC_Factory(appProvider, bookingServiceProvider, appExecutorsProvider);
  }

  public static BookingLoadUC newInstance(Application app, BookingService bookingService,
      AppExecutors appExecutors) {
    return new BookingLoadUC(app, bookingService, appExecutors);
  }
}
