package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.GetDroppingDailyTaskFilterUC;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskBrowseVM_AssistedFactory_Factory implements Factory<DroppingDailyTaskBrowseVM_AssistedFactory> {
  private final Provider<Application> appProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<GetDroppingDailyTaskBrowseHeaderUC> ucHeaderProvider;

  private final Provider<GetDroppingDailyTaskFilterUC> ucFilterProvider;

  public DroppingDailyTaskBrowseVM_AssistedFactory_Factory(Provider<Application> appProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<GetDroppingDailyTaskBrowseHeaderUC> ucHeaderProvider,
      Provider<GetDroppingDailyTaskFilterUC> ucFilterProvider) {
    this.appProvider = appProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.ucHeaderProvider = ucHeaderProvider;
    this.ucFilterProvider = ucFilterProvider;
  }

  @Override
  public DroppingDailyTaskBrowseVM_AssistedFactory get() {
    return newInstance(appProvider, appExecutorsProvider, ucHeaderProvider, ucFilterProvider);
  }

  public static DroppingDailyTaskBrowseVM_AssistedFactory_Factory create(
      Provider<Application> appProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<GetDroppingDailyTaskBrowseHeaderUC> ucHeaderProvider,
      Provider<GetDroppingDailyTaskFilterUC> ucFilterProvider) {
    return new DroppingDailyTaskBrowseVM_AssistedFactory_Factory(appProvider, appExecutorsProvider, ucHeaderProvider, ucFilterProvider);
  }

  public static DroppingDailyTaskBrowseVM_AssistedFactory newInstance(Provider<Application> app,
      Provider<AppExecutors> appExecutors, Provider<GetDroppingDailyTaskBrowseHeaderUC> ucHeader,
      Provider<GetDroppingDailyTaskFilterUC> ucFilter) {
    return new DroppingDailyTaskBrowseVM_AssistedFactory(app, appExecutors, ucHeader, ucFilter);
  }
}
