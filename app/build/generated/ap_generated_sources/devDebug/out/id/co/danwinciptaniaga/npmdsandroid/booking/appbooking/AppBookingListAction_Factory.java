package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingListAction_Factory implements Factory<AppBookingListAction> {
  private final Provider<AppBookingService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<Application> appProvider;

  public AppBookingListAction_Factory(Provider<AppBookingService> serviceProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<Application> appProvider) {
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.appProvider = appProvider;
  }

  @Override
  public AppBookingListAction get() {
    return newInstance(serviceProvider.get(), appExecutorsProvider.get(), appProvider.get());
  }

  public static AppBookingListAction_Factory create(Provider<AppBookingService> serviceProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<Application> appProvider) {
    return new AppBookingListAction_Factory(serviceProvider, appExecutorsProvider, appProvider);
  }

  public static AppBookingListAction newInstance(AppBookingService service,
      AppExecutors appExecutors, Application app) {
    return new AppBookingListAction(service, appExecutors, app);
  }
}
