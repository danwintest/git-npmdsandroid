package id.co.danwinciptaniaga.npmdsandroid.di;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import id.co.danwinciptaniaga.androcon.retrofit.WebServiceGenerator;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NpmdsModule_ProvideDroppingAdditionalServiceFactory implements Factory<DroppingAdditionalService> {
  private final NpmdsModule module;

  private final Provider<WebServiceGenerator> webServiceGeneratorProvider;

  public NpmdsModule_ProvideDroppingAdditionalServiceFactory(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    this.module = module;
    this.webServiceGeneratorProvider = webServiceGeneratorProvider;
  }

  @Override
  public DroppingAdditionalService get() {
    return provideDroppingAdditionalService(module, webServiceGeneratorProvider.get());
  }

  public static NpmdsModule_ProvideDroppingAdditionalServiceFactory create(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    return new NpmdsModule_ProvideDroppingAdditionalServiceFactory(module, webServiceGeneratorProvider);
  }

  public static DroppingAdditionalService provideDroppingAdditionalService(NpmdsModule instance,
      WebServiceGenerator webServiceGenerator) {
    return Preconditions.checkNotNull(instance.provideDroppingAdditionalService(webServiceGenerator), "Cannot return null from a non-@Nullable @Provides method");
  }
}
