package id.co.danwinciptaniaga.npmdsandroid.booking;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class BookingFilterVM_AssistedFactory_Factory implements Factory<BookingFilterVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<GetBookingFilterUC> ucServiceProvider;

  public BookingFilterVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<GetBookingFilterUC> ucServiceProvider) {
    this.applicationProvider = applicationProvider;
    this.ucServiceProvider = ucServiceProvider;
  }

  @Override
  public BookingFilterVM_AssistedFactory get() {
    return newInstance(applicationProvider, ucServiceProvider);
  }

  public static BookingFilterVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<GetBookingFilterUC> ucServiceProvider) {
    return new BookingFilterVM_AssistedFactory_Factory(applicationProvider, ucServiceProvider);
  }

  public static BookingFilterVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<GetBookingFilterUC> ucService) {
    return new BookingFilterVM_AssistedFactory(application, ucService);
  }
}
