package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class ExpenseEditViewModel_AssistedFactory implements ViewModelAssistedFactory<ExpenseEditViewModel> {
  private final Provider<Application> application;

  private final Provider<PrepareNewExpenseUseCase> ucPrepareNewExpense;

  private final Provider<ExpenseSaveDraftUseCase> ucExpenseSaveDraft;

  private final Provider<LoadExpenseUseCase> ucLoadExpense;

  private final Provider<ExpenseAocDecideUseCase> expenseAocDecideUseCase;

  private final Provider<ExpenseService> expenseService;

  private final Provider<UtilityService> utilityService;

  private final Provider<LoginUtil> loginUtil;

  private final Provider<AppExecutors> appExecutors;

  @Inject
  ExpenseEditViewModel_AssistedFactory(Provider<Application> application,
      Provider<PrepareNewExpenseUseCase> ucPrepareNewExpense,
      Provider<ExpenseSaveDraftUseCase> ucExpenseSaveDraft,
      Provider<LoadExpenseUseCase> ucLoadExpense,
      Provider<ExpenseAocDecideUseCase> expenseAocDecideUseCase,
      Provider<ExpenseService> expenseService, Provider<UtilityService> utilityService,
      Provider<LoginUtil> loginUtil, Provider<AppExecutors> appExecutors) {
    this.application = application;
    this.ucPrepareNewExpense = ucPrepareNewExpense;
    this.ucExpenseSaveDraft = ucExpenseSaveDraft;
    this.ucLoadExpense = ucLoadExpense;
    this.expenseAocDecideUseCase = expenseAocDecideUseCase;
    this.expenseService = expenseService;
    this.utilityService = utilityService;
    this.loginUtil = loginUtil;
    this.appExecutors = appExecutors;
  }

  @Override
  @NonNull
  public ExpenseEditViewModel create(@NonNull SavedStateHandle arg0) {
    return new ExpenseEditViewModel(application.get(), ucPrepareNewExpense.get(),
        ucExpenseSaveDraft.get(), ucLoadExpense.get(), expenseAocDecideUseCase.get(),
        expenseService.get(), utilityService.get(), loginUtil.get(), appExecutors.get());
  }
}
