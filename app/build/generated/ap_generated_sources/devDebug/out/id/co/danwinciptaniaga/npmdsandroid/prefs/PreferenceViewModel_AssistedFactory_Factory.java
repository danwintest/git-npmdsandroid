package id.co.danwinciptaniaga.npmdsandroid.prefs;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.update.DownloadUpdateUseCase;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.GetLatestVersionUseCase;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class PreferenceViewModel_AssistedFactory_Factory implements Factory<PreferenceViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<AndroconConfig> androconConfigProvider;

  private final Provider<GetLatestVersionUseCase> getLatestVersionUseCaseProvider;

  private final Provider<DownloadUpdateUseCase> downloadUpdateUseCaseProvider;

  public PreferenceViewModel_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<AndroconConfig> androconConfigProvider,
      Provider<GetLatestVersionUseCase> getLatestVersionUseCaseProvider,
      Provider<DownloadUpdateUseCase> downloadUpdateUseCaseProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.androconConfigProvider = androconConfigProvider;
    this.getLatestVersionUseCaseProvider = getLatestVersionUseCaseProvider;
    this.downloadUpdateUseCaseProvider = downloadUpdateUseCaseProvider;
  }

  @Override
  public PreferenceViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider, androconConfigProvider, getLatestVersionUseCaseProvider, downloadUpdateUseCaseProvider);
  }

  public static PreferenceViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<AndroconConfig> androconConfigProvider,
      Provider<GetLatestVersionUseCase> getLatestVersionUseCaseProvider,
      Provider<DownloadUpdateUseCase> downloadUpdateUseCaseProvider) {
    return new PreferenceViewModel_AssistedFactory_Factory(applicationProvider, appExecutorsProvider, androconConfigProvider, getLatestVersionUseCaseProvider, downloadUpdateUseCaseProvider);
  }

  public static PreferenceViewModel_AssistedFactory newInstance(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<AndroconConfig> androconConfig,
      Provider<GetLatestVersionUseCase> getLatestVersionUseCase,
      Provider<DownloadUpdateUseCase> downloadUpdateUseCase) {
    return new PreferenceViewModel_AssistedFactory(application, appExecutors, androconConfig, getLatestVersionUseCase, downloadUpdateUseCase);
  }
}
