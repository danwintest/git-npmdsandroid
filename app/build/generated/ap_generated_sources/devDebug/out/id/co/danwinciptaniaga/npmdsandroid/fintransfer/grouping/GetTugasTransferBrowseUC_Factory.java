package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import dagger.internal.Factory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetTugasTransferBrowseUC_Factory implements Factory<GetTugasTransferBrowseUC> {
  private final Provider<TugasTransferService> serviceProvider;

  public GetTugasTransferBrowseUC_Factory(Provider<TugasTransferService> serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

  @Override
  public GetTugasTransferBrowseUC get() {
    return newInstance(serviceProvider.get());
  }

  public static GetTugasTransferBrowseUC_Factory create(
      Provider<TugasTransferService> serviceProvider) {
    return new GetTugasTransferBrowseUC_Factory(serviceProvider);
  }

  public static GetTugasTransferBrowseUC newInstance(TugasTransferService service) {
    return new GetTugasTransferBrowseUC(service);
  }
}
