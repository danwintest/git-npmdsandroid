package id.co.danwinciptaniaga.npmdsandroid;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.security.ProtectedActivity_MembersInjector;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class BaseActivity_MembersInjector implements MembersInjector<BaseActivity> {
  private final Provider<LoginUtil> loginUtilProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<AndroconConfig> androconConfigProvider;

  public BaseActivity_MembersInjector(Provider<LoginUtil> loginUtilProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<AndroconConfig> androconConfigProvider) {
    this.loginUtilProvider = loginUtilProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.androconConfigProvider = androconConfigProvider;
  }

  public static MembersInjector<BaseActivity> create(Provider<LoginUtil> loginUtilProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<AndroconConfig> androconConfigProvider) {
    return new BaseActivity_MembersInjector(loginUtilProvider, appExecutorsProvider, androconConfigProvider);
  }

  @Override
  public void injectMembers(BaseActivity instance) {
    ProtectedActivity_MembersInjector.injectLoginUtil(instance, loginUtilProvider.get());
    ProtectedActivity_MembersInjector.injectAppExecutors(instance, appExecutorsProvider.get());
    injectAndroconConfig(instance, androconConfigProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.BaseActivity.androconConfig")
  public static void injectAndroconConfig(BaseActivity instance, AndroconConfig androconConfig) {
    instance.androconConfig = androconConfig;
  }
}
