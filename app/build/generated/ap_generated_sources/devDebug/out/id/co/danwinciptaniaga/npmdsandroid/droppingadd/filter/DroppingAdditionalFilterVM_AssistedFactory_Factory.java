package id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingAdditionalFilterVM_AssistedFactory_Factory implements Factory<DroppingAdditionalFilterVM_AssistedFactory> {
  private final Provider<Application> appProvider;

  private final Provider<GetDroppingAdditionalFilterUC> ucProvider;

  public DroppingAdditionalFilterVM_AssistedFactory_Factory(Provider<Application> appProvider,
      Provider<GetDroppingAdditionalFilterUC> ucProvider) {
    this.appProvider = appProvider;
    this.ucProvider = ucProvider;
  }

  @Override
  public DroppingAdditionalFilterVM_AssistedFactory get() {
    return newInstance(appProvider, ucProvider);
  }

  public static DroppingAdditionalFilterVM_AssistedFactory_Factory create(
      Provider<Application> appProvider, Provider<GetDroppingAdditionalFilterUC> ucProvider) {
    return new DroppingAdditionalFilterVM_AssistedFactory_Factory(appProvider, ucProvider);
  }

  public static DroppingAdditionalFilterVM_AssistedFactory newInstance(Provider<Application> app,
      Provider<GetDroppingAdditionalFilterUC> uc) {
    return new DroppingAdditionalFilterVM_AssistedFactory(app, uc);
  }
}
