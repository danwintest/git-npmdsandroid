package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class PrepareNewExpenseUseCase_Factory implements Factory<PrepareNewExpenseUseCase> {
  private final Provider<Application> appProvider;

  private final Provider<ExpenseService> expenseServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public PrepareNewExpenseUseCase_Factory(Provider<Application> appProvider,
      Provider<ExpenseService> expenseServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.expenseServiceProvider = expenseServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public PrepareNewExpenseUseCase get() {
    return newInstance(appProvider.get(), expenseServiceProvider.get(), appExecutorsProvider.get());
  }

  public static PrepareNewExpenseUseCase_Factory create(Provider<Application> appProvider,
      Provider<ExpenseService> expenseServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new PrepareNewExpenseUseCase_Factory(appProvider, expenseServiceProvider, appExecutorsProvider);
  }

  public static PrepareNewExpenseUseCase newInstance(Application app, ExpenseService expenseService,
      AppExecutors appExecutors) {
    return new PrepareNewExpenseUseCase(app, expenseService, appExecutors);
  }
}
