package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskWfVM_AssistedFactory_Factory implements Factory<DroppingDailyTaskWfVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<DroppingDailyTaskProcessUC> processUCProvider;

  public DroppingDailyTaskWfVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<DroppingDailyTaskProcessUC> processUCProvider) {
    this.applicationProvider = applicationProvider;
    this.processUCProvider = processUCProvider;
  }

  @Override
  public DroppingDailyTaskWfVM_AssistedFactory get() {
    return newInstance(applicationProvider, processUCProvider);
  }

  public static DroppingDailyTaskWfVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<DroppingDailyTaskProcessUC> processUCProvider) {
    return new DroppingDailyTaskWfVM_AssistedFactory_Factory(applicationProvider, processUCProvider);
  }

  public static DroppingDailyTaskWfVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<DroppingDailyTaskProcessUC> processUC) {
    return new DroppingDailyTaskWfVM_AssistedFactory(application, processUC);
  }
}
