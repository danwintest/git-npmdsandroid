package id.co.danwinciptaniaga.npmdsandroid.prefs;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.update.DownloadUpdateUseCase;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.GetLatestVersionUseCase;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class PreferenceViewModel_AssistedFactory implements ViewModelAssistedFactory<PreferenceViewModel> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<AndroconConfig> androconConfig;

  private final Provider<GetLatestVersionUseCase> getLatestVersionUseCase;

  private final Provider<DownloadUpdateUseCase> downloadUpdateUseCase;

  @Inject
  PreferenceViewModel_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<AndroconConfig> androconConfig,
      Provider<GetLatestVersionUseCase> getLatestVersionUseCase,
      Provider<DownloadUpdateUseCase> downloadUpdateUseCase) {
    this.application = application;
    this.appExecutors = appExecutors;
    this.androconConfig = androconConfig;
    this.getLatestVersionUseCase = getLatestVersionUseCase;
    this.downloadUpdateUseCase = downloadUpdateUseCase;
  }

  @Override
  @NonNull
  public PreferenceViewModel create(@NonNull SavedStateHandle arg0) {
    return new PreferenceViewModel(application.get(), appExecutors.get(), androconConfig.get(),
        getLatestVersionUseCase.get(), downloadUpdateUseCase.get());
  }
}
