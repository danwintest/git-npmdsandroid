package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ExpenseEditViewModel_AssistedFactory_Factory implements Factory<ExpenseEditViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<PrepareNewExpenseUseCase> ucPrepareNewExpenseProvider;

  private final Provider<ExpenseSaveDraftUseCase> ucExpenseSaveDraftProvider;

  private final Provider<LoadExpenseUseCase> ucLoadExpenseProvider;

  private final Provider<ExpenseAocDecideUseCase> expenseAocDecideUseCaseProvider;

  private final Provider<ExpenseService> expenseServiceProvider;

  private final Provider<UtilityService> utilityServiceProvider;

  private final Provider<LoginUtil> loginUtilProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public ExpenseEditViewModel_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<PrepareNewExpenseUseCase> ucPrepareNewExpenseProvider,
      Provider<ExpenseSaveDraftUseCase> ucExpenseSaveDraftProvider,
      Provider<LoadExpenseUseCase> ucLoadExpenseProvider,
      Provider<ExpenseAocDecideUseCase> expenseAocDecideUseCaseProvider,
      Provider<ExpenseService> expenseServiceProvider,
      Provider<UtilityService> utilityServiceProvider, Provider<LoginUtil> loginUtilProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.ucPrepareNewExpenseProvider = ucPrepareNewExpenseProvider;
    this.ucExpenseSaveDraftProvider = ucExpenseSaveDraftProvider;
    this.ucLoadExpenseProvider = ucLoadExpenseProvider;
    this.expenseAocDecideUseCaseProvider = expenseAocDecideUseCaseProvider;
    this.expenseServiceProvider = expenseServiceProvider;
    this.utilityServiceProvider = utilityServiceProvider;
    this.loginUtilProvider = loginUtilProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public ExpenseEditViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, ucPrepareNewExpenseProvider, ucExpenseSaveDraftProvider, ucLoadExpenseProvider, expenseAocDecideUseCaseProvider, expenseServiceProvider, utilityServiceProvider, loginUtilProvider, appExecutorsProvider);
  }

  public static ExpenseEditViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<PrepareNewExpenseUseCase> ucPrepareNewExpenseProvider,
      Provider<ExpenseSaveDraftUseCase> ucExpenseSaveDraftProvider,
      Provider<LoadExpenseUseCase> ucLoadExpenseProvider,
      Provider<ExpenseAocDecideUseCase> expenseAocDecideUseCaseProvider,
      Provider<ExpenseService> expenseServiceProvider,
      Provider<UtilityService> utilityServiceProvider, Provider<LoginUtil> loginUtilProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new ExpenseEditViewModel_AssistedFactory_Factory(applicationProvider, ucPrepareNewExpenseProvider, ucExpenseSaveDraftProvider, ucLoadExpenseProvider, expenseAocDecideUseCaseProvider, expenseServiceProvider, utilityServiceProvider, loginUtilProvider, appExecutorsProvider);
  }

  public static ExpenseEditViewModel_AssistedFactory newInstance(Provider<Application> application,
      Provider<PrepareNewExpenseUseCase> ucPrepareNewExpense,
      Provider<ExpenseSaveDraftUseCase> ucExpenseSaveDraft,
      Provider<LoadExpenseUseCase> ucLoadExpense,
      Provider<ExpenseAocDecideUseCase> expenseAocDecideUseCase,
      Provider<ExpenseService> expenseService, Provider<UtilityService> utilityService,
      Provider<LoginUtil> loginUtil, Provider<AppExecutors> appExecutors) {
    return new ExpenseEditViewModel_AssistedFactory(application, ucPrepareNewExpense, ucExpenseSaveDraft, ucLoadExpense, expenseAocDecideUseCase, expenseService, utilityService, loginUtil, appExecutors);
  }
}
