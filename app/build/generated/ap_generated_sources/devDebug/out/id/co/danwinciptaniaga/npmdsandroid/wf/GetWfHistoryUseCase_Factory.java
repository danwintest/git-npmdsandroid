package id.co.danwinciptaniaga.npmdsandroid.wf;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyService;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetWfHistoryUseCase_Factory implements Factory<GetWfHistoryUseCase> {
  private final Provider<Application> applicationProvider;

  private final Provider<ExpenseService> expenseServiceProvider;

  private final Provider<DroppingDailyService> droppingDailyServiceProvider;

  private final Provider<DroppingAdditionalService> droppingAdditionalServiceProvider;

  private final Provider<AppBookingService> appBookingServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetWfHistoryUseCase_Factory(Provider<Application> applicationProvider,
      Provider<ExpenseService> expenseServiceProvider,
      Provider<DroppingDailyService> droppingDailyServiceProvider,
      Provider<DroppingAdditionalService> droppingAdditionalServiceProvider,
      Provider<AppBookingService> appBookingServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.expenseServiceProvider = expenseServiceProvider;
    this.droppingDailyServiceProvider = droppingDailyServiceProvider;
    this.droppingAdditionalServiceProvider = droppingAdditionalServiceProvider;
    this.appBookingServiceProvider = appBookingServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetWfHistoryUseCase get() {
    return newInstance(applicationProvider.get(), expenseServiceProvider.get(), droppingDailyServiceProvider.get(), droppingAdditionalServiceProvider.get(), appBookingServiceProvider.get(), appExecutorsProvider.get());
  }

  public static GetWfHistoryUseCase_Factory create(Provider<Application> applicationProvider,
      Provider<ExpenseService> expenseServiceProvider,
      Provider<DroppingDailyService> droppingDailyServiceProvider,
      Provider<DroppingAdditionalService> droppingAdditionalServiceProvider,
      Provider<AppBookingService> appBookingServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new GetWfHistoryUseCase_Factory(applicationProvider, expenseServiceProvider, droppingDailyServiceProvider, droppingAdditionalServiceProvider, appBookingServiceProvider, appExecutorsProvider);
  }

  public static GetWfHistoryUseCase newInstance(Application application,
      ExpenseService expenseService, DroppingDailyService droppingDailyService,
      DroppingAdditionalService droppingAdditionalService, AppBookingService appBookingService,
      AppExecutors appExecutors) {
    return new GetWfHistoryUseCase(application, expenseService, droppingDailyService, droppingAdditionalService, appBookingService, appExecutors);
  }
}
