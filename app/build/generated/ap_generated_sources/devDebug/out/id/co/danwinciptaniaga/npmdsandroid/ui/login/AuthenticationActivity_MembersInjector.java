package id.co.danwinciptaniaga.npmdsandroid.ui.login;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.auth.AuthService;
import id.co.danwinciptaniaga.androcon.auth.AuthorizationHeaderProvider;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCaseSync;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AuthenticationActivity_MembersInjector implements MembersInjector<AuthenticationActivity> {
  private final Provider<LoginUtil> loginUtilProvider;

  private final Provider<AuthService> authServiceProvider;

  private final Provider<AuthorizationHeaderProvider> ahpProvider;

  private final Provider<AppExecutors> executorsProvider;

  private final Provider<GetTokenUseCaseSync> ucGetTokenSyncProvider;

  public AuthenticationActivity_MembersInjector(Provider<LoginUtil> loginUtilProvider,
      Provider<AuthService> authServiceProvider, Provider<AuthorizationHeaderProvider> ahpProvider,
      Provider<AppExecutors> executorsProvider,
      Provider<GetTokenUseCaseSync> ucGetTokenSyncProvider) {
    this.loginUtilProvider = loginUtilProvider;
    this.authServiceProvider = authServiceProvider;
    this.ahpProvider = ahpProvider;
    this.executorsProvider = executorsProvider;
    this.ucGetTokenSyncProvider = ucGetTokenSyncProvider;
  }

  public static MembersInjector<AuthenticationActivity> create(
      Provider<LoginUtil> loginUtilProvider, Provider<AuthService> authServiceProvider,
      Provider<AuthorizationHeaderProvider> ahpProvider, Provider<AppExecutors> executorsProvider,
      Provider<GetTokenUseCaseSync> ucGetTokenSyncProvider) {
    return new AuthenticationActivity_MembersInjector(loginUtilProvider, authServiceProvider, ahpProvider, executorsProvider, ucGetTokenSyncProvider);
  }

  @Override
  public void injectMembers(AuthenticationActivity instance) {
    injectLoginUtil(instance, loginUtilProvider.get());
    injectAuthService(instance, authServiceProvider.get());
    injectAhp(instance, ahpProvider.get());
    injectExecutors(instance, executorsProvider.get());
    injectUcGetTokenSync(instance, ucGetTokenSyncProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity.loginUtil")
  public static void injectLoginUtil(AuthenticationActivity instance, LoginUtil loginUtil) {
    instance.loginUtil = loginUtil;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity.authService")
  public static void injectAuthService(AuthenticationActivity instance, AuthService authService) {
    instance.authService = authService;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity.ahp")
  public static void injectAhp(AuthenticationActivity instance, AuthorizationHeaderProvider ahp) {
    instance.ahp = ahp;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity.executors")
  public static void injectExecutors(AuthenticationActivity instance, AppExecutors executors) {
    instance.executors = executors;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity.ucGetTokenSync")
  public static void injectUcGetTokenSync(AuthenticationActivity instance,
      GetTokenUseCaseSync ucGetTokenSync) {
    instance.ucGetTokenSync = ucGetTokenSync;
  }
}
