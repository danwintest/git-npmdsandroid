package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class OutletBalanceBrowseVM_AssistedFactory implements ViewModelAssistedFactory<OutletBalanceBrowseVM> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<GetOutletBalanceListUseCase> getOutletBalanceListUseCase;

  @Inject
  OutletBalanceBrowseVM_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors,
      Provider<GetOutletBalanceListUseCase> getOutletBalanceListUseCase) {
    this.application = application;
    this.appExecutors = appExecutors;
    this.getOutletBalanceListUseCase = getOutletBalanceListUseCase;
  }

  @Override
  @NonNull
  public OutletBalanceBrowseVM create(@NonNull SavedStateHandle arg0) {
    return new OutletBalanceBrowseVM(application.get(), appExecutors.get(),
        getOutletBalanceListUseCase.get());
  }
}
