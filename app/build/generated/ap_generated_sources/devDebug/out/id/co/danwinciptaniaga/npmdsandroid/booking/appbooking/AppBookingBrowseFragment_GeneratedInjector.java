package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = AppBookingBrowseFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface AppBookingBrowseFragment_GeneratedInjector {
  void injectAppBookingBrowseFragment(AppBookingBrowseFragment appBookingBrowseFragment);
}
