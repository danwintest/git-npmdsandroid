package id.co.danwinciptaniaga.npmdsandroid.booking;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = BookingBrowseFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface BookingBrowseFragment_GeneratedInjector {
  void injectBookingBrowseFragment(BookingBrowseFragment bookingBrowseFragment);
}
