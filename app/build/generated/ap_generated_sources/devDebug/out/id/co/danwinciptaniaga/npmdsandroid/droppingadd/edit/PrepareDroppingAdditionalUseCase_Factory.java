package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class PrepareDroppingAdditionalUseCase_Factory implements Factory<PrepareDroppingAdditionalUseCase> {
  private final Provider<Application> appProvider;

  private final Provider<DroppingAdditionalService> dasProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public PrepareDroppingAdditionalUseCase_Factory(Provider<Application> appProvider,
      Provider<DroppingAdditionalService> dasProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.dasProvider = dasProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public PrepareDroppingAdditionalUseCase get() {
    return newInstance(appProvider.get(), dasProvider.get(), appExecutorsProvider.get());
  }

  public static PrepareDroppingAdditionalUseCase_Factory create(Provider<Application> appProvider,
      Provider<DroppingAdditionalService> dasProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new PrepareDroppingAdditionalUseCase_Factory(appProvider, dasProvider, appExecutorsProvider);
  }

  public static PrepareDroppingAdditionalUseCase newInstance(Application app,
      DroppingAdditionalService das, AppExecutors appExecutors) {
    return new PrepareDroppingAdditionalUseCase(app, das, appExecutors);
  }
}
