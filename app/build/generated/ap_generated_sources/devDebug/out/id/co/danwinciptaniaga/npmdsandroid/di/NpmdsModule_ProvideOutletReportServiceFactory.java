package id.co.danwinciptaniaga.npmdsandroid.di;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import id.co.danwinciptaniaga.androcon.retrofit.WebServiceGenerator;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletReportService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NpmdsModule_ProvideOutletReportServiceFactory implements Factory<OutletReportService> {
  private final NpmdsModule module;

  private final Provider<WebServiceGenerator> webServiceGeneratorProvider;

  public NpmdsModule_ProvideOutletReportServiceFactory(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    this.module = module;
    this.webServiceGeneratorProvider = webServiceGeneratorProvider;
  }

  @Override
  public OutletReportService get() {
    return provideOutletReportService(module, webServiceGeneratorProvider.get());
  }

  public static NpmdsModule_ProvideOutletReportServiceFactory create(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    return new NpmdsModule_ProvideOutletReportServiceFactory(module, webServiceGeneratorProvider);
  }

  public static OutletReportService provideOutletReportService(NpmdsModule instance,
      WebServiceGenerator webServiceGenerator) {
    return Preconditions.checkNotNull(instance.provideOutletReportService(webServiceGenerator), "Cannot return null from a non-@Nullable @Provides method");
  }
}
