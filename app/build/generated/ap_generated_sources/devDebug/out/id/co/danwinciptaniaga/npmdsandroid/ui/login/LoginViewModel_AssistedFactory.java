package id.co.danwinciptaniaga.npmdsandroid.ui.login;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCase;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceUseCaseSync;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class LoginViewModel_AssistedFactory implements ViewModelAssistedFactory<LoginViewModel> {
  private final Provider<Application> application;

  private final Provider<GetTokenUseCase> getTokenUseCase;

  private final Provider<RegisterDeviceUseCaseSync> registerDeviceUseCaseSync;

  @Inject
  LoginViewModel_AssistedFactory(Provider<Application> application,
      Provider<GetTokenUseCase> getTokenUseCase,
      Provider<RegisterDeviceUseCaseSync> registerDeviceUseCaseSync) {
    this.application = application;
    this.getTokenUseCase = getTokenUseCase;
    this.registerDeviceUseCaseSync = registerDeviceUseCaseSync;
  }

  @Override
  @NonNull
  public LoginViewModel create(@NonNull SavedStateHandle arg0) {
    return new LoginViewModel(application.get(), getTokenUseCase.get(),
        registerDeviceUseCaseSync.get(), arg0);
  }
}
