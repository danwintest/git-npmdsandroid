package id.co.danwinciptaniaga.npmdsandroid.booking;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class BookingBrowseViewModel_AssistedFactory implements ViewModelAssistedFactory<BookingBrowseViewModel> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<GetBookingListUseCase> getListUC;

  @Inject
  BookingBrowseViewModel_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<GetBookingListUseCase> getListUC) {
    this.application = application;
    this.appExecutors = appExecutors;
    this.getListUC = getListUC;
  }

  @Override
  @NonNull
  public BookingBrowseViewModel create(@NonNull SavedStateHandle arg0) {
    return new BookingBrowseViewModel(application.get(), appExecutors.get(), getListUC.get());
  }
}
