package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class AppBookingBrowseVM_AssistedFactory implements ViewModelAssistedFactory<AppBookingBrowseVM> {
  private final Provider<Application> app;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<AppBookingBrowseUC> getListUC;

  private final Provider<AppBookingListAction> appBookingListAction;

  @Inject
  AppBookingBrowseVM_AssistedFactory(Provider<Application> app, Provider<AppExecutors> appExecutors,
      Provider<AppBookingBrowseUC> getListUC, Provider<AppBookingListAction> appBookingListAction) {
    this.app = app;
    this.appExecutors = appExecutors;
    this.getListUC = getListUC;
    this.appBookingListAction = appBookingListAction;
  }

  @Override
  @NonNull
  public AppBookingBrowseVM create(@NonNull SavedStateHandle arg0) {
    return new AppBookingBrowseVM(app.get(), appExecutors.get(), getListUC.get(),
        appBookingListAction.get());
  }
}
