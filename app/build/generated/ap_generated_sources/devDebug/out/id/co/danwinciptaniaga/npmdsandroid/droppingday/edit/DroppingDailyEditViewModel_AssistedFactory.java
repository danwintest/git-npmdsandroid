package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingDailyEditViewModel_AssistedFactory implements ViewModelAssistedFactory<DroppingDailyEditViewModel> {
  private final Provider<Application> application;

  private final Provider<LoadDroppingDailyUseCase> loadDroppingDailyUseCase;

  private final Provider<DroppingDailySupervisorDecideUseCase> droppingDailySupervisorDecideUseCase;

  private final Provider<UtilityService> utilityService;

  private final Provider<AppExecutors> appExecutors;

  @Inject
  DroppingDailyEditViewModel_AssistedFactory(Provider<Application> application,
      Provider<LoadDroppingDailyUseCase> loadDroppingDailyUseCase,
      Provider<DroppingDailySupervisorDecideUseCase> droppingDailySupervisorDecideUseCase,
      Provider<UtilityService> utilityService, Provider<AppExecutors> appExecutors) {
    this.application = application;
    this.loadDroppingDailyUseCase = loadDroppingDailyUseCase;
    this.droppingDailySupervisorDecideUseCase = droppingDailySupervisorDecideUseCase;
    this.utilityService = utilityService;
    this.appExecutors = appExecutors;
  }

  @Override
  @NonNull
  public DroppingDailyEditViewModel create(@NonNull SavedStateHandle arg0) {
    return new DroppingDailyEditViewModel(application.get(), loadDroppingDailyUseCase.get(),
        droppingDailySupervisorDecideUseCase.get(), utilityService.get(), appExecutors.get());
  }
}
