package id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class SelectBookingViewModel_AssistedFactory_Factory implements Factory<SelectBookingViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<GetBookingMediatorListUseCase> getBookingMediatorListUseCaseProvider;

  public SelectBookingViewModel_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<GetBookingMediatorListUseCase> getBookingMediatorListUseCaseProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.getBookingMediatorListUseCaseProvider = getBookingMediatorListUseCaseProvider;
  }

  @Override
  public SelectBookingViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider, getBookingMediatorListUseCaseProvider);
  }

  public static SelectBookingViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<GetBookingMediatorListUseCase> getBookingMediatorListUseCaseProvider) {
    return new SelectBookingViewModel_AssistedFactory_Factory(applicationProvider, appExecutorsProvider, getBookingMediatorListUseCaseProvider);
  }

  public static SelectBookingViewModel_AssistedFactory newInstance(
      Provider<Application> application, Provider<AppExecutors> appExecutors,
      Provider<GetBookingMediatorListUseCase> getBookingMediatorListUseCase) {
    return new SelectBookingViewModel_AssistedFactory(application, appExecutors, getBookingMediatorListUseCase);
  }
}
