package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import android.content.Context;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TugasTransferHeaderAdapter_Factory implements Factory<TugasTransferHeaderAdapter> {
  private final Provider<TugasTransferHeaderAdapter.ItemListener> listenerProvider;

  private final Provider<Context> ctxProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public TugasTransferHeaderAdapter_Factory(
      Provider<TugasTransferHeaderAdapter.ItemListener> listenerProvider,
      Provider<Context> ctxProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.listenerProvider = listenerProvider;
    this.ctxProvider = ctxProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public TugasTransferHeaderAdapter get() {
    return newInstance(listenerProvider.get(), ctxProvider.get(), appExecutorsProvider.get());
  }

  public static TugasTransferHeaderAdapter_Factory create(
      Provider<TugasTransferHeaderAdapter.ItemListener> listenerProvider,
      Provider<Context> ctxProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new TugasTransferHeaderAdapter_Factory(listenerProvider, ctxProvider, appExecutorsProvider);
  }

  public static TugasTransferHeaderAdapter newInstance(Object listener, Context ctx,
      AppExecutors appExecutors) {
    return new TugasTransferHeaderAdapter((TugasTransferHeaderAdapter.ItemListener) listener, ctx, appExecutors);
  }
}
