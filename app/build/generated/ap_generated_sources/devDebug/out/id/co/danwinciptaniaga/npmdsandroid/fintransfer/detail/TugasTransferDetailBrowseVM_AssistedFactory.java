package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class TugasTransferDetailBrowseVM_AssistedFactory implements ViewModelAssistedFactory<TugasTransferDetailBrowseVM> {
  private final Provider<Application> app;

  private final Provider<GetTugasTransferDetailBrowseUC> uc;

  private final Provider<AppExecutors> appExecutors;

  @Inject
  TugasTransferDetailBrowseVM_AssistedFactory(Provider<Application> app,
      Provider<GetTugasTransferDetailBrowseUC> uc, Provider<AppExecutors> appExecutors) {
    this.app = app;
    this.uc = uc;
    this.appExecutors = appExecutors;
  }

  @Override
  @NonNull
  public TugasTransferDetailBrowseVM create(@NonNull SavedStateHandle arg0) {
    return new TugasTransferDetailBrowseVM(app.get(), uc.get(), appExecutors.get());
  }
}
