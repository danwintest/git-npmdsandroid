package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.GetTugasTransferFilterUC;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TugasTransferHeaderBrowseVM_AssistedFactory_Factory implements Factory<TugasTransferHeaderBrowseVM_AssistedFactory> {
  private final Provider<Application> appProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<GetTugasTransferBrowseUC> ucHeaderProvider;

  private final Provider<GetTugasTransferFilterUC> ucFilterProvider;

  public TugasTransferHeaderBrowseVM_AssistedFactory_Factory(Provider<Application> appProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<GetTugasTransferBrowseUC> ucHeaderProvider,
      Provider<GetTugasTransferFilterUC> ucFilterProvider) {
    this.appProvider = appProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.ucHeaderProvider = ucHeaderProvider;
    this.ucFilterProvider = ucFilterProvider;
  }

  @Override
  public TugasTransferHeaderBrowseVM_AssistedFactory get() {
    return newInstance(appProvider, appExecutorsProvider, ucHeaderProvider, ucFilterProvider);
  }

  public static TugasTransferHeaderBrowseVM_AssistedFactory_Factory create(
      Provider<Application> appProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<GetTugasTransferBrowseUC> ucHeaderProvider,
      Provider<GetTugasTransferFilterUC> ucFilterProvider) {
    return new TugasTransferHeaderBrowseVM_AssistedFactory_Factory(appProvider, appExecutorsProvider, ucHeaderProvider, ucFilterProvider);
  }

  public static TugasTransferHeaderBrowseVM_AssistedFactory newInstance(Provider<Application> app,
      Provider<AppExecutors> appExecutors, Provider<GetTugasTransferBrowseUC> ucHeader,
      Provider<GetTugasTransferFilterUC> ucFilter) {
    return new TugasTransferHeaderBrowseVM_AssistedFactory(app, appExecutors, ucHeader, ucFilter);
  }
}
