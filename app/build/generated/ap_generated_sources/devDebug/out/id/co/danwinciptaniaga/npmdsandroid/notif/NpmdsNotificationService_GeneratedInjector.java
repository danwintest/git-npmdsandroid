package id.co.danwinciptaniaga.npmdsandroid.notif;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ServiceComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = NpmdsNotificationService.class
)
@GeneratedEntryPoint
@InstallIn(ServiceComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface NpmdsNotificationService_GeneratedInjector {
  void injectNpmdsNotificationService(NpmdsNotificationService npmdsNotificationService);
}
