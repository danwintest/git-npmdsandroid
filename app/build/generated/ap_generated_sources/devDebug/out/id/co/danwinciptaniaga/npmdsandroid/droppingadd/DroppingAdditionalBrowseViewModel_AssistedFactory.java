package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingAdditionalBrowseViewModel_AssistedFactory implements ViewModelAssistedFactory<DroppingAdditionalBrowseViewModel> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<GetDroppingAdditionalListUseCase> getDroppingAdditionalListUseCase;

  private final Provider<DroppingListActionUC> droppingListActionUC;

  @Inject
  DroppingAdditionalBrowseViewModel_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors,
      Provider<GetDroppingAdditionalListUseCase> getDroppingAdditionalListUseCase,
      Provider<DroppingListActionUC> droppingListActionUC) {
    this.application = application;
    this.appExecutors = appExecutors;
    this.getDroppingAdditionalListUseCase = getDroppingAdditionalListUseCase;
    this.droppingListActionUC = droppingListActionUC;
  }

  @Override
  @NonNull
  public DroppingAdditionalBrowseViewModel create(@NonNull SavedStateHandle arg0) {
    return new DroppingAdditionalBrowseViewModel(application.get(), appExecutors.get(),
        getDroppingAdditionalListUseCase.get(), droppingListActionUC.get());
  }
}
