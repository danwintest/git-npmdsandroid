package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ExpenseSaveDraftUseCase_Factory implements Factory<ExpenseSaveDraftUseCase> {
  private final Provider<Application> applicationProvider;

  private final Provider<ExpenseService> expenseServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public ExpenseSaveDraftUseCase_Factory(Provider<Application> applicationProvider,
      Provider<ExpenseService> expenseServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.expenseServiceProvider = expenseServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public ExpenseSaveDraftUseCase get() {
    return newInstance(applicationProvider.get(), expenseServiceProvider.get(), appExecutorsProvider.get());
  }

  public static ExpenseSaveDraftUseCase_Factory create(Provider<Application> applicationProvider,
      Provider<ExpenseService> expenseServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new ExpenseSaveDraftUseCase_Factory(applicationProvider, expenseServiceProvider, appExecutorsProvider);
  }

  public static ExpenseSaveDraftUseCase newInstance(Application application,
      ExpenseService expenseService, AppExecutors appExecutors) {
    return new ExpenseSaveDraftUseCase(application, expenseService, appExecutors);
  }
}
