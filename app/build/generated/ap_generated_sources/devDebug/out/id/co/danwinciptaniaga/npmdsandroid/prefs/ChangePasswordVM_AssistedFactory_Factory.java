package id.co.danwinciptaniaga.npmdsandroid.prefs;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ChangePasswordVM_AssistedFactory_Factory implements Factory<ChangePasswordVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<ChangePasswordUC> changePasswordUCProvider;

  public ChangePasswordVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<ChangePasswordUC> changePasswordUCProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.changePasswordUCProvider = changePasswordUCProvider;
  }

  @Override
  public ChangePasswordVM_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider, changePasswordUCProvider);
  }

  public static ChangePasswordVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<ChangePasswordUC> changePasswordUCProvider) {
    return new ChangePasswordVM_AssistedFactory_Factory(applicationProvider, appExecutorsProvider, changePasswordUCProvider);
  }

  public static ChangePasswordVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<ChangePasswordUC> changePasswordUC) {
    return new ChangePasswordVM_AssistedFactory(application, appExecutors, changePasswordUC);
  }
}
