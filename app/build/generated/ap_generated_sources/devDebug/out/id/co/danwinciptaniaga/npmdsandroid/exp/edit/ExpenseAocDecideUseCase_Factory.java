package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ExpenseAocDecideUseCase_Factory implements Factory<ExpenseAocDecideUseCase> {
  private final Provider<Application> applicationProvider;

  private final Provider<ExpenseService> expenseServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public ExpenseAocDecideUseCase_Factory(Provider<Application> applicationProvider,
      Provider<ExpenseService> expenseServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.expenseServiceProvider = expenseServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public ExpenseAocDecideUseCase get() {
    return newInstance(applicationProvider.get(), expenseServiceProvider.get(), appExecutorsProvider.get());
  }

  public static ExpenseAocDecideUseCase_Factory create(Provider<Application> applicationProvider,
      Provider<ExpenseService> expenseServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new ExpenseAocDecideUseCase_Factory(applicationProvider, expenseServiceProvider, appExecutorsProvider);
  }

  public static ExpenseAocDecideUseCase newInstance(Application application,
      ExpenseService expenseService, AppExecutors appExecutors) {
    return new ExpenseAocDecideUseCase(application, expenseService, appExecutors);
  }
}
