package id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TugasTransferFilterVM_AssistedFactory_Factory implements Factory<TugasTransferFilterVM_AssistedFactory> {
  private final Provider<Application> appProvider;

  private final Provider<GetTugasTransferFilterUC> ucProvider;

  public TugasTransferFilterVM_AssistedFactory_Factory(Provider<Application> appProvider,
      Provider<GetTugasTransferFilterUC> ucProvider) {
    this.appProvider = appProvider;
    this.ucProvider = ucProvider;
  }

  @Override
  public TugasTransferFilterVM_AssistedFactory get() {
    return newInstance(appProvider, ucProvider);
  }

  public static TugasTransferFilterVM_AssistedFactory_Factory create(
      Provider<Application> appProvider, Provider<GetTugasTransferFilterUC> ucProvider) {
    return new TugasTransferFilterVM_AssistedFactory_Factory(appProvider, ucProvider);
  }

  public static TugasTransferFilterVM_AssistedFactory newInstance(Provider<Application> app,
      Provider<GetTugasTransferFilterUC> uc) {
    return new TugasTransferFilterVM_AssistedFactory(app, uc);
  }
}
