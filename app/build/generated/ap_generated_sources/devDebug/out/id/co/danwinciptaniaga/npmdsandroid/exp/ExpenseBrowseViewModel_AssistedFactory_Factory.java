package id.co.danwinciptaniaga.npmdsandroid.exp;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ExpenseBrowseViewModel_AssistedFactory_Factory implements Factory<ExpenseBrowseViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<GetExpenseListUseCase> getExpenseListUseCaseProvider;

  private final Provider<ExpenseListAction> expListActionProvider;

  public ExpenseBrowseViewModel_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<GetExpenseListUseCase> getExpenseListUseCaseProvider,
      Provider<ExpenseListAction> expListActionProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.getExpenseListUseCaseProvider = getExpenseListUseCaseProvider;
    this.expListActionProvider = expListActionProvider;
  }

  @Override
  public ExpenseBrowseViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider, getExpenseListUseCaseProvider, expListActionProvider);
  }

  public static ExpenseBrowseViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<GetExpenseListUseCase> getExpenseListUseCaseProvider,
      Provider<ExpenseListAction> expListActionProvider) {
    return new ExpenseBrowseViewModel_AssistedFactory_Factory(applicationProvider, appExecutorsProvider, getExpenseListUseCaseProvider, expListActionProvider);
  }

  public static ExpenseBrowseViewModel_AssistedFactory newInstance(
      Provider<Application> application, Provider<AppExecutors> appExecutors,
      Provider<GetExpenseListUseCase> getExpenseListUseCase,
      Provider<ExpenseListAction> expListAction) {
    return new ExpenseBrowseViewModel_AssistedFactory(application, appExecutors, getExpenseListUseCase, expListAction);
  }
}
