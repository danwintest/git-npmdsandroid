package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingLoadUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingPrepareUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingProcessUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingTransferVM_AssistedFactory_Factory implements Factory<AppBookingTransferVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<AppBookingPrepareUC> prepareUCProvider;

  private final Provider<AppBookingLoadUC> appBookingLoadUCProvider;

  private final Provider<AppBookingProcessUC> processUCProvider;

  private final Provider<UtilityService> utilityServiceProvider;

  private final Provider<AppBookingService> appBookingServiceProvider;

  public AppBookingTransferVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<AppBookingPrepareUC> prepareUCProvider,
      Provider<AppBookingLoadUC> appBookingLoadUCProvider,
      Provider<AppBookingProcessUC> processUCProvider,
      Provider<UtilityService> utilityServiceProvider,
      Provider<AppBookingService> appBookingServiceProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.prepareUCProvider = prepareUCProvider;
    this.appBookingLoadUCProvider = appBookingLoadUCProvider;
    this.processUCProvider = processUCProvider;
    this.utilityServiceProvider = utilityServiceProvider;
    this.appBookingServiceProvider = appBookingServiceProvider;
  }

  @Override
  public AppBookingTransferVM_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider, prepareUCProvider, appBookingLoadUCProvider, processUCProvider, utilityServiceProvider, appBookingServiceProvider);
  }

  public static AppBookingTransferVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<AppBookingPrepareUC> prepareUCProvider,
      Provider<AppBookingLoadUC> appBookingLoadUCProvider,
      Provider<AppBookingProcessUC> processUCProvider,
      Provider<UtilityService> utilityServiceProvider,
      Provider<AppBookingService> appBookingServiceProvider) {
    return new AppBookingTransferVM_AssistedFactory_Factory(applicationProvider, appExecutorsProvider, prepareUCProvider, appBookingLoadUCProvider, processUCProvider, utilityServiceProvider, appBookingServiceProvider);
  }

  public static AppBookingTransferVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<AppBookingPrepareUC> prepareUC,
      Provider<AppBookingLoadUC> appBookingLoadUC, Provider<AppBookingProcessUC> processUC,
      Provider<UtilityService> utilityService, Provider<AppBookingService> appBookingService) {
    return new AppBookingTransferVM_AssistedFactory(application, appExecutors, prepareUC, appBookingLoadUC, processUC, utilityService, appBookingService);
  }
}
