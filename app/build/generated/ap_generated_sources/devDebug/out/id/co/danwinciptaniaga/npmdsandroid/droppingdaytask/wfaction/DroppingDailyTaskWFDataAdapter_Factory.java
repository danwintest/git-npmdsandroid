package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import android.content.Context;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import java.util.List;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskWFDataAdapter_Factory implements Factory<DroppingDailyTaskWFDataAdapter> {
  private final Provider<Context> ctxProvider;

  private final Provider<List<DroppingDailyTaskBrowseDetailData>> dataListProvider;

  public DroppingDailyTaskWFDataAdapter_Factory(Provider<Context> ctxProvider,
      Provider<List<DroppingDailyTaskBrowseDetailData>> dataListProvider) {
    this.ctxProvider = ctxProvider;
    this.dataListProvider = dataListProvider;
  }

  @Override
  public DroppingDailyTaskWFDataAdapter get() {
    return newInstance(ctxProvider.get(), dataListProvider.get());
  }

  public static DroppingDailyTaskWFDataAdapter_Factory create(Provider<Context> ctxProvider,
      Provider<List<DroppingDailyTaskBrowseDetailData>> dataListProvider) {
    return new DroppingDailyTaskWFDataAdapter_Factory(ctxProvider, dataListProvider);
  }

  public static DroppingDailyTaskWFDataAdapter newInstance(Context ctx,
      List<DroppingDailyTaskBrowseDetailData> dataList) {
    return new DroppingDailyTaskWFDataAdapter(ctx, dataList);
  }
}
