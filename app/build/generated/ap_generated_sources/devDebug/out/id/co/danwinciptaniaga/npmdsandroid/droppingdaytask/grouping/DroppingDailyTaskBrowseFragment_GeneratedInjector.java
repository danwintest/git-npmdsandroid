package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = DroppingDailyTaskBrowseFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface DroppingDailyTaskBrowseFragment_GeneratedInjector {
  void injectDroppingDailyTaskBrowseFragment(
      DroppingDailyTaskBrowseFragment droppingDailyTaskBrowseFragment);
}
