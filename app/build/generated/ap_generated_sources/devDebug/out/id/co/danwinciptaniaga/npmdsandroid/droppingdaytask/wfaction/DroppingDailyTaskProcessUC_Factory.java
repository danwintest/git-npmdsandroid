package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.DroppingDailyTaskService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskProcessUC_Factory implements Factory<DroppingDailyTaskProcessUC> {
  private final Provider<Application> appProvider;

  private final Provider<DroppingDailyTaskService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailyTaskProcessUC_Factory(Provider<Application> appProvider,
      Provider<DroppingDailyTaskService> serviceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingDailyTaskProcessUC get() {
    return newInstance(appProvider.get(), serviceProvider.get(), appExecutorsProvider.get());
  }

  public static DroppingDailyTaskProcessUC_Factory create(Provider<Application> appProvider,
      Provider<DroppingDailyTaskService> serviceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailyTaskProcessUC_Factory(appProvider, serviceProvider, appExecutorsProvider);
  }

  public static DroppingDailyTaskProcessUC newInstance(Application app,
      DroppingDailyTaskService service, AppExecutors appExecutors) {
    return new DroppingDailyTaskProcessUC(app, service, appExecutors);
  }
}
