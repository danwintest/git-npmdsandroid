package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingBrowseUC_Factory implements Factory<AppBookingBrowseUC> {
  private final Provider<AppBookingService> appBookingServiceProvider;

  public AppBookingBrowseUC_Factory(Provider<AppBookingService> appBookingServiceProvider) {
    this.appBookingServiceProvider = appBookingServiceProvider;
  }

  @Override
  public AppBookingBrowseUC get() {
    return newInstance(appBookingServiceProvider.get());
  }

  public static AppBookingBrowseUC_Factory create(
      Provider<AppBookingService> appBookingServiceProvider) {
    return new AppBookingBrowseUC_Factory(appBookingServiceProvider);
  }

  public static AppBookingBrowseUC newInstance(AppBookingService appBookingService) {
    return new AppBookingBrowseUC(appBookingService);
  }
}
