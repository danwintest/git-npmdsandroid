package id.co.danwinciptaniaga.npmdsandroid.di;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import id.co.danwinciptaniaga.androcon.retrofit.WebServiceGenerator;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NpmdsModule_ProvideTugasTransferServiceFactory implements Factory<TugasTransferService> {
  private final NpmdsModule module;

  private final Provider<WebServiceGenerator> webServiceGeneratorProvider;

  public NpmdsModule_ProvideTugasTransferServiceFactory(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    this.module = module;
    this.webServiceGeneratorProvider = webServiceGeneratorProvider;
  }

  @Override
  public TugasTransferService get() {
    return provideTugasTransferService(module, webServiceGeneratorProvider.get());
  }

  public static NpmdsModule_ProvideTugasTransferServiceFactory create(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    return new NpmdsModule_ProvideTugasTransferServiceFactory(module, webServiceGeneratorProvider);
  }

  public static TugasTransferService provideTugasTransferService(NpmdsModule instance,
      WebServiceGenerator webServiceGenerator) {
    return Preconditions.checkNotNull(instance.provideTugasTransferService(webServiceGenerator), "Cannot return null from a non-@Nullable @Provides method");
  }
}
