package id.co.danwinciptaniaga.npmdsandroid.droppingday;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetDroppingDailyListUC_Factory implements Factory<GetDroppingDailyListUC> {
  private final Provider<DroppingDailyService> droppingServiceProvider;

  public GetDroppingDailyListUC_Factory(Provider<DroppingDailyService> droppingServiceProvider) {
    this.droppingServiceProvider = droppingServiceProvider;
  }

  @Override
  public GetDroppingDailyListUC get() {
    return newInstance(droppingServiceProvider.get());
  }

  public static GetDroppingDailyListUC_Factory create(
      Provider<DroppingDailyService> droppingServiceProvider) {
    return new GetDroppingDailyListUC_Factory(droppingServiceProvider);
  }

  public static GetDroppingDailyListUC newInstance(DroppingDailyService droppingService) {
    return new GetDroppingDailyListUC(droppingService);
  }
}
