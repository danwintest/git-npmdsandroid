package id.co.danwinciptaniaga.npmdsandroid.booking;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class BookingFilterVM_AssistedFactory implements ViewModelAssistedFactory<BookingFilterVM> {
  private final Provider<Application> application;

  private final Provider<GetBookingFilterUC> ucService;

  @Inject
  BookingFilterVM_AssistedFactory(Provider<Application> application,
      Provider<GetBookingFilterUC> ucService) {
    this.application = application;
    this.ucService = ucService;
  }

  @Override
  @NonNull
  public BookingFilterVM create(@NonNull SavedStateHandle arg0) {
    return new BookingFilterVM(application.get(), ucService.get());
  }
}
