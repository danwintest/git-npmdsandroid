package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingAdditionalBrowseFragment_MembersInjector implements MembersInjector<DroppingAdditionalBrowseFragment> {
  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingAdditionalBrowseFragment_MembersInjector(
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<DroppingAdditionalBrowseFragment> create(
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingAdditionalBrowseFragment_MembersInjector(commonServiceProvider, appExecutorsProvider);
  }

  @Override
  public void injectMembers(DroppingAdditionalBrowseFragment instance) {
    injectCommonService(instance, commonServiceProvider.get());
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseFragment.commonService")
  public static void injectCommonService(DroppingAdditionalBrowseFragment instance,
      CommonService commonService) {
    instance.commonService = commonService;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseFragment.appExecutors")
  public static void injectAppExecutors(DroppingAdditionalBrowseFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
