package id.co.danwinciptaniaga.npmdsandroid.outlet;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = OutletStockByDayReportFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface OutletStockByDayReportFragment_GeneratedInjector {
  void injectOutletStockByDayReportFragment(
      OutletStockByDayReportFragment outletStockByDayReportFragment);
}
