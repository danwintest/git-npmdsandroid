package id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class TugasTransferFilterVM_AssistedFactory implements ViewModelAssistedFactory<TugasTransferFilterVM> {
  private final Provider<Application> app;

  private final Provider<GetTugasTransferFilterUC> uc;

  @Inject
  TugasTransferFilterVM_AssistedFactory(Provider<Application> app,
      Provider<GetTugasTransferFilterUC> uc) {
    this.app = app;
    this.uc = uc;
  }

  @Override
  @NonNull
  public TugasTransferFilterVM create(@NonNull SavedStateHandle arg0) {
    return new TugasTransferFilterVM(app.get(), uc.get());
  }
}
