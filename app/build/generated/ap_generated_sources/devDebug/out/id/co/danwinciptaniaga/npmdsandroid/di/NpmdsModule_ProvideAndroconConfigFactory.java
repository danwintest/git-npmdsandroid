package id.co.danwinciptaniaga.npmdsandroid.di;

import android.content.Context;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NpmdsModule_ProvideAndroconConfigFactory implements Factory<AndroconConfig> {
  private final NpmdsModule module;

  private final Provider<Context> contextProvider;

  public NpmdsModule_ProvideAndroconConfigFactory(NpmdsModule module,
      Provider<Context> contextProvider) {
    this.module = module;
    this.contextProvider = contextProvider;
  }

  @Override
  public AndroconConfig get() {
    return provideAndroconConfig(module, contextProvider.get());
  }

  public static NpmdsModule_ProvideAndroconConfigFactory create(NpmdsModule module,
      Provider<Context> contextProvider) {
    return new NpmdsModule_ProvideAndroconConfigFactory(module, contextProvider);
  }

  public static AndroconConfig provideAndroconConfig(NpmdsModule instance, Context context) {
    return Preconditions.checkNotNull(instance.provideAndroconConfig(context), "Cannot return null from a non-@Nullable @Provides method");
  }
}
