package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class TugasTransferCekSaldoVM_AssistedFactory implements ViewModelAssistedFactory<TugasTransferCekSaldoVM> {
  private final Provider<Application> application;

  private final Provider<TugasTransferCekSaldoUC> uc;

  private final Provider<TugasTransferCekSaldoBankListUC> bankListUC;

  private final Provider<TugasTransferCekSaldoAccountListUC> accountListUC;

  @Inject
  TugasTransferCekSaldoVM_AssistedFactory(Provider<Application> application,
      Provider<TugasTransferCekSaldoUC> uc, Provider<TugasTransferCekSaldoBankListUC> bankListUC,
      Provider<TugasTransferCekSaldoAccountListUC> accountListUC) {
    this.application = application;
    this.uc = uc;
    this.bankListUC = bankListUC;
    this.accountListUC = accountListUC;
  }

  @Override
  @NonNull
  public TugasTransferCekSaldoVM create(@NonNull SavedStateHandle arg0) {
    return new TugasTransferCekSaldoVM(application.get(), uc.get(), bankListUC.get(),
        accountListUC.get());
  }
}
