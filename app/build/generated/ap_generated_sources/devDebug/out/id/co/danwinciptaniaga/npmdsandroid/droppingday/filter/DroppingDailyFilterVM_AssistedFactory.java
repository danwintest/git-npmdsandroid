package id.co.danwinciptaniaga.npmdsandroid.droppingday.filter;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingDailyFilterVM_AssistedFactory implements ViewModelAssistedFactory<DroppingDailyFilterVM> {
  private final Provider<Application> app;

  private final Provider<GetDroppingDailyFilterUC> uc;

  @Inject
  DroppingDailyFilterVM_AssistedFactory(Provider<Application> app,
      Provider<GetDroppingDailyFilterUC> uc) {
    this.app = app;
    this.uc = uc;
  }

  @Override
  @NonNull
  public DroppingDailyFilterVM create(@NonNull SavedStateHandle arg0) {
    return new DroppingDailyFilterVM(app.get(), uc.get());
  }
}
