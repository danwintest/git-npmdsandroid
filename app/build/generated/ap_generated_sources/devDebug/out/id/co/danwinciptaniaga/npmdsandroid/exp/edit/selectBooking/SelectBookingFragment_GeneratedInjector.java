package id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = SelectBookingFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface SelectBookingFragment_GeneratedInjector {
  void injectSelectBookingFragment(SelectBookingFragment selectBookingFragment);
}
