package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingAdditionalDetailEditViewModel_AssistedFactory_Factory implements Factory<DroppingAdditionalDetailEditViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingAdditionalDetailEditViewModel_AssistedFactory_Factory(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingAdditionalDetailEditViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider);
  }

  public static DroppingAdditionalDetailEditViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingAdditionalDetailEditViewModel_AssistedFactory_Factory(applicationProvider, appExecutorsProvider);
  }

  public static DroppingAdditionalDetailEditViewModel_AssistedFactory newInstance(
      Provider<Application> application, Provider<AppExecutors> appExecutors) {
    return new DroppingAdditionalDetailEditViewModel_AssistedFactory(application, appExecutors);
  }
}
