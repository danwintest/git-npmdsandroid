package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.npmdsandroid.booking.GetBookingFilterUC;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class AppBookingFilterVM_AssistedFactory implements ViewModelAssistedFactory<AppBookingFilterVM> {
  private final Provider<Application> application;

  private final Provider<GetBookingFilterUC> ucService;

  @Inject
  AppBookingFilterVM_AssistedFactory(Provider<Application> application,
      Provider<GetBookingFilterUC> ucService) {
    this.application = application;
    this.ucService = ucService;
  }

  @Override
  @NonNull
  public AppBookingFilterVM create(@NonNull SavedStateHandle arg0) {
    return new AppBookingFilterVM(application.get(), ucService.get());
  }
}
