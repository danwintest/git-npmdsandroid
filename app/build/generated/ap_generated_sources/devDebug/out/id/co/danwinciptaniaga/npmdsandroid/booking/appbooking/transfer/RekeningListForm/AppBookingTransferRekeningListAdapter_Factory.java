package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import android.content.Context;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingTransferRekeningListAdapter_Factory implements Factory<AppBookingTransferRekeningListAdapter> {
  private final Provider<AppBookingTransferRekeningListAdapter.Listener> listenerProvider;

  private final Provider<Context> ctxProvider;

  public AppBookingTransferRekeningListAdapter_Factory(
      Provider<AppBookingTransferRekeningListAdapter.Listener> listenerProvider,
      Provider<Context> ctxProvider) {
    this.listenerProvider = listenerProvider;
    this.ctxProvider = ctxProvider;
  }

  @Override
  public AppBookingTransferRekeningListAdapter get() {
    return newInstance(listenerProvider.get(), ctxProvider.get());
  }

  public static AppBookingTransferRekeningListAdapter_Factory create(
      Provider<AppBookingTransferRekeningListAdapter.Listener> listenerProvider,
      Provider<Context> ctxProvider) {
    return new AppBookingTransferRekeningListAdapter_Factory(listenerProvider, ctxProvider);
  }

  public static AppBookingTransferRekeningListAdapter newInstance(Object listener, Context ctx) {
    return new AppBookingTransferRekeningListAdapter((AppBookingTransferRekeningListAdapter.Listener) listener, ctx);
  }
}
