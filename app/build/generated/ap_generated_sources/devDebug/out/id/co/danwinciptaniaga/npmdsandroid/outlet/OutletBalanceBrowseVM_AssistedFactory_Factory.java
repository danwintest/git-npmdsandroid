package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class OutletBalanceBrowseVM_AssistedFactory_Factory implements Factory<OutletBalanceBrowseVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<GetOutletBalanceListUseCase> getOutletBalanceListUseCaseProvider;

  public OutletBalanceBrowseVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<GetOutletBalanceListUseCase> getOutletBalanceListUseCaseProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.getOutletBalanceListUseCaseProvider = getOutletBalanceListUseCaseProvider;
  }

  @Override
  public OutletBalanceBrowseVM_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider, getOutletBalanceListUseCaseProvider);
  }

  public static OutletBalanceBrowseVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<GetOutletBalanceListUseCase> getOutletBalanceListUseCaseProvider) {
    return new OutletBalanceBrowseVM_AssistedFactory_Factory(applicationProvider, appExecutorsProvider, getOutletBalanceListUseCaseProvider);
  }

  public static OutletBalanceBrowseVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<AppExecutors> appExecutors,
      Provider<GetOutletBalanceListUseCase> getOutletBalanceListUseCase) {
    return new OutletBalanceBrowseVM_AssistedFactory(application, appExecutors, getOutletBalanceListUseCase);
  }
}
