package id.co.danwinciptaniaga.npmdsandroid.exp;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ExpenseListAction_Factory implements Factory<ExpenseListAction> {
  private final Provider<ExpenseService> expenseServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<Application> applicationProvider;

  public ExpenseListAction_Factory(Provider<ExpenseService> expenseServiceProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<Application> applicationProvider) {
    this.expenseServiceProvider = expenseServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.applicationProvider = applicationProvider;
  }

  @Override
  public ExpenseListAction get() {
    return newInstance(expenseServiceProvider.get(), appExecutorsProvider.get(), applicationProvider.get());
  }

  public static ExpenseListAction_Factory create(Provider<ExpenseService> expenseServiceProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<Application> applicationProvider) {
    return new ExpenseListAction_Factory(expenseServiceProvider, appExecutorsProvider, applicationProvider);
  }

  public static ExpenseListAction newInstance(ExpenseService expenseService,
      AppExecutors appExecutors, Application application) {
    return new ExpenseListAction(expenseService, appExecutors, application);
  }
}
