package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class OutletBalanceReportVM_AssistedFactory implements ViewModelAssistedFactory<OutletBalanceReportVM> {
  private final Provider<Application> application;

  private final Provider<GetOutletBalanceReportUseCase> getOutletBalanceReportUseCase;

  @Inject
  OutletBalanceReportVM_AssistedFactory(Provider<Application> application,
      Provider<GetOutletBalanceReportUseCase> getOutletBalanceReportUseCase) {
    this.application = application;
    this.getOutletBalanceReportUseCase = getOutletBalanceReportUseCase;
  }

  @Override
  @NonNull
  public OutletBalanceReportVM create(@NonNull SavedStateHandle arg0) {
    return new OutletBalanceReportVM(application.get(), getOutletBalanceReportUseCase.get());
  }
}
