package id.co.danwinciptaniaga.npmdsandroid.droppingday.filter;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyFilterVM_AssistedFactory_Factory implements Factory<DroppingDailyFilterVM_AssistedFactory> {
  private final Provider<Application> appProvider;

  private final Provider<GetDroppingDailyFilterUC> ucProvider;

  public DroppingDailyFilterVM_AssistedFactory_Factory(Provider<Application> appProvider,
      Provider<GetDroppingDailyFilterUC> ucProvider) {
    this.appProvider = appProvider;
    this.ucProvider = ucProvider;
  }

  @Override
  public DroppingDailyFilterVM_AssistedFactory get() {
    return newInstance(appProvider, ucProvider);
  }

  public static DroppingDailyFilterVM_AssistedFactory_Factory create(
      Provider<Application> appProvider, Provider<GetDroppingDailyFilterUC> ucProvider) {
    return new DroppingDailyFilterVM_AssistedFactory_Factory(appProvider, ucProvider);
  }

  public static DroppingDailyFilterVM_AssistedFactory newInstance(Provider<Application> app,
      Provider<GetDroppingDailyFilterUC> uc) {
    return new DroppingDailyFilterVM_AssistedFactory(app, uc);
  }
}
