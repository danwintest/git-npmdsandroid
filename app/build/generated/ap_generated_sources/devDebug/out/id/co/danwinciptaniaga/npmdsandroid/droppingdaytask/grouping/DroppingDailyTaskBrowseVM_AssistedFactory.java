package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.GetDroppingDailyTaskFilterUC;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingDailyTaskBrowseVM_AssistedFactory implements ViewModelAssistedFactory<DroppingDailyTaskBrowseVM> {
  private final Provider<Application> app;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<GetDroppingDailyTaskBrowseHeaderUC> ucHeader;

  private final Provider<GetDroppingDailyTaskFilterUC> ucFilter;

  @Inject
  DroppingDailyTaskBrowseVM_AssistedFactory(Provider<Application> app,
      Provider<AppExecutors> appExecutors, Provider<GetDroppingDailyTaskBrowseHeaderUC> ucHeader,
      Provider<GetDroppingDailyTaskFilterUC> ucFilter) {
    this.app = app;
    this.appExecutors = appExecutors;
    this.ucHeader = ucHeader;
    this.ucFilter = ucFilter;
  }

  @Override
  @NonNull
  public DroppingDailyTaskBrowseVM create(@NonNull SavedStateHandle arg0) {
    return new DroppingDailyTaskBrowseVM(app.get(), appExecutors.get(), ucHeader.get(),
        ucFilter.get());
  }
}
