package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingListActionUC_Factory implements Factory<DroppingListActionUC> {
  private final Provider<DroppingAdditionalService> droppingAdditionalServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<Application> applicationProvider;

  public DroppingListActionUC_Factory(
      Provider<DroppingAdditionalService> droppingAdditionalServiceProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<Application> applicationProvider) {
    this.droppingAdditionalServiceProvider = droppingAdditionalServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.applicationProvider = applicationProvider;
  }

  @Override
  public DroppingListActionUC get() {
    return newInstance(droppingAdditionalServiceProvider.get(), appExecutorsProvider.get(), applicationProvider.get());
  }

  public static DroppingListActionUC_Factory create(
      Provider<DroppingAdditionalService> droppingAdditionalServiceProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<Application> applicationProvider) {
    return new DroppingListActionUC_Factory(droppingAdditionalServiceProvider, appExecutorsProvider, applicationProvider);
  }

  public static DroppingListActionUC newInstance(
      DroppingAdditionalService droppingAdditionalService, AppExecutors appExecutors,
      Application application) {
    return new DroppingListActionUC(droppingAdditionalService, appExecutors, application);
  }
}
