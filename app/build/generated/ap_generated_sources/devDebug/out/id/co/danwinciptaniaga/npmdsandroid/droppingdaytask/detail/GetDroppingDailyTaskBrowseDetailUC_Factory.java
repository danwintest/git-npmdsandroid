package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import dagger.internal.Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.DroppingDailyTaskService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetDroppingDailyTaskBrowseDetailUC_Factory implements Factory<GetDroppingDailyTaskBrowseDetailUC> {
  private final Provider<DroppingDailyTaskService> serviceProvider;

  public GetDroppingDailyTaskBrowseDetailUC_Factory(
      Provider<DroppingDailyTaskService> serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

  @Override
  public GetDroppingDailyTaskBrowseDetailUC get() {
    return newInstance(serviceProvider.get());
  }

  public static GetDroppingDailyTaskBrowseDetailUC_Factory create(
      Provider<DroppingDailyTaskService> serviceProvider) {
    return new GetDroppingDailyTaskBrowseDetailUC_Factory(serviceProvider);
  }

  public static GetDroppingDailyTaskBrowseDetailUC newInstance(DroppingDailyTaskService service) {
    return new GetDroppingDailyTaskBrowseDetailUC(service);
  }
}
