package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.GetTugasTransferFilterUC;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class TugasTransferHeaderBrowseVM_AssistedFactory implements ViewModelAssistedFactory<TugasTransferHeaderBrowseVM> {
  private final Provider<Application> app;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<GetTugasTransferBrowseUC> ucHeader;

  private final Provider<GetTugasTransferFilterUC> ucFilter;

  @Inject
  TugasTransferHeaderBrowseVM_AssistedFactory(Provider<Application> app,
      Provider<AppExecutors> appExecutors, Provider<GetTugasTransferBrowseUC> ucHeader,
      Provider<GetTugasTransferFilterUC> ucFilter) {
    this.app = app;
    this.appExecutors = appExecutors;
    this.ucHeader = ucHeader;
    this.ucFilter = ucFilter;
  }

  @Override
  @NonNull
  public TugasTransferHeaderBrowseVM create(@NonNull SavedStateHandle arg0) {
    return new TugasTransferHeaderBrowseVM(app.get(), appExecutors.get(), ucHeader.get(),
        ucFilter.get());
  }
}
