package id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = DroppingAdditionalFilterFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface DroppingAdditionalFilterFragment_GeneratedInjector {
  void injectDroppingAdditionalFilterFragment(
      DroppingAdditionalFilterFragment droppingAdditionalFilterFragment);
}
