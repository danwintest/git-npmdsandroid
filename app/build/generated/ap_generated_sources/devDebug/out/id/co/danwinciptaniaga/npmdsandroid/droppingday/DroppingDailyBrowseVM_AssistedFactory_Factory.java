package id.co.danwinciptaniaga.npmdsandroid.droppingday;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingListActionUC;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyBrowseVM_AssistedFactory_Factory implements Factory<DroppingDailyBrowseVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<GetDroppingDailyListUC> getDroppingDailyListUCProvider;

  private final Provider<DroppingListActionUC> droppingListActionUCProvider;

  public DroppingDailyBrowseVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<GetDroppingDailyListUC> getDroppingDailyListUCProvider,
      Provider<DroppingListActionUC> droppingListActionUCProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.getDroppingDailyListUCProvider = getDroppingDailyListUCProvider;
    this.droppingListActionUCProvider = droppingListActionUCProvider;
  }

  @Override
  public DroppingDailyBrowseVM_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider, getDroppingDailyListUCProvider, droppingListActionUCProvider);
  }

  public static DroppingDailyBrowseVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<GetDroppingDailyListUC> getDroppingDailyListUCProvider,
      Provider<DroppingListActionUC> droppingListActionUCProvider) {
    return new DroppingDailyBrowseVM_AssistedFactory_Factory(applicationProvider, appExecutorsProvider, getDroppingDailyListUCProvider, droppingListActionUCProvider);
  }

  public static DroppingDailyBrowseVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<GetDroppingDailyListUC> getDroppingDailyListUC,
      Provider<DroppingListActionUC> droppingListActionUC) {
    return new DroppingDailyBrowseVM_AssistedFactory(application, appExecutors, getDroppingDailyListUC, droppingListActionUC);
  }
}
