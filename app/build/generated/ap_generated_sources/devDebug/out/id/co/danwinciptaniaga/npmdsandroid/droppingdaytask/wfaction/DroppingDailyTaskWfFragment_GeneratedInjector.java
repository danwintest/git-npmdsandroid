package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = DroppingDailyTaskWfFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface DroppingDailyTaskWfFragment_GeneratedInjector {
  void injectDroppingDailyTaskWfFragment(DroppingDailyTaskWfFragment droppingDailyTaskWfFragment);
}
