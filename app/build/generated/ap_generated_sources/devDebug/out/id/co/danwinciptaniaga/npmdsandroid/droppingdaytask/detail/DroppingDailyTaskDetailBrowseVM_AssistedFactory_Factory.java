package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskDetailBrowseVM_AssistedFactory_Factory implements Factory<DroppingDailyTaskDetailBrowseVM_AssistedFactory> {
  private final Provider<Application> appProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<GetDroppingDailyTaskBrowseDetailUC> ucDetailProvider;

  public DroppingDailyTaskDetailBrowseVM_AssistedFactory_Factory(Provider<Application> appProvider,
      Provider<AppExecutors> appExecutorsProvider,
      Provider<GetDroppingDailyTaskBrowseDetailUC> ucDetailProvider) {
    this.appProvider = appProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.ucDetailProvider = ucDetailProvider;
  }

  @Override
  public DroppingDailyTaskDetailBrowseVM_AssistedFactory get() {
    return newInstance(appProvider, appExecutorsProvider, ucDetailProvider);
  }

  public static DroppingDailyTaskDetailBrowseVM_AssistedFactory_Factory create(
      Provider<Application> appProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<GetDroppingDailyTaskBrowseDetailUC> ucDetailProvider) {
    return new DroppingDailyTaskDetailBrowseVM_AssistedFactory_Factory(appProvider, appExecutorsProvider, ucDetailProvider);
  }

  public static DroppingDailyTaskDetailBrowseVM_AssistedFactory newInstance(
      Provider<Application> app, Provider<AppExecutors> appExecutors,
      Provider<GetDroppingDailyTaskBrowseDetailUC> ucDetail) {
    return new DroppingDailyTaskDetailBrowseVM_AssistedFactory(app, appExecutors, ucDetail);
  }
}
