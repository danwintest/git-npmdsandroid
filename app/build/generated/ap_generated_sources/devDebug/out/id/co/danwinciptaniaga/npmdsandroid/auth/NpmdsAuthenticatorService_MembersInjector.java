package id.co.danwinciptaniaga.npmdsandroid.auth;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCaseSync;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NpmdsAuthenticatorService_MembersInjector implements MembersInjector<NpmdsAuthenticatorService> {
  private final Provider<GetTokenUseCaseSync> ucGetTokenSyncProvider;

  private final Provider<LoginUtil> loginUtilProvider;

  public NpmdsAuthenticatorService_MembersInjector(
      Provider<GetTokenUseCaseSync> ucGetTokenSyncProvider, Provider<LoginUtil> loginUtilProvider) {
    this.ucGetTokenSyncProvider = ucGetTokenSyncProvider;
    this.loginUtilProvider = loginUtilProvider;
  }

  public static MembersInjector<NpmdsAuthenticatorService> create(
      Provider<GetTokenUseCaseSync> ucGetTokenSyncProvider, Provider<LoginUtil> loginUtilProvider) {
    return new NpmdsAuthenticatorService_MembersInjector(ucGetTokenSyncProvider, loginUtilProvider);
  }

  @Override
  public void injectMembers(NpmdsAuthenticatorService instance) {
    injectUcGetTokenSync(instance, ucGetTokenSyncProvider.get());
    injectLoginUtil(instance, loginUtilProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.auth.NpmdsAuthenticatorService.ucGetTokenSync")
  public static void injectUcGetTokenSync(NpmdsAuthenticatorService instance,
      GetTokenUseCaseSync ucGetTokenSync) {
    instance.ucGetTokenSync = ucGetTokenSync;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.auth.NpmdsAuthenticatorService.loginUtil")
  public static void injectLoginUtil(NpmdsAuthenticatorService instance, LoginUtil loginUtil) {
    instance.loginUtil = loginUtil;
  }
}
