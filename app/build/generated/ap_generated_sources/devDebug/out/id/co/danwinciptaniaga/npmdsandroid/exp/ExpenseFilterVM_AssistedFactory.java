package id.co.danwinciptaniaga.npmdsandroid.exp;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class ExpenseFilterVM_AssistedFactory implements ViewModelAssistedFactory<ExpenseFilterVM> {
  private final Provider<Application> app;

  private final Provider<ExpenseFilterUC> uc;

  @Inject
  ExpenseFilterVM_AssistedFactory(Provider<Application> app, Provider<ExpenseFilterUC> uc) {
    this.app = app;
    this.uc = uc;
  }

  @Override
  @NonNull
  public ExpenseFilterVM create(@NonNull SavedStateHandle arg0) {
    return new ExpenseFilterVM(app.get(), uc.get());
  }
}
