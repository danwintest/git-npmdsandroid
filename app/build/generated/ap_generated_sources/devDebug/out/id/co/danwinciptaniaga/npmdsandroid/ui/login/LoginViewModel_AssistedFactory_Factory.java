package id.co.danwinciptaniaga.npmdsandroid.ui.login;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCase;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceUseCaseSync;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LoginViewModel_AssistedFactory_Factory implements Factory<LoginViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<GetTokenUseCase> getTokenUseCaseProvider;

  private final Provider<RegisterDeviceUseCaseSync> registerDeviceUseCaseSyncProvider;

  public LoginViewModel_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<GetTokenUseCase> getTokenUseCaseProvider,
      Provider<RegisterDeviceUseCaseSync> registerDeviceUseCaseSyncProvider) {
    this.applicationProvider = applicationProvider;
    this.getTokenUseCaseProvider = getTokenUseCaseProvider;
    this.registerDeviceUseCaseSyncProvider = registerDeviceUseCaseSyncProvider;
  }

  @Override
  public LoginViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, getTokenUseCaseProvider, registerDeviceUseCaseSyncProvider);
  }

  public static LoginViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<GetTokenUseCase> getTokenUseCaseProvider,
      Provider<RegisterDeviceUseCaseSync> registerDeviceUseCaseSyncProvider) {
    return new LoginViewModel_AssistedFactory_Factory(applicationProvider, getTokenUseCaseProvider, registerDeviceUseCaseSyncProvider);
  }

  public static LoginViewModel_AssistedFactory newInstance(Provider<Application> application,
      Provider<GetTokenUseCase> getTokenUseCase,
      Provider<RegisterDeviceUseCaseSync> registerDeviceUseCaseSync) {
    return new LoginViewModel_AssistedFactory(application, getTokenUseCase, registerDeviceUseCaseSync);
  }
}
