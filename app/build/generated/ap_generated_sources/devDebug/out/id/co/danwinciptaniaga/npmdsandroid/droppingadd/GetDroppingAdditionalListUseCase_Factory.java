package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetDroppingAdditionalListUseCase_Factory implements Factory<GetDroppingAdditionalListUseCase> {
  private final Provider<DroppingAdditionalService> droppingServiceProvider;

  public GetDroppingAdditionalListUseCase_Factory(
      Provider<DroppingAdditionalService> droppingServiceProvider) {
    this.droppingServiceProvider = droppingServiceProvider;
  }

  @Override
  public GetDroppingAdditionalListUseCase get() {
    return newInstance(droppingServiceProvider.get());
  }

  public static GetDroppingAdditionalListUseCase_Factory create(
      Provider<DroppingAdditionalService> droppingServiceProvider) {
    return new GetDroppingAdditionalListUseCase_Factory(droppingServiceProvider);
  }

  public static GetDroppingAdditionalListUseCase newInstance(
      DroppingAdditionalService droppingService) {
    return new GetDroppingAdditionalListUseCase(droppingService);
  }
}
