package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetOutletStockByDayReportUC_Factory implements Factory<GetOutletStockByDayReportUC> {
  private final Provider<Application> applicationProvider;

  private final Provider<OutletReportService> outletReportServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetOutletStockByDayReportUC_Factory(Provider<Application> applicationProvider,
      Provider<OutletReportService> outletReportServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.outletReportServiceProvider = outletReportServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetOutletStockByDayReportUC get() {
    return newInstance(applicationProvider.get(), outletReportServiceProvider.get(), appExecutorsProvider.get());
  }

  public static GetOutletStockByDayReportUC_Factory create(
      Provider<Application> applicationProvider,
      Provider<OutletReportService> outletReportServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new GetOutletStockByDayReportUC_Factory(applicationProvider, outletReportServiceProvider, appExecutorsProvider);
  }

  public static GetOutletStockByDayReportUC newInstance(Application application,
      OutletReportService outletReportService, AppExecutors appExecutors) {
    return new GetOutletStockByDayReportUC(application, outletReportService, appExecutors);
  }
}
