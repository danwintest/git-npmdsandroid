package id.co.danwinciptaniaga.npmdsandroid.droppingday;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = DroppingDailyBrowseFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface DroppingDailyBrowseFragment_GeneratedInjector {
  void injectDroppingDailyBrowseFragment(DroppingDailyBrowseFragment droppingDailyBrowseFragment);
}
