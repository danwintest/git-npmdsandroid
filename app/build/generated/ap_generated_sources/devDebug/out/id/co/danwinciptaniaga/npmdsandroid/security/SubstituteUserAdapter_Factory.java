package id.co.danwinciptaniaga.npmdsandroid.security;

import android.content.Context;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class SubstituteUserAdapter_Factory implements Factory<SubstituteUserAdapter> {
  private final Provider<Context> contextProvider;

  private final Provider<SubstituteUserAdapter.Listener> listenerProvider;

  public SubstituteUserAdapter_Factory(Provider<Context> contextProvider,
      Provider<SubstituteUserAdapter.Listener> listenerProvider) {
    this.contextProvider = contextProvider;
    this.listenerProvider = listenerProvider;
  }

  @Override
  public SubstituteUserAdapter get() {
    return newInstance(contextProvider.get(), listenerProvider.get());
  }

  public static SubstituteUserAdapter_Factory create(Provider<Context> contextProvider,
      Provider<SubstituteUserAdapter.Listener> listenerProvider) {
    return new SubstituteUserAdapter_Factory(contextProvider, listenerProvider);
  }

  public static SubstituteUserAdapter newInstance(Context context, Object listener) {
    return new SubstituteUserAdapter(context, (SubstituteUserAdapter.Listener) listener);
  }
}
