package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;
import javax.annotation.Generated;

@Generated("androidx.hilt.AndroidXHiltProcessor")
@Module
@InstallIn(ActivityRetainedComponent.class)
@OriginatingElement(
    topLevelClass = DroppingAdditionalBrowseViewModel.class
)
public interface DroppingAdditionalBrowseViewModel_HiltModule {
  @Binds
  @IntoMap
  @StringKey("id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseViewModel")
  ViewModelAssistedFactory<? extends ViewModel> bind(
      DroppingAdditionalBrowseViewModel_AssistedFactory factory);
}
