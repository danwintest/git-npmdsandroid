package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = TugasTransferBrowseHeaderFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface TugasTransferBrowseHeaderFragment_GeneratedInjector {
  void injectTugasTransferBrowseHeaderFragment(
      TugasTransferBrowseHeaderFragment tugasTransferBrowseHeaderFragment);
}
