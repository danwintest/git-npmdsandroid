package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class AppBookingTransferRekeningFormVM_AssistedFactory implements ViewModelAssistedFactory<AppBookingTransferRekeningFormVM> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<AppBookingService> appBookingService;

  @Inject
  AppBookingTransferRekeningFormVM_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors, Provider<AppBookingService> appBookingService) {
    this.application = application;
    this.appExecutors = appExecutors;
    this.appBookingService = appBookingService;
  }

  @Override
  @NonNull
  public AppBookingTransferRekeningFormVM create(@NonNull SavedStateHandle arg0) {
    return new AppBookingTransferRekeningFormVM(application.get(), appExecutors.get(),
        appBookingService.get());
  }
}
