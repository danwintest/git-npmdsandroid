package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingTransferRekeningListFragment_MembersInjector implements MembersInjector<AppBookingTransferRekeningListFragment> {
  private final Provider<AppExecutors> appExecutorsProvider;

  public AppBookingTransferRekeningListFragment_MembersInjector(
      Provider<AppExecutors> appExecutorsProvider) {
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<AppBookingTransferRekeningListFragment> create(
      Provider<AppExecutors> appExecutorsProvider) {
    return new AppBookingTransferRekeningListFragment_MembersInjector(appExecutorsProvider);
  }

  @Override
  public void injectMembers(AppBookingTransferRekeningListFragment instance) {
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningListFragment.appExecutors")
  public static void injectAppExecutors(AppBookingTransferRekeningListFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
