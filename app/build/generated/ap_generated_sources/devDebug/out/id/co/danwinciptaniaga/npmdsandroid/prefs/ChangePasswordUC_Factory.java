package id.co.danwinciptaniaga.npmdsandroid.prefs;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ChangePasswordUC_Factory implements Factory<ChangePasswordUC> {
  private final Provider<Application> applicationProvider;

  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public ChangePasswordUC_Factory(Provider<Application> applicationProvider,
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public ChangePasswordUC get() {
    return newInstance(applicationProvider.get(), commonServiceProvider.get(), appExecutorsProvider.get());
  }

  public static ChangePasswordUC_Factory create(Provider<Application> applicationProvider,
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new ChangePasswordUC_Factory(applicationProvider, commonServiceProvider, appExecutorsProvider);
  }

  public static ChangePasswordUC newInstance(Application application, CommonService commonService,
      AppExecutors appExecutors) {
    return new ChangePasswordUC(application, commonService, appExecutors);
  }
}
