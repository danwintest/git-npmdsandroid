package id.co.danwinciptaniaga.npmdsandroid;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.hilt.lifecycle.ViewModelFactoryModules_ActivityModule_ProvideFactoryFactory;
import androidx.hilt.lifecycle.ViewModelFactoryModules_FragmentModule_ProvideFactoryFactory;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import dagger.hilt.android.internal.builders.ActivityComponentBuilder;
import dagger.hilt.android.internal.builders.ActivityRetainedComponentBuilder;
import dagger.hilt.android.internal.builders.FragmentComponentBuilder;
import dagger.hilt.android.internal.builders.ServiceComponentBuilder;
import dagger.hilt.android.internal.builders.ViewComponentBuilder;
import dagger.hilt.android.internal.builders.ViewWithFragmentComponentBuilder;
import dagger.hilt.android.internal.modules.ApplicationContextModule;
import dagger.hilt.android.internal.modules.ApplicationContextModule_ProvideApplicationFactory;
import dagger.hilt.android.internal.modules.ApplicationContextModule_ProvideContextFactory;
import dagger.internal.DoubleCheck;
import dagger.internal.MemoizedSentinel;
import dagger.internal.Preconditions;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.DownloadReceiver;
import id.co.danwinciptaniaga.androcon.DownloadReceiver_MembersInjector;
import id.co.danwinciptaniaga.androcon.auth.AuthService;
import id.co.danwinciptaniaga.androcon.auth.AuthorizationHeaderProvider;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCase;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCaseSync;
import id.co.danwinciptaniaga.androcon.auth.GetUserPermissionUseCase;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.auth.UserService;
import id.co.danwinciptaniaga.androcon.di.AndroconModule;
import id.co.danwinciptaniaga.androcon.di.AndroconModule_ProvideAuthServiceFactory;
import id.co.danwinciptaniaga.androcon.di.AndroconModule_ProvideDeviceRegistrationServiceFactory;
import id.co.danwinciptaniaga.androcon.di.AndroconModule_ProvideUserServiceFactory;
import id.co.danwinciptaniaga.androcon.di.AndroconModule_ProviderOkHttpClientBuilderFactory;
import id.co.danwinciptaniaga.androcon.di.AndroconModule_ProviderUtilityServiceFactory;
import id.co.danwinciptaniaga.androcon.registration.DeviceRegistrationService;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceUseCase;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceUseCaseSync;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceViewModel_AssistedFactory;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.androcon.registration.RegistrationUtil;
import id.co.danwinciptaniaga.androcon.retrofit.WebServiceGenerator;
import id.co.danwinciptaniaga.androcon.security.ProtectedActivityViewModel_AssistedFactory;
import id.co.danwinciptaniaga.androcon.security.ProtectedActivityViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.androcon.security.ProtectedActivity_MembersInjector;
import id.co.danwinciptaniaga.androcon.update.DownloadUpdateUseCase;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.GetLatestVersionUseCase;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmdsandroid.auth.NpmdsAuthenticatorService;
import id.co.danwinciptaniaga.npmdsandroid.auth.NpmdsAuthenticatorService_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingFilterVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingFilterVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingService;
import id.co.danwinciptaniaga.npmdsandroid.booking.GetBookingFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.GetBookingListUseCase;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingListAction;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingLoadUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingPrepareUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingProcessUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash.AppBookingCashVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash.AppBookingCashVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningForm;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningFormVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningFormVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningListFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningListFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.BankAccountBrowseUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.BankAccountBrowseVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.BankAccountBrowseVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailTransferVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailTransferVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingLoadUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingProcessUC;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import id.co.danwinciptaniaga.npmdsandroid.common.GetExtUserInfoUseCase;
import id.co.danwinciptaniaga.npmdsandroid.common.GetSubstituteUsersUC;
import id.co.danwinciptaniaga.npmdsandroid.common.SubstituteUserUC;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule_ProvideAndroconConfigFactory;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule_ProvideAppBookingServiceFactory;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule_ProvideBookingServiceFactory;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule_ProvideCommonServiceFactory;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule_ProvideDroppingAdditionalServiceFactory;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule_ProvideDroppingDailyServiceFactory;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule_ProvideDroppingDailyTaskServiceFactory;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule_ProvideExpenseServiceFactory;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule_ProvideOutletReportServiceFactory;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule_ProvideTugasTransferServiceFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingListActionUC;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.GetDroppingAdditionalListUseCase;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalSaveUseCase;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalSupervisorDecideUseCase;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.LoadDroppingAdditionalUseCase;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.PrepareDroppingAdditionalUseCase;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.DroppingAdditionalFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.DroppingAdditionalFilterVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.DroppingAdditionalFilterVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.GetDroppingAdditionalFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyService;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.GetDroppingDailyListUC;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailySupervisorDecideUseCase;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.LoadDroppingDailyUseCase;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.DroppingDailyFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.DroppingDailyFilterVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.DroppingDailyFilterVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.GetDroppingDailyFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.DroppingDailyTaskService;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail.DroppingDailyTaskDetailBrowseVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail.DroppingDailyTaskDetailBrowseVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail.GetDroppingDailyTaskBrowseDetailUC;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.GetDroppingDailyTaskFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.GetDroppingDailyTaskBrowseHeaderUC;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskProcessUC;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListAction;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseSortFragment;
import id.co.danwinciptaniaga.npmdsandroid.exp.GetExpenseListUseCase;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseAocDecideUseCase;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseEditFragment;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseEditViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseEditViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseSaveDraftUseCase;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.LoadExpenseUseCase;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.PrepareNewExpenseUseCase;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.GetBookingMediatorListUseCase;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.SelectBookingFragment;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.SelectBookingViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.SelectBookingViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoAccountListUC;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoBankListUC;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoFragment;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoUC;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.GetTugasTransferDetailBrowseUC;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailBrowseVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailBrowseVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailFragment;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.GetTugasTransferFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.GetTugasTransferBrowseUC;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferBrowseHeaderFragment;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferBrowseHeaderFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferHeaderBrowseVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferHeaderBrowseVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWFGenerateOtpUC;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfFormUC;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfFragment;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfProcessUC;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.notif.NpmdsNotificationService;
import id.co.danwinciptaniaga.npmdsandroid.notif.NpmdsNotificationService_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.GetOutletBalanceListUseCase;
import id.co.danwinciptaniaga.npmdsandroid.outlet.GetOutletBalanceReportUseCase;
import id.co.danwinciptaniaga.npmdsandroid.outlet.GetOutletStockByDayReportUC;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceBrowseVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceBrowseVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportFragment;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletReportService;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockByDayReportFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockByDayReportFragment;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockReportByDayVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockReportByDayVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordFragment;
import id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordUC;
import id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceFragment;
import id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceFragment_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.prefs.TestPushNotificationUseCase;
import id.co.danwinciptaniaga.npmdsandroid.prefs.UploadLogFileUseCase;
import id.co.danwinciptaniaga.npmdsandroid.security.SubstituteUserFragment;
import id.co.danwinciptaniaga.npmdsandroid.security.SubstituteUserVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.security.SubstituteUserVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.BankAcctFormFragment;
import id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.BankAcctFormVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.BankAcctFormVM_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.GetBanksUseCase;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.DrawerViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.DrawerViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity_MembersInjector;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.LoginViewModel_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.LoginViewModel_AssistedFactory_Factory;
import id.co.danwinciptaniaga.npmdsandroid.wf.GetWfHistoryUseCase;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfHistoryFragment;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfHistoryVM_AssistedFactory;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfHistoryVM_AssistedFactory_Factory;
import java.util.Map;
import java.util.Set;
import javax.annotation.Generated;
import javax.inject.Provider;
import okhttp3.OkHttpClient;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DaggerApp_HiltComponents_ApplicationC extends App_HiltComponents.ApplicationC {
  private final ApplicationContextModule applicationContextModule;

  private final NpmdsModule npmdsModule;

  private final AndroconModule androconModule;

  private volatile Object androconConfig = new MemoizedSentinel();

  private volatile Object loginUtil = new MemoizedSentinel();

  private volatile Object okHttpClientBuilder = new MemoizedSentinel();

  private volatile Object webServiceGenerator = new MemoizedSentinel();

  private volatile Object appExecutors = new MemoizedSentinel();

  private volatile Provider<Application> provideApplicationProvider;

  private volatile Provider<AppExecutors> appExecutorsProvider;

  private volatile Provider<UtilityService> providerUtilityServiceProvider;

  private volatile Provider<AppBookingService> provideAppBookingServiceProvider;

  private volatile Provider<ExpenseService> provideExpenseServiceProvider;

  private volatile Provider<LoginUtil> loginUtilProvider;

  private volatile Object registrationUtil = new MemoizedSentinel();

  private volatile Provider<AndroconConfig> provideAndroconConfigProvider;

  private volatile Provider<AuthService> provideAuthServiceProvider;

  private DaggerApp_HiltComponents_ApplicationC(AndroconModule androconModuleParam,
      ApplicationContextModule applicationContextModuleParam, NpmdsModule npmdsModuleParam) {
    this.applicationContextModule = applicationContextModuleParam;
    this.npmdsModule = npmdsModuleParam;
    this.androconModule = androconModuleParam;
  }

  public static Builder builder() {
    return new Builder();
  }

  private AndroconConfig getAndroconConfig() {
    Object local = androconConfig;
    if (local instanceof MemoizedSentinel) {
      synchronized (local) {
        local = androconConfig;
        if (local instanceof MemoizedSentinel) {
          local = NpmdsModule_ProvideAndroconConfigFactory.provideAndroconConfig(npmdsModule, ApplicationContextModule_ProvideContextFactory.provideContext(applicationContextModule));
          androconConfig = DoubleCheck.reentrantCheck(androconConfig, local);
        }
      }
    }
    return (AndroconConfig) local;
  }

  private LoginUtil getLoginUtil() {
    Object local = loginUtil;
    if (local instanceof MemoizedSentinel) {
      synchronized (local) {
        local = loginUtil;
        if (local instanceof MemoizedSentinel) {
          local = new LoginUtil(ApplicationContextModule_ProvideContextFactory.provideContext(applicationContextModule));
          loginUtil = DoubleCheck.reentrantCheck(loginUtil, local);
        }
      }
    }
    return (LoginUtil) local;
  }

  private OkHttpClient.Builder getOkHttpClientBuilder() {
    Object local = okHttpClientBuilder;
    if (local instanceof MemoizedSentinel) {
      synchronized (local) {
        local = okHttpClientBuilder;
        if (local instanceof MemoizedSentinel) {
          local = AndroconModule_ProviderOkHttpClientBuilderFactory.providerOkHttpClientBuilder(androconModule, getAndroconConfig());
          okHttpClientBuilder = DoubleCheck.reentrantCheck(okHttpClientBuilder, local);
        }
      }
    }
    return (OkHttpClient.Builder) local;
  }

  private WebServiceGenerator getWebServiceGenerator() {
    Object local = webServiceGenerator;
    if (local instanceof MemoizedSentinel) {
      synchronized (local) {
        local = webServiceGenerator;
        if (local instanceof MemoizedSentinel) {
          local = new WebServiceGenerator(ApplicationContextModule_ProvideApplicationFactory.provideApplication(applicationContextModule), getAndroconConfig(), getLoginUtil(), getOkHttpClientBuilder());
          webServiceGenerator = DoubleCheck.reentrantCheck(webServiceGenerator, local);
        }
      }
    }
    return (WebServiceGenerator) local;
  }

  private AppExecutors getAppExecutors() {
    Object local = appExecutors;
    if (local instanceof MemoizedSentinel) {
      synchronized (local) {
        local = appExecutors;
        if (local instanceof MemoizedSentinel) {
          local = new AppExecutors();
          appExecutors = DoubleCheck.reentrantCheck(appExecutors, local);
        }
      }
    }
    return (AppExecutors) local;
  }

  private Provider<Application> getApplicationProvider() {
    Object local = provideApplicationProvider;
    if (local == null) {
      local = new SwitchingProvider<>(0);
      provideApplicationProvider = (Provider<Application>) local;
    }
    return (Provider<Application>) local;
  }

  private Provider<AppExecutors> getAppExecutorsProvider() {
    Object local = appExecutorsProvider;
    if (local == null) {
      local = new SwitchingProvider<>(1);
      appExecutorsProvider = (Provider<AppExecutors>) local;
    }
    return (Provider<AppExecutors>) local;
  }

  private AppBookingService getAppBookingService() {
    return NpmdsModule_ProvideAppBookingServiceFactory.provideAppBookingService(npmdsModule, getWebServiceGenerator());
  }

  private Provider<UtilityService> getUtilityServiceProvider() {
    Object local = providerUtilityServiceProvider;
    if (local == null) {
      local = new SwitchingProvider<>(2);
      providerUtilityServiceProvider = (Provider<UtilityService>) local;
    }
    return (Provider<UtilityService>) local;
  }

  private Provider<AppBookingService> getAppBookingServiceProvider() {
    Object local = provideAppBookingServiceProvider;
    if (local == null) {
      local = new SwitchingProvider<>(3);
      provideAppBookingServiceProvider = (Provider<AppBookingService>) local;
    }
    return (Provider<AppBookingService>) local;
  }

  private BookingService getBookingService() {
    return NpmdsModule_ProvideBookingServiceFactory.provideBookingService(npmdsModule, getWebServiceGenerator());
  }

  private CommonService getCommonService() {
    return NpmdsModule_ProvideCommonServiceFactory.provideCommonService(npmdsModule, getWebServiceGenerator());
  }

  private DroppingAdditionalService getDroppingAdditionalService() {
    return NpmdsModule_ProvideDroppingAdditionalServiceFactory.provideDroppingAdditionalService(npmdsModule, getWebServiceGenerator());
  }

  private DroppingDailyService getDroppingDailyService() {
    return NpmdsModule_ProvideDroppingDailyServiceFactory.provideDroppingDailyService(npmdsModule, getWebServiceGenerator());
  }

  private DroppingDailyTaskService getDroppingDailyTaskService() {
    return NpmdsModule_ProvideDroppingDailyTaskServiceFactory.provideDroppingDailyTaskService(npmdsModule, getWebServiceGenerator());
  }

  private ExpenseService getExpenseService() {
    return NpmdsModule_ProvideExpenseServiceFactory.provideExpenseService(npmdsModule, getWebServiceGenerator());
  }

  private Provider<ExpenseService> getExpenseServiceProvider() {
    Object local = provideExpenseServiceProvider;
    if (local == null) {
      local = new SwitchingProvider<>(4);
      provideExpenseServiceProvider = (Provider<ExpenseService>) local;
    }
    return (Provider<ExpenseService>) local;
  }

  private Provider<LoginUtil> getLoginUtilProvider() {
    Object local = loginUtilProvider;
    if (local == null) {
      local = new SwitchingProvider<>(5);
      loginUtilProvider = (Provider<LoginUtil>) local;
    }
    return (Provider<LoginUtil>) local;
  }

  private AuthService getAuthService() {
    return AndroconModule_ProvideAuthServiceFactory.provideAuthService(androconModule, getWebServiceGenerator());
  }

  private DeviceRegistrationService getDeviceRegistrationService() {
    return AndroconModule_ProvideDeviceRegistrationServiceFactory.provideDeviceRegistrationService(androconModule, getWebServiceGenerator());
  }

  private RegistrationUtil getRegistrationUtil() {
    Object local = registrationUtil;
    if (local instanceof MemoizedSentinel) {
      synchronized (local) {
        local = registrationUtil;
        if (local instanceof MemoizedSentinel) {
          local = new RegistrationUtil(ApplicationContextModule_ProvideContextFactory.provideContext(applicationContextModule));
          registrationUtil = DoubleCheck.reentrantCheck(registrationUtil, local);
        }
      }
    }
    return (RegistrationUtil) local;
  }

  private OutletReportService getOutletReportService() {
    return NpmdsModule_ProvideOutletReportServiceFactory.provideOutletReportService(npmdsModule, getWebServiceGenerator());
  }

  private Provider<AndroconConfig> getAndroconConfigProvider() {
    Object local = provideAndroconConfigProvider;
    if (local == null) {
      local = new SwitchingProvider<>(6);
      provideAndroconConfigProvider = (Provider<AndroconConfig>) local;
    }
    return (Provider<AndroconConfig>) local;
  }

  private UserService getUserService() {
    return AndroconModule_ProvideUserServiceFactory.provideUserService(androconModule, getWebServiceGenerator());
  }

  private Provider<AuthService> getAuthServiceProvider() {
    Object local = provideAuthServiceProvider;
    if (local == null) {
      local = new SwitchingProvider<>(7);
      provideAuthServiceProvider = (Provider<AuthService>) local;
    }
    return (Provider<AuthService>) local;
  }

  private TugasTransferService getTugasTransferService() {
    return NpmdsModule_ProvideTugasTransferServiceFactory.provideTugasTransferService(npmdsModule, getWebServiceGenerator());
  }

  @Override
  public ActivityRetainedComponentBuilder retainedComponentBuilder() {
    return new ActivityRetainedCBuilder();
  }

  @Override
  public ServiceComponentBuilder serviceComponentBuilder() {
    return new ServiceCBuilder();
  }

  @Override
  public void injectDownloadReceiver(DownloadReceiver arg0) {
    injectDownloadReceiver2(arg0);
  }

  @Override
  public UtilityService getUtilityService() {
    return AndroconModule_ProviderUtilityServiceFactory.providerUtilityService(androconModule, getWebServiceGenerator());
  }

  @Override
  public void injectApp(App app) {
    injectApp2(app);
  }

  @CanIgnoreReturnValue
  private DownloadReceiver injectDownloadReceiver2(DownloadReceiver instance) {
    DownloadReceiver_MembersInjector.injectAndroconConfig(instance, getAndroconConfig());
    return instance;
  }

  @CanIgnoreReturnValue
  private App injectApp2(App instance) {
    App_MembersInjector.injectAndroconConfig(instance, getAndroconConfig());
    App_MembersInjector.injectMAppExecutors(instance, getAppExecutors());
    App_MembersInjector.injectLoginUtil(instance, getLoginUtil());
    return instance;
  }

  public static final class Builder {
    private AndroconModule androconModule;

    private ApplicationContextModule applicationContextModule;

    private NpmdsModule npmdsModule;

    private Builder() {
    }

    public Builder androconModule(AndroconModule androconModule) {
      this.androconModule = Preconditions.checkNotNull(androconModule);
      return this;
    }

    public Builder applicationContextModule(ApplicationContextModule applicationContextModule) {
      this.applicationContextModule = Preconditions.checkNotNull(applicationContextModule);
      return this;
    }

    public Builder npmdsModule(NpmdsModule npmdsModule) {
      this.npmdsModule = Preconditions.checkNotNull(npmdsModule);
      return this;
    }

    public App_HiltComponents.ApplicationC build() {
      if (androconModule == null) {
        this.androconModule = new AndroconModule();
      }
      Preconditions.checkBuilderRequirement(applicationContextModule, ApplicationContextModule.class);
      if (npmdsModule == null) {
        this.npmdsModule = new NpmdsModule();
      }
      return new DaggerApp_HiltComponents_ApplicationC(androconModule, applicationContextModule, npmdsModule);
    }
  }

  private final class ActivityRetainedCBuilder implements App_HiltComponents.ActivityRetainedC.Builder {
    @Override
    public App_HiltComponents.ActivityRetainedC build() {
      return new ActivityRetainedCImpl();
    }
  }

  private final class ActivityRetainedCImpl extends App_HiltComponents.ActivityRetainedC {
    private ActivityRetainedCImpl() {

    }

    @Override
    public ActivityComponentBuilder activityComponentBuilder() {
      return new ActivityCBuilder();
    }

    private final class ActivityCBuilder implements App_HiltComponents.ActivityC.Builder {
      private Activity activity;

      @Override
      public ActivityCBuilder activity(Activity activity) {
        this.activity = Preconditions.checkNotNull(activity);
        return this;
      }

      @Override
      public App_HiltComponents.ActivityC build() {
        Preconditions.checkBuilderRequirement(activity, Activity.class);
        return new ActivityCImpl(activity);
      }
    }

    private final class ActivityCImpl extends App_HiltComponents.ActivityC {
      private final Activity activity;

      private volatile Provider<AppBookingBrowseUC> appBookingBrowseUCProvider;

      private volatile Provider<AppBookingListAction> appBookingListActionProvider;

      private volatile Provider<AppBookingBrowseVM_AssistedFactory> appBookingBrowseVM_AssistedFactoryProvider;

      private volatile Provider<AppBookingPrepareUC> appBookingPrepareUCProvider;

      private volatile Provider<AppBookingLoadUC> appBookingLoadUCProvider;

      private volatile Provider<AppBookingProcessUC> appBookingProcessUCProvider;

      private volatile Provider<AppBookingCashVM_AssistedFactory> appBookingCashVM_AssistedFactoryProvider;

      private volatile Provider<GetBookingFilterUC> getBookingFilterUCProvider;

      private volatile Provider<AppBookingFilterVM_AssistedFactory> appBookingFilterVM_AssistedFactoryProvider;

      private volatile Provider<AppBookingMultiActionUC> appBookingMultiActionUCProvider;

      private volatile Provider<AppBookingMultiActionVM_AssistedFactory> appBookingMultiActionVM_AssistedFactoryProvider;

      private volatile Provider<AppBookingTransferRekeningFormVM_AssistedFactory> appBookingTransferRekeningFormVM_AssistedFactoryProvider;

      private volatile Provider<AppBookingTransferVM_AssistedFactory> appBookingTransferVM_AssistedFactoryProvider;

      private volatile Provider<BankAccountBrowseUC> bankAccountBrowseUCProvider;

      private volatile Provider<BankAccountBrowseVM_AssistedFactory> bankAccountBrowseVM_AssistedFactoryProvider;

      private volatile Provider<GetBanksUseCase> getBanksUseCaseProvider;

      private volatile Provider<BankAcctFormVM_AssistedFactory> bankAcctFormVM_AssistedFactoryProvider;

      private volatile Provider<GetBookingListUseCase> getBookingListUseCaseProvider;

      private volatile Provider<BookingBrowseViewModel_AssistedFactory> bookingBrowseViewModel_AssistedFactoryProvider;

      private volatile Provider<BookingLoadUC> bookingLoadUCProvider;

      private volatile Provider<BookingProcessUC> bookingProcessUCProvider;

      private volatile Provider<BookingDetailTransferVM_AssistedFactory> bookingDetailTransferVM_AssistedFactoryProvider;

      private volatile Provider<BookingDetailVM_AssistedFactory> bookingDetailVM_AssistedFactoryProvider;

      private volatile Provider<BookingFilterVM_AssistedFactory> bookingFilterVM_AssistedFactoryProvider;

      private volatile Provider<ChangePasswordUC> changePasswordUCProvider;

      private volatile Provider<ChangePasswordVM_AssistedFactory> changePasswordVM_AssistedFactoryProvider;

      private volatile Provider<GetExtUserInfoUseCase> getExtUserInfoUseCaseProvider;

      private volatile Provider<DrawerViewModel_AssistedFactory> drawerViewModel_AssistedFactoryProvider;

      private volatile Provider<GetDroppingAdditionalListUseCase> getDroppingAdditionalListUseCaseProvider;

      private volatile Provider<DroppingListActionUC> droppingListActionUCProvider;

      private volatile Provider<DroppingAdditionalBrowseViewModel_AssistedFactory> droppingAdditionalBrowseViewModel_AssistedFactoryProvider;

      private volatile Provider<DroppingAdditionalDetailEditViewModel_AssistedFactory> droppingAdditionalDetailEditViewModel_AssistedFactoryProvider;

      private volatile Provider<PrepareDroppingAdditionalUseCase> prepareDroppingAdditionalUseCaseProvider;

      private volatile Provider<DroppingAdditionalSaveUseCase> droppingAdditionalSaveUseCaseProvider;

      private volatile Provider<LoadDroppingAdditionalUseCase> loadDroppingAdditionalUseCaseProvider;

      private volatile Provider<DroppingAdditionalSupervisorDecideUseCase> droppingAdditionalSupervisorDecideUseCaseProvider;

      private volatile Provider<DroppingAdditionalEditViewModel_AssistedFactory> droppingAdditionalEditViewModel_AssistedFactoryProvider;

      private volatile Provider<GetDroppingAdditionalFilterUC> getDroppingAdditionalFilterUCProvider;

      private volatile Provider<DroppingAdditionalFilterVM_AssistedFactory> droppingAdditionalFilterVM_AssistedFactoryProvider;

      private volatile Provider<GetDroppingDailyListUC> getDroppingDailyListUCProvider;

      private volatile Provider<DroppingDailyBrowseVM_AssistedFactory> droppingDailyBrowseVM_AssistedFactoryProvider;

      private volatile Provider<LoadDroppingDailyUseCase> loadDroppingDailyUseCaseProvider;

      private volatile Provider<DroppingDailySupervisorDecideUseCase> droppingDailySupervisorDecideUseCaseProvider;

      private volatile Provider<DroppingDailyEditViewModel_AssistedFactory> droppingDailyEditViewModel_AssistedFactoryProvider;

      private volatile Provider<GetDroppingDailyFilterUC> getDroppingDailyFilterUCProvider;

      private volatile Provider<DroppingDailyFilterVM_AssistedFactory> droppingDailyFilterVM_AssistedFactoryProvider;

      private volatile Provider<GetDroppingDailyTaskBrowseHeaderUC> getDroppingDailyTaskBrowseHeaderUCProvider;

      private volatile Provider<GetDroppingDailyTaskFilterUC> getDroppingDailyTaskFilterUCProvider;

      private volatile Provider<DroppingDailyTaskBrowseVM_AssistedFactory> droppingDailyTaskBrowseVM_AssistedFactoryProvider;

      private volatile Provider<GetDroppingDailyTaskBrowseDetailUC> getDroppingDailyTaskBrowseDetailUCProvider;

      private volatile Provider<DroppingDailyTaskDetailBrowseVM_AssistedFactory> droppingDailyTaskDetailBrowseVM_AssistedFactoryProvider;

      private volatile Provider<DroppingDailyTaskFilterVM_AssistedFactory> droppingDailyTaskFilterVM_AssistedFactoryProvider;

      private volatile Provider<DroppingDailyTaskProcessUC> droppingDailyTaskProcessUCProvider;

      private volatile Provider<DroppingDailyTaskWfVM_AssistedFactory> droppingDailyTaskWfVM_AssistedFactoryProvider;

      private volatile Provider<GetExpenseListUseCase> getExpenseListUseCaseProvider;

      private volatile Provider<ExpenseListAction> expenseListActionProvider;

      private volatile Provider<ExpenseBrowseViewModel_AssistedFactory> expenseBrowseViewModel_AssistedFactoryProvider;

      private volatile Provider<PrepareNewExpenseUseCase> prepareNewExpenseUseCaseProvider;

      private volatile Provider<ExpenseSaveDraftUseCase> expenseSaveDraftUseCaseProvider;

      private volatile Provider<LoadExpenseUseCase> loadExpenseUseCaseProvider;

      private volatile Provider<ExpenseAocDecideUseCase> expenseAocDecideUseCaseProvider;

      private volatile Provider<ExpenseEditViewModel_AssistedFactory> expenseEditViewModel_AssistedFactoryProvider;

      private volatile Provider<ExpenseFilterUC> expenseFilterUCProvider;

      private volatile Provider<ExpenseFilterVM_AssistedFactory> expenseFilterVM_AssistedFactoryProvider;

      private volatile Provider<GetTokenUseCase> getTokenUseCaseProvider;

      private volatile Provider<RegisterDeviceUseCaseSync> registerDeviceUseCaseSyncProvider;

      private volatile Provider<LoginViewModel_AssistedFactory> loginViewModel_AssistedFactoryProvider;

      private volatile Provider<GetOutletBalanceListUseCase> getOutletBalanceListUseCaseProvider;

      private volatile Provider<OutletBalanceBrowseVM_AssistedFactory> outletBalanceBrowseVM_AssistedFactoryProvider;

      private volatile Provider<GetOutletBalanceReportUseCase> getOutletBalanceReportUseCaseProvider;

      private volatile Provider<OutletBalanceReportVM_AssistedFactory> outletBalanceReportVM_AssistedFactoryProvider;

      private volatile Provider<GetOutletStockByDayReportUC> getOutletStockByDayReportUCProvider;

      private volatile Provider<OutletStockReportByDayVM_AssistedFactory> outletStockReportByDayVM_AssistedFactoryProvider;

      private volatile Provider<GetLatestVersionUseCase> getLatestVersionUseCaseProvider;

      private volatile Provider<DownloadUpdateUseCase> downloadUpdateUseCaseProvider;

      private volatile Provider<PreferenceViewModel_AssistedFactory> preferenceViewModel_AssistedFactoryProvider;

      private volatile Provider<GetUserPermissionUseCase> getUserPermissionUseCaseProvider;

      private volatile Provider<AuthorizationHeaderProvider> authorizationHeaderProvider;

      private volatile Provider<ProtectedActivityViewModel_AssistedFactory> protectedActivityViewModel_AssistedFactoryProvider;

      private volatile Provider<RegisterDeviceUseCase> registerDeviceUseCaseProvider;

      private volatile Provider<RegisterDeviceViewModel_AssistedFactory> registerDeviceViewModel_AssistedFactoryProvider;

      private volatile Provider<GetBookingMediatorListUseCase> getBookingMediatorListUseCaseProvider;

      private volatile Provider<SelectBookingViewModel_AssistedFactory> selectBookingViewModel_AssistedFactoryProvider;

      private volatile Provider<GetSubstituteUsersUC> getSubstituteUsersUCProvider;

      private volatile Provider<SubstituteUserUC> substituteUserUCProvider;

      private volatile Provider<SubstituteUserVM_AssistedFactory> substituteUserVM_AssistedFactoryProvider;

      private volatile Provider<TugasTransferCekSaldoUC> tugasTransferCekSaldoUCProvider;

      private volatile Provider<TugasTransferCekSaldoBankListUC> tugasTransferCekSaldoBankListUCProvider;

      private volatile Provider<TugasTransferCekSaldoAccountListUC> tugasTransferCekSaldoAccountListUCProvider;

      private volatile Provider<TugasTransferCekSaldoVM_AssistedFactory> tugasTransferCekSaldoVM_AssistedFactoryProvider;

      private volatile Provider<GetTugasTransferDetailBrowseUC> getTugasTransferDetailBrowseUCProvider;

      private volatile Provider<TugasTransferDetailBrowseVM_AssistedFactory> tugasTransferDetailBrowseVM_AssistedFactoryProvider;

      private volatile Provider<GetTugasTransferFilterUC> getTugasTransferFilterUCProvider;

      private volatile Provider<TugasTransferFilterVM_AssistedFactory> tugasTransferFilterVM_AssistedFactoryProvider;

      private volatile Provider<GetTugasTransferBrowseUC> getTugasTransferBrowseUCProvider;

      private volatile Provider<TugasTransferHeaderBrowseVM_AssistedFactory> tugasTransferHeaderBrowseVM_AssistedFactoryProvider;

      private volatile Provider<TugasTransferWfFormUC> tugasTransferWfFormUCProvider;

      private volatile Provider<TugasTransferWfProcessUC> tugasTransferWfProcessUCProvider;

      private volatile Provider<TugasTransferWFGenerateOtpUC> tugasTransferWFGenerateOtpUCProvider;

      private volatile Provider<TugasTransferWfVM_AssistedFactory> tugasTransferWfVM_AssistedFactoryProvider;

      private volatile Provider<GetWfHistoryUseCase> getWfHistoryUseCaseProvider;

      private volatile Provider<WfHistoryVM_AssistedFactory> wfHistoryVM_AssistedFactoryProvider;

      private ActivityCImpl(Activity activityParam) {
        this.activity = activityParam;
      }

      private AppBookingBrowseUC getAppBookingBrowseUC() {
        return new AppBookingBrowseUC(DaggerApp_HiltComponents_ApplicationC.this.getAppBookingService());
      }

      private Provider<AppBookingBrowseUC> getAppBookingBrowseUCProvider() {
        Object local = appBookingBrowseUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(1);
          appBookingBrowseUCProvider = (Provider<AppBookingBrowseUC>) local;
        }
        return (Provider<AppBookingBrowseUC>) local;
      }

      private AppBookingListAction getAppBookingListAction() {
        return new AppBookingListAction(DaggerApp_HiltComponents_ApplicationC.this.getAppBookingService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors(), ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule));
      }

      private Provider<AppBookingListAction> getAppBookingListActionProvider() {
        Object local = appBookingListActionProvider;
        if (local == null) {
          local = new SwitchingProvider<>(2);
          appBookingListActionProvider = (Provider<AppBookingListAction>) local;
        }
        return (Provider<AppBookingListAction>) local;
      }

      private AppBookingBrowseVM_AssistedFactory getAppBookingBrowseVM_AssistedFactory() {
        return AppBookingBrowseVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getAppBookingBrowseUCProvider(), getAppBookingListActionProvider());
      }

      private Provider<AppBookingBrowseVM_AssistedFactory> getAppBookingBrowseVM_AssistedFactoryProvider(
          ) {
        Object local = appBookingBrowseVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(0);
          appBookingBrowseVM_AssistedFactoryProvider = (Provider<AppBookingBrowseVM_AssistedFactory>) local;
        }
        return (Provider<AppBookingBrowseVM_AssistedFactory>) local;
      }

      private AppBookingPrepareUC getAppBookingPrepareUC() {
        return new AppBookingPrepareUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getAppBookingService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<AppBookingPrepareUC> getAppBookingPrepareUCProvider() {
        Object local = appBookingPrepareUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(4);
          appBookingPrepareUCProvider = (Provider<AppBookingPrepareUC>) local;
        }
        return (Provider<AppBookingPrepareUC>) local;
      }

      private AppBookingLoadUC getAppBookingLoadUC() {
        return new AppBookingLoadUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getAppBookingService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<AppBookingLoadUC> getAppBookingLoadUCProvider() {
        Object local = appBookingLoadUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(5);
          appBookingLoadUCProvider = (Provider<AppBookingLoadUC>) local;
        }
        return (Provider<AppBookingLoadUC>) local;
      }

      private AppBookingProcessUC getAppBookingProcessUC() {
        return new AppBookingProcessUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getAppBookingService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<AppBookingProcessUC> getAppBookingProcessUCProvider() {
        Object local = appBookingProcessUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(6);
          appBookingProcessUCProvider = (Provider<AppBookingProcessUC>) local;
        }
        return (Provider<AppBookingProcessUC>) local;
      }

      private AppBookingCashVM_AssistedFactory getAppBookingCashVM_AssistedFactory() {
        return AppBookingCashVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getAppBookingPrepareUCProvider(), getAppBookingLoadUCProvider(), getAppBookingProcessUCProvider(), DaggerApp_HiltComponents_ApplicationC.this.getUtilityServiceProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppBookingServiceProvider());
      }

      private Provider<AppBookingCashVM_AssistedFactory> getAppBookingCashVM_AssistedFactoryProvider(
          ) {
        Object local = appBookingCashVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(3);
          appBookingCashVM_AssistedFactoryProvider = (Provider<AppBookingCashVM_AssistedFactory>) local;
        }
        return (Provider<AppBookingCashVM_AssistedFactory>) local;
      }

      private GetBookingFilterUC getGetBookingFilterUC() {
        return new GetBookingFilterUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getBookingService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetBookingFilterUC> getGetBookingFilterUCProvider() {
        Object local = getBookingFilterUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(8);
          getBookingFilterUCProvider = (Provider<GetBookingFilterUC>) local;
        }
        return (Provider<GetBookingFilterUC>) local;
      }

      private AppBookingFilterVM_AssistedFactory getAppBookingFilterVM_AssistedFactory() {
        return AppBookingFilterVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetBookingFilterUCProvider());
      }

      private Provider<AppBookingFilterVM_AssistedFactory> getAppBookingFilterVM_AssistedFactoryProvider(
          ) {
        Object local = appBookingFilterVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(7);
          appBookingFilterVM_AssistedFactoryProvider = (Provider<AppBookingFilterVM_AssistedFactory>) local;
        }
        return (Provider<AppBookingFilterVM_AssistedFactory>) local;
      }

      private AppBookingMultiActionUC getAppBookingMultiActionUC() {
        return new AppBookingMultiActionUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getAppBookingService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<AppBookingMultiActionUC> getAppBookingMultiActionUCProvider() {
        Object local = appBookingMultiActionUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(10);
          appBookingMultiActionUCProvider = (Provider<AppBookingMultiActionUC>) local;
        }
        return (Provider<AppBookingMultiActionUC>) local;
      }

      private AppBookingMultiActionVM_AssistedFactory getAppBookingMultiActionVM_AssistedFactory() {
        return AppBookingMultiActionVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getAppBookingMultiActionUCProvider());
      }

      private Provider<AppBookingMultiActionVM_AssistedFactory> getAppBookingMultiActionVM_AssistedFactoryProvider(
          ) {
        Object local = appBookingMultiActionVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(9);
          appBookingMultiActionVM_AssistedFactoryProvider = (Provider<AppBookingMultiActionVM_AssistedFactory>) local;
        }
        return (Provider<AppBookingMultiActionVM_AssistedFactory>) local;
      }

      private AppBookingTransferRekeningFormVM_AssistedFactory getAppBookingTransferRekeningFormVM_AssistedFactory(
          ) {
        return AppBookingTransferRekeningFormVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppBookingServiceProvider());
      }

      private Provider<AppBookingTransferRekeningFormVM_AssistedFactory> getAppBookingTransferRekeningFormVM_AssistedFactoryProvider(
          ) {
        Object local = appBookingTransferRekeningFormVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(11);
          appBookingTransferRekeningFormVM_AssistedFactoryProvider = (Provider<AppBookingTransferRekeningFormVM_AssistedFactory>) local;
        }
        return (Provider<AppBookingTransferRekeningFormVM_AssistedFactory>) local;
      }

      private AppBookingTransferVM_AssistedFactory getAppBookingTransferVM_AssistedFactory() {
        return AppBookingTransferVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getAppBookingPrepareUCProvider(), getAppBookingLoadUCProvider(), getAppBookingProcessUCProvider(), DaggerApp_HiltComponents_ApplicationC.this.getUtilityServiceProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppBookingServiceProvider());
      }

      private Provider<AppBookingTransferVM_AssistedFactory> getAppBookingTransferVM_AssistedFactoryProvider(
          ) {
        Object local = appBookingTransferVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(12);
          appBookingTransferVM_AssistedFactoryProvider = (Provider<AppBookingTransferVM_AssistedFactory>) local;
        }
        return (Provider<AppBookingTransferVM_AssistedFactory>) local;
      }

      private BankAccountBrowseUC getBankAccountBrowseUC() {
        return new BankAccountBrowseUC(DaggerApp_HiltComponents_ApplicationC.this.getAppBookingService());
      }

      private Provider<BankAccountBrowseUC> getBankAccountBrowseUCProvider() {
        Object local = bankAccountBrowseUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(14);
          bankAccountBrowseUCProvider = (Provider<BankAccountBrowseUC>) local;
        }
        return (Provider<BankAccountBrowseUC>) local;
      }

      private BankAccountBrowseVM_AssistedFactory getBankAccountBrowseVM_AssistedFactory() {
        return BankAccountBrowseVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getBankAccountBrowseUCProvider());
      }

      private Provider<BankAccountBrowseVM_AssistedFactory> getBankAccountBrowseVM_AssistedFactoryProvider(
          ) {
        Object local = bankAccountBrowseVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(13);
          bankAccountBrowseVM_AssistedFactoryProvider = (Provider<BankAccountBrowseVM_AssistedFactory>) local;
        }
        return (Provider<BankAccountBrowseVM_AssistedFactory>) local;
      }

      private GetBanksUseCase getGetBanksUseCase() {
        return new GetBanksUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getCommonService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetBanksUseCase> getGetBanksUseCaseProvider() {
        Object local = getBanksUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(16);
          getBanksUseCaseProvider = (Provider<GetBanksUseCase>) local;
        }
        return (Provider<GetBanksUseCase>) local;
      }

      private BankAcctFormVM_AssistedFactory getBankAcctFormVM_AssistedFactory() {
        return BankAcctFormVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetBanksUseCaseProvider());
      }

      private Provider<BankAcctFormVM_AssistedFactory> getBankAcctFormVM_AssistedFactoryProvider() {
        Object local = bankAcctFormVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(15);
          bankAcctFormVM_AssistedFactoryProvider = (Provider<BankAcctFormVM_AssistedFactory>) local;
        }
        return (Provider<BankAcctFormVM_AssistedFactory>) local;
      }

      private GetBookingListUseCase getGetBookingListUseCase() {
        return new GetBookingListUseCase(DaggerApp_HiltComponents_ApplicationC.this.getBookingService());
      }

      private Provider<GetBookingListUseCase> getGetBookingListUseCaseProvider() {
        Object local = getBookingListUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(18);
          getBookingListUseCaseProvider = (Provider<GetBookingListUseCase>) local;
        }
        return (Provider<GetBookingListUseCase>) local;
      }

      private BookingBrowseViewModel_AssistedFactory getBookingBrowseViewModel_AssistedFactory() {
        return BookingBrowseViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getGetBookingListUseCaseProvider());
      }

      private Provider<BookingBrowseViewModel_AssistedFactory> getBookingBrowseViewModel_AssistedFactoryProvider(
          ) {
        Object local = bookingBrowseViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(17);
          bookingBrowseViewModel_AssistedFactoryProvider = (Provider<BookingBrowseViewModel_AssistedFactory>) local;
        }
        return (Provider<BookingBrowseViewModel_AssistedFactory>) local;
      }

      private BookingLoadUC getBookingLoadUC() {
        return new BookingLoadUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getBookingService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<BookingLoadUC> getBookingLoadUCProvider() {
        Object local = bookingLoadUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(20);
          bookingLoadUCProvider = (Provider<BookingLoadUC>) local;
        }
        return (Provider<BookingLoadUC>) local;
      }

      private BookingProcessUC getBookingProcessUC() {
        return new BookingProcessUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getBookingService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<BookingProcessUC> getBookingProcessUCProvider() {
        Object local = bookingProcessUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(21);
          bookingProcessUCProvider = (Provider<BookingProcessUC>) local;
        }
        return (Provider<BookingProcessUC>) local;
      }

      private BookingDetailTransferVM_AssistedFactory getBookingDetailTransferVM_AssistedFactory() {
        return BookingDetailTransferVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getAppBookingPrepareUCProvider(), getAppBookingLoadUCProvider(), getAppBookingProcessUCProvider(), DaggerApp_HiltComponents_ApplicationC.this.getUtilityServiceProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppBookingServiceProvider(), getBookingLoadUCProvider(), getBookingProcessUCProvider());
      }

      private Provider<BookingDetailTransferVM_AssistedFactory> getBookingDetailTransferVM_AssistedFactoryProvider(
          ) {
        Object local = bookingDetailTransferVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(19);
          bookingDetailTransferVM_AssistedFactoryProvider = (Provider<BookingDetailTransferVM_AssistedFactory>) local;
        }
        return (Provider<BookingDetailTransferVM_AssistedFactory>) local;
      }

      private BookingDetailVM_AssistedFactory getBookingDetailVM_AssistedFactory() {
        return BookingDetailVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getAppBookingPrepareUCProvider(), getAppBookingLoadUCProvider(), getAppBookingProcessUCProvider(), DaggerApp_HiltComponents_ApplicationC.this.getUtilityServiceProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppBookingServiceProvider(), getBookingLoadUCProvider(), getBookingProcessUCProvider());
      }

      private Provider<BookingDetailVM_AssistedFactory> getBookingDetailVM_AssistedFactoryProvider(
          ) {
        Object local = bookingDetailVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(22);
          bookingDetailVM_AssistedFactoryProvider = (Provider<BookingDetailVM_AssistedFactory>) local;
        }
        return (Provider<BookingDetailVM_AssistedFactory>) local;
      }

      private BookingFilterVM_AssistedFactory getBookingFilterVM_AssistedFactory() {
        return BookingFilterVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetBookingFilterUCProvider());
      }

      private Provider<BookingFilterVM_AssistedFactory> getBookingFilterVM_AssistedFactoryProvider(
          ) {
        Object local = bookingFilterVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(23);
          bookingFilterVM_AssistedFactoryProvider = (Provider<BookingFilterVM_AssistedFactory>) local;
        }
        return (Provider<BookingFilterVM_AssistedFactory>) local;
      }

      private ChangePasswordUC getChangePasswordUC() {
        return new ChangePasswordUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getCommonService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<ChangePasswordUC> getChangePasswordUCProvider() {
        Object local = changePasswordUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(25);
          changePasswordUCProvider = (Provider<ChangePasswordUC>) local;
        }
        return (Provider<ChangePasswordUC>) local;
      }

      private ChangePasswordVM_AssistedFactory getChangePasswordVM_AssistedFactory() {
        return ChangePasswordVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getChangePasswordUCProvider());
      }

      private Provider<ChangePasswordVM_AssistedFactory> getChangePasswordVM_AssistedFactoryProvider(
          ) {
        Object local = changePasswordVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(24);
          changePasswordVM_AssistedFactoryProvider = (Provider<ChangePasswordVM_AssistedFactory>) local;
        }
        return (Provider<ChangePasswordVM_AssistedFactory>) local;
      }

      private GetExtUserInfoUseCase getGetExtUserInfoUseCase() {
        return new GetExtUserInfoUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getCommonService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetExtUserInfoUseCase> getGetExtUserInfoUseCaseProvider() {
        Object local = getExtUserInfoUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(27);
          getExtUserInfoUseCaseProvider = (Provider<GetExtUserInfoUseCase>) local;
        }
        return (Provider<GetExtUserInfoUseCase>) local;
      }

      private DrawerViewModel_AssistedFactory getDrawerViewModel_AssistedFactory() {
        return DrawerViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetExtUserInfoUseCaseProvider());
      }

      private Provider<DrawerViewModel_AssistedFactory> getDrawerViewModel_AssistedFactoryProvider(
          ) {
        Object local = drawerViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(26);
          drawerViewModel_AssistedFactoryProvider = (Provider<DrawerViewModel_AssistedFactory>) local;
        }
        return (Provider<DrawerViewModel_AssistedFactory>) local;
      }

      private GetDroppingAdditionalListUseCase getGetDroppingAdditionalListUseCase() {
        return new GetDroppingAdditionalListUseCase(DaggerApp_HiltComponents_ApplicationC.this.getDroppingAdditionalService());
      }

      private Provider<GetDroppingAdditionalListUseCase> getGetDroppingAdditionalListUseCaseProvider(
          ) {
        Object local = getDroppingAdditionalListUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(29);
          getDroppingAdditionalListUseCaseProvider = (Provider<GetDroppingAdditionalListUseCase>) local;
        }
        return (Provider<GetDroppingAdditionalListUseCase>) local;
      }

      private DroppingListActionUC getDroppingListActionUC() {
        return new DroppingListActionUC(DaggerApp_HiltComponents_ApplicationC.this.getDroppingAdditionalService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors(), ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule));
      }

      private Provider<DroppingListActionUC> getDroppingListActionUCProvider() {
        Object local = droppingListActionUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(30);
          droppingListActionUCProvider = (Provider<DroppingListActionUC>) local;
        }
        return (Provider<DroppingListActionUC>) local;
      }

      private DroppingAdditionalBrowseViewModel_AssistedFactory getDroppingAdditionalBrowseViewModel_AssistedFactory(
          ) {
        return DroppingAdditionalBrowseViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getGetDroppingAdditionalListUseCaseProvider(), getDroppingListActionUCProvider());
      }

      private Provider<DroppingAdditionalBrowseViewModel_AssistedFactory> getDroppingAdditionalBrowseViewModel_AssistedFactoryProvider(
          ) {
        Object local = droppingAdditionalBrowseViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(28);
          droppingAdditionalBrowseViewModel_AssistedFactoryProvider = (Provider<DroppingAdditionalBrowseViewModel_AssistedFactory>) local;
        }
        return (Provider<DroppingAdditionalBrowseViewModel_AssistedFactory>) local;
      }

      private DroppingAdditionalDetailEditViewModel_AssistedFactory getDroppingAdditionalDetailEditViewModel_AssistedFactory(
          ) {
        return DroppingAdditionalDetailEditViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider());
      }

      private Provider<DroppingAdditionalDetailEditViewModel_AssistedFactory> getDroppingAdditionalDetailEditViewModel_AssistedFactoryProvider(
          ) {
        Object local = droppingAdditionalDetailEditViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(31);
          droppingAdditionalDetailEditViewModel_AssistedFactoryProvider = (Provider<DroppingAdditionalDetailEditViewModel_AssistedFactory>) local;
        }
        return (Provider<DroppingAdditionalDetailEditViewModel_AssistedFactory>) local;
      }

      private PrepareDroppingAdditionalUseCase getPrepareDroppingAdditionalUseCase() {
        return new PrepareDroppingAdditionalUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getDroppingAdditionalService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<PrepareDroppingAdditionalUseCase> getPrepareDroppingAdditionalUseCaseProvider(
          ) {
        Object local = prepareDroppingAdditionalUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(33);
          prepareDroppingAdditionalUseCaseProvider = (Provider<PrepareDroppingAdditionalUseCase>) local;
        }
        return (Provider<PrepareDroppingAdditionalUseCase>) local;
      }

      private DroppingAdditionalSaveUseCase getDroppingAdditionalSaveUseCase() {
        return new DroppingAdditionalSaveUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getDroppingAdditionalService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<DroppingAdditionalSaveUseCase> getDroppingAdditionalSaveUseCaseProvider() {
        Object local = droppingAdditionalSaveUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(34);
          droppingAdditionalSaveUseCaseProvider = (Provider<DroppingAdditionalSaveUseCase>) local;
        }
        return (Provider<DroppingAdditionalSaveUseCase>) local;
      }

      private LoadDroppingAdditionalUseCase getLoadDroppingAdditionalUseCase() {
        return new LoadDroppingAdditionalUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getDroppingAdditionalService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<LoadDroppingAdditionalUseCase> getLoadDroppingAdditionalUseCaseProvider() {
        Object local = loadDroppingAdditionalUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(35);
          loadDroppingAdditionalUseCaseProvider = (Provider<LoadDroppingAdditionalUseCase>) local;
        }
        return (Provider<LoadDroppingAdditionalUseCase>) local;
      }

      private DroppingAdditionalSupervisorDecideUseCase getDroppingAdditionalSupervisorDecideUseCase(
          ) {
        return new DroppingAdditionalSupervisorDecideUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getDroppingAdditionalService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<DroppingAdditionalSupervisorDecideUseCase> getDroppingAdditionalSupervisorDecideUseCaseProvider(
          ) {
        Object local = droppingAdditionalSupervisorDecideUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(36);
          droppingAdditionalSupervisorDecideUseCaseProvider = (Provider<DroppingAdditionalSupervisorDecideUseCase>) local;
        }
        return (Provider<DroppingAdditionalSupervisorDecideUseCase>) local;
      }

      private DroppingAdditionalEditViewModel_AssistedFactory getDroppingAdditionalEditViewModel_AssistedFactory(
          ) {
        return DroppingAdditionalEditViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getPrepareDroppingAdditionalUseCaseProvider(), getDroppingAdditionalSaveUseCaseProvider(), getLoadDroppingAdditionalUseCaseProvider(), getDroppingAdditionalSupervisorDecideUseCaseProvider(), DaggerApp_HiltComponents_ApplicationC.this.getUtilityServiceProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider());
      }

      private Provider<DroppingAdditionalEditViewModel_AssistedFactory> getDroppingAdditionalEditViewModel_AssistedFactoryProvider(
          ) {
        Object local = droppingAdditionalEditViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(32);
          droppingAdditionalEditViewModel_AssistedFactoryProvider = (Provider<DroppingAdditionalEditViewModel_AssistedFactory>) local;
        }
        return (Provider<DroppingAdditionalEditViewModel_AssistedFactory>) local;
      }

      private GetDroppingAdditionalFilterUC getGetDroppingAdditionalFilterUC() {
        return new GetDroppingAdditionalFilterUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getDroppingAdditionalService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetDroppingAdditionalFilterUC> getGetDroppingAdditionalFilterUCProvider() {
        Object local = getDroppingAdditionalFilterUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(38);
          getDroppingAdditionalFilterUCProvider = (Provider<GetDroppingAdditionalFilterUC>) local;
        }
        return (Provider<GetDroppingAdditionalFilterUC>) local;
      }

      private DroppingAdditionalFilterVM_AssistedFactory getDroppingAdditionalFilterVM_AssistedFactory(
          ) {
        return DroppingAdditionalFilterVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetDroppingAdditionalFilterUCProvider());
      }

      private Provider<DroppingAdditionalFilterVM_AssistedFactory> getDroppingAdditionalFilterVM_AssistedFactoryProvider(
          ) {
        Object local = droppingAdditionalFilterVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(37);
          droppingAdditionalFilterVM_AssistedFactoryProvider = (Provider<DroppingAdditionalFilterVM_AssistedFactory>) local;
        }
        return (Provider<DroppingAdditionalFilterVM_AssistedFactory>) local;
      }

      private GetDroppingDailyListUC getGetDroppingDailyListUC() {
        return new GetDroppingDailyListUC(DaggerApp_HiltComponents_ApplicationC.this.getDroppingDailyService());
      }

      private Provider<GetDroppingDailyListUC> getGetDroppingDailyListUCProvider() {
        Object local = getDroppingDailyListUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(40);
          getDroppingDailyListUCProvider = (Provider<GetDroppingDailyListUC>) local;
        }
        return (Provider<GetDroppingDailyListUC>) local;
      }

      private DroppingDailyBrowseVM_AssistedFactory getDroppingDailyBrowseVM_AssistedFactory() {
        return DroppingDailyBrowseVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getGetDroppingDailyListUCProvider(), getDroppingListActionUCProvider());
      }

      private Provider<DroppingDailyBrowseVM_AssistedFactory> getDroppingDailyBrowseVM_AssistedFactoryProvider(
          ) {
        Object local = droppingDailyBrowseVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(39);
          droppingDailyBrowseVM_AssistedFactoryProvider = (Provider<DroppingDailyBrowseVM_AssistedFactory>) local;
        }
        return (Provider<DroppingDailyBrowseVM_AssistedFactory>) local;
      }

      private LoadDroppingDailyUseCase getLoadDroppingDailyUseCase() {
        return new LoadDroppingDailyUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getDroppingDailyService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<LoadDroppingDailyUseCase> getLoadDroppingDailyUseCaseProvider() {
        Object local = loadDroppingDailyUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(42);
          loadDroppingDailyUseCaseProvider = (Provider<LoadDroppingDailyUseCase>) local;
        }
        return (Provider<LoadDroppingDailyUseCase>) local;
      }

      private DroppingDailySupervisorDecideUseCase getDroppingDailySupervisorDecideUseCase() {
        return new DroppingDailySupervisorDecideUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getDroppingDailyService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<DroppingDailySupervisorDecideUseCase> getDroppingDailySupervisorDecideUseCaseProvider(
          ) {
        Object local = droppingDailySupervisorDecideUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(43);
          droppingDailySupervisorDecideUseCaseProvider = (Provider<DroppingDailySupervisorDecideUseCase>) local;
        }
        return (Provider<DroppingDailySupervisorDecideUseCase>) local;
      }

      private DroppingDailyEditViewModel_AssistedFactory getDroppingDailyEditViewModel_AssistedFactory(
          ) {
        return DroppingDailyEditViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getLoadDroppingDailyUseCaseProvider(), getDroppingDailySupervisorDecideUseCaseProvider(), DaggerApp_HiltComponents_ApplicationC.this.getUtilityServiceProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider());
      }

      private Provider<DroppingDailyEditViewModel_AssistedFactory> getDroppingDailyEditViewModel_AssistedFactoryProvider(
          ) {
        Object local = droppingDailyEditViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(41);
          droppingDailyEditViewModel_AssistedFactoryProvider = (Provider<DroppingDailyEditViewModel_AssistedFactory>) local;
        }
        return (Provider<DroppingDailyEditViewModel_AssistedFactory>) local;
      }

      private GetDroppingDailyFilterUC getGetDroppingDailyFilterUC() {
        return new GetDroppingDailyFilterUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getDroppingDailyService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetDroppingDailyFilterUC> getGetDroppingDailyFilterUCProvider() {
        Object local = getDroppingDailyFilterUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(45);
          getDroppingDailyFilterUCProvider = (Provider<GetDroppingDailyFilterUC>) local;
        }
        return (Provider<GetDroppingDailyFilterUC>) local;
      }

      private DroppingDailyFilterVM_AssistedFactory getDroppingDailyFilterVM_AssistedFactory() {
        return DroppingDailyFilterVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetDroppingDailyFilterUCProvider());
      }

      private Provider<DroppingDailyFilterVM_AssistedFactory> getDroppingDailyFilterVM_AssistedFactoryProvider(
          ) {
        Object local = droppingDailyFilterVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(44);
          droppingDailyFilterVM_AssistedFactoryProvider = (Provider<DroppingDailyFilterVM_AssistedFactory>) local;
        }
        return (Provider<DroppingDailyFilterVM_AssistedFactory>) local;
      }

      private GetDroppingDailyTaskBrowseHeaderUC getGetDroppingDailyTaskBrowseHeaderUC() {
        return new GetDroppingDailyTaskBrowseHeaderUC(DaggerApp_HiltComponents_ApplicationC.this.getDroppingDailyTaskService());
      }

      private Provider<GetDroppingDailyTaskBrowseHeaderUC> getGetDroppingDailyTaskBrowseHeaderUCProvider(
          ) {
        Object local = getDroppingDailyTaskBrowseHeaderUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(47);
          getDroppingDailyTaskBrowseHeaderUCProvider = (Provider<GetDroppingDailyTaskBrowseHeaderUC>) local;
        }
        return (Provider<GetDroppingDailyTaskBrowseHeaderUC>) local;
      }

      private GetDroppingDailyTaskFilterUC getGetDroppingDailyTaskFilterUC() {
        return new GetDroppingDailyTaskFilterUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getDroppingDailyTaskService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetDroppingDailyTaskFilterUC> getGetDroppingDailyTaskFilterUCProvider() {
        Object local = getDroppingDailyTaskFilterUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(48);
          getDroppingDailyTaskFilterUCProvider = (Provider<GetDroppingDailyTaskFilterUC>) local;
        }
        return (Provider<GetDroppingDailyTaskFilterUC>) local;
      }

      private DroppingDailyTaskBrowseVM_AssistedFactory getDroppingDailyTaskBrowseVM_AssistedFactory(
          ) {
        return DroppingDailyTaskBrowseVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getGetDroppingDailyTaskBrowseHeaderUCProvider(), getGetDroppingDailyTaskFilterUCProvider());
      }

      private Provider<DroppingDailyTaskBrowseVM_AssistedFactory> getDroppingDailyTaskBrowseVM_AssistedFactoryProvider(
          ) {
        Object local = droppingDailyTaskBrowseVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(46);
          droppingDailyTaskBrowseVM_AssistedFactoryProvider = (Provider<DroppingDailyTaskBrowseVM_AssistedFactory>) local;
        }
        return (Provider<DroppingDailyTaskBrowseVM_AssistedFactory>) local;
      }

      private GetDroppingDailyTaskBrowseDetailUC getGetDroppingDailyTaskBrowseDetailUC() {
        return new GetDroppingDailyTaskBrowseDetailUC(DaggerApp_HiltComponents_ApplicationC.this.getDroppingDailyTaskService());
      }

      private Provider<GetDroppingDailyTaskBrowseDetailUC> getGetDroppingDailyTaskBrowseDetailUCProvider(
          ) {
        Object local = getDroppingDailyTaskBrowseDetailUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(50);
          getDroppingDailyTaskBrowseDetailUCProvider = (Provider<GetDroppingDailyTaskBrowseDetailUC>) local;
        }
        return (Provider<GetDroppingDailyTaskBrowseDetailUC>) local;
      }

      private DroppingDailyTaskDetailBrowseVM_AssistedFactory getDroppingDailyTaskDetailBrowseVM_AssistedFactory(
          ) {
        return DroppingDailyTaskDetailBrowseVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getGetDroppingDailyTaskBrowseDetailUCProvider());
      }

      private Provider<DroppingDailyTaskDetailBrowseVM_AssistedFactory> getDroppingDailyTaskDetailBrowseVM_AssistedFactoryProvider(
          ) {
        Object local = droppingDailyTaskDetailBrowseVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(49);
          droppingDailyTaskDetailBrowseVM_AssistedFactoryProvider = (Provider<DroppingDailyTaskDetailBrowseVM_AssistedFactory>) local;
        }
        return (Provider<DroppingDailyTaskDetailBrowseVM_AssistedFactory>) local;
      }

      private DroppingDailyTaskFilterVM_AssistedFactory getDroppingDailyTaskFilterVM_AssistedFactory(
          ) {
        return DroppingDailyTaskFilterVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetDroppingDailyTaskFilterUCProvider());
      }

      private Provider<DroppingDailyTaskFilterVM_AssistedFactory> getDroppingDailyTaskFilterVM_AssistedFactoryProvider(
          ) {
        Object local = droppingDailyTaskFilterVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(51);
          droppingDailyTaskFilterVM_AssistedFactoryProvider = (Provider<DroppingDailyTaskFilterVM_AssistedFactory>) local;
        }
        return (Provider<DroppingDailyTaskFilterVM_AssistedFactory>) local;
      }

      private DroppingDailyTaskProcessUC getDroppingDailyTaskProcessUC() {
        return new DroppingDailyTaskProcessUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getDroppingDailyTaskService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<DroppingDailyTaskProcessUC> getDroppingDailyTaskProcessUCProvider() {
        Object local = droppingDailyTaskProcessUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(53);
          droppingDailyTaskProcessUCProvider = (Provider<DroppingDailyTaskProcessUC>) local;
        }
        return (Provider<DroppingDailyTaskProcessUC>) local;
      }

      private DroppingDailyTaskWfVM_AssistedFactory getDroppingDailyTaskWfVM_AssistedFactory() {
        return DroppingDailyTaskWfVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getDroppingDailyTaskProcessUCProvider());
      }

      private Provider<DroppingDailyTaskWfVM_AssistedFactory> getDroppingDailyTaskWfVM_AssistedFactoryProvider(
          ) {
        Object local = droppingDailyTaskWfVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(52);
          droppingDailyTaskWfVM_AssistedFactoryProvider = (Provider<DroppingDailyTaskWfVM_AssistedFactory>) local;
        }
        return (Provider<DroppingDailyTaskWfVM_AssistedFactory>) local;
      }

      private GetExpenseListUseCase getGetExpenseListUseCase() {
        return new GetExpenseListUseCase(DaggerApp_HiltComponents_ApplicationC.this.getExpenseService());
      }

      private Provider<GetExpenseListUseCase> getGetExpenseListUseCaseProvider() {
        Object local = getExpenseListUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(55);
          getExpenseListUseCaseProvider = (Provider<GetExpenseListUseCase>) local;
        }
        return (Provider<GetExpenseListUseCase>) local;
      }

      private ExpenseListAction getExpenseListAction() {
        return new ExpenseListAction(DaggerApp_HiltComponents_ApplicationC.this.getExpenseService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors(), ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule));
      }

      private Provider<ExpenseListAction> getExpenseListActionProvider() {
        Object local = expenseListActionProvider;
        if (local == null) {
          local = new SwitchingProvider<>(56);
          expenseListActionProvider = (Provider<ExpenseListAction>) local;
        }
        return (Provider<ExpenseListAction>) local;
      }

      private ExpenseBrowseViewModel_AssistedFactory getExpenseBrowseViewModel_AssistedFactory() {
        return ExpenseBrowseViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getGetExpenseListUseCaseProvider(), getExpenseListActionProvider());
      }

      private Provider<ExpenseBrowseViewModel_AssistedFactory> getExpenseBrowseViewModel_AssistedFactoryProvider(
          ) {
        Object local = expenseBrowseViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(54);
          expenseBrowseViewModel_AssistedFactoryProvider = (Provider<ExpenseBrowseViewModel_AssistedFactory>) local;
        }
        return (Provider<ExpenseBrowseViewModel_AssistedFactory>) local;
      }

      private PrepareNewExpenseUseCase getPrepareNewExpenseUseCase() {
        return new PrepareNewExpenseUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getExpenseService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<PrepareNewExpenseUseCase> getPrepareNewExpenseUseCaseProvider() {
        Object local = prepareNewExpenseUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(58);
          prepareNewExpenseUseCaseProvider = (Provider<PrepareNewExpenseUseCase>) local;
        }
        return (Provider<PrepareNewExpenseUseCase>) local;
      }

      private ExpenseSaveDraftUseCase getExpenseSaveDraftUseCase() {
        return new ExpenseSaveDraftUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getExpenseService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<ExpenseSaveDraftUseCase> getExpenseSaveDraftUseCaseProvider() {
        Object local = expenseSaveDraftUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(59);
          expenseSaveDraftUseCaseProvider = (Provider<ExpenseSaveDraftUseCase>) local;
        }
        return (Provider<ExpenseSaveDraftUseCase>) local;
      }

      private LoadExpenseUseCase getLoadExpenseUseCase() {
        return new LoadExpenseUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getExpenseService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<LoadExpenseUseCase> getLoadExpenseUseCaseProvider() {
        Object local = loadExpenseUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(60);
          loadExpenseUseCaseProvider = (Provider<LoadExpenseUseCase>) local;
        }
        return (Provider<LoadExpenseUseCase>) local;
      }

      private ExpenseAocDecideUseCase getExpenseAocDecideUseCase() {
        return new ExpenseAocDecideUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getExpenseService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<ExpenseAocDecideUseCase> getExpenseAocDecideUseCaseProvider() {
        Object local = expenseAocDecideUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(61);
          expenseAocDecideUseCaseProvider = (Provider<ExpenseAocDecideUseCase>) local;
        }
        return (Provider<ExpenseAocDecideUseCase>) local;
      }

      private ExpenseEditViewModel_AssistedFactory getExpenseEditViewModel_AssistedFactory() {
        return ExpenseEditViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getPrepareNewExpenseUseCaseProvider(), getExpenseSaveDraftUseCaseProvider(), getLoadExpenseUseCaseProvider(), getExpenseAocDecideUseCaseProvider(), DaggerApp_HiltComponents_ApplicationC.this.getExpenseServiceProvider(), DaggerApp_HiltComponents_ApplicationC.this.getUtilityServiceProvider(), DaggerApp_HiltComponents_ApplicationC.this.getLoginUtilProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider());
      }

      private Provider<ExpenseEditViewModel_AssistedFactory> getExpenseEditViewModel_AssistedFactoryProvider(
          ) {
        Object local = expenseEditViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(57);
          expenseEditViewModel_AssistedFactoryProvider = (Provider<ExpenseEditViewModel_AssistedFactory>) local;
        }
        return (Provider<ExpenseEditViewModel_AssistedFactory>) local;
      }

      private ExpenseFilterUC getExpenseFilterUC() {
        return new ExpenseFilterUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getExpenseService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<ExpenseFilterUC> getExpenseFilterUCProvider() {
        Object local = expenseFilterUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(63);
          expenseFilterUCProvider = (Provider<ExpenseFilterUC>) local;
        }
        return (Provider<ExpenseFilterUC>) local;
      }

      private ExpenseFilterVM_AssistedFactory getExpenseFilterVM_AssistedFactory() {
        return ExpenseFilterVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getExpenseFilterUCProvider());
      }

      private Provider<ExpenseFilterVM_AssistedFactory> getExpenseFilterVM_AssistedFactoryProvider(
          ) {
        Object local = expenseFilterVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(62);
          expenseFilterVM_AssistedFactoryProvider = (Provider<ExpenseFilterVM_AssistedFactory>) local;
        }
        return (Provider<ExpenseFilterVM_AssistedFactory>) local;
      }

      private AuthorizationHeaderProvider getAuthorizationHeaderProvider() {
        return new AuthorizationHeaderProvider(DaggerApp_HiltComponents_ApplicationC.this.getAndroconConfig(), DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
      }

      private GetTokenUseCase getGetTokenUseCase() {
        return new GetTokenUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), getAuthorizationHeaderProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAuthService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors(), DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
      }

      private Provider<GetTokenUseCase> getGetTokenUseCaseProvider() {
        Object local = getTokenUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(65);
          getTokenUseCaseProvider = (Provider<GetTokenUseCase>) local;
        }
        return (Provider<GetTokenUseCase>) local;
      }

      private RegisterDeviceUseCaseSync getRegisterDeviceUseCaseSync() {
        return new RegisterDeviceUseCaseSync(getAuthorizationHeaderProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAndroconConfig(), DaggerApp_HiltComponents_ApplicationC.this.getDeviceRegistrationService(), DaggerApp_HiltComponents_ApplicationC.this.getRegistrationUtil());
      }

      private Provider<RegisterDeviceUseCaseSync> getRegisterDeviceUseCaseSyncProvider() {
        Object local = registerDeviceUseCaseSyncProvider;
        if (local == null) {
          local = new SwitchingProvider<>(66);
          registerDeviceUseCaseSyncProvider = (Provider<RegisterDeviceUseCaseSync>) local;
        }
        return (Provider<RegisterDeviceUseCaseSync>) local;
      }

      private LoginViewModel_AssistedFactory getLoginViewModel_AssistedFactory() {
        return LoginViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetTokenUseCaseProvider(), getRegisterDeviceUseCaseSyncProvider());
      }

      private Provider<LoginViewModel_AssistedFactory> getLoginViewModel_AssistedFactoryProvider() {
        Object local = loginViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(64);
          loginViewModel_AssistedFactoryProvider = (Provider<LoginViewModel_AssistedFactory>) local;
        }
        return (Provider<LoginViewModel_AssistedFactory>) local;
      }

      private GetOutletBalanceListUseCase getGetOutletBalanceListUseCase() {
        return new GetOutletBalanceListUseCase(DaggerApp_HiltComponents_ApplicationC.this.getOutletReportService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetOutletBalanceListUseCase> getGetOutletBalanceListUseCaseProvider() {
        Object local = getOutletBalanceListUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(68);
          getOutletBalanceListUseCaseProvider = (Provider<GetOutletBalanceListUseCase>) local;
        }
        return (Provider<GetOutletBalanceListUseCase>) local;
      }

      private OutletBalanceBrowseVM_AssistedFactory getOutletBalanceBrowseVM_AssistedFactory() {
        return OutletBalanceBrowseVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getGetOutletBalanceListUseCaseProvider());
      }

      private Provider<OutletBalanceBrowseVM_AssistedFactory> getOutletBalanceBrowseVM_AssistedFactoryProvider(
          ) {
        Object local = outletBalanceBrowseVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(67);
          outletBalanceBrowseVM_AssistedFactoryProvider = (Provider<OutletBalanceBrowseVM_AssistedFactory>) local;
        }
        return (Provider<OutletBalanceBrowseVM_AssistedFactory>) local;
      }

      private GetOutletBalanceReportUseCase getGetOutletBalanceReportUseCase() {
        return new GetOutletBalanceReportUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getOutletReportService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetOutletBalanceReportUseCase> getGetOutletBalanceReportUseCaseProvider() {
        Object local = getOutletBalanceReportUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(70);
          getOutletBalanceReportUseCaseProvider = (Provider<GetOutletBalanceReportUseCase>) local;
        }
        return (Provider<GetOutletBalanceReportUseCase>) local;
      }

      private OutletBalanceReportVM_AssistedFactory getOutletBalanceReportVM_AssistedFactory() {
        return OutletBalanceReportVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetOutletBalanceReportUseCaseProvider());
      }

      private Provider<OutletBalanceReportVM_AssistedFactory> getOutletBalanceReportVM_AssistedFactoryProvider(
          ) {
        Object local = outletBalanceReportVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(69);
          outletBalanceReportVM_AssistedFactoryProvider = (Provider<OutletBalanceReportVM_AssistedFactory>) local;
        }
        return (Provider<OutletBalanceReportVM_AssistedFactory>) local;
      }

      private GetOutletStockByDayReportUC getGetOutletStockByDayReportUC() {
        return new GetOutletStockByDayReportUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getOutletReportService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetOutletStockByDayReportUC> getGetOutletStockByDayReportUCProvider() {
        Object local = getOutletStockByDayReportUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(72);
          getOutletStockByDayReportUCProvider = (Provider<GetOutletStockByDayReportUC>) local;
        }
        return (Provider<GetOutletStockByDayReportUC>) local;
      }

      private OutletStockReportByDayVM_AssistedFactory getOutletStockReportByDayVM_AssistedFactory(
          ) {
        return OutletStockReportByDayVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetOutletStockByDayReportUCProvider());
      }

      private Provider<OutletStockReportByDayVM_AssistedFactory> getOutletStockReportByDayVM_AssistedFactoryProvider(
          ) {
        Object local = outletStockReportByDayVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(71);
          outletStockReportByDayVM_AssistedFactoryProvider = (Provider<OutletStockReportByDayVM_AssistedFactory>) local;
        }
        return (Provider<OutletStockReportByDayVM_AssistedFactory>) local;
      }

      private GetLatestVersionUseCase getGetLatestVersionUseCase() {
        return new GetLatestVersionUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors(), DaggerApp_HiltComponents_ApplicationC.this.getUtilityService());
      }

      private Provider<GetLatestVersionUseCase> getGetLatestVersionUseCaseProvider() {
        Object local = getLatestVersionUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(74);
          getLatestVersionUseCaseProvider = (Provider<GetLatestVersionUseCase>) local;
        }
        return (Provider<GetLatestVersionUseCase>) local;
      }

      private DownloadUpdateUseCase getDownloadUpdateUseCase() {
        return new DownloadUpdateUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getAndroconConfig(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors(), DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
      }

      private Provider<DownloadUpdateUseCase> getDownloadUpdateUseCaseProvider() {
        Object local = downloadUpdateUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(75);
          downloadUpdateUseCaseProvider = (Provider<DownloadUpdateUseCase>) local;
        }
        return (Provider<DownloadUpdateUseCase>) local;
      }

      private PreferenceViewModel_AssistedFactory getPreferenceViewModel_AssistedFactory() {
        return PreferenceViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAndroconConfigProvider(), getGetLatestVersionUseCaseProvider(), getDownloadUpdateUseCaseProvider());
      }

      private Provider<PreferenceViewModel_AssistedFactory> getPreferenceViewModel_AssistedFactoryProvider(
          ) {
        Object local = preferenceViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(73);
          preferenceViewModel_AssistedFactoryProvider = (Provider<PreferenceViewModel_AssistedFactory>) local;
        }
        return (Provider<PreferenceViewModel_AssistedFactory>) local;
      }

      private GetUserPermissionUseCase getGetUserPermissionUseCase() {
        return new GetUserPermissionUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getUserService());
      }

      private Provider<GetUserPermissionUseCase> getGetUserPermissionUseCaseProvider() {
        Object local = getUserPermissionUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(77);
          getUserPermissionUseCaseProvider = (Provider<GetUserPermissionUseCase>) local;
        }
        return (Provider<GetUserPermissionUseCase>) local;
      }

      private Provider<AuthorizationHeaderProvider> getAuthorizationHeaderProviderProvider() {
        Object local = authorizationHeaderProvider;
        if (local == null) {
          local = new SwitchingProvider<>(78);
          authorizationHeaderProvider = (Provider<AuthorizationHeaderProvider>) local;
        }
        return (Provider<AuthorizationHeaderProvider>) local;
      }

      private ProtectedActivityViewModel_AssistedFactory getProtectedActivityViewModel_AssistedFactory(
          ) {
        return ProtectedActivityViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetUserPermissionUseCaseProvider(), DaggerApp_HiltComponents_ApplicationC.this.getLoginUtilProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAuthServiceProvider(), getAuthorizationHeaderProviderProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider());
      }

      private Provider<ProtectedActivityViewModel_AssistedFactory> getProtectedActivityViewModel_AssistedFactoryProvider(
          ) {
        Object local = protectedActivityViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(76);
          protectedActivityViewModel_AssistedFactoryProvider = (Provider<ProtectedActivityViewModel_AssistedFactory>) local;
        }
        return (Provider<ProtectedActivityViewModel_AssistedFactory>) local;
      }

      private RegisterDeviceUseCase getRegisterDeviceUseCase() {
        return new RegisterDeviceUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors(), getRegisterDeviceUseCaseSync());
      }

      private Provider<RegisterDeviceUseCase> getRegisterDeviceUseCaseProvider() {
        Object local = registerDeviceUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(80);
          registerDeviceUseCaseProvider = (Provider<RegisterDeviceUseCase>) local;
        }
        return (Provider<RegisterDeviceUseCase>) local;
      }

      private RegisterDeviceViewModel_AssistedFactory getRegisterDeviceViewModel_AssistedFactory() {
        return RegisterDeviceViewModel_AssistedFactory_Factory.newInstance(getRegisterDeviceUseCaseProvider());
      }

      private Provider<RegisterDeviceViewModel_AssistedFactory> getRegisterDeviceViewModel_AssistedFactoryProvider(
          ) {
        Object local = registerDeviceViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(79);
          registerDeviceViewModel_AssistedFactoryProvider = (Provider<RegisterDeviceViewModel_AssistedFactory>) local;
        }
        return (Provider<RegisterDeviceViewModel_AssistedFactory>) local;
      }

      private GetBookingMediatorListUseCase getGetBookingMediatorListUseCase() {
        return new GetBookingMediatorListUseCase(DaggerApp_HiltComponents_ApplicationC.this.getExpenseService());
      }

      private Provider<GetBookingMediatorListUseCase> getGetBookingMediatorListUseCaseProvider() {
        Object local = getBookingMediatorListUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(82);
          getBookingMediatorListUseCaseProvider = (Provider<GetBookingMediatorListUseCase>) local;
        }
        return (Provider<GetBookingMediatorListUseCase>) local;
      }

      private SelectBookingViewModel_AssistedFactory getSelectBookingViewModel_AssistedFactory() {
        return SelectBookingViewModel_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getGetBookingMediatorListUseCaseProvider());
      }

      private Provider<SelectBookingViewModel_AssistedFactory> getSelectBookingViewModel_AssistedFactoryProvider(
          ) {
        Object local = selectBookingViewModel_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(81);
          selectBookingViewModel_AssistedFactoryProvider = (Provider<SelectBookingViewModel_AssistedFactory>) local;
        }
        return (Provider<SelectBookingViewModel_AssistedFactory>) local;
      }

      private GetSubstituteUsersUC getGetSubstituteUsersUC() {
        return new GetSubstituteUsersUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getCommonService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetSubstituteUsersUC> getGetSubstituteUsersUCProvider() {
        Object local = getSubstituteUsersUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(84);
          getSubstituteUsersUCProvider = (Provider<GetSubstituteUsersUC>) local;
        }
        return (Provider<GetSubstituteUsersUC>) local;
      }

      private SubstituteUserUC getSubstituteUserUC() {
        return new SubstituteUserUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getCommonService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<SubstituteUserUC> getSubstituteUserUCProvider() {
        Object local = substituteUserUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(85);
          substituteUserUCProvider = (Provider<SubstituteUserUC>) local;
        }
        return (Provider<SubstituteUserUC>) local;
      }

      private SubstituteUserVM_AssistedFactory getSubstituteUserVM_AssistedFactory() {
        return SubstituteUserVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetSubstituteUsersUCProvider(), getSubstituteUserUCProvider());
      }

      private Provider<SubstituteUserVM_AssistedFactory> getSubstituteUserVM_AssistedFactoryProvider(
          ) {
        Object local = substituteUserVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(83);
          substituteUserVM_AssistedFactoryProvider = (Provider<SubstituteUserVM_AssistedFactory>) local;
        }
        return (Provider<SubstituteUserVM_AssistedFactory>) local;
      }

      private TugasTransferCekSaldoUC getTugasTransferCekSaldoUC() {
        return new TugasTransferCekSaldoUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getTugasTransferService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<TugasTransferCekSaldoUC> getTugasTransferCekSaldoUCProvider() {
        Object local = tugasTransferCekSaldoUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(87);
          tugasTransferCekSaldoUCProvider = (Provider<TugasTransferCekSaldoUC>) local;
        }
        return (Provider<TugasTransferCekSaldoUC>) local;
      }

      private TugasTransferCekSaldoBankListUC getTugasTransferCekSaldoBankListUC() {
        return new TugasTransferCekSaldoBankListUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getTugasTransferService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<TugasTransferCekSaldoBankListUC> getTugasTransferCekSaldoBankListUCProvider(
          ) {
        Object local = tugasTransferCekSaldoBankListUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(88);
          tugasTransferCekSaldoBankListUCProvider = (Provider<TugasTransferCekSaldoBankListUC>) local;
        }
        return (Provider<TugasTransferCekSaldoBankListUC>) local;
      }

      private TugasTransferCekSaldoAccountListUC getTugasTransferCekSaldoAccountListUC() {
        return new TugasTransferCekSaldoAccountListUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getTugasTransferService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<TugasTransferCekSaldoAccountListUC> getTugasTransferCekSaldoAccountListUCProvider(
          ) {
        Object local = tugasTransferCekSaldoAccountListUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(89);
          tugasTransferCekSaldoAccountListUCProvider = (Provider<TugasTransferCekSaldoAccountListUC>) local;
        }
        return (Provider<TugasTransferCekSaldoAccountListUC>) local;
      }

      private TugasTransferCekSaldoVM_AssistedFactory getTugasTransferCekSaldoVM_AssistedFactory() {
        return TugasTransferCekSaldoVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getTugasTransferCekSaldoUCProvider(), getTugasTransferCekSaldoBankListUCProvider(), getTugasTransferCekSaldoAccountListUCProvider());
      }

      private Provider<TugasTransferCekSaldoVM_AssistedFactory> getTugasTransferCekSaldoVM_AssistedFactoryProvider(
          ) {
        Object local = tugasTransferCekSaldoVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(86);
          tugasTransferCekSaldoVM_AssistedFactoryProvider = (Provider<TugasTransferCekSaldoVM_AssistedFactory>) local;
        }
        return (Provider<TugasTransferCekSaldoVM_AssistedFactory>) local;
      }

      private GetTugasTransferDetailBrowseUC getGetTugasTransferDetailBrowseUC() {
        return new GetTugasTransferDetailBrowseUC(DaggerApp_HiltComponents_ApplicationC.this.getTugasTransferService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetTugasTransferDetailBrowseUC> getGetTugasTransferDetailBrowseUCProvider() {
        Object local = getTugasTransferDetailBrowseUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(91);
          getTugasTransferDetailBrowseUCProvider = (Provider<GetTugasTransferDetailBrowseUC>) local;
        }
        return (Provider<GetTugasTransferDetailBrowseUC>) local;
      }

      private TugasTransferDetailBrowseVM_AssistedFactory getTugasTransferDetailBrowseVM_AssistedFactory(
          ) {
        return TugasTransferDetailBrowseVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetTugasTransferDetailBrowseUCProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider());
      }

      private Provider<TugasTransferDetailBrowseVM_AssistedFactory> getTugasTransferDetailBrowseVM_AssistedFactoryProvider(
          ) {
        Object local = tugasTransferDetailBrowseVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(90);
          tugasTransferDetailBrowseVM_AssistedFactoryProvider = (Provider<TugasTransferDetailBrowseVM_AssistedFactory>) local;
        }
        return (Provider<TugasTransferDetailBrowseVM_AssistedFactory>) local;
      }

      private GetTugasTransferFilterUC getGetTugasTransferFilterUC() {
        return new GetTugasTransferFilterUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getTugasTransferService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetTugasTransferFilterUC> getGetTugasTransferFilterUCProvider() {
        Object local = getTugasTransferFilterUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(93);
          getTugasTransferFilterUCProvider = (Provider<GetTugasTransferFilterUC>) local;
        }
        return (Provider<GetTugasTransferFilterUC>) local;
      }

      private TugasTransferFilterVM_AssistedFactory getTugasTransferFilterVM_AssistedFactory() {
        return TugasTransferFilterVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetTugasTransferFilterUCProvider());
      }

      private Provider<TugasTransferFilterVM_AssistedFactory> getTugasTransferFilterVM_AssistedFactoryProvider(
          ) {
        Object local = tugasTransferFilterVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(92);
          tugasTransferFilterVM_AssistedFactoryProvider = (Provider<TugasTransferFilterVM_AssistedFactory>) local;
        }
        return (Provider<TugasTransferFilterVM_AssistedFactory>) local;
      }

      private GetTugasTransferBrowseUC getGetTugasTransferBrowseUC() {
        return new GetTugasTransferBrowseUC(DaggerApp_HiltComponents_ApplicationC.this.getTugasTransferService());
      }

      private Provider<GetTugasTransferBrowseUC> getGetTugasTransferBrowseUCProvider() {
        Object local = getTugasTransferBrowseUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(95);
          getTugasTransferBrowseUCProvider = (Provider<GetTugasTransferBrowseUC>) local;
        }
        return (Provider<GetTugasTransferBrowseUC>) local;
      }

      private TugasTransferHeaderBrowseVM_AssistedFactory getTugasTransferHeaderBrowseVM_AssistedFactory(
          ) {
        return TugasTransferHeaderBrowseVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutorsProvider(), getGetTugasTransferBrowseUCProvider(), getGetTugasTransferFilterUCProvider());
      }

      private Provider<TugasTransferHeaderBrowseVM_AssistedFactory> getTugasTransferHeaderBrowseVM_AssistedFactoryProvider(
          ) {
        Object local = tugasTransferHeaderBrowseVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(94);
          tugasTransferHeaderBrowseVM_AssistedFactoryProvider = (Provider<TugasTransferHeaderBrowseVM_AssistedFactory>) local;
        }
        return (Provider<TugasTransferHeaderBrowseVM_AssistedFactory>) local;
      }

      private TugasTransferWfFormUC getTugasTransferWfFormUC() {
        return new TugasTransferWfFormUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getTugasTransferService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<TugasTransferWfFormUC> getTugasTransferWfFormUCProvider() {
        Object local = tugasTransferWfFormUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(97);
          tugasTransferWfFormUCProvider = (Provider<TugasTransferWfFormUC>) local;
        }
        return (Provider<TugasTransferWfFormUC>) local;
      }

      private TugasTransferWfProcessUC getTugasTransferWfProcessUC() {
        return new TugasTransferWfProcessUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getTugasTransferService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<TugasTransferWfProcessUC> getTugasTransferWfProcessUCProvider() {
        Object local = tugasTransferWfProcessUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(98);
          tugasTransferWfProcessUCProvider = (Provider<TugasTransferWfProcessUC>) local;
        }
        return (Provider<TugasTransferWfProcessUC>) local;
      }

      private TugasTransferWFGenerateOtpUC getTugasTransferWFGenerateOtpUC() {
        return new TugasTransferWFGenerateOtpUC(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getTugasTransferService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<TugasTransferWFGenerateOtpUC> getTugasTransferWFGenerateOtpUCProvider() {
        Object local = tugasTransferWFGenerateOtpUCProvider;
        if (local == null) {
          local = new SwitchingProvider<>(99);
          tugasTransferWFGenerateOtpUCProvider = (Provider<TugasTransferWFGenerateOtpUC>) local;
        }
        return (Provider<TugasTransferWFGenerateOtpUC>) local;
      }

      private TugasTransferWfVM_AssistedFactory getTugasTransferWfVM_AssistedFactory() {
        return TugasTransferWfVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getTugasTransferWfFormUCProvider(), getTugasTransferWfProcessUCProvider(), getTugasTransferWFGenerateOtpUCProvider());
      }

      private Provider<TugasTransferWfVM_AssistedFactory> getTugasTransferWfVM_AssistedFactoryProvider(
          ) {
        Object local = tugasTransferWfVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(96);
          tugasTransferWfVM_AssistedFactoryProvider = (Provider<TugasTransferWfVM_AssistedFactory>) local;
        }
        return (Provider<TugasTransferWfVM_AssistedFactory>) local;
      }

      private GetWfHistoryUseCase getGetWfHistoryUseCase() {
        return new GetWfHistoryUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getExpenseService(), DaggerApp_HiltComponents_ApplicationC.this.getDroppingDailyService(), DaggerApp_HiltComponents_ApplicationC.this.getDroppingAdditionalService(), DaggerApp_HiltComponents_ApplicationC.this.getAppBookingService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
      }

      private Provider<GetWfHistoryUseCase> getGetWfHistoryUseCaseProvider() {
        Object local = getWfHistoryUseCaseProvider;
        if (local == null) {
          local = new SwitchingProvider<>(101);
          getWfHistoryUseCaseProvider = (Provider<GetWfHistoryUseCase>) local;
        }
        return (Provider<GetWfHistoryUseCase>) local;
      }

      private WfHistoryVM_AssistedFactory getWfHistoryVM_AssistedFactory() {
        return WfHistoryVM_AssistedFactory_Factory.newInstance(DaggerApp_HiltComponents_ApplicationC.this.getApplicationProvider(), getGetWfHistoryUseCaseProvider());
      }

      private Provider<WfHistoryVM_AssistedFactory> getWfHistoryVM_AssistedFactoryProvider() {
        Object local = wfHistoryVM_AssistedFactoryProvider;
        if (local == null) {
          local = new SwitchingProvider<>(100);
          wfHistoryVM_AssistedFactoryProvider = (Provider<WfHistoryVM_AssistedFactory>) local;
        }
        return (Provider<WfHistoryVM_AssistedFactory>) local;
      }

      private Map<String, Provider<ViewModelAssistedFactory<? extends ViewModel>>> getMapOfStringAndProviderOfViewModelAssistedFactoryOf(
          ) {
        return ImmutableMap.<String, Provider<ViewModelAssistedFactory<? extends ViewModel>>>builderWithExpectedSize(43).put("id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseVM", (Provider) getAppBookingBrowseVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash.AppBookingCashVM", (Provider) getAppBookingCashVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterVM", (Provider) getAppBookingFilterVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionVM", (Provider) getAppBookingMultiActionVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningFormVM", (Provider) getAppBookingTransferRekeningFormVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferVM", (Provider) getAppBookingTransferVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.BankAccountBrowseVM", (Provider) getBankAccountBrowseVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.BankAcctFormVM", (Provider) getBankAcctFormVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel", (Provider) getBookingBrowseViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailTransferVM", (Provider) getBookingDetailTransferVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailVM", (Provider) getBookingDetailVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.booking.BookingFilterVM", (Provider) getBookingFilterVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordVM", (Provider) getChangePasswordVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.ui.landing.DrawerViewModel", (Provider) getDrawerViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseViewModel", (Provider) getDroppingAdditionalBrowseViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditViewModel", (Provider) getDroppingAdditionalDetailEditViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditViewModel", (Provider) getDroppingAdditionalEditViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.DroppingAdditionalFilterVM", (Provider) getDroppingAdditionalFilterVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseVM", (Provider) getDroppingDailyBrowseVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditViewModel", (Provider) getDroppingDailyEditViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.DroppingDailyFilterVM", (Provider) getDroppingDailyFilterVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseVM", (Provider) getDroppingDailyTaskBrowseVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail.DroppingDailyTaskDetailBrowseVM", (Provider) getDroppingDailyTaskDetailBrowseVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterVM", (Provider) getDroppingDailyTaskFilterVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfVM", (Provider) getDroppingDailyTaskWfVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseViewModel", (Provider) getExpenseBrowseViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseEditViewModel", (Provider) getExpenseEditViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterVM", (Provider) getExpenseFilterVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.ui.login.LoginViewModel", (Provider) getLoginViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceBrowseVM", (Provider) getOutletBalanceBrowseVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportVM", (Provider) getOutletBalanceReportVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockReportByDayVM", (Provider) getOutletStockReportByDayVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceViewModel", (Provider) getPreferenceViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.androcon.security.ProtectedActivityViewModel", (Provider) getProtectedActivityViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.androcon.registration.RegisterDeviceViewModel", (Provider) getRegisterDeviceViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.SelectBookingViewModel", (Provider) getSelectBookingViewModel_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.security.SubstituteUserVM", (Provider) getSubstituteUserVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoVM", (Provider) getTugasTransferCekSaldoVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailBrowseVM", (Provider) getTugasTransferDetailBrowseVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterVM", (Provider) getTugasTransferFilterVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferHeaderBrowseVM", (Provider) getTugasTransferHeaderBrowseVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfVM", (Provider) getTugasTransferWfVM_AssistedFactoryProvider()).put("id.co.danwinciptaniaga.npmdsandroid.wf.WfHistoryVM", (Provider) getWfHistoryVM_AssistedFactoryProvider()).build();
      }

      private ViewModelProvider.Factory getProvideFactory() {
        return ViewModelFactoryModules_ActivityModule_ProvideFactoryFactory.provideFactory(activity, ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), getMapOfStringAndProviderOfViewModelAssistedFactoryOf());
      }

      private GetTokenUseCaseSync getGetTokenUseCaseSync() {
        return new GetTokenUseCaseSync(getAuthorizationHeaderProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAuthService());
      }

      @Override
      public Set<ViewModelProvider.Factory> getActivityViewModelFactory() {
        return ImmutableSet.<ViewModelProvider.Factory>of(getProvideFactory());
      }

      @Override
      public FragmentComponentBuilder fragmentComponentBuilder() {
        return new FragmentCBuilder();
      }

      @Override
      public ViewComponentBuilder viewComponentBuilder() {
        return new ViewCBuilder();
      }

      @Override
      public void injectLaunchActivity(LaunchActivity launchActivity) {
        injectLaunchActivity2(launchActivity);
      }

      @Override
      public void injectLandingActivity(LandingActivity landingActivity) {
        injectLandingActivity2(landingActivity);
      }

      @Override
      public void injectAuthenticationActivity(AuthenticationActivity authenticationActivity) {
        injectAuthenticationActivity2(authenticationActivity);
      }

      @CanIgnoreReturnValue
      private LaunchActivity injectLaunchActivity2(LaunchActivity instance) {
        LaunchActivity_MembersInjector.injectAndroconConfig(instance, DaggerApp_HiltComponents_ApplicationC.this.getAndroconConfig());
        LaunchActivity_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
        LaunchActivity_MembersInjector.injectLoginUtil(instance, DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
        LaunchActivity_MembersInjector.injectRegistrationUtil(instance, DaggerApp_HiltComponents_ApplicationC.this.getRegistrationUtil());
        LaunchActivity_MembersInjector.injectGetTokenUseCaseSync(instance, getGetTokenUseCaseSync());
        return instance;
      }

      @CanIgnoreReturnValue
      private LandingActivity injectLandingActivity2(LandingActivity instance) {
        ProtectedActivity_MembersInjector.injectLoginUtil(instance, DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
        ProtectedActivity_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
        BaseActivity_MembersInjector.injectAndroconConfig(instance, DaggerApp_HiltComponents_ApplicationC.this.getAndroconConfig());
        LandingActivity_MembersInjector.injectLoginUtil(instance, DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
        return instance;
      }

      @CanIgnoreReturnValue
      private AuthenticationActivity injectAuthenticationActivity2(
          AuthenticationActivity instance) {
        AuthenticationActivity_MembersInjector.injectLoginUtil(instance, DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
        AuthenticationActivity_MembersInjector.injectAuthService(instance, DaggerApp_HiltComponents_ApplicationC.this.getAuthService());
        AuthenticationActivity_MembersInjector.injectAhp(instance, getAuthorizationHeaderProvider());
        AuthenticationActivity_MembersInjector.injectExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
        AuthenticationActivity_MembersInjector.injectUcGetTokenSync(instance, getGetTokenUseCaseSync());
        return instance;
      }

      private final class FragmentCBuilder implements App_HiltComponents.FragmentC.Builder {
        private Fragment fragment;

        @Override
        public FragmentCBuilder fragment(Fragment fragment) {
          this.fragment = Preconditions.checkNotNull(fragment);
          return this;
        }

        @Override
        public App_HiltComponents.FragmentC build() {
          Preconditions.checkBuilderRequirement(fragment, Fragment.class);
          return new FragmentCImpl(fragment);
        }
      }

      private final class FragmentCImpl extends App_HiltComponents.FragmentC {
        private final Fragment fragment;

        private FragmentCImpl(Fragment fragmentParam) {
          this.fragment = fragmentParam;
        }

        private ViewModelProvider.Factory getProvideFactory() {
          return ViewModelFactoryModules_FragmentModule_ProvideFactoryFactory.provideFactory(fragment, ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), ActivityCImpl.this.getMapOfStringAndProviderOfViewModelAssistedFactoryOf());
        }

        private TestPushNotificationUseCase getTestPushNotificationUseCase() {
          return new TestPushNotificationUseCase(DaggerApp_HiltComponents_ApplicationC.this.getCommonService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
        }

        private UploadLogFileUseCase getUploadLogFileUseCase() {
          return new UploadLogFileUseCase(ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule), DaggerApp_HiltComponents_ApplicationC.this.getUtilityService(), DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
        }

        @Override
        public Set<ViewModelProvider.Factory> getFragmentViewModelFactory() {
          return ImmutableSet.<ViewModelProvider.Factory>of(getProvideFactory());
        }

        @Override
        public ViewWithFragmentComponentBuilder viewWithFragmentComponentBuilder() {
          return new ViewWithFragmentCBuilder();
        }

        @Override
        public void injectBookingBrowseFragment(BookingBrowseFragment bookingBrowseFragment) {
        }

        @Override
        public void injectBookingFilterFragment(BookingFilterFragment bookingFilterFragment) {
        }

        @Override
        public void injectAbstract_AppBookingFragment(
            Abstract_AppBookingFragment abstract_AppBookingFragment) {
          injectAbstract_AppBookingFragment2(abstract_AppBookingFragment);
        }

        @Override
        public void injectAppBookingBrowseFragment(
            AppBookingBrowseFragment appBookingBrowseFragment) {
        }

        @Override
        public void injectAppBookingFilterFragment(
            AppBookingFilterFragment appBookingFilterFragment) {
        }

        @Override
        public void injectAppBookingTransferRekeningForm(
            AppBookingTransferRekeningForm appBookingTransferRekeningForm) {
        }

        @Override
        public void injectAppBookingTransferRekeningListFragment(
            AppBookingTransferRekeningListFragment appBookingTransferRekeningListFragment) {
          injectAppBookingTransferRekeningListFragment2(appBookingTransferRekeningListFragment);
        }

        @Override
        public void injectAppBookingMultiActionFragment(
            AppBookingMultiActionFragment appBookingMultiActionFragment) {
        }

        @Override
        public void injectDroppingAdditionalBrowseFragment(
            DroppingAdditionalBrowseFragment droppingAdditionalBrowseFragment) {
          injectDroppingAdditionalBrowseFragment2(droppingAdditionalBrowseFragment);
        }

        @Override
        public void injectDroppingAdditionalDetailEditFragment(
            DroppingAdditionalDetailEditFragment droppingAdditionalDetailEditFragment) {
        }

        @Override
        public void injectDroppingAdditionalEditFragment(
            DroppingAdditionalEditFragment droppingAdditionalEditFragment) {
          injectDroppingAdditionalEditFragment2(droppingAdditionalEditFragment);
        }

        @Override
        public void injectDroppingAdditionalFilterFragment(
            DroppingAdditionalFilterFragment droppingAdditionalFilterFragment) {
        }

        @Override
        public void injectDroppingDailyBrowseFragment(
            DroppingDailyBrowseFragment droppingDailyBrowseFragment) {
          injectDroppingDailyBrowseFragment2(droppingDailyBrowseFragment);
        }

        @Override
        public void injectDroppingDailyEditFragment(
            DroppingDailyEditFragment droppingDailyEditFragment) {
          injectDroppingDailyEditFragment2(droppingDailyEditFragment);
        }

        @Override
        public void injectDroppingDailyFilterFragment(
            DroppingDailyFilterFragment droppingDailyFilterFragment) {
        }

        @Override
        public void injectDroppingDailyTaskFilterFragment(
            DroppingDailyTaskFilterFragment droppingDailyTaskFilterFragment) {
        }

        @Override
        public void injectDroppingDailyTaskBrowseFragment(
            DroppingDailyTaskBrowseFragment droppingDailyTaskBrowseFragment) {
          injectDroppingDailyTaskBrowseFragment2(droppingDailyTaskBrowseFragment);
        }

        @Override
        public void injectDroppingDailyTaskWfFragment(
            DroppingDailyTaskWfFragment droppingDailyTaskWfFragment) {
          injectDroppingDailyTaskWfFragment2(droppingDailyTaskWfFragment);
        }

        @Override
        public void injectExpenseBrowseFragment(ExpenseBrowseFragment expenseBrowseFragment) {
        }

        @Override
        public void injectExpenseFilterFragment(ExpenseFilterFragment expenseFilterFragment) {
        }

        @Override
        public void injectExpenseSortFragment(ExpenseSortFragment expenseSortFragment) {
        }

        @Override
        public void injectExpenseEditFragment(ExpenseEditFragment expenseEditFragment) {
        }

        @Override
        public void injectSelectBookingFragment(SelectBookingFragment selectBookingFragment) {
        }

        @Override
        public void injectTugasTransferCekSaldoFragment(
            TugasTransferCekSaldoFragment tugasTransferCekSaldoFragment) {
        }

        @Override
        public void injectTugasTransferDetailFragment(
            TugasTransferDetailFragment tugasTransferDetailFragment) {
          injectTugasTransferDetailFragment2(tugasTransferDetailFragment);
        }

        @Override
        public void injectTugasTransferFilterFragment(
            TugasTransferFilterFragment tugasTransferFilterFragment) {
        }

        @Override
        public void injectTugasTransferBrowseHeaderFragment(
            TugasTransferBrowseHeaderFragment tugasTransferBrowseHeaderFragment) {
          injectTugasTransferBrowseHeaderFragment2(tugasTransferBrowseHeaderFragment);
        }

        @Override
        public void injectTugasTransferWfFragment(TugasTransferWfFragment tugasTransferWfFragment) {
          injectTugasTransferWfFragment2(tugasTransferWfFragment);
        }

        @Override
        public void injectOutletBalanceReportFilterFragment(
            OutletBalanceReportFilterFragment outletBalanceReportFilterFragment) {
        }

        @Override
        public void injectOutletBalanceReportFragment(
            OutletBalanceReportFragment outletBalanceReportFragment) {
        }

        @Override
        public void injectOutletStockByDayReportFilterFragment(
            OutletStockByDayReportFilterFragment outletStockByDayReportFilterFragment) {
        }

        @Override
        public void injectOutletStockByDayReportFragment(
            OutletStockByDayReportFragment outletStockByDayReportFragment) {
        }

        @Override
        public void injectChangePasswordFragment(ChangePasswordFragment changePasswordFragment) {
          injectChangePasswordFragment2(changePasswordFragment);
        }

        @Override
        public void injectPreferenceFragment(PreferenceFragment preferenceFragment) {
          injectPreferenceFragment2(preferenceFragment);
        }

        @Override
        public void injectSubstituteUserFragment(SubstituteUserFragment substituteUserFragment) {
        }

        @Override
        public void injectBankAcctFormFragment(BankAcctFormFragment bankAcctFormFragment) {
        }

        @Override
        public void injectWfHistoryFragment(WfHistoryFragment wfHistoryFragment) {
        }

        @CanIgnoreReturnValue
        private Abstract_AppBookingFragment injectAbstract_AppBookingFragment2(
            Abstract_AppBookingFragment instance) {
          Abstract_AppBookingFragment_MembersInjector.injectLoginUtil(instance, DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
          return instance;
        }

        @CanIgnoreReturnValue
        private AppBookingTransferRekeningListFragment injectAppBookingTransferRekeningListFragment2(
            AppBookingTransferRekeningListFragment instance) {
          AppBookingTransferRekeningListFragment_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
          return instance;
        }

        @CanIgnoreReturnValue
        private DroppingAdditionalBrowseFragment injectDroppingAdditionalBrowseFragment2(
            DroppingAdditionalBrowseFragment instance) {
          DroppingAdditionalBrowseFragment_MembersInjector.injectCommonService(instance, DaggerApp_HiltComponents_ApplicationC.this.getCommonService());
          DroppingAdditionalBrowseFragment_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
          return instance;
        }

        @CanIgnoreReturnValue
        private DroppingAdditionalEditFragment injectDroppingAdditionalEditFragment2(
            DroppingAdditionalEditFragment instance) {
          DroppingAdditionalEditFragment_MembersInjector.injectCommonService(instance, DaggerApp_HiltComponents_ApplicationC.this.getCommonService());
          DroppingAdditionalEditFragment_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
          return instance;
        }

        @CanIgnoreReturnValue
        private DroppingDailyBrowseFragment injectDroppingDailyBrowseFragment2(
            DroppingDailyBrowseFragment instance) {
          DroppingDailyBrowseFragment_MembersInjector.injectCommonService(instance, DaggerApp_HiltComponents_ApplicationC.this.getCommonService());
          DroppingDailyBrowseFragment_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
          return instance;
        }

        @CanIgnoreReturnValue
        private DroppingDailyEditFragment injectDroppingDailyEditFragment2(
            DroppingDailyEditFragment instance) {
          DroppingDailyEditFragment_MembersInjector.injectCommonService(instance, DaggerApp_HiltComponents_ApplicationC.this.getCommonService());
          DroppingDailyEditFragment_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
          return instance;
        }

        @CanIgnoreReturnValue
        private DroppingDailyTaskBrowseFragment injectDroppingDailyTaskBrowseFragment2(
            DroppingDailyTaskBrowseFragment instance) {
          DroppingDailyTaskBrowseFragment_MembersInjector.injectCommonService(instance, DaggerApp_HiltComponents_ApplicationC.this.getCommonService());
          DroppingDailyTaskBrowseFragment_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
          return instance;
        }

        @CanIgnoreReturnValue
        private DroppingDailyTaskWfFragment injectDroppingDailyTaskWfFragment2(
            DroppingDailyTaskWfFragment instance) {
          DroppingDailyTaskWfFragment_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
          return instance;
        }

        @CanIgnoreReturnValue
        private TugasTransferDetailFragment injectTugasTransferDetailFragment2(
            TugasTransferDetailFragment instance) {
          TugasTransferDetailFragment_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
          return instance;
        }

        @CanIgnoreReturnValue
        private TugasTransferBrowseHeaderFragment injectTugasTransferBrowseHeaderFragment2(
            TugasTransferBrowseHeaderFragment instance) {
          TugasTransferBrowseHeaderFragment_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
          return instance;
        }

        @CanIgnoreReturnValue
        private TugasTransferWfFragment injectTugasTransferWfFragment2(
            TugasTransferWfFragment instance) {
          TugasTransferWfFragment_MembersInjector.injectAppExecutors(instance, DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors());
          TugasTransferWfFragment_MembersInjector.injectLoginUtil(instance, DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
          return instance;
        }

        @CanIgnoreReturnValue
        private ChangePasswordFragment injectChangePasswordFragment2(
            ChangePasswordFragment instance) {
          ChangePasswordFragment_MembersInjector.injectLoginUtil(instance, DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
          return instance;
        }

        @CanIgnoreReturnValue
        private PreferenceFragment injectPreferenceFragment2(PreferenceFragment instance) {
          PreferenceFragment_MembersInjector.injectAndroconConfig(instance, DaggerApp_HiltComponents_ApplicationC.this.getAndroconConfig());
          PreferenceFragment_MembersInjector.injectLoginUtil(instance, DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
          PreferenceFragment_MembersInjector.injectUcTestPushNotification(instance, getTestPushNotificationUseCase());
          PreferenceFragment_MembersInjector.injectUcUploadLogFile(instance, getUploadLogFileUseCase());
          return instance;
        }

        private final class ViewWithFragmentCBuilder implements App_HiltComponents.ViewWithFragmentC.Builder {
          private View view;

          @Override
          public ViewWithFragmentCBuilder view(View view) {
            this.view = Preconditions.checkNotNull(view);
            return this;
          }

          @Override
          public App_HiltComponents.ViewWithFragmentC build() {
            Preconditions.checkBuilderRequirement(view, View.class);
            return new ViewWithFragmentCImpl(view);
          }
        }

        private final class ViewWithFragmentCImpl extends App_HiltComponents.ViewWithFragmentC {
          private ViewWithFragmentCImpl(View view) {

          }
        }
      }

      private final class ViewCBuilder implements App_HiltComponents.ViewC.Builder {
        private View view;

        @Override
        public ViewCBuilder view(View view) {
          this.view = Preconditions.checkNotNull(view);
          return this;
        }

        @Override
        public App_HiltComponents.ViewC build() {
          Preconditions.checkBuilderRequirement(view, View.class);
          return new ViewCImpl(view);
        }
      }

      private final class ViewCImpl extends App_HiltComponents.ViewC {
        private ViewCImpl(View view) {

        }
      }

      private final class SwitchingProvider<T> implements Provider<T> {
        private final int id;

        SwitchingProvider(int id) {
          this.id = id;
        }

        @SuppressWarnings("unchecked")
        private T get0() {
          switch (id) {
            case 0: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseVM_AssistedFactory 
            return (T) ActivityCImpl.this.getAppBookingBrowseVM_AssistedFactory();

            case 1: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseUC 
            return (T) ActivityCImpl.this.getAppBookingBrowseUC();

            case 2: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingListAction 
            return (T) ActivityCImpl.this.getAppBookingListAction();

            case 3: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash.AppBookingCashVM_AssistedFactory 
            return (T) ActivityCImpl.this.getAppBookingCashVM_AssistedFactory();

            case 4: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingPrepareUC 
            return (T) ActivityCImpl.this.getAppBookingPrepareUC();

            case 5: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingLoadUC 
            return (T) ActivityCImpl.this.getAppBookingLoadUC();

            case 6: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingProcessUC 
            return (T) ActivityCImpl.this.getAppBookingProcessUC();

            case 7: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterVM_AssistedFactory 
            return (T) ActivityCImpl.this.getAppBookingFilterVM_AssistedFactory();

            case 8: // id.co.danwinciptaniaga.npmdsandroid.booking.GetBookingFilterUC 
            return (T) ActivityCImpl.this.getGetBookingFilterUC();

            case 9: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionVM_AssistedFactory 
            return (T) ActivityCImpl.this.getAppBookingMultiActionVM_AssistedFactory();

            case 10: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionUC 
            return (T) ActivityCImpl.this.getAppBookingMultiActionUC();

            case 11: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningFormVM_AssistedFactory 
            return (T) ActivityCImpl.this.getAppBookingTransferRekeningFormVM_AssistedFactory();

            case 12: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferVM_AssistedFactory 
            return (T) ActivityCImpl.this.getAppBookingTransferVM_AssistedFactory();

            case 13: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.BankAccountBrowseVM_AssistedFactory 
            return (T) ActivityCImpl.this.getBankAccountBrowseVM_AssistedFactory();

            case 14: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.BankAccountBrowseUC 
            return (T) ActivityCImpl.this.getBankAccountBrowseUC();

            case 15: // id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.BankAcctFormVM_AssistedFactory 
            return (T) ActivityCImpl.this.getBankAcctFormVM_AssistedFactory();

            case 16: // id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.GetBanksUseCase 
            return (T) ActivityCImpl.this.getGetBanksUseCase();

            case 17: // id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getBookingBrowseViewModel_AssistedFactory();

            case 18: // id.co.danwinciptaniaga.npmdsandroid.booking.GetBookingListUseCase 
            return (T) ActivityCImpl.this.getGetBookingListUseCase();

            case 19: // id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailTransferVM_AssistedFactory 
            return (T) ActivityCImpl.this.getBookingDetailTransferVM_AssistedFactory();

            case 20: // id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingLoadUC 
            return (T) ActivityCImpl.this.getBookingLoadUC();

            case 21: // id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingProcessUC 
            return (T) ActivityCImpl.this.getBookingProcessUC();

            case 22: // id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailVM_AssistedFactory 
            return (T) ActivityCImpl.this.getBookingDetailVM_AssistedFactory();

            case 23: // id.co.danwinciptaniaga.npmdsandroid.booking.BookingFilterVM_AssistedFactory 
            return (T) ActivityCImpl.this.getBookingFilterVM_AssistedFactory();

            case 24: // id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordVM_AssistedFactory 
            return (T) ActivityCImpl.this.getChangePasswordVM_AssistedFactory();

            case 25: // id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordUC 
            return (T) ActivityCImpl.this.getChangePasswordUC();

            case 26: // id.co.danwinciptaniaga.npmdsandroid.ui.landing.DrawerViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getDrawerViewModel_AssistedFactory();

            case 27: // id.co.danwinciptaniaga.npmdsandroid.common.GetExtUserInfoUseCase 
            return (T) ActivityCImpl.this.getGetExtUserInfoUseCase();

            case 28: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingAdditionalBrowseViewModel_AssistedFactory();

            case 29: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.GetDroppingAdditionalListUseCase 
            return (T) ActivityCImpl.this.getGetDroppingAdditionalListUseCase();

            case 30: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingListActionUC 
            return (T) ActivityCImpl.this.getDroppingListActionUC();

            case 31: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingAdditionalDetailEditViewModel_AssistedFactory();

            case 32: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingAdditionalEditViewModel_AssistedFactory();

            case 33: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.PrepareDroppingAdditionalUseCase 
            return (T) ActivityCImpl.this.getPrepareDroppingAdditionalUseCase();

            case 34: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalSaveUseCase 
            return (T) ActivityCImpl.this.getDroppingAdditionalSaveUseCase();

            case 35: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.LoadDroppingAdditionalUseCase 
            return (T) ActivityCImpl.this.getLoadDroppingAdditionalUseCase();

            case 36: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalSupervisorDecideUseCase 
            return (T) ActivityCImpl.this.getDroppingAdditionalSupervisorDecideUseCase();

            case 37: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.DroppingAdditionalFilterVM_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingAdditionalFilterVM_AssistedFactory();

            case 38: // id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.GetDroppingAdditionalFilterUC 
            return (T) ActivityCImpl.this.getGetDroppingAdditionalFilterUC();

            case 39: // id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseVM_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingDailyBrowseVM_AssistedFactory();

            case 40: // id.co.danwinciptaniaga.npmdsandroid.droppingday.GetDroppingDailyListUC 
            return (T) ActivityCImpl.this.getGetDroppingDailyListUC();

            case 41: // id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingDailyEditViewModel_AssistedFactory();

            case 42: // id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.LoadDroppingDailyUseCase 
            return (T) ActivityCImpl.this.getLoadDroppingDailyUseCase();

            case 43: // id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailySupervisorDecideUseCase 
            return (T) ActivityCImpl.this.getDroppingDailySupervisorDecideUseCase();

            case 44: // id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.DroppingDailyFilterVM_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingDailyFilterVM_AssistedFactory();

            case 45: // id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.GetDroppingDailyFilterUC 
            return (T) ActivityCImpl.this.getGetDroppingDailyFilterUC();

            case 46: // id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseVM_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingDailyTaskBrowseVM_AssistedFactory();

            case 47: // id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.GetDroppingDailyTaskBrowseHeaderUC 
            return (T) ActivityCImpl.this.getGetDroppingDailyTaskBrowseHeaderUC();

            case 48: // id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.GetDroppingDailyTaskFilterUC 
            return (T) ActivityCImpl.this.getGetDroppingDailyTaskFilterUC();

            case 49: // id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail.DroppingDailyTaskDetailBrowseVM_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingDailyTaskDetailBrowseVM_AssistedFactory();

            case 50: // id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail.GetDroppingDailyTaskBrowseDetailUC 
            return (T) ActivityCImpl.this.getGetDroppingDailyTaskBrowseDetailUC();

            case 51: // id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterVM_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingDailyTaskFilterVM_AssistedFactory();

            case 52: // id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfVM_AssistedFactory 
            return (T) ActivityCImpl.this.getDroppingDailyTaskWfVM_AssistedFactory();

            case 53: // id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskProcessUC 
            return (T) ActivityCImpl.this.getDroppingDailyTaskProcessUC();

            case 54: // id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getExpenseBrowseViewModel_AssistedFactory();

            case 55: // id.co.danwinciptaniaga.npmdsandroid.exp.GetExpenseListUseCase 
            return (T) ActivityCImpl.this.getGetExpenseListUseCase();

            case 56: // id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListAction 
            return (T) ActivityCImpl.this.getExpenseListAction();

            case 57: // id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseEditViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getExpenseEditViewModel_AssistedFactory();

            case 58: // id.co.danwinciptaniaga.npmdsandroid.exp.edit.PrepareNewExpenseUseCase 
            return (T) ActivityCImpl.this.getPrepareNewExpenseUseCase();

            case 59: // id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseSaveDraftUseCase 
            return (T) ActivityCImpl.this.getExpenseSaveDraftUseCase();

            case 60: // id.co.danwinciptaniaga.npmdsandroid.exp.edit.LoadExpenseUseCase 
            return (T) ActivityCImpl.this.getLoadExpenseUseCase();

            case 61: // id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseAocDecideUseCase 
            return (T) ActivityCImpl.this.getExpenseAocDecideUseCase();

            case 62: // id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterVM_AssistedFactory 
            return (T) ActivityCImpl.this.getExpenseFilterVM_AssistedFactory();

            case 63: // id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterUC 
            return (T) ActivityCImpl.this.getExpenseFilterUC();

            case 64: // id.co.danwinciptaniaga.npmdsandroid.ui.login.LoginViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getLoginViewModel_AssistedFactory();

            case 65: // id.co.danwinciptaniaga.androcon.auth.GetTokenUseCase 
            return (T) ActivityCImpl.this.getGetTokenUseCase();

            case 66: // id.co.danwinciptaniaga.androcon.registration.RegisterDeviceUseCaseSync 
            return (T) ActivityCImpl.this.getRegisterDeviceUseCaseSync();

            case 67: // id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceBrowseVM_AssistedFactory 
            return (T) ActivityCImpl.this.getOutletBalanceBrowseVM_AssistedFactory();

            case 68: // id.co.danwinciptaniaga.npmdsandroid.outlet.GetOutletBalanceListUseCase 
            return (T) ActivityCImpl.this.getGetOutletBalanceListUseCase();

            case 69: // id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportVM_AssistedFactory 
            return (T) ActivityCImpl.this.getOutletBalanceReportVM_AssistedFactory();

            case 70: // id.co.danwinciptaniaga.npmdsandroid.outlet.GetOutletBalanceReportUseCase 
            return (T) ActivityCImpl.this.getGetOutletBalanceReportUseCase();

            case 71: // id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockReportByDayVM_AssistedFactory 
            return (T) ActivityCImpl.this.getOutletStockReportByDayVM_AssistedFactory();

            case 72: // id.co.danwinciptaniaga.npmdsandroid.outlet.GetOutletStockByDayReportUC 
            return (T) ActivityCImpl.this.getGetOutletStockByDayReportUC();

            case 73: // id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getPreferenceViewModel_AssistedFactory();

            case 74: // id.co.danwinciptaniaga.androcon.utility.GetLatestVersionUseCase 
            return (T) ActivityCImpl.this.getGetLatestVersionUseCase();

            case 75: // id.co.danwinciptaniaga.androcon.update.DownloadUpdateUseCase 
            return (T) ActivityCImpl.this.getDownloadUpdateUseCase();

            case 76: // id.co.danwinciptaniaga.androcon.security.ProtectedActivityViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getProtectedActivityViewModel_AssistedFactory();

            case 77: // id.co.danwinciptaniaga.androcon.auth.GetUserPermissionUseCase 
            return (T) ActivityCImpl.this.getGetUserPermissionUseCase();

            case 78: // id.co.danwinciptaniaga.androcon.auth.AuthorizationHeaderProvider 
            return (T) ActivityCImpl.this.getAuthorizationHeaderProvider();

            case 79: // id.co.danwinciptaniaga.androcon.registration.RegisterDeviceViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getRegisterDeviceViewModel_AssistedFactory();

            case 80: // id.co.danwinciptaniaga.androcon.registration.RegisterDeviceUseCase 
            return (T) ActivityCImpl.this.getRegisterDeviceUseCase();

            case 81: // id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.SelectBookingViewModel_AssistedFactory 
            return (T) ActivityCImpl.this.getSelectBookingViewModel_AssistedFactory();

            case 82: // id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.GetBookingMediatorListUseCase 
            return (T) ActivityCImpl.this.getGetBookingMediatorListUseCase();

            case 83: // id.co.danwinciptaniaga.npmdsandroid.security.SubstituteUserVM_AssistedFactory 
            return (T) ActivityCImpl.this.getSubstituteUserVM_AssistedFactory();

            case 84: // id.co.danwinciptaniaga.npmdsandroid.common.GetSubstituteUsersUC 
            return (T) ActivityCImpl.this.getGetSubstituteUsersUC();

            case 85: // id.co.danwinciptaniaga.npmdsandroid.common.SubstituteUserUC 
            return (T) ActivityCImpl.this.getSubstituteUserUC();

            case 86: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoVM_AssistedFactory 
            return (T) ActivityCImpl.this.getTugasTransferCekSaldoVM_AssistedFactory();

            case 87: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoUC 
            return (T) ActivityCImpl.this.getTugasTransferCekSaldoUC();

            case 88: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoBankListUC 
            return (T) ActivityCImpl.this.getTugasTransferCekSaldoBankListUC();

            case 89: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoAccountListUC 
            return (T) ActivityCImpl.this.getTugasTransferCekSaldoAccountListUC();

            case 90: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailBrowseVM_AssistedFactory 
            return (T) ActivityCImpl.this.getTugasTransferDetailBrowseVM_AssistedFactory();

            case 91: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.GetTugasTransferDetailBrowseUC 
            return (T) ActivityCImpl.this.getGetTugasTransferDetailBrowseUC();

            case 92: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterVM_AssistedFactory 
            return (T) ActivityCImpl.this.getTugasTransferFilterVM_AssistedFactory();

            case 93: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.GetTugasTransferFilterUC 
            return (T) ActivityCImpl.this.getGetTugasTransferFilterUC();

            case 94: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferHeaderBrowseVM_AssistedFactory 
            return (T) ActivityCImpl.this.getTugasTransferHeaderBrowseVM_AssistedFactory();

            case 95: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.GetTugasTransferBrowseUC 
            return (T) ActivityCImpl.this.getGetTugasTransferBrowseUC();

            case 96: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfVM_AssistedFactory 
            return (T) ActivityCImpl.this.getTugasTransferWfVM_AssistedFactory();

            case 97: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfFormUC 
            return (T) ActivityCImpl.this.getTugasTransferWfFormUC();

            case 98: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfProcessUC 
            return (T) ActivityCImpl.this.getTugasTransferWfProcessUC();

            case 99: // id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWFGenerateOtpUC 
            return (T) ActivityCImpl.this.getTugasTransferWFGenerateOtpUC();

            default: throw new AssertionError(id);
          }
        }

        @SuppressWarnings("unchecked")
        private T get1() {
          switch (id) {
            case 100: // id.co.danwinciptaniaga.npmdsandroid.wf.WfHistoryVM_AssistedFactory 
            return (T) ActivityCImpl.this.getWfHistoryVM_AssistedFactory();

            case 101: // id.co.danwinciptaniaga.npmdsandroid.wf.GetWfHistoryUseCase 
            return (T) ActivityCImpl.this.getGetWfHistoryUseCase();

            default: throw new AssertionError(id);
          }
        }

        @Override
        public T get() {
          switch (id / 100) {
            case 0: return get0();
            case 1: return get1();
            default: throw new AssertionError(id);
          }
        }
      }
    }
  }

  private final class ServiceCBuilder implements App_HiltComponents.ServiceC.Builder {
    private Service service;

    @Override
    public ServiceCBuilder service(Service service) {
      this.service = Preconditions.checkNotNull(service);
      return this;
    }

    @Override
    public App_HiltComponents.ServiceC build() {
      Preconditions.checkBuilderRequirement(service, Service.class);
      return new ServiceCImpl(service);
    }
  }

  private final class ServiceCImpl extends App_HiltComponents.ServiceC {
    private ServiceCImpl(Service service) {

    }

    private AuthorizationHeaderProvider getAuthorizationHeaderProvider() {
      return new AuthorizationHeaderProvider(DaggerApp_HiltComponents_ApplicationC.this.getAndroconConfig(), DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
    }

    private GetTokenUseCaseSync getGetTokenUseCaseSync() {
      return new GetTokenUseCaseSync(getAuthorizationHeaderProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAuthService());
    }

    private RegisterDeviceUseCaseSync getRegisterDeviceUseCaseSync() {
      return new RegisterDeviceUseCaseSync(getAuthorizationHeaderProvider(), DaggerApp_HiltComponents_ApplicationC.this.getAndroconConfig(), DaggerApp_HiltComponents_ApplicationC.this.getDeviceRegistrationService(), DaggerApp_HiltComponents_ApplicationC.this.getRegistrationUtil());
    }

    @Override
    public void injectNpmdsAuthenticatorService(
        NpmdsAuthenticatorService npmdsAuthenticatorService) {
      injectNpmdsAuthenticatorService2(npmdsAuthenticatorService);
    }

    @Override
    public void injectNpmdsNotificationService(NpmdsNotificationService npmdsNotificationService) {
      injectNpmdsNotificationService2(npmdsNotificationService);
    }

    @CanIgnoreReturnValue
    private NpmdsAuthenticatorService injectNpmdsAuthenticatorService2(
        NpmdsAuthenticatorService instance) {
      NpmdsAuthenticatorService_MembersInjector.injectUcGetTokenSync(instance, getGetTokenUseCaseSync());
      NpmdsAuthenticatorService_MembersInjector.injectLoginUtil(instance, DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil());
      return instance;
    }

    @CanIgnoreReturnValue
    private NpmdsNotificationService injectNpmdsNotificationService2(
        NpmdsNotificationService instance) {
      NpmdsNotificationService_MembersInjector.injectUcRegisterDeviceSync(instance, getRegisterDeviceUseCaseSync());
      return instance;
    }
  }

  private final class SwitchingProvider<T> implements Provider<T> {
    private final int id;

    SwitchingProvider(int id) {
      this.id = id;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T get() {
      switch (id) {
        case 0: // android.app.Application 
        return (T) ApplicationContextModule_ProvideApplicationFactory.provideApplication(DaggerApp_HiltComponents_ApplicationC.this.applicationContextModule);

        case 1: // id.co.danwinciptaniaga.androcon.utility.AppExecutors 
        return (T) DaggerApp_HiltComponents_ApplicationC.this.getAppExecutors();

        case 2: // id.co.danwinciptaniaga.androcon.utility.UtilityService 
        return (T) DaggerApp_HiltComponents_ApplicationC.this.getUtilityService();

        case 3: // id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService 
        return (T) DaggerApp_HiltComponents_ApplicationC.this.getAppBookingService();

        case 4: // id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService 
        return (T) DaggerApp_HiltComponents_ApplicationC.this.getExpenseService();

        case 5: // id.co.danwinciptaniaga.androcon.auth.LoginUtil 
        return (T) DaggerApp_HiltComponents_ApplicationC.this.getLoginUtil();

        case 6: // id.co.danwinciptaniaga.androcon.AndroconConfig 
        return (T) DaggerApp_HiltComponents_ApplicationC.this.getAndroconConfig();

        case 7: // id.co.danwinciptaniaga.androcon.auth.AuthService 
        return (T) DaggerApp_HiltComponents_ApplicationC.this.getAuthService();

        default: throw new AssertionError(id);
      }
    }
  }
}
