package id.co.danwinciptaniaga.npmdsandroid;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class App_MembersInjector implements MembersInjector<App> {
  private final Provider<AndroconConfig> androconConfigProvider;

  private final Provider<AppExecutors> mAppExecutorsProvider;

  private final Provider<LoginUtil> loginUtilProvider;

  public App_MembersInjector(Provider<AndroconConfig> androconConfigProvider,
      Provider<AppExecutors> mAppExecutorsProvider, Provider<LoginUtil> loginUtilProvider) {
    this.androconConfigProvider = androconConfigProvider;
    this.mAppExecutorsProvider = mAppExecutorsProvider;
    this.loginUtilProvider = loginUtilProvider;
  }

  public static MembersInjector<App> create(Provider<AndroconConfig> androconConfigProvider,
      Provider<AppExecutors> mAppExecutorsProvider, Provider<LoginUtil> loginUtilProvider) {
    return new App_MembersInjector(androconConfigProvider, mAppExecutorsProvider, loginUtilProvider);
  }

  @Override
  public void injectMembers(App instance) {
    injectAndroconConfig(instance, androconConfigProvider.get());
    injectMAppExecutors(instance, mAppExecutorsProvider.get());
    injectLoginUtil(instance, loginUtilProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.App.androconConfig")
  public static void injectAndroconConfig(App instance, AndroconConfig androconConfig) {
    instance.androconConfig = androconConfig;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.App.mAppExecutors")
  public static void injectMAppExecutors(App instance, AppExecutors mAppExecutors) {
    instance.mAppExecutors = mAppExecutors;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.App.loginUtil")
  public static void injectLoginUtil(App instance, LoginUtil loginUtil) {
    instance.loginUtil = loginUtil;
  }
}
