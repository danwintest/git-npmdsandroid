package id.co.danwinciptaniaga.npmdsandroid.notif;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceUseCaseSync;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NpmdsNotificationService_MembersInjector implements MembersInjector<NpmdsNotificationService> {
  private final Provider<RegisterDeviceUseCaseSync> ucRegisterDeviceSyncProvider;

  public NpmdsNotificationService_MembersInjector(
      Provider<RegisterDeviceUseCaseSync> ucRegisterDeviceSyncProvider) {
    this.ucRegisterDeviceSyncProvider = ucRegisterDeviceSyncProvider;
  }

  public static MembersInjector<NpmdsNotificationService> create(
      Provider<RegisterDeviceUseCaseSync> ucRegisterDeviceSyncProvider) {
    return new NpmdsNotificationService_MembersInjector(ucRegisterDeviceSyncProvider);
  }

  @Override
  public void injectMembers(NpmdsNotificationService instance) {
    injectUcRegisterDeviceSync(instance, ucRegisterDeviceSyncProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.notif.NpmdsNotificationService.ucRegisterDeviceSync")
  public static void injectUcRegisterDeviceSync(NpmdsNotificationService instance,
      RegisterDeviceUseCaseSync ucRegisterDeviceSync) {
    instance.ucRegisterDeviceSync = ucRegisterDeviceSync;
  }
}
