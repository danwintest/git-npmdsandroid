package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TugasTransferCekSaldoVM_AssistedFactory_Factory implements Factory<TugasTransferCekSaldoVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<TugasTransferCekSaldoUC> ucProvider;

  private final Provider<TugasTransferCekSaldoBankListUC> bankListUCProvider;

  private final Provider<TugasTransferCekSaldoAccountListUC> accountListUCProvider;

  public TugasTransferCekSaldoVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<TugasTransferCekSaldoUC> ucProvider,
      Provider<TugasTransferCekSaldoBankListUC> bankListUCProvider,
      Provider<TugasTransferCekSaldoAccountListUC> accountListUCProvider) {
    this.applicationProvider = applicationProvider;
    this.ucProvider = ucProvider;
    this.bankListUCProvider = bankListUCProvider;
    this.accountListUCProvider = accountListUCProvider;
  }

  @Override
  public TugasTransferCekSaldoVM_AssistedFactory get() {
    return newInstance(applicationProvider, ucProvider, bankListUCProvider, accountListUCProvider);
  }

  public static TugasTransferCekSaldoVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<TugasTransferCekSaldoUC> ucProvider,
      Provider<TugasTransferCekSaldoBankListUC> bankListUCProvider,
      Provider<TugasTransferCekSaldoAccountListUC> accountListUCProvider) {
    return new TugasTransferCekSaldoVM_AssistedFactory_Factory(applicationProvider, ucProvider, bankListUCProvider, accountListUCProvider);
  }

  public static TugasTransferCekSaldoVM_AssistedFactory newInstance(
      Provider<Application> application, Provider<TugasTransferCekSaldoUC> uc,
      Provider<TugasTransferCekSaldoBankListUC> bankListUC,
      Provider<TugasTransferCekSaldoAccountListUC> accountListUC) {
    return new TugasTransferCekSaldoVM_AssistedFactory(application, uc, bankListUC, accountListUC);
  }
}
