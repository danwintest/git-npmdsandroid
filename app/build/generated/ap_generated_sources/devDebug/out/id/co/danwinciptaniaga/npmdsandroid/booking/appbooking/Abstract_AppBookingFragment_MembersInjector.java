package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class Abstract_AppBookingFragment_MembersInjector implements MembersInjector<Abstract_AppBookingFragment> {
  private final Provider<LoginUtil> loginUtilProvider;

  public Abstract_AppBookingFragment_MembersInjector(Provider<LoginUtil> loginUtilProvider) {
    this.loginUtilProvider = loginUtilProvider;
  }

  public static MembersInjector<Abstract_AppBookingFragment> create(
      Provider<LoginUtil> loginUtilProvider) {
    return new Abstract_AppBookingFragment_MembersInjector(loginUtilProvider);
  }

  @Override
  public void injectMembers(Abstract_AppBookingFragment instance) {
    injectLoginUtil(instance, loginUtilProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingFragment.loginUtil")
  public static void injectLoginUtil(Abstract_AppBookingFragment instance, LoginUtil loginUtil) {
    instance.loginUtil = loginUtil;
  }
}
