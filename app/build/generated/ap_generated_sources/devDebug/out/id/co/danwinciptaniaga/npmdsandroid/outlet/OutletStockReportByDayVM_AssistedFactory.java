package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class OutletStockReportByDayVM_AssistedFactory implements ViewModelAssistedFactory<OutletStockReportByDayVM> {
  private final Provider<Application> application;

  private final Provider<GetOutletStockByDayReportUC> getOutletStockByDayReportUC;

  @Inject
  OutletStockReportByDayVM_AssistedFactory(Provider<Application> application,
      Provider<GetOutletStockByDayReportUC> getOutletStockByDayReportUC) {
    this.application = application;
    this.getOutletStockByDayReportUC = getOutletStockByDayReportUC;
  }

  @Override
  @NonNull
  public OutletStockReportByDayVM create(@NonNull SavedStateHandle arg0) {
    return new OutletStockReportByDayVM(application.get(), getOutletStockByDayReportUC.get());
  }
}
