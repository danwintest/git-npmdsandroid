package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingDailyTaskFilterVM_AssistedFactory implements ViewModelAssistedFactory<DroppingDailyTaskFilterVM> {
  private final Provider<Application> app;

  private final Provider<GetDroppingDailyTaskFilterUC> uc;

  @Inject
  DroppingDailyTaskFilterVM_AssistedFactory(Provider<Application> app,
      Provider<GetDroppingDailyTaskFilterUC> uc) {
    this.app = app;
    this.uc = uc;
  }

  @Override
  @NonNull
  public DroppingDailyTaskFilterVM create(@NonNull SavedStateHandle arg0) {
    return new DroppingDailyTaskFilterVM(app.get(), uc.get());
  }
}
