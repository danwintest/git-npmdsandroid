package id.co.danwinciptaniaga.npmdsandroid.wf;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = WfHistoryFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface WfHistoryFragment_GeneratedInjector {
  void injectWfHistoryFragment(WfHistoryFragment wfHistoryFragment);
}
