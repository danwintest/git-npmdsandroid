package id.co.danwinciptaniaga.npmdsandroid.droppingday;

import android.content.Context;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyListAdapter_Factory implements Factory<DroppingDailyListAdapter> {
  private final Provider<DroppingDailyListAdapter.DddRecyclerViewAdapterListener> listenerProvider;

  private final Provider<Context> ctxProvider;

  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailyListAdapter_Factory(
      Provider<DroppingDailyListAdapter.DddRecyclerViewAdapterListener> listenerProvider,
      Provider<Context> ctxProvider, Provider<CommonService> commonServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.listenerProvider = listenerProvider;
    this.ctxProvider = ctxProvider;
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingDailyListAdapter get() {
    return newInstance(listenerProvider.get(), ctxProvider.get(), commonServiceProvider.get(), appExecutorsProvider.get());
  }

  public static DroppingDailyListAdapter_Factory create(
      Provider<DroppingDailyListAdapter.DddRecyclerViewAdapterListener> listenerProvider,
      Provider<Context> ctxProvider, Provider<CommonService> commonServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailyListAdapter_Factory(listenerProvider, ctxProvider, commonServiceProvider, appExecutorsProvider);
  }

  public static DroppingDailyListAdapter newInstance(Object listener, Context ctx,
      CommonService commonService, AppExecutors appExecutors) {
    return new DroppingDailyListAdapter((DroppingDailyListAdapter.DddRecyclerViewAdapterListener) listener, ctx, commonService, appExecutors);
  }
}
