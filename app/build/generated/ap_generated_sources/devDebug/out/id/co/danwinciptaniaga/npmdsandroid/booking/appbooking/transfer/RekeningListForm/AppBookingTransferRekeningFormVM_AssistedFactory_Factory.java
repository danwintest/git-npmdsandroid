package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingTransferRekeningFormVM_AssistedFactory_Factory implements Factory<AppBookingTransferRekeningFormVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<AppBookingService> appBookingServiceProvider;

  public AppBookingTransferRekeningFormVM_AssistedFactory_Factory(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<AppBookingService> appBookingServiceProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.appBookingServiceProvider = appBookingServiceProvider;
  }

  @Override
  public AppBookingTransferRekeningFormVM_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider, appBookingServiceProvider);
  }

  public static AppBookingTransferRekeningFormVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<AppBookingService> appBookingServiceProvider) {
    return new AppBookingTransferRekeningFormVM_AssistedFactory_Factory(applicationProvider, appExecutorsProvider, appBookingServiceProvider);
  }

  public static AppBookingTransferRekeningFormVM_AssistedFactory newInstance(
      Provider<Application> application, Provider<AppExecutors> appExecutors,
      Provider<AppBookingService> appBookingService) {
    return new AppBookingTransferRekeningFormVM_AssistedFactory(application, appExecutors, appBookingService);
  }
}
