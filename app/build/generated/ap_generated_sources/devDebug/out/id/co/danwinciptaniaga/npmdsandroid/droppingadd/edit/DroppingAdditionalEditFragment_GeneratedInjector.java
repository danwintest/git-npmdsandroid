package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = DroppingAdditionalEditFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface DroppingAdditionalEditFragment_GeneratedInjector {
  void injectDroppingAdditionalEditFragment(
      DroppingAdditionalEditFragment droppingAdditionalEditFragment);
}
