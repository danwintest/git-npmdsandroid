package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingAdditionalEditViewModel_AssistedFactory implements ViewModelAssistedFactory<DroppingAdditionalEditViewModel> {
  private final Provider<Application> application;

  private final Provider<PrepareDroppingAdditionalUseCase> prepareDroppingAdditionalUseCase;

  private final Provider<DroppingAdditionalSaveUseCase> droppingAdditionalSaveUseCase;

  private final Provider<LoadDroppingAdditionalUseCase> loadDroppingAdditionalUseCase;

  private final Provider<DroppingAdditionalSupervisorDecideUseCase> droppingAdditionalSupervisorDecideUseCase;

  private final Provider<UtilityService> utilityService;

  private final Provider<AppExecutors> appExecutors;

  @Inject
  DroppingAdditionalEditViewModel_AssistedFactory(Provider<Application> application,
      Provider<PrepareDroppingAdditionalUseCase> prepareDroppingAdditionalUseCase,
      Provider<DroppingAdditionalSaveUseCase> droppingAdditionalSaveUseCase,
      Provider<LoadDroppingAdditionalUseCase> loadDroppingAdditionalUseCase,
      Provider<DroppingAdditionalSupervisorDecideUseCase> droppingAdditionalSupervisorDecideUseCase,
      Provider<UtilityService> utilityService, Provider<AppExecutors> appExecutors) {
    this.application = application;
    this.prepareDroppingAdditionalUseCase = prepareDroppingAdditionalUseCase;
    this.droppingAdditionalSaveUseCase = droppingAdditionalSaveUseCase;
    this.loadDroppingAdditionalUseCase = loadDroppingAdditionalUseCase;
    this.droppingAdditionalSupervisorDecideUseCase = droppingAdditionalSupervisorDecideUseCase;
    this.utilityService = utilityService;
    this.appExecutors = appExecutors;
  }

  @Override
  @NonNull
  public DroppingAdditionalEditViewModel create(@NonNull SavedStateHandle arg0) {
    return new DroppingAdditionalEditViewModel(application.get(),
        prepareDroppingAdditionalUseCase.get(), droppingAdditionalSaveUseCase.get(),
        loadDroppingAdditionalUseCase.get(), droppingAdditionalSupervisorDecideUseCase.get(),
        utilityService.get(), appExecutors.get());
  }
}
