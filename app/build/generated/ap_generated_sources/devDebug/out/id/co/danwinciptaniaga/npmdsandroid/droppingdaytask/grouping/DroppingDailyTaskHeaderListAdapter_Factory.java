package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import android.content.Context;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskHeaderListAdapter_Factory implements Factory<DroppingDailyTaskHeaderListAdapter> {
  private final Provider<DroppingDailyTaskHeaderListAdapter.DddRecyclerViewAdapterListener> listenerProvider;

  private final Provider<Context> ctxProvider;

  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailyTaskHeaderListAdapter_Factory(
      Provider<DroppingDailyTaskHeaderListAdapter.DddRecyclerViewAdapterListener> listenerProvider,
      Provider<Context> ctxProvider, Provider<CommonService> commonServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.listenerProvider = listenerProvider;
    this.ctxProvider = ctxProvider;
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingDailyTaskHeaderListAdapter get() {
    return newInstance(listenerProvider.get(), ctxProvider.get(), commonServiceProvider.get(), appExecutorsProvider.get());
  }

  public static DroppingDailyTaskHeaderListAdapter_Factory create(
      Provider<DroppingDailyTaskHeaderListAdapter.DddRecyclerViewAdapterListener> listenerProvider,
      Provider<Context> ctxProvider, Provider<CommonService> commonServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailyTaskHeaderListAdapter_Factory(listenerProvider, ctxProvider, commonServiceProvider, appExecutorsProvider);
  }

  public static DroppingDailyTaskHeaderListAdapter newInstance(Object listener, Context ctx,
      CommonService commonService, AppExecutors appExecutors) {
    return new DroppingDailyTaskHeaderListAdapter((DroppingDailyTaskHeaderListAdapter.DddRecyclerViewAdapterListener) listener, ctx, commonService, appExecutors);
  }
}
