package id.co.danwinciptaniaga.npmdsandroid.exp;

import android.content.Context;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ExpenseListAdapter_Factory implements Factory<ExpenseListAdapter> {
  private final Provider<ExpenseListAdapter.MyExpenseDataRecyclerViewAdapterListener> listenerProvider;

  private final Provider<Context> ctxProvider;

  public ExpenseListAdapter_Factory(
      Provider<ExpenseListAdapter.MyExpenseDataRecyclerViewAdapterListener> listenerProvider,
      Provider<Context> ctxProvider) {
    this.listenerProvider = listenerProvider;
    this.ctxProvider = ctxProvider;
  }

  @Override
  public ExpenseListAdapter get() {
    return newInstance(listenerProvider.get(), ctxProvider.get());
  }

  public static ExpenseListAdapter_Factory create(
      Provider<ExpenseListAdapter.MyExpenseDataRecyclerViewAdapterListener> listenerProvider,
      Provider<Context> ctxProvider) {
    return new ExpenseListAdapter_Factory(listenerProvider, ctxProvider);
  }

  public static ExpenseListAdapter newInstance(Object listener, Context ctx) {
    return new ExpenseListAdapter((ExpenseListAdapter.MyExpenseDataRecyclerViewAdapterListener) listener, ctx);
  }
}
