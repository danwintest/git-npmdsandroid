package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import dagger.internal.Factory;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.DroppingDailyTaskService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetDroppingDailyTaskBrowseHeaderUC_Factory implements Factory<GetDroppingDailyTaskBrowseHeaderUC> {
  private final Provider<DroppingDailyTaskService> serviceProvider;

  public GetDroppingDailyTaskBrowseHeaderUC_Factory(
      Provider<DroppingDailyTaskService> serviceProvider) {
    this.serviceProvider = serviceProvider;
  }

  @Override
  public GetDroppingDailyTaskBrowseHeaderUC get() {
    return newInstance(serviceProvider.get());
  }

  public static GetDroppingDailyTaskBrowseHeaderUC_Factory create(
      Provider<DroppingDailyTaskService> serviceProvider) {
    return new GetDroppingDailyTaskBrowseHeaderUC_Factory(serviceProvider);
  }

  public static GetDroppingDailyTaskBrowseHeaderUC newInstance(DroppingDailyTaskService service) {
    return new GetDroppingDailyTaskBrowseHeaderUC(service);
  }
}
