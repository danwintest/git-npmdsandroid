package id.co.danwinciptaniaga.npmdsandroid.wf;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class WfHistoryVM_AssistedFactory implements ViewModelAssistedFactory<WfHistoryVM> {
  private final Provider<Application> application;

  private final Provider<GetWfHistoryUseCase> getWfHistoryUseCase;

  @Inject
  WfHistoryVM_AssistedFactory(Provider<Application> application,
      Provider<GetWfHistoryUseCase> getWfHistoryUseCase) {
    this.application = application;
    this.getWfHistoryUseCase = getWfHistoryUseCase;
  }

  @Override
  @NonNull
  public WfHistoryVM create(@NonNull SavedStateHandle arg0) {
    return new WfHistoryVM(application.get(), getWfHistoryUseCase.get());
  }
}
