package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskFilterVM_AssistedFactory_Factory implements Factory<DroppingDailyTaskFilterVM_AssistedFactory> {
  private final Provider<Application> appProvider;

  private final Provider<GetDroppingDailyTaskFilterUC> ucProvider;

  public DroppingDailyTaskFilterVM_AssistedFactory_Factory(Provider<Application> appProvider,
      Provider<GetDroppingDailyTaskFilterUC> ucProvider) {
    this.appProvider = appProvider;
    this.ucProvider = ucProvider;
  }

  @Override
  public DroppingDailyTaskFilterVM_AssistedFactory get() {
    return newInstance(appProvider, ucProvider);
  }

  public static DroppingDailyTaskFilterVM_AssistedFactory_Factory create(
      Provider<Application> appProvider, Provider<GetDroppingDailyTaskFilterUC> ucProvider) {
    return new DroppingDailyTaskFilterVM_AssistedFactory_Factory(appProvider, ucProvider);
  }

  public static DroppingDailyTaskFilterVM_AssistedFactory newInstance(Provider<Application> app,
      Provider<GetDroppingDailyTaskFilterUC> uc) {
    return new DroppingDailyTaskFilterVM_AssistedFactory(app, uc);
  }
}
