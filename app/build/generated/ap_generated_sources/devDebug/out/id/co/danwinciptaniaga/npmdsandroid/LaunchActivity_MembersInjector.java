package id.co.danwinciptaniaga.npmdsandroid;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCaseSync;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.registration.RegistrationUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LaunchActivity_MembersInjector implements MembersInjector<LaunchActivity> {
  private final Provider<AndroconConfig> androconConfigProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<LoginUtil> loginUtilProvider;

  private final Provider<RegistrationUtil> registrationUtilProvider;

  private final Provider<GetTokenUseCaseSync> getTokenUseCaseSyncProvider;

  public LaunchActivity_MembersInjector(Provider<AndroconConfig> androconConfigProvider,
      Provider<AppExecutors> appExecutorsProvider, Provider<LoginUtil> loginUtilProvider,
      Provider<RegistrationUtil> registrationUtilProvider,
      Provider<GetTokenUseCaseSync> getTokenUseCaseSyncProvider) {
    this.androconConfigProvider = androconConfigProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.loginUtilProvider = loginUtilProvider;
    this.registrationUtilProvider = registrationUtilProvider;
    this.getTokenUseCaseSyncProvider = getTokenUseCaseSyncProvider;
  }

  public static MembersInjector<LaunchActivity> create(
      Provider<AndroconConfig> androconConfigProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<LoginUtil> loginUtilProvider, Provider<RegistrationUtil> registrationUtilProvider,
      Provider<GetTokenUseCaseSync> getTokenUseCaseSyncProvider) {
    return new LaunchActivity_MembersInjector(androconConfigProvider, appExecutorsProvider, loginUtilProvider, registrationUtilProvider, getTokenUseCaseSyncProvider);
  }

  @Override
  public void injectMembers(LaunchActivity instance) {
    injectAndroconConfig(instance, androconConfigProvider.get());
    injectAppExecutors(instance, appExecutorsProvider.get());
    injectLoginUtil(instance, loginUtilProvider.get());
    injectRegistrationUtil(instance, registrationUtilProvider.get());
    injectGetTokenUseCaseSync(instance, getTokenUseCaseSyncProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.LaunchActivity.androconConfig")
  public static void injectAndroconConfig(LaunchActivity instance, AndroconConfig androconConfig) {
    instance.androconConfig = androconConfig;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.LaunchActivity.appExecutors")
  public static void injectAppExecutors(LaunchActivity instance, AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.LaunchActivity.loginUtil")
  public static void injectLoginUtil(LaunchActivity instance, LoginUtil loginUtil) {
    instance.loginUtil = loginUtil;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.LaunchActivity.registrationUtil")
  public static void injectRegistrationUtil(LaunchActivity instance,
      RegistrationUtil registrationUtil) {
    instance.registrationUtil = registrationUtil;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.LaunchActivity.getTokenUseCaseSync")
  public static void injectGetTokenUseCaseSync(LaunchActivity instance,
      GetTokenUseCaseSync getTokenUseCaseSync) {
    instance.getTokenUseCaseSync = getTokenUseCaseSync;
  }
}
