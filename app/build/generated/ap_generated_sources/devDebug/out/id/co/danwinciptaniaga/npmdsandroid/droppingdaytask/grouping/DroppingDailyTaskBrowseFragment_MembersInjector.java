package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskBrowseFragment_MembersInjector implements MembersInjector<DroppingDailyTaskBrowseFragment> {
  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailyTaskBrowseFragment_MembersInjector(
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  public static MembersInjector<DroppingDailyTaskBrowseFragment> create(
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailyTaskBrowseFragment_MembersInjector(commonServiceProvider, appExecutorsProvider);
  }

  @Override
  public void injectMembers(DroppingDailyTaskBrowseFragment instance) {
    injectCommonService(instance, commonServiceProvider.get());
    injectAppExecutors(instance, appExecutorsProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseFragment.commonService")
  public static void injectCommonService(DroppingDailyTaskBrowseFragment instance,
      CommonService commonService) {
    instance.commonService = commonService;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseFragment.appExecutors")
  public static void injectAppExecutors(DroppingDailyTaskBrowseFragment instance,
      AppExecutors appExecutors) {
    instance.appExecutors = appExecutors;
  }
}
