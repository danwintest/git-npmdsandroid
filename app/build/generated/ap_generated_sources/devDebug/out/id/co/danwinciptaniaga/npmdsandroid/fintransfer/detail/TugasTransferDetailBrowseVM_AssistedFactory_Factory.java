package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TugasTransferDetailBrowseVM_AssistedFactory_Factory implements Factory<TugasTransferDetailBrowseVM_AssistedFactory> {
  private final Provider<Application> appProvider;

  private final Provider<GetTugasTransferDetailBrowseUC> ucProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public TugasTransferDetailBrowseVM_AssistedFactory_Factory(Provider<Application> appProvider,
      Provider<GetTugasTransferDetailBrowseUC> ucProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.ucProvider = ucProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public TugasTransferDetailBrowseVM_AssistedFactory get() {
    return newInstance(appProvider, ucProvider, appExecutorsProvider);
  }

  public static TugasTransferDetailBrowseVM_AssistedFactory_Factory create(
      Provider<Application> appProvider, Provider<GetTugasTransferDetailBrowseUC> ucProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new TugasTransferDetailBrowseVM_AssistedFactory_Factory(appProvider, ucProvider, appExecutorsProvider);
  }

  public static TugasTransferDetailBrowseVM_AssistedFactory newInstance(Provider<Application> app,
      Provider<GetTugasTransferDetailBrowseUC> uc, Provider<AppExecutors> appExecutors) {
    return new TugasTransferDetailBrowseVM_AssistedFactory(app, uc, appExecutors);
  }
}
