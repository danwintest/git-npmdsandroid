package id.co.danwinciptaniaga.npmdsandroid.wf;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class WfHistoryVM_AssistedFactory_Factory implements Factory<WfHistoryVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<GetWfHistoryUseCase> getWfHistoryUseCaseProvider;

  public WfHistoryVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<GetWfHistoryUseCase> getWfHistoryUseCaseProvider) {
    this.applicationProvider = applicationProvider;
    this.getWfHistoryUseCaseProvider = getWfHistoryUseCaseProvider;
  }

  @Override
  public WfHistoryVM_AssistedFactory get() {
    return newInstance(applicationProvider, getWfHistoryUseCaseProvider);
  }

  public static WfHistoryVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<GetWfHistoryUseCase> getWfHistoryUseCaseProvider) {
    return new WfHistoryVM_AssistedFactory_Factory(applicationProvider, getWfHistoryUseCaseProvider);
  }

  public static WfHistoryVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<GetWfHistoryUseCase> getWfHistoryUseCase) {
    return new WfHistoryVM_AssistedFactory(application, getWfHistoryUseCase);
  }
}
