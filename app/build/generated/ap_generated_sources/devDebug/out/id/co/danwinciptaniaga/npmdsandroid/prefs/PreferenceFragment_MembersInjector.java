package id.co.danwinciptaniaga.npmdsandroid.prefs;

import dagger.MembersInjector;
import dagger.internal.InjectedFieldSignature;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class PreferenceFragment_MembersInjector implements MembersInjector<PreferenceFragment> {
  private final Provider<AndroconConfig> androconConfigProvider;

  private final Provider<LoginUtil> loginUtilProvider;

  private final Provider<TestPushNotificationUseCase> ucTestPushNotificationProvider;

  private final Provider<UploadLogFileUseCase> ucUploadLogFileProvider;

  public PreferenceFragment_MembersInjector(Provider<AndroconConfig> androconConfigProvider,
      Provider<LoginUtil> loginUtilProvider,
      Provider<TestPushNotificationUseCase> ucTestPushNotificationProvider,
      Provider<UploadLogFileUseCase> ucUploadLogFileProvider) {
    this.androconConfigProvider = androconConfigProvider;
    this.loginUtilProvider = loginUtilProvider;
    this.ucTestPushNotificationProvider = ucTestPushNotificationProvider;
    this.ucUploadLogFileProvider = ucUploadLogFileProvider;
  }

  public static MembersInjector<PreferenceFragment> create(
      Provider<AndroconConfig> androconConfigProvider, Provider<LoginUtil> loginUtilProvider,
      Provider<TestPushNotificationUseCase> ucTestPushNotificationProvider,
      Provider<UploadLogFileUseCase> ucUploadLogFileProvider) {
    return new PreferenceFragment_MembersInjector(androconConfigProvider, loginUtilProvider, ucTestPushNotificationProvider, ucUploadLogFileProvider);
  }

  @Override
  public void injectMembers(PreferenceFragment instance) {
    injectAndroconConfig(instance, androconConfigProvider.get());
    injectLoginUtil(instance, loginUtilProvider.get());
    injectUcTestPushNotification(instance, ucTestPushNotificationProvider.get());
    injectUcUploadLogFile(instance, ucUploadLogFileProvider.get());
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceFragment.androconConfig")
  public static void injectAndroconConfig(PreferenceFragment instance,
      AndroconConfig androconConfig) {
    instance.androconConfig = androconConfig;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceFragment.loginUtil")
  public static void injectLoginUtil(PreferenceFragment instance, LoginUtil loginUtil) {
    instance.loginUtil = loginUtil;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceFragment.ucTestPushNotification")
  public static void injectUcTestPushNotification(PreferenceFragment instance,
      TestPushNotificationUseCase ucTestPushNotification) {
    instance.ucTestPushNotification = ucTestPushNotification;
  }

  @InjectedFieldSignature("id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceFragment.ucUploadLogFile")
  public static void injectUcUploadLogFile(PreferenceFragment instance,
      UploadLogFileUseCase ucUploadLogFile) {
    instance.ucUploadLogFile = ucUploadLogFile;
  }
}
