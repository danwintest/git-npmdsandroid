package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = TugasTransferDetailFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface TugasTransferDetailFragment_GeneratedInjector {
  void injectTugasTransferDetailFragment(TugasTransferDetailFragment tugasTransferDetailFragment);
}
