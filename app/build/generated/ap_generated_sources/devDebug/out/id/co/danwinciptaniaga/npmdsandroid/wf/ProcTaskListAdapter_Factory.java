package id.co.danwinciptaniaga.npmdsandroid.wf;

import android.content.Context;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ProcTaskListAdapter_Factory implements Factory<ProcTaskListAdapter> {
  private final Provider<Context> contextProvider;

  public ProcTaskListAdapter_Factory(Provider<Context> contextProvider) {
    this.contextProvider = contextProvider;
  }

  @Override
  public ProcTaskListAdapter get() {
    return newInstance(contextProvider.get());
  }

  public static ProcTaskListAdapter_Factory create(Provider<Context> contextProvider) {
    return new ProcTaskListAdapter_Factory(contextProvider);
  }

  public static ProcTaskListAdapter newInstance(Context context) {
    return new ProcTaskListAdapter(context);
  }
}
