package id.co.danwinciptaniaga.npmdsandroid.outlet;

import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetOutletBalanceListUseCase_Factory implements Factory<GetOutletBalanceListUseCase> {
  private final Provider<OutletReportService> outletReportServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetOutletBalanceListUseCase_Factory(
      Provider<OutletReportService> outletReportServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.outletReportServiceProvider = outletReportServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetOutletBalanceListUseCase get() {
    return newInstance(outletReportServiceProvider.get(), appExecutorsProvider.get());
  }

  public static GetOutletBalanceListUseCase_Factory create(
      Provider<OutletReportService> outletReportServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new GetOutletBalanceListUseCase_Factory(outletReportServiceProvider, appExecutorsProvider);
  }

  public static GetOutletBalanceListUseCase newInstance(OutletReportService outletReportService,
      AppExecutors appExecutors) {
    return new GetOutletBalanceListUseCase(outletReportService, appExecutors);
  }
}
