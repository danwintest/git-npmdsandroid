package id.co.danwinciptaniaga.npmdsandroid.di;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import id.co.danwinciptaniaga.androcon.retrofit.WebServiceGenerator;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class NpmdsModule_ProvideBookingServiceFactory implements Factory<BookingService> {
  private final NpmdsModule module;

  private final Provider<WebServiceGenerator> webServiceGeneratorProvider;

  public NpmdsModule_ProvideBookingServiceFactory(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    this.module = module;
    this.webServiceGeneratorProvider = webServiceGeneratorProvider;
  }

  @Override
  public BookingService get() {
    return provideBookingService(module, webServiceGeneratorProvider.get());
  }

  public static NpmdsModule_ProvideBookingServiceFactory create(NpmdsModule module,
      Provider<WebServiceGenerator> webServiceGeneratorProvider) {
    return new NpmdsModule_ProvideBookingServiceFactory(module, webServiceGeneratorProvider);
  }

  public static BookingService provideBookingService(NpmdsModule instance,
      WebServiceGenerator webServiceGenerator) {
    return Preconditions.checkNotNull(instance.provideBookingService(webServiceGenerator), "Cannot return null from a non-@Nullable @Provides method");
  }
}
