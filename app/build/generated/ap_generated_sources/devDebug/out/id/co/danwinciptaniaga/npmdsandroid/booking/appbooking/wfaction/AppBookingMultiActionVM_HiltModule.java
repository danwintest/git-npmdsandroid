package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;
import javax.annotation.Generated;

@Generated("androidx.hilt.AndroidXHiltProcessor")
@Module
@InstallIn(ActivityRetainedComponent.class)
@OriginatingElement(
    topLevelClass = AppBookingMultiActionVM.class
)
public interface AppBookingMultiActionVM_HiltModule {
  @Binds
  @IntoMap
  @StringKey("id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionVM")
  ViewModelAssistedFactory<? extends ViewModel> bind(
      AppBookingMultiActionVM_AssistedFactory factory);
}
