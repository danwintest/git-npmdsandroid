package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class LoadDroppingDailyUseCase_Factory implements Factory<LoadDroppingDailyUseCase> {
  private final Provider<Application> appProvider;

  private final Provider<DroppingDailyService> droppingDailyServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public LoadDroppingDailyUseCase_Factory(Provider<Application> appProvider,
      Provider<DroppingDailyService> droppingDailyServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.droppingDailyServiceProvider = droppingDailyServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public LoadDroppingDailyUseCase get() {
    return newInstance(appProvider.get(), droppingDailyServiceProvider.get(), appExecutorsProvider.get());
  }

  public static LoadDroppingDailyUseCase_Factory create(Provider<Application> appProvider,
      Provider<DroppingDailyService> droppingDailyServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new LoadDroppingDailyUseCase_Factory(appProvider, droppingDailyServiceProvider, appExecutorsProvider);
  }

  public static LoadDroppingDailyUseCase newInstance(Application app,
      DroppingDailyService droppingDailyService, AppExecutors appExecutors) {
    return new LoadDroppingDailyUseCase(app, droppingDailyService, appExecutors);
  }
}
