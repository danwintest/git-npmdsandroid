package id.co.danwinciptaniaga.npmdsandroid.outlet;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class OutletBalanceReportVM_AssistedFactory_Factory implements Factory<OutletBalanceReportVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<GetOutletBalanceReportUseCase> getOutletBalanceReportUseCaseProvider;

  public OutletBalanceReportVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<GetOutletBalanceReportUseCase> getOutletBalanceReportUseCaseProvider) {
    this.applicationProvider = applicationProvider;
    this.getOutletBalanceReportUseCaseProvider = getOutletBalanceReportUseCaseProvider;
  }

  @Override
  public OutletBalanceReportVM_AssistedFactory get() {
    return newInstance(applicationProvider, getOutletBalanceReportUseCaseProvider);
  }

  public static OutletBalanceReportVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<GetOutletBalanceReportUseCase> getOutletBalanceReportUseCaseProvider) {
    return new OutletBalanceReportVM_AssistedFactory_Factory(applicationProvider, getOutletBalanceReportUseCaseProvider);
  }

  public static OutletBalanceReportVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<GetOutletBalanceReportUseCase> getOutletBalanceReportUseCase) {
    return new OutletBalanceReportVM_AssistedFactory(application, getOutletBalanceReportUseCase);
  }
}
