package id.co.danwinciptaniaga.npmdsandroid.droppingday.filter;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetDroppingDailyFilterUC_Factory implements Factory<GetDroppingDailyFilterUC> {
  private final Provider<Application> appProvider;

  private final Provider<DroppingDailyService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetDroppingDailyFilterUC_Factory(Provider<Application> appProvider,
      Provider<DroppingDailyService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetDroppingDailyFilterUC get() {
    return newInstance(appProvider.get(), serviceProvider.get(), appExecutorsProvider.get());
  }

  public static GetDroppingDailyFilterUC_Factory create(Provider<Application> appProvider,
      Provider<DroppingDailyService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new GetDroppingDailyFilterUC_Factory(appProvider, serviceProvider, appExecutorsProvider);
  }

  public static GetDroppingDailyFilterUC newInstance(Application app, DroppingDailyService service,
      AppExecutors appExecutors) {
    return new GetDroppingDailyFilterUC(app, service, appExecutors);
  }
}
