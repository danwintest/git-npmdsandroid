package id.co.danwinciptaniaga.npmdsandroid.prefs;

import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class TestPushNotificationUseCase_Factory implements Factory<TestPushNotificationUseCase> {
  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public TestPushNotificationUseCase_Factory(Provider<CommonService> commonServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public TestPushNotificationUseCase get() {
    return newInstance(commonServiceProvider.get(), appExecutorsProvider.get());
  }

  public static TestPushNotificationUseCase_Factory create(
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new TestPushNotificationUseCase_Factory(commonServiceProvider, appExecutorsProvider);
  }

  public static TestPushNotificationUseCase newInstance(CommonService commonService,
      AppExecutors appExecutors) {
    return new TestPushNotificationUseCase(commonService, appExecutors);
  }
}
