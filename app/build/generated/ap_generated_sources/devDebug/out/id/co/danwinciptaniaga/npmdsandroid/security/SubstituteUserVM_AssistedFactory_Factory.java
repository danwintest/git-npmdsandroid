package id.co.danwinciptaniaga.npmdsandroid.security;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.npmdsandroid.common.GetSubstituteUsersUC;
import id.co.danwinciptaniaga.npmdsandroid.common.SubstituteUserUC;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class SubstituteUserVM_AssistedFactory_Factory implements Factory<SubstituteUserVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<GetSubstituteUsersUC> getSubstituteUsersUCProvider;

  private final Provider<SubstituteUserUC> substituteUserUCProvider;

  public SubstituteUserVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<GetSubstituteUsersUC> getSubstituteUsersUCProvider,
      Provider<SubstituteUserUC> substituteUserUCProvider) {
    this.applicationProvider = applicationProvider;
    this.getSubstituteUsersUCProvider = getSubstituteUsersUCProvider;
    this.substituteUserUCProvider = substituteUserUCProvider;
  }

  @Override
  public SubstituteUserVM_AssistedFactory get() {
    return newInstance(applicationProvider, getSubstituteUsersUCProvider, substituteUserUCProvider);
  }

  public static SubstituteUserVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<GetSubstituteUsersUC> getSubstituteUsersUCProvider,
      Provider<SubstituteUserUC> substituteUserUCProvider) {
    return new SubstituteUserVM_AssistedFactory_Factory(applicationProvider, getSubstituteUsersUCProvider, substituteUserUCProvider);
  }

  public static SubstituteUserVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<GetSubstituteUsersUC> getSubstituteUsersUC,
      Provider<SubstituteUserUC> substituteUserUC) {
    return new SubstituteUserVM_AssistedFactory(application, getSubstituteUsersUC, substituteUserUC);
  }
}
