package id.co.danwinciptaniaga.npmdsandroid.exp;

import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetExpenseListUseCase_Factory implements Factory<GetExpenseListUseCase> {
  private final Provider<ExpenseService> expenseServiceProvider;

  public GetExpenseListUseCase_Factory(Provider<ExpenseService> expenseServiceProvider) {
    this.expenseServiceProvider = expenseServiceProvider;
  }

  @Override
  public GetExpenseListUseCase get() {
    return newInstance(expenseServiceProvider.get());
  }

  public static GetExpenseListUseCase_Factory create(
      Provider<ExpenseService> expenseServiceProvider) {
    return new GetExpenseListUseCase_Factory(expenseServiceProvider);
  }

  public static GetExpenseListUseCase newInstance(ExpenseService expenseService) {
    return new GetExpenseListUseCase(expenseService);
  }
}
