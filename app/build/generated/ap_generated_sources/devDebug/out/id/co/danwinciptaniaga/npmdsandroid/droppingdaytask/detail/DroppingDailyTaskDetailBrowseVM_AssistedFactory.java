package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class DroppingDailyTaskDetailBrowseVM_AssistedFactory implements ViewModelAssistedFactory<DroppingDailyTaskDetailBrowseVM> {
  private final Provider<Application> app;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<GetDroppingDailyTaskBrowseDetailUC> ucDetail;

  @Inject
  DroppingDailyTaskDetailBrowseVM_AssistedFactory(Provider<Application> app,
      Provider<AppExecutors> appExecutors, Provider<GetDroppingDailyTaskBrowseDetailUC> ucDetail) {
    this.app = app;
    this.appExecutors = appExecutors;
    this.ucDetail = ucDetail;
  }

  @Override
  @NonNull
  public DroppingDailyTaskDetailBrowseVM create(@NonNull SavedStateHandle arg0) {
    return new DroppingDailyTaskDetailBrowseVM(app.get(), appExecutors.get(), ucDetail.get());
  }
}
