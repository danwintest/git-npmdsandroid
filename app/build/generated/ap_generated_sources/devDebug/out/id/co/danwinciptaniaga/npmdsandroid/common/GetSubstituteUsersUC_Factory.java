package id.co.danwinciptaniaga.npmdsandroid.common;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetSubstituteUsersUC_Factory implements Factory<GetSubstituteUsersUC> {
  private final Provider<Application> applicationProvider;

  private final Provider<CommonService> commonServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetSubstituteUsersUC_Factory(Provider<Application> applicationProvider,
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.commonServiceProvider = commonServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetSubstituteUsersUC get() {
    return newInstance(applicationProvider.get(), commonServiceProvider.get(), appExecutorsProvider.get());
  }

  public static GetSubstituteUsersUC_Factory create(Provider<Application> applicationProvider,
      Provider<CommonService> commonServiceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new GetSubstituteUsersUC_Factory(applicationProvider, commonServiceProvider, appExecutorsProvider);
  }

  public static GetSubstituteUsersUC newInstance(Application application,
      CommonService commonService, AppExecutors appExecutors) {
    return new GetSubstituteUsersUC(application, commonService, appExecutors);
  }
}
