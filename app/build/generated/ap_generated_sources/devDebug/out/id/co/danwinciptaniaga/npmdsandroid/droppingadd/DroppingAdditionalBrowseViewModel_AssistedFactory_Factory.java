package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingAdditionalBrowseViewModel_AssistedFactory_Factory implements Factory<DroppingAdditionalBrowseViewModel_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  private final Provider<GetDroppingAdditionalListUseCase> getDroppingAdditionalListUseCaseProvider;

  private final Provider<DroppingListActionUC> droppingListActionUCProvider;

  public DroppingAdditionalBrowseViewModel_AssistedFactory_Factory(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<GetDroppingAdditionalListUseCase> getDroppingAdditionalListUseCaseProvider,
      Provider<DroppingListActionUC> droppingListActionUCProvider) {
    this.applicationProvider = applicationProvider;
    this.appExecutorsProvider = appExecutorsProvider;
    this.getDroppingAdditionalListUseCaseProvider = getDroppingAdditionalListUseCaseProvider;
    this.droppingListActionUCProvider = droppingListActionUCProvider;
  }

  @Override
  public DroppingAdditionalBrowseViewModel_AssistedFactory get() {
    return newInstance(applicationProvider, appExecutorsProvider, getDroppingAdditionalListUseCaseProvider, droppingListActionUCProvider);
  }

  public static DroppingAdditionalBrowseViewModel_AssistedFactory_Factory create(
      Provider<Application> applicationProvider, Provider<AppExecutors> appExecutorsProvider,
      Provider<GetDroppingAdditionalListUseCase> getDroppingAdditionalListUseCaseProvider,
      Provider<DroppingListActionUC> droppingListActionUCProvider) {
    return new DroppingAdditionalBrowseViewModel_AssistedFactory_Factory(applicationProvider, appExecutorsProvider, getDroppingAdditionalListUseCaseProvider, droppingListActionUCProvider);
  }

  public static DroppingAdditionalBrowseViewModel_AssistedFactory newInstance(
      Provider<Application> application, Provider<AppExecutors> appExecutors,
      Provider<GetDroppingAdditionalListUseCase> getDroppingAdditionalListUseCase,
      Provider<DroppingListActionUC> droppingListActionUC) {
    return new DroppingAdditionalBrowseViewModel_AssistedFactory(application, appExecutors, getDroppingAdditionalListUseCase, droppingListActionUC);
  }
}
