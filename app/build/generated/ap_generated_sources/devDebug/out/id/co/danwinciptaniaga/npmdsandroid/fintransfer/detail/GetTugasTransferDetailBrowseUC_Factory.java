package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class GetTugasTransferDetailBrowseUC_Factory implements Factory<GetTugasTransferDetailBrowseUC> {
  private final Provider<TugasTransferService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public GetTugasTransferDetailBrowseUC_Factory(Provider<TugasTransferService> serviceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public GetTugasTransferDetailBrowseUC get() {
    return newInstance(serviceProvider.get(), appExecutorsProvider.get());
  }

  public static GetTugasTransferDetailBrowseUC_Factory create(
      Provider<TugasTransferService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new GetTugasTransferDetailBrowseUC_Factory(serviceProvider, appExecutorsProvider);
  }

  public static GetTugasTransferDetailBrowseUC newInstance(TugasTransferService service,
      AppExecutors appExecutors) {
    return new GetTugasTransferDetailBrowseUC(service, appExecutors);
  }
}
