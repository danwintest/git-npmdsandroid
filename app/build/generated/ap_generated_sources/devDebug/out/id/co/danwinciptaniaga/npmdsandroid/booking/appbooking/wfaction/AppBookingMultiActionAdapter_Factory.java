package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import dagger.internal.Factory;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import java.util.List;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingMultiActionAdapter_Factory implements Factory<AppBookingMultiActionAdapter> {
  private final Provider<List<AppBookingData>> dataListProvider;

  public AppBookingMultiActionAdapter_Factory(Provider<List<AppBookingData>> dataListProvider) {
    this.dataListProvider = dataListProvider;
  }

  @Override
  public AppBookingMultiActionAdapter get() {
    return newInstance(dataListProvider.get());
  }

  public static AppBookingMultiActionAdapter_Factory create(
      Provider<List<AppBookingData>> dataListProvider) {
    return new AppBookingMultiActionAdapter_Factory(dataListProvider);
  }

  public static AppBookingMultiActionAdapter newInstance(List<AppBookingData> dataList) {
    return new AppBookingMultiActionAdapter(dataList);
  }
}
