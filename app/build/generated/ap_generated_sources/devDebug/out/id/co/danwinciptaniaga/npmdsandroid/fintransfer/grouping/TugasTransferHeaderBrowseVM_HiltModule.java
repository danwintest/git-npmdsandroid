package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;
import javax.annotation.Generated;

@Generated("androidx.hilt.AndroidXHiltProcessor")
@Module
@InstallIn(ActivityRetainedComponent.class)
@OriginatingElement(
    topLevelClass = TugasTransferHeaderBrowseVM.class
)
public interface TugasTransferHeaderBrowseVM_HiltModule {
  @Binds
  @IntoMap
  @StringKey("id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferHeaderBrowseVM")
  ViewModelAssistedFactory<? extends ViewModel> bind(
      TugasTransferHeaderBrowseVM_AssistedFactory factory);
}
