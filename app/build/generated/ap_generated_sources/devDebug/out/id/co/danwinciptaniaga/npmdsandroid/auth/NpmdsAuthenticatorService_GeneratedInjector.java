package id.co.danwinciptaniaga.npmdsandroid.auth;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ServiceComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = NpmdsAuthenticatorService.class
)
@GeneratedEntryPoint
@InstallIn(ServiceComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface NpmdsAuthenticatorService_GeneratedInjector {
  void injectNpmdsAuthenticatorService(NpmdsAuthenticatorService npmdsAuthenticatorService);
}
