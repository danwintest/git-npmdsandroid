package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import android.content.Context;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailyTaskDetailListAdapter_Factory implements Factory<DroppingDailyTaskDetailListAdapter> {
  private final Provider<DroppingDailyTaskDetailListAdapter.RecyclerViewAdapterListener> listenerProvider;

  private final Provider<Context> ctxProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailyTaskDetailListAdapter_Factory(
      Provider<DroppingDailyTaskDetailListAdapter.RecyclerViewAdapterListener> listenerProvider,
      Provider<Context> ctxProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.listenerProvider = listenerProvider;
    this.ctxProvider = ctxProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingDailyTaskDetailListAdapter get() {
    return newInstance(listenerProvider.get(), ctxProvider.get(), appExecutorsProvider.get());
  }

  public static DroppingDailyTaskDetailListAdapter_Factory create(
      Provider<DroppingDailyTaskDetailListAdapter.RecyclerViewAdapterListener> listenerProvider,
      Provider<Context> ctxProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailyTaskDetailListAdapter_Factory(listenerProvider, ctxProvider, appExecutorsProvider);
  }

  public static DroppingDailyTaskDetailListAdapter newInstance(Object listener, Context ctx,
      AppExecutors appExecutors) {
    return new DroppingDailyTaskDetailListAdapter((DroppingDailyTaskDetailListAdapter.RecyclerViewAdapterListener) listener, ctx, appExecutors);
  }
}
