package id.co.danwinciptaniaga.npmdsandroid;

import androidx.hilt.lifecycle.ViewModelFactoryModules;
import dagger.Binds;
import dagger.Component;
import dagger.Module;
import dagger.Subcomponent;
import dagger.hilt.android.components.ActivityComponent;
import dagger.hilt.android.components.ActivityRetainedComponent;
import dagger.hilt.android.components.ApplicationComponent;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.android.components.ServiceComponent;
import dagger.hilt.android.components.ViewComponent;
import dagger.hilt.android.components.ViewWithFragmentComponent;
import dagger.hilt.android.internal.builders.ActivityComponentBuilder;
import dagger.hilt.android.internal.builders.ActivityRetainedComponentBuilder;
import dagger.hilt.android.internal.builders.FragmentComponentBuilder;
import dagger.hilt.android.internal.builders.ServiceComponentBuilder;
import dagger.hilt.android.internal.builders.ViewComponentBuilder;
import dagger.hilt.android.internal.builders.ViewWithFragmentComponentBuilder;
import dagger.hilt.android.internal.lifecycle.DefaultViewModelFactories;
import dagger.hilt.android.internal.managers.ActivityComponentManager;
import dagger.hilt.android.internal.managers.FragmentComponentManager;
import dagger.hilt.android.internal.managers.HiltWrapper_ActivityRetainedComponentManager_LifecycleComponentBuilderEntryPoint;
import dagger.hilt.android.internal.managers.ServiceComponentManager;
import dagger.hilt.android.internal.managers.ViewComponentManager;
import dagger.hilt.android.internal.modules.ApplicationContextModule;
import dagger.hilt.android.internal.modules.HiltWrapper_ActivityModule;
import dagger.hilt.android.scopes.ActivityRetainedScoped;
import dagger.hilt.android.scopes.ActivityScoped;
import dagger.hilt.android.scopes.FragmentScoped;
import dagger.hilt.android.scopes.ServiceScoped;
import dagger.hilt.android.scopes.ViewScoped;
import dagger.hilt.internal.GeneratedComponent;
import dagger.hilt.migration.DisableInstallInCheck;
import id.co.danwinciptaniaga.androcon.DownloadReceiver_GeneratedInjector;
import id.co.danwinciptaniaga.androcon.di.AndroconModule;
import id.co.danwinciptaniaga.androcon.glide.HiltWrapper_AndroconGlideModule_AndroconGlideEntryPoint;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceViewModel_HiltModule;
import id.co.danwinciptaniaga.androcon.security.ProtectedActivityViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.auth.NpmdsAuthenticatorService_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingFilterVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash.AppBookingCashVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningFormVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningForm_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningListFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.BankAccountBrowseVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailTransferVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail.BookingDetailVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.di.NpmdsModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalBrowseViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.DroppingAdditionalFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.DroppingAdditionalFilterVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.DroppingDailyFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.DroppingDailyFilterVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail.DroppingDailyTaskDetailBrowseVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseFilterVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseSortFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseEditFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseEditViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.SelectBookingFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.SelectBookingViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo.TugasTransferCekSaldoVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailBrowseVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail.TugasTransferDetailFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferBrowseHeaderFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping.TugasTransferHeaderBrowseVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction.TugasTransferWfVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.notif.NpmdsNotificationService_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceBrowseVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletBalanceReportVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockByDayReportFilterFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockByDayReportFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletStockReportByDayVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.prefs.ChangePasswordVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.prefs.PreferenceViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.security.SubstituteUserFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.security.SubstituteUserVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.BankAcctFormFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.ui.bankacct.BankAcctFormVM_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.DrawerViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.LoginViewModel_HiltModule;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfHistoryFragment_GeneratedInjector;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfHistoryVM_HiltModule;
import javax.annotation.Generated;
import javax.inject.Singleton;

@Generated("dagger.hilt.processor.internal.root.RootProcessor")
public final class App_HiltComponents {
  private App_HiltComponents() {
  }

  @Module(
      subcomponents = ActivityC.class
  )
  @DisableInstallInCheck
  @Generated("dagger.hilt.processor.internal.root.RootProcessor")
  abstract interface ActivityCBuilderModule {
    @Binds
    ActivityComponentBuilder bind(ActivityC.Builder builder);
  }

  @Module(
      subcomponents = ActivityRetainedC.class
  )
  @DisableInstallInCheck
  @Generated("dagger.hilt.processor.internal.root.RootProcessor")
  abstract interface ActivityRetainedCBuilderModule {
    @Binds
    ActivityRetainedComponentBuilder bind(ActivityRetainedC.Builder builder);
  }

  @Module(
      subcomponents = FragmentC.class
  )
  @DisableInstallInCheck
  @Generated("dagger.hilt.processor.internal.root.RootProcessor")
  abstract interface FragmentCBuilderModule {
    @Binds
    FragmentComponentBuilder bind(FragmentC.Builder builder);
  }

  @Module(
      subcomponents = ServiceC.class
  )
  @DisableInstallInCheck
  @Generated("dagger.hilt.processor.internal.root.RootProcessor")
  abstract interface ServiceCBuilderModule {
    @Binds
    ServiceComponentBuilder bind(ServiceC.Builder builder);
  }

  @Module(
      subcomponents = ViewC.class
  )
  @DisableInstallInCheck
  @Generated("dagger.hilt.processor.internal.root.RootProcessor")
  abstract interface ViewCBuilderModule {
    @Binds
    ViewComponentBuilder bind(ViewC.Builder builder);
  }

  @Module(
      subcomponents = ViewWithFragmentC.class
  )
  @DisableInstallInCheck
  @Generated("dagger.hilt.processor.internal.root.RootProcessor")
  abstract interface ViewWithFragmentCBuilderModule {
    @Binds
    ViewWithFragmentComponentBuilder bind(ViewWithFragmentC.Builder builder);
  }

  @Subcomponent(
      modules = {
          FragmentCBuilderModule.class,
          ViewCBuilderModule.class,
          DefaultViewModelFactories.ActivityModule.class,
          HiltWrapper_ActivityModule.class,
          ViewModelFactoryModules.ActivityModule.class
      }
  )
  @ActivityScoped
  public abstract static class ActivityC implements ActivityComponent,
      DefaultViewModelFactories.ActivityEntryPoint,
      FragmentComponentManager.FragmentComponentBuilderEntryPoint,
      ViewComponentManager.ViewComponentBuilderEntryPoint,
      GeneratedComponent,
      LaunchActivity_GeneratedInjector,
      LandingActivity_GeneratedInjector,
      AuthenticationActivity_GeneratedInjector {
    @Subcomponent.Builder
    abstract interface Builder extends ActivityComponentBuilder {
    }
  }

  @Subcomponent(
      modules = {
          AppBookingBrowseVM_HiltModule.class,
          AppBookingCashVM_HiltModule.class,
          AppBookingFilterVM_HiltModule.class,
          AppBookingMultiActionVM_HiltModule.class,
          AppBookingTransferRekeningFormVM_HiltModule.class,
          AppBookingTransferVM_HiltModule.class,
          ActivityCBuilderModule.class,
          BankAccountBrowseVM_HiltModule.class,
          BankAcctFormVM_HiltModule.class,
          BookingBrowseViewModel_HiltModule.class,
          BookingDetailTransferVM_HiltModule.class,
          BookingDetailVM_HiltModule.class,
          BookingFilterVM_HiltModule.class,
          ChangePasswordVM_HiltModule.class,
          DrawerViewModel_HiltModule.class,
          DroppingAdditionalBrowseViewModel_HiltModule.class,
          DroppingAdditionalDetailEditViewModel_HiltModule.class,
          DroppingAdditionalEditViewModel_HiltModule.class,
          DroppingAdditionalFilterVM_HiltModule.class,
          DroppingDailyBrowseVM_HiltModule.class,
          DroppingDailyEditViewModel_HiltModule.class,
          DroppingDailyFilterVM_HiltModule.class,
          DroppingDailyTaskBrowseVM_HiltModule.class,
          DroppingDailyTaskDetailBrowseVM_HiltModule.class,
          DroppingDailyTaskFilterVM_HiltModule.class,
          DroppingDailyTaskWfVM_HiltModule.class,
          ExpenseBrowseViewModel_HiltModule.class,
          ExpenseEditViewModel_HiltModule.class,
          ExpenseFilterVM_HiltModule.class,
          LoginViewModel_HiltModule.class,
          OutletBalanceBrowseVM_HiltModule.class,
          OutletBalanceReportVM_HiltModule.class,
          OutletStockReportByDayVM_HiltModule.class,
          PreferenceViewModel_HiltModule.class,
          ProtectedActivityViewModel_HiltModule.class,
          RegisterDeviceViewModel_HiltModule.class,
          SelectBookingViewModel_HiltModule.class,
          SubstituteUserVM_HiltModule.class,
          TugasTransferCekSaldoVM_HiltModule.class,
          TugasTransferDetailBrowseVM_HiltModule.class,
          TugasTransferFilterVM_HiltModule.class,
          TugasTransferHeaderBrowseVM_HiltModule.class,
          TugasTransferWfVM_HiltModule.class,
          WfHistoryVM_HiltModule.class
      }
  )
  @ActivityRetainedScoped
  public abstract static class ActivityRetainedC implements ActivityRetainedComponent,
      ActivityComponentManager.ActivityComponentBuilderEntryPoint,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ActivityRetainedComponentBuilder {
    }
  }

  @Component(
      modules = {
          AndroconModule.class,
          ActivityRetainedCBuilderModule.class,
          ServiceCBuilderModule.class,
          ApplicationContextModule.class,
          NpmdsModule.class
      }
  )
  @Singleton
  public abstract static class ApplicationC implements ApplicationComponent,
      HiltWrapper_ActivityRetainedComponentManager_LifecycleComponentBuilderEntryPoint,
      ServiceComponentManager.ServiceComponentBuilderEntryPoint,
      GeneratedComponent,
      DownloadReceiver_GeneratedInjector,
      HiltWrapper_AndroconGlideModule_AndroconGlideEntryPoint,
      App_GeneratedInjector {
  }

  @Subcomponent(
      modules = {
          ViewWithFragmentCBuilderModule.class,
          DefaultViewModelFactories.FragmentModule.class,
          ViewModelFactoryModules.FragmentModule.class
      }
  )
  @FragmentScoped
  public abstract static class FragmentC implements FragmentComponent,
      DefaultViewModelFactories.FragmentEntryPoint,
      ViewComponentManager.ViewWithFragmentComponentBuilderEntryPoint,
      GeneratedComponent,
      BookingBrowseFragment_GeneratedInjector,
      BookingFilterFragment_GeneratedInjector,
      Abstract_AppBookingFragment_GeneratedInjector,
      AppBookingBrowseFragment_GeneratedInjector,
      AppBookingFilterFragment_GeneratedInjector,
      AppBookingTransferRekeningForm_GeneratedInjector,
      AppBookingTransferRekeningListFragment_GeneratedInjector,
      AppBookingMultiActionFragment_GeneratedInjector,
      DroppingAdditionalBrowseFragment_GeneratedInjector,
      DroppingAdditionalDetailEditFragment_GeneratedInjector,
      DroppingAdditionalEditFragment_GeneratedInjector,
      DroppingAdditionalFilterFragment_GeneratedInjector,
      DroppingDailyBrowseFragment_GeneratedInjector,
      DroppingDailyEditFragment_GeneratedInjector,
      DroppingDailyFilterFragment_GeneratedInjector,
      DroppingDailyTaskFilterFragment_GeneratedInjector,
      DroppingDailyTaskBrowseFragment_GeneratedInjector,
      DroppingDailyTaskWfFragment_GeneratedInjector,
      ExpenseBrowseFragment_GeneratedInjector,
      ExpenseFilterFragment_GeneratedInjector,
      ExpenseSortFragment_GeneratedInjector,
      ExpenseEditFragment_GeneratedInjector,
      SelectBookingFragment_GeneratedInjector,
      TugasTransferCekSaldoFragment_GeneratedInjector,
      TugasTransferDetailFragment_GeneratedInjector,
      TugasTransferFilterFragment_GeneratedInjector,
      TugasTransferBrowseHeaderFragment_GeneratedInjector,
      TugasTransferWfFragment_GeneratedInjector,
      OutletBalanceReportFilterFragment_GeneratedInjector,
      OutletBalanceReportFragment_GeneratedInjector,
      OutletStockByDayReportFilterFragment_GeneratedInjector,
      OutletStockByDayReportFragment_GeneratedInjector,
      ChangePasswordFragment_GeneratedInjector,
      PreferenceFragment_GeneratedInjector,
      SubstituteUserFragment_GeneratedInjector,
      BankAcctFormFragment_GeneratedInjector,
      WfHistoryFragment_GeneratedInjector {
    @Subcomponent.Builder
    abstract interface Builder extends FragmentComponentBuilder {
    }
  }

  @Subcomponent
  @ServiceScoped
  public abstract static class ServiceC implements ServiceComponent,
      GeneratedComponent,
      NpmdsAuthenticatorService_GeneratedInjector,
      NpmdsNotificationService_GeneratedInjector {
    @Subcomponent.Builder
    abstract interface Builder extends ServiceComponentBuilder {
    }
  }

  @Subcomponent
  @ViewScoped
  public abstract static class ViewC implements ViewComponent,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewComponentBuilder {
    }
  }

  @Subcomponent
  @ViewScoped
  public abstract static class ViewWithFragmentC implements ViewWithFragmentComponent,
      GeneratedComponent {
    @Subcomponent.Builder
    abstract interface Builder extends ViewWithFragmentComponentBuilder {
    }
  }
}
