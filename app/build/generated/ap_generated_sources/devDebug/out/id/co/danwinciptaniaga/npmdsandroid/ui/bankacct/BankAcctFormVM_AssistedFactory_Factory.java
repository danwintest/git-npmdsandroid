package id.co.danwinciptaniaga.npmdsandroid.ui.bankacct;

import android.app.Application;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class BankAcctFormVM_AssistedFactory_Factory implements Factory<BankAcctFormVM_AssistedFactory> {
  private final Provider<Application> applicationProvider;

  private final Provider<GetBanksUseCase> getBanksUseCaseProvider;

  public BankAcctFormVM_AssistedFactory_Factory(Provider<Application> applicationProvider,
      Provider<GetBanksUseCase> getBanksUseCaseProvider) {
    this.applicationProvider = applicationProvider;
    this.getBanksUseCaseProvider = getBanksUseCaseProvider;
  }

  @Override
  public BankAcctFormVM_AssistedFactory get() {
    return newInstance(applicationProvider, getBanksUseCaseProvider);
  }

  public static BankAcctFormVM_AssistedFactory_Factory create(
      Provider<Application> applicationProvider,
      Provider<GetBanksUseCase> getBanksUseCaseProvider) {
    return new BankAcctFormVM_AssistedFactory_Factory(applicationProvider, getBanksUseCaseProvider);
  }

  public static BankAcctFormVM_AssistedFactory newInstance(Provider<Application> application,
      Provider<GetBanksUseCase> getBanksUseCase) {
    return new BankAcctFormVM_AssistedFactory(application, getBanksUseCase);
  }
}
