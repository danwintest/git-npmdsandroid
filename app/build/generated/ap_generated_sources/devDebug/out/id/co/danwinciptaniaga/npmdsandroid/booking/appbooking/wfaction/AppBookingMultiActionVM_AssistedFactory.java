package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class AppBookingMultiActionVM_AssistedFactory implements ViewModelAssistedFactory<AppBookingMultiActionVM> {
  private final Provider<Application> application;

  private final Provider<AppBookingMultiActionUC> processUC;

  @Inject
  AppBookingMultiActionVM_AssistedFactory(Provider<Application> application,
      Provider<AppBookingMultiActionUC> processUC) {
    this.application = application;
    this.processUC = processUC;
  }

  @Override
  @NonNull
  public AppBookingMultiActionVM create(@NonNull SavedStateHandle arg0) {
    return new AppBookingMultiActionVM(application.get(), processUC.get());
  }
}
