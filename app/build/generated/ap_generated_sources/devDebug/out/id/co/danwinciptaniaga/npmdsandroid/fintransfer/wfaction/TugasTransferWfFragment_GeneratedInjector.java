package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = TugasTransferWfFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface TugasTransferWfFragment_GeneratedInjector {
  void injectTugasTransferWfFragment(TugasTransferWfFragment tugasTransferWfFragment);
}
