package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppBookingLoadUC_Factory implements Factory<AppBookingLoadUC> {
  private final Provider<Application> appProvider;

  private final Provider<AppBookingService> serviceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public AppBookingLoadUC_Factory(Provider<Application> appProvider,
      Provider<AppBookingService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    this.appProvider = appProvider;
    this.serviceProvider = serviceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public AppBookingLoadUC get() {
    return newInstance(appProvider.get(), serviceProvider.get(), appExecutorsProvider.get());
  }

  public static AppBookingLoadUC_Factory create(Provider<Application> appProvider,
      Provider<AppBookingService> serviceProvider, Provider<AppExecutors> appExecutorsProvider) {
    return new AppBookingLoadUC_Factory(appProvider, serviceProvider, appExecutorsProvider);
  }

  public static AppBookingLoadUC newInstance(Application app, AppBookingService service,
      AppExecutors appExecutors) {
    return new AppBookingLoadUC(app, service, appExecutors);
  }
}
