package id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelAssistedFactory;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import java.lang.Override;
import javax.annotation.Generated;
import javax.inject.Inject;
import javax.inject.Provider;

@Generated("androidx.hilt.AndroidXHiltProcessor")
public final class SelectBookingViewModel_AssistedFactory implements ViewModelAssistedFactory<SelectBookingViewModel> {
  private final Provider<Application> application;

  private final Provider<AppExecutors> appExecutors;

  private final Provider<GetBookingMediatorListUseCase> getBookingMediatorListUseCase;

  @Inject
  SelectBookingViewModel_AssistedFactory(Provider<Application> application,
      Provider<AppExecutors> appExecutors,
      Provider<GetBookingMediatorListUseCase> getBookingMediatorListUseCase) {
    this.application = application;
    this.appExecutors = appExecutors;
    this.getBookingMediatorListUseCase = getBookingMediatorListUseCase;
  }

  @Override
  @NonNull
  public SelectBookingViewModel create(@NonNull SavedStateHandle arg0) {
    return new SelectBookingViewModel(application.get(), appExecutors.get(),
        getBookingMediatorListUseCase.get());
  }
}
