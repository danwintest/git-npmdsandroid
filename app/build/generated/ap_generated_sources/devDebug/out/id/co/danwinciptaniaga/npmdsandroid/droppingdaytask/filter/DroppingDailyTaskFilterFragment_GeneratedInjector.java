package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;
import javax.annotation.Generated;

@OriginatingElement(
    topLevelClass = DroppingDailyTaskFilterFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
@Generated("dagger.hilt.android.processor.internal.androidentrypoint.InjectorEntryPointGenerator")
public interface DroppingDailyTaskFilterFragment_GeneratedInjector {
  void injectDroppingDailyTaskFilterFragment(
      DroppingDailyTaskFilterFragment droppingDailyTaskFilterFragment);
}
