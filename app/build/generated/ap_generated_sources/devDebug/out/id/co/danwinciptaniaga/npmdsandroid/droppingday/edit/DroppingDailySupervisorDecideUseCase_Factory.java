package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import android.app.Application;
import dagger.internal.Factory;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyService;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class DroppingDailySupervisorDecideUseCase_Factory implements Factory<DroppingDailySupervisorDecideUseCase> {
  private final Provider<Application> applicationProvider;

  private final Provider<DroppingDailyService> ddServiceProvider;

  private final Provider<AppExecutors> appExecutorsProvider;

  public DroppingDailySupervisorDecideUseCase_Factory(Provider<Application> applicationProvider,
      Provider<DroppingDailyService> ddServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    this.applicationProvider = applicationProvider;
    this.ddServiceProvider = ddServiceProvider;
    this.appExecutorsProvider = appExecutorsProvider;
  }

  @Override
  public DroppingDailySupervisorDecideUseCase get() {
    return newInstance(applicationProvider.get(), ddServiceProvider.get(), appExecutorsProvider.get());
  }

  public static DroppingDailySupervisorDecideUseCase_Factory create(
      Provider<Application> applicationProvider, Provider<DroppingDailyService> ddServiceProvider,
      Provider<AppExecutors> appExecutorsProvider) {
    return new DroppingDailySupervisorDecideUseCase_Factory(applicationProvider, ddServiceProvider, appExecutorsProvider);
  }

  public static DroppingDailySupervisorDecideUseCase newInstance(Application application,
      DroppingDailyService ddService, AppExecutors appExecutors) {
    return new DroppingDailySupervisorDecideUseCase(application, ddService, appExecutors);
  }
}
