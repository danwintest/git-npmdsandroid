package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;
import javax.annotation.Generated;

/**
 * Generated class to pass information through multiple javac runs.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityRetainedComponent",
    modules = "id.co.danwinciptaniaga.npmdsandroid.droppingday.edit.DroppingDailyEditViewModel_HiltModule"
)
@Generated("dagger.hilt.processor.internal.aggregateddeps.AggregatedDepsGenerator")
class id_co_danwinciptaniaga_npmdsandroid_droppingday_edit_DroppingDailyEditViewModel_HiltModuleModuleDeps {
}
