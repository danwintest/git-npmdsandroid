package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;
import javax.annotation.Generated;

/**
 * Generated class to pass information through multiple javac runs.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.FragmentComponent",
    entryPoints = "id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking.SelectBookingFragment_GeneratedInjector"
)
@Generated("dagger.hilt.processor.internal.aggregateddeps.AggregatedDepsGenerator")
class id_co_danwinciptaniaga_npmdsandroid_exp_edit_selectBooking_SelectBookingFragment_GeneratedInjectorModuleDeps {
}
