package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;
import javax.annotation.Generated;

/**
 * Generated class to pass information through multiple javac runs.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ServiceComponent",
    entryPoints = "id.co.danwinciptaniaga.npmdsandroid.notif.NpmdsNotificationService_GeneratedInjector"
)
@Generated("dagger.hilt.processor.internal.aggregateddeps.AggregatedDepsGenerator")
class id_co_danwinciptaniaga_npmdsandroid_notif_NpmdsNotificationService_GeneratedInjectorModuleDeps {
}
