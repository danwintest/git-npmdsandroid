package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;
import javax.annotation.Generated;

/**
 * Generated class to pass information through multiple javac runs.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.FragmentComponent",
    entryPoints = "id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningForm_GeneratedInjector"
)
@Generated("dagger.hilt.processor.internal.aggregateddeps.AggregatedDepsGenerator")
class id_co_danwinciptaniaga_npmdsandroid_booking_appbooking_transfer_RekeningListForm_AppBookingTransferRekeningForm_GeneratedInjectorModuleDeps {
}
