package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;
import javax.annotation.Generated;

/**
 * Generated class to pass information through multiple javac runs.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ActivityComponent",
    entryPoints = "id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity_GeneratedInjector"
)
@Generated("dagger.hilt.processor.internal.aggregateddeps.AggregatedDepsGenerator")
class id_co_danwinciptaniaga_npmdsandroid_ui_landing_LandingActivity_GeneratedInjectorModuleDeps {
}
