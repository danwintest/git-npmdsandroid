package id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking;

import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmds.data.booking.BookingListResponse;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListPagingSource;

public class SelectBookingListPagingSource extends ListenableFuturePagingSource<Integer, BookingData> {
  private static final String TAG = ExpenseListPagingSource.class.getSimpleName();
  private final GetBookingMediatorListUseCase bookingMediatorListUseCase;
  private final Executor mBgExecutor;

  public SelectBookingListPagingSource(GetBookingMediatorListUseCase bookingMediatorListUseCase,
      Executor mBgExecutor) {
    this.bookingMediatorListUseCase = bookingMediatorListUseCase;
    this.mBgExecutor = mBgExecutor;
  }

  @NotNull
  @Override
  public ListenableFuture<LoadResult<Integer, BookingData>> loadFuture(
      @NotNull LoadParams<Integer> loadParams) {
    // Start refresh at page 1 if undefined.
    Integer nextPageNumber = loadParams.getKey();
    if (nextPageNumber == null) {
      nextPageNumber = 1;
    }
    // TODO: cek apakah perlu fix?
    // kalau pakai LoadSize, pertama kali (nextPageNumber=1) = 3 * PageSize
    // tapi ini mengakibatkan pada nextPageNumber=2, data yang terbaca adalah data yg sudah didapatkan pada pertama kali
    // sehingga data menjadi duplikat
    int pageSize = loadParams.getPageSize();

    ListenableFuture<LoadResult<Integer, BookingData>> pageFuture = Futures.transform(
        bookingMediatorListUseCase.getMediatorBookingList(nextPageNumber, pageSize),
        this::toLoadResult, mBgExecutor);
    return Futures.catching(
        pageFuture, Exception.class,
        input -> {
          HyperLog.e(TAG, "Error while getting ExpenseList", input);
          return new LoadResult.Error(input);
        }, mBgExecutor);
  }

  private LoadResult<Integer, BookingData> toLoadResult(
      @NonNull BookingListResponse response) {
    return new LoadResult.Page<>(
        response.getBookingData(),
        null, // Only paging forward.
        response.getNextPageNumber(),
        LoadResult.Page.COUNT_UNDEFINED,
        LoadResult.Page.COUNT_UNDEFINED);
  }
}
