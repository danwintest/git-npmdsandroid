package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailResponse;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.TugasDroppingHarianDetailSort;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.DroppingDailyTaskService;

public class GetDroppingDailyTaskBrowseDetailUC {
  private final DroppingDailyTaskService service;
  private DroppingDailyTaskBrowseFilter filterField = new DroppingDailyTaskBrowseFilter();
  private TugasDroppingHarianDetailSort sortField = new TugasDroppingHarianDetailSort();

  @Inject
  public GetDroppingDailyTaskBrowseDetailUC(DroppingDailyTaskService service){
    this.service = service;
  }

  public void setFilterField(
      DroppingDailyTaskBrowseFilter filterField) {
    this.filterField = filterField;
  }

  public void setSortField(Set<SortOrder> sortOrder) {
    this.sortField.setSortOrders(sortOrder);
  }

  public ListenableFuture<DroppingDailyTaskBrowseDetailResponse> getDroppingTaskBrowseDetailList(
      Integer page, int pageSize) {
    ListenableFuture<DroppingDailyTaskBrowseDetailResponse> result = service.getDetailList(page,
        pageSize, filterField, sortField);
    return result;
  }
}
