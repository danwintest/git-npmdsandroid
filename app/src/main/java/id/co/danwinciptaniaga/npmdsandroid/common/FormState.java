package id.co.danwinciptaniaga.npmdsandroid.common;

import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseFormState;

public class FormState {
  public enum State {
    LOADING, READY, ACTION_IN_PROGRESS, ERROR
  }

  private String message;
  private FormState.State state;

  public FormState(FormState.State state, String message) {
    this.state = state;
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public FormState.State getState() {
    return state;
  }

  public static FormState loading(String message) {
    return new FormState(FormState.State.LOADING, message);
  }

  public static FormState ready(String message) {
    return new FormState(FormState.State.READY, message);
  }

  public static FormState actionInProgress(String message) {
    return new FormState(FormState.State.ACTION_IN_PROGRESS, message);
  }

  public static FormState error(String message) {
    return new FormState(FormState.State.ERROR, message);
  }
}
