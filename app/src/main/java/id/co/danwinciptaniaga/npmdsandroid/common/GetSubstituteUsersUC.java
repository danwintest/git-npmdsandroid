package id.co.danwinciptaniaga.npmdsandroid.common;

import java.util.List;

import javax.inject.Inject;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import androidx.annotation.Nullable;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.ExtUserInfoData;

public class GetSubstituteUsersUC
    extends BaseObservable<GetSubstituteUsersUC.Listener, List<ExtUserInfoData>> {

  public interface Listener {

    void onGetSubstituteUsersStarted();

    void onGetSubstituteUsersSuccess(Resource<List<ExtUserInfoData>> result);

    void onSubstituteUsersFailure(Resource<List<ExtUserInfoData>> responseError);
  }

  private CommonService commonService;

  private final AppExecutors appExecutors;

  @Inject
  public GetSubstituteUsersUC(Application application, CommonService commonService,
      AppExecutors appExecutors) {
    super(application);
    this.commonService = commonService;
    this.appExecutors = appExecutors;
  }

  public ListenableFuture<List<ExtUserInfoData>> getSubstituteUsers() {
    notifyStart(null, null);

    ListenableFuture<List<ExtUserInfoData>> lf = commonService.getSubstituteUsers();
    Futures.addCallback(lf, new FutureCallback<List<ExtUserInfoData>>() {
      @Override
      public void onSuccess(@Nullable List<ExtUserInfoData> result) {
        notifySuccess("Daftar Substitusi User berhasil dimuat", result);
      }

      @Override
      public void onFailure(Throwable t) {
        notifyFailure("Gagal memuat Daftar Substitusi User", null, t);
      }
    }, appExecutors.backgroundIO());
    return lf;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<List<ExtUserInfoData>> result) {
    listener.onGetSubstituteUsersStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<List<ExtUserInfoData>> result) {
    listener.onGetSubstituteUsersSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<List<ExtUserInfoData>> result) {
    listener.onSubstituteUsersFailure(result);
  }
}
