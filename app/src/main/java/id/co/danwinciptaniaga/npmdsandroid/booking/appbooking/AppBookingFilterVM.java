package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.CompanyData;
import id.co.danwinciptaniaga.npmds.data.booking.BookingFilterResponse;
import id.co.danwinciptaniaga.npmds.data.common.WfStatusData;
import id.co.danwinciptaniaga.npmdsandroid.booking.GetBookingFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class AppBookingFilterVM extends AndroidViewModel implements GetBookingFilterUC.Listener {
  private final static String TAG = AppBookingFilterVM.class.getSimpleName();
  private AppBookingBrowseFilter filterField = new AppBookingBrowseFilter();
  private MutableLiveData<KeyValueModel<String, String>> bookingTypeKvm = new MutableLiveData<>();
  private MutableLiveData<KeyValueModel<String, String>> operationKvm = new MutableLiveData<>();
  private MutableLiveData<LocalDate> toDate = new MutableLiveData<>();
  private MutableLiveData<LocalDate> fromDate = new MutableLiveData<>();
  private MutableLiveData<String> consumerName = new MutableLiveData<>();
  private MutableLiveData<String> vehicleNo= new MutableLiveData<>();
  private MutableLiveData<String> trxNo = new MutableLiveData<>();

  private List<KeyValueModel<String, String>> bookingTypeKvmList = new ArrayList<>();
  private List<KeyValueModel<String, String>> operationKvmList = new ArrayList<>();

  private final GetBookingFilterUC ucFilter;
  private KeyValueModel<UUID, String> companyKvm = null;
  private MutableLiveData<List<KeyValueModel<UUID, String>>> companyKvmList = new MutableLiveData<>();
  private MutableLiveData<List<KeyValueModel<String, String>>> statusKvmList = new MutableLiveData<>();
  private MutableLiveData<List<KeyValueModel<String, String>>> selectedStatusKvmList = new MutableLiveData<>();
  private MutableLiveData<FormState> formState = new MutableLiveData<>();

  @ViewModelInject
  public AppBookingFilterVM(@NonNull Application application, GetBookingFilterUC ucService) {
    super(application);
    this.ucFilter = ucService;
    this.ucFilter.registerListener(this);
    bookingTypeKvmList.add(new KeyValueModel<>("-", "All"));
    bookingTypeKvmList.add(new KeyValueModel<>(Utility.BOOKING_TYPE_CASH, "Cash"));
    bookingTypeKvmList.add(new KeyValueModel<>(Utility.BOOKING_TYPE_TRANSFER, "Transfer"));

    operationKvmList.add(new KeyValueModel<>("-", "All"));
    operationKvmList.add(new KeyValueModel<>(Utility.OPERATION_NEW, "Baru"));
    operationKvmList.add(new KeyValueModel<>(Utility.OPERATION_EDIT, "Ubah"));
    operationKvmList.add(new KeyValueModel<>(Utility.OPERATION_CANCEL, "Batal"));
  }

  public List<KeyValueModel<String,String>> getBookingTypeKvmList(){
    return this.bookingTypeKvmList;
  }

  public List<KeyValueModel<String, String>> getOperationKvmList() {
    return operationKvmList;
  }

  public MutableLiveData<KeyValueModel<String, String>> getBookingTypeKvm() {
    return bookingTypeKvm;
  }

  public void setBookingTypeKvm(KeyValueModel<String, String> kvmObj, String id) {
    HyperLog.d(TAG, "setBookingTypeKvm kvmObj->" + kvmObj + "<- id->" + id + "<-");
    if (id != null) {
      for (KeyValueModel<String, String> typeKvm : bookingTypeKvmList) {
        if (typeKvm.getKey().equals(id))
          bookingTypeKvm.postValue(typeKvm);
      }
    } else {
      if (kvmObj != null) {
        if (!kvmObj.equals(bookingTypeKvm.getValue()))
          bookingTypeKvm.postValue(kvmObj);
      } else {
        bookingTypeKvm.postValue(bookingTypeKvmList.get(0));
      }
    }
  }

  public MutableLiveData<KeyValueModel<String, String>> getOperationKvm() {
    return operationKvm;
  }

  public void setOperationKvm(KeyValueModel<String, String> kvmObj, String id) {
    HyperLog.d(TAG, "setOperationKvm kvmObj->" + kvmObj + "<- id->" + id + "<-");
    if (id != null) {
      for (KeyValueModel<String, String> opKvm : operationKvmList) {
        if (opKvm.getKey().equals(id))
          operationKvm.postValue(opKvm);
      }
    } else {
      if (kvmObj != null) {
        if (!kvmObj.equals(operationKvm.getValue()))
          operationKvm.postValue(kvmObj);
      } else {
        operationKvm.postValue(operationKvmList.get(0));
      }
    }
  }

  public MutableLiveData<LocalDate> getToDate() {
    return toDate;
  }

  public void setToDate(LocalDate date) {
    HyperLog.d(TAG, "setToDate date->" + date + "<-");
    this.toDate.postValue(date);
  }

  public MutableLiveData<LocalDate> getFromDate() {
    return fromDate;
  }

  public void setFromDate(LocalDate date) {
    HyperLog.d(TAG, "setFromDate date->" + date + "<-");
    this.fromDate.postValue(date);
  }

  public MutableLiveData<String> getConsumerName() {
    return consumerName;
  }

  public void setConsumerName(String name) {
    HyperLog.d(TAG, "setConsumerName name->" + name + "<-");
    if (consumerName.getValue() != name)
      consumerName.postValue(name);
  }

  public MutableLiveData<String> getVehicleNo() {
    return vehicleNo;
  }

  public void setVehicleNo(String no) {
    HyperLog.d(TAG, "setVehicleNo no->" + no + "<-");
    if (vehicleNo.getValue() != no)
      vehicleNo.postValue(no);
  }

  public MutableLiveData<String> getTrxNo() {
    return trxNo;
  }

  public void setTrxNo(String data) {
    HyperLog.d(TAG, "setTrxNo [" + data + "]");
    if (trxNo.getValue() != data)
      trxNo.postValue(data);
  }

  public AppBookingBrowseFilter getFilterField(){
    String bookingType = null;
    String operation = null;
    if (getBookingTypeKvm().getValue() != null) {
      bookingType = getBookingTypeKvm().getValue().getKey() == "-" ?
          null :
          getBookingTypeKvm().getValue().getKey();
    }

    if (getOperationKvm().getValue() != null) {
      operation = getOperationKvm().getValue().getKey() == "-" ?
          null :
          getOperationKvm().getValue().getKey();
    }

    filterField.setBookingType(bookingType);
    filterField.setOperation(operation);
    filterField.setToBookingDate(getToDate().getValue() != null ? getToDate().getValue() : null);
    filterField.setFromBookingDate(getFromDate().getValue()!=null?getFromDate().getValue():null);
    filterField.setConsumerName(getConsumerName().getValue()!=null?getConsumerName().getValue():null);
    filterField.setVehicleNo(getVehicleNo().getValue()!=null?getVehicleNo().getValue():null);
    filterField.setCompanyId(getCompanyKvm() != null ? getCompanyKvm().getKey() : null);
    filterField.setTransactionNo(getTrxNo() != null ? getTrxNo().getValue() : null);
    Set<String> selectedStatus = new HashSet<>();
    if (selectedStatusKvmList.getValue() != null) {
      for (KeyValueModel<String, String> selectedStatus2 : selectedStatusKvmList.getValue()) {
        selectedStatus.add(selectedStatus2.getKey());
      }
    }
    filterField.setStatus(selectedStatus);
    return filterField;
  }

  public void loadFilter() {
    this.ucFilter.getAppBookingFilter();
  }

  public void setFilterField(AppBookingBrowseFilter filterField){
    this.filterField = filterField;
    setBookingTypeKvm(null, this.filterField.getBookingType());
    setOperationKvm(null, this.filterField.getOperation());
    setFromDate(this.filterField.getFromBookingDate());
    setToDate(this.filterField.getToBookingDate());
    setConsumerName(this.filterField.getConsumerName());
    setVehicleNo(this.filterField.getVehicleNo());
    setTrxNo(this.filterField.getTransactionNo());
  }

  public KeyValueModel<UUID, String> getCompanyKvm() {
    return companyKvm;
  }

  public void setCompanyKvm(KeyValueModel<UUID, String> companyKvm) {
    this.companyKvm = companyKvm;
  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getCompanyKvmList() {
    return companyKvmList;
  }

  public MutableLiveData<List<KeyValueModel<String, String>>> getSelectedStatusKvmList() {
    return this.selectedStatusKvmList;
  }

  public void setSelectedStatus(boolean isAdd, KeyValueModel<String, String> selectedKvm) {
    List<KeyValueModel<String, String>> currentSelectedData = selectedStatusKvmList.getValue();
    currentSelectedData = currentSelectedData == null ? new ArrayList<>() : currentSelectedData;
    if (isAdd) {
      if (!currentSelectedData.contains(selectedKvm))
        currentSelectedData.add(selectedKvm);
    } else {
      currentSelectedData.remove(selectedKvm);
    }
    selectedStatusKvmList.postValue(currentSelectedData);
  }

  private void setFirstSelectedStatus(List<KeyValueModel<String,String>> selectedStatusKvmList){
    this.selectedStatusKvmList.postValue(selectedStatusKvmList);
  }

  public MutableLiveData<List<KeyValueModel<String,String>>> getStatusKvmList(){
    return this.statusKvmList;
  }

  public void setStatusKvmList(List<KeyValueModel<String, String>> statusKvmList) {
    this.statusKvmList.postValue(statusKvmList);
  }

  public void setStatusKvm(List<WfStatusData> wsdList) {
    List<KeyValueModel<String, String>> kvmStatusList = new ArrayList<>();
    List<KeyValueModel<String, String>> kvmSelecedStatusList = new ArrayList<>();
    for (WfStatusData wsd : wsdList) {
      KeyValueModel<String, String> kvmStatus = new KeyValueModel<>(wsd.getId(), wsd.getName());
      boolean isSelected = this.filterField.getStatus() == null ?
          false :
          this.filterField.getStatus().contains(wsd.getId());
      if (isSelected) {
        kvmSelecedStatusList.add(kvmStatus);
      }
      kvmStatusList.add(kvmStatus);
    }
    setFirstSelectedStatus(kvmSelecedStatusList);
    setStatusKvmList(kvmStatusList);
  }

  public void setCompanyKvmList(List<CompanyData> cdList) {
    List<KeyValueModel<UUID, String>> kvmCompanyList = new ArrayList<>();
    KeyValueModel<UUID, String> kvmCompanyBlank = new KeyValueModel<>(null, null);
    kvmCompanyList.add(kvmCompanyBlank);
    for (CompanyData cd : cdList) {
      KeyValueModel<UUID, String> kvmCompany = new KeyValueModel<>(cd.getId(), cd.getCompanyName());
      HyperLog.d(TAG,"current["+this.filterField.getCompanyId()+"] - opt["+cd.getId()+"]");
      if (cd.getId().equals(this.filterField.getCompanyId())) {
        setCompanyKvm(kvmCompany);
      }
      kvmCompanyList.add(kvmCompany);
    }
    this.companyKvmList.postValue(kvmCompanyList);
  }

  public MutableLiveData<FormState> getFormState() {
    return formState;
  }

  @Override
  public void onProcessStarted(Resource<BookingFilterResponse> loading) {
    formState.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessSuccess(Resource<BookingFilterResponse> res) {
    List<CompanyData> cdList = res.getData().getCompanyDataList();
    List<WfStatusData> statusList = res.getData().getWfStatusDataList();
    setCompanyKvmList(cdList);
    setStatusKvm(statusList);
    formState.postValue(FormState.ready(res.getMessage()));
  }

  @Override
  public void onProcessFailure(Resource<BookingFilterResponse> res) {
    formState.postValue(FormState.error(res.getMessage()));
  }

}
