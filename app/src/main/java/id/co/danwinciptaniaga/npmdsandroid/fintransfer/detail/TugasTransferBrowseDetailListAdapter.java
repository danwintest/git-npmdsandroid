package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseDetailData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiTugasTransferDetailBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class TugasTransferBrowseDetailListAdapter extends
    PagingDataAdapter<TugasTransferBrowseDetailData, TugasTransferBrowseDetailListAdapter.ViewHolder> {
  private final RecyclerViewAdapterListener mListener;
  private SparseBooleanArray selectedItems;
  private List<TugasTransferBrowseDetailData> selectedData = new ArrayList<>();
  private SparseBooleanArray animationItemsIndex;
  private Context mCtx;
  public AppExecutors appExecutors;

  public TugasTransferBrowseDetailListAdapter(RecyclerViewAdapterListener listener, Context ctx,
      AppExecutors appExecutors) {
    super(new DiffCallBack());
    this.mListener = listener;
    selectedItems = new SparseBooleanArray();
    animationItemsIndex = new SparseBooleanArray();
    mCtx = ctx;
    this.appExecutors = appExecutors;

  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.li_tugas_transfer_detail_browse, parent, false);
    return new ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
    holder.view.setActivated(selectedItems.get(position, false));
    holder.binding.getRoot().setActivated(selectedItems.get(position, false));
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiTugasTransferDetailBrowseBinding binding;
    private final View view;

    public ViewHolder(View v) {
      super(v);
      view = v;
      binding = LiTugasTransferDetailBrowseBinding.bind(v);
    }

    public void bind(TugasTransferBrowseDetailData data, int pos) {
      setObject(data);
      setObjectListener(data, pos);
    }

    private void setObject(TugasTransferBrowseDetailData data) {
      binding.tvTrxNo.setText(data.getTransactionNo());
      binding.tvJenisTransferTransferDate.setText(data.getTransferInfo());
      binding.tvOutletCodeOutletName.setText(data.getOutletInfo());
      binding.tvJenisTransaksi.setText(data.getJenisTransaksiName());
      binding.tvStatusTransfer.setText(data.getTransferStatus());
      binding.tvNamaTugas.setText(data.getNamaTugas());
      binding.tvBatch.setText(data.getBatchName());
//      binding.tvRekeningSumber.setText(data.getBankName());
      binding.tvConsumerNameAccNameAccNoIsAlt.setText(data.getAccInfo());
      binding.tvAmountVal.setText(Utility.getFormattedAmt(data.getAmount()));
    }

    private void setObjectListener(TugasTransferBrowseDetailData data, int pos) {
      binding.clContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, data, pos);
        }
      });

      binding.clContainer.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
          mListener.onItemLongClickedListener(v, data, pos);
          return true;
        }
      });
    }

  }

  interface RecyclerViewAdapterListener {
    void onItemClicked(View view, TugasTransferBrowseDetailData data, int position);

    void onItemLongClickedListener(View v, TugasTransferBrowseDetailData data, int position);
  }

  private static class DiffCallBack extends DiffUtil.ItemCallback<TugasTransferBrowseDetailData> {

    @Override
    public boolean areItemsTheSame(@NonNull TugasTransferBrowseDetailData oldItem,
        @NonNull TugasTransferBrowseDetailData newItem) {
      return oldItem.getTransferInstructionId() == newItem.getTransferInstructionId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull TugasTransferBrowseDetailData oldItem,
        @NonNull TugasTransferBrowseDetailData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public void toggleSelection(int pos) {
    TugasTransferBrowseDetailData data = getItem(pos);
    if (selectedItems.get(pos, false)) {
      selectedItems.delete(pos);
      selectedData.remove(data);
      animationItemsIndex.delete(pos);
    } else {
      selectedData.add(data);
      selectedItems.put(pos, true);
    }
    notifyItemChanged(pos);
  }

  public int getSelectedItemCount() {
    return selectedItems.size();
  }

  public List<TugasTransferBrowseDetailData> getSelectedData() {
    return selectedData;
  }

  public TugasTransferBrowseDetailData getSelecteData(int pos){
    return getItem(pos);
  }

  public void clearSelections() {
    // reverseAllAnimations = true;
    selectedItems.clear();
    selectedData.clear();
    notifyDataSetChanged();
  }

  public void resetAnimationIndex() {
    // reverseAllAnimations = false;
    animationItemsIndex.clear();
  }

}
