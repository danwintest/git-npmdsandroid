package id.co.danwinciptaniaga.npmdsandroid.ui.login;

import android.app.Application;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.hilt.Assisted;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCase;
import id.co.danwinciptaniaga.androcon.auth.GetUserInfoUseCase;
import id.co.danwinciptaniaga.androcon.auth.TokenResponse;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceUseCaseSync;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmdsandroid.R;

public class LoginViewModel extends AndroidViewModel implements GetTokenUseCase.Listener {

  private final SavedStateHandle savedStateHandle;
  private final RegisterDeviceUseCaseSync registerDeviceUseCaseSync;
  private GetTokenUseCase getTokenUseCase;

  private boolean usernameOk;
  private boolean passwordOk;

  private MutableLiveData<Integer> usernameError = new MutableLiveData<>(null);
  private MutableLiveData<Integer> passwordError = new MutableLiveData<>(null);
  private MutableLiveData<Boolean> dataValid = new MutableLiveData<>(false);

  // loginResult dapat dimonitor ketika action login dilakukan
  private MutableLiveData<Resource<TokenResponse>> loginResult = new MutableLiveData<>();

  @ViewModelInject
  public LoginViewModel(@NonNull Application application, GetTokenUseCase getTokenUseCase,
      RegisterDeviceUseCaseSync registerDeviceUseCaseSync,
      @Assisted SavedStateHandle savedStateHandle) {
    super(application);
    this.getTokenUseCase = getTokenUseCase;
    this.registerDeviceUseCaseSync = registerDeviceUseCaseSync;

    // TODO: observe apakah register/unregister UseCase.Listener aman di sini
    this.getTokenUseCase.registerListener(this);
    this.savedStateHandle = savedStateHandle;
  }

  public MutableLiveData<Integer> getUsernameError() {
    return usernameError;
  }

  public MutableLiveData<Integer> getPasswordError() {
    return passwordError;
  }

  public MutableLiveData<Boolean> getDataValid() {
    return dataValid;
  }

  public MutableLiveData<Resource<TokenResponse>> login(String username, String password) {
    getTokenUseCase.getToken(username, password);
    return loginResult;
  }

  public void usernameChanged(String username) {
    usernameOk = false;
    if (username == null || TextUtils.isEmpty(username)) {
      usernameError.postValue(R.string.invalid_username);
    } else {
      usernameError.postValue(null);
      usernameOk = true;
    }
    dataValid.postValue(usernameOk && passwordOk);
  }

  public void passwordChanged(String password) {
    passwordOk = false;
    if (password == null || TextUtils.isEmpty(password) || password.length() < 5) {
      passwordError.postValue(R.string.invalid_password);
    } else {
      passwordError.postValue(null);
      passwordOk = true;
    }
    dataValid.postValue(usernameOk && passwordOk);
  }

  @Override
  public void onGetTokenStarted() {
    loginResult.postValue(Resource.Builder.loading(null, null));
  }

  @Override
  public void onGetTokenSuccess(Resource<TokenResponse> response) {
    loginResult.postValue(response);
  }

  @Override
  public void onGetTokenFailure(Resource responseError) {
    loginResult.postValue(responseError);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    // TODO: observe apakah register/unregister UseCase.Listener aman di sini
    getTokenUseCase.unregisterListener(this);
  }
}
