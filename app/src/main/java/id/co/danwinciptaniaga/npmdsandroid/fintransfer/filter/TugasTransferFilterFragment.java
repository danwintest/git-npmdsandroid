package id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentTugasTransferFilterBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

@AndroidEntryPoint
public class TugasTransferFilterFragment extends BottomSheetDialogFragment {
  private static final String TAG = TugasTransferFilterFragment.class.getSimpleName();
  public static final String TUGAS_TRANSFER_FILTER_FIELD_GROUPING = "TUGAS_TRANSFER_FILTER_FIELD_GROUPING";
  public static final String TUGAS_TRANSFER_FILTER_FIELD_DETAIL = "TUGAS_TRANSFER_FILTER_FIELD_DETAIL";
  private FragmentTugasTransferFilterBinding binding;
  private ArrayAdapter<KeyValueModel<UUID, String>> companyAdapter;
  private ArrayAdapter<KeyValueModel<String, String>> jenisTransferAdapter;
  private ArrayAdapter<KeyValueModel<UUID, String>> rekeningSumberAdapter;
  private ArrayAdapter<KeyValueModel<String, String>> batchAdapter;
  private ArrayAdapter<KeyValueModel<String, String>> transferStatusAdapter;
  private ArrayAdapter<KeyValueModel<String, String>> jenisTransaksiAdapter;
  private TugasTransferFilterVM vm;

  public TugasTransferFilterFragment() {
    // Required empty public constructor
  }

  public static TugasTransferFilterFragment newInstance(String param1, String param2) {
    TugasTransferFilterFragment fragment = new TugasTransferFilterFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentTugasTransferFilterBinding.inflate(inflater, container, false);
    TugasTransferFilterFragmentArgs args = TugasTransferFilterFragmentArgs.fromBundle(
        getArguments());
    vm = new ViewModelProvider(this).get(TugasTransferFilterVM.class);
    vm.setFilterField(args.getFilterField());
    vm.setGroup(args.getIsGrouping());
    return binding.getRoot();
  }

  @Override
  public void onResume() {
    super.onResume();
    setFormMode();
    setFormState();
    vm.processLoadFilterData();
    setFieldCompany();
    setFieldOutlet();
    setFieldTransferType();
    setFieldRekeningSumber();
    setFieldTrxNo();
    setFieldStartDate();
    setFieldEndDate();
    setFieldPendingTaskStatus();
    setFieldBatch();
    setFieldTransferStatus();
    setFieldJenisTransaksi();
    setButton();
  }

  private void setFormMode() {
    binding.tvCompany.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tilCompany.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tvJenisTransfer.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tilJenisTransfer.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tvRekeningSumber.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tilRekeningSumber.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tvStartDate.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tilStartDate.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tvEndDate.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tilEndDate.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tvPendingTaskStatus.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.swPendingTaskStatus.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tvBatch.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tilBatch.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tvTransferStatus.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tilTransferStatus.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tvJenisTransaksi.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);
    binding.tilJenisTransaksi.setVisibility(vm.isGroup() ? View.VISIBLE : View.GONE);

    binding.tvOutlet.setVisibility(vm.isGroup() ? View.GONE : View.VISIBLE);
    binding.tilOutlet.setVisibility(vm.isGroup() ? View.GONE : View.VISIBLE);
  }

  private void setFormState() {
    vm.getFormState().observe(getViewLifecycleOwner(), state -> {
      if (FormState.State.LOADING.equals(state.getState())) {
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
      } else if (FormState.State.READY.equals(state.getState())) {
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
      } else if (FormState.State.ERROR.equals(state.getState())) {
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), state.getMessage(), Toast.LENGTH_LONG).show();
      }
    });
  }

  private void setFieldCompany() {
    vm.getCompanyKvmList().observe(getViewLifecycleOwner(), dataList -> {
      companyAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spCompany.setAdapter(companyAdapter);
      binding.spCompany.setFocusable(false);
      companyAdapter.addAll(dataList);
      companyAdapter.notifyDataSetChanged();

      int pos = companyAdapter.getPosition(vm.getCompanyKvm());
      binding.spCompany.setSelection(pos);

      binding.spCompany.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
            int i, long l) {
          KeyValueModel<UUID, String> kvm = companyAdapter.getItem(i);
          vm.setCompanyFilter(kvm.getKey(), kvm.getValue());
        }

        @Override
        public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
          //TODO belum ada implementasi
        }
      });
    });
  }

  private void setFieldOutlet() {
    binding.etOutlet.setText(vm.getFilterField().getOutletName());
    binding.etOutlet.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        vm.setOutletFilter(s.toString());
      }
    });
  }

  private void setFieldTransferType() {
    vm.getTransferTypeKvmList().observe(getViewLifecycleOwner(), dataList -> {
      jenisTransferAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spJenisTransfer.setAdapter(jenisTransferAdapter);
      binding.spJenisTransfer.setFocusable(false);
      jenisTransferAdapter.addAll(dataList);
      jenisTransferAdapter.notifyDataSetChanged();

      int pos = jenisTransferAdapter.getPosition(vm.getTranfeTyperKvm());
      binding.spJenisTransfer.setSelection(pos);

      binding.spJenisTransfer.setOnItemSelectedListener(
          new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
                @Nullable View view,
                int i, long l) {
              KeyValueModel<String, String> kvm = jenisTransferAdapter.getItem(i);
              vm.setJenisTransferFilter(kvm.getKey(), kvm.getValue());
            }

            @Override
            public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
              //TODO belum ada implementasi
            }
          });
    });
  }

  private void setFieldRekeningSumber() {
    vm.getRekenginSumberKvmList().observe(getViewLifecycleOwner(), dataList -> {
      if (rekeningSumberAdapter == null) {
        rekeningSumberAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
        binding.spRekeningSumber.setAdapter(rekeningSumberAdapter);
        binding.spRekeningSumber.setFocusable(false);
        rekeningSumberAdapter.addAll(dataList);
        rekeningSumberAdapter.notifyDataSetChanged();

        int pos = rekeningSumberAdapter.getPosition(vm.getRekeningSumberKvm());
        binding.spRekeningSumber.setSelection(pos);
      } else {
        rekeningSumberAdapter.clear();
        rekeningSumberAdapter.addAll(dataList);
        rekeningSumberAdapter.notifyDataSetChanged();

        int pos = rekeningSumberAdapter.getPosition(vm.getRekeningSumberKvm());
        binding.spRekeningSumber.setSelection(pos);
      }

      binding.spRekeningSumber.setOnItemSelectedListener(
          new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
                @Nullable View view,
                int i, long l) {
              KeyValueModel<UUID, String> kvm = rekeningSumberAdapter.getItem(i);
              vm.setRekeningSumberFilter(kvm.getKey(), kvm.getValue());
            }

            @Override
            public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
              //TODO belum ada implementasi
            }
          });
    });
  }

  private void setFieldTrxNo(){
    String noTrx = vm.getFilterField().getTransactionNo();
    binding.etNoTrx.setText(noTrx);
    binding.etNoTrx.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        vm.setNoTrx(s.toString());
      }
    });
  }

  private void setFieldStartDate(){
    Date startDate = vm.getFilterField().getStartDateFrom();
    if (startDate != null) {
      String startDateString = Formatter.SDF_dd_MM_yyyy.format(startDate);
      binding.etStartDate.setText(startDateString);
    }

    binding.etStartDate.setOnClickListener(v->{
      Date date = new Date();
      if (binding.etStartDate.getText() != null
          && !binding.etStartDate.getText().toString().isEmpty()) {
        String dateString = binding.etStartDate.getText().toString();
        date = Utility.getDateFromString(TAG, dateString);
      }
      Calendar cal = Calendar.getInstance();
      cal.setTime(date);
      int day = cal.get(Calendar.DAY_OF_MONTH);
      int month = cal.get(Calendar.MONTH);
      int year = cal.get(Calendar.YEAR);

      DatePickerDialog dateDialog = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              Calendar cal = Calendar.getInstance();
              cal.set(year,month,dayOfMonth);
              cal.set(Calendar.HOUR_OF_DAY, 0);
              cal.set(Calendar.MINUTE, 0);
              cal.set(Calendar.SECOND, 0);
              cal.set(Calendar.MILLISECOND, 0);
              vm.setStartDate(cal.getTime());
              binding.etStartDate.setText(Formatter.SDF_dd_MM_yyyy.format(cal.getTime()));
            }
          }, year, month,day);
      dateDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          vm.setStartDate(null);
          binding.etStartDate.setText(null);
        }
      });
      dateDialog.show();
    });
  }

  private void setFieldEndDate(){
    Date endDate = vm.getFilterField().getStartDateTo();
    if (endDate != null) {
      String endDateString = Formatter.SDF_dd_MM_yyyy.format(endDate);
      binding.etEndDate.setText(endDateString);
    }

    binding.etEndDate.setOnClickListener(v->{
      Date date = new Date();
      if (binding.etEndDate.getText() != null
          && !binding.etEndDate.getText().toString().isEmpty()) {
        String dateString = binding.etEndDate.getText().toString();
        date = Utility.getDateFromString(TAG, dateString);
      }
      Calendar cal = Calendar.getInstance();
      cal.setTime(date);
      int day = cal.get(Calendar.DAY_OF_MONTH);
      int month = cal.get(Calendar.MONTH);
      int year = cal.get(Calendar.YEAR);

      DatePickerDialog dateDialog = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              Calendar cal = Calendar.getInstance();
              cal.set(year,month,dayOfMonth);
              cal.set(Calendar.HOUR_OF_DAY, 0);
              cal.set(Calendar.MINUTE, 0);
              cal.set(Calendar.SECOND, 0);
              cal.set(Calendar.MILLISECOND, 0);
              vm.setEndDate(cal.getTime());
              binding.etEndDate.setText(Formatter.SDF_dd_MM_yyyy.format(cal.getTime()));
            }
          }, year, month,day);
      dateDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          vm.setEndDate(null);
          binding.etEndDate.setText(null);
        }
      });
      dateDialog.show();
    });
  }

  private void setFieldPendingTaskStatus() {
    boolean isTrue = vm.getFilterField().getPendingTask().booleanValue();
    binding.swPendingTaskStatus.setChecked(isTrue);
    binding.swPendingTaskStatus.setOnCheckedChangeListener(
        new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            vm.setPendingTaskStatusFilter(isChecked);
          }
        });
  }

  private void setFieldBatch() {
    vm.getBatchKvmList().observe(getViewLifecycleOwner(), dataList -> {
      batchAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spBatch.setAdapter(batchAdapter);
      binding.spBatch.setFocusable(false);
      batchAdapter.addAll(dataList);
      batchAdapter.notifyDataSetChanged();

      int pos = batchAdapter.getPosition(vm.getBatchKvm());
      binding.spBatch.setSelection(pos);

      binding.spBatch.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
            int i, long l) {
          KeyValueModel<String, String> kvm = batchAdapter.getItem(i);
          vm.setBatchFilter(kvm.getKey(), kvm.getValue());
        }

        @Override
        public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
          //TODO belum ada implementasi
        }
      });
    });
  }

  private void setFieldTransferStatus() {
    vm.getTransferStatusKvmList().observe(getViewLifecycleOwner(), dataList -> {
      transferStatusAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spTransferStatus.setAdapter(transferStatusAdapter);
      binding.spTransferStatus.setFocusable(false);
      transferStatusAdapter.addAll(dataList);
      transferStatusAdapter.notifyDataSetChanged();

      int pos = transferStatusAdapter.getPosition(vm.getTransferStatusKvm());
      binding.spTransferStatus.setSelection(pos);

      binding.spTransferStatus.setOnItemSelectedListener(
          new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
                @Nullable View view,
                int i, long l) {
              KeyValueModel<String, String> kvm = transferStatusAdapter.getItem(i);
              vm.setTransferStatusFilter(kvm.getKey(), kvm.getValue());
            }

            @Override
            public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
              //TODO belum ada implementasi
            }
          });
    });
  }

  private void setFieldJenisTransaksi() {
    vm.getJenisTransaksiKvmList().observe(getViewLifecycleOwner(), dataList -> {
      jenisTransaksiAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spJenisTransaksi.setAdapter(jenisTransaksiAdapter);
      binding.spJenisTransaksi.setFocusable(false);
      jenisTransaksiAdapter.addAll(dataList);
      jenisTransaksiAdapter.notifyDataSetChanged();

      int pos = jenisTransaksiAdapter.getPosition(vm.getJenisTransaksiKvm());
      binding.spJenisTransaksi.setSelection(pos);

      binding.spJenisTransaksi.setOnItemSelectedListener(
          new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
                @Nullable View view,
                int i, long l) {
              KeyValueModel<String, String> kvm = jenisTransaksiAdapter.getItem(i);
              vm.setJenisTransaksiFilter(kvm.getKey(), kvm.getValue());
            }

            @Override
            public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
              //TODO belum ada implementasi
            }
          });
    });
  }

  private void setButton() {
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String key = vm.isGroup() ?
            TUGAS_TRANSFER_FILTER_FIELD_GROUPING : TUGAS_TRANSFER_FILTER_FIELD_DETAIL;
        HyperLog.d(TAG, "key ->" + key + "<-");
        NavController nc = NavHostFragment.findNavController(
            TugasTransferFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(key, vm.getFilterField());
        nc.popBackStack();
      }
    });

    binding.btnClearAll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        TugasTransferBrowseAndroidFilter existingFilter = vm.getFilterField();
        TugasTransferBrowseAndroidFilter defFilter = new TugasTransferBrowseAndroidFilter();
        if (vm.isGroup()) {
          defFilter.setCompanyId(existingFilter.getCompanyId());
          defFilter.setCompanyName(existingFilter.getCompanyName());
        } else {
          defFilter = existingFilter;
          defFilter.setOutletName(null);
        }
        defFilter.setPendingTask(true);
        String key = vm.isGroup() ?
            TUGAS_TRANSFER_FILTER_FIELD_GROUPING : TUGAS_TRANSFER_FILTER_FIELD_DETAIL;
        NavController nc = NavHostFragment.findNavController(
            TugasTransferFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(key, defFilter);
        nc.popBackStack();
      }
    });
  }

}