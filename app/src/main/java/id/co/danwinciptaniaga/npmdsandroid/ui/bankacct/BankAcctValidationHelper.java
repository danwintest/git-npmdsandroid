package id.co.danwinciptaniaga.npmdsandroid.ui.bankacct;

import android.content.Context;
import android.text.TextUtils;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.data.ConstraintViolation;

public class BankAcctValidationHelper {
  public static void validateOtherBank(Context context, String otherBankName)
      throws ConstraintViolation {
    if (TextUtils.isEmpty(otherBankName))
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.other_bank_name)));
  }

  public static void validateAccountName(Context context, String accountName)
      throws ConstraintViolation {
    if (TextUtils.isEmpty(accountName))
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.account_name)));
  }

  public static void validateAccountNo(Context context, String accountNo)
      throws ConstraintViolation {
    if (TextUtils.isEmpty(accountNo))
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.account_no)));

  }
}
