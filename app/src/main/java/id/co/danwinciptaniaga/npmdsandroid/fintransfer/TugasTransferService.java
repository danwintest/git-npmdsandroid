package id.co.danwinciptaniaga.npmdsandroid.fintransfer;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.BankData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.BalanceInfo;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseSort;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferDetailBrowseResponse;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferDetailBrowseSort;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferFilterResponse;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferHeaderBrowseResponse;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferProcessResponse;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferWfResponse;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface TugasTransferService {
  @GET("rest/s/TugasTransfer/getHeaderList")
  ListenableFuture<TugasTransferHeaderBrowseResponse> getHeaderList(@Query("page") Integer pages,
      @Query("pageSize") Integer pageSize, @Query("filter")
      TugasTransferBrowseAndroidFilter filterString, @Query("sort") TugasTransferBrowseSort sortString);

  @GET("rest/s/TugasTransfer/getDetailList")
  ListenableFuture<TugasTransferDetailBrowseResponse> getDetailList(@Query("page") Integer pages,
      @Query("pageSize") Integer pageSize, @Query("filter")
      TugasTransferBrowseAndroidFilter filterString, @Query("sort") TugasTransferDetailBrowseSort sortString);

  @GET("rest/s/TugasTransfer/getDefaultCompanyFilter")
  ListenableFuture<TugasTransferFilterResponse> getDefaultCompanyFilter();

  @GET("rest/s/TugasTransfer/getFilterField")
  ListenableFuture<TugasTransferFilterResponse> getFilterField(@Query("companyId") UUID companyId,
      @Query("isReloadSourceAcc") Boolean isReloadSourceAcc);

  @GET("rest/s/TugasTransfer/getWfFormResponse")
  ListenableFuture<TugasTransferWfResponse> getWfFormResponse(@Query("companyId") UUID companyId,
      @Query("isAuto") Boolean isAuto);

  @GET("rest/s/TugasTransfer/Balance/bankList")
  ListenableFuture<List<BankData>> getBankDataListByCompanyId(@Query("companyId") UUID companyId);

  @GET("rest/s/TugasTransfer/Balance/accountList")
  ListenableFuture<List<BankAccountData>> getAccountListByCompanyIdAndBankId(
      @Query("companyId") UUID companyId, @Query("bankId") UUID bankId);

  @GET("rest/s/TugasTransfer/accountBalanceList")
  ListenableFuture<BalanceInfo> getAccountBalanceList(
      @Query("companyId") UUID companyId, @Query("bankId") UUID bankId, @Query("accId") UUID accId);

  @Multipart
  @POST("rest/s/TugasTransfer/processApproveTransferOtomatis")
  ListenableFuture<TugasTransferProcessResponse> processApproveTransferOtomatis(
      @Query("dataList") String dataString, @Query("comment") String comment,
      @Part("transferDate") LocalDate transferDate, @Query("sourceAccId") UUID sourceAccId);

  @Multipart
  @POST("rest/s/TugasTransfer/processApproveTransferManual")
  ListenableFuture<TugasTransferProcessResponse> processApproveTransferManual(
      @Query("dataList") String dataString, @Query("comment") String comment,
      @Part("transferDate") LocalDate transferDate, @Query("sourceAccId") UUID sourceAccId);

  @POST("rest/s/TugasTransfer/processReturn")
  ListenableFuture<TugasTransferProcessResponse> processReturn(
      @Query("dataList") String dataString, @Query("comment") String comment);

  @POST("rest/s/TugasTransfer/processReject")
  ListenableFuture<TugasTransferProcessResponse> processReject(
      @Query("dataList") String dataString, @Query("comment") String comment);

  @GET("rest/s/TugasTransfer/processGenerateOTP")
  ListenableFuture<Boolean> processGenerateOTP();

  @POST("rest/s/TugasTransfer/processApprove")
  ListenableFuture<TugasTransferProcessResponse> processApprove(
      @Query("dataList") String dataString, @Query("comment") String comment,
      @Query("otp") String otp, @Query("isManual") Boolean isManual);
}
