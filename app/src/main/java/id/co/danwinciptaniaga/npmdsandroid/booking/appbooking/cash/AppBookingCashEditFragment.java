package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingVM;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;

public class AppBookingCashEditFragment extends AppBookingCashNewFragment {
  private final String TAG = AppBookingCashEditFragment.class.getSimpleName();
  private BookingBrowseViewModel mBrowseVM;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    Abstract_AppBookingVM vm = new ViewModelProvider(this).get(AppBookingCashVM.class);

    mBrowseVM = new ViewModelProvider(requireActivity()).get(BookingBrowseViewModel.class);
    AppBookingCashEditFragmentArgs args = AppBookingCashEditFragmentArgs.fromBundle(getArguments());

    boolean isHasCreatePermission = args.getIsHasCreatePermission();
    setOnCreateView(isHasCreatePermission, inflater, container, this, TAG, vm);
    getCashVm().setField_BookingId(args.getBookingId());
    return getBinding().getRoot();
  }

  @Override
  protected void processLoadData() {
    getCashVm().processLoadAppBookingDataByBooking(getCashVm().getField_BookingId(), true, false);
  }

  @Override
  protected void setStandartButtonListener(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode scTransfer) {
    getBinding().btnSimpan.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        switch (smCash) {
        case KASIR_SUBMITTED_EDIT_APPBOOKING:
          getCashVm().processSaveDraftEdit(true, true);
          break;
        default:
          break;
        }
      }
    });
    getBinding().btnSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        switch (smCash) {
        case KASIR_SUBMITTED_EDIT_APPBOOKING:
          getCashVm().processWfSubmitEdit(true);
          break;
        default:
          break;
        }
      }
    });

    getBinding().btnWfHistory.setOnClickListener(v -> {
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      AppBookingCashEditFragmentDirections.ActionWfHistory dir = AppBookingCashEditFragmentDirections
          .actionWfHistory(WfType.APP_BOOKING,
              getCashVm().getField_AppBookingId());
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });
  }

  @Override
  protected void setReadyFormState(AppBookingCashScreenMode smCash, AppBookingTransferScreenMode smTrans, String message,
      Animation inAnim, Animation outAnim, boolean isFromAction) {
    if (isFromAction) {
      mBrowseVM.setRefreshList(true);
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      navController.navigateUp();
      return;
    } else
      super.setReadyFormState(smCash, smTrans, message, inAnim, outAnim, isFromAction);
  }

  @Override
  protected void setMode_generalField(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer, String message,
      Animation inAnimation, Animation outAnimation) {
    super.setMode_generalField(smCash, smTransfer, message, inAnimation, outAnimation);
  }
}
