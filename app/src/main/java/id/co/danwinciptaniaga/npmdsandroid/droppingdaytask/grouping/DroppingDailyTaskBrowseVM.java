package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import java.util.LinkedHashSet;
import java.util.Set;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.LoadState;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseHeaderData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskFilterResponse;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.TugasDroppingHarianBrowseSort;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.GetDroppingDailyTaskFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class DroppingDailyTaskBrowseVM extends AndroidViewModel
    implements GetDroppingDailyTaskFilterUC.Listener {
  private final String TAG = DroppingDailyTaskBrowseVM.class.getSimpleName();
  private LiveData<PagingData<DroppingDailyTaskBrowseHeaderData>> headerList;
  private final GetDroppingDailyTaskBrowseHeaderUC ucHeader;
  private final GetDroppingDailyTaskFilterUC ucFilter;
  private final AppExecutors appExecutors;
  private final Pager<Integer, DroppingDailyTaskBrowseHeaderData> pagerHeader;
  private MutableLiveData<LoadState> loadStateHeader = new MutableLiveData<>();
  private MutableLiveData<Boolean> refreshHeaderList = new SingleLiveEvent<>();
  private MutableLiveData<Resource<String>> actionEventHeader = new SingleLiveEvent<>();
  private MutableLiveData<DroppingDailyTaskBrowseFilter> filterField = new MutableLiveData<>();
  private Set<SortOrder> sortFields = new LinkedHashSet<>();

  @ViewModelInject
  public DroppingDailyTaskBrowseVM(@NonNull Application app, AppExecutors appExecutors,
      GetDroppingDailyTaskBrowseHeaderUC ucHeader,
      GetDroppingDailyTaskFilterUC ucFilter) {
    super(app);
    this.ucHeader = ucHeader;
    this.ucFilter = ucFilter;
    this.appExecutors = appExecutors;
    this.ucFilter.registerListener(this);
    this.sortFields.add(
        TugasDroppingHarianBrowseSort.sortBy(TugasDroppingHarianBrowseSort.Field.BANK_NAME,
            SortOrder.Direction.ASC));

    pagerHeader = new Pager<Integer, DroppingDailyTaskBrowseHeaderData>(
        new PagingConfig(Utility.PAGE_SIZE),
        () -> new DroppingDailyTaskBrowseHeaderPagingSource(this.ucHeader,
            this.appExecutors.networkIO()));
    CoroutineScope vmScopeHeader = ViewModelKt.getViewModelScope(this);

    headerList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pagerHeader), vmScopeHeader);
  }

  public LiveData<PagingData<DroppingDailyTaskBrowseHeaderData>> getHeaderList() {
    String fn = "getHeaderList() ";
    HyperLog.d(TAG, fn + "terpanggil ");
    boolean processSetHeaderList = false;

    boolean isMandatoryField = false;
    if (getFilterField().getValue() != null) {
      if (getFilterField().getValue().getCompanyId() != null) {
        isMandatoryField = true;
      }
    }


    if(!isMandatoryField){
      String msg = "defaultFilter belum ada, maka panggil service untuk mendapatkan defaultFilterCompanyId";
      HyperLog.d(TAG, fn + msg);
      ucFilter.getDefaultCompanyFilter();
      processSetHeaderList = true;
    } else {
      String msg = "defaultFilter sudah ada, maka panggil service untuk loadDataHeader";
      HyperLog.d(TAG, fn + msg);
      processSetHeaderList = headerList == null ? true : false;
    }

    if (processSetHeaderList) {
      CoroutineScope vmScope = ViewModelKt.getViewModelScope(this);
      return headerList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pagerHeader),
          vmScope);
    } else {
      return headerList;
    }
  }

  public MutableLiveData<LoadState> getLoadStateHeader() {
    return loadStateHeader;
  }

  public void setLoadStateHeader(LoadState loadStateHeader) {
    this.loadStateHeader.postValue(loadStateHeader);
  }

  public LiveData<Boolean> getRefreshHeaderList() {
    return refreshHeaderList;
  }


  public LiveData<Resource<String>> getActionEventHeader() {
    return actionEventHeader;
  }


  public void setRefreshHeaderList(Boolean refreshHeaderList) {
    this.refreshHeaderList.postValue(refreshHeaderList);
  }

  public MutableLiveData<DroppingDailyTaskBrowseFilter> getFilterField() {
    return filterField;
  }

  public void loadHeaderListWithFilter(DroppingDailyTaskBrowseFilter filterField){
    this.filterField.postValue(filterField);
    ucHeader.setFilter(filterField);
    refreshHeaderList.postValue(true);
  }

  public Set<SortOrder> getSortFields() {
    return sortFields;
  }

  public void setSortFields(Set<SortOrder> sortFields) {
    this.sortFields.clear();
    if (sortFields != null) {
      this.sortFields.addAll(sortFields);
    }
    ucHeader.setSort(sortFields);
    refreshHeaderList.postValue(true);
  }

  @Override
  public void onProcessStarted(Resource<DroppingDailyTaskFilterResponse> loading) {
    //TODO BELUM ADA IMPLEMENTASI
//    setLoadStateHeader(AppBookingLoadUC.LoadResult);
    HyperLog.d(TAG, "belum ada implementasi");
  }

  @Override
  public void onProcessSuccess(Resource<DroppingDailyTaskFilterResponse> res) {
    boolean isMandatoryField = false;
    if (getFilterField().getValue() != null) {
      if (getFilterField().getValue().getCompanyId() != null) {
        isMandatoryField = true;
      }
    }

    if (isMandatoryField)
      return;
    else {
      DroppingDailyTaskBrowseFilter filterData = new DroppingDailyTaskBrowseFilter();
      filterData.setCompanyId(res.getData().getDefaultFilterCompanyId());
      filterData.setCompanyName(res.getData().getDefaultFilterCompanyName());
      HyperLog.d(TAG,"defaultFilterCompanyId yang di dapatkan dari webService["+filterData.getCompanyId()+"]");
      HyperLog.d(TAG,"defaultFilterCompanyName yang di dapatkan dari webService["+filterData.getCompanyName()+"]");
      loadHeaderListWithFilter(filterData);
    }
  }

  @Override
  public void onProcessFailure(Resource<DroppingDailyTaskFilterResponse> res) {
    //TODO BELUM ADA IMPLEMENTASI
    HyperLog.d(TAG, "belum ada implementasi");
  }
}
