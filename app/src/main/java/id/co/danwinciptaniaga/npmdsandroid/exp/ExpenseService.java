package id.co.danwinciptaniaga.npmdsandroid.exp;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseSort;
import id.co.danwinciptaniaga.npmds.data.booking.BookingListResponse;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseSort;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseDetailResponse;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseFilterResponse;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseItemData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseListResponse;
import id.co.danwinciptaniaga.npmds.data.expense.NewExpenseResponse;
import id.co.danwinciptaniaga.npmds.data.wf.WfHistoryResponse;
import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ExpenseService {
  @GET("rest/s/expense")
  ListenableFuture<ExpenseListResponse> getExpenseList(@Query("page") Integer page,
      @Query("pageSize") Integer pageSize,
      @Query("filter") ExpenseBrowseFilter filter,
      @Query("sort") ExpenseBrowseSort sort);

  @GET("rest/s/expense/new")
  ListenableFuture<NewExpenseResponse> getNewExpense();

  @GET("rest/s/expenseItem")
  ListenableFuture<List<ExpenseItemData>> getExpenseItemList();

  @GET("rest/s/expense/booking")
  ListenableFuture<BookingListResponse> getBookingMediatorList(@Query("outletId") UUID outletId,
      @Query("page") Integer page, @Query("pageSize") Integer pageSize,
      @Query("filter") Object filter, @Query("sort") BookingBrowseSort sort);

  @Multipart
  @POST("rest/s/expense/saveDraft")
  ListenableFuture<ExpenseData> saveExpenseDraft(@Part("id") UUID expenseId,
      @Part("outletId") UUID outletId, @Part("statementDate") LocalDate statementDate,
      @Part("expenseDate") LocalDate expenseDate, @Part("itemId") UUID expenseItemId,
      @Part("bookingId") UUID bookingId, @Part("amount") BigDecimal amount,
      @Part("quantity") Integer quantity,
      @Part("description") String description,
      @Part MultipartBody.Part[] existingAttachmentIds,
      @Part MultipartBody.Part[] newAttachments, @Part("checkTs") Date checkTs);

  @Multipart
  @POST("rest/s/expense/submit")
  ListenableFuture<ExpenseData> submitExpense(@Part("id") UUID expenseId,
      @Part("outletId") UUID outletId, @Part("statementDate") LocalDate statementDate,
      @Part("expenseDate") LocalDate expenseDate, @Part("itemId") UUID expenseItemId,
      @Part("bookingId") UUID bookingId, @Part("amount") BigDecimal amount,
      @Part("quantity") Integer quantity,
      @Part("description") String description,
      @Part MultipartBody.Part[] existingAttachmentIds,
      @Part MultipartBody.Part[] newAttachments, @Part("checkTs") Date checkTs);

  @FormUrlEncoded
  @POST("rest/s/expense/decide")
    // untuk Expense, method ini generic, karena hanya ada 1 level supervisor. comment mandatory kalau reject/return saja.
    // untuk module lain, method ini boleh per WF Role, kalau berbeda parameter
  ListenableFuture<ExpenseData> decide(@Field("procTaskId") UUID procTaskId,
      @Field("expenseId") UUID expenseId, @Field("outcome") String outcome,
      @Field("comment") String comment, @Field("checkTs") Date checkTs);

  @POST("rest/s/expense/deleteExpense")
  ListenableFuture<HashMap<String, List<String>>> deleteExpense(@Query("ids") List<UUID> ids);

  @POST("rest/s/expense/rejectExpense")
  ListenableFuture<HashMap<String, List<String>>> rejectExpense(@Query("ids") List<UUID> ids);

  @GET("rest/s/expense/{id}")
  ListenableFuture<ExpenseDetailResponse> loadExpense(@Path("id") UUID expenseId);

  @Multipart
  @POST("rest/s/expense/submitRevision")
  ListenableFuture<ExpenseData> submitExpenseRevision(@Part("id") UUID expenseId,
      @Part("procTaskId") UUID procTaskId,
      @Part("comment") String comment,
      @Part("outletId") UUID outletId,
      @Part("statementDate") LocalDate statementDate,
      @Part("expenseDate") LocalDate expenseDate,
      @Part("itemId") UUID expenseItemId,
      @Part("bookingId") UUID bookingId,
      @Part("amount") BigDecimal amount,
      @Part("quantity") Integer quantity,
      @Part("description") String description,
      @Part MultipartBody.Part[] existingAttachmentIds,
      @Part MultipartBody.Part[] newAttachments, @Part("checkTs") Date checkTs);

  @GET("rest/s/expense/{id}/wfHistory")
  ListenableFuture<WfHistoryResponse> getWfHistory(@Path("id") UUID expenseId);

  @GET("rest/s/expense/getFilterField")
  ListenableFuture<ExpenseFilterResponse> getFilterField();
}
