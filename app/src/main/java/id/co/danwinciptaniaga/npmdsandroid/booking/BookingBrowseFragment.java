package id.co.danwinciptaniaga.npmdsandroid.booking;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.PermissionHelper;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseVM;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentBookingBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import kotlin.Unit;

@AndroidEntryPoint
public class BookingBrowseFragment extends ProtectedFragment {
  private static final String TAG = BookingBrowseFragment.class.getSimpleName();

  private static final String ARG_COLUMN_COUNT = "column-count";
  private int mColumnCount = 1;
  private BookingBrowseViewModel mViewModel;
  private FragmentBookingBrowseBinding binding;
  private BookingListAdapter adapter;
  private ConcatAdapter concatAdapter;
  private RecyclerView recyclerView;
  private Dialog mMenuDialog;
  private TextView mPengajuanEdit;
  private TextView mPengajuanBatal;
  private boolean isPermissionCreate = false;

  public BookingBrowseFragment() {
  }

  public static BookingBrowseFragment newInstance(int columnCount) {
    BookingBrowseFragment fragment = new BookingBrowseFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_COLUMN_COUNT, columnCount);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    if (getArguments() != null) {
      mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
    }
    HyperLog.d(TAG, "set bookingBrowseViewModel (START)");
    mViewModel = new ViewModelProvider(requireActivity()).get(BookingBrowseViewModel.class);
    HyperLog.d(TAG, "set bookingBrowseViewModel (FINISH):->" + mViewModel);
  }

  private View.OnClickListener reloadFragment = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      adapter.refresh();
      binding.swipeRefresh.setRefreshing(false);
    }
  };

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentBookingBrowseBinding.inflate(inflater, container, false);
    // setItemDialog();
    setAdapterAndRecyler();
    setLoadAdapter();
    setFilterObserver();
    setSortObserver();

    binding.swipeRefresh.setOnRefreshListener(() -> {
      adapter.refresh();
      binding.swipeRefresh.setRefreshing(false);
    });

    mViewModel.getRefreshList().observe(getViewLifecycleOwner(), o -> {
      if (o != null && o) {
        adapter.refresh();
      }
    });

    return binding.getRoot();
  }

  private void setLoadAdapter() {
    View.OnClickListener retryCallback = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        adapter.retry();
      }
    };

    concatAdapter = adapter.withLoadStateHeaderAndFooter(
        new BookingListLoadStateAdapter(retryCallback),
        new BookingListLoadStateAdapter(retryCallback));

    adapter.addLoadStateListener((combinedLoadStates) -> {
      // listener ini mengupdate LoadState di ViewModel
      mViewModel.setBookingLoadState(combinedLoadStates.getRefresh());
      return Unit.INSTANCE;
    });
    // lalu gunakan LiveData observer untuk mengupdate view
    mViewModel.getBookingLoadState().observe(getViewLifecycleOwner(), state -> {
      if (state instanceof LoadState.Loading) {
        binding.pageNsi.getRoot().setVisibility(View.VISIBLE);
        binding.pageNsi.progressBar.setVisibility(View.VISIBLE);
        binding.pageNsi.retryButton.setVisibility(View.GONE);
        binding.pageNsi.errorMsg.setVisibility(View.GONE);
        binding.pageNsi.errorMsg.setVisibility(View.GONE);
      } else if (state instanceof LoadState.Error) {
        binding.pageNsi.getRoot().setVisibility(View.VISIBLE);
        binding.pageNsi.progressBar.setVisibility(View.GONE);
        binding.pageNsi.retryButton.setVisibility(View.VISIBLE);
        binding.pageNsi.retryButton.setOnClickListener(reloadFragment);
        binding.pageNsi.errorMsg.setVisibility(View.VISIBLE);
        binding.pageNsi.errorMsg.setText(((LoadState.Error) state).getError().getMessage());
        binding.swipeRefresh.setVisibility(View.GONE);
      } else if (state instanceof LoadState.NotLoading) {
        binding.pageNsi.getRoot().setVisibility(View.GONE);
        binding.pageNsi.progressBar.setVisibility(View.GONE);
        binding.pageNsi.retryButton.setVisibility(View.GONE);
        binding.pageNsi.errorMsg.setVisibility(View.GONE);
        binding.swipeRefresh.setVisibility(View.VISIBLE);
      }
    });
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    if (status.getStatus() == Status.LOADING) {
      binding.tvStatus.setVisibility(View.VISIBLE);
      binding.tvStatus.setText(R.string.msg_please_wait);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.tvStatus.setText(status.getMessage());
      binding.tvStatus.setVisibility(View.GONE);

      Resource<List<PermissionInfo>> permissions = basemodel.getPermissionsLd().getValue();
      isPermissionCreate = PermissionHelper.hasPermission(permissions.getData(),
          PermissionHelper.PERMISSION_TYPE_ENTITY_OP, AppBookingBrowseVM.PERMISSION_CREATE);
      recyclerView.setAdapter(concatAdapter);
    }
  }

  private void setAdapterAndRecyler() {
    View list = binding.bookinglist;
    Resource<List<PermissionInfo>> permissions = basemodel.getPermissionsLd().getValue();
    boolean isPermissionCreate = PermissionHelper.hasPermission(permissions.getData(),
        PermissionHelper.PERMISSION_TYPE_ENTITY_OP,
        AppBookingBrowseVM.PERMISSION_CREATE);

    if (list instanceof RecyclerView) {
      Context ctx = list.getContext();
      recyclerView = (RecyclerView) list;
      if (mColumnCount <= 1) {
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
      } else {
        recyclerView.setLayoutManager(new GridLayoutManager(ctx, mColumnCount));
      }
    }
    adapter = new BookingListAdapter(
        new BookingListAdapter.MyBookingDataRecyclerViewAdapterListener() {
//          @Override
//          public void onItemMenuClicked(View v, BookingData bookingData, int position) {
//            if (!bookingData.isActiveWfBooking()) {
//              setItemDialog(bookingData);
//              mMenuDialog.show();
//            }
//          }

          @Override
          public void onItemCliked(View v, BookingData bookingData, int posistion) {
            NavController navController = Navigation.findNavController(v);
            if (bookingData.getBookingTypeId().equals("C")) {
              BookingBrowseFragmentDirections.ActionBookingViewDetailFragment dir = BookingBrowseFragmentDirections
                  .actionBookingViewDetailFragment(isPermissionCreate);
              dir.setBookingId(bookingData.getId());
              navController.navigate(dir, NavHelper.animParentToChild().build());
            } else if (bookingData.getBookingTypeId().equals("T")) {
              BookingBrowseFragmentDirections.ActionBookingViewDetailTransferFragment dir = BookingBrowseFragmentDirections
                  .actionBookingViewDetailTransferFragment(isPermissionCreate);
              dir.setBookingId(bookingData.getId());
              navController.navigate(dir, NavHelper.animParentToChild().build());
            } else {
              Snackbar
                  .make(binding.getRoot(), "Jenis Pencairan Tidak Di Ketahui", Snackbar.LENGTH_LONG)
                  .show();
            }
          }

          @Override
          public void onItemMenuEdit(View v, BookingData bd, int position) {
            if (bd.isActiveWfBooking())
              return;

            NavController navController = Navigation.findNavController(binding.getRoot());

            if (bd.getBookingTypeId().equals("C")) {
              BookingBrowseFragmentDirections.ActionBookingPengajuanEditCash dir = BookingBrowseFragmentDirections
                  .actionBookingPengajuanEditCash(bd.getId(), isPermissionCreate);
              navController.navigate(dir, NavHelper.animParentToChild().build());
            } else {
              BookingBrowseFragmentDirections.ActionBookingPengajuanEditTransfer dir = BookingBrowseFragmentDirections
                  .actionBookingPengajuanEditTransfer(bd.getId(), isPermissionCreate);
              navController.navigate(dir, NavHelper.animParentToChild().build());
            }
          }

          @Override
          public void onItemMenuDelete(View v, BookingData bd, int position) {
            if (bd.isActiveWfBooking())
              return;

            NavController navController = Navigation.findNavController(binding.getRoot());
            if (bd.getBookingTypeId().equals("C")) {
              BookingBrowseFragmentDirections.ActionBookingPengajuanBatalCash dir = BookingBrowseFragmentDirections
                  .actionBookingPengajuanBatalCash(bd.getId(), isPermissionCreate);
              navController.navigate(dir, NavHelper.animParentToChild().build());
            } else {
              BookingBrowseFragmentDirections.ActionBookingPengajuanBatalTransfer dir = BookingBrowseFragmentDirections
                  .actionBookingPengajuanBatalTransfer(bd.getId(), isPermissionCreate);
              navController.navigate(dir, NavHelper.animParentToChild().build());
            }
          }
        }, getActivity().getApplicationContext(), isPermissionCreate);
    recyclerView.setAdapter(adapter);
    mViewModel.getBookingList().observe(getViewLifecycleOwner(), pagingData -> {
      adapter.submitData(getLifecycle(), pagingData);
    });
  }

//  private void setItemDialog(BookingData bd) {
//    if (mMenuDialog == null) {
//      mMenuDialog = new Dialog(getActivity());
//      mMenuDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//      mMenuDialog.setContentView(R.layout.bookinglist_item_custom_menu);
//      mMenuDialog.setCancelable(true);
//    }
//    if (mPengajuanEdit == null)
//      mPengajuanEdit = mMenuDialog.findViewById(R.id.tvBkItemMenuPengajuanEdit);
//    if (mPengajuanBatal == null)
//      mPengajuanBatal = mMenuDialog.findViewById(R.id.tvBkItemMenuPengajuanBatal);
//
//    mPengajuanEdit.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        NavController navController = Navigation.findNavController(binding.getRoot());
//
//        if (bd.getBookingTypeId().equals("C")) {
//          BookingBrowseFragmentDirections.ActionBookingPengajuanEditCash dir = BookingBrowseFragmentDirections
//              .actionBookingPengajuanEditCash(bd.getId(), isPermissionCreate);
//          navController.navigate(dir, NavHelper.animParentToChild().build());
//          mMenuDialog.dismiss();
//        } else {
//          BookingBrowseFragmentDirections.ActionBookingPengajuanEditTransfer dir = BookingBrowseFragmentDirections
//              .actionBookingPengajuanEditTransfer(bd.getId(), isPermissionCreate);
//          navController.navigate(dir, NavHelper.animParentToChild().build());
//          mMenuDialog.dismiss();
//        }
//      }
//    });
//    mPengajuanBatal.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        NavController navController = Navigation.findNavController(binding.getRoot());
//        if (bd.getBookingTypeId().equals("C")) {
//          BookingBrowseFragmentDirections.ActionBookingPengajuanBatalCash dir = BookingBrowseFragmentDirections
//              .actionBookingPengajuanBatalCash(bd.getId(), isPermissionCreate);
//          navController.navigate(dir, NavHelper.animParentToChild().build());
//          mMenuDialog.dismiss();
//        } else {
//          BookingBrowseFragmentDirections.ActionBookingPengajuanBatalTransfer dir = BookingBrowseFragmentDirections
//              .actionBookingPengajuanBatalTransfer(bd.getId(), isPermissionCreate);
//          navController.navigate(dir, NavHelper.animParentToChild().build());
//          mMenuDialog.dismiss();
//        }
//      }
//    });
//  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.expense_browse_menu, menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (item.getItemId() == R.id.menu_filter) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_booking_browse, nc.getCurrentDestination().getId()))
        return true;
      BookingBrowseFragmentDirections.ActionFilter dir = BookingBrowseFragmentDirections.actionFilter();
      dir.setFilterField(mViewModel.getFilterField());
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    } else if (item.getItemId() == R.id.menu_sort) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_booking_browse, nc.getCurrentDestination().getId()))
        return true;
      List<SortOrder> soList = new ArrayList<>(mViewModel.getSortFields());
      String soListJson = new Gson().toJson(soList);
      BookingBrowseFragmentDirections.ActionSort dir = BookingBrowseFragmentDirections.actionSort();
      dir.setSortField(soListJson);
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    } else {
      // FIXME JIKA BUKAN DARI SALAH SATU OPTS INI, MAKA APA YANG HARUS DILAKUKAN?
      // Snackbar.make(binding.getRoot(), "Menu Tidak tersedia", Snackbar.LENGTH_LONG).show();
    }
    return false;
  }

  private void setFilterObserver() {
    NavController navController = NavHostFragment.findNavController(this);
    MutableLiveData<BookingBrowseFilter> filterField = navController
        .getCurrentBackStackEntry()
        .getSavedStateHandle()
        .getLiveData(AppBookingFilterFragment.FILTER_FIELD);

    filterField.observe(getViewLifecycleOwner(), filterFieldObj -> {
      mViewModel.loadDataWithFilter(filterFieldObj);
      navController.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(AppBookingFilterFragment.FILTER_FIELD);
    });
  }

  private void setSortObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<String> soListJsonMld = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        BookingSortFragment.BOOKING_SORT_FIELDS);

    soListJsonMld.observe(getViewLifecycleOwner(), soListJsonObj -> {
      String soListJson = soListJsonObj.replace("\\", "");
      Type dataType = new TypeToken<List<SortOrder>>() {
      }.getType();
      List<SortOrder> soList = new Gson().fromJson(soListJson, dataType);
      Set<SortOrder> sso = new HashSet<SortOrder>(soList);
      mViewModel.setSortFields(sso);
      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
          BookingSortFragment.BOOKING_SORT_FIELDS);
    });
  }
}