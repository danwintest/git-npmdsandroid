package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskCompanyFilterData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskFilterResponse;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;

public class DroppingDailyTaskFilterVM extends AndroidViewModel
    implements GetDroppingDailyTaskFilterUC.Listener {
  private final static String TAG = DroppingDailyTaskFilterVM.class.getSimpleName();
  private final GetDroppingDailyTaskFilterUC uc;
  public UUID defaultFilterCompanyId;
  private DroppingDailyTaskBrowseFilter filterField = new DroppingDailyTaskBrowseFilter();
  private KeyValueModel<UUID, String> companyKvm = null;
  private MutableLiveData<Boolean> group = new MutableLiveData<>();
//  private MutableLiveData<KeyValueModel<String, String>> outletKvm = new MutableLiveData<>();
//  private MutableLiveData<KeyValueModel<String, String>> bankKvm = new MutableLiveData<>();
//  private LocalDate>droppingDate = new MutableLiveData<>();

  private MutableLiveData<List<KeyValueModel<UUID, String>>> companyKvmList = new MutableLiveData<>();
//  private List<KeyValueModel<String, String>> outletKvmList = new ArrayList<>();
//  private List<KeyValueModel<String, String>> bankKvmList = new ArrayList<>();

  @ViewModelInject
  public DroppingDailyTaskFilterVM(@NonNull Application app, GetDroppingDailyTaskFilterUC uc) {
    super(app);
    this.uc = uc;
    this.uc.registerListener(this);
  }

  public DroppingDailyTaskBrowseFilter getFilterField() {
    return filterField;
  }

  public void setFilterField(DroppingDailyTaskBrowseFilter filterField) {
    HyperLog.d(TAG, "setFilterField() terpanggil" + filterField.toString());
    this.filterField = filterField;
    this.defaultFilterCompanyId = filterField.getCompanyId();
  }

  public void setCompanyId(UUID companyId,String companyName) {
    filterField.setCompanyId(companyId);
    filterField.setCompanyName(companyName);
  }

  public void setOutletName(String outletName) {
    filterField.setOutletName(outletName);
  }

  public void setRequestDate(LocalDate date) {
    filterField.setRequestDate(date);
  }

  public void processLoadCompanyList() {
    if (getCompanyKvmList().getValue() == null) {
      HyperLog.d(TAG, "companyKvmList kosong, maka processLoadCompanyList() dijalankan");
      uc.getCompanyFilter();
    }
  }

  public KeyValueModel<UUID, String> getCompanyKvm() {
    return companyKvm;
  }

  public void setCompanyKvm(KeyValueModel<UUID, String> data) {
    this.companyKvm = data;
  }

//  public MutableLiveData<KeyValueModel<String, String>> getOutletKvm() {
//    return outletKvm;
//  }

//  public void setOutletKvm(
//      MutableLiveData<KeyValueModel<String, String>> outletKvm) {
//    this.outletKvm = outletKvm;
//  }

//  public MutableLiveData<KeyValueModel<String, String>> getBankKvm() {
//    return bankKvm;
//  }

//  public void setBankKvm(
//      MutableLiveData<KeyValueModel<String, String>> bankKvm) {
//    this.bankKvm = bankKvm;
//  }

//  public MutableLiveData<LocalDate> getDroppingDate() {
//    return droppingDate;
//  }

//  public void setDroppingDate(MutableLiveData<LocalDate> droppingDate) {
//    this.droppingDate = droppingDate;
//  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getCompanyKvmList() {
    return companyKvmList;
  }

  public void setCompanyKvmList(List<KeyValueModel<UUID, String>> companyKvmList) {
    this.companyKvmList.postValue(companyKvmList);
  }

  @Override
  public void onProcessStarted(Resource<DroppingDailyTaskFilterResponse> loading) {
    //TODO BELUM ADA IMPLEMENTASI
  }

  @Override
  public void onProcessSuccess(Resource<DroppingDailyTaskFilterResponse> res) {
    List<DroppingDailyTaskCompanyFilterData> dataList = res.getData().getListFilterData();
    List<KeyValueModel<UUID, String>> companyList = new ArrayList<>();
    for (DroppingDailyTaskCompanyFilterData data : dataList) {
      KeyValueModel<UUID, String> companyKvm = new KeyValueModel<>(data.getId(), data.getCompanyName());
      if (data.getId().equals(getFilterField().getCompanyId())) {
        setCompanyKvm(companyKvm);
      }
      companyList.add(companyKvm);
    }
    setCompanyKvmList(companyList);
  }

  public MutableLiveData<Boolean> isGroup() {
    return group;
  }

  public void setGroup(Boolean group) {
    this.group.postValue(group);
  }

  @Override
  public void onProcessFailure(Resource<DroppingDailyTaskFilterResponse> res) {
    // TODO BELUM ADA IMPLEMENTASI
  }
}
