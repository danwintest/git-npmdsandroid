package id.co.danwinciptaniaga.npmdsandroid.ui.landing;

import java.util.List;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.auth.GetUserPermissionUseCase;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.ExtUserInfoData;
import id.co.danwinciptaniaga.npmdsandroid.common.GetExtUserInfoUseCase;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;

public class DrawerViewModel extends AndroidViewModel implements GetExtUserInfoUseCase.Listener {

  private final MutableLiveData<Resource<ExtUserInfoData>> userInfoLd = new MutableLiveData<>(null);
  private final MutableLiveData<String> errorMessage = new SingleLiveEvent<>();
  private final GetExtUserInfoUseCase ucGetExtUserInfo;

  private ExtUserInfoData userInfo = null;

  @ViewModelInject
  public DrawerViewModel(@NonNull Application application,
      GetExtUserInfoUseCase getUserInfoUseCase) {
    super(application);
    this.ucGetExtUserInfo = getUserInfoUseCase;
    this.ucGetExtUserInfo.registerListener(this);
  }

  public void loadUserInfo() {
    this.ucGetExtUserInfo.getUserInfo();
  }

  public LiveData<Resource<ExtUserInfoData>> getUserInfoLd() {
    return userInfoLd;
  }

  public LiveData<String> getErrorMessage() {
    return errorMessage;
  }

  @Override
  public void onGetExtUserInfoStarted() {
    this.userInfoLd.postValue(Resource.Builder.loading(null, null));
  }

  @Override
  public void onGetExtUserInfoSuccess(Resource<ExtUserInfoData> response) {
    userInfo = response.getData();
    this.userInfoLd.postValue(response);
  }

  @Override
  public void onGetExtUserInfoFailure(Resource<ExtUserInfoData> responseError) {
    String message = null;
    if (responseError.getErrorResponse() != null) {
      message = responseError.getErrorResponse().getErrorDescription();
    }
    if (message == null) {
      if (responseError.getMessage() != null) {
        message = responseError.getMessage();
      }
    }

    this.userInfoLd.postValue(null);
    this.errorMessage.postValue(message);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.ucGetExtUserInfo.unregisterListener(this);
  }
}
