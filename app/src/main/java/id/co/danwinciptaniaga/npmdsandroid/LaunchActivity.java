package id.co.danwinciptaniaga.npmdsandroid;

import javax.inject.Inject;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.accounts.AccountManager;
import android.accounts.OperationCanceledException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.AccountAuthenticator;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCaseSync;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceViewModel;
import id.co.danwinciptaniaga.androcon.registration.RegistrationUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity;

@AndroidEntryPoint
public class LaunchActivity extends AppCompatActivity {
  private static final String TAG = LaunchActivity.class.getSimpleName();

  private static final int REQ_SIGNUP = 1;
  private AccountManager mAccountManager;

  private boolean isAuthenticating = false;

  BroadcastReceiver br = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      if (AndroconConfig.INTENT_INIT_COMPLETE.equals(intent.getAction())) {
        HyperLog.i(TAG, "AndroconInitialization complete");
        HyperLog.d(TAG, "performRouting INTENT_INIT_COMPLETE");
        performRouting();
      } else if (AndroconConfig.INTENT_INIT_PROGRESS.equals(intent.getAction())) {
        String step = intent.getStringExtra(AndroconConfig.INTENT_ARG_STEP);
        HyperLog.i(TAG, "AndroconInitialization progressing: " + step);
        tvMessage.setText(step);
      } else {
        HyperLog.w(TAG, "What is this? Unknown intent: " + intent);
      }
    }
  };

  @Inject
  AndroconConfig androconConfig;
  @Inject
  AppExecutors appExecutors;
  @Inject
  LoginUtil loginUtil;
  @Inject
  RegistrationUtil registrationUtil;

  @Inject
  GetTokenUseCaseSync getTokenUseCaseSync;

  private RegisterDeviceViewModel registerDeviceViewModel;
  private TextView tvMessage;
  private Button btnRetry;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_launch);

    tvMessage = findViewById(R.id.tvMessage);
    btnRetry = findViewById(R.id.btnRetry);
    btnRetry.setOnClickListener(this::retryInit);

    mAccountManager = AccountManager.get(this);

    registerDeviceViewModel = new ViewModelProvider(LaunchActivity.this)
        .get(RegisterDeviceViewModel.class);
    registerDeviceViewModel.getDeviceRegistrationStatus()
        .observe(LaunchActivity.this, status -> {
          if (status != null) {
            Log.d(TAG, "Registering to server status: " + status.getStatus());
            if (status.getStatus() == Status.LOADING) {
              tvMessage.setText(getString(R.string.msg_please_wait));
              btnRetry.setVisibility(View.GONE);
            } else if (status.getStatus() == Status.SUCCESS) {
              tvMessage.setText(null);
              btnRetry.setVisibility(View.GONE);
              Futures.submit(new Runnable() {
                @Override
                public void run() {
                  showNextPage();
                }
              }, appExecutors.backgroundIO());

            } else if (status.getStatus() == Status.ERROR) {
              // TODO: handle error dengan benar, harusnya User bisa coba lagi
              loginUtil.clearToken(); // TODO: remove this

              tvMessage.setText("Gagal Registrasi");

              btnRetry.setVisibility(View.VISIBLE);
            }
          }
        });
    mAccountManager = AccountManager.get(LaunchActivity.this);
  }

  private void retryInit(View view) {
    HyperLog.d(TAG, "performRouting retryInit");
    performRouting();
  }

  @Override
  protected void onResume() {
    Log.d(TAG, "onResume invoked");
    super.onResume();
    if (androconConfig.isInitialized()) {
      if (!isAuthenticating) {
        HyperLog.d(TAG, "performRouting onResume");
        performRouting();
      }
    } else {
      Log.i(TAG, "Androcon initialization not done yet, wait ...");

      IntentFilter intentFilter = new IntentFilter();
      intentFilter.addAction(AndroconConfig.INTENT_INIT_PROGRESS);
      intentFilter.addAction(AndroconConfig.INTENT_INIT_COMPLETE);
      LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
      lbm.registerReceiver(br, intentFilter);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    LocalBroadcastManager.getInstance(this).unregisterReceiver(br);
  }

  private void performRouting() {
    Log.d(TAG, "performRouting invoked");
    // Bagian ini harus dijalankan secara async, karena ada bagian yang membaca Token dari AuthManager secara sync
    ListenableFuture<Void> future = Futures.submit(new Runnable() {
      @Override
      public void run() {
        // Device harus selalu coba didaftarkan, karena:
        // - record device di server bisa terhapus
        Log.d(TAG, "Registering to server");
        registerDeviceViewModel.registerDevice();
        isAuthenticating = false;
      }
    }, appExecutors.backgroundIO());
    Futures.catching(future, Exception.class, e -> {
      Log.w(TAG, "Problem registering to server", e);
      return null;
    }, appExecutors.backgroundIO());
  }

  private void showNextPage() {
    // check User login etc
    if (!loginUtil.isLoggedIn()) {
      HyperLog.i(TAG, "User is not logged in");
      // user belum login, tampilkan halaman Authentication
      //      App.showAuthenticationPage(loginUtil, this);
      try {
        isAuthenticating = true;
        Bundle options = new Bundle();
        // tandai ini untuk keperluan login
        options.putBoolean(AccountAuthenticator.ARG_GENERAL_LOGIN, true);
        options.putBoolean(AccountAuthenticator.ARG_PERFORM_APP_LOGIN, true);
        Bundle result = mAccountManager.addAccount(App.ACCOUNT_TYPE, App.AUTH_TOKEN_TYPE, null,
            options, this, null,
            null).getResult();
        // harusnya result tidak pernah mengembalikan Intent, karena addAccount yang akan membuka
        if (result.get(AccountManager.KEY_INTENT) != null)
          throw new IllegalStateException("During login, addAccount should not return Intent");
        // Activity yang diperlukan untuk Authentication
        String username = result.getString(AccountManager.KEY_ACCOUNT_NAME);
        HyperLog.i(TAG, "Successful login for " + username);
        isAuthenticating = false;
        finish();
        App.showLandingPage(this);
      } catch (OperationCanceledException e) {
        HyperLog.w(TAG, "Attempt to prompt User to login is cancelled", e);
        finish();
      } catch (Exception e) {
        HyperLog.e(TAG, "Error while attempting to prompt User to login", e);
        Toast.makeText(this, getString(R.string.generic_error, e.getMessage()),
            Toast.LENGTH_LONG).show();
        finish();
      }
    } else {
      HyperLog.i(TAG, "User is logged in");
      App.showLandingPage(this);
      finish();
    }
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Log.i(TAG, "onActivityResult: " + requestCode + " - " + resultCode);
    if (requestCode == AuthenticationActivity.REQ_GENERAL_AUTH) {
      if (resultCode == AuthenticationActivity.RESULT_OK) {
        showNextPage();
      } else {
        finish();
      }
    }
  }
}