package id.co.danwinciptaniaga.npmdsandroid.exp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.hypertrack.hyperlog.HyperLog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseSort;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.SortPropertyAdapter;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentExpenseBrowseSortBinding;
import id.co.danwinciptaniaga.npmdsandroid.sort.SortPropertyEntry;

@AndroidEntryPoint
public class ExpenseSortFragment extends BottomSheetDialogFragment {
  private static final String TAG = ExpenseSortFragment.class.getSimpleName();
  private ExpenseBrowseViewModel viewModel;
  private FragmentExpenseBrowseSortBinding binding;

  private static Map<ExpenseBrowseSort.Field, Integer> labelMaps = new HashMap();

  static {
    labelMaps.put(ExpenseBrowseSort.Field.TRANSACTION_NO, R.string.transactionNo);
    labelMaps.put(ExpenseBrowseSort.Field.STATEMENT_DATE, R.string.statement_date);
    labelMaps.put(ExpenseBrowseSort.Field.EXPENSE_DATE, R.string.expense_date);
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(requireActivity()).get(ExpenseBrowseViewModel.class);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    binding = FragmentExpenseBrowseSortBinding.inflate(inflater);
    setupSortPropertyList();
    setupButtons(binding);
    return binding.getRoot();
  }

  private void setupSortPropertyList() {
    final List<SortPropertyEntry> speSet = new ArrayList<>(); // urutan dipertahankan
    final Map<SortOrder, SortPropertyEntry> map = new HashMap<>();
    for (ExpenseBrowseSort.Field field : ExpenseBrowseSort.Field.values()) {
      Integer labelId = labelMaps.get(field);
      if (labelId == null) {
        HyperLog.w(TAG, String.format("Property label for Sort supported: %s", field.getName()));
        continue;
      }

      Optional<SortOrder> oSo = viewModel.getSortFields().stream().filter(
          sf -> sf.getProperty().equals(field.getName())).findAny();

      boolean isSelected;
      boolean isOn = false;
      if (oSo.isPresent()) {
        isSelected = true;
        isOn = SortOrder.Direction.ASC.equals(oSo.get().getDirection()); // on = ASC
      } else {
        isSelected = false;
      }

      SortPropertyEntry spe = new SortPropertyEntry(field.getName(), labelId, isSelected, isOn);
      if (oSo.isPresent()) {
        // catat mapping SortOrder ke SPE
        map.put(oSo.get(), spe);
      }
      speSet.add(spe);
    }

    Set<SortPropertyEntry> speInOrder = viewModel.getSortFields().stream().map(
        sf -> map.get(sf)).collect(Collectors.toSet());

    SortPropertyAdapter spa = new SortPropertyAdapter(speSet, speInOrder);
    binding.fieldList.setAdapter(spa);
  }

  private void setupButtons(FragmentExpenseBrowseSortBinding binding) {
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // pada saat tombol Apply diklik, baru baca semua dan set semua sort fields
        SortPropertyAdapter adapter = (SortPropertyAdapter) binding.fieldList.getAdapter();
        Set<SortPropertyEntry> selectedSpeSet = adapter.getPropertySelection();
        Set<SortOrder> sortOrderSet = new LinkedHashSet<>();
        selectedSpeSet.stream().forEach(spe -> {
          // dikonversi ke field untuk memastikan bahwa SpeName yang digunakan memang valid
          ExpenseBrowseSort.Field f = ExpenseBrowseSort.Field.fromId(spe.getName());
          sortOrderSet.add(ExpenseBrowseSort
              .sortBy(f, spe.isOn() ? SortOrder.Direction.ASC : SortOrder.Direction.DESC));
        });
        viewModel.setSortFields(sortOrderSet);
        // tutup filter
        dismiss();
      }
    });
  }
}
