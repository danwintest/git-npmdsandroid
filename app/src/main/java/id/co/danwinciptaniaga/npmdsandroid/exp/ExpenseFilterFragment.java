package id.co.danwinciptaniaga.npmdsandroid.exp;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentExpenseBrowseFilterBinding;
import id.co.danwinciptaniaga.npmdsandroid.ui.DatePickerUIHelper;

@AndroidEntryPoint
public class ExpenseFilterFragment extends BottomSheetDialogFragment {
  private final String TAG = ExpenseFilterFragment.class.getSimpleName();
  public static String EXPENSE_FILTER_FIELD = "EXPENSE_FILTER_FIELD";
  private FragmentExpenseBrowseFilterBinding binding;
  private ArrayAdapter<KeyValueModel<UUID, String>> jenisBiayaAdapter;
  private ArrayAdapter<KeyValueModel<String,String>> statusAdapter;
  private ArrayAdapter<KeyValueModel<UUID, String>> companyAdapter;
  private ExpenseFilterVM vm;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    vm = new ViewModelProvider(this).get(ExpenseFilterVM.class);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    binding = FragmentExpenseBrowseFilterBinding.inflate(inflater, container, false);

    // TODO: filter outlet, expense Item
    ExpenseFilterFragmentArgs args = ExpenseFilterFragmentArgs.fromBundle(getArguments());
    setFromLoadingProgress();
    setupButtons();
    vm.setFilterField(args.getFilterField());
    vm.getStatusList();
    setupFilter_Field(inflater);
    setFilterCompany();
    return binding.getRoot();
  }

  private void setFromLoadingProgress() {
    vm.getFormState().observe(getViewLifecycleOwner(), state -> {
      switch (state.getState()) {
      case LOADING:
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
        break;
      case READY:
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        break;
      case ERROR:
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), state.getMessage(), Toast.LENGTH_LONG).show();
        break;
      }
    });
  }

  private void setupFilter_Field(@NonNull LayoutInflater inflater) {
    //set statusList
    vm.getStatusKvmList().observe(getViewLifecycleOwner(), dataList -> {
      statusAdapter = new ArrayAdapter(getContext(),android.R.layout.select_dialog_item);
      statusAdapter.addAll(dataList);
      binding.acStatus.setAdapter(statusAdapter);
      binding.acStatus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
          KeyValueModel<String, String> statusKvm = (KeyValueModel<String, String>) parent.getItemAtPosition(
              position);
          vm.setSelectedStatusKvmList(true, statusKvm);
          binding.acStatus.setText(null);
        }
      });
    });


    //set selectedStatusList
    vm.getSelectedStatusKvmList().observe(getViewLifecycleOwner(), dataList -> {
      binding.cgStatus.removeAllViews();
      for (int i = 0; i < dataList.size(); i++) {
        KeyValueModel<String, String> kvm = dataList.get(i);
        Chip chip = (Chip) inflater.inflate(R.layout.chip_filter, null);
        chip.setId(i);
        chip.setText(kvm.getValue());
        chip.setTag(kvm.getKey());
        chip.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            KeyValueModel<String, String> selectedItem = new KeyValueModel(chip.getTag(),
                chip.getText());
            vm.setSelectedStatusKvmList(false, selectedItem);
          }
        });
        binding.cgStatus.addView(chip);
      }
    });


    //set statementDateFrom
    DatePickerUIHelper.setupEditTextDateField(getContext(), binding.etStatementDateFrom,
        () -> vm.getFilterField().getStatementDateFrom());

    //set statementDateTo
    DatePickerUIHelper.setupEditTextDateField(getContext(), binding.etStatementDateTo,
        () -> vm.getFilterField().getStatementDateTo());

    //set expenseDateFrom
    DatePickerUIHelper.setupEditTextDateField(getContext(), binding.etExpenseDateFrom,
        () -> vm.getFilterField().getExpenseDateFrom());

    //set expenseDateTo
    DatePickerUIHelper.setupEditTextDateField(getContext(), binding.etExpenseDateTo,
        () -> vm.getFilterField().getExpenseDateTo());

    // set outletName
    binding.etExpenseOutlet.setText(vm.getFilterField().getOutletName());

    //set JenisBiaya
    vm.getJenisBiayaKvmList().observe(getViewLifecycleOwner(), dataList -> {
      jenisBiayaAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spExpenseItem.setAdapter(jenisBiayaAdapter);
      binding.spExpenseItem.setFocusable(false);
      jenisBiayaAdapter.addAll(dataList);
      jenisBiayaAdapter.notifyDataSetChanged();

      if (vm.getJenisBiayaKvm() != null) {
        int pos = jenisBiayaAdapter.getPosition(vm.getJenisBiayaKvm());
        binding.spExpenseItem.setSelection(pos);
      }

    });
  }

  private void setFilterCompany() {
    vm.getCompanyKvmList().observe(getViewLifecycleOwner(), cdList -> {
      companyAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spCompany.setAdapter(companyAdapter);
      binding.spCompany.setFocusable(false);
      companyAdapter.addAll(cdList);
      companyAdapter.notifyDataSetChanged();
      int pos = companyAdapter.getPosition(vm.getCompanyKvm());
      binding.spCompany.setSelection(pos);
    });

    binding.spCompany.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
          @org.jetbrains.annotations.Nullable View view, int i, long l) {
        KeyValueModel<UUID, String> kvm = companyAdapter.getItem(i);
        vm.setCompanyKvm(kvm);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {

      }
    });
  }

  private void setupButtons() {
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // pada saat tombol Apply diklik, baru baca semua dan set semua filter
        Set<String> selectedStatuses =  new HashSet<String>();
        if (vm.getSelectedStatusKvmList().getValue() != null) {
          for (KeyValueModel<String, String> selectedStatus : vm.getSelectedStatusKvmList().getValue()) {
            selectedStatuses.add(selectedStatus.getKey());
          }
        }

        LocalDate statementDateFrom = (LocalDate) binding.etStatementDateFrom.getTag();
        LocalDate statementDateTo = (LocalDate) binding.etStatementDateTo.getTag();
        LocalDate expenseDateFrom = (LocalDate) binding.etExpenseDateFrom.getTag();
        LocalDate expenseDateTo = (LocalDate) binding.etExpenseDateTo.getTag();
        String outletName = binding.etExpenseOutlet.getText().toString();
        KeyValueModel<UUID, String> selectedExpenseKvm = (KeyValueModel<UUID, String>)binding.spExpenseItem.getSelectedItem();
        UUID expenseItemId = selectedExpenseKvm != null ? selectedExpenseKvm.getKey() : null;

        HyperLog.d(TAG, "btnApplyFilter statementDateFrom[" + statementDateFrom + "]");
        HyperLog.d(TAG, "btnApplyFilter statementDateTo[" + statementDateTo + "]");
        HyperLog.d(TAG, "btnApplyFilter expenseDateFrom[" + expenseDateFrom + "]");
        HyperLog.d(TAG, "btnApplyFilter expenseDateTo[" + expenseDateTo + "]");
        HyperLog.d(TAG, "btnApplyFilter outletName[" + outletName + "]");
        HyperLog.d(TAG, "btnApplyFilter selectedStatuses[" + selectedStatuses + "]");
        HyperLog.d(TAG, "btnApplyFilter expenseItemId[" + expenseItemId + "]");

        ExpenseBrowseFilter ebf = vm.getFilterField();
        ebf.setStatementDateFrom(statementDateFrom);
        ebf.setStatementDateTo(statementDateTo);
        ebf.setExpenseDateFrom(expenseDateFrom);
        ebf.setExpenseDateTo(expenseDateTo);
        ebf.setStatus(selectedStatuses);
        ebf.setOutletName(outletName.isEmpty() ? null : outletName);
        ebf.setExpenseItemId(expenseItemId);
        ebf.setCompanyId(
            !Objects.equals(vm.getCompanyKvm(), null) ? vm.getCompanyKvm().getKey() : null);

        NavController nc = NavHostFragment.findNavController(ExpenseFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(EXPENSE_FILTER_FIELD, ebf);
        nc.popBackStack();
      }
    });
    binding.btnClearAll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        NavController nc = NavHostFragment.findNavController(ExpenseFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(EXPENSE_FILTER_FIELD,
            new ExpenseBrowseFilter());
        nc.popBackStack();
      }
    });
  }
}
