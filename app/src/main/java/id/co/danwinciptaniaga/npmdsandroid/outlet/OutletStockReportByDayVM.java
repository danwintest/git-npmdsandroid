package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.time.LocalDate;
import java.util.UUID;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.LoadState;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletStockByDayReportResponse;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;

public class OutletStockReportByDayVM extends AndroidViewModel
    implements GetOutletStockByDayReportUC.Listener {

  private final GetOutletStockByDayReportUC ucGetOutletStockReport;

  // initial value = null supaya observer dipanggil pertama kali dengan nilai null
  // dan loading dilakukan di sana
  private MutableLiveData<FormState> formState = new MutableLiveData<>(null);
  private UUID outletId;
  private MutableLiveData<LoadState> outletStockReportLoadState = new MutableLiveData<>();

  private LocalDate statementDateFromFilter = null;
  private LocalDate statementDateToFilter = null;

  private OutletStockByDayReportResponse osrResponse = null;

  @ViewModelInject
  public OutletStockReportByDayVM(@NonNull Application application,
      GetOutletStockByDayReportUC getOutletStockByDayReportUC) {
    super(application);
    this.ucGetOutletStockReport = getOutletStockByDayReportUC;
    LocalDate fromDate = LocalDate.now().withDayOfMonth(1);
    LocalDate toDate = LocalDate.now();

    setFilter(fromDate, toDate);

    ucGetOutletStockReport.registerListener(this);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.ucGetOutletStockReport.unregisterListener(this);
  }

  public UUID getOutletId() {
    return outletId;
  }

  public void setOutletId(UUID outletId) {
    this.outletId = outletId;
  }

  public LiveData<FormState> getFormState() {
    return formState;
  }

  public void clearFormState() {
    osrResponse = null;
    formState.setValue(null);
  }

  public void loadReport(boolean forceReload) {
    // skip loading kalau sudah ada isi atau bukan dipaksa clear
    if (forceReload || osrResponse == null) {
      ucGetOutletStockReport.getOutletStockByDayReport(outletId);
    }
  }

  public LiveData<LoadState> getOutletStockReportLoadState() {
    return outletStockReportLoadState;
  }

  public void setOutletStockReportLoadState(LoadState loadState) {
    this.outletStockReportLoadState.postValue(loadState);
  }

  public OutletStockByDayReportResponse getOsrResponse() {
    return osrResponse;
  }

  @Override
  public void onGetStockByDayReportStarted() {
    formState.postValue(FormState.loading(getApplication().getString(R.string.memuat_data)));
  }

  @Override
  public void onGetStockByDayReportSuccess(Resource<OutletStockByDayReportResponse> result) {
    osrResponse = result.getData();
    formState.postValue(FormState.ready(result.getMessage()));
  }

  @Override
  public void onGetStockByDayReportFailure(Resource<OutletStockByDayReportResponse> responseError) {
    String message = Resource.getMessageFromError(responseError,
        getApplication().getString(R.string.error_contact_support));
    formState.postValue(FormState.error(message));
  }

  public LocalDate getStatementDateFromFilter() {
    return statementDateFromFilter;
  }

  public LocalDate getStatementDateToFilter() {
    return statementDateToFilter;
  }

  public void setFilter(LocalDate statementDateFrom, LocalDate statementDateTo) {
    this.statementDateFromFilter = statementDateFrom;
    ucGetOutletStockReport.setStatementDateFrom(this.statementDateFromFilter);
    this.statementDateToFilter = statementDateTo;
    ucGetOutletStockReport.setStatementDateTo(this.statementDateToFilter);
    this.loadReport(true);
  }
}
