package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.expense.NewExpenseResponse;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;

public class PrepareNewExpenseUseCase
    extends BaseObservable<PrepareNewExpenseUseCase.Listener, NewExpenseResponse> {
  public interface Listener {
    void onPrepareNewExpenseStarted();

    void onPrepareNewExpenseSuccess(Resource<NewExpenseResponse> response);

    void onPrepareNewExpenseFailure(Resource<NewExpenseResponse> responseError);
  }

  private final ExpenseService expenseService;
  private final AppExecutors appExecutors;

  @Inject
  public PrepareNewExpenseUseCase(Application app, ExpenseService expenseService,
      AppExecutors appExecutors) {
    super(app);
    this.expenseService = expenseService;
    this.appExecutors = appExecutors;
  }

  public void prepare() {
    notifyStart(null, null);

    ListenableFuture<NewExpenseResponse> newExpense = expenseService.getNewExpense();
    Futures.addCallback(newExpense, new FutureCallback<NewExpenseResponse>() {
      @Override
      public void onSuccess(@NullableDecl NewExpenseResponse result) {
        notifySuccess(null, result);
      }

      @Override
      public void onFailure(Throwable t) {
        notifyFailure(null, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<NewExpenseResponse> result) {
    listener.onPrepareNewExpenseStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<NewExpenseResponse> result) {
    listener.onPrepareNewExpenseSuccess(result);

  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<NewExpenseResponse> result) {
    listener.onPrepareNewExpenseFailure(result);

  }
}
