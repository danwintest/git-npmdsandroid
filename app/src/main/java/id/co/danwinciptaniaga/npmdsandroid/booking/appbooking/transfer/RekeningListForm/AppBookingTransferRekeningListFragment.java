package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import java.util.UUID;

import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferVM;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentAppBookingTransferRekeningListBinding;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListLoadStateAdapter;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;
import kotlin.Unit;

@AndroidEntryPoint
public class AppBookingTransferRekeningListFragment extends Fragment {
  private final String TAG = AppBookingTransferRekeningListFragment.class.getSimpleName();
  public static final String SAVED_STATE_LD_REFRESH = "refresh_AppBookingTransferRekeningListFragment";
  public static final String SELECTED_DATA = "SELECTED_DATA";
  FragmentAppBookingTransferRekeningListBinding binding;
  private BankAccountBrowseVM vm;
  private RecyclerView rv;
  private AppBookingTransferRekeningListAdapter adapter;
  private ConcatAdapter concatAdapter;

  @Inject
  AppExecutors appExecutors;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    //TODO HIDUPKAN JIKA BUTUH FILTER DAN SORTING (BELUM ADA IMPLEMETASI UNTUK FILTER DAN SORTING)
    //    setHasOptionsMenu(true);
    vm = new ViewModelProvider(this).get(BankAccountBrowseVM.class);
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    //TODO HIDUPKAN JIKA BUTUH FILTER DAN SORTING (BELUM ADA IMPLEMETASI UNTUK FILTER DAN SORTING)
    //    inflater.inflate(R.menu.expense_browse_menu, menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (item.getItemId() == R.id.menu_filter) {
      Toast.makeText(getContext(), "Belum ada implementasi", Toast.LENGTH_SHORT).show();
      //      NavController nc = Navigation.findNavController(binding.getRoot());
      //      TugasTransferDetailFragmentDirections.ActionFilter dir = TugasTransferDetailFragmentDirections.actionFilter(
      //          false);
      //      dir.setFilterField(vm.getFilterField().getValue());
      //      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    } else if (item.getItemId() == R.id.menu_sort) {
      Toast.makeText(getContext(), "Belum ada implementasi", Toast.LENGTH_SHORT).show();
      return true;
    }
    return super.onContextItemSelected(item);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentAppBookingTransferRekeningListBinding.inflate(inflater, container, false);
    AppBookingTransferRekeningListFragmentArgs arg = AppBookingTransferRekeningListFragmentArgs.fromBundle(
        getArguments());
    UUID outletId = arg.getOutletId();
    String trxType = arg.getTrxType();
    vm.setTxnMode_OutletId(trxType, outletId);
    onReady();
    setAdapterAndRecyclerAndSwipe();
    return binding.getRoot();
  }

  private void onReady() {
    vm.getLoadState().observe(getViewLifecycleOwner(), state -> {
      HyperLog.d(TAG, "appBookingTransfreRekeningList changed: " + state);
      if (state instanceof LoadState.Loading) {
        // sedang loading pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
      } else if (state instanceof LoadState.Error) {
        // terjadi error pada waktu pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);

        // retry = reload semua data
        binding.progressWrapper.retryButton.setOnClickListener(reloadFragment);
        // sembunyikan list, karena retry akan dihandle oleh button di atas
        binding.swipeRefresh.setVisibility(View.GONE);

        binding.progressWrapper.progressText.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            ((LoadState.Error) state).getError().getMessage());
      } else if (state instanceof LoadState.NotLoading) {
        // sudah ok
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.swipeRefresh.setVisibility(View.VISIBLE);
      }
    });
  }

  private View.OnClickListener reloadFragment = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // reload fragment ini = memuat ulang seluruh list
      adapter.refresh();
      // karena menggunakan progress indicator sendiri, jadi  tidak perlu yg dari swipeRefresh
      binding.swipeRefresh.setRefreshing(false);
    }
  };

  private View.OnClickListener retryCallBack = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      adapter.retry();
    }
  };

  private void setAdapterAndRecyclerAndSwipe() {
    rv = binding.rvbankAccountList;
    adapter = new AppBookingTransferRekeningListAdapter(
        new AppBookingTransferRekeningListAdapter.Listener() {
          @Override
          public void onItemClicked(View v, BankAccountData data, int position) {
            NavController nc = Navigation.findNavController(binding.getRoot());
            nc.getPreviousBackStackEntry().getSavedStateHandle().set(SELECTED_DATA, data);
            nc.popBackStack();
          }
        }, getActivity().getApplicationContext());

    concatAdapter = adapter.withLoadStateHeaderAndFooter(
        new ExpenseListLoadStateAdapter(retryCallBack),
        new ExpenseListLoadStateAdapter(retryCallBack));
    adapter.addLoadStateListener((combinedLoadStates) -> {
      vm.setLoadState(combinedLoadStates.getRefresh());
      return Unit.INSTANCE;
    });
    rv.setAdapter(concatAdapter);

    View.OnClickListener retryCallback = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        adapter.retry();
      }
    };

    vm.getActionEvent().observe(getViewLifecycleOwner(), s -> {
      HyperLog.d(TAG, "getActionEvent[" + s + "]");
      if (Status.SUCCESS.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
        adapter.refresh();
      } else if (Status.LOADING.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
        ViewUtil.setVisibility(binding.progressWrapper.progressText, View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            s.getMessage() == null ? getString(R.string.msg_please_wait) : s.getMessage());
        ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
      } else {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
      }
    });

    NavController nc = NavHostFragment.findNavController(this);

    LiveData<Boolean> refreshLd = nc.getCurrentBackStackEntry()
        .getSavedStateHandle().getLiveData(SAVED_STATE_LD_REFRESH);
    refreshLd.observe(getViewLifecycleOwner(), refresh -> {
      HyperLog.d(TAG, "refresh signaled: " + refresh);
      // tidak bisa langsung adapter.refresh, sepertinya adapternya belum connect ke RecyclerView-nya
      // jadi kita tunda dengan sinyal melalui viewmodel
      vm.setRefresh(refresh);
      nc.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(SAVED_STATE_LD_REFRESH);
    });

    binding.swipeRefresh.setOnRefreshListener(() -> {
      adapter.refresh();
      binding.swipeRefresh.setRefreshing(false);
    });

    vm.getBankAccountDataList().observe(getViewLifecycleOwner(), pagingData -> {
      adapter.submitData(getLifecycle(), pagingData);
    });
  }

}