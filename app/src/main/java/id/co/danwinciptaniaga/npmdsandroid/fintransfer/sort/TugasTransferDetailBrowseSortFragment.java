package id.co.danwinciptaniaga.npmdsandroid.fintransfer.sort;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.os.Bundle;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingSort;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferDetailBrowseSort;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingSortFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingSortFragmentArgs;
import id.co.danwinciptaniaga.npmdsandroid.common.SortPropertyAdapter;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentTugasTransferDetailBrowseSortBinding;
import id.co.danwinciptaniaga.npmdsandroid.sort.SortPropertyEntry;

public class TugasTransferDetailBrowseSortFragment extends BottomSheetDialogFragment {
  private static String TAG = TugasTransferDetailBrowseSortFragment.class.getSimpleName();
  public static String TUGAS_TRANSFER_DETAIL_SORT = "TUGAS_TRANSFER_DETAIL_SORT";
  FragmentTugasTransferDetailBrowseSortBinding binding;
  private static Map<TugasTransferDetailBrowseSort.Field, Integer> labelMap = new HashMap<>();

  static {
    labelMap.put(TugasTransferDetailBrowseSort.Field.TRANSACTION_NO, R.string.transactionNo);
    labelMap.put(TugasTransferDetailBrowseSort.Field.TRANSFER_DATE, R.string.transfer_date);
    labelMap.put(TugasTransferDetailBrowseSort.Field.PROC_TASK_NAME, R.string.nama_tugas);
    labelMap.put(TugasTransferDetailBrowseSort.Field.APP_TYPE_NAME, R.string.jenis_transaksi);
    labelMap.put(TugasTransferDetailBrowseSort.Field.OUTLET_NAME, R.string.outlet_name);
    labelMap.put(TugasTransferDetailBrowseSort.Field.AMOUNT, R.string.amount);
    labelMap.put(TugasTransferDetailBrowseSort.Field.ACC_NO, R.string.account_no);
    labelMap.put(TugasTransferDetailBrowseSort.Field.ACC_NAME, R.string.account_name);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentTugasTransferDetailBrowseSortBinding.inflate(inflater, container, false);
    AppBookingSortFragmentArgs args = AppBookingSortFragmentArgs.fromBundle(getArguments());
    String soListString = args.getSortField().replace("\\", "");
    Type dataType = new TypeToken<List<SortOrder>>() {
    }.getType();
    List<SortOrder> soList = new Gson().fromJson(soListString, dataType);
    Set<SortOrder> sso = new HashSet<SortOrder>(soList);
    setupSortPropertyList(sso);
    setupButtons();
    return binding.getRoot();
  }

  private void setupSortPropertyList(Set<SortOrder> sso) {
    final List<SortPropertyEntry> speSet = new ArrayList<>();
    final Map<SortOrder, SortPropertyEntry> map = new HashMap<>();
    for (TugasTransferDetailBrowseSort.Field field : TugasTransferDetailBrowseSort.Field.values()) {
      Integer labelId = labelMap.get(field);
      Preconditions.checkNotNull(labelId,
          String.format("Property label for Sort not found: %s", field.getName()));
      SortOrder isPresentSo = null;
      for (SortOrder so : sso) {
        if (so.getProperty().equals(field.getName())) {
          isPresentSo = so;
          break;
        }
      }

      boolean isSelected;
      boolean isOn = false;
      if (isPresentSo != null) {
        isSelected = true;
        isOn = SortOrder.Direction.ASC.equals(isPresentSo.getDirection()); // on = ASC
      } else {
        isSelected = false;
      }

      SortPropertyEntry spe = new SortPropertyEntry(field.getName(), labelId, isSelected, isOn);
      if (isPresentSo != null) {
        map.put(isPresentSo, spe);
      }
      speSet.add(spe);
    }

    Set<SortPropertyEntry> speInOrder = sso.stream().map(
        sf -> map.get(sf)).collect(Collectors.toSet());

    SortPropertyAdapter spa = new SortPropertyAdapter(speSet, speInOrder);
    binding.fieldList.setAdapter(spa);
  }

  private void setupButtons() {
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        SortPropertyAdapter adapter = (SortPropertyAdapter) binding.fieldList.getAdapter();
        Set<SortPropertyEntry> selectedSpeSet = adapter.getPropertySelection();
        Set<SortOrder> sortOrderSet = new LinkedHashSet<>();
        selectedSpeSet.stream().forEach(spe -> {
          TugasTransferDetailBrowseSort.Field f = TugasTransferDetailBrowseSort.Field.formId(spe.getName());
          sortOrderSet.add(TugasTransferDetailBrowseSort.sortBy(f,
              spe.isOn() ? SortOrder.Direction.ASC : SortOrder.Direction.DESC));
        });

        List<SortOrder> soList = new ArrayList(sortOrderSet);
        String soListJson = new Gson().toJson(soList);

        NavController nc = NavHostFragment.findNavController(TugasTransferDetailBrowseSortFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(TUGAS_TRANSFER_DETAIL_SORT,
            soListJson);
        nc.popBackStack();
      }
    });

    binding.btnReset.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(getContext(), "Belum ada implementasi", Toast.LENGTH_LONG).show();
      }
    });
  }
}