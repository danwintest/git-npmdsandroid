package id.co.danwinciptaniaga.npmdsandroid.prefs;

import java.io.File;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import com.google.common.net.MediaType;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;

public class UploadLogFileUseCase {
  private static final String TAG = UploadLogFileUseCase.class.getSimpleName();

  private final Application application;
  private final UtilityService utilityService;
  private final AppExecutors appExecutors;

  @Inject
  public UploadLogFileUseCase(Application application, UtilityService utilityService,
      AppExecutors appExecutors) {
    this.application = application;
    this.utilityService = utilityService;
    this.appExecutors = appExecutors;
  }

  public void upload() {
    ListenableFuture<Void> submit = Futures.submit(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        File latestFile = HyperLog.getDeviceLogsInFile(application);
        File dir = latestFile.getParentFile();
        for (File f : dir.listFiles()) {
          MultipartBody.Part attachmentPart = RetrofitUtility
              .prepareFilePart("logfile", f, MediaType.ANY_TEXT_TYPE.toString());
          try {
            ListenableFuture<ResponseBody> lf = utilityService.uploadLogFile(attachmentPart);
            lf.get();
            Toast.makeText(application, "File log " + f.getName() + " berhasil diupload",
                Toast.LENGTH_LONG).show();
          } catch (Exception e) {
            HyperLog.w(TAG, "Failed to upload log file " + f.getName(), e);
          }
        }
        return null;
      }
    }, appExecutors.networkIO());
  }
}
