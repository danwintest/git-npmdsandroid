package id.co.danwinciptaniaga.npmdsandroid.ui;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.function.Consumer;
import java.util.function.Supplier;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.DatePicker;

public class DatePickerDialogListener implements View.OnClickListener {
  private final Context context;
  private final Supplier<LocalDate> dateSupplier;
  private final Consumer<LocalDate> dateConsumer;
  private final boolean cancelClearField;

  public DatePickerDialogListener(Context context, Supplier<LocalDate> dateSupplier,
      Consumer<LocalDate> dateConsumer) {
    this(context, dateSupplier, dateConsumer, true);
  }

  public DatePickerDialogListener(Context context, Supplier<LocalDate> dateSupplier,
      Consumer<LocalDate> dateConsumer, boolean cancelClearField) {
    this.context = context;
    this.dateSupplier = dateSupplier;
    this.dateConsumer = dateConsumer;
    this.cancelClearField = cancelClearField;
  }

  @Override
  public void onClick(View v) {
    int day = -1;
    int month = -1;
    int year = -1;
    if (dateSupplier.get() != null) {
      day = dateSupplier.get().getDayOfMonth();
      month = dateSupplier.get().getMonthValue() - 1;
      year = dateSupplier.get().getYear();
    } else {
      LocalDate now = LocalDate.now();
      day = now.getDayOfMonth();
      month = now.getMonthValue() - 1;
      year = now.getYear();
    }
    DatePickerDialog dpd = new DatePickerDialog(context,
        new DatePickerDialog.OnDateSetListener() {
          @Override
          public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            LocalDate localDate = LocalDate.of(year, month + 1, dayOfMonth);
            dateConsumer.accept(localDate);
          }
        }, year, month, day);
    dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        if (cancelClearField) {
          // kalau dicancel, artinya mengosongkan isi field-nya
          dateConsumer.accept(null);
        } else {
          dateConsumer.accept(dateSupplier.get() != null ? dateSupplier.get() : LocalDate.now());
        }
      }
    });
    dpd.show();
  }
}
