package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import java.time.LocalDate;

import com.google.common.base.Preconditions;

import android.content.Context;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.data.ConstraintViolation;

public class ExpenseValidationHelper {
  public static void validateStatementDate(Context context, LocalDate now, LocalDate statementDate)
      throws ConstraintViolation {
    Preconditions.checkNotNull(now, "Now cannot be null");
    if (statementDate == null)
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.statement_date)));

    if (statementDate.isAfter(now))
      throw new ConstraintViolation(context.getString(R.string.validation_s_date_cannot_be_future,
          context.getString(R.string.statement_date)));
  }

  public static void validateExpenseDate(Context context, LocalDate now, LocalDate expenseDate)
      throws ConstraintViolation {
    Preconditions.checkNotNull(now, "Now cannot be null");
    if (expenseDate == null)
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.expense_date)));

    if (expenseDate.isAfter(now))
      throw new ConstraintViolation(context.getString(R.string.validation_s_date_cannot_be_future,
          context.getString(R.string.expense_date)));
  }

  public static void validateAmount(Context context, Integer amount) throws ConstraintViolation {
    if (amount == null)
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.expense_amount)));

    if (amount <= 0)
      throw new ConstraintViolation(context.getString(R.string.validation_s_positive,
          context.getString(R.string.expense_amount)));
  }

  public static void validateQuantity(Context context, Integer quantity)
      throws ConstraintViolation {
    if (quantity == null)
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.expense_quantity)));

    if (quantity <= 0)
      throw new ConstraintViolation(context.getString(R.string.validation_s_positive,
          context.getString(R.string.expense_quantity)));
  }

  public static void validateBooking(Context context, BookingData bookingData)
      throws ConstraintViolation {
    if (bookingData == null)
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.expense_booking)));
  }
}
