package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import retrofit2.HttpException;

public class DroppingAdditionalSupervisorDecideUseCase extends
    BaseObservable<DroppingAdditionalSupervisorDecideUseCase.Listener, DroppingAdditionalData> {
  private String TAG = DroppingAdditionalSupervisorDecideUseCase.class.getSimpleName();
  public interface Listener {

    void onDaDecideStarted(Resource<DroppingAdditionalData> loading);

    void onDaDecideSuccess(Resource<DroppingAdditionalData> response);

    void onDaDecideFailure(Resource<DroppingAdditionalData> response);

  }

  private final DroppingAdditionalService daService;

  private final AppExecutors appExecutors;

  @Inject
  public DroppingAdditionalSupervisorDecideUseCase(Application application,
      DroppingAdditionalService daService,
      AppExecutors appExecutors) {
    super(application);
    this.daService = daService;
    this.appExecutors = appExecutors;
  }

  public void docApprove(UUID procTaskId, String comment, UUID droppingId,
      String batchCode, long approvedAmount, boolean altAccount,
      BankAccountData bankAccountData, boolean opTransfer, Date checkTs) {
    notifyStart(null, null);
    ListenableFuture<DroppingAdditionalData> saveDraftLf = daService.docApprove(procTaskId, comment,
        droppingId, batchCode, approvedAmount, altAccount, bankAccountData, opTransfer, checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<DroppingAdditionalData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingAdditionalData result) {
        notifySuccess(application.getString(R.string.dropping_additional_decide_success,
            application.getString(R.string.action_approve)), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_additional_decide_failed,
                application.getString(R.string.action_approve)) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void captainApprove(UUID procTaskId, String comment, UUID droppingId,
      String batchCode, long approvedAmount, boolean altAccount,
      BankAccountData bankAccountData, boolean opTransfer, Date checkTs) {
    notifyStart(null, null);
    // todo: harusnya Captain bisa ubah rekening alternatif
    ListenableFuture<DroppingAdditionalData> saveDraftLf = daService.captainApprove(procTaskId,
        comment, droppingId, batchCode, approvedAmount, altAccount, bankAccountData, opTransfer,
        checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<DroppingAdditionalData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingAdditionalData result) {
        notifySuccess(application.getString(R.string.dropping_additional_decide_success,
            application.getString(R.string.action_approve)), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_additional_decide_failed,
                application.getString(R.string.action_reject)) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void finStaffApprove(UUID procTaskId, String comment, UUID droppingId,
      LocalDate transferDate, String transferType, UUID sourceAccountId, Date checkTs) {
    notifyStart(null, null);
    ListenableFuture<DroppingAdditionalData> saveDraftLf = daService.finStaffApprove(procTaskId,
        comment, droppingId, transferDate,
        transferType, sourceAccountId, checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<DroppingAdditionalData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingAdditionalData result) {
        notifySuccess(application.getString(R.string.dropping_additional_decide_success,
            application.getString(R.string.action_approve)), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_additional_decide_failed,
                application.getString(R.string.action_reject)) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void finSpvApprove(UUID procTaskId, String comment, UUID droppingId, String otpCode,
      String transferType, Date checkTs) {
    notifyStart(null, null);
    ListenableFuture<DroppingAdditionalData> process = daService.finSpvApprove(procTaskId,
        comment, droppingId, otpCode, transferType, checkTs);
    Futures.addCallback(process, new FutureCallback<DroppingAdditionalData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingAdditionalData result) {
        notifySuccess(application.getString(R.string.dropping_daily_decide_success,
            application.getString(R.string.action_approve)), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_daily_decide_failed,
                application.getString(R.string.action_reject)) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void processGenerateOTP(UUID procTaskId, UUID droppingId) {
    notifyStart(null, null);
    ListenableFuture<DroppingAdditionalData> saveDraftLf = daService.generateOtp(procTaskId, droppingId);
    Futures.addCallback(saveDraftLf, new FutureCallback<DroppingAdditionalData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingAdditionalData result) {
        HyperLog.d(TAG,"generateOtp [BERHASIL]");
//        notifySuccess(application.getString(R.string.dropping_additional_decide_success,
//            application.getString(R.string.action_approve)), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.request_otp_failed) : null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void rejectOrReturn(UUID procTaskId, UUID droppingId, String outcome, String comment,
      Date checkTs) {
    notifyStart(null, null);
    ListenableFuture<DroppingAdditionalData> saveDraftLf = daService.rejectOrReturn(procTaskId,
        outcome,
        comment, droppingId, checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<DroppingAdditionalData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingAdditionalData result) {
        notifySuccess(application.getString(R.string.dropping_additional_decide_success, outcome),
            result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_additional_decide_failed, outcome) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<DroppingAdditionalData> result) {
    listener.onDaDecideStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<DroppingAdditionalData> result) {
    listener.onDaDecideSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<DroppingAdditionalData> result) {
    listener.onDaDecideFailure(result);
  }
}
