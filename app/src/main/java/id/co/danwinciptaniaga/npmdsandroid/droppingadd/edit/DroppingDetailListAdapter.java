package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import java.io.File;
import java.util.Objects;

import com.bumptech.glide.Glide;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDetailDTO;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiDroppingAdditionalDetailBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

public class DroppingDetailListAdapter
    extends ListAdapter<DroppingDetailDTO, DroppingDetailListAdapter.ViewHolder> {
  private static final String TAG = DroppingDetailListAdapter.class.getSimpleName();

  private final Context context;
  private final AdapterListener listener;
  private final CommonService commonService;
  private final AppExecutors appExecutors;
  private boolean readonly;

  interface AdapterListener {

    void onEditClicked(View view, int position);

    void onRemoveClicked(View v, int position);

    void onAttachmentClicked(View v, int position);

    void onPrepareAttachment(ImageView ivAttachment, int position);

  }

  protected DroppingDetailListAdapter(Context context, AdapterListener listener,
      CommonService commonService, AppExecutors appExecutors) {
    super(new DroppingDetailDataDiffCallBack());
    this.context = context;
    this.listener = listener;
    this.commonService = commonService;
    this.appExecutors = appExecutors;
  }

  public void setReadOnly(boolean readonly) {
    this.readonly = readonly;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.li_dropping_additional_detail, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.bind(position);
  }

  private static class DroppingDetailDataDiffCallBack
      extends DiffUtil.ItemCallback<DroppingDetailDTO> {
    @Override
    public boolean areItemsTheSame(@NonNull DroppingDetailDTO oldItem,
        @NonNull DroppingDetailDTO newItem) {
      return oldItem.getDetailId() == newItem.getDetailId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull DroppingDetailDTO oldItem,
        @NonNull DroppingDetailDTO newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiDroppingAdditionalDetailBinding binding;

    public ViewHolder(@NonNull View itemView) {
      super(itemView);
      binding = LiDroppingAdditionalDetailBinding.bind(itemView);
    }

    public void bind(int position) {
      DroppingDetailDTO droppingDetailData = getItem(position);
      binding.tvRemarks.setText(droppingDetailData.getDescription());
      binding.tvAmount
          .setText(Formatter.DF_AMOUNT_NO_DECIMAL.format(droppingDetailData.getAmount()));
      if (!readonly) {
        // menurut dokumentasi #onBindViewHolder, position hanya boleh digunakan pada waktu binding
        // DAN tidak boleh disimpan, karena bisa sudah tidak valid lagi
        // jadi di listener, kita pass bindingAdapterPosition supaya listener bisa menggunakannya untuk
        // mendapatkan item dari adapter (atau backing list-nya)
        binding.ibEditDroppingDetail.setOnClickListener(v -> {
          listener.onEditClicked(v, getBindingAdapterPosition());
        });

        binding.ibDeleteDroppingDetail.setOnClickListener(v -> {
          listener.onRemoveClicked(v, getBindingAdapterPosition());
        });
        binding.ibEditDroppingDetail.setVisibility(View.VISIBLE);
        binding.ibDeleteDroppingDetail.setVisibility(View.VISIBLE);
      } else {
        binding.ibEditDroppingDetail.setVisibility(View.GONE);
        binding.ibDeleteDroppingDetail.setVisibility(View.GONE);
      }

      if (droppingDetailData.getAttachmentDTO() != null) {
        if (droppingDetailData.isArchived()) {
          binding.ivAttachment.setImageResource(R.drawable.ic_baseline_archive_24);
          return;
        }
        File file = droppingDetailData.getAttachmentDTO().getFile();
        if (file != null && file.exists()) {
          Glide.with(context).load(file)
              .placeholder(R.drawable.ic_baseline_image_24)
              .into(binding.ivAttachment);
        } else {
          Glide.with(context).load(droppingDetailData.getAttachmentDTO().getFileId())
              .placeholder(R.drawable.ic_baseline_image_24)
              .error(R.drawable.ic_baseline_broken_image_24)
              .into(binding.ivAttachment);
          listener.onPrepareAttachment(binding.ivAttachment, getBindingAdapterPosition());
        }
        binding.ivAttachment.setOnClickListener(v -> {
          listener.onAttachmentClicked(v, getBindingAdapterPosition());
        });
      } else {
        binding.ivAttachment.setOnClickListener(null);
      }
    }
  }
}
