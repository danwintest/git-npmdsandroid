package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import java.io.Serializable;

import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDetailDTO;

public class DroppingDetailFormData extends DroppingDetailDTO implements Serializable {
  private Integer index;

  public Integer getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }
}
