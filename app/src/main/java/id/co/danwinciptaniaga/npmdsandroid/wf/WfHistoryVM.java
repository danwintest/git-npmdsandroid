package id.co.danwinciptaniaga.npmdsandroid.wf;

import java.util.UUID;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.LoadState;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.wf.WfHistoryResponse;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;

public class WfHistoryVM extends AndroidViewModel
    implements GetWfHistoryUseCase.Listener {

  private final GetWfHistoryUseCase ucGetWfHistory;

  // initial value = null supaya observer dipanggil pertama kali dengan nilai null
  // dan loading dilakukan di sana
  private MutableLiveData<FormState> formState = new MutableLiveData<>(null);
  private WfType wfType = null;
  private UUID entityId;
  private MutableLiveData<LoadState> wfHistoryLoadState = new MutableLiveData<>();

  private WfHistoryResponse wfHistoryResponse = null;

  @ViewModelInject
  public WfHistoryVM(@NonNull Application application,
      GetWfHistoryUseCase getWfHistoryUseCase) {
    super(application);
    this.ucGetWfHistory = getWfHistoryUseCase;

    ucGetWfHistory.registerListener(this);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.ucGetWfHistory.unregisterListener(this);
  }

  public void setEntity(WfType type, UUID entityId) {
    this.wfType = type;
    this.entityId = entityId;
  }

  public void loadWfHistory() {
    ucGetWfHistory.loadWfHistory(wfType, entityId);
  }

  public LiveData<FormState> getFormState() {
    return formState;
  }

  public LiveData<LoadState> getWfHistoryLoadState() {
    return wfHistoryLoadState;
  }

  public void setWfHistoryLoadState(LoadState loadState) {
    this.wfHistoryLoadState.postValue(loadState);
  }

  public WfHistoryResponse getWfHistoryResponse() {
    return wfHistoryResponse;
  }

  @Override
  public void onGetWfHistoryStarted() {
    formState.postValue(FormState.loading(getApplication().getString(R.string.memuat_data)));
  }

  @Override
  public void onGetWfHistorySuccess(Resource<WfHistoryResponse> result) {
    wfHistoryResponse = result.getData();
    formState.postValue(FormState.ready(result.getMessage()));
  }

  @Override
  public void onGetWfHistoryFailure(Resource<WfHistoryResponse> responseError) {
    String message = Resource.getMessageFromError(responseError,
        getApplication().getString(R.string.error_contact_support));
    formState.postValue(FormState.error(message));
  }
}
