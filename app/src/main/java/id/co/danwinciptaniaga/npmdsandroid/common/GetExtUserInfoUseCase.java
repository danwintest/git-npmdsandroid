package id.co.danwinciptaniaga.npmdsandroid.common;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.ExtUserInfoData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import retrofit2.HttpException;

public class GetExtUserInfoUseCase
    extends BaseObservable<GetExtUserInfoUseCase.Listener, ExtUserInfoData> {
  private static final String TAG = GetExtUserInfoUseCase.class.getSimpleName();

  public interface Listener {
    void onGetExtUserInfoStarted();

    void onGetExtUserInfoSuccess(Resource<ExtUserInfoData> response);

    void onGetExtUserInfoFailure(Resource<ExtUserInfoData> response);
  }

  private final CommonService commonService;
  private final AppExecutors appExecutors;

  @Inject
  public GetExtUserInfoUseCase(Application application, CommonService commonService,
      AppExecutors appExecutors) {
    super(application);
    this.commonService = commonService;
    this.appExecutors = appExecutors;
  }

  public ListenableFuture<ExtUserInfoData> getUserInfo() {
    HyperLog.i(TAG, "getUserInfo called");
    notifyStart(application.getString(R.string.get_ext_user_info_started), null);
    ListenableFuture<ExtUserInfoData> extUserInfoLf = commonService.getExtUserInfo();
    Futures.addCallback(extUserInfoLf, new FutureCallback<ExtUserInfoData>() {
      @Override
      public void onSuccess(@NullableDecl ExtUserInfoData result) {
        notifySuccess(application.getString(R.string.get_ext_user_info_success), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.get_ext_user_info_failed) :
            null, er, t);
      }
    }, appExecutors.networkIO());
    return extUserInfoLf;
  }

  @Override
  protected void doNotifyStart(GetExtUserInfoUseCase.Listener listener,
      Resource<ExtUserInfoData> result) {
    listener.onGetExtUserInfoStarted();
  }

  @Override
  protected void doNotifySuccess(GetExtUserInfoUseCase.Listener listener,
      Resource<ExtUserInfoData> result) {
    listener.onGetExtUserInfoSuccess(result);
  }

  @Override
  protected void doNotifyFailure(GetExtUserInfoUseCase.Listener listener,
      Resource<ExtUserInfoData> result) {
    listener.onGetExtUserInfoFailure(result);
  }
}
