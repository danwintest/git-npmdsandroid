package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.BankData;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class TugasTransferCekSaldoBankListUC extends
    BaseObservable<TugasTransferCekSaldoBankListUC.Listener, List<BankData>> {
  private final String TAG = TugasTransferCekSaldoBankListUC.class.getSimpleName();
  private final TugasTransferService service;
  private final Application app;
  private final AppExecutors appExecutors;

  @Inject
  public TugasTransferCekSaldoBankListUC(Application app, TugasTransferService service,
      AppExecutors appExecutors) {
    super(app);
    this.app = app;
    this.service = service;
    this.appExecutors = appExecutors;
  }


  public void getBankDataListByCompanyId(UUID companyId) {
    notifyStart("Memuat data.. ", null);
    ListenableFuture<List<BankData>> process = service.getBankDataListByCompanyId(companyId);
    Futures.addCallback(process, new FutureCallback<List<BankData>>() {
      @Override
      public void onSuccess(@NullableDecl List<BankData> result) {
        HyperLog.d(TAG, "getBankDataListByCompanyId() berhasil");
        notifySuccess("-", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String msg = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("getBankDataListByCompanyId() Gagal ->" + msg, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(
      TugasTransferCekSaldoBankListUC.Listener listener, Resource<List<BankData>> result) {
    listener.onLoadBankStarted(result);
  }

  @Override
  protected void doNotifySuccess(TugasTransferCekSaldoBankListUC.Listener listener,
      Resource<List<BankData>> result) {
    listener.onLoadBankSuccess(result);
  }

  @Override
  protected void doNotifyFailure(TugasTransferCekSaldoBankListUC.Listener listener,
      Resource<List<BankData>> result) {
    listener.onLoadBankFailure(result);
  }

  public interface Listener {
    void onLoadBankStarted(Resource<List<BankData>> loading);

    void onLoadBankSuccess(Resource<List<BankData>> res);

    void onLoadBankFailure(Resource<List<BankData>> res);
  }
}
