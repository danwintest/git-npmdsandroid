package id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking;

import java.util.Objects;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiEditExpenseSelectBookingBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

public class SelectBookingListAdapter extends
    PagingDataAdapter<BookingData, SelectBookingListAdapter.ViewHolder> {
  private final static String TAG = SelectBookingListAdapter.class.getSimpleName();

  private final BookingDataRecyclerViewAdapterListener mListener;
  private Context mCtx;

  public SelectBookingListAdapter(BookingDataRecyclerViewAdapterListener listener, Context ctx) {
    super(new BookingDataDiffCallBack());
    mListener = listener;
    mCtx = ctx;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.li_edit_expense_select_booking, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull SelectBookingListAdapter.ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
    // FIXME SET ACTIVATED SELECTED VIEW
    // holder.mView.setActivated();
  }

  interface BookingDataRecyclerViewAdapterListener {
    void onItemClicked(View v, BookingData bookingData, int position);
  }

  private static class BookingDataDiffCallBack extends DiffUtil.ItemCallback<BookingData> {

    @Override
    public boolean areItemsTheSame(@NonNull BookingData oldItem, @NonNull BookingData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull BookingData oldItem, @NonNull BookingData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiEditExpenseSelectBookingBinding binding;

    public ViewHolder(@NonNull View v) {
      super(v);
      binding = LiEditExpenseSelectBookingBinding.bind(v);
    }

    public void bind(BookingData bookingData, int position) {
      setObject(bookingData);
      setObjectListener(bookingData, position);
    }

    private void setObject(BookingData bookingData) {
      binding.tvBkConsumerNo.setText(bookingData.getConsumerNo());
      binding.tvBkStatus.setText(bookingData.getStatus());
      binding.tvBkOutletAndCode.setText(
          String.format("%s (%s)", bookingData.getOutletObj().getOutletName(), bookingData.getOutletObj().getOutletCode()));
      binding.tvBkBookingDate.setText(
          String.format("%s (%s)", bookingData.getBookingDate().format(Formatter.DTF_dd_MM_yyyy),
              bookingData.getBookingTypeName()));
      binding.tvBkConsumerInfo.setText(String.format("%s - %s - %s", bookingData.getConsumerNo(),
          bookingData.getConsumerName(), bookingData.getVehicleNo()));

    }

    private void setObjectListener(BookingData bookingData, int position) {
      binding.clBkContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, bookingData, position);
        }
      });
    }

  }
}