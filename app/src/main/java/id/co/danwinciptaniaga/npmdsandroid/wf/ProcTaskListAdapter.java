package id.co.danwinciptaniaga.npmdsandroid.wf;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceReportData;
import id.co.danwinciptaniaga.npmds.data.wf.ProcTaskData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiOutletFintransactionBinding;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiProcTaskBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

public class ProcTaskListAdapter extends
    ListAdapter<ProcTaskData, ProcTaskListAdapter.ViewHolder> {
  private final static String TAG = ProcTaskListAdapter.class.getSimpleName();

  private Context mContext;

  @Inject
  public ProcTaskListAdapter(Context context) {
    super(new DiffCallBack());
    mContext = context;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.li_proc_task, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
  }

  private static class DiffCallBack extends DiffUtil.ItemCallback<ProcTaskData> {
    @Override
    public boolean areItemsTheSame(@NonNull ProcTaskData oldItem,
        @NonNull ProcTaskData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull ProcTaskData oldItem,
        @NonNull ProcTaskData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiProcTaskBinding binding;

    public ViewHolder(View view) {
      super(view);
      binding = LiProcTaskBinding.bind(view);
    }

    public void bind(ProcTaskData ptd, int position) {
      setObject(ptd, position);
    }

    private void setObject(ProcTaskData ptd, int position) {
      binding.tvTaskName.setText(ptd.getName());
      binding.tvStartDate.setText(Formatter.DTF_dd_MM_yyyy_HH_mm.format(ptd.getStartDate()));
      if (ptd.getEndDate() != null) {
        binding.tvEndDate.setText(Formatter.DTF_dd_MM_yyyy_HH_mm.format(ptd.getEndDate()));
      } else {
        binding.tvEndDate.setText(null);
      }
      binding.tvUser.setText(ptd.getUserName());
      binding.tvOutcome.setText(ptd.getOutcome());
      binding.tvComment.setText(ptd.getComment());
    }
  }
}
