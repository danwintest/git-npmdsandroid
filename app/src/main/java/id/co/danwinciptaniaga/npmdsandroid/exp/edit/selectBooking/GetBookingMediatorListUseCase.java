package id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking;

import java.util.UUID;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseSort;
import id.co.danwinciptaniaga.npmds.data.booking.BookingListResponse;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;

public class GetBookingMediatorListUseCase {
  private static final String TAG = GetBookingMediatorListUseCase.class.getSimpleName();

  private final ExpenseService expenseService;
  private BookingBrowseSort sort = new BookingBrowseSort();
  private UUID outletId;

  @Inject
  public GetBookingMediatorListUseCase(ExpenseService expenseService) {
    this.expenseService = expenseService;
  }

  public ListenableFuture<BookingListResponse> getMediatorBookingList(Integer page,
      Integer pageSize) {
    ListenableFuture<BookingListResponse> result = expenseService.getBookingMediatorList(outletId,
        page, pageSize, null, sort);
    return result;
  }

  public UUID getOutletId() {
    return outletId;
  }

  public void setOutletId(UUID outletId) {
    this.outletId = outletId;
  }

  public BookingBrowseSort getSort() {
    return sort;
  }

  public void setSort(BookingBrowseSort sort) {
    this.sort = sort;
  }
}
