package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import javax.inject.Inject;

import org.acra.ACRA;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.BaseActivity;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentTugasTransferWfBinding;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfProcessParameter;

@AndroidEntryPoint
public class TugasTransferWfFragment extends ProtectedFragment {
  private final static String TAG = TugasTransferWfFragment.class.getSimpleName();
  FragmentTugasTransferWfBinding binding;
  private RecyclerView rv;
  private TugasTransferWfVM vm;
  private TugasTransferBrowseWfListAdapter adapter;
  protected ArrayAdapter<KeyValueModel<UUID, String>> sourceAccAdapter;

  @Inject
  AppExecutors appExecutors;

  @Inject
  LoginUtil loginUtil;

  public TugasTransferWfFragment() {
  }

  public static TugasTransferWfFragment newInstance(String param1, String param2) {
    TugasTransferWfFragment fragment = new TugasTransferWfFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    if (vm.getIsShowHitungPenyesuaian()) {
      menu.add(0, 0, 0, "Penyesuaian")
          .setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
    }
    super.onCreateOptionsMenu(menu, inflater);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if ("Penyesuaian" == item.getTitle()) {
      BigDecimal allowanceRate = vm.getAllowanceRate();

      NavController nc = Navigation.findNavController(binding.getRoot());
      TugasTransferWfFragmentDirections.ActionHitungPenyesuaian dir = TugasTransferWfFragmentDirections.actionHitungPenyesuaian(
          allowanceRate);
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    }
    return super.onContextItemSelected(item);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    setHasOptionsMenu(true);
    super.onCreate(savedInstanceState);
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    if (status.getStatus() == Status.LOADING) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      loadData();
    } else if (status.getStatus() == Status.ERROR) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.GONE);
      binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);

      // retry = restart fragment (karena authentication gagal)
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    HyperLog.d(TAG, "onCreateView TERPANGGIL");
    binding = FragmentTugasTransferWfBinding.inflate(inflater, container, false);
    vm = new ViewModelProvider(this).get(TugasTransferWfVM.class);
    TugasTransferWfFragmentArgs args = TugasTransferWfFragmentArgs.fromBundle(getArguments());
    vm.setDataList(getDataList(args.getDataList()));
    vm.setDecisionList(getDecisionList(args.getDecision()));
    vm.setAuto(args.getIsAuto());
    setFormState();
    setOtpProcessObserver();
    setCompanyName();
    setAdapterData();
    if (vm.getIsShowAdditionalField()) {
      setFieldTransferDate();
      setFieldSourceAcc();
    }
    setTotalAmt();
    setButtonProcess();
    setHitungPenyesuaianDialog();
    return binding.getRoot();
  }

  private List<TugasTransferBrowseDetailData> getDataList(String dlString) {
    List<TugasTransferBrowseDetailData> dl = new ArrayList<>();
    Type dataType = new TypeToken<List<TugasTransferBrowseDetailData>>() {
    }.getType();
    dl = new Gson().fromJson(dlString, dataType);
    return dl;
  }

  private List<String> getDecisionList(String dcListString) {
    List<String> dcList = new ArrayList<>();
    Type dataType = new TypeToken<List<String>>() {
    }.getType();
    dcList = new Gson().fromJson(dcListString, dataType);
    return dcList;
  }


  private void setFormState() {
    vm.getFormState().observe(getViewLifecycleOwner(), state -> {
      if (FormState.State.LOADING.equals(state.getState())) {
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
      } else if (FormState.State.READY.equals(state.getState())) {
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
      } else if (FormState.State.ERROR.equals(state.getState())) {
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), state.getMessage(), Toast.LENGTH_LONG).show();
      }
    });

    vm.getFormStateProcess().observe(getViewLifecycleOwner(),state->{
      switch(state.getState()){
      case LOADING:
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
          break;
      case READY:
        Toast.makeText(getContext(),state.getMessage(),Toast.LENGTH_LONG).show();
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        NavController nc = NavHostFragment.findNavController(TugasTransferWfFragment.this);
        //        nc.getPreviousBackStackEntry().getSavedStateHandle().set(
        //            ARG_DROPPING_DAILY_DETAIL_FILTER, vm.getFilterField());
        nc.popBackStack();
        break;
      case ERROR:
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        Toast.makeText(getContext(),state.getMessage(),Toast.LENGTH_LONG).show();
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        break;
      }
    });

    vm.getFormStateGetOtp().observe(getViewLifecycleOwner(),state->{
      switch(state.getState()){
      case LOADING:
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
        break;
      case READY:
        Toast.makeText(getContext(),state.getMessage(),Toast.LENGTH_LONG).show();
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        break;
      case ERROR:
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        Toast.makeText(getContext(),state.getMessage(),Toast.LENGTH_LONG).show();
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        break;
      }
    });
  }

  private void setOtpProcessObserver() {
    vm.getOtpVerifyFailMsg().observe(getViewLifecycleOwner(), msg -> {
      if (msg != null) {
        BaseActivity act = ((BaseActivity) getActivity());
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        AccountManager am = AccountManager.get(getContext());
        Account a = loginUtil.getAccount(App.ACCOUNT_TYPE);
        if (a != null) {
          am.invalidateAuthToken(App.ACCOUNT_TYPE, loginUtil.getSavedToken());
        }

        act.logout();
        App.gotoLauncher(getContext());
        act.finish();
      }
    });
  }

  private void setAdapterData() {
    rv = binding.listWf;
    adapter = new TugasTransferBrowseWfListAdapter(getContext());
    adapter.setDataList(vm.getDataList());
    rv.setAdapter(adapter);
  }

  private void loadData() {
    vm.loadFormData();
  }

  private void setCompanyName() {
    binding.tvCompanyName.setText(vm.getCompanyName());
  }

  private void setFieldTransferDate() {
    binding.tilTglTransfer.setVisibility(View.VISIBLE);
    vm.getAllowBackDate().observe(getViewLifecycleOwner(), isAllow -> {
      binding.tilTglTransfer.setEnabled(isAllow);
    });

    vm.getIsFieldTglTransferOk().observe(getViewLifecycleOwner(),isOk->{
      binding.etTglTransfer.setError(isOk ? null : getString(R.string.validation_mandatory));
    });

    vm.getFieldTglTransfer().observe(getViewLifecycleOwner(), date -> {
      LocalDate defaultDate = LocalDate.now();
      String dateString = defaultDate.format(Formatter.DTF_dd_MM_yyyy);
      if (date != null)
        dateString = date.format(Formatter.DTF_dd_MM_yyyy);
      binding.etTglTransfer.setText(dateString);
    });

    binding.etTglTransfer.setOnClickListener(v -> {
      LocalDate trxDate = vm.getFieldTglTransfer().getValue() != null
          ? vm.getFieldTglTransfer().getValue()
          : LocalDate.now();
      int day = trxDate.getDayOfMonth();
      int month = trxDate.getMonthValue() - 1;
      int year = trxDate.getYear();
      DatePickerDialog dpdTrxDate = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate trxDate = LocalDate.of(year, month + 1, dayOfMonth);
              vm.setFieldTglTransfer(trxDate);
            }
          }, year, month, day);
      dpdTrxDate.show();
    });

  }

  private void setFieldSourceAcc() {
    binding.tilRekening.setVisibility(View.VISIBLE);
    sourceAccAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    binding.spRekening.setAdapter(sourceAccAdapter);
    binding.spRekening.setFocusable(false);

    vm.getIsFieldSourceAccIdOk().observe(getViewLifecycleOwner(), isOk -> {
      binding.spRekening.setError(isOk ? null : getString(R.string.validation_mandatory));
    });

    vm.getSourceAccKvmList().observe(getViewLifecycleOwner(), dataList -> {
      sourceAccAdapter.addAll(dataList);
      sourceAccAdapter.notifyDataSetChanged();

      // Set Listener
      binding.spRekening.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
            int i, long l) {
          KeyValueModel<UUID, String> selectedObj = sourceAccAdapter.getItem(i);
          vm.setFieldSelectedSourceAccId(selectedObj.getKey());
        }

        @Override
        public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
          vm.setFieldTglTransfer(null);
        }
      });
    });
  }

  private void setTotalAmt() {
    vm.getTotalAmt().observe(getViewLifecycleOwner(), totalAmt -> {
      binding.tvTotalAmt.setText(Utility.getFormattedAmt(totalAmt));
    });
  }

  private void setButtonProcess() {
    binding.llWorkflowButtonContainer.setVisibility(View.VISIBLE);
    if (vm.isContainingAutomaticTask()) {
      Utility.renderCustomWfButton(vm.getDecisionList(), binding.llWorkflowButtonContainer,
          getLayoutInflater(), getActivity(), this::WfValidationProcess, this::handleWfAction,
          false);
    } else {
      Utility.renderCustomWfButton(vm.getDecisionList(), binding.llWorkflowButtonContainer,
          getLayoutInflater(), getActivity(), this::WfValidationProcess, this::handleWfAction,
          false);
    }
  }

  private void handleWfAction(WfProcessParameter paramObj) {
    String decision = paramObj.getDecision();
    String comment = paramObj.getComment();
    String otpCode = paramObj.getOtp();
    boolean isNotSupportedWF = false;

    switch (decision) {
    case WorkflowConstants.WF_OUTCOME_REQ_OTP:
      vm.processGenerateOTP();
      break;
    case Utility.WF_OUTCOME_APPROVE_TRANSFER_OTOMATIS:
      vm.processApproveTransferOtomatis(comment);
      break;
    case Utility.WF_OUTCOME_APPROVE_TRANSFER_MANUAL:
      vm.processApproveTransferManual(comment);
      break;
    case WorkflowConstants.WF_OUTCOME_APPROVE:
      vm.processApprove(comment, otpCode);
      break;
    case WorkflowConstants.WF_OUTCOME_RETURN:
      vm.processReturn(comment);
      break;
    case WorkflowConstants.WF_OUTCOME_REJECT:
      vm.processReject(comment);
      break;
    default:
      isNotSupportedWF = true;
      break;
    }
    if (isNotSupportedWF) {
      String scMode = String.format("Screen Mode[%s] ", decision);
      String wf = String.format("wfOutCome[%s] ", decision);
      String msg = scMode + wf + " Not Supported";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      HyperLog.w(TAG, msg);
      msg = TAG + " handleWfAction_Txn() " + msg;
      RuntimeException re = new RuntimeException(msg);
      ACRA.getErrorReporter().handleException(re);
    }
  }

  private void setHitungPenyesuaianDialog() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<BigDecimal> allowanceRate = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        TugasTransferHitungPenyesuaianFragment.TUGAS_TRANSFER_PENYESUAIAN_FIELD);
    allowanceRate.observe(getViewLifecycleOwner(), val -> {
      vm.allowanceRateCalculate(val);
      adapter.setDataList(vm.getDataList());
      adapter.notifyDataSetChanged();
      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
          TugasTransferHitungPenyesuaianFragment.TUGAS_TRANSFER_PENYESUAIAN_FIELD);
    });
  }

  private Boolean WfValidationProcess(WfProcessParameter paramObj){
    boolean isOk = true;
    String decision = paramObj.getDecision();
    switch (decision){
    case Utility.WF_OUTCOME_APPROVE_TRANSFER_OTOMATIS:
      if (vm.getIsFieldSourceAccIdOk().getValue() && vm.getIsFieldTglTransferOk().getValue())
        isOk = true;
      else
        isOk = false;

      if(isOk){
        LocalDate txnDate = vm.getFieldTglTransfer().getValue();
        LocalDate currentDate = LocalDate.now();
        if(!Objects.equals(currentDate,txnDate)){
          String msg = getContext().getString(R.string.msg_not_supported_backdate_automatic_trx);
          Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
          return false;
        }
      }
      break;
    case Utility.WF_OUTCOME_APPROVE_TRANSFER_MANUAL:
      if (vm.getIsFieldSourceAccIdOk().getValue() && vm.getIsFieldTglTransferOk().getValue())
        isOk = true;
      else
        isOk = false;
      break;
    }
    return isOk;
  }
}