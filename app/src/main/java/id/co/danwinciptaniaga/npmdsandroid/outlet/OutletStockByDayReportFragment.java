package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.util.UUID;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.hypertrack.hyperlog.HyperLog;

import android.app.PendingIntent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDeepLinkBuilder;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentOutletStockByDayReportBinding;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.util.AnimationUtil;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OutletStockByDayReportFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
public class OutletStockByDayReportFragment extends ProtectedFragment {
  private static final String TAG = OutletStockByDayReportFragment.class.getSimpleName();

  private static final String ARG_OUTLET_ID = "outletId";

  private OutletStockReportByDayVM viewModel;
  private FragmentOutletStockByDayReportBinding binding;
  private OutletStockByDayReportAdapter adapter;

  private View.OnClickListener restart = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // kita coba restart seluruh activity
      Bundle bundle = new Bundle();
      bundle.putSerializable(ARG_OUTLET_ID, viewModel.getOutletId());
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_outlet_stock_by_day_report)
          .setArguments(bundle)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragment = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      viewModel.loadReport(true);
      binding.swipeRefresh.setRefreshing(false);
    }
  };

  public OutletStockByDayReportFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param outletId outletId
   * @return A new instance of fragment OutletBalanceReportFragment.
   */
  public static OutletStockByDayReportFragment newInstance(UUID outletId) {
    OutletStockByDayReportFragment fragment = new OutletStockByDayReportFragment();
    Bundle args = new Bundle();
    args.putSerializable(ARG_OUTLET_ID, outletId);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    HyperLog.d(TAG, "onCreate called");
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    viewModel = new ViewModelProvider(requireActivity()).get(OutletStockReportByDayVM.class);
    OutletStockByDayReportFragmentArgs args = OutletStockByDayReportFragmentArgs
        .fromBundle(getArguments());
    // kalau sampai onCreate dipanggil, maka selalu clearFormState
    viewModel.clearFormState();
    viewModel.setOutletId(args.getOutletId());
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    HyperLog.d(TAG, "onCreateView called");
    binding = FragmentOutletStockByDayReportBinding.inflate(inflater, container, false);
    viewModel.getFormState().observe(getViewLifecycleOwner(), this::onFormStateChange);

    adapter = new OutletStockByDayReportAdapter();
    binding.list.setAdapter(adapter);

    binding.swipeRefresh.setOnRefreshListener(() -> {
      viewModel.loadReport(true);
      binding.swipeRefresh.setRefreshing(false);
    });

    return binding.getRoot();
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    HyperLog.d(TAG, "onCreateOptionsMenu called");
    inflater.inflate(R.menu.outlet_stock_by_day_report_menu, menu);
  }

  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (item.getItemId() == R.id.menu_filter) {
      BottomSheetDialogFragment bsdf = new OutletStockByDayReportFilterFragment();
      bsdf.show(getParentFragmentManager(), "Filter");
      return true;
    }
    return false;
  }

  private void onFormStateChange(FormState formState) {
    HyperLog.d(TAG, "onFormStateChange called: " + formState);
    if (formState == null)
      return;
    switch (formState.getState()) {
    case READY:
      bindProgressAndPageContentReady(AnimationUtil.fadeoutAnimationMedium(),
          AnimationUtil.fadeinAnimationMedium());
      break;
    case ERROR:
      bindProgressAndPageContentError(formState, AnimationUtil.fadeoutAnimationMedium(),
          AnimationUtil.fadeinAnimationMedium());
      break;
    case LOADING:
    default:
      bindProgressAndPageContentLoading(formState, AnimationUtil.fadeoutAnimationMedium(),
          AnimationUtil.fadeinAnimationMedium());
      break;
    }
  }

  private void bindProgressAndPageContentReady(Animation outAnimation, Animation inAnimation) {
    HyperLog.d(TAG, "bindProgressAndPageContentReady");
    binding.tvCompany.setText(viewModel.getOsrResponse().getCompanyName());
    binding.tvOutlet.setText(String.format("%s (%s)", viewModel.getOsrResponse().getOutletName(),
        viewModel.getOsrResponse().getOutletCode()));

    // summary
    binding.tvPrevBalanceQty.setText(
        Formatter.DF_AMOUNT_NO_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getPreviousBalanceQty()));
    binding.tvPrevBalanceValue.setText(
        Formatter.DF_AMOUNT_NO_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getPreviousBalanceValue()));
    binding.tvPrevBalanceAvgCost.setText(
        Formatter.DF_AMOUNT_2_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getPreviousAverageCost()));

    binding.tvQtyIn.setText(
        Formatter.DF_AMOUNT_NO_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getCurrentPurchaseQty()));
    binding.tvValueIn.setText(
        Formatter.DF_AMOUNT_NO_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getCurrentPurchaseValue()));
    binding.tvAvgInCost.setText(
        Formatter.DF_AMOUNT_2_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getCurrentAverageCost()));

    binding.tvQtyOut.setText(
        Formatter.DF_AMOUNT_NO_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getUsedQty()));
    binding.tvValueOut.setText(
        Formatter.DF_AMOUNT_NO_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getUsedValue()));
    binding.tvAvgOutCost.setText(
        Formatter.DF_AMOUNT_2_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getUsedAverageCost()));

    binding.tvBalanceQty.setText(
        Formatter.DF_AMOUNT_NO_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getFinalBalanceQty()));
    binding.tvBalanceValue.setText(
        Formatter.DF_AMOUNT_NO_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getFinalBalanceValue()));
    binding.tvBalanceAvgCost.setText(
        Formatter.DF_AMOUNT_2_DECIMAL
            .format(viewModel.getOsrResponse().getSummaryData().getFinalAverageCost()));

    adapter.submitList(viewModel.getOsrResponse().getDataList());

    ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE, outAnimation);
    ViewUtil.setVisibility(binding.pageContent, View.VISIBLE, inAnimation);
  }

  private void bindProgressAndPageContentError(FormState formState, Animation outAnimation,
      Animation inAnimation) {
    HyperLog.d(TAG, "bindProgressAndPageContentError");
    if (formState.getMessage() != null) {
      binding.progressWrapper.progressText.setText(formState.getMessage());
    } else {
      binding.progressWrapper.progressText.setText(getString(R.string.error_contact_support));
    }
    ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.GONE);
    ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE, inAnimation);
    ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.VISIBLE);
    ViewUtil.setVisibility(binding.pageContent, View.GONE, outAnimation);
  }

  private void bindProgressAndPageContentLoading(FormState formState, Animation outAnimation,
      Animation inAnimation) {
    HyperLog.d(TAG, "bindProgressAndPageContentLoading");
    if (formState.getMessage() != null) {
      binding.progressWrapper.progressText.setText(formState.getMessage());
    } else {
      binding.progressWrapper.progressText.setText(getString(R.string.msg_please_wait));
    }
    ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.VISIBLE);
    ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
    binding.progressWrapper.retryButton.setOnClickListener(reloadFragment);
    ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE, inAnimation);
    ViewUtil.setVisibility(binding.pageContent, View.GONE, outAnimation);
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    if (status.getStatus() == Status.LOADING) {
      ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
      ViewUtil.setVisibility(binding.swipeRefresh, View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.GONE);
      ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.VISIBLE);
      // retry = restart fragment (karena authentication gagal)
      binding.progressWrapper.retryButton.setOnClickListener(restart);
    }
  }

  @UiThread
  private void onReady() {
    // trigger loading pertama kali setelah authentication Success
    viewModel.loadReport(false);
  }
}