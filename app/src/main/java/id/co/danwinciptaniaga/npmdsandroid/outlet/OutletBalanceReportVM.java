package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.LoadState;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceReportData;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceReportResponse;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletReportSort;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseFormState;

public class OutletBalanceReportVM extends AndroidViewModel
    implements GetOutletBalanceReportUseCase.Listener {

  private final GetOutletBalanceReportUseCase ucGetOutletReport;

  // initial value = null supaya observer dipanggil pertama kali dengan nilai null
  // dan loading dilakukan di sana
  private MutableLiveData<FormState> formState = new MutableLiveData<>(null);
  private UUID outletId;
  private MutableLiveData<LoadState> outletBalanceReportLoadState = new MutableLiveData<>();

  private LocalDate statementDateFromFilter = null;
  private LocalDate statementDateToFilter = null;

  private OutletBalanceReportResponse obrResponse = null;

  @ViewModelInject
  public OutletBalanceReportVM(@NonNull Application application,
      GetOutletBalanceReportUseCase getOutletBalanceReportUseCase) {
    super(application);
    this.ucGetOutletReport = getOutletBalanceReportUseCase;
    LocalDate fromDate = LocalDate.now().withDayOfMonth(1);
    LocalDate toDate = LocalDate.now();

    setFilter(fromDate, toDate);

    ucGetOutletReport.registerListener(this);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.ucGetOutletReport.unregisterListener(this);
  }

  public UUID getOutletId() {
    return outletId;
  }

  public void setOutletId(UUID outletId) {
    this.outletId = outletId;
  }

  public LiveData<FormState> getFormState() {
    return formState;
  }

  public void clearFormState() {
    obrResponse = null;
    formState.setValue(null);
  }

  public void loadReport(boolean forceReload) {
    // skip loading kalau sudah ada isi atau bukan dipaksa clear
    if (forceReload || obrResponse == null) {
      ucGetOutletReport.getOutletBalanceReport(outletId);
    }
  }

  public LiveData<LoadState> getOutletBalanceReportLoadState() {
    return outletBalanceReportLoadState;
  }

  public void setOutletBalanceReportLoadState(LoadState loadState) {
    this.outletBalanceReportLoadState.postValue(loadState);
  }

  public OutletBalanceReportResponse getObrResponse() {
    return obrResponse;
  }

  @Override
  public void onGetBalanceReportStarted() {
    formState.postValue(FormState.loading(getApplication().getString(R.string.memuat_data)));
  }

  @Override
  public void onGetBalanceReportSuccess(Resource<OutletBalanceReportResponse> result) {
    obrResponse = result.getData();
    formState.postValue(FormState.ready(result.getMessage()));
  }

  @Override
  public void onGetBalanceReportFailure(Resource<OutletBalanceReportResponse> responseError) {
    String message = Resource.getMessageFromError(responseError,
        getApplication().getString(R.string.error_contact_support));
    formState.postValue(FormState.error(message));
  }

  public LocalDate getStatementDateFromFilter() {
    return statementDateFromFilter;
  }

  public LocalDate getStatementDateToFilter() {
    return statementDateToFilter;
  }

  public void setFilter(LocalDate statementDateFrom, LocalDate statementDateTo) {
    this.statementDateFromFilter = statementDateFrom;
    ucGetOutletReport.setStatementDateFrom(this.statementDateFromFilter);
    this.statementDateToFilter = statementDateTo;
    ucGetOutletReport.setStatementDateTo(this.statementDateToFilter);
    this.loadReport(true);
  }
}
