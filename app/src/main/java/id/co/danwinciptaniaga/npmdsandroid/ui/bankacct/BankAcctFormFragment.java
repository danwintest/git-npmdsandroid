package id.co.danwinciptaniaga.npmdsandroid.ui.bankacct;

import org.jetbrains.annotations.NotNull;

import com.tiper.MaterialSpinner;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.androcon.utility.Utility;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmdsandroid.BaseFragment;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentBankAcctFormBinding;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalEditFragment;
import id.co.danwinciptaniaga.npmdsandroid.util.AnimationUtil;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

@AndroidEntryPoint
public class BankAcctFormFragment extends BaseFragment {

  private FragmentBankAcctFormBinding binding;
  private BankAcctFormVM viewModel;
  private ArrayAdapter banksAdapter;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(this).get(BankAcctFormVM.class);

    BankAcctFormFragmentArgs args = BankAcctFormFragmentArgs.fromBundle(getArguments());
    BankAccountData bad = args.getBankAccountData();
    viewModel.setBankAccountData(bad);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentBankAcctFormBinding.inflate(inflater, container, false);
    viewModel.getFormState().observe(getViewLifecycleOwner(), this::onFormStateChange);

    setupSwitchOthers();
    setupBank();
    setupAcctName();
    setupAcctNo();
    setupButtons();

    return binding.getRoot();
  }

  private void setupSwitchOthers() {
    binding.swLainLain.setOnCheckedChangeListener((buttonView, isChecked) -> {
      if (isChecked) {
        // bank lain-lain
        binding.tilBank.setVisibility(View.GONE);
        binding.tilBankManual.setVisibility(View.VISIBLE);
        viewModel.setOthers(true);
        viewModel.setSelectedBank(null);
      } else {
        // bank dari pilihan
        binding.tilBank.setVisibility(View.VISIBLE);
        binding.tilBankManual.setVisibility(View.GONE);
        viewModel.setOthers(false);
      }
    });
  }

  private void setupBank() {
    binding.spBank.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
          @org.jetbrains.annotations.Nullable View view, int i, long l) {
        viewModel.setSelectedBank(i);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
        viewModel.setSelectedBank(null);
      }
    });

    binding.etBankManual.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          viewModel.setOtherBankName(Utility.getTrimmedString(s));
        else
          viewModel.setOtherBankName(null);
      }
    });

    viewModel.getOtherBankNameErrorLd().observe(getViewLifecycleOwner(), s -> {
      binding.tilBankManual.setError(s);
    });
  }

  private void setupAcctName() {
    binding.etAccountName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          viewModel.setAccountName(Utility.getTrimmedString(s));
        else
          viewModel.setAccountName(null);
      }
    });

    viewModel.getAccountNameErrorLd().observe(getViewLifecycleOwner(), s -> {
      binding.tilAccountName.setError(s);
    });
  }

  private void setupAcctNo() {
    binding.etAccountNo.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          viewModel.setAccountNo(Utility.getTrimmedString(s));
        else
          viewModel.setAccountNo(null);
      }
    });

    viewModel.getAccountNoLengthLd().observe(getViewLifecycleOwner(), l -> {
      if (l != null) {
        binding.etAccountNo.setFilters(
            new InputFilter[] { new InputFilter.LengthFilter(l) });
      } else {
        binding.etAccountNo.setFilters(new InputFilter[0]);
      }
    });

    viewModel.getAccountNoErrorLd().observe(getViewLifecycleOwner(), s -> {
      binding.tilAccountNo.setError(s);
    });
  }

  private void setupButtons() {
    binding.btnOk.setOnClickListener(v -> {
      if (viewModel.validateForm()) {
        BankAccountData bad = viewModel.getBankAccountData();
        if (!viewModel.isOthers()) {
          bad.setBankId(viewModel.getBankId());
          bad.setBankName(viewModel.getBankName());
          bad.setBankCode(viewModel.getBankCode());
          bad.setOtherBankName(null);
        } else {
          bad.setBankId(null);
          bad.setBankName(null);
          bad.setBankCode(null);
          bad.setOtherBankName(viewModel.getOtherBankName());
        }
        bad.setAccountName(viewModel.getAccountName());
        bad.setAccountNo(viewModel.getAccountNo());
        NavController navController = Navigation.findNavController(v);
        navController.getPreviousBackStackEntry().getSavedStateHandle()
            .set(DroppingAdditionalEditFragment.SAVED_STATE_LD_ALT_BANK_ACCT_DATA, bad);
        navController.popBackStack();
      }
    });
  }

  private void onFormStateChange(FormState formState) {
    if (formState == null) {
      // tahap ini dilakukan 1 kali saja, yaitu sewaktu pertama kali FormState masih null
      if (viewModel.getBankAccountData() != null
          && viewModel.getBankAccountData().getId() != null) {
        viewModel.loadBankAccountData();
      } else {
        viewModel.prepareNewBankAccountData();
      }
      return;
    } else {
      switch (formState.getState()) {
      case READY:
        bindProgressAndPageContentReady(AnimationUtil.fadeoutAnimationMedium(),
            AnimationUtil.fadeinAnimationMedium());
        break;
      case ACTION_IN_PROGRESS:
        bindProgressAndPageContentAip(formState, AnimationUtil.fadeinAnimationShort());
        break;
      case LOADING:
      default:
        bindProgressAndPageContentLoading(formState, AnimationUtil.fadeoutAnimationMedium(),
            AnimationUtil.fadeinAnimationMedium());
        break;
      }
    }
  }

  private void bindProgressAndPageContentReady(Animation outAnimation, Animation inAnimation) {
    banksAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    banksAdapter.addAll(viewModel.getBankKvms());
    banksAdapter.notifyDataSetChanged();
    binding.spBank.setAdapter(banksAdapter);
    if (viewModel.getSelectedBank() != null) {
      binding.spBank.setSelection(viewModel.getSelectedBank());
    }

    binding.swLainLain.setChecked(viewModel.isOthers());
    if (viewModel.isOthers()) {
      binding.etBankManual.setText(viewModel.getOtherBankName());
      binding.tilBank.setVisibility(View.GONE);
      binding.tilBankManual.setVisibility(View.VISIBLE);
    } else {
      binding.etBankManual.setText(null);
      if (viewModel.getLastValidated() != null) {
        binding.tvDate.setText(Formatter.DTF_dd_MM_yyyy_HH_mm.format(viewModel.getLastValidated()));
        binding.tvDate.setVisibility(View.VISIBLE);
      } else {
        binding.tvDate.setText(null);
        binding.tvDate.setVisibility(View.GONE);
      }
      if (viewModel.isValidated()) {
        binding.ivDate.setVisibility(View.VISIBLE);
      } else {
        binding.ivDate.setVisibility(View.GONE);
      }
      binding.tilBank.setVisibility(View.VISIBLE);
      binding.tilBankManual.setVisibility(View.GONE);
    }

    binding.etAccountName.setText(viewModel.getAccountName());
    binding.etAccountNo.setText(viewModel.getAccountNo());

    binding.progressWrapper.progressView.setAnimation(outAnimation);
    binding.progressWrapper.progressView.setVisibility(View.GONE);
    binding.pageContent.setAnimation(inAnimation);
    binding.pageContent.setVisibility(View.VISIBLE);
  }

  private void bindProgressAndPageContentAip(FormState formState, Animation inAnimation) {
    if (formState.getMessage() != null) {
      binding.progressWrapper.progressText.setText(formState.getMessage());
    }
    binding.progressWrapper.progressView.setAnimation(inAnimation);
    binding.progressWrapper.progressView.setVisibility(View.VISIBLE);
    // tidak mengubah visibilitas pageContent
  }

  private void bindProgressAndPageContentLoading(FormState formState, Animation outAnimation,
      Animation inAnimation) {
    if (formState.getMessage() != null) {
      binding.progressWrapper.progressText.setText(formState.getMessage());
    }
    binding.progressWrapper.progressView.setAnimation(inAnimation);
    binding.progressWrapper.progressView.setVisibility(View.VISIBLE);
    binding.pageContent.setAnimation(outAnimation);
    binding.pageContent.setVisibility(View.GONE);
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    if (status.getStatus() == Status.LOADING) {
      binding.tvStatus.setVisibility(View.VISIBLE);
      binding.tvStatus.setText(R.string.msg_please_wait);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.tvStatus.setText(status.getMessage());
      binding.tvStatus.setVisibility(View.GONE);
    }
  }
}
