package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import java.util.UUID;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBankListResponse;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;

public class BankAccountBrowseUC {
  private static final String TAG = BankAccountBrowseUC.class.getSimpleName();

  private final AppBookingService service;
  private String filter;
  private String txnMode;
  private UUID outletId;

  @Inject
  public BankAccountBrowseUC(AppBookingService service) {
    this.service = service;
  }

  public ListenableFuture<AppBookingBankListResponse> getBankAccountDataList(Integer page,
      Integer pageSize) {
    HyperLog.d(TAG, "getBankList terpanggil");
    ListenableFuture<AppBookingBankListResponse> res = service.getBankAccountByOutlet(page,
        pageSize, filter, outletId, txnMode);
    return res;
  }

  public void setFilter(String filter) {
    this.filter = filter;
  }


  public void setTxnMode(String txnMode) {
    this.txnMode = txnMode;
  }

  public void setOutletId(UUID outletId) {
    this.outletId = outletId;
  }
}
