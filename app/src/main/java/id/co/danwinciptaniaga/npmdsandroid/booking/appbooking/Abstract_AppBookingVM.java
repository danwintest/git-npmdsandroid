package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import org.acra.ACRA;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.ImageUtility;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingDetailResponse;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmds.data.booking.NewBookingReponse;
import id.co.danwinciptaniaga.npmds.data.common.LobShortData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.common.PoPendingReasonShortData;
import id.co.danwinciptaniaga.npmds.data.common.SoShortData;
import id.co.danwinciptaniaga.npmds.data.common.UtilityState;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import okhttp3.ResponseBody;

public abstract class Abstract_AppBookingVM extends AndroidViewModel implements
    AppBookingPrepareUC.Listener, AppBookingProcessUC.Listener, AppBookingLoadUC.Listener {
  private static final String TAG = Abstract_AppBookingVM.class.getSimpleName();
  /**
   * flag yang menandakan bahwa fungsi yang sedang berjalan adalah bagian dari LaodData(proses membuka form pertamakali)
   * sehingga, semua validasi yang ada diabaikan
   */
  protected boolean isLoadProcess = false;
  protected Boolean isFirstTimeLoadFromEditorCancel = false;
  protected boolean hasBeenInitializedForm = false;
  protected boolean isTransferFieldEnabled = false;

  protected BigDecimal staticConsumerAmt = BigDecimal.ZERO;
  protected BigDecimal staticFifAmt = BigDecimal.ZERO;
  protected BigDecimal staticBjAmt = BigDecimal.ZERO;

  public final UtilityService utilityService;
  public final AppBookingService appBookingService;
  public final AppBookingPrepareUC prepareUC;
  public final AppBookingLoadUC appBookingLoadUC;
  public final AppBookingProcessUC processUC;
  public final ListeningExecutorService bgExecutor;

  public boolean allowBackdate = false;
  private String attachMode;

  //  private MutableLiveData<AppBookingCashScreenMode> screenMode = new MutableLiveData<>();

  public AppExecutors appExecutors;
  public NewBookingReponse prepareFormResponse = null;
  public AppBookingDetailResponse detailResponse = null;

  // --BLOCK_VARIABLE yang dibutuhkan untuk menampilkan data pada header (HANYA TEXTVIEW)
  private MutableLiveData<String> header_PtName = new MutableLiveData<>();
  private MutableLiveData<String> header_Status = new MutableLiveData<>();
  private MutableLiveData<String> header_StatusCode = new MutableLiveData<>();
  private MutableLiveData<String> fieldWfStatus = new MutableLiveData<>();
  private MutableLiveData<String> header_OutletName = new MutableLiveData<>();
  private MutableLiveData<String> header_OutletCode = new MutableLiveData<>();
  private MutableLiveData<String> header_JenisPengajuan = new MutableLiveData<>();
  private MutableLiveData<String> header_BookingDate = new MutableLiveData<>();
  private MutableLiveData<String> header_PengajuanDate = new MutableLiveData<>();
  // --#BLOCK_VARIABLE yang dibutuhkan untuk menampilkan data pada header (HANYA TEXTVIEW)

  // --BLOCK_VARIABLE data awal yang dibutuhkan pada halaman

  private MutableLiveData<String> field_TrxNo = new MutableLiveData<>();
  private MutableLiveData<AppBookingFormState> formState = new MutableLiveData<>(null);
  private MutableLiveData<String> otpVerifyFailMsg = new MutableLiveData<>();

  private MutableLiveData<Boolean> validateVehilceProgress = new MutableLiveData<>();

  // --#BLOCK_VARIABLE data awal yang dibutuhkan pada halaman

  private UUID field_AppBookingId = null;
  private MutableLiveData<String> appBookingTypeId = new MutableLiveData<>();
  private String appOperation;
  private MutableLiveData<List<OutletShortData>> outletList_Ld = new MutableLiveData<>();
  private MutableLiveData<List<SoShortData>> soList_Ld = new MutableLiveData<>();
  private MutableLiveData<List<LobShortData>> lobList_Ld = new MutableLiveData<>();
  private MutableLiveData<List<PoPendingReasonShortData>> poprList_Ld = new MutableLiveData<>();

  // variable yang digunakan untuk manipulasi data
  private MutableLiveData<UUID> field_ProcTaskId = new MutableLiveData<>();
  private MutableLiveData<List<String>> wfTaskList = new MutableLiveData<>();
  private MutableLiveData<KeyValueModel<OutletShortData, String>> field_outletKvm = new MutableLiveData<>();

  // variable_outlet
  protected boolean field_outletKvm_OK = false;
  protected MutableLiveData<String> field_outletKvm_ERROR = new MutableLiveData<>();
  // variable_outlet

  private MutableLiveData<String> field_ConsumerNo = new MutableLiveData<>();
  private MutableLiveData<LocalDate> field_BookingDate = new MutableLiveData<>();

  // variable lob
  private MutableLiveData<KeyValueModel<LobShortData, String>> field_LobKvm = new MutableLiveData<>();
  protected boolean field_lobKvm_OK = false;
  protected MutableLiveData<String> field_LobKvm_ERROR = new MutableLiveData<>();
  // variable lob

  // variable so
  private MutableLiveData<KeyValueModel<SoShortData, String>> field_SoKvm = new MutableLiveData<>();
  protected boolean field_SoKvm_OK = false;
  protected MutableLiveData<String> field_SoKvm_ERROR = new MutableLiveData<>();
  // variable so

  // variable IDNPKSF
  private MutableLiveData<String> field_Idnpksf = new MutableLiveData<>();
  protected boolean field_Idnpksf_OK = true;
  protected MutableLiveData<String> field_Idnpksf_ERROR = new MutableLiveData<>();
  // variable IDNPKSF

  private MutableLiveData<String> field_AlasanPinjam = new MutableLiveData<>();
  protected boolean field_AlasanPinjam_OK = false;
  protected MutableLiveData<String> field_AlasanPinjam_ERROR = new MutableLiveData<>();

  private MutableLiveData<String> field_AlasanPinjamCash = new MutableLiveData<>();
  protected boolean field_AlasanPinjamCash_OK = false;
  protected MutableLiveData<String> field_AlasanPinjamCash_ERROR = new MutableLiveData<>();

  private MutableLiveData<String> field_AppNo = new MutableLiveData<>();

  private MutableLiveData<String> field_PoNo = new MutableLiveData<>();
  protected boolean field_PoNo_OK = true;
  protected MutableLiveData<String> field_PoNo_ERROR = new MutableLiveData<>();

  private MutableLiveData<LocalDate> field_PoDate = new MutableLiveData<>();
  protected boolean field_PoDate_OK = true;
  protected MutableLiveData<String> field_PoDate_ERROR = new MutableLiveData<>();

  private File field_Attach_Temp_PO = null;
  protected File field_Attach_Final_PO = null;
  protected UUID att_po_id = null;
  protected Boolean is_att_po_change = false;
  private MutableLiveData<Resource<Bitmap>> field_Attach_ReducedFinal_PO = new MutableLiveData<>();
  protected MutableLiveData<Boolean> field_Attach_FINAL_PO_OK = new MutableLiveData<>();
  private MutableLiveData<Boolean> field_Attach_PO_isArchive = new MutableLiveData<>();

  private MutableLiveData<List<KeyValueModel<UUID, String>>> field_poprList = new MutableLiveData<>();
  protected boolean field_poprList_OK = false;
  protected MutableLiveData<String> field_poprList_ERROR = new MutableLiveData<>();
  protected MutableLiveData<String> field_poprList_WARN = new MutableLiveData<>();

  private MutableLiveData<String> field_ConsumerName = new MutableLiveData<>();
  protected boolean field_ConsumerName_OK = false;
  protected MutableLiveData<String> field_ConsumerName_ERROR = new MutableLiveData<>();

  private MutableLiveData<String> field_ConsumerAddress = new MutableLiveData<>();
  protected boolean field_ConsumerAddress_OK = false;
  protected MutableLiveData<String> field_ConsumerAddress_ERROR = new MutableLiveData<>();

  private MutableLiveData<String> field_ConsumerPhone = new MutableLiveData<>();
  protected boolean field_ConsumerPhone_OK = false;
  protected MutableLiveData<String> field_ConsumerPhone_ERROR = new MutableLiveData<>();

  private MutableLiveData<String> field_VehicleNo = new MutableLiveData<>();
  protected boolean field_VehicleNo_OK = false;
  protected MutableLiveData<String> field_VehicleNo_ERROR = new MutableLiveData<>();

  protected MutableLiveData<Boolean> field_IsOpenClose = new MutableLiveData<>();

  protected MutableLiveData<BigDecimal> field_ConsumerAmount = new MutableLiveData<>();
  protected boolean field_ConsumerAmount_OK = false;
  protected MutableLiveData<String> field_ConsumerAmount_ERROR = new MutableLiveData<>();

  protected MutableLiveData<BigDecimal> field_FIFAmount = new MutableLiveData<>();
  protected boolean field_FIFAmount_OK = true;
  protected MutableLiveData<String> field_FIFAmount_ERROR = new MutableLiveData<>();

  private MutableLiveData<BigDecimal> field_BiroJasaAmount = new MutableLiveData<>();

  protected MutableLiveData<BigDecimal> field_BookingAmount = new MutableLiveData<>();
  protected boolean field_BookingAmount_OK = false;
  protected MutableLiveData<String> field_BookingAmount_ERROR = new MutableLiveData<>();

  private MutableLiveData<BigDecimal> field_FeeMatrix = new MutableLiveData<>();
  protected boolean field_FeeMatrix_OK = true;
  protected MutableLiveData<String> field_FeeMatrix_ERROR = new MutableLiveData<>();
  protected MutableLiveData<Boolean> field_FeeMatrix_ISEDITABLE = new MutableLiveData<>();

  private MutableLiveData<BigDecimal> field_FeeScheme = new MutableLiveData<>(BigDecimal.ZERO);
  private MutableLiveData<String> field_FeeScheme_WARN = new MutableLiveData<>();
  private MutableLiveData<Integer> field_FeeScheme_ISVISIBLE = new MutableLiveData<>();
  private MutableLiveData<Boolean> field_FeeScheme_ISEDITABLE = new MutableLiveData<>();

  protected MutableLiveData<Boolean> readOnly_AppNoPoNoPoDatePoAttchment = new MutableLiveData<>(
      false);

  private File field_Attach_Temp_STNK = null;
  protected File field_Attach_Final_STNK = null;
  protected UUID att_stnk_id = null;
  protected Boolean is_att_stnk_change = false;
  private MutableLiveData<Resource<Bitmap>> field_Attach_ReducedFinal_STNK = new MutableLiveData<>();
  protected MutableLiveData<Boolean> field_Attach_Final_STNK_OK = new MutableLiveData<>();
  private MutableLiveData<Boolean> field_Attach_STNK_isArchive = new MutableLiveData<>();

  private File field_Attach_Temp_KK = null;
  protected File field_Attach_Final_KK = null;
  protected UUID att_kk_id = null;
  protected Boolean is_att_kk_change = false;
  private MutableLiveData<Resource<Bitmap>> field_Attach_ReducedFinal_KK = new MutableLiveData<>();
  protected MutableLiveData<Boolean> field_Attach_Final_KK_OK = new MutableLiveData<>();
  private MutableLiveData<Boolean> field_Attach_KK_isArchive = new MutableLiveData<>();

  private File field_Attach_Temp_BPKB = null;
  protected File field_Attach_Final_BPKB = null;
  protected UUID att_bpkb_id = null;
  protected Boolean is_att_bpkb_change = false;
  private MutableLiveData<Resource<Bitmap>> field_Attach_ReducedFinal_BPKB = new MutableLiveData<>();
  protected MutableLiveData<Boolean> field_Attach_Final_BPKB_OK = new MutableLiveData<>();
  private MutableLiveData<Boolean> field_Attach_BPKB_isArchive = new MutableLiveData<>();

  private File field_Attach_Temp_SPT = null;
  protected File field_Attach_Final_SPT = null;
  protected UUID att_spt_id = null;
  protected Boolean is_att_spt_change= false;
  private MutableLiveData<Resource<Bitmap>> field_Attach_ReducedFinal_SPT = new MutableLiveData<>();
  protected MutableLiveData<Boolean> field_Attach_Final_SPT_OK = new MutableLiveData<>();
  private MutableLiveData<Boolean> field_Attach_SPT_isArchive = new MutableLiveData<>();

  private File field_Attach_Temp_SERAH_TERIMA = null;
  protected File field_Attach_Final_SERAH_TERIMA = null;
  protected UUID att_serah_terima_id = null;
  protected Boolean is_att_serah_terima_change = false;
  private MutableLiveData<Resource<Bitmap>> field_Attach_ReducedFinal_SERAH_TERIMA = new MutableLiveData<>();
  protected MutableLiveData<Boolean> field_Attach_Final_SERAH_TERIMA_OK = new MutableLiveData<>();
  private MutableLiveData<Boolean> field_Attach_SERAH_TERIMA_isArchive = new MutableLiveData<>();

  private File field_Attach_Temp_FISIK_MOTOR = null;
  protected File field_Attach_Final_FISIK_MOTOR = null;
  protected UUID att_fisik_motor_id = null;
  protected Boolean is_att_fisik_motor_change = false;
  private MutableLiveData<Resource<Bitmap>> field_Attach_ReducedFinal_FISIK_MOTOR = new MutableLiveData<>();
  private MutableLiveData<Boolean> field_Attach_FISIK_MOTOR_isArchive = new MutableLiveData<>();

  private File field_Attach_Temp_HO = null;
  protected File field_Attach_Final_HO = null;
  protected UUID att_HO_id = null;
  protected Boolean is_att_HO_change = false;
  private MutableLiveData<Resource<Bitmap>> field_Attach_ReducedFinal_HO = new MutableLiveData<>();
  protected MutableLiveData<Boolean> field_Attach_Final_HO_OK = new MutableLiveData<>();
  private MutableLiveData<Boolean> field_Attach_HO_isArchive = new MutableLiveData<>();

  private File field_Attach_Temp_BUKTIRETUR = null;
  protected File field_Attach_Final_BUKTIRETUR = null;
  protected UUID att_bukti_retur_id = null;
  protected Boolean is_att_bukti_retur_change = false;
  private MutableLiveData<Resource<Bitmap>> field_Attach_ReducedFinal_BUKTIRETUR = new MutableLiveData<>();
  protected MutableLiveData<Boolean> field_Attach_Final_BUKTIRETUR_OK = new MutableLiveData<>();
  private MutableLiveData<Boolean> field_Attach_BUKTIRETUR_isArchive = new MutableLiveData<>();

  private MutableLiveData<Date> field_UpdateTs = new MutableLiveData<>();
  private UUID field_BookingId = null;

  private MutableLiveData<String> field_CancelReason = new MutableLiveData<>();
  private MutableLiveData<String> field_CancelReason_ERROR = new MutableLiveData<>();
  protected boolean field_CancelReason_OK = false;

  private MutableLiveData<String> generalErrorMessage = new MutableLiveData<>();

  protected BigDecimal field_ConsAmount_OLD = BigDecimal.ZERO;
  protected BigDecimal field_FIFAmount_OLD = BigDecimal.ZERO;
  protected BigDecimal field_BiroJasaAmount_OLD = BigDecimal.ZERO;
  protected MutableLiveData<Boolean> showBuktiRetur = new MutableLiveData<>(false);

  protected MutableLiveData<Boolean> field_OperationalTransfer = new MutableLiveData<>(false);

  public Abstract_AppBookingVM(@NonNull Application application, AppExecutors appExecutors,
      AppBookingPrepareUC prepareUC, AppBookingLoadUC appBookingLoadUC,
      AppBookingProcessUC processUC, UtilityService utilityService,
      AppBookingService appBookingService) {
    super(application);
    this.appExecutors = appExecutors;
    this.prepareUC = prepareUC;
    this.appBookingLoadUC = appBookingLoadUC;
    this.prepareUC.registerListener(this);
    this.appBookingLoadUC.registerListener(this);
    this.processUC = processUC;
    this.processUC.registerListener(this);
    this.utilityService = utilityService;
    this.appBookingService = appBookingService;

    // set first data
    // canProcess.postValue(false);
    setField_IsOpenClose(false);
    setField_BookingDate(LocalDate.now());
    setHeader_PtName(null);
    setHeader_Status(null);
    setHeader_OutletName(null);
    setHeader_OutletCode(null);
    setHeader_JenisPengajuan(null);
    setHeader_BookingDate(null);
    setHeader_PengajuanDate(null);
    field_Attach_FINAL_PO_OK.postValue(true);
    bgExecutor = MoreExecutors.listeningDecorator((ExecutorService) appExecutors.backgroundIO());
  }

  public boolean isTransferFieldEnabled() {
    return isTransferFieldEnabled;
  }

  public void setTransferFieldEnabled(boolean transferFieldEnabled) {
    isTransferFieldEnabled = transferFieldEnabled;
  }

  public MutableLiveData<Boolean> getField_OperationalTransfer() {
    return field_OperationalTransfer;
  }

  public void setField_OperationalTransfer(Boolean field_OperationalTransfer) {
    if (!Objects.equals(this.field_OperationalTransfer.getValue(), field_OperationalTransfer))
      this.field_OperationalTransfer.postValue(field_OperationalTransfer);
  }

  public boolean isHasBeenInitializedForm() {
    return hasBeenInitializedForm;
  }

  public void setHasBeenInitializedForm(boolean hasBeenInitializedForm) {
    this.hasBeenInitializedForm = hasBeenInitializedForm;
  }

  public MutableLiveData<Boolean> isShowBuktiRetur() {
    return showBuktiRetur;
  }

  public void setShowBuktiRetur(boolean isCons, boolean isBj, boolean isFif, BigDecimal data) {
    String msg = "setShowBuktiTransfer() operation[" + getField_AppOperation() + "] ";
    if (!Utility.OPERATION_EDIT.equals(getField_AppOperation())) {
      HyperLog.d(TAG, msg
          + " operation bukan edit, maka pengecekan untuk menampilkan Attachment_Bukti_Transfer diabaikan");
      return;
    }

    boolean doAction = false;
    if(!Objects.equals(formState.getValue(),null)){
      AppBookingCashScreenMode cashMode = formState.getValue().getScreenModeCash();
      AppBookingTransferScreenMode transMode = formState.getValue().getScreenModeTransfer();
      if (!Objects.equals(cashMode,null) && cashMode.equals(AppBookingCashScreenMode.KASIR_SUBMITTED_EDIT_APPBOOKING)) {
        doAction = true;
      }
      if(!Objects.equals(transMode,null) && transMode.equals(AppBookingTransferScreenMode.KASIR_SUBMITTED_EDIT_APPBOOKING)){
        doAction = true;
      }
    }

    if(!doAction)
      return;

    boolean consLessThanOldValue = false;
    boolean consEqualsOldValue = false;
    boolean bjLessThanOldValue = false;
    boolean bjEqualsOldValue = false;
    boolean fifLessThanOldValue = false;
    boolean fifEqualsOldValue = false;

    BigDecimal consAmt = staticConsumerAmt == null ? field_ConsAmount_OLD : staticConsumerAmt;
    BigDecimal bjAmt = staticBjAmt == null ? field_BiroJasaAmount_OLD : staticBjAmt;
    BigDecimal fifAmt = staticFifAmt == null ? field_FIFAmount_OLD : staticFifAmt;

    setAttachMode(UtilityState.ATTCH_BUKTIRETUR.getId());

    consLessThanOldValue = consAmt.compareTo(field_ConsAmount_OLD) == -1;
    consEqualsOldValue = consAmt.compareTo(field_ConsAmount_OLD) == 0;
    bjLessThanOldValue = bjAmt.compareTo(field_BiroJasaAmount_OLD) == -1;
    bjEqualsOldValue = bjAmt.compareTo(field_BiroJasaAmount_OLD) == 0;
    fifLessThanOldValue = fifAmt.compareTo(field_FIFAmount_OLD) == -1;
    fifEqualsOldValue = fifAmt.compareTo(field_FIFAmount_OLD) == 0;

    if(consEqualsOldValue && fifEqualsOldValue && bjEqualsOldValue && !Objects.equals(field_Attach_Final_BUKTIRETUR,null)){
      showBuktiRetur.postValue(true);
      return;
    }

    if (!consLessThanOldValue && !bjLessThanOldValue && !fifLessThanOldValue) {
      showBuktiRetur.postValue(false);
      setAttachMode(UtilityState.ATTCH_BUKTIRETUR.getId());
      setAttachment_Filled(false, true);
    } else {
      showBuktiRetur.postValue(true);
    }
  }

  public MutableLiveData<String> getGeneralErrorMessage() {
    return generalErrorMessage;
  }

  public void setGeneralErrorMessage(String msg) {
    this.generalErrorMessage.postValue(msg);
  }

  public String getAttachMode() {
    return attachMode;
  }

  public void setAttachMode(String attachMode) {
    this.attachMode = attachMode;
  }

  public MutableLiveData<Boolean> getValidateVehilceProgress() {
    return validateVehilceProgress;
  }

  public MutableLiveData<String> getField_TrxNo() {
    return field_TrxNo;
  }

  public void setField_TrxNo(String data) {
    field_TrxNo.postValue(data);
  }

  public MutableLiveData<String> getField_CancelReason() {
    return field_CancelReason;
  }

  public void setField_CancelReason(String data, boolean isFromLoad) {
    if (data != field_CancelReason.getValue())
      field_CancelReason.postValue(data);
    validation_field_CancelReason(data, isFromLoad);
  }

  public MutableLiveData<String> getField_CancelReason_ERROR() {
    return field_CancelReason_ERROR;
  }

  protected void setField_CancelReason_ERROR(String message) {
    field_CancelReason.postValue(message);
  }

  public UUID getField_BookingId() {
    return field_BookingId;
  }

  public void setField_BookingId(UUID id) {
    field_BookingId = id;
  }

  public MutableLiveData<UUID> getField_ProcTaskId() {
    return field_ProcTaskId;
  }

  public void setField_ProcTaskId(UUID id) {
    field_ProcTaskId.postValue(id);
  }

  public MutableLiveData<List<OutletShortData>> getOutletList_Ld() {
    return outletList_Ld;
  }

  public MutableLiveData<List<SoShortData>> getSoList_Ld() {
    return soList_Ld;
  }

  public MutableLiveData<List<LobShortData>> getLobList_Ld() {
    return lobList_Ld;
  }

  public MutableLiveData<List<PoPendingReasonShortData>> getPoprList_Ld() {
    return poprList_Ld;
  }

  public void setPoPrList_Ld(List<PoPendingReasonShortData> data) {
    poprList_Ld.postValue(data);
  }

  public MutableLiveData<KeyValueModel<OutletShortData, String>> getField_outletKvm() {
    return field_outletKvm;
  }

  public void setField_outletKvm(KeyValueModel<OutletShortData, String> outletKvm) {
    if (outletKvm == null)
      this.field_outletKvm.postValue(outletKvm);
    else {
      if (outletKvm != this.field_outletKvm.getValue()) {
        this.field_outletKvm.postValue(outletKvm);
      }
    }
    OutletShortData osd = outletKvm != null ? outletKvm.getKey() : null;
    KeyValueModel<SoShortData, String> soKvm = this.getField_soKvm().getValue();
    SoShortData sod = soKvm != null ? soKvm.getKey() : null;
    BigDecimal amtTotal = this.field_BookingAmount.getValue();
    validation_field_FeeScheme(sod, osd, amtTotal);
    validation_field_FeeMatrix_From_OutletAndSo(osd, sod);
    // validateProcess();
  }

  public MutableLiveData<String> getField_outletKvm_ERROR() {
    return this.field_outletKvm_ERROR;
  }

  public MutableLiveData<String> getField_ConsumerNo() {
    return field_ConsumerNo;
  }

  public void setField_ConsumerNo(String data) {
    field_ConsumerNo.postValue(data);
  }

  public MutableLiveData<LocalDate> getField_BookingDate() {
    return field_BookingDate;
  }

  public void setField_BookingDate(LocalDate data) {
    if (field_BookingDate.getValue() == null) {
      field_BookingDate.postValue(data);
    } else {
      if (field_BookingDate.getValue() != data) {
        field_BookingDate.postValue(data);
      }
    }
  }

  public MutableLiveData<KeyValueModel<LobShortData, String>> getField_LobKvm() {
    return field_LobKvm;
  }

  public void setField_LobKvm(KeyValueModel<LobShortData, String> data) {
    if (data == null) {
      field_LobKvm.postValue(data);
      field_lobKvm_OK = false;
      field_LobKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    } else {
      if (data != field_LobKvm.getValue())
        field_LobKvm.postValue(data);

      field_lobKvm_OK = true;
      field_LobKvm_ERROR.postValue(null);
    }
    // validateProcess();
  }

  public MutableLiveData<String> getField_LobKvm_ERROR() {
    return field_LobKvm_ERROR;
  }

  public void setField_SoKvm(KeyValueModel<SoShortData, String> data) {
    if (data == null) {
      field_SoKvm.postValue(data);
      field_SoKvm_OK = false;
      field_SoKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    } else {
      if (data != field_SoKvm.getValue())
        field_SoKvm.postValue(data);

      field_SoKvm_OK = true;
      field_SoKvm_ERROR.postValue(null);
    }

    SoShortData sod = data != null ? data.getKey() : null;
    KeyValueModel<OutletShortData, String> oKvm = this.getField_outletKvm().getValue();
    OutletShortData osd = oKvm != null ? oKvm.getKey() : null;
    BigDecimal bookingAmt = this.getField_BookingAmount().getValue() != null ?
        this.getField_BookingAmount().getValue() : BigDecimal.ZERO;
    validation_field_Idnpksf(sod, this.field_Idnpksf.getValue());
    validation_field_FeeMatrix_From_OutletAndSo(osd, sod);
    validation_field_FeeScheme(sod, osd, bookingAmt);
    // validateProcess();
  }

  public MutableLiveData<KeyValueModel<SoShortData, String>> getField_soKvm() {
    return field_SoKvm;
  }

  public MutableLiveData<String> getField_SoKvm_ERROR() {
    return this.field_SoKvm_ERROR;
  }

  public MutableLiveData<String> getField_Idnpksf() {
    return this.field_Idnpksf;
  }

  public void setField_Idnpksf(String data) {
    if (data != field_Idnpksf.getValue())
      field_Idnpksf.postValue(data);

    validation_field_Idnpksf(
        field_SoKvm.getValue() != null ? field_SoKvm.getValue().getKey() : null, data);
    // validateProcess();
  }

  public boolean isAllowBackDate() {
    return allowBackdate;
  }

  public MutableLiveData<String> getField_Idnpksf_ERROR() {
    return field_Idnpksf_ERROR;
  }

  public MutableLiveData<String> getField_AlasanPinjam_ERROR() {
    return field_AlasanPinjam_ERROR;
  }

  public MutableLiveData<String> getField_AlasanPinjamCash_ERROR() {
    return field_AlasanPinjamCash_ERROR;
  }

  public MutableLiveData<String> getField_AlasanPinjamCash() {
    return field_AlasanPinjamCash;
  }

  public void setField_AlasanPinjamCash(String data) {
    if (data == null || data.isEmpty()) {
      field_AlasanPinjamCash_OK = false;
      field_AlasanPinjamCash_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    } else {
      field_AlasanPinjamCash.postValue(data);
      field_AlasanPinjamCash_OK = true;
      field_AlasanPinjamCash_ERROR.postValue(null);
    }
    // validateProcess();
  }

  public MutableLiveData<String> getField_AppNo() {
    return field_AppNo;
  }

  public void setField_AppNo(String appNo) {
    this.field_AppNo.postValue(appNo);
  }

  /**
   * @param isAdd       menandakan apakah fungsi ini akan melakukan add atau remove
   * @param popr        variable yang akan di add atau remove pada poprList
   * @param newPoprList digunakan untuk meload poprlist yang sudah di pilih (data dari sisi server), jika
   *                    newPoprList diisi, maka isAdd dan id tidak berlaku
   */
  public void setField_PoprList(Boolean isAdd, KeyValueModel<UUID, String> popr,
      List<KeyValueModel<UUID, String>> newPoprList) {
    String fnc = "setField_PoprList ";
    HyperLog.d(TAG,
        fnc + ": isAdd ->" + isAdd + "<-, id->" + popr + "<- , newPoprList->" + newPoprList + "<-");
    List<KeyValueModel<UUID, String>> currentPoprList = this.field_poprList.getValue();

    if (newPoprList != null) {
      HyperLog.d(TAG, fnc + "karena ids tidak kosong, maka diartikan load data dari server");
      currentPoprList = newPoprList;
      this.field_poprList.postValue(currentPoprList);
    } else {
      HyperLog.d(TAG, fnc + "karena ids kosong, maka diartikan tambah a/ hapus data yang terpilih");
      // jika kosong, maka buat list baru
      if (currentPoprList == null) {
        HyperLog.d(TAG,
            fnc + "karena this.field_poprList NULL, maka dibuatkan object List<UUID> baru");
        currentPoprList = new ArrayList<>();
      }

      if (isAdd) {
        HyperLog.d(TAG, fnc + "Block Tambah data yagn terpilih");
        if (currentPoprList.contains(popr)) {
          HyperLog.d(TAG, fnc + "data yang terpilih sudah ada di currentList, maka abaikan");
          return;
        }
        HyperLog.d(TAG, fnc + "Proses Tambah data yagn terpilih");
        currentPoprList.add(popr);
      } else {
        HyperLog.d(TAG, fnc + "Block dan Proses Hapus data yagn terpilih");
        currentPoprList.remove(popr);
      }

      if (currentPoprList != null)
        validation_field_PonoDateAttach(field_PoNo.getValue(), field_PoDate.getValue(),
            field_Attach_Final_PO, currentPoprList);
    }

    // jika semua field po terisi, maka poPendingReasonDihapus
    if (field_PoNo.getValue() != null && field_PoDate.getValue() != null
        && field_Attach_Final_PO != null)
      currentPoprList = new ArrayList<KeyValueModel<UUID, String>>();

    String warnMessage = getApplication().getString(R.string.poprlist_warn);
    boolean isOpenCloseWarning = currentPoprList != null && currentPoprList.contains(getPoPrWarn())
        && (field_IsOpenClose.getValue() == null || field_IsOpenClose.getValue() == false);
    if (isOpenCloseWarning) {
      field_poprList_WARN.postValue(warnMessage);
      setGeneralErrorMessage(warnMessage);
    } else {
      field_poprList_WARN.postValue(null);
    }

    field_poprList.postValue(currentPoprList);
    if (currentPoprList.size() > 0) {
      field_poprList_OK = true;
      if (!isOpenCloseWarning)
        field_poprList_ERROR.postValue(null);
    } else {
      field_poprList_OK = false;
      if (!isOpenCloseWarning)
        validation_field_PonoDateAttach(getField_PoNo().getValue(), getField_PoDate().getValue(),
            field_Attach_Final_PO, currentPoprList);
      //        field_poprList_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    }
    // validateProcess();
  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getField_PoprList() {
    return field_poprList;
  }

  public MutableLiveData<String> getField_PoprList_WARN() {
    return this.field_poprList_WARN;
  }

  public MutableLiveData<String> getField_PoprList_ERROR() {
    return this.field_poprList_ERROR;
  }

  public MutableLiveData<String> getField_PoNo() {
    return this.field_PoNo;
  }

  public void setField_PoNo(String poNo) {
    //
    // poNo = "test aja"
    // this.field_PoNo.getValue() = "test aja"
    // validasi dibawah ini tidak berjalan dengan data test seperti diatas
    // poNo != this.field_PoNo.getValue()
    //
    // ---------------
    // bisa ditest kembali jika mau, hal ini terjadi pada saat loadData
    // String pono2 = this.field_PoNo.getValue();
    // boolean aa = poNo == pono2;
    // boolean aaa = poNo.equals(pono2);

    boolean isNull = poNo == null;
    boolean valid = isNull ? poNo != this.field_PoNo.getValue()
        : !poNo.equals(this.field_PoNo.getValue());

    // validasi aslinya
    // if (poNo != this.field_PoNo.getValue()) {
    if (valid) {
      this.field_PoNo.postValue(poNo);
      if (!isLoadProcess)
        validation_field_PonoDateAttach(poNo, field_PoDate.getValue(), field_Attach_Final_PO,
            this.field_poprList.getValue());
    }
  }

  public MutableLiveData<String> getField_PoNo_ERROR() {
    return this.field_PoNo_ERROR;
  }

  public MutableLiveData<LocalDate> getField_PoDate() {
    return field_PoDate;
  }

  public void setField_PoDate(LocalDate poDate) {
    this.field_PoDate.postValue(poDate);

    if (!isLoadProcess)
      validation_field_PonoDateAttach(field_PoNo.getValue(), poDate, field_Attach_Final_PO,
          this.field_poprList.getValue());
  }

  public MutableLiveData<String> getField_PoDate_ERROR() {
    return this.field_PoDate_ERROR;
  }

  public MutableLiveData<Boolean> getField_Attach_FINAL_PO_OK() {
    return this.field_Attach_FINAL_PO_OK;
  }

  public void setAttach_TempFile(File attachFile, String mode) {
    HyperLog.d(TAG, "setField_Temp_Attach terpanggil dengan mode->" + mode + "<-");
    if (mode == UtilityState.ATTCH_PO.getId()) {
      field_Attach_Temp_PO = attachFile;
    } else if (mode == UtilityState.ATTCH_STNK.getId()) {
      field_Attach_Temp_STNK = attachFile;
    } else if (mode == UtilityState.ATTCH_KK.getId()) {
      field_Attach_Temp_KK = attachFile;
    } else if (mode == UtilityState.ATTCH_BPKB.getId()) {
      field_Attach_Temp_BPKB = attachFile;
    } else if (mode == UtilityState.ATTCH_SPT.getId()) {
      field_Attach_Temp_SPT = attachFile;
    } else if (mode == UtilityState.ATTCH_SERAHTERIMA.getId()) {
      field_Attach_Temp_SERAH_TERIMA = attachFile;
    } else if (mode == UtilityState.ATTCH_FISIKMOTOR.getId()) {
      field_Attach_Temp_FISIK_MOTOR = attachFile;
    } else if (mode == UtilityState.ATTCH_HO.getId()) {
      field_Attach_Temp_HO = attachFile;
    } else if (mode == UtilityState.ATTCH_BUKTIRETUR.getId()) {
      field_Attach_Temp_BUKTIRETUR = attachFile;
    } else {
      HyperLog.e(TAG, "tidak ada mode yang cocok untuk setField_Temp_Attach() mode yang dilempar->"
          + mode + "<-");
    }
  }

  public void deleteAttach_TempFile(String mode) {
    HyperLog.d(TAG, "deleteTempAttachFile() terpanggil dengan mode->" + mode + "<-");
    if (mode == UtilityState.ATTCH_PO.getId()) {
      this.field_Attach_Temp_PO.delete();
    } else if (mode == UtilityState.ATTCH_STNK.getId()) {
      this.field_Attach_Temp_STNK.delete();
    } else if (mode == UtilityState.ATTCH_KK.getId()) {
      this.field_Attach_Temp_KK.delete();
    } else if (mode == UtilityState.ATTCH_BPKB.getId()) {
      this.field_Attach_Temp_BPKB.delete();
    } else if (mode == UtilityState.ATTCH_SPT.getId()) {
      this.field_Attach_Temp_SPT.delete();
    } else if (mode == UtilityState.ATTCH_SERAHTERIMA.getId()) {
      this.field_Attach_Temp_SERAH_TERIMA.delete();
    } else if (mode == UtilityState.ATTCH_FISIKMOTOR.getId()) {
      this.field_Attach_Temp_FISIK_MOTOR.delete();
    } else if (mode == UtilityState.ATTCH_HO.getId()) {
      this.field_Attach_Temp_HO.delete();
    } else if (mode == UtilityState.ATTCH_BUKTIRETUR.getId()) {
      this.field_Attach_Temp_BUKTIRETUR.delete();
    } else {
      HyperLog.e(TAG, "tidak ada mode yang cocok untuk deleteTempAttachFile() mode yang dilempar->"
          + mode + "<-");
    }
  }

  private File getAttach_TempFile(String mode) {
    HyperLog.d(TAG, "getTempAttachFile() terpanggil dengan mode->" + mode + "<-");
    if (mode == UtilityState.ATTCH_PO.getId()) {
      return field_Attach_Temp_PO;
    } else if (mode == UtilityState.ATTCH_STNK.getId()) {
      return field_Attach_Temp_STNK;
    } else if (mode == UtilityState.ATTCH_KK.getId()) {
      return field_Attach_Temp_KK;
    } else if (mode == UtilityState.ATTCH_BPKB.getId()) {
      return field_Attach_Temp_BPKB;
    } else if (mode == UtilityState.ATTCH_SPT.getId()) {
      return field_Attach_Temp_SPT;
    } else if (mode == UtilityState.ATTCH_SERAHTERIMA.getId()) {
      return field_Attach_Temp_SERAH_TERIMA;
    } else if (mode == UtilityState.ATTCH_FISIKMOTOR.getId()) {
      return field_Attach_Temp_FISIK_MOTOR;
    } else if (mode == UtilityState.ATTCH_HO.getId()) {
      return field_Attach_Temp_HO;
    } else if (mode == UtilityState.ATTCH_BUKTIRETUR.getId()) {
      return field_Attach_Temp_BUKTIRETUR;
    } else {
      HyperLog.exception(TAG,
          "tidak ada mode yang cocok untuk getTempAttachFile() mode yang dilempar->" + mode + "<-");
      return null;
    }
  }

  private void setAttach_FinalFile(File attachFile, String mode) {
    HyperLog.d(TAG, "setFinalAttachFile() terpanggil dengan mode->" + mode + "<-");
    if (mode == UtilityState.ATTCH_PO.getId()) {

      is_att_po_change = true;
      if (Objects.equals(null, attachFile)) {
        att_po_id = null;
      } else {
        if (!Objects.equals(null, att_po_id))
          att_po_id = UUID.randomUUID();
      }

      field_Attach_Final_PO = attachFile;
      validation_field_PonoDateAttach(field_PoNo.getValue(), field_PoDate.getValue(), attachFile,
          field_poprList.getValue());
    } else if (mode == UtilityState.ATTCH_STNK.getId()) {

      is_att_stnk_change = true;
      if (Objects.equals(null, attachFile)) {
        att_stnk_id = null;
      } else {
        if (!Objects.equals(null, att_stnk_id))
          att_stnk_id = UUID.randomUUID();
      }

      field_Attach_Final_STNK = attachFile;
      field_Attach_Final_STNK_OK.postValue(attachFile != null ? true : false);
      // validateProcess();
    } else if (mode == UtilityState.ATTCH_KK.getId()) {

      is_att_kk_change = true;
      if (Objects.equals(null, attachFile)) {
        att_kk_id = null;
      } else {
        if (!Objects.equals(null, att_kk_id))
          att_kk_id = UUID.randomUUID();
      }

      field_Attach_Final_KK = attachFile;
      field_Attach_Final_KK_OK.postValue(attachFile != null ? true : false);
      // validateProcess();
    } else if (mode == UtilityState.ATTCH_BPKB.getId()) {

      is_att_bpkb_change = true;
      if (Objects.equals(null, attachFile)) {
        att_bpkb_id = null;
      } else {
        if (!Objects.equals(null, att_bpkb_id))
          att_bpkb_id = UUID.randomUUID();
      }

      field_Attach_Final_BPKB = attachFile;
      field_Attach_Final_BPKB_OK.postValue(attachFile != null ? true : false);
      // validateProcess();
    } else if (mode == UtilityState.ATTCH_SPT.getId()) {

      is_att_spt_change = true;
      if (Objects.equals(null, attachFile)) {
        att_spt_id = null;
      } else {
        if (!Objects.equals(null, att_spt_id))
          att_spt_id = UUID.randomUUID();
      }

      field_Attach_Final_SPT = attachFile;
      field_Attach_Final_SPT_OK.postValue(attachFile != null ? true : false);
      // validateProcess();
    } else if (mode == UtilityState.ATTCH_SERAHTERIMA.getId()) {

      is_att_serah_terima_change=true;
      if (Objects.equals(null, attachFile)) {
        att_serah_terima_id = null;
      } else {
        if (!Objects.equals(null, att_serah_terima_id))
          att_serah_terima_id = UUID.randomUUID();
      }

      field_Attach_Final_SERAH_TERIMA = attachFile;
      field_Attach_Final_SERAH_TERIMA_OK.postValue(attachFile != null ? true : false);
      // validateProcess();
    } else if (mode == UtilityState.ATTCH_FISIKMOTOR.getId()) {

      is_att_fisik_motor_change = true;
      if (Objects.equals(null, attachFile)) {
        att_fisik_motor_id = null;
      } else {
        if (!Objects.equals(null, att_fisik_motor_id))
          att_fisik_motor_id = UUID.randomUUID();
      }

      field_Attach_Final_FISIK_MOTOR = attachFile;
    } else if (mode == UtilityState.ATTCH_HO.getId()) {

      is_att_HO_change = true;
      if (Objects.equals(null, attachFile)) {
        att_HO_id = null;
      } else {
        if (!Objects.equals(null, att_HO_id))
          att_HO_id = UUID.randomUUID();
      }

      field_Attach_Final_HO = attachFile;
      field_Attach_Final_HO_OK.postValue(attachFile != null ? true : false);
    } else if (mode == UtilityState.ATTCH_BUKTIRETUR.getId()) {

      is_att_bukti_retur_change = true;
      if (Objects.equals(null, attachFile)) {
        att_bukti_retur_id = null;
      } else {
        if (!Objects.equals(null, att_bukti_retur_id))
          att_bukti_retur_id = UUID.randomUUID();
      }

      field_Attach_Final_BUKTIRETUR = attachFile;
      field_Attach_Final_BUKTIRETUR_OK.postValue(attachFile != null ? true : false);
    } else {
      HyperLog.e(TAG, "tidak ada mode yang cocok untuk setFinalAttachFile() mode yang dilempar->"
          + mode + "<-");
    }
  }

  public MutableLiveData<Boolean> getField_Attach_Final_STNK_OK() {
    return this.field_Attach_Final_STNK_OK;
  }

  public MutableLiveData<Boolean> getField_Attach_Final_KK_OK() {
    return this.field_Attach_Final_KK_OK;
  }

  public MutableLiveData<Boolean> getField_Attach_Final_BPKB_OK() {
    return this.field_Attach_Final_BPKB_OK;
  }

  public MutableLiveData<Boolean> getField_Attach_Final_SPT_OK() {
    return this.field_Attach_Final_SPT_OK;
  }

  public MutableLiveData<Boolean> getField_Attach_Final_SERAH_TERIMA_OK() {
    return field_Attach_Final_SERAH_TERIMA_OK;
  }

  public MutableLiveData<Boolean> getField_Attach_Final_HO_OK() {
    return field_Attach_Final_HO_OK;
  }

  public MutableLiveData<Boolean> getField_Attach_Final_BUKTIRETUR_OK() {
    return field_Attach_Final_BUKTIRETUR_OK;
  }

  public File getAttach_FinalFile(String mode) {
    HyperLog.d(TAG, "getAttach_FinalFile() terpanggil dengan mode->" + mode + "<-");
    if (mode == UtilityState.ATTCH_PO.getId()) {
      return field_Attach_Final_PO;
    } else if (mode == UtilityState.ATTCH_STNK.getId()) {
      return field_Attach_Final_STNK;
    } else if (mode == UtilityState.ATTCH_KK.getId()) {
      return field_Attach_Final_KK;
    } else if (mode == UtilityState.ATTCH_BPKB.getId()) {
      return field_Attach_Final_BPKB;
    } else if (mode == UtilityState.ATTCH_SPT.getId()) {
      return field_Attach_Final_SPT;
    } else if (mode == UtilityState.ATTCH_SERAHTERIMA.getId()) {
      return field_Attach_Final_SERAH_TERIMA;
    } else if (mode == UtilityState.ATTCH_FISIKMOTOR.getId()) {
      return field_Attach_Final_FISIK_MOTOR;
    } else if (mode == UtilityState.ATTCH_HO.getId()) {
      return field_Attach_Final_HO;
    } else if (mode == UtilityState.ATTCH_BUKTIRETUR.getId()) {
      return field_Attach_Final_BUKTIRETUR;
    } else {
      HyperLog.e(TAG, "tidak ada mode yang cocok untuk getAttach_FinalFile() mode yang dilempar->"
          + mode + "<-");
      return null;
    }
  }

  private void deleteAttach_FinalFile(String mode) {
    HyperLog.d(TAG, "deleteFinalAttachFile() terpanggil dengan mode->" + mode + "<-");
    if (mode == UtilityState.ATTCH_PO.getId()) {
      this.field_Attach_Final_PO.delete();

      validation_field_PonoDateAttach(field_PoNo.getValue(), field_PoDate.getValue(), null,
          this.field_poprList.getValue());
    } else if (mode == UtilityState.ATTCH_STNK.getId()) {
      field_Attach_Final_STNK.delete();
    } else if (mode == UtilityState.ATTCH_KK.getId()) {
      field_Attach_Final_KK.delete();
    } else if (mode == UtilityState.ATTCH_BPKB.getId()) {
      field_Attach_Final_BPKB.delete();
    } else if (mode == UtilityState.ATTCH_SPT.getId()) {
      field_Attach_Final_SPT.delete();
    } else if (mode == UtilityState.ATTCH_SERAHTERIMA.getId()) {
      field_Attach_Final_SERAH_TERIMA.delete();
    } else if (mode == UtilityState.ATTCH_FISIKMOTOR.getId()) {
      field_Attach_Final_FISIK_MOTOR.delete();
    } else if (mode == UtilityState.ATTCH_HO.getId()) {
      field_Attach_Final_HO.delete();
    } else if (mode == UtilityState.ATTCH_BUKTIRETUR.getId()) {
      field_Attach_Final_BUKTIRETUR.delete();
    } else {
      HyperLog.e(TAG, "tidak ada mode yang cocok untuk deleteFinalAttachFile() mode yang dilempar->"
          + mode + "<-");
    }
  }

  private void setAttach_ReducedFinalFile(Boolean isDeleted, Bitmap file, String mode) {
    HyperLog.d(TAG, "setFinalAttachFile() terpanggil dengan mode->" + mode + "<-");
    Resource bitmapResource = null;
    if (!isDeleted)
      bitmapResource = Resource.Builder.success(null, file);

    if (mode == UtilityState.ATTCH_PO.getId()) {
      field_Attach_ReducedFinal_PO.postValue(bitmapResource);
    } else if (mode == UtilityState.ATTCH_STNK.getId()) {
      field_Attach_ReducedFinal_STNK.postValue(bitmapResource);
    } else if (mode == UtilityState.ATTCH_KK.getId()) {
      field_Attach_ReducedFinal_KK.postValue(bitmapResource);
    } else if (mode == UtilityState.ATTCH_BPKB.getId()) {
      field_Attach_ReducedFinal_BPKB.postValue(bitmapResource);
    } else if (mode == UtilityState.ATTCH_SPT.getId()) {
      field_Attach_ReducedFinal_SPT.postValue(bitmapResource);
    } else if (mode == UtilityState.ATTCH_SERAHTERIMA.getId()) {
      field_Attach_ReducedFinal_SERAH_TERIMA.postValue(bitmapResource);
    } else if (mode == UtilityState.ATTCH_FISIKMOTOR.getId()) {
      field_Attach_ReducedFinal_FISIK_MOTOR.postValue(bitmapResource);
    } else if (mode == UtilityState.ATTCH_HO.getId()) {
      field_Attach_ReducedFinal_HO.postValue(bitmapResource);
    } else if (mode == UtilityState.ATTCH_BUKTIRETUR.getId()) {
      field_Attach_ReducedFinal_BUKTIRETUR.postValue(bitmapResource);
    } else {
      HyperLog.e(TAG, "tidak ada mode yang cocok untuk setFinalAttachFile() mode yang dilempar->"
          + mode + "<-");
    }
  }

  public void setAttachment_Filled(boolean isFilled, boolean deleteTemp) {
    File tempAttachFile = getAttach_TempFile(getAttachMode());
    if (isFilled && tempAttachFile != null) {
      ListenableFuture<File> attachment_Lf = Futures.submit(new Callable<File>() {
        @Override
        public File call() throws Exception {
          BitmapFactory.Options bmOptions = new BitmapFactory.Options();
          Bitmap photo = BitmapFactory.decodeFile(tempAttachFile.getAbsolutePath(), bmOptions);
          Bitmap photoRescaled = ImageUtility.getResizedBitmap(photo, 1280);
          Bitmap finalPhoto = null;
          int orientation = ImageUtility.getImageOrientation(tempAttachFile);
          if (orientation != 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);
            finalPhoto = Bitmap.createBitmap(photoRescaled, 0, 0, photoRescaled.getWidth(),
                photoRescaled.getHeight(), matrix, true);
          } else {
            finalPhoto = photoRescaled;
          }
          try {
            File finalFile = ImageUtility.saveBitmapToJpg(getApplication(), finalPhoto,
                getApplication().getCacheDir(), App.IMAGE_DIR, getAttachMode());
            setAttach_FinalFile(finalFile, getAttachMode());
            if (deleteTemp) {
              deleteAttach_TempFile(getAttachMode());
            }
            setAttach_TempFile(null, getAttachMode());
            if (photo != finalPhoto) {
              photo.recycle();
            }
            setAttach_ReducedFinalFile(false, finalPhoto, getAttachMode());
          } catch (Exception e) {
            HyperLog.e(TAG, "Failed to post-process attachment", e);
            if (deleteTemp) {
              deleteAttach_TempFile(getAttachMode());
            }
            // kalau error, pastikan attachment jadi kosong
            setAttach_TempFile(null, getAttachMode());
            // hapus juga versi final kalau error
            if (getAttach_FinalFile(getAttachMode()) != null) {
              deleteAttach_FinalFile(getAttachMode());
            }
            setAttach_FinalFile(null, getAttachMode());
          }
          return getAttach_TempFile(getAttachMode());
        }
      }, bgExecutor);
    } else {
      if (getAttach_TempFile(getAttachMode()) != null) {
        if (deleteTemp) {
          deleteAttach_TempFile(getAttachMode());
        }
        setAttach_TempFile(null, getAttachMode());
      }

      if (getAttach_FinalFile(getAttachMode()) != null) {
        deleteAttach_FinalFile(getAttachMode());
        setAttach_FinalFile(null, getAttachMode());
      }
      setAttach_ReducedFinalFile(true, null, getAttachMode());
    }
  }

  public MutableLiveData<String> getHeader_PtName() {
    return header_PtName;
  }

  public void setHeader_PtName(String header_PtName) {
    this.header_PtName.postValue(header_PtName == null ? "-" : header_PtName);
  }

  public MutableLiveData<String> getHeader_StatusCode() {
    return header_StatusCode;
  }

  public void setHeader_StatusCode(String header_StatusCode) {
    this.header_StatusCode.postValue(header_StatusCode);
  }

  public MutableLiveData<String> getHeader_Status() {
    return header_Status;
  }

  public void setFieldWfStatus(String fieldWfStatus) {
    this.fieldWfStatus.postValue(fieldWfStatus);
  }

  public MutableLiveData<String> getFieldWfStatus() {
    return fieldWfStatus;
  }

  public void setHeader_Status(String header_Status) {
    this.header_Status.postValue(header_Status == null ? "-" : header_Status);
  }

  public MutableLiveData<String> getHeader_OutletName() {
    return header_OutletName;
  }

  public void setHeader_OutletName(String header_OutletName) {
    this.header_OutletName.postValue(header_OutletName == null ? "-" : header_OutletName);
  }

  public MutableLiveData<String> getHeader_OutletCode() {
    return header_OutletCode;
  }

  public void setHeader_OutletCode(String header_OutletCode) {
    this.header_OutletCode
        .postValue("(" + (header_OutletCode == null ? " - " : header_OutletCode) + " )");
  }

  public MutableLiveData<String> getHeader_JenisPengajuan() {
    return header_JenisPengajuan;
  }

  public void setHeader_JenisPengajuan(String header_JenisPengajuan) {
    this.header_JenisPengajuan
        .postValue(header_JenisPengajuan == null ? "-" : header_JenisPengajuan);
  }

  public MutableLiveData<String> getHeader_BookingDate() {
    return header_BookingDate;
  }

  public void setHeader_BookingDate(String header_BookingDate) {
    this.header_BookingDate.postValue(header_BookingDate == null ? "-" : header_BookingDate);
  }

  public MutableLiveData<String> getHeader_PengajuanDate() {
    return header_PengajuanDate;
  }

  public void setHeader_PengajuanDate(String header_PengajuanDate) {
    this.header_PengajuanDate.postValue(header_PengajuanDate == null ? "-" : header_PengajuanDate);
  }

  public MutableLiveData<Resource<Bitmap>> getField_Attach_ReducedFinal_PO() {
    return field_Attach_ReducedFinal_PO;
  }

  public MutableLiveData<Resource<Bitmap>> getField_Attach_ReducedFinal_STNK() {
    return field_Attach_ReducedFinal_STNK;
  }

  public MutableLiveData<Resource<Bitmap>> getField_Attach_ReducedFinal_KK() {
    return field_Attach_ReducedFinal_KK;
  }

  public MutableLiveData<Resource<Bitmap>> getField_Attach_ReducedFinal_BPKB() {
    return field_Attach_ReducedFinal_BPKB;
  }

  public MutableLiveData<Resource<Bitmap>> getField_Attach_ReducedFinal_SPT() {
    return field_Attach_ReducedFinal_SPT;
  }

  public MutableLiveData<Resource<Bitmap>> getField_Attach_ReducedFinal_SERAH_TERIMA() {
    return field_Attach_ReducedFinal_SERAH_TERIMA;
  }

  public MutableLiveData<Resource<Bitmap>> getField_Attach_ReducedFinal_FISIK_MOTOR() {
    return field_Attach_ReducedFinal_FISIK_MOTOR;
  }

  public MutableLiveData<Resource<Bitmap>> getField_Attach_ReducedFinal_HO() {
    return field_Attach_ReducedFinal_HO;
  }

  public MutableLiveData<Resource<Bitmap>> getField_Attach_ReducedFinal_BUKTIRETUR() {
    return field_Attach_ReducedFinal_BUKTIRETUR;
  }

  public MutableLiveData<String> getField_ConsumerName() {
    return this.field_ConsumerName;
  }

  public void setField_ConsumerName(String consumerName) {
    if (consumerName == null || consumerName.isEmpty()) {
      field_ConsumerName_OK = false;
      this.field_ConsumerName_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    } else {
      this.field_ConsumerName.postValue(consumerName);
      field_ConsumerName_OK = true;
      this.field_ConsumerName_ERROR.postValue(null);
    }

    // validateProcess();
  }

  public MutableLiveData<String> getField_ConsumerName_ERROR() {
    return this.field_ConsumerName_ERROR;
  }

  public MutableLiveData<String> getField_ConsumerAddress() {
    return this.field_ConsumerAddress;
  }

  public void setField_ConsumerAddress(String consumerAddress) {
    if (consumerAddress == null || consumerAddress.isEmpty()) {
      field_ConsumerAddress_OK = false;
      this.field_ConsumerAddress_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    } else {
      this.field_ConsumerAddress.postValue(consumerAddress);
      field_ConsumerAddress_OK = true;
      this.field_ConsumerAddress_ERROR.postValue(null);
    }
    // validateProcess();
  }

  public MutableLiveData<String> getField_ConsumerAddress_ERROR() {
    return this.field_ConsumerAddress_ERROR;
  }

  public MutableLiveData<String> getField_ConsumerPhone() {
    return this.field_ConsumerPhone;
  }

  public void setField_ConsumerPhone(String consumerPhone) {
    if (consumerPhone == null || consumerPhone.isEmpty()) {
      field_ConsumerPhone_OK = false;
      this.field_ConsumerPhone_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    } else {
      this.field_ConsumerPhone.postValue(consumerPhone);
      field_ConsumerPhone_OK = true;
      this.field_ConsumerPhone_ERROR.postValue(null);
    }
    // validateProcess();
  }

  public MutableLiveData<String> getField_ConsumerPhone_ERROR() {
    return this.field_ConsumerPhone_ERROR;
  }

  public MutableLiveData<String> getField_VehicleNo() {
    return this.field_VehicleNo;
  }

  public void setField_VehicleNo(String vehicleNo) {
    if (vehicleNo == null || vehicleNo.isEmpty()) {
      HyperLog.d(TAG, "GETTER SETTER setField_VehicleNo: SET FALSE");
      field_VehicleNo_OK = false;
      this.field_VehicleNo_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    } else {
      field_VehicleNo_OK = true;
      field_VehicleNo_ERROR.postValue(null);
      HyperLog.d(TAG, "GETTER SETTER setField_VehicleNo: TIDAK DISET APA APA ");
      if (vehicleNo != this.field_VehicleNo.getValue())
        this.field_VehicleNo.postValue(vehicleNo);
    }
    // validateProcess();
  }

  public MutableLiveData<Boolean> isField_isOpenClose() {
    return this.field_IsOpenClose;
  }

  public void setField_IsOpenClose(Boolean isOpenClose) {
    this.field_IsOpenClose.postValue(isOpenClose);
    if (!isOpenClose && this.field_poprList.getValue() != null
        && this.field_poprList.getValue().contains(getPoPrWarn())) {
      this.field_poprList_WARN.postValue(getApplication().getString(R.string.poprlist_warn));
      field_poprList_WARN.postValue(getApplication().getString(R.string.poprlist_warn));
      setGeneralErrorMessage(getApplication().getString(R.string.poprlist_warn));
    } else
      this.field_poprList_WARN.postValue(null);
    validation_field_PencairanFIF(isOpenClose, this.getField_FIFAmount().getValue());
  }

  protected void setAttachmentPO(UUID att_id, String fileName, String poNo, LocalDate poDate,
      List<KeyValueModel<UUID, String>> poprList) {

    HyperLog.d(TAG, "DANWIN PO ID->" + (att_id != null ? att_id : "NULL") + "<- fileName->"
        + (fileName != null ? fileName : "NULL") + "<- poNo->" + (poNo != null ? poNo : "NULL")
        + "<- poDate->" + (poDate != null ? poDate : "NULL")
        + "<- poprList->" + (poprList.size() > 0 ? poprList : "NULL") + "<-");
    ListenableFuture<ResponseBody> rblf = utilityService.getFile(att_id);

    Futures.addCallback(rblf, new FutureCallback<ResponseBody>() {
      @Override
      public void onSuccess(@NullableDecl ResponseBody result) {
        HyperLog.d(TAG, "DANWIN PO onSuccess[TERPANGGIL]");
        InputStream is = result.byteStream();
        FileOutputStream fos = null;
        try {
          File dir = new File(getApplication().getCacheDir() + File.separator + App.IMAGE_DIR);
          HyperLog.d(TAG, "DANWIN PO membuat dir->" + dir + "<-");
          boolean dirExists = false;
          if (!dir.exists()) {
            dirExists = dir.mkdirs();
          } else {
            dirExists = true;
          }
          HyperLog.d(TAG, "DANWIN PO membuat dir sukses->" + dirExists + "<-");
          if (dirExists) {
            File output = new File(dir, fileName);
            fos = new FileOutputStream(output);
            byte buffer[] = new byte[1024];
            int read = -1;
            while ((read = is.read(buffer)) != -1) {
              fos.write(buffer, 0, read);
            }

            HyperLog.d(TAG, "DANWIN PO membuat fos->" + fos + "<-");
            fos.flush();
            HyperLog.d(TAG, "DANWIN PO membuat fos diflush ");

            field_Attach_Final_PO = output;
            att_po_id = isFirstTimeLoadFromEditorCancel ? UUID.randomUUID() : att_id;
            HyperLog.d(TAG, "DANWIN PO set file final attc->" + output + "<-");
            validation_field_PonoDateAttach(poNo, poDate,
                field_Attach_Final_PO, poprList);

            HyperLog.d(TAG, "DANWIN PO validasi PoDate");

            Glide.with(getApplication()).asBitmap().load(output)
                .placeholder(R.drawable.ic_spinner_drawable).into(new CustomTarget<Bitmap>() {
              @Override
              public void onResourceReady(@NonNull Bitmap resource,
                  @Nullable Transition<? super Bitmap> transition) {
                field_Attach_ReducedFinal_PO.postValue(Resource.Builder.success(null, resource));
                HyperLog.d(TAG,
                    "DANWIN PO set file tumbnail attc->" + Resource.Builder.success(null, resource)
                        + "<-");
              }

              @Override
              public void onLoadCleared(@Nullable Drawable placeholder) {
                HyperLog.d(TAG, "DANWIN onLoadCleared terpanggil, apa yang harus dilakukan ?");
              }
            });
          } else {
            throw new IOException("Failed to create dir: " + dir);
          }
        } catch (Exception e) {
          String msg = String.format("DANWIN Failed to save PO Attachment %s locally ", att_id);
          HyperLog.exception(TAG + "DANWIN PO id->" + att_id + "<-", e);
          HyperLog.e(TAG, msg, e);
          field_Attach_ReducedFinal_PO.postValue(Resource.Builder.error(
              getApplication().getString(R.string.failed_to_save_local_file)));
        } finally {
          if (fos != null) {
            try {
              fos.close();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
          HyperLog.d(TAG, "DANWIN PO onSuccess[SELESAI]");
        }
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        HyperLog.e(TAG,
            String.format("DANWIN Failed to download AppBooking PO Attachment %s, cause %s", att_id,
                message), t);
        HyperLog.exception(TAG, "DANWIN id->" + att_id + "<-", t);
      }
    }, appExecutors.networkIO());
  }
  
  protected void setAttachment(String attachMode, UUID att_id, String fileName) {
    String finalFileName = attachMode + fileName;

    HyperLog.d(TAG, "DANWIN mode " + attachMode + "_ID->" + (att_id != null ? att_id : "NULL")
        + "<- fileName->" + (fileName != null ? fileName : "NULL") + "<-");

    ListenableFuture<ResponseBody> rblf = utilityService.getFile(att_id);

    Futures.addCallback(rblf, new FutureCallback<ResponseBody>() {
      @Override
      public void onSuccess(@NullableDecl ResponseBody result) {
        HyperLog.d(TAG, "DANWIN mode " + attachMode + " onSuccess[TERPANGGIL]");
        InputStream is = result.byteStream();
        FileOutputStream fos = null;
        try {
          File dir = new File(getApplication().getCacheDir() + File.separator + App.IMAGE_DIR);
          HyperLog.d(TAG, "DANWIN mode " + attachMode + " membuat dir->" + dir + "<-");
          boolean dirExists = false;
          if (!dir.exists()) {
            dirExists = dir.mkdirs();
          } else {
            dirExists = true;
          }
          HyperLog.d(TAG, "DANWIN mode " + attachMode + " membuat dir sukses->" + dirExists + "<-");
          if (dirExists) {
            File output = new File(dir, finalFileName);
            fos = new FileOutputStream(output);
            byte buffer[] = new byte[1024];
            int read = -1;
            while ((read = is.read(buffer)) != -1) {
              fos.write(buffer, 0, read);
            }

            HyperLog.d(TAG, "DANWIN mode " + attachMode + " membuat fos->" + fos + "<-");
            fos.flush();
            HyperLog.d(TAG, "DANWIN mode " + attachMode + " membuat fos diflush ");

            if (attachMode.equals(UtilityState.ATTCH_PO.getId())) {
              field_Attach_Final_PO = output;
              att_po_id = isFirstTimeLoadFromEditorCancel ? UUID.randomUUID() : att_id;
              HyperLog.d(TAG,
                  "DANWIN mode " + attachMode + " set file final attc->" + output + "<-");
            }
            if (attachMode.equals(UtilityState.ATTCH_STNK.getId())) {
              field_Attach_Final_STNK = output;
              att_stnk_id = isFirstTimeLoadFromEditorCancel?UUID.randomUUID():att_id;
              HyperLog.d(TAG,
                  "DANWIN mode " + attachMode + " set file final attc->" + output + "<-");
            }
            if (attachMode.equals(UtilityState.ATTCH_KK.getId())) {
              field_Attach_Final_KK = output;
              att_kk_id = isFirstTimeLoadFromEditorCancel?UUID.randomUUID():att_id;
              HyperLog.d(TAG,
                  "DANWIN mode " + attachMode + " set file final attc->" + output + "<-");
            }
            if (attachMode.equals(UtilityState.ATTCH_BPKB.getId())) {
              field_Attach_Final_BPKB = output;
              att_bpkb_id = isFirstTimeLoadFromEditorCancel?UUID.randomUUID():att_id;
              HyperLog.d(TAG,
                  "DANWIN mode" + attachMode + " set file final attc->" + output + "<-");
            }
            if (attachMode.equals(UtilityState.ATTCH_SPT.getId())) {
              field_Attach_Final_SPT = output;
              att_spt_id = isFirstTimeLoadFromEditorCancel?UUID.randomUUID():att_id;
              HyperLog.d(TAG,
                  "DANWIN mode " + attachMode + " set file final attc->" + output + "<-");
            }
            if (attachMode.equals(UtilityState.ATTCH_SERAHTERIMA.getId())) {
              field_Attach_Final_SERAH_TERIMA = output;
              att_serah_terima_id = isFirstTimeLoadFromEditorCancel?UUID.randomUUID():att_id;
              HyperLog.d(TAG,
                  "DANWIN mode " + attachMode + " set file final attc->" + output + "<-");
            }
            if (attachMode.equals(UtilityState.ATTCH_FISIKMOTOR.getId())) {
              field_Attach_Final_FISIK_MOTOR = output;
              att_fisik_motor_id = isFirstTimeLoadFromEditorCancel?UUID.randomUUID():att_id;
              HyperLog.d(TAG,
                  "DANWIN mode " + attachMode + " set file final attc->" + output + "<-");
            }
            if (attachMode.equals(UtilityState.ATTCH_HO.getId())) {
              field_Attach_Final_HO = output;
              att_HO_id = isFirstTimeLoadFromEditorCancel?UUID.randomUUID():att_id;
              HyperLog.d(TAG,
                  "DANWIN mode " + attachMode + " set file final attc->" + output + "<-");
            }

            if (attachMode.equals(UtilityState.ATTCH_BUKTIRETUR.getId())) {
              field_Attach_Final_BUKTIRETUR = output;
              att_bukti_retur_id = isFirstTimeLoadFromEditorCancel?UUID.randomUUID():att_id;
              HyperLog.d(TAG,
                  "DANWIN mode " + attachMode + " set file final attc->" + output + "<-");
            }

            Glide.with(getApplication()).asBitmap().load(output)
                .placeholder(R.drawable.ic_spinner_drawable).into(new CustomTarget<Bitmap>() {
              @Override
              public void onResourceReady(@NonNull Bitmap resource,
                  @Nullable Transition<? super Bitmap> transition) {
                Resource bitmapResource = Resource.Builder.success(null, resource);
                if (attachMode.equals(UtilityState.ATTCH_PO.getId())) {
                  field_Attach_ReducedFinal_PO.postValue(bitmapResource);
                  HyperLog.d(TAG,
                      "DANWIN mode " + attachMode + " set file tumbnail attc->" + bitmapResource
                          + "<-");
                }
                if (attachMode.equals(UtilityState.ATTCH_STNK.getId())) {
                  field_Attach_ReducedFinal_STNK.postValue(bitmapResource);
                  HyperLog.d(TAG,
                      "DANWIN mode " + attachMode + " set file tumbnail attc->" + bitmapResource
                          + "<-");
                }
                if (attachMode.equals(UtilityState.ATTCH_KK.getId())) {
                  field_Attach_ReducedFinal_KK.postValue(bitmapResource);
                  HyperLog.d(TAG,
                      "DANWIN mode " + attachMode + " set file tumbnail attc->" + bitmapResource
                          + "<-");
                }
                if (attachMode.equals(UtilityState.ATTCH_BPKB.getId())) {
                  field_Attach_ReducedFinal_BPKB.postValue(bitmapResource);
                  HyperLog.d(TAG,
                      "DANWIN mode " + attachMode + " set file tumbnail attc->" + bitmapResource
                          + "<-");
                }
                if (attachMode.equals(UtilityState.ATTCH_SPT.getId())) {
                  field_Attach_ReducedFinal_SPT.postValue(bitmapResource);
                  HyperLog.d(TAG,
                      "DANWIN mode " + attachMode + " set file tumbnail attc->" + bitmapResource
                          + "<-");
                }
                if (attachMode.equals(UtilityState.ATTCH_SERAHTERIMA.getId())) {
                  field_Attach_ReducedFinal_SERAH_TERIMA.postValue(bitmapResource);
                  HyperLog.d(TAG,
                      "DANWIN mode " + attachMode + " set file tumbnail attc->" + bitmapResource
                          + "<-");
                }
                if (attachMode.equals(UtilityState.ATTCH_FISIKMOTOR.getId())) {
                  field_Attach_ReducedFinal_FISIK_MOTOR.postValue(bitmapResource);
                  HyperLog.d(TAG,
                      "DANWIN mode " + attachMode + " set file tumbnail attc->" + bitmapResource
                          + "<-");
                }
                if (attachMode.equals(UtilityState.ATTCH_HO.getId())) {
                  field_Attach_ReducedFinal_HO.postValue(bitmapResource);
                  HyperLog.d(TAG,
                      "DANWIN mode " + attachMode + " set file tumbnail attc->" + bitmapResource
                          + "<-");
                }
                if (attachMode.equals(UtilityState.ATTCH_BUKTIRETUR.getId())) {
                  field_Attach_ReducedFinal_BUKTIRETUR.postValue(bitmapResource);
                  HyperLog.d(TAG,
                      "DANWIN mode " + attachMode + " set file tumbnail attc->" + bitmapResource
                          + "<-");
                }
              }

              @Override
              public void onLoadCleared(@Nullable Drawable placeholder) {
                HyperLog.d(TAG,
                    attachMode + "DANWIN onLoadCleared terpanggil, apa yang harus dilakukan ?");
              }
            });
          } else {
            Resource error = Resource.Builder.error(
                getApplication().getString(R.string.failed_to_save_local_file));
            HyperLog.d(TAG,
                "DANWIN mode " + attachMode + " gagal menyimpan file dan post pesan->"
                    + getApplication().getString(
                    R.string.failed_to_save_local_file) + "<-");
            if (attachMode.equals(UtilityState.ATTCH_PO.getId()))
              field_Attach_ReducedFinal_PO.postValue(error);
            if (attachMode.equals(UtilityState.ATTCH_STNK.getId()))
              field_Attach_ReducedFinal_STNK.postValue(error);
            if (attachMode.equals(UtilityState.ATTCH_KK.getId()))
              field_Attach_ReducedFinal_KK.postValue(error);
            if (attachMode.equals(UtilityState.ATTCH_BPKB.getId()))
              field_Attach_ReducedFinal_BPKB.postValue(error);
            if (attachMode.equals(UtilityState.ATTCH_SPT.getId()))
              field_Attach_ReducedFinal_SPT.postValue(error);
            if (attachMode.equals(UtilityState.ATTCH_SERAHTERIMA.getId()))
              field_Attach_ReducedFinal_SERAH_TERIMA.postValue(error);
            if (attachMode.equals(UtilityState.ATTCH_FISIKMOTOR.getId()))
              field_Attach_ReducedFinal_FISIK_MOTOR.postValue(error);
            if (attachMode.equals(UtilityState.ATTCH_HO))
              field_Attach_ReducedFinal_HO.postValue(error);
            if (attachMode.equals(UtilityState.ATTCH_BUKTIRETUR))
              field_Attach_ReducedFinal_BUKTIRETUR.postValue(error);
          }
        } catch (Exception e) {
          String msg = String.format("DANWIN Failed to save Attachment %s locally mode %s", att_id,
              attachMode);
          HyperLog.e(TAG, msg, e);
          HyperLog.exception(TAG, msg, e);
          setGeneralErrorMessage(msg);
        } finally {
          if (fos != null) {
            try {
              fos.close();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
          HyperLog.d(TAG, "DANWIN mode " + attachMode + " onSuccess[SELESAI]");
        }
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        HyperLog.e(TAG,
            String.format(
                "DANWIN Failed to download AppBooking MODE[ %s] Attachment %s, cause %s",
                attachMode, att_id, message), t);
        HyperLog.exception(TAG, String.format(
            "DANWIN Failed to download AppBooking MODE[ %s] Attachment %s, cause %s",
            attachMode, att_id, message), t);
      }
    }, appExecutors.networkIO());

  }

  public LiveData<AppBookingFormState> getFormState() {
    return formState;
  }

  public void setFormState(AppBookingFormState fs) {
    formState.postValue(fs);
  }

  public UUID getField_AppBookingId() {
    return field_AppBookingId;
  }

  public void setField_AppBookingId(UUID id) {
    field_AppBookingId = id;
  }

  public MutableLiveData<String> getField_AppBookingTypeId() {
    return appBookingTypeId;
  }

  public void setField_AppBookingTypeId(String data) {
    appBookingTypeId.postValue(data);
  }

  public MutableLiveData<String> getField_AlasanPinjam() {
    return field_AlasanPinjam;
  }

  public void setField_AlasanPinjam(String alasanPinjam) {
    if (alasanPinjam == null || alasanPinjam.isEmpty()) {
      field_AlasanPinjam_OK = false;
      field_AlasanPinjam_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    } else {
      this.field_AlasanPinjam.postValue(alasanPinjam);
      field_AlasanPinjam_OK = true;
      field_AlasanPinjam_ERROR.postValue(null);
    }
    // validateProcess();
  }

  public String getField_AppOperation() {
    return appOperation;
  }

  public void setField_AppOperation(String appOperation) {
    this.appOperation = appOperation;
  }

  public MutableLiveData<BigDecimal> getField_ConsumerAmount() {
    return this.field_ConsumerAmount;
  }

  public void setField_ConsumerAmount(BigDecimal data) {
    if (!Objects.equals(data, field_ConsumerAmount.getValue())) {
      if (data == null) {
        field_ConsumerAmount_OK = false;
        this.field_ConsumerAmount_ERROR
            .postValue(getApplication().getString(R.string.validation_mandatory));
      } else {
        field_ConsumerAmount_OK = true;
        this.field_ConsumerAmount_ERROR.postValue(null);
      }
      if (!Objects.equals(data, this.field_ConsumerAmount.getValue())) {
        this.field_ConsumerAmount.postValue(data);
        staticConsumerAmt = Objects.equals(data, null) ? BigDecimal.ZERO : data;
      }

      // validateProcess();
      setField_BookingAmount();
      if (!isLoadProcess) {
        setShowBuktiRetur(true, false, false, data);
      }
    }
  }

  public MutableLiveData<String> getField_ConsumerAmount_ERROR() {
    return this.field_ConsumerAmount_ERROR;
  }

  public MutableLiveData<BigDecimal> getField_FIFAmount() {
    return this.field_FIFAmount;
  }

  public void setField_FIFAmount(BigDecimal data) {
    if (!Objects.equals(data, this.field_FIFAmount.getValue())) {
      this.field_FIFAmount.postValue(data);
      staticFifAmt = Objects.equals(data, null) ? BigDecimal.ZERO : data;
    }

    validation_field_PencairanFIF(this.field_IsOpenClose.getValue(), data);

    setField_BookingAmount();
    if (!isLoadProcess) {
      setShowBuktiRetur(false, false, true, data);
    }
  }

  public MutableLiveData<String> getField_FIFAmount_ERROR() {
    return this.field_FIFAmount_ERROR;
  }

  public MutableLiveData<BigDecimal> getField_BiroJasaAmount() {
    return this.field_BiroJasaAmount;
  }

  public void setField_BiroJasaAmount(BigDecimal data) {
    if (!Objects.equals(data, field_BiroJasaAmount.getValue())) {
      field_BiroJasaAmount.postValue(data);
      staticBjAmt = Objects.equals(data, null) ? BigDecimal.ZERO : data;
    }
    setField_BookingAmount();
    if (!isLoadProcess) {
      setShowBuktiRetur(false, true, false, data);
    }
  }

  public MutableLiveData<BigDecimal> getField_BookingAmount() {
    return this.field_BookingAmount;
  }

  public MutableLiveData<BigDecimal> getField_FeeMatrix() {
    return this.field_FeeMatrix;
  }

  public void setField_FeeMatrix(BigDecimal amt) {
    if (amt != this.field_FeeMatrix.getValue())
      validation_field_FeeMatrix_From_FeeMatrix(amt);
  }

  public MutableLiveData<String> getField_FeeMatrix_ERROR() {
    return this.field_FeeMatrix_ERROR;
  }

  public MutableLiveData<Boolean> getField_FeeMatrix_ISEDITABLE() {
    return this.field_FeeMatrix_ISEDITABLE;
  }

  public MutableLiveData<BigDecimal> getField_FeeScheme() {
    return this.field_FeeScheme;
  }

  public void setField_FeeScheme(BigDecimal amt) {
    boolean isPostNewValue = (amt != this.field_FeeScheme.getValue());
    HyperLog.d(TAG,
        "setField_FeeScheme amt[" + amt + "] field_FeeSchemeVal[" + field_FeeScheme.getValue()
            + "] isPostNewValue[" + isPostNewValue + "]");
    if (isPostNewValue) {
      field_FeeScheme.postValue(amt);
    }
  }

  public MutableLiveData<String> getField_FeeScheme_WARN() {
    return this.field_FeeScheme_WARN;
  }

  public MutableLiveData<Integer> getField_FeeScheme_ISVISIBLE() {
    return this.field_FeeScheme_ISVISIBLE;
  }

  public MutableLiveData<Boolean> getField_FeeScheme_ISEDITABLE() {
    return this.field_FeeScheme_ISEDITABLE;
  }

  public MutableLiveData<String> getErrorMessage_VehicleNo() {
    return this.field_VehicleNo_ERROR;
  }

  protected void setField_BookingAmount() {
    String fn = "setField_BookingAmount() ";

    BigDecimal total = staticConsumerAmt.add(staticFifAmt).add(staticBjAmt);
    HyperLog.d(TAG, fn + "total[" + total + "]");
    this.field_BookingAmount.postValue(total);

    OutletShortData osd = this.field_outletKvm.getValue() != null
        ? this.field_outletKvm.getValue().getKey()
        : null;
    KeyValueModel<SoShortData, String> soKvm = this.getField_soKvm().getValue();
    SoShortData sod = soKvm != null ? soKvm.getKey() : null;
    validation_field_FeeScheme(sod, osd, total);
    if (total == null) {
      field_BookingAmount_OK = false;
      field_BookingAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    } else if (total.compareTo(BigDecimal.ZERO) <= 0) {
      field_BookingAmount_OK = false;
      field_BookingAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    } else {
      field_BookingAmount_OK = true;
      field_BookingAmount_ERROR.postValue(null);
    }
    // validateProcess();
  }

  public MutableLiveData<String> getField_BookingAmount_ERROR() {
    return this.field_BookingAmount_ERROR;
  }

  public MutableLiveData<Date> getField_UpdateTS() {
    return field_UpdateTs;
  }

  public void setField_UpdateTS(Date date) {
    this.field_UpdateTs.postValue(date);
  }

  public MutableLiveData<List<String>> getWfTaskList() {
    return wfTaskList;
  }

  public void setWfTaskList(List<String> list) {
    wfTaskList.postValue(list);
  }

  public MutableLiveData<Boolean> isReadOnly_AppNoPoNoPoDatePoAttchment() {
    return readOnly_AppNoPoNoPoDatePoAttchment;
  }

  //  public void setReadOnly_AppNoPoNoPoDatePoAttchment(boolean data) {
  //    readOnly_AppNoPoNoPoDatePoAttchment.postValue(data);
  //  }

  protected abstract void setReadOnly_AppNoPoNoPoDatePoAttchment2(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans, String appNo, String poNo, LocalDate poDate,
      boolean isAttchmentPoExist);

  public MutableLiveData<String> getOtpVerifyFailMsg() {
    return otpVerifyFailMsg;
  }

  public MutableLiveData<Boolean> getField_Attach_PO_isArchive() {
    return field_Attach_PO_isArchive;
  }

  public void setField_Attach_PO_isArchive(Boolean isArchive) {
    this.field_Attach_PO_isArchive.postValue(isArchive); 
  }

  public MutableLiveData<Boolean> getField_Attach_STNK_isArchive() {
    return field_Attach_STNK_isArchive;
  }

  public void setField_Attach_STNK_isArchive(Boolean isArchive) {
    this.field_Attach_STNK_isArchive.postValue(isArchive);
  }

  public MutableLiveData<Boolean> getField_Attach_KK_isArchive() {
    return field_Attach_KK_isArchive;
  }

  public void setField_Attach_KK_isArchive(Boolean isArchive) {
    this.field_Attach_KK_isArchive.postValue(isArchive);
  }

  public MutableLiveData<Boolean> getField_Attach_BPKB_isArchive() {
    return field_Attach_BPKB_isArchive;
  }

  public void setField_Attach_BPKB_isArchive(Boolean isArchive) {
    this.field_Attach_BPKB_isArchive.postValue(isArchive);
  }

  public MutableLiveData<Boolean> getField_Attach_SPT_isArchive() {
    return field_Attach_SPT_isArchive;
  }

  public void setField_Attach_SPT_isArchive(Boolean isArchive) {
    this.field_Attach_SPT_isArchive.postValue(isArchive);
  }

  public MutableLiveData<Boolean> getField_Attach_SERAH_TERIMA_isArchive() {
    return field_Attach_SERAH_TERIMA_isArchive;
  }

  public void setField_Attach_SERAH_TERIMA_isArchive(Boolean isArchive) {
    this.field_Attach_SERAH_TERIMA_isArchive.postValue(isArchive);
  }

  public MutableLiveData<Boolean> getField_Attach_FISIK_MOTOR_isArchive() {
    return field_Attach_FISIK_MOTOR_isArchive;
  }

  public void setField_Attach_FISIK_MOTOR_isArchive(Boolean isArchive) {
    this.field_Attach_FISIK_MOTOR_isArchive.postValue(isArchive);
  }

  public MutableLiveData<Boolean> getField_Attach_HO_isArchive() {
    return field_Attach_HO_isArchive;
  }

  public void setField_Attach_HO_isArchive(Boolean isArchive) {
    this.field_Attach_HO_isArchive.postValue(isArchive);
  }

  public MutableLiveData<Boolean> getField_Attach_BUKTIRETUR_isArchive() {
    return field_Attach_BUKTIRETUR_isArchive;
  }

  public void setField_Attach_BUKTIRETUR_isArchive(Boolean isArchive) {
    this.field_Attach_BUKTIRETUR_isArchive.postValue(isArchive);
  }

  // --BLOCK_GETTER SETTER#---

  // --BLOCK_VALIDASI---

  protected void validation_field_CancelReason(String data, boolean isFromLoad) {
    if (isFromLoad) {
      field_CancelReason_ERROR.postValue(null);
      field_CancelReason_OK = true;
      return;
    }

    if (data != null) {
      field_CancelReason_ERROR.postValue(null);
      field_CancelReason_OK = true;
    } else {
      field_CancelReason_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
      field_CancelReason_OK = false;
    }
  }

  private void validation_field_FeeMatrix_From_OutletAndSo(OutletShortData osd, SoShortData sod) {
    String fn = "validation_field_FeeMatrix_From_OutletAndSo() ";
    if (isLoadProcess)
      return;
    if (sod != null && osd != null) {
      if (sod.isKaryawan()||sod.isNonKios()) {
        HyperLog.d(TAG, fn
            + "karena (so adalah karyawan atau non kios) dan outlet tidak kosong, masuk blok pengecekan soId["
            + sod.getSoId() + "]");
        if (this.field_FeeMatrix.getValue() == null) {
          this.field_FeeMatrix.postValue(osd.getMinEmployeeMatrixFee());
        }
        this.field_FeeMatrix_ISEDITABLE.postValue(true);
      } else {
        this.field_FeeMatrix.postValue(BigDecimal.ZERO);
        this.field_FeeMatrix_OK = true;
        this.field_FeeMatrix_ERROR.postValue(null);
        this.field_FeeMatrix_ISEDITABLE.postValue(false);
      }
    } else {
      this.field_FeeMatrix_OK = true;
      this.field_FeeMatrix_ERROR.postValue(null);
      this.field_FeeMatrix_ISEDITABLE.postValue(true);
    }
  }

  private void validation_field_FeeMatrix_From_FeeMatrix(BigDecimal amt) {
    KeyValueModel<OutletShortData, String> outletKvm = this.field_outletKvm.getValue();
    KeyValueModel<SoShortData, String> soKvm = this.field_SoKvm.getValue();
    OutletShortData osd = outletKvm != null ? outletKvm.getKey() : null;
    SoShortData sod = soKvm != null ? soKvm.getKey() : null;

    if (osd == null || sod == null) {
      this.field_FeeMatrix_OK = true;
      this.field_FeeMatrix_ERROR.postValue(null);
      this.field_FeeMatrix_ISEDITABLE.postValue(true);
    } else {

      if (sod.isKaryawan() || sod.isNonKios()) {
        HyperLog.d(TAG, "karena ini so adalah karyawan atau non kios, masuk blok pengecekan");
        if (amt == null) {
          this.field_FeeMatrix.postValue(amt);
          this.field_FeeMatrix_OK = false;
          this.field_FeeMatrix_ERROR
              .postValue(getApplication().getString(R.string.validation_mandatory));
        } else {
          this.field_FeeMatrix_OK = true;
          this.field_FeeMatrix_ERROR.postValue(null);
        }
      } else {
        this.field_FeeMatrix.postValue(BigDecimal.ZERO);
        this.field_FeeMatrix_OK = true;
        this.field_FeeMatrix_ERROR.postValue(null);
        this.field_FeeMatrix_ISEDITABLE.postValue(false);
      }
    }
  }

  private void validation_field_FeeScheme(SoShortData sod, OutletShortData osd,
      BigDecimal amtTotal) {
    if (isLoadProcess)
      return;
    String fn = "validation_field_FeeScheme() ";
    this.field_FeeScheme_ISEDITABLE.postValue(true);
    this.field_FeeScheme_ISVISIBLE.postValue(View.VISIBLE);
    HyperLog.d(TAG, fn + "osd[" + osd + "] jika osd null, post outletKvm ERROR");
    if (osd == null) {
      field_outletKvm_OK = false;
      field_outletKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    } else {
      field_outletKvm_OK = true;
      field_outletKvm_ERROR.postValue(null);

      HyperLog.d(TAG, fn + "oName ->" + osd.getOutletName() + "<- oId->" + osd.getOutletId());
      boolean isHybrid = osd.isHybrid();
      boolean isNonKios = sod == null ? false : sod.isNonKios();
      boolean isFeeScheme = osd.isFeeScheme();
      BigDecimal feeSchemeAmt = osd.getFeeSchemeAmt();
      BigDecimal feeSchemeLimit = osd.getFeeSchemeLimit();

      if (isHybrid || isNonKios) {
        HyperLog.d(TAG, fn + "oId->" + osd.getOutletId() + "<- [HYBRID START]");
        HyperLog.d(TAG, fn + "amtTotal > feeSchemelimit ->" + amtTotal + " > " + feeSchemeLimit);
        if (amtTotal != null && amtTotal.compareTo(feeSchemeLimit) == 1) {
          HyperLog.d(TAG,
              fn + "BLOCK[isHybrid] setFeeScheme dengan feeScheme Amt ->" + feeSchemeAmt);
          this.field_FeeScheme.postValue(feeSchemeAmt);
        } else {
          HyperLog.d(TAG, fn + " BLOCK[isHybrid] setFeeScheme dengan 0");
          this.field_FeeScheme.postValue(BigDecimal.ZERO);
        }
        HyperLog.d(TAG, fn + "setFeeScheme !EDITABLE");
        if (isNonKios)
          this.field_FeeScheme_ISEDITABLE.postValue(true);
        else
          this.field_FeeScheme_ISEDITABLE.postValue(false);
        HyperLog.d(TAG, fn + "oId->" + osd.getOutletId() + "<- [HYBRID END]");
      } else {
        HyperLog.d(TAG, fn + "oId->" + osd.getOutletId() + "<- [!HYBRID START]");
        if (isFeeScheme) {
          if (feeSchemeAmt.compareTo(BigDecimal.ZERO) == 0)
            this.field_FeeScheme_WARN
                .postValue(getApplication().getString(R.string.fee_scheme_warn));
          else
            this.field_FeeScheme_WARN.postValue(null);

          HyperLog.d(TAG, fn + "BLOCK[isFeeScheme] set [0] oId->" + osd.getOutletId()
              + "<- [isFEESCHEME START]");
          this.field_FeeScheme.postValue(BigDecimal.ZERO);
          HyperLog.d(TAG, fn + "oId->" + osd.getOutletId() + "<- [isFEESCHEME END]");
        } else {
          HyperLog.d(TAG, fn + "oId->" + osd.getOutletId() + "<- [!isFEESCHEME START]");
          this.field_FeeScheme_ISEDITABLE.postValue(false);
          this.field_FeeScheme_ISVISIBLE.postValue(View.GONE);
          HyperLog.d(TAG, fn + "oId->" + osd.getOutletId() + "<- [!isFEESCHEME END]");
        }
        HyperLog.d(TAG, fn + "oId->" + osd.getOutletId() + "<- [!HYBRID END]");
      }
    }
  }

  private void validation_field_Idnpksf(SoShortData sod, String idnpksf) {
    if (isLoadProcess)
      return;
    
    HyperLog.d(TAG, "validation_field_Idnpksf: soId->" + (sod != null ? sod.getSoId() : null) + "<-");
    if (sod != null && !sod.isIdSalesForce() && !sod.isNonKios()) {
      field_Idnpksf_OK = true;
      this.field_Idnpksf_ERROR.postValue(null);
    } else {
      if (idnpksf == null) {
        field_Idnpksf_OK = false;
        this.field_Idnpksf_ERROR
            .postValue(getApplication().getString(R.string.validation_mandatory));
      } else {
        field_Idnpksf_OK = true;
        this.field_Idnpksf_ERROR.postValue(null);
      }

    }
    // validateProcess();
  }

  protected void validation_field_PencairanFIF(Boolean isOpenClose, BigDecimal fif) {
    if (isLoadProcess)
      return;

    if (isOpenClose != null && isOpenClose) {
      if (fif != null) {
        this.field_FIFAmount_OK = true;
        this.field_FIFAmount_ERROR.postValue(null);
      } else {
        this.field_FIFAmount_OK = false;
        this.field_FIFAmount_ERROR
            .postValue(getApplication().getString(R.string.validation_mandatory));
      }
    }
    // validateProcess();
  }

  protected void validation_field_PonoDateAttach(String poNo, LocalDate poDate, File poAttch,
      List<KeyValueModel<UUID, String>> poprList) {
    String fnc = "validation_field_PonoDateAttach ";
    String msgPo = "poNo is NULL->" + (poNo == null) + "<-";
    String msgPoDate = ", poDate is NULL ->" + (poDate == null) + "<-";
    String msgPoAttch = ", poAttach is NULL->" + (poAttch == null) + "<-";
    boolean isList = poprList == null ? false : (poprList.size() > 0 ? true : false);
    String msgList = ", poprList is EMPTY ->" + isList + "<-";

    HyperLog.d(TAG, fnc + msgPo + msgPoDate + msgPoAttch + msgList);
    if (poNo == null && poDate == null && poAttch == null) {
      if (!isList) {
        field_poprList_OK = false;
        field_poprList_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
      } else {
        field_poprList_OK = true;
        field_poprList_ERROR.postValue(null);
      }
      field_PoDate_OK = true;
      field_PoDate_ERROR.postValue(null);
      field_PoNo_OK = true;
      field_PoNo_ERROR.postValue(null);
      field_Attach_FINAL_PO_OK.postValue(true);
    } else {
      field_poprList_OK = true;
      field_poprList_ERROR.postValue(null);
      if (poNo == null) {
        field_PoNo_OK = false;
        field_PoNo_ERROR.postValue(getApplication().getString(R.string.validation_message));
      } else {
        field_PoNo_OK = true;
        field_PoNo_ERROR.postValue(null);
      }
      if (poDate == null) {
        field_PoDate_OK = false;
        field_PoDate_ERROR.postValue(getApplication().getString(R.string.validation_message));
      } else {
        field_PoDate_OK = true;
        field_PoDate_ERROR.postValue(null);
      }

      // jika semua field po terisi, maka poPendingReasonDihapus
      if (poNo != null && poDate != null && poAttch != null) {
        List<KeyValueModel<UUID, String>> currentPoprList = new ArrayList<KeyValueModel<UUID, String>>();
        field_poprList.postValue(new ArrayList<>(currentPoprList));
      }
      if (poAttch == null)
        field_Attach_FINAL_PO_OK.postValue(false);
      else
        field_Attach_FINAL_PO_OK.postValue(true);
    }
  }

  public boolean validateProcessDraft(boolean isCash) {
    if (!field_outletKvm_OK)
      field_outletKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_lobKvm_OK)
      field_LobKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_SoKvm_OK)
      field_SoKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));

    if(isCash) {
      if (!field_AlasanPinjamCash_OK)
        field_AlasanPinjamCash_ERROR
            .postValue(getApplication().getString(R.string.validation_mandatory));
    }else{
      if (!field_AlasanPinjam_OK)
        field_AlasanPinjam_ERROR
            .postValue(getApplication().getString(R.string.validation_mandatory));
    }

    if (!field_poprList_OK)
      field_poprList_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));

    if (!field_ConsumerName_OK)
      field_ConsumerName_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));

    if (!field_ConsumerAddress_OK)
      field_ConsumerAddress_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    if (!field_ConsumerPhone_OK)
      field_ConsumerPhone_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    if (!field_VehicleNo_OK)
      field_VehicleNo_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));

    boolean mandatori_field =
        field_outletKvm_OK && field_lobKvm_OK && field_SoKvm_OK &&
            (isCash?field_AlasanPinjamCash_OK:field_AlasanPinjam_OK) && field_poprList_OK
            && field_ConsumerName_OK && field_ConsumerAddress_OK && field_ConsumerPhone_OK
            && field_VehicleNo_OK;
    if (!mandatori_field)
      setGeneralErrorMessage(getApplication().getString(R.string.validation_message));
    return mandatori_field;
  }

  public boolean validateProcess() {
    // cek semua field mandatrory sudah terisi atau belum
    if (!field_outletKvm_OK)
      field_outletKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_lobKvm_OK)
      field_LobKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_SoKvm_OK)
      field_SoKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_Idnpksf_OK)
      field_Idnpksf_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_AlasanPinjam_OK)
      field_AlasanPinjam_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_AlasanPinjamCash_OK)
      field_AlasanPinjamCash_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_poprList_OK)
      field_poprList_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerName_OK)
      field_ConsumerName_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerAddress_OK)
      field_ConsumerAddress_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerPhone_OK)
      field_ConsumerPhone_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_VehicleNo_OK)
      field_VehicleNo_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerAmount_OK)
      field_ConsumerAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    validation_field_PencairanFIF(this.isField_isOpenClose().getValue(),
        this.getField_FIFAmount().getValue());

    if (!field_BookingAmount_OK)
      field_BookingAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    if (!field_FeeMatrix_OK)
      field_FeeMatrix_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));

    if (field_Attach_Final_KK == null)
      field_Attach_Final_KK_OK.postValue(false);
    if (field_Attach_Final_STNK == null)
      field_Attach_Final_STNK_OK.postValue(false);
    if (field_Attach_Final_BPKB == null)
      field_Attach_Final_BPKB_OK.postValue(false);
    if (field_Attach_Final_SPT == null)
      field_Attach_Final_SPT_OK.postValue(false);
    if (field_Attach_Final_SERAH_TERIMA == null)
      field_Attach_Final_SERAH_TERIMA_OK.postValue(false);

    boolean isShowBR = isShowBuktiRetur().getValue().booleanValue();
    boolean mandatoryBR = isShowBR && field_Attach_Final_BUKTIRETUR != null
        || !isShowBR && field_Attach_Final_BUKTIRETUR == null;
    if (!mandatoryBR)
      field_Attach_Final_BUKTIRETUR_OK.postValue(false);

    boolean mandatori_field = field_outletKvm_OK && field_lobKvm_OK && field_SoKvm_OK
        && field_Idnpksf_OK && field_AlasanPinjam_OK && field_AlasanPinjamCash_OK && field_PoNo_OK
        && field_PoDate_OK && field_Attach_FINAL_PO_OK.getValue() && field_poprList_OK
        && field_ConsumerName_OK && field_ConsumerAddress_OK && field_ConsumerPhone_OK
        && field_VehicleNo_OK && field_ConsumerAmount_OK && field_FIFAmount_OK
        && field_BookingAmount_OK && field_FeeMatrix_OK;
    // cek semua Attachment mandatrory sudah terisi atau belum
    boolean mandatory_attach = field_Attach_Final_KK != null && field_Attach_Final_STNK != null
        && field_Attach_Final_BPKB != null && field_Attach_Final_SPT != null
        && field_Attach_Final_SERAH_TERIMA != null && mandatoryBR;

    if (field_PoDate.getValue() != null && field_PoDate.getValue().compareTo(LocalDate.now()) > 0) {
      setGeneralErrorMessage(getApplication().getString(R.string.podate_validation));
      return false;
    }

    // jika semua field dan attachment mandatory sudah terisi, maka tombol action dapat digunakan
    if (mandatori_field && mandatory_attach) {
      // this.canProcess.postValue(true);
      return true;
    } else {
      // this.canProcess.postValue(false);
      setGeneralErrorMessage(getApplication().getString(R.string.validation_message));
      return false;
    }
  }

  /**
   * @param vehicleNo   data yang akan divalidasi
   * @param bookingDate null jika validasi ini dipanggil bukan dari process LoadData
   */
  public void validate_field_VehicleNo(String vehicleNo, LocalDate bookingDate) {
    if (!getField_AppOperation().equals(Utility.OPERATION_NEW)) {
      field_VehicleNo_OK = true;
      field_VehicleNo_ERROR.postValue(null);
      return;
    }
    if (vehicleNo.isEmpty()) {
      field_VehicleNo_OK = false;
      field_VehicleNo_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
      return;
    }
    //    formState.postValue(
    //        AppBookingFormState.loading(getApplication().getString(R.string.silahkan_tunggu)));

    validateVehilceProgress.postValue(true);
    // process valid
    UUID appBookingId = this.field_AppBookingId;
    bookingDate = bookingDate == null ? this.field_BookingDate.getValue() : bookingDate;
    String appBookingDateString = bookingDate.format(Formatter.DTF_dd_MM_yyyy);
    ListenableFuture<String> isValid_Lf = appBookingService.validateVehicleNo(appBookingId,
        vehicleNo, getField_AppOperation(), appBookingDateString);
    HyperLog.d(TAG, "VALIDATE setField_VehicleNo: vehicleNo ->" + vehicleNo + "<- bookingDate ->"
        + bookingDate + "<-");

    Futures.addCallback(isValid_Lf, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String msg) {
        if (!msg.isEmpty()) {
          String message = getApplication().getString(R.string.validation_vehicleno_message,
              vehicleNo);
          //          formState.postValue(AppBookingFormState.ready(message, false, screenMode.getValue()));
          validateVehilceProgress.postValue(false);
          HyperLog.d(TAG, "VALIDATE setField_VehicleNo: SET FALSE");
          field_VehicleNo_OK = false;
          field_VehicleNo_ERROR.postValue(msg);
        } else {
          //          formState.postValue(AppBookingFormState.ready(null, false, screenMode.getValue()));
          validateVehilceProgress.postValue(false);
          HyperLog.d(TAG, "VALIDATE setField_VehicleNo: SET TRUE");
          field_VehicleNo_OK = true;
          field_VehicleNo_ERROR.postValue(null);
        }
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        formState.postValue(AppBookingFormState.errorProcess("Validasi gagal ->" + message));
      }
    }, appExecutors.networkIO());
  }
  // ---BLOCK_VALIDASI#---

  // ---BLOCK_PROCESS---
  protected List<UUID> getPoprIdList() {
    if (field_poprList.getValue() == null)
      return new ArrayList<>();

    List<UUID> poprIdList = this.field_poprList.getValue().stream().map(obj -> {
      UUID objId = obj.getKey();
      return objId;
    }).collect(Collectors.toList());
    return poprIdList;
  }

  public void processLoadAppBookingData(UUID id) {
    appBookingLoadUC.loadAppBookingData(id, false);
  }

  public void processLoadAppBookingDataByBooking(UUID bookingId, Boolean isEdit,
      Boolean isTransfer) {
    isFirstTimeLoadFromEditorCancel = true;
    appBookingLoadUC.loadAppBookingDataByBookingId(bookingId, isEdit, isTransfer);
  }

  public void processPrepareBookingForm() {
    prepareUC.prepare(false);
  }

  public abstract void processSaveDraftNew(boolean processSaveDraftNew);

  public abstract void processSaveDraftCancel(boolean isNewDocument);

  public abstract void processSaveDraftEdit(boolean processSaveDraftNew, boolean isNewDocument);

  public abstract void processWfSubmitNew();

  public abstract void processWfSubmitCancel(boolean isNewDocument);

  public abstract void processWfSubmitEdit(boolean isNewDocument);

  //  public abstract void processSubmitRevision(String comment);

  // ---#BLOCK_PROCESS---
  protected void setPrepareForm(NewBookingReponse nbr) {
    allowBackdate = nbr.isAllowBackDate();
    outletList_Ld.postValue(nbr.getAssignedOutlet());
    soList_Ld.postValue(nbr.getSoDataList());
    lobList_Ld.postValue(nbr.getLobData());
    poprList_Ld.postValue(nbr.getPoprDataList());
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.prepareUC.unregisterListener(this);
    this.processUC.unregisterListener(this);
  }

  // ---BLOCK_LISTENER---
  // listener untuk appBookingAction
  @Override
  public void onAppBookingProcessStarted(Resource<Boolean> loading) {
    HyperLog.d(TAG, "AppBookingProcess START" + loading.getMessage());
    formState.postValue(AppBookingFormState.inProgress(loading.getMessage()));
  }

  @Override
  public void onAppBookingProcessSuccess(Resource<Boolean> res) {
    /**
     * penambahan variable isSuccess validasi untuk mengetahui apakah proses benar benar sukses
     * atau gagal dan memberi signal apk untuk logout
     * (jika otp gagal verify dan mengharuskan apk untuk logout)
     */
    boolean isSuccess = res.getData();
    if(isSuccess){
      HyperLog.d(TAG, "AppBookingProcess SUCCESS" + res.getMessage());
      formState.postValue(AppBookingFormState.ready(res.getMessage(), true, null, null));
      otpVerifyFailMsg.postValue(null);
    }else{
      HyperLog.d(TAG, "AppBookingProcess FAILED" + res.getMessage()
          + " berikan signal ke fragment untuk force logout");
      otpVerifyFailMsg.postValue(res.getMessage());
    }
  }

  @Override
  public void onAppBookingProcessFailure(Resource<Boolean> res) {
    HyperLog.e(TAG, "AppBookingProcess FAIL");
    formState.postValue(AppBookingFormState.errorProcess(res.getMessage()));
  }

  // listener untuk appBooking Prepare form
  @Override
  public void onPrepareAppBookingStarted() {
    HyperLog.d(TAG, "Prepare Pengajuan Booking START");
    setField_Attach_PO_isArchive(false);
    setField_Attach_STNK_isArchive(false);
    setField_Attach_KK_isArchive(false);
    setField_Attach_BPKB_isArchive(false);
    setField_Attach_SPT_isArchive(false);
    setField_Attach_SERAH_TERIMA_isArchive(false);
    setField_Attach_FISIK_MOTOR_isArchive(false);
    formState.postValue(AppBookingFormState.inProgress("Menyiapkan data"));
  }

  @Override
  public void onPrepareAppBookingSuccess(Resource<NewBookingReponse> response) {
    setPrepareForm(response.getData());
    AppBookingCashScreenMode screenMode = AppBookingCashScreenMode.getScreenMode(true,
        Utility.OPERATION_NEW, null, null);
    formState.postValue(AppBookingFormState.ready(response.getMessage(), false, screenMode, null));
  }

  @Override
  public void onPrepareAppBookingFailure(Resource<NewBookingReponse> resource) {
    HyperLog.e(TAG, "Persiapan Pengajuan Booking GAGAL");
    formState.postValue(AppBookingFormState.errorLoadData(resource.getMessage()));
  }

  // listener untuk load data form
  @Override
  public void onLoadStarted() {
    HyperLog.d(TAG, "Prepare Pengajuan Booking START");
    formState.postValue(AppBookingFormState.inProgress("Menyiapkan data"));
  }

  @Override
  public void onLoadSuccess(Resource<AppBookingLoadUC.LoadResult> res) {
    String msg = "method onLoad ini seharusnya di override oleh class yang meng extend";
    HyperLog.exception(TAG, msg);
    RuntimeException re = new RuntimeException(msg);
    ACRA.getErrorReporter().handleException(re);
  }

  @Override
  public void onLoadFailure(Resource<AppBookingLoadUC.LoadResult> res) {
    HyperLog.e(TAG, "Persiapan Pengajuan Booking GAGAL");
    formState.postValue(AppBookingFormState.errorLoadData(res.getMessage()));
  }

  protected KeyValueModel<UUID, String> getPoPrWarn() {

    KeyValueModel<UUID, String> data = new KeyValueModel<>(null, null);
    List<PoPendingReasonShortData> poprList = poprList_Ld.getValue() != null
        ? poprList_Ld.getValue()
        : new ArrayList<>();
    for (PoPendingReasonShortData poprSd : poprList) {
      if (poprSd.isPendingReasonFIFCloseCode()) {
        data = new KeyValueModel<>(poprSd.getPoprId(), poprSd.getName());
      }
    }
    return data;
  }
  // ---BLOCK_LISTENER#---
}
