package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.jetbrains.annotations.NotNull;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

public class AppBookingListAdapter
    extends PagingDataAdapter<AppBookingData, AppBookingListAdapter.ViewHolder> {

  private final static String TAG = AppBookingListAdapter.class.getSimpleName();
  private final MyAppBookingDataRecyclerViewAdapterListener mListener;
  private Context mCtx;
  private SparseBooleanArray selectedItems;
  private SparseBooleanArray animationItemsIndex;
  private List<AppBookingData> selectedAppBookingData = new ArrayList<>();

  public AppBookingListAdapter(MyAppBookingDataRecyclerViewAdapterListener listener, Context ctx) {
    super(new AppBookingDataDiffCallBack());
    mCtx = ctx;
    mListener = listener;
    selectedItems = new SparseBooleanArray();
    animationItemsIndex = new SparseBooleanArray();
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_appbooking_item,
        parent, false);
    return new ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
    holder.mView.setActivated(selectedItems.get(position, false));
//    HyperLog.d(TAG, "onBindViewHolder: pos->" + selectedItems.get(position, false) + "<-");
  }

  public int getSelectedItemCout() {
    return selectedItems.size();
  }

  public List<AppBookingData> getSelectedAppBookingData(){
    return selectedAppBookingData;
  }

  public void clearSelections() {
    selectedItems.clear();
    selectedAppBookingData.clear();
    notifyDataSetChanged();
  }

  public void toggleSelection(int pos) {
    AppBookingData data = getItem(pos);
    if (selectedItems.get(pos, false)) {
      selectedItems.delete(pos);
      selectedAppBookingData.remove(data);
      animationItemsIndex.delete(pos);

    } else {
      selectedAppBookingData.add(data);
      selectedItems.put(pos, true);
      animationItemsIndex.put(pos, true);
    }
    notifyItemChanged(pos);
  }

  public boolean isAllowaCaptainAction() {
    for (AppBookingData data : selectedAppBookingData) {
      if (!WorkflowConstants.WF_STATUS_PENDING_APPROVAL_CAPTAIN.equals(data.getWfId())) {
        return false;
      }
    }
    return true;
  }

  public void resetAnimationIndex() {
    animationItemsIndex.clear();
  }

  interface MyAppBookingDataRecyclerViewAdapterListener {
    void onItemClicked(View v, AppBookingData appBookingData, int position);

    void onItemLongClicked(View v, AppBookingData data, int pos);
  }

  private static class AppBookingDataDiffCallBack extends DiffUtil.ItemCallback<AppBookingData> {

    @Override
    public boolean areItemsTheSame(@NonNull AppBookingData oldItem,
        @NonNull AppBookingData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull AppBookingData oldItem,
        @NonNull AppBookingData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final ConstraintLayout mContainer;
    public final TextView mTrxNo;
    public final TextView mStatus;
    public final TextView mOutlet;
    public final TextView mBookingInfo;
    public final TextView mConsInfo;
    public final TextView mPengajuan;
    public AppBookingData mItem;

    public ViewHolder(@NotNull View v) {
      super(v);
      mView = v;
      mContainer = (ConstraintLayout) v.findViewById(R.id.clAppBookingContainer);
      mTrxNo = (TextView) v.findViewById(R.id.tvAppBookingTrxNo);
      mStatus = (TextView) v.findViewById(R.id.tvAppBookingStatus);
      mOutlet = (TextView) v.findViewById(R.id.tvAppBookingOutlet);
      mBookingInfo = (TextView) v.findViewById(R.id.tvAppBookingBdatePencairanType);
      mConsInfo = (TextView) v.findViewById(R.id.tvAppBookingConsData);
      mPengajuan = (TextView) v.findViewById(R.id.tvAppBookingPengajuan);
    }

    public void bind(AppBookingData appData, int position) {
      this.mItem = appData;
      appData.getId();
      boolean isAvailableData = appData != null;
      setObject(appData, isAvailableData);
      setObjectListener(appData, position);
    }

    public void setObject(AppBookingData a, boolean isAvailableData) {
      String trxNo = a.getTrxNo() != null ? a.getTrxNo() : "-";
      String status = a.getWfName() != null ? a.getWfName() : "-";

      String outletNameTypeCode = !isAvailableData ? "Loading" : a.getOutletObj().getOutletName();

      String bookingDatePencairan = a.getBookingDate() != null
          ? a.getBookingDate().format(Formatter.DTF_dd_MM_yyyy) + " (" + a.getBookingTypeName()
              + ")"
          : "-";
      String consumerNo = a.getConsumerNo()!=null ?a.getConsumerNo(): "-";
      String consumerName = a.getConsumerName() != null ? a.getConsumerName() : "-";
      String vehicleNo = !isAvailableData ? "Loading"
          : (a.getVehicleNo() != null && !a.getVehicleNo().isEmpty() ? " - " + a.getVehicleNo()
              : "");
      String consumerInfo = consumerNo + " - " + consumerName + vehicleNo;

      String pengajuanInfo = !isAvailableData ? "Loading"
          : (a.getPengajuanDate() != null ? Formatter.SDF_dd_MM_yyyy.format(a.getPengajuanDate()) : " - ") + " ("
              + (!isAvailableData ? "Loading"
                  : (a.getAppTypeName() != null ? a.getAppTypeName() : " - "))
              + ")";

      mTrxNo.setText(trxNo);
      mStatus.setText(status);
      mOutlet.setText(outletNameTypeCode);
      mBookingInfo.setText(bookingDatePencairan);
      mConsInfo.setText(consumerInfo);
      mPengajuan.setText(pengajuanInfo);
    }

    public void setObjectListener(AppBookingData data, int pos) {
      this.mContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, data, pos);
        }
      });

      this.mContainer.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
          mListener.onItemLongClicked(v, data, pos);
          v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
          return true;
        }
      });

    }
  }
}
