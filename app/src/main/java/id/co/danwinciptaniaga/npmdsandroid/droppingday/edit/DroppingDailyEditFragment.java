package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

import javax.inject.Inject;

import org.acra.ACRA;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyScreenMode;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.BuildConfig;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.data.ConstraintViolation;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentDroppingDailyEditBinding;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.DrawerViewModel;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.util.AnimationUtil;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfProcessParameter;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DroppingDailyEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
public class DroppingDailyEditFragment extends ProtectedFragment {
  private static final String TAG = DroppingDailyEditFragment.class.getSimpleName();

  private final int PERMISSION_REQUEST_CODE = 201;

  private static final String ARG_DROPPING_ID = "droppingId";
  public static final String SAVED_STATE_LD_ALT_BANK_ACCT_DATA = "altBankAcctData";

  @Inject
  CommonService commonService;
  @Inject
  AppExecutors appExecutors;

  private FragmentDroppingDailyEditBinding binding;
  private DroppingDailyEditViewModel viewModel;
  private ArrayAdapter outletsAdapter;
  private ArrayAdapter<KeyValueModel<UUID, String>> sourceAccAdapter;
  private String dateString;
  private LocalDate defaultTransferDate;
  private DrawerViewModel drawerVm;

  private View.OnClickListener restart = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // kita coba restart seluruh activity
      Bundle bundle = new Bundle();
      bundle.putSerializable(ARG_DROPPING_ID, viewModel.getDroppingId());
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_dropping_daily_edit)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragment = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      onReady();
    }
  };

  public DroppingDailyEditFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param droppingId
   * @return A new instance of fragment DroppingDailyEditFragment.
   */
  public static DroppingDailyEditFragment newInstance(UUID droppingId) {
    DroppingDailyEditFragment fragment = new DroppingDailyEditFragment();
    Bundle args = new Bundle();
    args.putSerializable(ARG_DROPPING_ID, droppingId);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    drawerVm = new ViewModelProvider(getActivity()).get(DrawerViewModel.class);
    UUID cosuId = Utility.getCurrentOrSubstitutedUserId(TAG, drawerVm);
    viewModel = new ViewModelProvider(this).get(DroppingDailyEditViewModel.class);
    DroppingDailyEditFragmentArgs args = DroppingDailyEditFragmentArgs
        .fromBundle(getArguments());
    viewModel.setCurrentOrSubstitutedUserId(cosuId);
    viewModel.setDroppingId(args.getDroppingId());
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    binding = FragmentDroppingDailyEditBinding.inflate(inflater, container, false);

    viewModel.getFormStateLd().observe(getViewLifecycleOwner(), this::onFormStateChange);

    viewModel.getActionEvent().observe(getViewLifecycleOwner(), this::onFormActionEvent);
    setupOutlet();

    setupRequestDate();
    setupDroppingDate();
    setupApprovedAmount();
    setupAltAccount();
    setupButtons();

    return binding.getRoot();
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    if (status.getStatus() == Status.LOADING) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      binding.pageContent.setVisibility(View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.GONE);
      binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);
      // retry = restart fragment (karena authentication gagal)
      binding.progressWrapper.retryButton.setOnClickListener(restart);
    }
  }

  private void onReady() {
    // tahap ini dilakukan 1 kali saja
    if (viewModel.getDroppingId() != null) {
      viewModel.loadDropping(false);
    } else {
      //      viewModel.prepareNewDropping(false);
    }
  }

  private void setupOutlet() {
    binding.spOutlet.setFocusable(false);
    binding.spOutlet.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
          int i, long l) {
        // todo sebetulnya cukup index-nya saja?
        viewModel.setSelectedOutlet(i);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
        viewModel.setSelectedOutlet(0);
      }
    });
  }

  @Override
  public void onResume() {
    super.onResume();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
      requestPermission();
  }

  private void requestPermission() {
    int resultReadExternalStorage = ContextCompat.checkSelfPermission(getContext(),
        Manifest.permission.READ_EXTERNAL_STORAGE);
    int resultWriteExternalStorage = ContextCompat.checkSelfPermission(getContext(),
        Manifest.permission.WRITE_EXTERNAL_STORAGE);

    if (resultReadExternalStorage != PackageManager.PERMISSION_GRANTED
        || resultWriteExternalStorage != PackageManager.PERMISSION_GRANTED) {
      requestPermissions(new String[] {
              Manifest.permission.READ_EXTERNAL_STORAGE,
              Manifest.permission.WRITE_EXTERNAL_STORAGE },
          PERMISSION_REQUEST_CODE);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    if (requestCode == PERMISSION_REQUEST_CODE) {
      if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)
          && permissions[1].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED
            && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
          Toast.makeText(getContext(),
              "Permission granted",
              Toast.LENGTH_LONG).show();
        } else {
          Toast.makeText(getContext(),
              "Anda harus mengizinkan aplikasi ini membaca/tulis ke media penyimpanan",
              Toast.LENGTH_LONG).show();
          NavController navController = Navigation.findNavController(binding.getRoot());
          navController.popBackStack();
        }
      }
    }
  }

  private void setupRequestDate() {
    binding.etRequestDate.setOnClickListener(v -> {
      LocalDate requestDate = viewModel.getRequestDate();
      int day = requestDate.getDayOfMonth();
      int month = requestDate.getMonthValue() - 1;
      int year = requestDate.getYear();
      DatePickerDialog dpdRequestDate = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate requestDate = LocalDate.of(year, month + 1, dayOfMonth);
              viewModel.setRequestDate(requestDate);
            }
          }, year, month, day);
      dpdRequestDate.show();
    });

    // error tetap harus ditampilkan baik read-only atau tidak
    viewModel.getRequestDateErrorLd().observe(getViewLifecycleOwner(), errorMsg -> {
      binding.tilRequestDate.setError(errorMsg);
    });
  }

  private void setupDroppingDate() {
    binding.etDroppingDate.setOnClickListener(v -> {
      LocalDate droppingDate = viewModel.getDroppingDate();
      int day = droppingDate.getDayOfMonth();
      int month = droppingDate.getMonthValue() - 1;
      int year = droppingDate.getYear();
      DatePickerDialog dpdDroppingDate = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate droppingDate = LocalDate.of(year, month + 1, dayOfMonth);
              viewModel.setDroppingDate(droppingDate);
            }
          }, year, month, day);
      dpdDroppingDate.show();
    });
  }

  private void setupApprovedAmount() {
    binding.etApprovedAmount.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (viewModel.getDdData() != null
            && viewModel.getDdData().getNetRequestAmount() != null &&
            validateApprovedAmount(s)) {
          Long approvedAmount = id.co.danwinciptaniaga.npmdsandroid.util.Utility.getLongAmtFromFromattedString(
              s.toString());
          viewModel.setApprovedAmount(approvedAmount);
        }
      }
    });
  }

  private void checkCanProcess() {
    switch (viewModel.getEditPageMode()) {
    case CAPTAIN_VERIFICATION:
      // validasi form captain
      if (validateApprovedAmount(binding.etApprovedAmount.getText())) {
        int buttonCount = binding.llWorkflowButtonContainer.getChildCount();
        HyperLog.d(TAG, "Enabling WF buttons: " + true);
        for (int z = 0; z < buttonCount; z++) {
          View view = binding.llWorkflowButtonContainer.getChildAt(z);
          view.setEnabled(true);
        }
      } else {

      }
    }
  }

  private void setupAltAccount() {
    binding.swAltAccount.setOnCheckedChangeListener((buttonView, isChecked) -> {
      if (isChecked) {
        binding.btnAltAcctEdit.setVisibility(View.VISIBLE);
      } else {
        binding.btnAltAcctEdit.setVisibility(View.GONE);
      }
      viewModel.setAltAccount(isChecked);
      updateAltAccountData();
    });

    binding.btnAltAcctEdit.setOnClickListener(v -> {
      NavController navController = Navigation.findNavController(binding.getRoot());
      DroppingDailyEditFragmentDirections.ActionEditAltAccount dir = DroppingDailyEditFragmentDirections.actionEditAltAccount();
      dir.setBankAccountData(viewModel.getBankAccountData());
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });
    NavController navController = NavHostFragment.findNavController(this);
    MutableLiveData<BankAccountData> altBankAcctData = navController
        .getCurrentBackStackEntry()
        .getSavedStateHandle()
        .getLiveData(SAVED_STATE_LD_ALT_BANK_ACCT_DATA);
    altBankAcctData.observe(getViewLifecycleOwner(), bankAccountData -> {
      viewModel.setBankAccountData(bankAccountData);
      updateAltAccountData();
      // hapus setelah selesai baca
      navController.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(SAVED_STATE_LD_ALT_BANK_ACCT_DATA);
    });
  }

  private void setupButtons() {
    binding.btnWfHistory.setOnClickListener(v -> {
      NavController navController = Navigation.findNavController(binding.getRoot());
      DroppingDailyEditFragmentDirections.ActionWfHistory dir =
          DroppingDailyEditFragmentDirections.actionWfHistory(
              WfType.DROPPING_HARIAN, viewModel.getDroppingId());
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });
  }

  private void onFormActionEvent(Resource<DroppingDailyData> s) {
    if (Status.SUCCESS.equals(s.getStatus())) {
      Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
      NavController navController = Navigation.findNavController(binding.getRoot());
      navController.getPreviousBackStackEntry().getSavedStateHandle()
          .set(DroppingDailyBrowseFragment.SAVED_STATE_LD_REFRESH, true);
      navController.popBackStack();
    } else {
      String message = s.getMessage();
      if (message == null)
        message =
            s.getData() != null ? s.getMessage() : getString(R.string.error_contact_support);
      Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
  }

  private void onFormStateChange(FormState formState) {
    if (formState == null) {
      return;
    }
    if (BuildConfig.DEBUG && BuildConfig.SHOW_EDIT_PAGE_MODE) {
      Toast.makeText(getContext(),
          viewModel.getEditPageMode() + " is " + (formState != null ? formState.getState() : null),
          Toast.LENGTH_SHORT).show();
    }
    switch (formState.getState()) {
    case READY:
      bindProgressAndPageContentReady(AnimationUtil.fadeoutAnimationMedium(),
          AnimationUtil.fadeinAnimationMedium());
      break;
    case ACTION_IN_PROGRESS:
      bindProgressAndPageContentAip(formState, AnimationUtil.fadeinAnimationShort());
      break;
    case LOADING:
      bindProgressAndPageContentLoading(formState, AnimationUtil.fadeoutAnimationMedium(),
          AnimationUtil.fadeinAnimationMedium());
      break;
    case ERROR:
      bindProgressAndPageContentError(formState, AnimationUtil.fadeoutAnimationMedium(),
          AnimationUtil.fadeinAnimationMedium());
    }
  }

  private void bindProgressAndPageContentReady(Animation outAnimation, Animation inAnimation) {
    DroppingDailyData daData = viewModel.getDdData();

    // field outlet selalu diperlukan
    outletsAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    outletsAdapter.addAll(viewModel.getOutlets());
    outletsAdapter.notifyDataSetChanged();
    binding.spOutlet.setAdapter(outletsAdapter);

    OutletShortData selectedOutlet = null;
    if (viewModel.getSelectedOutlet() != null) {
      binding.spOutlet.setSelection(viewModel.getSelectedOutlet());
      KeyValueModel<OutletShortData, String> selectedOsd = (KeyValueModel<OutletShortData, String>) binding.spOutlet.getSelectedItem();
      selectedOutlet = selectedOsd.getKey();
      binding.tvCurrentBalance.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(selectedOutlet.getCurrentBalance()));
      binding.tvCurrentBalanceLimit.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(selectedOutlet.getBalanceLimit()));
    }

    binding.tvRequestDate.setText(Formatter.DTF_dd_MM_yyyy.format(viewModel.getRequestDate()));
    binding.etRequestDate.setText(Formatter.DTF_dd_MM_yyyy.format(viewModel.getRequestDate()));
    binding.etDroppingDate.setText(Formatter.DTF_dd_MM_yyyy.format(viewModel.getDroppingDate()));
    if (daData != null) {
      // kalau ada existing daData set
      if (daData.getTransactionNo() != null) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(daData.getTransactionNo());
      }
      // -- header START
      binding.tvPt.setText(daData.getCompanyCode());
      binding.tvOutlet.setText(
          String.format("%s (%s)", daData.getOutletName(), daData.getOutletCode()));
      binding.tvWfStatus.setText(daData.getWfStatus());
      binding.tvTransferStatus.setText(daData.getTransferStatus());
      binding.tvDroppingDate.setText(
          Formatter.DTF_dd_MM_yyyy.format(daData.getDroppingDate()));
      // -- header END
      binding.tvDroppingBalance.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(daData.getBalance()));
      binding.tvDroppingLimit.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(daData.getBalanceLimit()));
      binding.etDifferenceAmount.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(daData.getRequestAmount()));
      if (daData.getDeductionRate() != null) {
        binding.tvDroppingDeductionPoRed.setText(
            String.format("%s (PO Red %s/%s=%s%%)",
                Formatter.DF_AMOUNT_NO_DECIMAL.format(daData.getDeductionRate()),
                daData.getQtyPoRedBooking(), daData.getQtyBooking(),
                daData.getPoRedRate()));
      } else {
        binding.tvDroppingDeductionPoRed.setText(
            String.format("%s (PO Red %s/%s=%s%%)", 0, 0, 0, 0));
      }
      binding.etDeductionAmount.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(daData.getDeductionAmount()));
      binding.etNetAmount.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(daData.getNetRequestAmount()));
      // rounded harus dihitung
      binding.etRoundedAmount.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(daData.getNetRoundedAmount()));
    }
    binding.swAltAccount.setChecked(viewModel.isAltAccount());
    updateAltAccountData();

    switch (viewModel.getEditPageMode()) {
    case CAPTAIN_VERIFICATION:
      // semua field readonly
      // tapi ada action keputusan
      // tidak ada action standar
      binding.spOutlet.setEnabled(false);
      binding.etRequestDate.setEnabled(false);
      binding.tvDroppingDeductionPoRed.setVisibility(View.VISIBLE);
      binding.tilDeductionAmount.setVisibility(View.VISIBLE);
      binding.tilRoundedAmount.setVisibility(View.VISIBLE);
      binding.tilApprovedAmount.setVisibility(View.VISIBLE);
      binding.etApprovedAmount.setEnabled(true);
      binding.etApprovedAmount.requestFocus();
      // bisa input rekening alternatif
      binding.swAltAccount.setEnabled(true);
      binding.swAltAccount.setVisibility(View.VISIBLE);
      if (viewModel.isAltAccount()) {
        binding.btnAltAcctEdit.setVisibility(View.VISIBLE);
      } else {
        binding.btnAltAcctEdit.setVisibility(View.GONE);
      }

      binding.svWorkflowAction.setVisibility(View.VISIBLE);

      id.co.danwinciptaniaga.npmdsandroid.util.Utility.renderCustomWfButton(
          viewModel.getProcTaskData().getPossibleOutcome(), binding.llWorkflowButtonContainer,
          getLayoutInflater(), getActivity(), this::WfValidationProcess, this::handleWFAction,
          false);

      binding.btnWfHistory.setVisibility(View.VISIBLE);
      break;
    case READ_ONLY:
    case FINANCE_STAFF_VERIFICATION:
    case FINANCE_SPV_VERIFICATION:
      // semua field readonly
      // tidak ada action keputusan
      // tidak ada action standar
      binding.spOutlet.setEnabled(false);
      binding.tvCurrentBalanceLabel.setVisibility(View.GONE);
      binding.tvCurrentBalance.setVisibility(View.GONE);
      binding.tvCurrentBalanceLimitLabel.setVisibility(View.GONE);
      binding.tvCurrentBalanceLimit.setVisibility(View.GONE);
      binding.etRequestDate.setEnabled(false);
      binding.tvDroppingDeductionPoRed.setVisibility(View.VISIBLE);
      binding.tilDeductionAmount.setVisibility(View.VISIBLE);
      binding.tilRoundedAmount.setVisibility(View.VISIBLE);
      binding.tilApprovedAmount.setVisibility(View.VISIBLE);
      binding.etApprovedAmount.setEnabled(false);
      // tidak bisa input rekening alternatif
      binding.swAltAccount.setEnabled(false);
      binding.swAltAccount.setVisibility(View.VISIBLE);
      binding.btnAltAcctEdit.setVisibility(View.GONE);
      binding.btnWfHistory.setVisibility(View.VISIBLE);
      switch (viewModel.getEditPageMode()) {
      case FINANCE_STAFF_VERIFICATION:
        binding.svWorkflowAction.setVisibility(View.VISIBLE);

        id.co.danwinciptaniaga.npmdsandroid.util.Utility.renderCustomWfButton(
            viewModel.getProcTaskData().getPossibleOutcome(), binding.llWorkflowButtonContainer,
            getLayoutInflater(), getActivity(), this::WfValidationProcess, this::handleWFAction,
            false);

        prepareTransferInstruction(true);
        binding.btnWfHistory.setVisibility(View.VISIBLE);
        break;
      case FINANCE_SPV_VERIFICATION:
        binding.svWorkflowAction.setVisibility(View.VISIBLE);

        viewModel.getTransferMode().observe(getViewLifecycleOwner(), mode -> {
          boolean isOtp = id.co.danwinciptaniaga.npmdsandroid.util.Utility.AUTOMATIC.equals(mode);

          id.co.danwinciptaniaga.npmdsandroid.util.Utility.renderCustomWfButton(
              viewModel.getProcTaskData().getPossibleOutcome(), binding.llWorkflowButtonContainer,
              getLayoutInflater(), getActivity(), this::WfValidationProcess, this::handleWFAction,
              false);
        });
        prepareTransferInstruction(false);

        binding.btnWfHistory.setVisibility(View.VISIBLE);
        break;
      case READ_ONLY:
        viewModel.getTransferMode().observe(getViewLifecycleOwner(), mode -> {
          if (mode != null) {
            prepareTransferInstruction(false);
          }
        });
        break;
      default:
        break;
      }
      break;
    }

    if (daData != null) {
      if (viewModel.getDdData().getApprovedAmount() != null) {
        // akan null kalau masih Draft
        Formatter.DF_AMOUNT_NO_DECIMAL.format(viewModel.getDdData().getApprovedAmount());
        if (binding.etApprovedAmount.isEnabled()) {
          binding.etApprovedAmount.setText(viewModel.getDdData().getApprovedAmount().toString());
        } else {
          binding.etApprovedAmount
              .setText(
                  Formatter.DF_AMOUNT_NO_DECIMAL.format(viewModel.getDdData().getApprovedAmount()));
        }
      } else {
        binding.etApprovedAmount.setText(null);
      }
    }

    ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE, outAnimation);
    ViewUtil.setVisibility(binding.pageContent, View.VISIBLE, inAnimation);
  }

  private void updateAltAccountData() {
    if (viewModel.getBankAccountData() != null) {
      BankAccountData bad = viewModel.getBankAccountData();
      if (bad.getBankId() == null) {
        binding.tvAltAcctBankName.setText(bad.getOtherBankName());
      } else {
        binding.tvAltAcctBankName.setText(bad.getBankName());
      }
      binding.tvAltAcctName.setText(bad.getAccountName());
      binding.tvAltAcctNo.setText(bad.getAccountNo());
      if (bad.getLastValidated() != null) {
        binding.tvAltAcctValidDate.setText(
            Formatter.DTF_dd_MM_yyyy_HH_mm.format(bad.getLastValidated()));
      } else {
        binding.tvAltAcctValidDate.setText(null);
      }
    }
  }

  private void bindProgressAndPageContentAip(FormState formState, Animation inAnimation) {
    if (formState.getMessage() != null) {
      binding.progressWrapper.progressText.setText(formState.getMessage());
    } else {
      binding.progressWrapper.progressText.setText(getString(R.string.msg_please_wait));
    }
    ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE, inAnimation);
    ViewUtil.setVisibility(binding.progressWrapper.progressView, View.VISIBLE, inAnimation);
    ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
    // tidak mengubah visibilitas pageContent
  }

  private void bindProgressAndPageContentLoading(FormState formState, Animation outAnimation,
      Animation inAnimation) {
    if (formState.getMessage() != null) {
      binding.progressWrapper.progressText.setText(formState.getMessage());
    } else {
      binding.progressWrapper.progressText.setText(getString(R.string.msg_please_wait));
    }
    ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
    ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.VISIBLE);
    ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
    ViewUtil.setVisibility(binding.pageContent, View.GONE);
  }

  private void bindProgressAndPageContentError(FormState formState, Animation outAnimation,
      Animation inAnimation) {
    binding.progressWrapper.progressText.setText(formState.getMessage());
    ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE, inAnimation);
    ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.GONE, outAnimation);
    ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.VISIBLE, inAnimation);
    binding.progressWrapper.retryButton.setOnClickListener(reloadFragment);
    ViewUtil.setVisibility(binding.pageContent, View.GONE, outAnimation);
  }

  /**
   * @param isEnable, meng hidup matikan semua field pada card transferInstruction
   */
  private void prepareTransferInstruction(boolean isEnable) {
    binding.cvTransferInstruction.setVisibility(View.VISIBLE);

    // set mode transfer otomatis/manual dan sourceAccount
    setTransferMode(isEnable);
    setTransferDate(isEnable);
    setSourceAccountField(isEnable);
  }

  private void setDefaultSourceAccountList(boolean isManual) {
    // set source Adapter berdasarkan mode transfer pada awal data di load
    sourceAccAdapter.clear();
    if (isManual) {
      sourceAccAdapter.addAll(viewModel.getKvmBadManualList());
    } else {
      sourceAccAdapter.addAll(viewModel.getKvmBadAutoList());
    }
    sourceAccAdapter.notifyDataSetChanged();
    binding.spAccount.setFocusable(false);
    binding.spAccount.setAdapter(sourceAccAdapter);
  }

  private void setTransferDate(boolean isEnable) {
    binding.tilTransferDate.setEnabled(true);
    defaultTransferDate = LocalDate.now();
    dateString = defaultTransferDate.format(Formatter.DTF_dd_MM_yyyy);
    viewModel.getTransferDate().observe(getViewLifecycleOwner(), date -> {
      if (date != null) {
        defaultTransferDate = date;
        dateString = date.format(Formatter.DTF_dd_MM_yyyy);
      } else {
        viewModel.setTransferDate(defaultTransferDate);
      }

      binding.etTransferDate.setText(dateString);

      if (!isEnable) {
        binding.tilTransferDate.setEnabled(false);
        return;
      } else {
        if (!viewModel.isAllowBackdate()) {
          binding.tilTransferDate.setEnabled(false);
          return;
        }
      }

      binding.etTransferDate.setOnClickListener(v -> {
        int day = defaultTransferDate.getDayOfMonth();
        int month = defaultTransferDate.getMonthValue() - 1;
        int year = defaultTransferDate.getYear();
        DatePickerDialog dpd = new DatePickerDialog(getActivity(),
            new DatePickerDialog.OnDateSetListener() {
              @Override
              public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                LocalDate transferDate = LocalDate.of(year, month + 1, dayOfMonth);
                viewModel.setTransferDate(transferDate);
              }
            }, year, month, day);
        dpd.show();
      });
    });
  }

  private void setTransferMode(boolean isEnable) {
    binding.swTransferMode.setEnabled(isEnable);
    sourceAccAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    viewModel.getTransferMode().observe(getViewLifecycleOwner(), isManualVal -> {
      boolean isManual =id.co.danwinciptaniaga.npmdsandroid.util.Utility.MANUAL.equals(isManualVal);
      boolean isAltAcc = viewModel.getBankAccountData() != null
          && viewModel.getBankAccountData().getOtherBankName() != null;
      boolean isManualFinal = isAltAcc && !isManual?true:isManual;
      boolean currentIsManualStatus = binding.swTransferMode.isChecked();
      if(currentIsManualStatus!=isManualFinal)
        binding.swTransferMode.setChecked(isManualFinal);

      setDefaultSourceAccountList(isManual);
      if(viewModel.getKvmSelectedBad() !=null && sourceAccAdapter.getCount()>0){
        int pos = sourceAccAdapter.getPosition(viewModel.getKvmSelectedBad());
        binding.spAccount.setSelection(pos);
      }else{
        binding.spAccount.setSelection(-1);
        viewModel.setKvmSelectedBad(null);
      }
    });

    binding.swTransferMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isManual) {
        String transferModeId = null;
        boolean isAltAcc = viewModel.getBankAccountData() != null
            && viewModel.getBankAccountData().getOtherBankName() != null;
        if (isAltAcc && !isManual) {
          Toast.makeText(getContext(), getString(R.string.manual_transfer_recomended),
              Toast.LENGTH_LONG).show();

          HyperLog.d(TAG, "isAltAcc[" + isAltAcc + "] isManual[" + isManual + "]");
          setDefaultSourceAccountList(true);
          viewModel.setTransferMode(id.co.danwinciptaniaga.npmdsandroid.util.Utility.MANUAL);
          binding.swTransferMode.setChecked(true);
        }else{
          setDefaultSourceAccountList(isManual);
          if (isManual) {
            transferModeId = id.co.danwinciptaniaga.npmdsandroid.util.Utility.MANUAL;
          } else {
            transferModeId = id.co.danwinciptaniaga.npmdsandroid.util.Utility.AUTOMATIC;
          }
        }

        if (viewModel.getKvmSelectedBad() != null && sourceAccAdapter.getCount() > 0) {
          if (sourceAccAdapter.getPosition(viewModel.getKvmSelectedBad()) == -1) {
            binding.spAccount.setSelection(-1);
            viewModel.setSelectedAccountId(null);
            viewModel.setKvmSelectedBad(null);
          }
        }
        viewModel.setTransferMode(transferModeId);
      }
    });
  }

  private void setSourceAccountField(boolean isEnable) {
    binding.tilAccount.setEnabled(isEnable);
    binding.spAccount.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
          int i, long l) {
        KeyValueModel<UUID, String> selectedObj = null;
        selectedObj = sourceAccAdapter.getItem(i);
        viewModel.setSelectedAccountId(selectedObj.getKey());
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
        viewModel.setKvmSelectedBad(null);
        viewModel.setSelectedAccountId(null);
      }
    });
  }

  private Boolean WfValidationProcess(WfProcessParameter paramObj) {
    boolean isOk = true;
    String decision = paramObj.getDecision();
    DroppingDailyScreenMode mode = viewModel.getEditPageMode();
    switch (mode) {
    case CAPTAIN_VERIFICATION:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)) {
        if (!validateFormCaptainApprove(binding.etApprovedAmount.getText())) {
          Toast.makeText(getContext(), getString(R.string.form_validation_error),
              Toast.LENGTH_SHORT).show();
          isOk = false;
        }
      }
      break;
    case FINANCE_STAFF_VERIFICATION:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)) {
        if (!viewModel.isOkFinStaffApprove()) {
          Toast.makeText(getContext(), getString(R.string.msg_error_dropping_harian_approval_finst),
              Toast.LENGTH_LONG).show();
          isOk = false;
        }else {
          boolean isAuto = Objects.equals(Utility.AUTOMATIC,
              viewModel.getTransferMode().getValue());
          LocalDate currentDate = LocalDate.now();
          LocalDate trxDate = viewModel.getTransferDate().getValue();
          if (!Objects.equals(currentDate, trxDate) && isAuto) {
            String msg = getContext().getString(R.string.msg_not_supported_backdate_automatic_trx);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            return false;
          }
        }
      }
      break;
    case FINANCE_SPV_VERIFICATION:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)) {
        // tidak ada validasi
      }
      break;
    default:
      break;
    }
    return isOk;
  }

  private void handleWFAction(WfProcessParameter paramObj){
    String decision = paramObj.getDecision();
    String comment = paramObj.getComment();
    String otpCode = paramObj.getOtp();
    boolean isNotSupportedWF = false;
    DroppingDailyScreenMode mode = viewModel.getEditPageMode();
    switch (mode){
    case CAPTAIN_VERIFICATION:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)) {
        viewModel.captainApprove(comment);
      } else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision)
          || WorkflowConstants.WF_OUTCOME_REJECT.equals(decision)) {
        viewModel.rejectOrReturn(decision, comment);
      } else {
        isNotSupportedWF = true;
      }
      break;
    case FINANCE_STAFF_VERIFICATION:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)) {
          viewModel.finStaffApprove(comment);
        break;
      } else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision)
          || WorkflowConstants.WF_OUTCOME_REJECT.equals(decision)) {
        viewModel.rejectOrReturn(decision, comment);
      } else {
        isNotSupportedWF = true;
      }
      break;
    case FINANCE_SPV_VERIFICATION:
      if (WorkflowConstants.WF_OUTCOME_REQ_OTP.equals(decision)) {
          viewModel.processGenerateOTP();
        Toast.makeText(getContext(), getString(R.string.msg_request_otp), Toast.LENGTH_LONG).show();
      }
      else if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)) {
        viewModel.finSpvApprove(comment, otpCode);
      } else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision)
          || WorkflowConstants.WF_OUTCOME_REJECT.equals(decision)) {
        viewModel.rejectOrReturn(decision, comment);
      } else {
        isNotSupportedWF = true;
      }
      break;
    default:
      isNotSupportedWF = true;
      break;
    }

    if (isNotSupportedWF) {
      String scMode = String.format("Screen Mode[%s] ", mode);
      String wf = String.format("wfOutCome[%s] ", decision);
      String msg = scMode + wf + " Not Supported";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      msg = TAG + " handleWfAction_Txn() " + msg;
      RuntimeException re = new RuntimeException(msg);
      HyperLog.exception(TAG, re);
      ACRA.getErrorReporter().handleException(re);
    }
  }

  private boolean validateFormCaptainApprove(Editable approvedAmount) {
    return validateApprovedAmount(approvedAmount);
  }

  private boolean validateApprovedAmount(Editable s) {
    boolean result = false;
    try {
      Long approvedAmount = s.length() > 0 ?
          id.co.danwinciptaniaga.npmdsandroid.util.Utility.getLongAmtFromFromattedString(
              s.toString()) : null;
      DroppingDailyValidationHelper.validateApprovedAmount(getContext(),
          viewModel.getDdData().getNetRoundedAmount(), approvedAmount);
      binding.tilApprovedAmount.setError(null);
      result = true;
    } catch (ConstraintViolation constraintViolation) {
      binding.tilApprovedAmount.setError(constraintViolation.getMessage());
      result = false;
    }
    return result;
  }
}