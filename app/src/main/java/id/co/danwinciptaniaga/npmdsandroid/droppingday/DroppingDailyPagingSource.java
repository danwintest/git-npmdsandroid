package id.co.danwinciptaniaga.npmdsandroid.droppingday;

import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.npmds.data.ListResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyData;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListPagingSource;

public class DroppingDailyPagingSource extends
    ListenableFuturePagingSource<Integer, DroppingDailyData> {
  private static final String TAG = ExpenseListPagingSource.class.getSimpleName();
  private final GetDroppingDailyListUC ucGetDroppingDailyList;
  private final Executor mBgExecutor;

  public DroppingDailyPagingSource(
      GetDroppingDailyListUC getDroppingDailyListUC, Executor mBgExecutor) {
    this.ucGetDroppingDailyList = getDroppingDailyListUC;
    this.mBgExecutor = mBgExecutor;
  }

  @NotNull
  @Override
  public ListenableFuture<LoadResult<Integer, DroppingDailyData>> loadFuture(
      @NotNull LoadParams<Integer> loadParams) {
    // Start refresh at page 1 if undefined.
    Integer nextPageNumber = loadParams.getKey();
    if (nextPageNumber == null) {
      nextPageNumber = 1;
    }
    // TODO: cek apakah perlu fix?
    // kalau pakai LoadSize, pertama kali (nextPageNumber=1) = 3 * PageSize
    // tapi ini mengakibatkan pada nextPageNumber=2, data yang terbaca adalah data yg sudah didapatkan pada pertama kali
    // sehingga data menjadi duplikat
    int pageSize = loadParams.getPageSize();

    ListenableFuture<LoadResult<Integer, DroppingDailyData>> pageFuture = Futures.transform(
        ucGetDroppingDailyList.getDroppingDailyList(nextPageNumber, pageSize),
        this::toLoadResult, mBgExecutor);
    return Futures.catching(
        pageFuture, Exception.class,
        input -> {
          HyperLog.e(TAG, "Error while getting DroppingAdditionalList", input);
          ErrorResponse er = RetrofitUtility.parseErrorBody(input);
          return new LoadResult.Error(
              er != null ? new Exception(er.getFormattedMessage(), input) : input);
        }, mBgExecutor);
  }

  private LoadResult<Integer, DroppingDailyData> toLoadResult(
      @NonNull ListResponse<DroppingDailyData> response) {
    return new LoadResult.Page<>(
        response.getDataList(),
        null, // Only paging forward.
        response.getNextPageNumber(),
        LoadResult.Page.COUNT_UNDEFINED,
        LoadResult.Page.COUNT_UNDEFINED);
  }
}
