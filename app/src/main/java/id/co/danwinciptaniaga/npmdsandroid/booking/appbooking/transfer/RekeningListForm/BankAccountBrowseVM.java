package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.LoadState;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class BankAccountBrowseVM  extends AndroidViewModel {

  private final static String TAG = BankAccountBrowseVM.class.getSimpleName();
  private BankAccountBrowseUC loadUC;
  private MutableLiveData<Boolean> refreshList = new SingleLiveEvent<>();
  private MutableLiveData<LoadState> loadState = new MutableLiveData<>();
  private MutableLiveData<Resource<String>> actionEvent = new SingleLiveEvent<>();
  private MutableLiveData<Boolean> refresh = new SingleLiveEvent<>();


  private LiveData<PagingData<BankAccountData>> badList;

  @ViewModelInject
  public BankAccountBrowseVM(@NonNull Application app, AppExecutors appExecutors, BankAccountBrowseUC loadUC){
    super(app);
    HyperLog.d(TAG, "construc BankAccountBrowseVM terpanggil");
    this.loadUC = loadUC;
    CoroutineScope vmScope = ViewModelKt.getViewModelScope(this);
    Pager<Integer, BankAccountData> pager = new Pager<Integer, BankAccountData>(new PagingConfig(
        Utility.PAGE_SIZE),
        () -> new BankAccountListPagingSource(loadUC, appExecutors.networkIO()));

    badList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pager), vmScope);
  }

  public MutableLiveData<Boolean> getRefreshList() {
    return refreshList;
  }

  public void setTxnMode_OutletId(String txnMode,UUID outletId) {
    loadUC.setTxnMode(txnMode);
    loadUC.setOutletId(outletId);
  }

  public void setFilter(String filter) {
    loadUC.setFilter(filter);
    refreshList.postValue(true);
  }

  public void setLoadState(LoadState loadState) {
    this.loadState.postValue(loadState);
  }

  public MutableLiveData<LoadState> getLoadState() {
    return loadState;
  }

  public void setRefresh(Boolean refresh) {
    this.refresh.postValue(refresh);
  }

  public MutableLiveData<Resource<String>> getActionEvent() {
    return actionEvent;
  }

  public LiveData<PagingData<BankAccountData>> getBankAccountDataList() {
    return badList;
  }
}
