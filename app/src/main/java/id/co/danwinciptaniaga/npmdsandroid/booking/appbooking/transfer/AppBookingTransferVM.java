package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBankAccountData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmds.data.booking.NewBookingReponse;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.BatchData;
import id.co.danwinciptaniaga.npmds.data.common.LobShortData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.common.SoShortData;
import id.co.danwinciptaniaga.npmds.data.common.UtilityState;
import id.co.danwinciptaniaga.npmds.data.wf.ProcTaskData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingVM;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFormState;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingLoadUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingPrepareUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingProcessUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class AppBookingTransferVM extends Abstract_AppBookingVM implements Serializable {
  private String TAG = AppBookingTransferVM.class.getSimpleName();
  public static final String REAL_TIME_CODE = "R";
  public static final String REAL_TIME_VALUE = "RealTime";

  public static final String TXN_CONSUMER = "C";
  public static final String TXN_FIF = "F";
  public static final String TXN_BIRO_JASA = "B";
  public static final String FROM_TXN_PROCESS = "FROM_TXN_PROCESS";

  private String txnMode = null;
  private UUID txnConsId = null;
  private UUID txnBjId = null;
  private UUID txnFifId = null;
  private Date txnConsUpdateTs = null;
  private Date txnBjUpdateTs = null;
  private Date txnFifUpdateTs = null;
  private MutableLiveData<List<BatchData>> batch_Ld = new MutableLiveData<>();

  private List<KeyValueModel<AppBookingBankAccountData,String>> txnBankAccountDataList = new ArrayList<>();

  private MutableLiveData<KeyValueModel<BatchData, String>> field_Batch = new MutableLiveData<>();
  private MutableLiveData<UUID> field_ConsumerBankId = new MutableLiveData<>();
  private String field_ConsumerBankName = null;
  private MutableLiveData<String> field_ConsumerOtherBank = new MutableLiveData<>();
  private MutableLiveData<String> field_ConsumerBank_LABEL = new MutableLiveData<>();
  private MutableLiveData<String> field_ConsumerBank_LABEL_ERROR = new MutableLiveData<>();
  private Boolean field_ConsumerBank_LABEL_OK = false;
  private MutableLiveData<String> field_ConsumerAccountNo = new MutableLiveData<>();
  private MutableLiveData<String> field_ConsumerAccountNo_ERROR = new MutableLiveData<>();
  private Boolean field_ConsumerAccountNo_OK = false;
  private MutableLiveData<String> field_ConsumerAccountName = new MutableLiveData<>();
  private MutableLiveData<String> field_ConsumerAccountName_ERROR = new MutableLiveData<>();
  private Boolean field_ConsumerAccountName_OK = false;
  private Boolean field_Cons_FINAL_OK = false;
  private LocalDate field_Wf_Txn_DateCons = null;
  private Boolean field_Wf_Txn_IsManualCons = false;
  private KeyValueModel<AppBookingBankAccountData, String> field_Wf_Txn_BankIdCons = new KeyValueModel<>(
      null, null);

  private Boolean isFromBookingStatus = false;

  private MutableLiveData<String> field_BiroJasaAmount_ERROR = new MutableLiveData<>(null);
  private MutableLiveData<UUID> field_BiroJasaBankId = new MutableLiveData<>();
  private String field_BiroJasaBankName = null;
  private MutableLiveData<String> field_BiroJasaBank_LABEL = new MutableLiveData<>();
  private MutableLiveData<String> field_BiroJasaOtherBank = new MutableLiveData<>();
  private MutableLiveData<String> field_BiroJasaBank_LABEL_ERROR = new MutableLiveData<>();
  private Boolean field_BiroJasaBank_LABEL_OK = false;
  private MutableLiveData<String> field_BiroJasaAccountNo = new MutableLiveData<>();
  private MutableLiveData<String> field_BiroJasaAccountNo_ERROR = new MutableLiveData<>();
  private Boolean field_BiroJasaAccountNo_OK = false;
  private MutableLiveData<String> field_BiroJasaAccountName = new MutableLiveData<>();
  private MutableLiveData<String> field_BiroJasaAccountName_ERROR = new MutableLiveData<>();
  private Boolean field_BiroJasaAccountName_OK = false;
  private Boolean field_BiroJasaAmount_OK = true;
  private Boolean field_BiroJasa_FINAL_OK = false;
  private LocalDate field_Wf_Txn_DateBj = null;
  private Boolean field_Wf_Txn_IsManualBj = false;
  private KeyValueModel<AppBookingBankAccountData, String> field_Wf_Txn_BankIdBj = new KeyValueModel<>(
      null, null);

  private MutableLiveData<String> field_FIFAmount_ERROR = new MutableLiveData<>(null);
  private MutableLiveData<UUID> field_FIFBankId = new MutableLiveData<>();
  private String field_FIFBankName = null;
  private MutableLiveData<String> field_FIFOtherBank = new MutableLiveData<>();
  private MutableLiveData<String> field_FIFBank_LABEL = new MutableLiveData<>();
  private MutableLiveData<String> field_FIFBank_LABEL_ERROR = new MutableLiveData<>();
  private Boolean field_FIFBank_LABEL_OK = false;
  private MutableLiveData<String> field_FIFAccountNo = new MutableLiveData<>();
  private MutableLiveData<String> field_FIFAccountNo_ERROR = new MutableLiveData<>();
  private Boolean field_FIFAccountNo_OK = false;
  private MutableLiveData<String> field_FIFAccountName = new MutableLiveData<>();
  private MutableLiveData<String> field_FIFAccountName_ERROR = new MutableLiveData<>();
  private Boolean field_FIFAccountName_OK = false;
  private Boolean field_FIF_FINAL_OK = false;
  private LocalDate field_Wf_Txn_DateFIF = null;
  private Boolean field_Wf_Txn_IsManualFIF = false;
  private KeyValueModel<AppBookingBankAccountData, String> field_Wf_Txn_BankIdFIF = new KeyValueModel<>(
      null, null);

  private UUID field_CompanyId = null;
  private MutableLiveData<ValidateBankIdRes> validateBankAccountProgress = new MutableLiveData<>();

  private boolean isConsumerAccountDialogShow = false;
  private boolean isFIFAccountDialogShow = false;
  private boolean isBiroJasaAccountDialogShow = false;

  private boolean isBankListDialogShow = false;

  private boolean isConsumerWfDialogShow = false;
  private boolean isFIFWfDialogShow = false;
  private boolean isBiroJasaWfDialogShow = false;

  private MutableLiveData<Boolean> field_ConsValid = new MutableLiveData<>(false);
  private MutableLiveData<Boolean> field_BjValid = new MutableLiveData<>(false);
  private MutableLiveData<Boolean> field_FIFValid = new MutableLiveData<>(false);

  private MutableLiveData<String> btn_ConsTakeAction_Text = new MutableLiveData<>();
  private MutableLiveData<String> btn_BjTakeAction_Text = new MutableLiveData<>();
  private MutableLiveData<String> btn_FIFTakeAction_Text = new MutableLiveData<>();

  private MutableLiveData<AppBookingTransferScreenMode> txnScreenMode = new MutableLiveData<>();

  private MutableLiveData<AppBookingTransferScreenMode> txnScreenModeCons = new MutableLiveData<>();
  private ProcTaskData ptCons = null;
  private MutableLiveData<AppBookingTransferScreenMode> txnScreenModeBj = new MutableLiveData<>();
  private ProcTaskData ptBj = null;
  private MutableLiveData<AppBookingTransferScreenMode> txnScreenModeFIF = new MutableLiveData<>();
  private ProcTaskData ptFIF = null;

  public List<KeyValueModel<AppBookingBankAccountData, String>> getTxnBankAccountDataList() {
    return txnBankAccountDataList;
  }

  public void setTxnBankAccountDataList(
      List<KeyValueModel<AppBookingBankAccountData, String>> objList) {
    txnBankAccountDataList = objList;
  }

  public LocalDate getField_Wf_Txn_DateCons() {
    return field_Wf_Txn_DateCons;
  }

  public LocalDate getField_Wf_Txn_DateBj() {
    return field_Wf_Txn_DateBj;
  }

  public void setField_Wf_Txn_DateBj(LocalDate date) {
    this.field_Wf_Txn_DateBj = date;
  }

  public Boolean getField_Wf_Txn_IsManualBj() {
    return field_Wf_Txn_IsManualBj;
  }

  public void setField_Wf_Txn_IsManualBj(Boolean val) {
    this.field_Wf_Txn_IsManualBj = val;
  }

  public KeyValueModel<AppBookingBankAccountData, String> getField_Wf_Txn_BankIdBj() {
    return field_Wf_Txn_BankIdBj;
  }

  public void setField_Wf_Txn_BankIdBj(KeyValueModel<AppBookingBankAccountData, String> kvm) {
    this.field_Wf_Txn_BankIdBj = kvm;
  }

  public LocalDate getField_Wf_Txn_DateFIF() {
    return field_Wf_Txn_DateFIF;
  }

  public void setField_Wf_Txn_DateFIF(LocalDate date) {
    this.field_Wf_Txn_DateFIF = date;
  }

  public Boolean getField_Wf_Txn_IsManualFIF() {
    return field_Wf_Txn_IsManualFIF;
  }

  public void setField_Wf_Txn_IsManualFIF(Boolean val) {
    this.field_Wf_Txn_IsManualFIF = val;
  }

  public KeyValueModel<AppBookingBankAccountData, String> getField_Wf_Txn_BankIdFIF() {
    return field_Wf_Txn_BankIdFIF;
  }

  public void setField_Wf_Txn_BankIdFIF(KeyValueModel<AppBookingBankAccountData, String> kvm) {
    field_Wf_Txn_BankIdFIF = kvm;
  }

  public void setField_Wf_Txn_DateCons(LocalDate date) {
    field_Wf_Txn_DateCons = date;
  }

  public Boolean getField_Wf_Txn_IsManualCons() {
    return field_Wf_Txn_IsManualCons;
  }

  public void setField_Wf_Txn_IsManualCons(Boolean isManual) {
    this.field_Wf_Txn_IsManualCons= isManual;
  }

  public KeyValueModel<AppBookingBankAccountData, String> getField_Wf_Txn_BankIdCons() {
    return field_Wf_Txn_BankIdCons;
  }

  public void setField_Wf_Txn_BankIdCons(KeyValueModel<AppBookingBankAccountData, String> kvm) {
    this.field_Wf_Txn_BankIdCons = kvm;
  }

  public String getTxnMode() {
    return txnMode;
  }

  public void setTxnMode(String txnMode) {
    this.txnMode = txnMode;
  }

  public String getField_ConsumerBankName() {
    return field_ConsumerBankName;
  }

  public void setField_ConsumerBankName(String field_ConsumerBankName) {
    this.field_ConsumerBankName = field_ConsumerBankName;
  }

  public String getField_BiroJasaBankName() {
    return field_BiroJasaBankName;
  }

  public void setField_BiroJasaBankName(String field_BiroJasaBankName) {
    this.field_BiroJasaBankName = field_BiroJasaBankName;
  }

  public String getField_FIFBankName() {
    return field_FIFBankName;
  }

  public void setField_FIFBankName(String field_FIFBankName) {
    this.field_FIFBankName = field_FIFBankName;
  }

  public Boolean getFromBookingStatus() {
    return isFromBookingStatus;
  }

  public void setFromBookingStatus(Boolean fromBookingStatus) {
    isFromBookingStatus = fromBookingStatus;
  }

  public MutableLiveData<ValidateBankIdRes> getValidateBankAccountProgress() {
    return validateBankAccountProgress;
  }

  public void setValidateBankAccountProgress(ValidateBankIdRes data) {
    validateBankAccountProgress.postValue(data);
  }

  public void setField_CompanyId(UUID id) {
    field_CompanyId = id;
  }

  public UUID getField_CompanyId() {
    return field_CompanyId;
  }

  private MutableLiveData<Boolean> enable_PO = new MutableLiveData<>(true);

  public MutableLiveData<Boolean> isEnable_PO() {
    return enable_PO;
  }

  public void setEnable_PO(Boolean data) {
    enable_PO.postValue(data);
  }

  public MutableLiveData<AppBookingTransferScreenMode> getTxnScreenMode() {
    return txnScreenMode;
  }

  public ProcTaskData getPtCons() {
    return ptCons;
  }

  public void setPtCons(ProcTaskData ptCons) {
    this.ptCons = ptCons;
  }

  public ProcTaskData getPtBj() {
    return ptBj;
  }

  public void setPtBj(ProcTaskData ptBj) {
    this.ptBj = ptBj;
  }

  public ProcTaskData getPtFIF() {
    return ptFIF;
  }

  public void setPtFIF(ProcTaskData ptFIF) {
    this.ptFIF = ptFIF;
  }

  public void setTxnScreenMode(AppBookingTransferScreenMode sm) {
    this.txnScreenMode.postValue(sm);
  }

  public MutableLiveData<AppBookingTransferScreenMode> getTxnScreenModeCons() {
    return txnScreenModeCons;
  }

  public void setTxnScreenModeCons(
      AppBookingTransferScreenMode txnScreenModeCons) {
    this.txnScreenModeCons.postValue(txnScreenModeCons);
  }

  public MutableLiveData<AppBookingTransferScreenMode> getTxnScreenModeBj() {
    return txnScreenModeBj;
  }

  public void setTxnScreenModeBj(
      AppBookingTransferScreenMode txnScreenModeBj) {
    this.txnScreenModeBj.postValue(txnScreenModeBj);
  }

  public MutableLiveData<AppBookingTransferScreenMode> getTxnScreenModeFIF() {
    return txnScreenModeFIF;
  }

  public void setTxnScreenModeFIF(
      AppBookingTransferScreenMode txnScreenModeFIF) {
    this.txnScreenModeFIF.postValue(txnScreenModeFIF);
  }

  public MutableLiveData<String> getBtn_ConsTakeAction() {
    return btn_ConsTakeAction_Text;
  }

  public void setBtn_ConsTakeAction_Text(String text) {
    this.btn_ConsTakeAction_Text.postValue(text);
  }

  public MutableLiveData<String> getBtn_BjTakeAction() {
    return btn_BjTakeAction_Text;
  }

  public void setBtn_BjTakeAction_Text(String text) {
    this.btn_BjTakeAction_Text.postValue(text);
  }

  public MutableLiveData<String> getBtn_FIFTakeAction() {
    return btn_FIFTakeAction_Text;
  }

  public void setBtn_FIFTakeAction_Text(String text) {
    this.btn_FIFTakeAction_Text.postValue(text);
  }

  public MutableLiveData<Boolean> isField_ConsValid() {
    return field_ConsValid;
  }

  public void setField_ConsValid(Boolean data) {
    field_ConsValid.postValue(data);
  }

  public MutableLiveData<Boolean> isField_BjValid() {
    return field_BjValid;
  }

  public void setField_BjValid(Boolean data) {
    field_BjValid.postValue(data);
  }

  public MutableLiveData<Boolean> isField_FIFValid() {
    return field_FIFValid;
  }

  public void setField_FIFValid(Boolean data) {
    field_FIFValid.postValue(data);
  }

  public boolean isConsumerAccountDialogShow() {
    return isConsumerAccountDialogShow;
  }

  public void setConsumerAccountDialogShow(boolean consumerAccountDialogShow) {
    isConsumerAccountDialogShow = consumerAccountDialogShow;
  }

  public boolean isFIFAccountDialogShow() {
    return isFIFAccountDialogShow;
  }

  public void setFIFAccountDialogShow(boolean FIFAccountDialogShow) {
    isFIFAccountDialogShow = FIFAccountDialogShow;
  }

  public boolean isBiroJasaAccountDialogShow() {
    return isBiroJasaAccountDialogShow;
  }

  public void setBiroJasaAccountDialogShow(boolean biroJasaAccountDialogShow) {
    isBiroJasaAccountDialogShow = biroJasaAccountDialogShow;
  }

  public boolean isBankListDialogShow() {
    return isBankListDialogShow;
  }

  public void setBankListDialogShow(boolean bankListDialogShow) {
    isBankListDialogShow = bankListDialogShow;
  }

  public boolean isConsumerWfDialogShow() {
    return isConsumerWfDialogShow;
  }

  public void setConsumerWfDialogShow(boolean consumerWfDialogShow) {
    isConsumerWfDialogShow = consumerWfDialogShow;
  }

  public boolean isFIFWfDialogShow() {
    return isFIFWfDialogShow;
  }

  public void setFIFWfDialogShow(boolean FIFWfDialogShow) {
    isFIFWfDialogShow = FIFWfDialogShow;
  }

  public boolean isBiroJasaWfDialogShow() {
    return isBiroJasaWfDialogShow;
  }

  public void setBiroJasaWfDialogShow(boolean biroJasaWfDialogShow) {
    isBiroJasaWfDialogShow = biroJasaWfDialogShow;
  }

  private MutableLiveData<Boolean> field_TF_ConsumerNameWarn = new MutableLiveData<>();

  private MutableLiveData<List<BankAccountData>> listBookingAccount = new MutableLiveData<>();

  //  public MutableLiveData<AppBookingTransferScreenMode> getConsScreenMode() {
  //    return consScreenMode;
  //  }
  //
  //  public void setConsScreenMode(
  //      AppBookingTransferScreenMode screenMode) {
  //    consScreenMode.postValue(screenMode);
  //  }
  //
  //  public MutableLiveData<AppBookingTransferScreenMode> getBjScreenMode() {
  //    return bjScreenMode;
  //  }
  //
  //  public void setBjScreenMode(
  //      AppBookingTransferScreenMode screenMode) {
  //    this.bjScreenMode.postValue(screenMode);
  //  }

  //  public MutableLiveData<AppBookingTransferScreenMode> getFifScreenMode() {
  //    return fifScreenMode;
  //  }
  //
  //  public void setFifScreenMode(AppBookingTransferScreenMode screenMode) {
  //    this.fifScreenMode.postValue(screenMode);
  //  }
  //
  //  public MutableLiveData<String> getTrxButtonText() {
  //    return trxButtonText;
  //  }
  //
  //  public void setTrxButtonText(String text) {
  //    this.trxButtonText.postValue(text);
  //  }

  @ViewModelInject
  public AppBookingTransferVM(@NonNull Application application, AppExecutors appExecutors,
      AppBookingPrepareUC prepareUC, AppBookingLoadUC appBookingLoadUC,
      AppBookingProcessUC processUC, UtilityService utilityService,
      AppBookingService appBookingService) {
    super(application, appExecutors, prepareUC, appBookingLoadUC, processUC, utilityService,
        appBookingService);
  }

  //  public MutableLiveData<List<BankAccountData>> getBankAccount_Ld() {
  //    return bankAccount_Ld;
  //  }

  //  public void setBankAccount_Ld(List<BankAccountData> list) {
  //    bankAccount_Ld.postValue(list);
  //  }

  public MutableLiveData<List<BatchData>> getBatch_Ld() {
    return batch_Ld;
  }

  public MutableLiveData<KeyValueModel<BatchData, String>> getField_Batch() {
    return field_Batch;
  }

  public void setField_Batch(KeyValueModel<BatchData, String> data) {
    if (data == null) {
      field_Batch.postValue(data);
    } else {
      if (data != field_Batch.getValue())
        field_Batch.postValue(data);
    }
  }

  protected void setBatch_Ld(
      List<BatchData> data) {
    batch_Ld.postValue(data);
  }

  public MutableLiveData<UUID> getField_ConsumerBankId() {
    return field_ConsumerBankId;
  }

  public void setField_ConsumerBankId(UUID bankId, String bankName) {
    field_ConsumerBankId.postValue(bankId);
    if (bankId != null) {
      setField_ConsumerBank_LABEL(bankName != null ? bankName : "-");
      return;
    }
  }

  public MutableLiveData<String> getField_ConsumerOtherBank() {
    return field_ConsumerOtherBank;
  }

  public void setField_ConsumerOtherBank(String data) {
    field_ConsumerOtherBank.postValue(data);
    if (data != null)
      setField_ConsumerBank_LABEL(data);
  }

  public MutableLiveData<String> getField_ConsumerAccountNo() {
    return field_ConsumerAccountNo;
  }

  public void setField_ConsumerAccountNo(String data) {
    field_ConsumerAccountNo.postValue(data);
  }

  public MutableLiveData<String> getField_ConsumerAccountName() {
    return field_ConsumerAccountName;
  }

  protected void setField_ConsumerAccountName(String data, boolean isFromLoad) {
    field_ConsumerAccountName.postValue(data);
    if (!isFromLoad)
      setField_TF_ConsumerNameWarn(data, getField_ConsumerName().getValue());
  }

  public MutableLiveData<UUID> getField_BiroJasaBankId() {
    return field_BiroJasaBankId;
  }

  public void setField_BiroJasaBankId(UUID bankId, String bankName) {
    field_BiroJasaBankId.postValue(bankId);
    if (bankId != null) {
      setField_BiroJasaBank_LABEL(bankName != null ? bankName : "-");
      return;
    }
  }

  public MutableLiveData<String> getField_BiroJasaOtherBank() {
    return field_BiroJasaOtherBank;
  }

  public void setField_BiroJasaOtherBank(String data) {
    field_BiroJasaOtherBank.postValue(data);
    if (data != null)
      setField_BiroJasaBank_LABEL(data);
  }

  public MutableLiveData<String> getField_BiroJasaAccountNo() {
    return field_BiroJasaAccountNo;
  }

  public void setField_BiroJasaAccountNo(String data) {
    field_BiroJasaAccountNo.postValue(data);
  }

  public MutableLiveData<String> getField_BiroJasaAccountName() {
    return field_BiroJasaAccountName;
  }

  public void setField_BiroJasaAccountName(String data) {
    field_BiroJasaAccountName.postValue(data);
  }

  public MutableLiveData<UUID> getField_FIFBankId() {
    return field_FIFBankId;
  }

  public void setField_FIFBankId(UUID bankId, String bankName) {
    field_FIFBankId.postValue(bankId);
    if (bankId != null) {
      setField_FIFBank_LABEL(bankName != null ? bankName : "-");
      return;
    }
  }

  @Override
  public void setField_outletKvm(KeyValueModel<OutletShortData, String> outletKvm) {
    if (outletKvm == null)
      super.setField_outletKvm(outletKvm);
    else {
      if (outletKvm != getField_outletKvm().getValue()) {
        setField_CompanyId(outletKvm.getKey().getCompanyId());
        super.setField_outletKvm(outletKvm);
      }
    }

  }

  public MutableLiveData<String> getField_FIFOtherBank() {
    return field_FIFOtherBank;
  }

  public void setField_FIFOtherBank(String data) {
    field_FIFOtherBank.postValue(data);
    if (data != null)
      setField_FIFBank_LABEL(data);
  }

  public MutableLiveData<String> getField_FIFAccountNo() {
    return field_FIFAccountNo;
  }

  public void setField_FIFAccountNo(String data) {
    field_FIFAccountNo.postValue(data);
  }

  public MutableLiveData<String> getField_FIFAccountName() {
    return field_FIFAccountName;
  }

  public void setField_FIFAccountName(String data) {
    field_FIFAccountName.postValue(data);
  }

  public MutableLiveData<String> getField_ConsumerBank_LABEL() {
    return field_ConsumerBank_LABEL;
  }

  public void setField_ConsumerBank_LABEL(String data) {
    field_ConsumerBank_LABEL.postValue(data);
  }

  public MutableLiveData<String> getField_BiroJasaBank_LABEL() {
    return field_BiroJasaBank_LABEL;
  }

  public void setField_BiroJasaBank_LABEL(String data) {
    field_BiroJasaBank_LABEL.postValue(data);
  }

  public MutableLiveData<String> getField_FIFBank_LABEL() {
    return field_FIFBank_LABEL;
  }

  public void setField_FIFBank_LABEL(String data) {
    field_FIFBank_LABEL.postValue(data);
  }

  public MutableLiveData<String> getField_ConsumerBank_LABEL_ERROR() {
    return field_ConsumerBank_LABEL_ERROR;
  }

  public MutableLiveData<String> getField_ConsumerAccountNo_ERROR() {
    return field_ConsumerAccountNo_ERROR;
  }

  protected MutableLiveData<String> getField_ConsumerAccountName_ERROR() {
    return field_ConsumerAccountName_ERROR;
  }

  public MutableLiveData<String> getField_FIFBank_LABEL_ERROR() {
    return field_FIFBank_LABEL_ERROR;
  }

  public MutableLiveData<String> getField_FIFAccountNo_ERROR() {
    return field_FIFAccountNo_ERROR;
  }

  public MutableLiveData<String> getField_FIFAccountName_ERROR() {
    return field_FIFAccountName_ERROR;
  }

  public MutableLiveData<String> getField_BiroJasaBank_LABEL_ERROR() {
    return field_BiroJasaBank_LABEL_ERROR;
  }

  public MutableLiveData<String> getField_BiroJasaAccountNo_ERROR() {
    return field_BiroJasaAccountNo_ERROR;
  }

  public MutableLiveData<String> getField_BiroJasaAccountName_ERROR() {
    return field_BiroJasaAccountName_ERROR;
  }

  public MutableLiveData<String> getField_BiroJasaAmount_ERROR() {
    return field_BiroJasaAmount_ERROR;
  }

  public MutableLiveData<String> getField_FIFAmount_ERROR() {
    return field_FIFAmount_ERROR;
  }

  public MutableLiveData<Boolean> getField_TF_ConsumerNameWarn() {
    return field_TF_ConsumerNameWarn;
  }

  public void setField_TF_ConsumerNameWarn(String consAccName, String consName) {
    consName = consName != null ? consName.toLowerCase() : "-";
    consAccName = consAccName != null ? consAccName.toLowerCase() : "-";
    if (consName.equals(consAccName))
      field_TF_ConsumerNameWarn.postValue(false);
    else
      field_TF_ConsumerNameWarn.postValue(true);
  }

  public void setField_ConsumerName(String consumerName, boolean isFromLoad) {
    super.setField_ConsumerName(consumerName);
    //    if(!consumerName.equals(getField_ConsumerAccountName().getValue()))
    //      field_TF_ConsumerNameWarn.postValue(true);
    //    else
    //      field_TF_ConsumerNameWarn.postValue(false);
    if (!isFromLoad)
      setField_TF_ConsumerNameWarn(getField_ConsumerAccountName().getValue(), consumerName);
  }

  @Override
  public void setField_IsOpenClose(Boolean isOpenClose) {
    super.setField_IsOpenClose(isOpenClose);
    if (!isOpenClose) {
      if (field_FIFBankId != null)
        field_FIFBankId.postValue(null);
      if (field_FIFOtherBank != null)
        field_FIFOtherBank.postValue(null);

      if (field_FIFBank_LABEL != null)
        field_FIFBank_LABEL.postValue(null);

      if (field_FIFAccountName != null)
        field_FIFAccountName.postValue(null);
      if (field_FIFAccountNo != null)
        field_FIFAccountNo.postValue(null);

      if (field_FIFAccountName_ERROR != null)
        field_FIFAccountName_ERROR.postValue(null);

      field_FIFAccountName_OK = true;

      if (field_FIFAccountNo_ERROR != null)
        field_FIFAccountNo_ERROR.postValue(null);

      field_FIFAccountNo_OK = true;

      if (field_FIFBank_LABEL_ERROR != null)
        field_FIFBank_LABEL_ERROR.postValue(null);
    }

  }

  public void validateBankAccount(String txnType, UUID bankId, String accNo, String accName) {
    String logMsg = "VALIDATE validateBankAccount: bankId[" + bankId + "] companyId["
        + getField_CompanyId() + "] txnType[" + txnType + "] accNo[" + accNo + "] accName["
        + accName + "]";

    ValidateBankIdRes res = new ValidateBankIdRes(true, false, null, null);
    setValidateBankAccountProgress(res);

    // process valid
    ListenableFuture<Boolean> isValid_Lf = appBookingService.validateAppBookingBankAccount(
        getField_AppBookingId(), getField_BookingId(), txnType, bankId, getFromBookingStatus(),
        getField_CompanyId(), accNo, accName);
    HyperLog.d(TAG, logMsg);

    Futures.addCallback(isValid_Lf, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean result) {
        String message = " - ";
        if (result)
          message = getApplication().getString(R.string.validation_bankaccount_valid_message);
        else
          message = getApplication().getString(R.string.validation_bankaccount_invalid_message);

        ValidateBankIdRes res = new ValidateBankIdRes(false, result, message, null);
        setValidateBankAccountProgress(res);
        setValidityTxnBankAccountByTxnType(txnType, result);
        HyperLog.d(TAG, logMsg + " " + message);
      }

      @Override
      public void onFailure(Throwable t) {
        String errorMessage = Utility.getErrorMessageFromThrowable(TAG, t);
        String message = getApplication().getString(
            R.string.validation_bankaccount_invalid_message);
        ValidateBankIdRes res = new ValidateBankIdRes(false, false, message, errorMessage);
        //        ValidateBankIdRes res = new ValidateBankIdRes(false, true, message, errorMessage);
        setValidateBankAccountProgress(res);
        setValidityTxnBankAccountByTxnType(txnType, false);
      }
    }, appExecutors.networkIO());
  }

  private void setValidityTxnBankAccountByTxnType(String txnType, boolean isValid) {
    if (txnType.equals(TXN_CONSUMER))
      setField_ConsValid(isValid);
    else if (txnType.equals(TXN_FIF))
      setField_FIFValid(isValid);
    else if (txnType.equals(TXN_BIRO_JASA))
      setField_BjValid(isValid);
    else
      HyperLog.d(TAG, "setValidityTxnBankAccountByTxnType, mode tidak diketahui mode->" + txnType);
  }

  protected void validateConsRek() {
    if (getField_ConsumerBankId().getValue() == null
        && getField_ConsumerAccountNo().getValue() == null) {
      field_ConsumerBank_LABEL_ERROR.postValue(
          getApplication().getString(R.string.validation_mandatory));
      field_ConsumerBank_LABEL_OK = false;
    } else {
      field_ConsumerBank_LABEL_ERROR.postValue(null);
      field_ConsumerBank_LABEL_OK = true;
    }

    if (getField_ConsumerAccountName().getValue() == null) {
      field_ConsumerAccountName_ERROR.postValue(
          getApplication().getString(R.string.validation_mandatory));
      field_ConsumerAccountName_OK = false;
    } else {
      field_ConsumerAccountName_ERROR.postValue(null);
      field_ConsumerAccountName_OK = true;
    }

    if (getField_ConsumerAccountNo().getValue() == null) {
      field_ConsumerAccountNo_ERROR.postValue(
          getApplication().getString(R.string.validation_mandatory));
      field_ConsumerAccountNo_OK = false;
    } else {
      field_ConsumerAccountNo_ERROR.postValue(null);
      field_ConsumerAccountNo_OK = true;
    }

    if (field_ConsumerAccountNo_OK && field_ConsumerAccountName_OK && field_ConsumerBank_LABEL_OK
        && field_ConsumerAmount_OK)
      field_Cons_FINAL_OK = true;
      //    else if(!field_ConsumerAccountNo_OK && !field_ConsumerAccountName_OK && !field_ConsumerBank_LABEL_OK && !field_ConsumerAmount_OK)
      //      field_Cons_FINAL_OK = true;
    else
      field_Cons_FINAL_OK = false;

    if (field_Cons_FINAL_OK) {
      field_ConsumerAmount_ERROR.postValue(null);
      field_ConsumerBank_LABEL_ERROR.postValue(null);
      field_ConsumerAccountName_ERROR.postValue(null);
      field_ConsumerAccountNo_ERROR.postValue(null);
    }
  }

  protected void validateFIFRek() {
    if (isField_isOpenClose().getValue() == true) {
      if (getField_FIFAmount().getValue() != null
          && getField_FIFAmount().getValue().compareTo(BigDecimal.ZERO) == 1) {
        field_FIFAmount_OK = true;
      } else {
        setField_FIFAmount(null);
        //        field_FIFAmount_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
        field_FIFAmount_OK = false;
      }

      if (getField_FIFBankId().getValue() == null && getField_FIFOtherBank().getValue() == null) {
        field_FIFBank_LABEL_ERROR.postValue(
            getApplication().getString(R.string.validation_mandatory));
        field_FIFBank_LABEL_OK = false;
      } else {
        field_FIFBank_LABEL_ERROR.postValue(null);
        field_FIFBank_LABEL_OK = true;
      }

      if (getField_FIFAccountName().getValue() == null) {
        field_FIFAccountName_ERROR.postValue(
            getApplication().getString(R.string.validation_mandatory));
        field_FIFAccountName_OK = false;
      } else {
        field_FIFAccountName_ERROR.postValue(null);
        field_FIFAccountName_OK = true;
      }

      if (getField_FIFAccountNo().getValue() == null) {
        field_FIFAccountNo_ERROR.postValue(
            getApplication().getString(R.string.validation_mandatory));
        field_FIFAccountNo_OK = false;
      } else {
        field_FIFAccountNo_ERROR.postValue(null);
        field_FIFAccountNo_OK = true;
      }
      if (field_FIFAccountNo_OK && field_FIFAccountName_OK && field_FIFBank_LABEL_OK
          && field_FIFAmount_OK) {
        field_FIF_FINAL_OK = true;
      } else
        field_FIF_FINAL_OK = false;

    } else {
      field_FIFAmount_ERROR.postValue(null);
      field_FIFBank_LABEL_ERROR.postValue(null);
      field_FIFAccountName_ERROR.postValue(null);
      field_FIFAccountNo_ERROR.postValue(null);
      field_FIF_FINAL_OK = true;
    }
  }

  protected void validateBjRek() {
    if (getField_BiroJasaAmount().getValue() != null
        && getField_BiroJasaAmount().getValue().compareTo(BigDecimal.ZERO) == 1) {
      field_BiroJasaAmount_ERROR.postValue(null);
      field_BiroJasaAmount_OK = true;
    } else {
      field_BiroJasaAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
      field_BiroJasaAmount_OK = false;
    }

    if (getField_BiroJasaBankId().getValue() == null
        && getField_BiroJasaOtherBank().getValue() == null) {
      field_BiroJasaBank_LABEL_ERROR.postValue(
          getApplication().getString(R.string.validation_mandatory));
      field_BiroJasaBank_LABEL_OK = false;
    } else {
      field_BiroJasaBank_LABEL_ERROR.postValue(null);
      field_BiroJasaBank_LABEL_OK = true;
    }

    if (getField_BiroJasaAccountName().getValue() == null) {
      field_BiroJasaAccountName_ERROR.postValue(
          getApplication().getString(R.string.validation_mandatory));
      field_BiroJasaAccountName_OK = false;
    } else {
      field_BiroJasaAccountName_ERROR.postValue(null);
      field_BiroJasaAccountName_OK = true;
    }

    if (getField_BiroJasaAccountNo().getValue() == null) {
      field_BiroJasaAccountNo_ERROR.postValue(
          getApplication().getString(R.string.validation_mandatory));
      field_BiroJasaAccountNo_OK = false;
    } else {
      field_BiroJasaAccountNo_ERROR.postValue(null);
      field_BiroJasaAccountNo_OK = true;
    }

    if (field_BiroJasaAccountNo_OK && field_BiroJasaAccountName_OK && field_BiroJasaBank_LABEL_OK
        && field_BiroJasaAmount_OK) {
      field_BiroJasa_FINAL_OK = true;
    } else if (!field_BiroJasaAccountNo_OK && !field_BiroJasaAccountName_OK
        && !field_BiroJasaBank_LABEL_OK && !field_BiroJasaAmount_OK) {
      field_BiroJasa_FINAL_OK = true;
    } else
      field_BiroJasa_FINAL_OK = false;

    if (field_BiroJasa_FINAL_OK) {
      field_BiroJasaAmount_ERROR.postValue(null);
      field_BiroJasaBank_LABEL_ERROR.postValue(null);
      field_BiroJasaAccountName_ERROR.postValue(null);
      field_BiroJasaAccountNo_ERROR.postValue(null);
    }
  }

  public boolean validateProcessTxn_FINSTAFF_CUS() {
    if (getField_Wf_Txn_DateCons() != null && getField_Wf_Txn_BankIdCons() != null
        && getField_Wf_Txn_IsManualCons() != null)
      return true;
    else
      return false;
  }

  public boolean validateProcessTxn_FINSTAFF_BJ() {
    if (getField_Wf_Txn_DateBj() != null && getField_Wf_Txn_BankIdBj() != null
        && getField_Wf_Txn_IsManualBj() != null)
      return true;
    else
      return false;
  }

  public boolean validateProcessTxn_FINSTAFF_FIF() {
    if (getField_Wf_Txn_DateFIF() != null && getField_Wf_Txn_BankIdFIF() != null
        && getField_Wf_Txn_IsManualFIF() != null)
      return true;
    else
      return false;
  }

  public boolean validateProcessTxn_KASIR_CUS() {
    validateConsRek();

    if (!field_outletKvm_OK)
      field_outletKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_lobKvm_OK)
      field_LobKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_SoKvm_OK)
      field_SoKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_Idnpksf_OK)
      field_Idnpksf_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_AlasanPinjam_OK)
      field_AlasanPinjam_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_poprList_OK)
      field_poprList_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerName_OK)
      field_ConsumerName_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerAddress_OK)
      field_ConsumerAddress_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerPhone_OK)
      field_ConsumerPhone_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_VehicleNo_OK)
      field_VehicleNo_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerAmount_OK)
      field_ConsumerAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    if (!field_BookingAmount_OK)
      field_BookingAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    if (!field_FeeMatrix_OK)
      field_FeeMatrix_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));

    if (field_Attach_Final_KK == null)
      field_Attach_Final_KK_OK.postValue(false);
    if (field_Attach_Final_STNK == null)
      field_Attach_Final_STNK_OK.postValue(false);
    if (field_Attach_Final_BPKB == null)
      field_Attach_Final_BPKB_OK.postValue(false);
    if (field_Attach_Final_SPT == null)
      field_Attach_Final_SPT_OK.postValue(false);

    boolean mandatori_field =
        field_outletKvm_OK && field_lobKvm_OK && field_SoKvm_OK && field_Idnpksf_OK
            && field_AlasanPinjam_OK && field_PoNo_OK && field_PoDate_OK
            && field_Attach_FINAL_PO_OK.getValue() && field_poprList_OK && field_ConsumerName_OK
            && field_ConsumerAddress_OK && field_ConsumerPhone_OK && field_VehicleNo_OK
            && field_Cons_FINAL_OK;
    // cek semua Attachment mandatrory sudah terisi atau belum
    boolean mandatory_attach = field_Attach_Final_KK != null && field_Attach_Final_STNK != null
        && field_Attach_Final_BPKB != null && field_Attach_Final_SPT != null;

    if (getField_PoDate().getValue() != null
        && getField_PoDate().getValue().compareTo(LocalDate.now()) > 0) {
      Toast
          .makeText(getApplication(),
              getApplication().getString(R.string.podate_validation).toString(), Toast.LENGTH_LONG)
          .show();
      return false;
    }
    // jika semua field dan attachment mandatory sudah terisi, maka tombol action dapat digunakan
    if (mandatori_field && mandatory_attach) {
      // this.canProcess.postValue(true);
      return true;
    } else {
      // this.canProcess.postValue(false);
      Toast
          .makeText(getApplication(),
              getApplication().getString(R.string.validation_message).toString(), Toast.LENGTH_LONG)
          .show();
      return false;
    }
  }

  public boolean validateProcessTxn_KASIR_BJ() {
    validateBjRek();

    if (!field_outletKvm_OK)
      field_outletKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_BiroJasaAmount_OK)
      field_BiroJasaAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    if (!field_BookingAmount_OK)
      field_BookingAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    if (field_Attach_Final_BUKTIRETUR == null)
      field_Attach_Final_BUKTIRETUR_OK.postValue(false);

    //TODO bukti retur belum ada, silahkan perbaiki
    // jika semua field dan attachment mandatory sudah terisi, maka tombol action dapat digunakan
    //    if (field_BiroJasa_FINAL_OK && field_Attach_Final_BUKTIRETUR != null) {
    if (field_BiroJasa_FINAL_OK) {
      return true;
    } else {
      setGeneralErrorMessage(getApplication().getString(R.string.validation_message).toString());
      return false;
    }
  }

  public boolean validateProcessTxn_KASIR_FIF() {
    validateFIFRek();

    if (!field_outletKvm_OK)
      field_outletKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_FIFAmount_OK)
      field_FIFAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    if (!field_BookingAmount_OK)
      field_BookingAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    if (field_Attach_Final_BUKTIRETUR == null)
      field_Attach_Final_BUKTIRETUR_OK.postValue(false);

    //TODO bukti retur belum ada, silahkan perbaiki
    // jika semua field dan attachment mandatory sudah terisi, maka tombol action dapat digunakan
    //    if (field_BiroJasa_FINAL_OK && field_Attach_Final_BUKTIRETUR != null) {
    if (field_FIF_FINAL_OK) {
      return true;
    } else {
      setGeneralErrorMessage(getApplication().getString(R.string.validation_message).toString());
      return false;
    }
  }

  @Override
  public boolean validateProcess() {
    validateConsRek();
    validateFIFRek();
    validateBjRek();

    if (!field_outletKvm_OK)
      field_outletKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_lobKvm_OK)
      field_LobKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_SoKvm_OK)
      field_SoKvm_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_Idnpksf_OK)
      field_Idnpksf_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_AlasanPinjam_OK)
      field_AlasanPinjam_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    // if (!field_AlasanPinjamCash_OK)
    // field_AlasanPinjamCash_ERROR
    // .postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_poprList_OK)
      field_poprList_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerName_OK)
      field_ConsumerName_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerAddress_OK)
      field_ConsumerAddress_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerPhone_OK)
      field_ConsumerPhone_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_VehicleNo_OK)
      field_VehicleNo_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));
    if (!field_ConsumerAmount_OK)
      field_ConsumerAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    validation_field_PencairanFIF(this.isField_isOpenClose().getValue(),
        this.getField_FIFAmount().getValue());

    if (!field_BookingAmount_OK)
      field_BookingAmount_ERROR
          .postValue(getApplication().getString(R.string.validation_mandatory));

    if (!field_FeeMatrix_OK)
      field_FeeMatrix_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));

    if (field_Attach_Final_KK == null)
      field_Attach_Final_KK_OK.postValue(false);
    if (field_Attach_Final_STNK == null)
      field_Attach_Final_STNK_OK.postValue(false);
    if (field_Attach_Final_BPKB == null)
      field_Attach_Final_BPKB_OK.postValue(false);
    if (field_Attach_Final_SPT == null)
      field_Attach_Final_SPT_OK.postValue(false);
    // if (field_Attach_Final_SERAH_TERIMA == null)
    // field_Attach_Final_SERAH_TERIMA_OK.postValue(false);
    boolean isShowBR = isShowBuktiRetur().getValue().booleanValue();
    boolean mandatoryBR = isShowBR && field_Attach_Final_BUKTIRETUR != null
        || !isShowBR && field_Attach_Final_BUKTIRETUR == null;
    if(!mandatoryBR)
      field_Attach_Final_BUKTIRETUR_OK.postValue(false);

    boolean mandatori_field = field_outletKvm_OK && field_lobKvm_OK && field_SoKvm_OK
        && field_Idnpksf_OK && field_AlasanPinjam_OK
        // && field_AlasanPinjamCash_OK
        && field_PoNo_OK && field_PoDate_OK && field_Attach_FINAL_PO_OK.getValue()
        && field_poprList_OK && field_ConsumerName_OK && field_ConsumerAddress_OK
        && field_ConsumerPhone_OK && field_VehicleNo_OK && field_BiroJasa_FINAL_OK
        && field_Cons_FINAL_OK && field_FIF_FINAL_OK && mandatoryBR;
    //        && field_ConsumerAmount_OK
    //        && field_FIFAmount_OK && field_BookingAmount_OK && field_FeeMatrix_OK
    //        && field_ConsumerBank_LABEL_OK && field_ConsumerAccountName_OK
    //        && field_ConsumerAccountNo_OK && field_FIFBank_LABEL_OK && field_FIFAccountName_OK
    //        && field_FIFAccountNo_OK;
    // cek semua Attachment mandatrory sudah terisi atau belum
    boolean mandatory_attach = field_Attach_Final_KK != null && field_Attach_Final_STNK != null
        && field_Attach_Final_BPKB != null && field_Attach_Final_SPT != null
        // && field_Attach_Final_SERAH_TERIMA != null
        ;

    if (getField_PoDate().getValue() != null
        && getField_PoDate().getValue().compareTo(LocalDate.now()) > 0) {
      Toast
          .makeText(getApplication(),
              getApplication().getString(R.string.podate_validation).toString(), Toast.LENGTH_LONG)
          .show();
      return false;
    }
    // jika semua field dan attachment mandatory sudah terisi, maka tombol action dapat digunakan
    if (mandatori_field && mandatory_attach) {
      // this.canProcess.postValue(true);
      return true;
    } else {
      // this.canProcess.postValue(false);
      Toast
          .makeText(getApplication(),
              getApplication().getString(R.string.validation_message).toString(), Toast.LENGTH_LONG)
          .show();
      return false;
    }
  }

  @Override
  public void processSaveDraftNew(boolean isSaveDraftProcess) {
    if (isSaveDraftProcess) {
      if (!validateProcessDraft(false))
        return;
    } else {
      if (!validateProcess())
        return;
    }
    UUID outletId = getField_outletKvm().getValue() != null ?
        getField_outletKvm().getValue().getKey().getOutletId() :
        null;
    processUC.saveDraftTransferNew(getField_AppBookingId(),
        outletId, getField_BookingDate().getValue(),
        getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AppNo().getValue(), getField_PoNo().getValue(),
        getField_PoDate().getValue(), att_po_id,(is_att_po_change?field_Attach_Final_PO:null), getPoprIdList(),
        getField_ConsumerName().getValue(), getField_ConsumerAddress().getValue(),
        getField_ConsumerPhone().getValue(), getField_VehicleNo().getValue(),
        isField_isOpenClose().getValue(),
        getField_ConsumerAmount().getValue()!=null?getField_ConsumerAmount().getValue():BigDecimal.ZERO,
        getField_ConsumerBankId().getValue(), getField_ConsumerOtherBank().getValue(),
        getField_ConsumerAccountNo().getValue(), getField_ConsumerAccountName().getValue(),
        getField_BiroJasaAmount().getValue()!=null?getField_BiroJasaAmount().getValue():BigDecimal.ZERO,
        getField_BiroJasaBankId().getValue(), getField_BiroJasaOtherBank().getValue(),
        getField_BiroJasaAccountNo().getValue(), getField_BiroJasaAccountName().getValue(),
        getField_FIFAmount().getValue()!=null?getField_FIFAmount().getValue():BigDecimal.ZERO ,
        getField_FIFBankId().getValue(), getField_FIFOtherBank().getValue(),
        getField_FIFAccountNo().getValue(), getField_FIFAccountName().getValue(),
        getField_BookingAmount().getValue()!=null?getField_BookingAmount().getValue():BigDecimal.ZERO,
        getField_FeeMatrix().getValue(), getField_FeeScheme().getValue(),
        att_stnk_id, (is_att_stnk_change ? field_Attach_Final_STNK : null), att_kk_id,
        (is_att_kk_change ? field_Attach_Final_KK : null), att_bpkb_id,
        (is_att_bpkb_change ? field_Attach_Final_BPKB : null),
        att_spt_id, (is_att_spt_change ? field_Attach_Final_SPT : null), att_fisik_motor_id,
        (is_att_fisik_motor_change ? field_Attach_Final_FISIK_MOTOR : null),
        getField_UpdateTS().getValue(),
        getFieldWfStatus().getValue(), getField_AppOperation(), isField_ConsValid().getValue(),
        isField_BjValid().getValue(), isField_FIFValid().getValue(), isSaveDraftProcess);
  }

  @Override
  public void processSaveDraftCancel(boolean isNewDocument) {
    validation_field_CancelReason(getField_CancelReason().getValue(), false);
    if (field_Attach_Final_HO == null)
      field_Attach_Final_HO_OK.postValue(false);

    if (!field_CancelReason_OK || field_Attach_Final_HO == null)
      return;
    processUC.saveDraftCashTransferCancel(getField_AppBookingId(),
        getField_AppBookingTypeId().getValue(), getField_BookingId(),
        getField_CancelReason().getValue(), isNewDocument ? UUID.randomUUID() : att_HO_id,
        isNewDocument ? field_Attach_Final_HO : (is_att_HO_change ? field_Attach_Final_HO : null),
        getField_UpdateTS().getValue(),
        isNewDocument);
  }

  @Override
  public void processSaveDraftEdit(boolean isSaveDraftProcess, boolean isNewDocument) {
    if (!validateProcess())
      return;

    processUC.saveDraftTransferEdit(getField_AppBookingId(),
        getField_outletKvm().getValue().getKey().getOutletId(), getField_BookingDate().getValue(),
        getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AppNo().getValue(), getField_PoNo().getValue(),
        getField_PoDate().getValue(), att_po_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_PO:((is_att_po_change?field_Attach_Final_PO:null)), getPoprIdList(),
        getField_ConsumerName().getValue(), getField_ConsumerAddress().getValue(),
        getField_ConsumerPhone().getValue(), getField_VehicleNo().getValue(),
        isField_isOpenClose().getValue(), getField_ConsumerAmount().getValue(),
        getField_ConsumerBankId().getValue(), getField_ConsumerOtherBank().getValue(),
        getField_ConsumerAccountNo().getValue(), getField_ConsumerAccountName().getValue(),
        isField_ConsValid().getValue(),
        getField_BiroJasaAmount().getValue(), getField_BiroJasaBankId().getValue(),
        getField_BiroJasaOtherBank().getValue(), getField_BiroJasaAccountNo().getValue(),
        getField_BiroJasaAccountName().getValue(), isField_BjValid().getValue(),
        getField_FIFAmount().getValue(),
        getField_FIFBankId().getValue(), getField_FIFOtherBank().getValue(),
        getField_FIFAccountNo().getValue(), getField_FIFAccountName().getValue(),
        isField_FIFValid().getValue(),
        getField_BookingAmount().getValue(), getField_FeeMatrix().getValue(),
        getField_FeeScheme().getValue(), att_stnk_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_STNK:(is_att_stnk_change ? field_Attach_Final_STNK : null),
        att_kk_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_KK:(is_att_kk_change ? field_Attach_Final_KK : null),
        att_bpkb_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_BPKB:(is_att_bpkb_change?field_Attach_Final_BPKB:null),
        att_spt_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_SPT:(is_att_spt_change?field_Attach_Final_SPT:null),
        att_fisik_motor_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_FISIK_MOTOR:(is_att_fisik_motor_change?field_Attach_Final_FISIK_MOTOR:null),
        getField_UpdateTS().getValue(), att_bukti_retur_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_BUKTIRETUR:(is_att_bukti_retur_change ? field_Attach_Final_BUKTIRETUR : null),
        getField_BookingId(), isSaveDraftProcess, isNewDocument);
  }

  @Override
  public void processWfSubmitNew() {
    if (!validateProcess())
      return;

    processUC.submitTransferNew(getField_AppBookingId(),
        getField_outletKvm().getValue().getKey().getOutletId(), getField_BookingDate().getValue(),
        getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AppNo().getValue(), getField_PoNo().getValue(),
        getField_PoDate().getValue(), att_po_id,(is_att_po_change?field_Attach_Final_PO:null), getPoprIdList(),
        getField_ConsumerName().getValue(), getField_ConsumerAddress().getValue(),
        getField_ConsumerPhone().getValue(), getField_VehicleNo().getValue(),
        isField_isOpenClose().getValue(), getField_ConsumerAmount().getValue(),
        getField_ConsumerBankId().getValue(), getField_ConsumerOtherBank().getValue(),
        getField_ConsumerAccountNo().getValue(), getField_ConsumerAccountName().getValue(),
        getField_BiroJasaAmount().getValue(), getField_BiroJasaBankId().getValue(),
        getField_BiroJasaOtherBank().getValue(), getField_BiroJasaAccountNo().getValue(),
        getField_BiroJasaAccountName().getValue(), getField_FIFAmount().getValue(),
        getField_FIFBankId().getValue(), getField_FIFOtherBank().getValue(),
        getField_FIFAccountNo().getValue(), getField_FIFAccountName().getValue(),
        getField_BookingAmount().getValue(), getField_FeeMatrix().getValue(),
        getField_FeeScheme().getValue(), att_stnk_id,(is_att_stnk_change?field_Attach_Final_STNK:null),
        att_kk_id,(is_att_kk_change?field_Attach_Final_KK:null),
        att_bpkb_id,(is_att_bpkb_change?field_Attach_Final_BPKB:null), att_spt_id,(is_att_spt_change?field_Attach_Final_SPT:null),
        att_fisik_motor_id,(is_att_fisik_motor_change?field_Attach_Final_FISIK_MOTOR:null),
        getField_UpdateTS().getValue(), isField_ConsValid().getValue(),
        isField_BjValid().getValue(), isField_FIFValid().getValue());
  }

  @Override
  public void processWfSubmitCancel(boolean isNewDocument) {
    validation_field_CancelReason(getField_CancelReason().getValue(), false);

    if (field_Attach_Final_HO == null)
      field_Attach_Final_HO_OK.postValue(false);

    if (!field_CancelReason_OK || field_Attach_Final_HO == null)
      return;

    processUC.submitCashTransfersCancel(getField_AppBookingId(),
        getField_AppBookingTypeId().getValue(), getField_BookingId(),
        getField_CancelReason().getValue(), att_HO_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_HO:(is_att_HO_change ? field_Attach_Final_HO : null), getField_UpdateTS().getValue(),
        isNewDocument);
  }

  @Override
  public void processWfSubmitEdit(boolean isNewDocument) {
    if (!validateProcess())
      return;

    processUC.submitTransferEdit(getField_AppBookingId(),
        getField_outletKvm().getValue().getKey().getOutletId(), getField_BookingDate().getValue(),
        getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AppNo().getValue(), getField_PoNo().getValue(),
        getField_PoDate().getValue(), att_po_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_PO:(is_att_po_change?field_Attach_Final_PO:null),
        getPoprIdList(), getField_ConsumerName().getValue(), getField_ConsumerAddress().getValue(),
        getField_ConsumerPhone().getValue(), getField_VehicleNo().getValue(),
        isField_isOpenClose().getValue(), getField_ConsumerAmount().getValue(),
        getField_ConsumerBankId().getValue(), getField_ConsumerOtherBank().getValue(),
        getField_ConsumerAccountNo().getValue(), getField_ConsumerAccountName().getValue(),
        isField_ConsValid().getValue(), getField_BiroJasaAmount().getValue(),
        getField_BiroJasaBankId().getValue(), getField_BiroJasaOtherBank().getValue(),
        getField_BiroJasaAccountNo().getValue(), getField_BiroJasaAccountName().getValue(),
        isField_BjValid().getValue(), getField_FIFAmount().getValue(),
        getField_FIFBankId().getValue(), getField_FIFOtherBank().getValue(),
        getField_FIFAccountNo().getValue(), getField_FIFAccountName().getValue(),
        isField_FIFValid().getValue(), getField_BookingAmount().getValue(),
        getField_FeeMatrix().getValue(), getField_FeeScheme().getValue(),att_stnk_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_STNK:(is_att_stnk_change?field_Attach_Final_STNK:null),
        att_kk_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_KK:(is_att_kk_change ? field_Attach_Final_KK : null),
        att_bpkb_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_BPKB:(is_att_bpkb_change ? field_Attach_Final_BPKB : null),
        att_spt_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_SPT:(is_att_spt_change ? field_Attach_Final_SPT : null),
        att_fisik_motor_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_FISIK_MOTOR:(is_att_fisik_motor_change ? field_Attach_Final_FISIK_MOTOR : null),
        att_bukti_retur_id,
        isFirstTimeLoadFromEditorCancel?field_Attach_Final_BUKTIRETUR:(is_att_bukti_retur_change ? field_Attach_Final_BUKTIRETUR : null),
        getField_BookingId(),
        getField_UpdateTS().getValue(), isNewDocument);
  }


  public void processTransWF_KASIR_SUBMIT_REVISION_NEW(String comment) {
    if (!validateProcess())
      return;

    UUID procTaskId = getField_ProcTaskId().getValue();

    processUC.processTransWF_KASIR_SUBMIT_REVISION_NEW(procTaskId, comment,
        getField_AppBookingId(), getField_outletKvm().getValue().getKey().getOutletId(),
        getField_BookingDate().getValue(), getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AppNo().getValue(), getField_PoNo().getValue(),
        getField_PoDate().getValue(), att_po_id,(is_att_po_change?field_Attach_Final_PO:null), getPoprIdList(),
        getField_ConsumerName().getValue(), getField_ConsumerAddress().getValue(),
        getField_ConsumerPhone().getValue(), getField_VehicleNo().getValue(),
        isField_isOpenClose().getValue(), getField_ConsumerAmount().getValue(),
        getField_ConsumerBankId().getValue(), getField_ConsumerOtherBank().getValue(),
        getField_ConsumerAccountNo().getValue(), getField_ConsumerAccountName().getValue(),
        isField_ConsValid().getValue(),
        getField_BiroJasaAmount().getValue(), getField_BiroJasaBankId().getValue(),
        getField_BiroJasaOtherBank().getValue(), getField_BiroJasaAccountNo().getValue(),
        getField_BiroJasaAccountName().getValue(), isField_BjValid().getValue(),
        getField_FIFAmount().getValue(),
        getField_FIFBankId().getValue(), getField_FIFOtherBank().getValue(),
        getField_FIFAccountNo().getValue(), getField_FIFAccountName().getValue(),
        isField_FIFValid().getValue(),
        getField_BookingAmount().getValue(), getField_FeeMatrix().getValue(),
        getField_FeeScheme().getValue(), att_stnk_id,
        (is_att_stnk_change ? field_Attach_Final_STNK : null), att_kk_id,
        (is_att_kk_change ? field_Attach_Final_KK : null),
        att_bpkb_id,(is_att_bpkb_change?field_Attach_Final_BPKB:null), att_spt_id,(is_att_spt_change?field_Attach_Final_SPT:null),
        att_fisik_motor_id,(is_att_fisik_motor_change?field_Attach_Final_FISIK_MOTOR:null),
        getField_UpdateTS().getValue());
  }

  public void processTransWF_AOC_APPROVE_NEW(String comment) {
    processUC.processTransWF_AOC_APPROVE_NEW(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue(),
        getField_Batch().getValue().getKey().getCode(), getField_OperationalTransfer().getValue());
  }

  public void processTransWF_AOC_RETURN_NEW(String comment) {
    processUC.processTransWF_AOC_RETURN_NEW(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_CAPTAIN_APPROVE_NEW(String comment) {
    processUC.processTransWF_CAPTAIN_APPROVE_NEW(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue(),
        getField_Batch().getValue().getKey().getCode(), getField_OperationalTransfer().getValue());
  }

  public void processTransWF_CAPTAIN_RETURN_NEW(String comment) {
    processUC.processTransWF_CAPTAIN_RETURN_NEW(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_FINST_CUS_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_FINST_CUS_RETURN_NEW(getPtCons().getId(), comment,
        txnConsId);
  }

  public void processTransTxn_WF_FINST_CUS_APPROVE_NEW(String comment) {
    if (!validateProcessTxn_FINSTAFF_CUS()) {
      setGeneralErrorMessage(getApplication().getString(R.string.mandatory_txn_field));
      return;
    }

    processUC.processTransTxn_WF_FINST_CUS_APPROVE_NEW(getPtCons().getId(), comment,
        getField_Wf_Txn_DateCons(),
        getField_Wf_Txn_BankIdCons().getKey().getId(),
        getField_Wf_Txn_IsManualCons(), txnConsId);
  }

  public void processTransTxn_WF_CAPTAIN_CUS_APPROVE_NEW(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_CUS_APPROVE_NEW(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_CAPTAIN_CUS_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_CUS_RETURN_NEW(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransWF_KASIR_CUS_REVISION_NEW(String comment) {
    if (!validateProcessTxn_KASIR_CUS())
      return;

    processUC.processTransWF_KASIR_CUS_REVISION_NEW(ptCons.getId(), comment,
        getField_AppBookingId(), getField_outletKvm().getValue().getKey().getOutletId(),
        getField_BookingDate().getValue(), getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AppNo().getValue(), getField_PoNo().getValue(),
        getField_PoDate().getValue(), att_po_id,(is_att_po_change?field_Attach_Final_PO:null), getPoprIdList(),
        getField_ConsumerName().getValue(), getField_ConsumerAddress().getValue(),
        getField_ConsumerPhone().getValue(), getField_VehicleNo().getValue(),
        getField_ConsumerAmount().getValue(), getField_ConsumerBankId().getValue(),
        getField_ConsumerOtherBank().getValue(), getField_ConsumerAccountNo().getValue(),
        getField_ConsumerAccountName().getValue(), isField_ConsValid().getValue(),
        getField_BookingAmount().getValue(), getField_FeeMatrix().getValue(),
        getField_FeeScheme().getValue(), att_stnk_id,
        (is_att_stnk_change ? field_Attach_Final_STNK : null), att_kk_id,
        (is_att_kk_change ? field_Attach_Final_KK : null),
        att_bpkb_id, (is_att_bpkb_change ? field_Attach_Final_BPKB : null), att_spt_id,
        (is_att_spt_change ? field_Attach_Final_SPT : null), att_fisik_motor_id,
        (is_att_fisik_motor_change ? field_Attach_Final_FISIK_MOTOR : null),
        getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_CUS_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_AOC_CUS_RETURN_NEW(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_CUS_APPROVE_NEW(String comment) {
    processUC.processTransTxn_WF_AOC_CUS_APPROVE_NEW(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_FINSPV_CUS_APPROVE_NEW(String comment, String OTP) {
    processUC.processTransTxn_WF_FINSPV_CUS_APPROVE_NEW(ptCons.getId(), comment, OTP,
        getField_Wf_Txn_IsManualCons());
  }

  public void processTransTxn_WF_FINSPV_CUS_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_FINSPV_CUS_RETURN_NEW(ptCons.getId(), comment,
        txnConsId);
  }

  public void processTransTxn_WF_FINSPV_CUS_REJECT_NEW(String comment) {
    processUC.processTransTxn_WF_FINSPV_CUS_REJECT_NEW(ptCons.getId(), comment,
        txnConsId);
  }

  public void processTransTxn_WF_FINST_BJ_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_FINST_BJ_RETURN_NEW(getPtBj().getId(), comment,
        txnBjId);
  }

  public void processTransTxn_WF_FINST_BJ_APPROVE_NEW(String comment) {

    if (!validateProcessTxn_FINSTAFF_BJ()) {
      setGeneralErrorMessage(getApplication().getString(R.string.mandatory_txn_field));
      return;
    }

    processUC.processTransTxn_WF_FINST_BJ_APPROVE_NEW(getPtBj().getId(), comment,
        getField_Wf_Txn_DateBj(), getField_Wf_Txn_BankIdBj().getKey().getId(),
        getField_Wf_Txn_IsManualBj(), txnBjId);
  }

  public void processTransTxn_WF_CAPTAIN_BJ_APPROVE_NEW(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_BJ_APPROVE_NEW(getPtBj().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_CAPTAIN_BJ_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_BJ_RETURN_NEW(getPtBj().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransWF_KASIR_BJ_REVISION_NEW(String comment) {
    if (!validateProcessTxn_KASIR_BJ())
      return;

    processUC.processTransWF_KASIR_BJ_REVISION_NEW(ptBj.getId(), txnBjId, comment,
        getField_AppBookingId(), getField_outletKvm().getValue().getKey().getOutletId(),
        getField_BiroJasaAmount().getValue(), getField_BiroJasaBankId().getValue(),
        getField_BiroJasaOtherBank().getValue(), getField_BiroJasaAccountNo().getValue(),
        getField_BiroJasaAccountName().getValue(), isField_BjValid().getValue(),
        getField_BookingAmount().getValue(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_BJ_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_AOC_BJ_RETURN_NEW(getPtBj().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_BJ_APPROVE_NEW(String comment) {
    processUC.processTransTxn_WF_AOC_BJ_APPROVE_NEW(getPtBj().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_FINSPV_BJ_APPROVE_NEW(String comment, String OTP) {
    processUC.processTransTxn_WF_FINSPV_BJ_APPROVE_NEW(ptBj.getId(), comment, OTP,
        getField_Wf_Txn_IsManualBj());
  }

  public void processTransTxn_WF_FINSPV_BJ_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_FINSPV_BJ_RETURN_NEW(ptBj.getId(), comment,
        txnConsId);
  }

  public void processTransTxn_WF_FINSPV_BJ_REJECT_NEW(String comment) {
    processUC.processTransTxn_WF_FINSPV_BJ_REJECT_NEW(ptBj.getId(), comment,
        txnBjId);
  }

  public void processTransTxn_WF_FINST_FIF_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_FINST_FIF_RETURN_NEW(getPtFIF().getId(), comment,
        txnFifId);
  }

  public void processTransTxn_WF_FINST_FIF_APPROVE_NEW(String comment) {
    if (!validateProcessTxn_FINSTAFF_FIF()) {
      setGeneralErrorMessage(getApplication().getString(R.string.mandatory_txn_field));
      return;
    }

    processUC.processTransTxn_WF_FINST_FIF_APPROVE_NEW(getPtFIF().getId(), comment,
        getField_Wf_Txn_DateFIF(), getField_Wf_Txn_BankIdFIF().getKey().getId(),
        getField_Wf_Txn_IsManualFIF(), txnFifId);
  }

  public void processTransTxn_WF_CAPTAIN_FIF_APPROVE_NEW(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_FIF_APPROVE_NEW(getPtFIF().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_CAPTAIN_FIF_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_FIF_RETURN_NEW(getPtFIF().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransWF_KASIR_FIF_REVISION_NEW(String comment) {
    if (!validateProcessTxn_KASIR_FIF())
      return;

    processUC.processTransWF_KASIR_FIF_REVISION_NEW(ptFIF.getId(), txnFifId, comment,
        getField_AppBookingId(), getField_outletKvm().getValue().getKey().getOutletId(),
        isField_isOpenClose().getValue(), getField_FIFAmount().getValue(),
        getField_FIFBankId().getValue(), getField_FIFOtherBank().getValue(),
        getField_FIFAccountNo().getValue(), getField_FIFAccountName().getValue(),
        isField_FIFValid().getValue(), getField_BookingAmount().getValue(),
        getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_FIF_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_AOC_FIF_RETURN_NEW(getPtFIF().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_FIF_APPROVE_NEW(String comment) {
    processUC.processTransTxn_WF_AOC_FIF_APPROVE_NEW(getPtFIF().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_FINSPV_FIF_APPROVE_NEW(String comment, String OTP) {
    processUC.processTransTxn_WF_FINSPV_FIF_APPROVE_NEW(ptFIF.getId(), comment, OTP,
        getField_Wf_Txn_IsManualFIF());
  }

  public void processTransTxn_WF_FINSPV_FIF_RETURN_NEW(String comment) {
    processUC.processTransTxn_WF_FINSPV_FIF_RETURN_NEW(ptFIF.getId(), comment,
        txnConsId);
  }

  public void processTransTxn_WF_FINSPV_FIF_REJECT_NEW(String comment) {
    processUC.processTransTxn_WF_FINSPV_FIF_REJECT_NEW(ptFIF.getId(), comment,
        txnFifId);
  }

  public void processTransWF_AOC_APPROVE_EDIT(String comment) {
    processUC.processTransWF_AOC_APPROVE_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue(),
        getField_Batch().getValue().getKey().getCode(), getField_OperationalTransfer().getValue());
  }

  public void processTransWF_AOC_RETURN_EDIT(String comment) {
    processUC.processTransWF_AOC_RETURN_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_AOC_REJECT_EDIT(String comment) {
    processUC.processTransWF_AOC_REJECT_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_KASIR_SUBMIT_REVISION_EDIT(String comment) {
    if (!validateProcess())
      return;

    UUID procTaskId = getField_ProcTaskId().getValue();

    processUC.processTransWF_KASIR_SUBMIT_REVISION_EDIT(procTaskId, comment,
        getField_AppBookingId(), getField_outletKvm().getValue().getKey().getOutletId(),
        getField_BookingDate().getValue(), getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AppNo().getValue(), getField_PoNo().getValue(),
        getField_PoDate().getValue(), att_po_id,(is_att_po_change?field_Attach_Final_PO:null), getPoprIdList(),
        getField_ConsumerName().getValue(), getField_ConsumerAddress().getValue(),
        getField_ConsumerPhone().getValue(), getField_VehicleNo().getValue(),
        isField_isOpenClose().getValue(), getField_ConsumerAmount().getValue(),
        getField_ConsumerBankId().getValue(), getField_ConsumerOtherBank().getValue(),
        getField_ConsumerAccountNo().getValue(), getField_ConsumerAccountName().getValue(),
        isField_ConsValid().getValue(),
        getField_BiroJasaAmount().getValue(), getField_BiroJasaBankId().getValue(),
        getField_BiroJasaOtherBank().getValue(), getField_BiroJasaAccountNo().getValue(),
        getField_BiroJasaAccountName().getValue(), isField_BjValid().getValue(),
        getField_FIFAmount().getValue(),
        getField_FIFBankId().getValue(), getField_FIFOtherBank().getValue(),
        getField_FIFAccountNo().getValue(), getField_FIFAccountName().getValue(),
        isField_FIFValid().getValue(),
        getField_BookingAmount().getValue(), getField_FeeMatrix().getValue(),
        getField_FeeScheme().getValue(), att_stnk_id,
        (is_att_stnk_change ? field_Attach_Final_STNK : null), att_kk_id,
        (is_att_kk_change ? field_Attach_Final_KK : null),
        att_bpkb_id, (is_att_bpkb_change ? field_Attach_Final_BPKB : null), att_spt_id,
        (is_att_spt_change ? field_Attach_Final_SPT : null), att_fisik_motor_id,
        (is_att_fisik_motor_change ? field_Attach_Final_FISIK_MOTOR : null),
        getField_BookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_CAPTAIN_APPROVE_EDIT(String comment) {
    processUC.processTransWF_CAPTAIN_APPROVE_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue(), getField_OperationalTransfer().getValue());
  }

  public void processTransWF_CAPTAIN_RETURN_EDIT(String comment) {
    processUC.processTransWF_CAPTAIN_RETURN_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_CAPTAIN_REJECT_EDIT(String comment) {
    processUC.processTransWF_CAPTAIN_REJECT_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_DIRECTOR_APPROVE_EDIT(String comment) {
    processUC.processTransWF_DIRECTOR_APPROVE_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_BookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_DIRECTOR_RETURN_EDIT(String comment) {
    processUC.processTransWF_DIRECTOR_RETURN_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_DIRECTOR_REJECT_EDIT(String comment) {
    processUC.processTransWF_DIRECTOR_REJECT_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_FINST_CUS_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_FINST_CUS_RETURN_EDIT(getPtCons().getId(), comment,
        txnConsId);
  }

  public void processTransTxn_WF_FINST_CUS_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_FINST_CUS_REJECT_EDIT(ptCons.getId(), comment,
        txnConsId);
  }

  public void processTransTxn_WF_FINST_CUS_APPROVE_EDIT(String comment) {

    if (!validateProcessTxn_FINSTAFF_CUS()) {
      setGeneralErrorMessage(getApplication().getString(R.string.mandatory_txn_field));
      return;
    }

    processUC.processTransTxn_WF_FINST_CUS_APPROVE_EDIT(getPtCons().getId(), comment,
        getField_Wf_Txn_DateCons(), getField_Wf_Txn_BankIdCons().getKey().getId(),
        getField_Wf_Txn_IsManualCons(), txnConsId);
  }

  public void processTransTxn_WF_DIRECTOR_CUS_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_DIRECTOR_CUS_APPROVE_EDIT(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_BookingId(), getField_ConsumerAmount().getValue(),
        getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_DIRECTOR_CUS_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_DIRECTOR_CUS_RETURN_EDIT(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_DIRECTOR_CUS_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_DIRECTOR_CUS_REJECT_EDIT(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_KASIR_CUS_REVISION_EDIT(String comment) {
    if (!validateProcessTxn_KASIR_CUS())
      return;
    processUC.processTransWF_KASIR_CUS_REVISION_EDIT(ptCons.getId(), comment,
        getField_AppBookingId(), getField_outletKvm().getValue().getKey().getOutletId(),
        getField_BookingDate().getValue(), getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AppNo().getValue(), getField_PoNo().getValue(),
        getField_PoDate().getValue(), att_po_id,(is_att_po_change?field_Attach_Final_PO:null), getPoprIdList(),
        getField_ConsumerName().getValue(), getField_ConsumerAddress().getValue(),
        getField_ConsumerPhone().getValue(), getField_VehicleNo().getValue(),
        getField_ConsumerAmount().getValue(), getField_ConsumerBankId().getValue(),
        getField_ConsumerOtherBank().getValue(), getField_ConsumerAccountNo().getValue(),
        getField_ConsumerAccountName().getValue(), isField_ConsValid().getValue(),
        getField_BookingAmount().getValue(), getField_FeeMatrix().getValue(),
        getField_FeeScheme().getValue(), att_stnk_id,(is_att_stnk_change?field_Attach_Final_STNK:null), att_kk_id,(is_att_kk_change?field_Attach_Final_KK:null),
        att_bpkb_id, (is_att_bpkb_change ? field_Attach_Final_BPKB : null), att_spt_id,
        (is_att_spt_change ? field_Attach_Final_SPT : null), att_fisik_motor_id,
        (is_att_fisik_motor_change ? field_Attach_Final_FISIK_MOTOR : null),
        att_bukti_retur_id, (is_att_bukti_retur_change ? field_Attach_Final_BUKTIRETUR : null),
        getField_BookingId(),
        getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_CUS_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_AOC_CUS_RETURN_EDIT(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_CUS_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_AOC_CUS_APPROVE_EDIT(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_CUS_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_AOC_CUS_REJECT_EDIT(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_CAPTAIN_CUS_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_CUS_RETURN_EDIT(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_CAPTAIN_CUS_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_CUS_APPROVE_EDIT(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_CAPTAIN_CUS_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_CUS_REJECT_EDIT(getPtCons().getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_FINSPV_CUS_APPROVE_EDIT(String comment, String OTP) {
    processUC.processTransTxn_WF_FINSPV_CUS_APPROVE_EDIT(ptCons.getId(), comment, OTP,
        getField_Wf_Txn_IsManualCons());
  }

  public void processTransTxn_WF_FINSPV_CUS_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_FINSPV_CUS_RETURN_EDIT(ptCons.getId(), comment,
        txnConsId);
  }

  public void processTransTxn_WF_FINSPV_CUS_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_FINSPV_CUS_REJECT_EDIT(ptCons.getId(), comment,
        txnConsId);
  }

  public void processTransTxn_WF_FINST_BJ_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_FINST_BJ_RETURN_EDIT(ptBj.getId(), comment,
        txnBjId);
  }

  public void processTransTxn_WF_FINST_BJ_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_FINST_BJ_REJECT_EDIT(ptBj.getId(), comment,
        txnBjId);
  }

  public void processTransTxn_WF_FINST_BJ_APPROVE_EDIT(String comment) {
    if (!validateProcessTxn_FINSTAFF_BJ()) {
      setGeneralErrorMessage(getApplication().getString(R.string.mandatory_txn_field));
      return;
    }

    processUC.processTransTxn_WF_FINST_BJ_APPROVE_EDIT(ptBj.getId(), comment,
        getField_Wf_Txn_DateBj(), getField_Wf_Txn_BankIdBj().getKey().getId(),
        getField_Wf_Txn_IsManualBj(), txnConsId);
  }

  public void processTransTxn_WF_DIRECTOR_BJ_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_DIRECTOR_BJ_APPROVE_EDIT(ptBj.getId(), comment,
        getField_AppBookingId(), getField_BookingId(), getField_BiroJasaAmount().getValue(),
        getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_DIRECTOR_BJ_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_DIRECTOR_BJ_RETURN_EDIT(ptBj.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_DIRECTOR_BJ_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_DIRECTOR_BJ_REJECT_EDIT(ptBj.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_KASIR_BJ_REVISION_EDIT(String comment) {
    if (!validateProcessTxn_KASIR_BJ())
      return;
    processUC.processTransWF_KASIR_BJ_REVISION_EDIT(ptBj.getId(), comment,
        getField_AppBookingId(), getField_outletKvm().getValue().getKey().getOutletId(),
        txnBjId, getField_BiroJasaAmount().getValue(), getField_BiroJasaBankId().getValue(),
        getField_BiroJasaOtherBank().getValue(), getField_BiroJasaAccountNo().getValue(),
        getField_BiroJasaAccountName().getValue(), isField_BjValid().getValue(),
        getField_BookingAmount().getValue(), att_bukti_retur_id,
        (is_att_bukti_retur_change ? field_Attach_Final_BUKTIRETUR : null), getField_BookingId(),
        getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_BJ_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_AOC_BJ_RETURN_EDIT(ptBj.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_BJ_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_AOC_BJ_APPROVE_EDIT(ptBj.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_BJ_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_AOC_BJ_REJECT_EDIT(ptBj.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_CAPTAIN_BJ_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_BJ_RETURN_EDIT(ptBj.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_CAPTAIN_BJ_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_BJ_APPROVE_EDIT(ptBj.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_CAPTAIN_BJ_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_BJ_REJECT_EDIT(ptBj.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_FINSPV_BJ_APPROVE_EDIT(String comment, String OTP) {
    processUC.processTransTxn_WF_FINSPV_BJ_APPROVE_EDIT(ptBj.getId(), comment, OTP,
        getField_Wf_Txn_IsManualBj());
  }

  public void processTransTxn_WF_FINSPV_BJ_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_FINSPV_BJ_RETURN_EDIT(ptBj.getId(), comment,
        txnBjId);
  }

  public void processTransTxn_WF_FINSPV_BJ_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_FINSPV_BJ_REJECT_EDIT(ptBj.getId(), comment,
        txnBjId);
  }

  public void processTransTxn_WF_FINST_FIF_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_FINST_FIF_RETURN_EDIT(ptFIF.getId(), comment,
        txnFifId);
  }

  public void processTransTxn_WF_FINST_FIF_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_FINST_FIF_REJECT_EDIT(ptFIF.getId(), comment,
        txnFifId);
  }

  public void processTransTxn_WF_FINST_FIF_APPROVE_EDIT(String comment) {
    if (!validateProcessTxn_FINSTAFF_FIF()) {
      setGeneralErrorMessage(getApplication().getString(R.string.mandatory_txn_field));
      return;
    }
    processUC.processTransTxn_WF_FINST_FIF_APPROVE_EDIT(ptFIF.getId(), comment,
        getField_Wf_Txn_DateFIF(), getField_Wf_Txn_BankIdFIF().getKey().getId(),
        getField_Wf_Txn_IsManualFIF(), txnFifId);

  }

  public void processTransTxn_WF_DIRECTOR_FIF_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_DIRECTOR_FIF_APPROVE_EDIT(ptFIF.getId(), comment,
        getField_AppBookingId(), getField_BookingId(), getField_FIFAmount().getValue(),
        getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_DIRECTOR_FIF_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_DIRECTOR_FIF_RETURN_EDIT(ptFIF.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_DIRECTOR_FIF_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_DIRECTOR_FIF_REJECT_EDIT(ptFIF.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_KASIR_FIF_REVISION_EDIT(String comment) {
    if (!validateProcessTxn_KASIR_FIF())
      return;

    processUC.processTransWF_KASIR_FIF_REVISION_EDIT(ptFIF.getId(), comment,
        getField_AppBookingId(), getField_outletKvm().getValue().getKey().getOutletId(), txnFifId,
        isField_isOpenClose().getValue(), getField_FIFAmount().getValue(),
        getField_FIFBankId().getValue(), getField_FIFOtherBank().getValue(),
        getField_FIFAccountNo().getValue(), getField_FIFAccountName().getValue(),
        isField_FIFValid().getValue(), getField_BookingAmount().getValue(),
        field_Attach_Final_BUKTIRETUR, getField_BookingId(),
        getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_FIF_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_AOC_FIF_RETURN_EDIT(ptFIF.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_FIF_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_AOC_FIF_APPROVE_EDIT(ptFIF.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_AOC_FIF_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_AOC_FIF_REJECT_EDIT(ptFIF.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_CAPTAIN_FIF_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_FIF_RETURN_EDIT(ptFIF.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_CAPTAIN_FIF_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_FIF_APPROVE_EDIT(ptFIF.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());//txnConsUpdateTs);
  }

  public void processTransTxn_WF_CAPTAIN_FIF_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_CAPTAIN_FIF_REJECT_EDIT(ptFIF.getId(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_FINSPV_FIF_APPROVE_EDIT(String comment,String OTP) {
    processUC.processTransTxn_WF_FINSPV_FIF_APPROVE_EDIT(ptFIF.getId(), comment, OTP, getField_Wf_Txn_IsManualFIF());
  }

  public void processTransTxn_WF_FINSPV_FIF_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_FINSPV_FIF_RETURN_EDIT(ptFIF.getId(), comment,
        txnFifId);
  }

  public void processTransTxn_WF_FINSPV_FIF_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_FINSPV_FIF_REJECT_EDIT(ptFIF.getId(), comment,
        txnFifId);
  }

  public void processTransWF_AOC_APPROVE_CANCEL(String comment) {
    processUC.processTransWF_AOC_APPROVE_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_AOC_RETURN_CANCEL(String comment) {
    processUC.processTransWF_AOC_RETURN_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_AOC_REJECT_CANCEL(String comment) {
    processUC.processTransWF_AOC_REJECT_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_KASIR_REVISION_CANCEL(String comment) {
    validation_field_CancelReason(getField_CancelReason().getValue(), false);

    if (field_Attach_Final_HO == null)
      field_Attach_Final_HO_OK.postValue(false);

    if (!field_CancelReason_OK || field_Attach_Final_HO == null)
      return;

    processUC.submitRevisionTransferCancel(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_AppBookingTypeId().getValue(),
        getField_BookingId(), getField_CancelReason().getValue(),
        att_HO_id, (is_att_HO_change ? field_Attach_Final_HO : null),
        getField_UpdateTS().getValue());
  }

  public void processTransWF_CAPTAIN_APPROVE_CANCEL(String comment) {
    processUC.processTransWF_CAPTAIN_APPROVE_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_CAPTAIN_RETURN_CANCEL(String comment) {
    processUC.processTransWF_CAPTAIN_RETURN_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_CAPTAIN_REJECT_CANCEL(String comment) {
    processUC.processTransWF_CAPTAIN_REJECT_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_DIRECTOR_APPROVE_CANCEL(String comment) {
    processUC.processTransWF_DIRECTOR_APPROVE_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_DIRECTOR_RETURN_CANCEL(String comment) {
    processUC.processTransWF_DIRECTOR_RETURN_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_DIRECTOR_REJECT_CANCEL(String comment) {
    processUC.processTransWF_DIRECTOR_REJECT_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_FINRETUR_APPROVE_CANCEL(String comment) {
    processUC.processTransWF_FINRETUR_APPROVE_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_FINRETUR_RETURN_CANCEL(String comment) {
    processUC.processTransWF_FINRETUR_RETURN_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransWF_FINRETUR_REJECT_CANCEL(String comment) {
    processUC.processTransWF_FINRETUR_REJECT_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processTransTxn_WF_FINRETUR_FIF_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_FINRETUR_FIF_APPROVE_EDIT(ptFIF.getId(), comment);
  }

  public void processTransTxn_WF_FINRETUR_FIF_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_FINRETUR_FIF_RETURN_EDIT(ptFIF.getId(), comment,
        txnFifId);
  }

  public void processTransTxn_WF_FINRETUR_FIF_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_FINRETUR_FIF_REJECT_EDIT(ptFIF.getId(), comment,
        txnFifId);
  }

  public void processTransTxn_WF_FINRETUR_BJ_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_FINRETUR_BJ_APPROVE_EDIT(ptBj.getId(), comment);
  }

  public void processTransTxn_WF_FINRETUR_BJ_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_FINRETUR_BJ_RETURN_EDIT(ptBj.getId(), comment,
        txnBjId);
  }

  public void processTransTxn_WF_FINRETUR_BJ_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_FINRETUR_BJ_REJECT_EDIT(ptBj.getId(), comment,
        txnBjId);
  }

  public void processTransTxn_WF_FINRETUR_CUS_APPROVE_EDIT(String comment) {
    processUC.processTransTxn_WF_FINRETUR_CUS_APPROVE_EDIT(ptCons.getId(), comment);
  }

  public void processTransTxn_WF_FINRETUR_CUS_RETURN_EDIT(String comment) {
    processUC.processTransTxn_WF_FINRETUR_CUS_RETURN_EDIT(ptCons.getId(), comment,
        txnConsId);
  }

  public void processTransTxn_WF_FINRETUR_CUS_REJECT_EDIT(String comment) {
    processUC.processTransTxn_WF_FINRETUR_CUS_REJECT_EDIT(ptCons.getId(), comment,
        txnConsId);
  }

  public void processGenerateOTP_AppBookingTransferCustomer() {
    processUC.processGenerateOTP_AppBookingTransferCustomer(ptCons.getId());
  }

  public void processGenerateOTP_AppBookingTransferBiroJasa() {
    processUC.processGenerateOTP_AppBookingTransferBiroJasa(ptBj.getId());
  }

  public void processGenerateOTP_AppBookingTransferFIF() {
    processUC.processGenerateOTP_AppBookingTransferFIF(ptFIF.getId());
  }

  @Override
  public void setField_BiroJasaAmount(BigDecimal data) {
    if (TXN_BIRO_JASA.equals(getTxnMode())) {
      if (data == null)
        field_BiroJasaAmount_ERROR.postValue(
            getApplication().getString(R.string.validation_mandatory));
      else
        field_BiroJasaAmount_ERROR.postValue(null);
    }
    super.setField_BiroJasaAmount(data);
  }

  @Override
  public void setField_FIFAmount(BigDecimal data) {
    if (TXN_FIF.equals(getTxnMode())) {
      if (data == null)
        field_FIFAmount_ERROR.postValue(
            getApplication().getString(R.string.validation_mandatory));
      else
        field_FIFAmount_ERROR.postValue(null);
    }
    super.setField_FIFAmount(data);
  }

  @Override
  public void onLoadSuccess(Resource<AppBookingLoadUC.LoadResult> res) {
    String fn = "onLoadSuccess()";
    isLoadProcess = true;
    detailResponse = res.getData().getAppBookingDetailResponse();
    AppBookingData abd = detailResponse.getAppBookingData();
    prepareFormResponse = res.getData().getNewBookingReponse();


    List<ProcTaskData> ptdList = res.getData().getAppBookingDetailResponse().getProcTaskDataList();
    ProcTaskData pd = ptdList.size() > 0 ? ptdList.get(0) : null;
    setField_ProcTaskId(pd != null ? pd.getId() : null);
    if (pd != null && pd.getPossibleOutcome() != null)
      setWfTaskList(pd.getPossibleOutcome());

    AppBookingTransferScreenMode screenMode = getScreenMode(ptdList, abd.getAppOperationId(),
        abd.getWfId());

    switch (screenMode){
    case KASIR_SUBMITTED_NEW_APPBOOKING:
    case KASIR_SUBMITTED_EDIT_APPBOOKING:
    case KASIR_REVISION_NEW_APPBOOKING:
    case KASIR_REVISION_EDIT_APPBOOKING:
      setTransferFieldEnabled(true);
      break;
    default:
      setTransferFieldEnabled(false);
      break;
    }

    setField_AppBookingId(abd.getId());

    //    List<BankAccountData> badList = res.getData().getNewBookingReponse().getBankAccountDataList();
    //    setBankAccount_Ld(badList);
    List<BatchData> bdList = res.getData().getNewBookingReponse().getBatchDataList();
    setBatch_Ld(bdList);

    txnConsId = abd.getConsTxnId();
    txnConsUpdateTs = abd.getConsTxnUpdateTs();
    txnBjId = abd.getBjTxnId();
    txnBjUpdateTs = abd.getBjTxnUpdateTs();
    txnFifId = abd.getFifTxnId();
    txnFifUpdateTs = abd.getFifTxnUpdateTs();

    BatchData bdObj = abd.getBatchData();
    if (bdObj != null) {
      for (BatchData bd : bdList) {
        if (bd.getCode().equals(bdObj.getCode())) {
          String timeValue = bd.getCode().equals(REAL_TIME_CODE) ?
              REAL_TIME_VALUE :
              bd.getEndTime().toString();
          setField_Batch(new KeyValueModel<>(bd, timeValue));
        }
      }
    }

    // set Header
    setField_TrxNo(abd.getTrxNo());
    setHeader_PtName(abd.getOutletObj().getCompanyName());
    setField_CompanyId(abd.getOutletObj().getCompanyId());
    setFieldWfStatus(abd.getWfId());
    setHeader_Status(abd.getWfName());
    setHeader_OutletName(abd.getOutletObj().getOutletName());
    setHeader_OutletCode(abd.getOutletObj().getOutletCode());
    setHeader_JenisPengajuan(abd.getAppTypeName());
    setHeader_BookingDate(abd.getBookingDate().format(Formatter.DTF_dd_MM_yyyy));

    String pengajuanDate = abd.getPengajuanDate() != null
        ? Formatter.SDF_dd_MM_yyyy.format(abd.getPengajuanDate())
        : "-";
    setHeader_PengajuanDate(pengajuanDate);

    // set prepare form
    setPrepareForm(prepareFormResponse);

    // set Field
    for (int i = 0; i < prepareFormResponse.getAssignedOutlet().size(); i++) {
      OutletShortData osd = prepareFormResponse.getAssignedOutlet().get(i);
      if (osd.getOutletId().equals(abd.getOutletObj().getOutletId())) {
        setField_outletKvm(new KeyValueModel<>(osd, osd.getOutletName()));
      }
    }

    setField_OperationalTransfer(abd.getOperationalTransfer());

    setField_AppBookingTypeId(abd.getBookingTypeId());
    setField_AppOperation(abd.getAppOperationId());
    setField_ConsumerNo(abd.getConsumerNo());
    setField_BookingDate(abd.getBookingDate());

    for (int i = 0; i < prepareFormResponse.getLobData().size(); i++) {
      LobShortData lsd = prepareFormResponse.getLobData().get(i);
      if (lsd.getLobId().equals(abd.getLobId())) {
        setField_LobKvm(new KeyValueModel<>(lsd, lsd.getLobName()));
      }
    }

    for (int i = 0; i < prepareFormResponse.getSoDataList().size(); i++) {
      SoShortData ssd = prepareFormResponse.getSoDataList().get(i);
      if (ssd.getSoId().equals(abd.getSobId())) {
        setField_SoKvm(new KeyValueModel<SoShortData, String>(ssd, ssd.getSoName()));
      }
    }

    setField_BookingId(abd.getBookingId());
    setField_Idnpksf(abd.getIdnpksf());
    setField_AlasanPinjam(abd.getReason());
    setField_AlasanPinjamCash(abd.getCashReason());
    setField_AppNo(abd.getAppNo());
    setField_PoNo(abd.getPoNo());
    setField_PoDate(abd.getPoDate());
    setField_UpdateTS(abd.getUpdateTs());

    List<KeyValueModel<UUID, String>> poprIdList = abd.getPoprList().stream().map(obj -> {
      KeyValueModel<UUID, String> newObj = new KeyValueModel<>(obj.getPoprId(), obj.getName());
      return newObj;
    }).collect(Collectors.toList());
    setField_PoprList(null, null, poprIdList);

    setField_ConsumerName(abd.getConsumerName(), true);
    setField_ConsumerAddress(abd.getConsumerAddress());
    setField_ConsumerPhone(abd.getConsumerPhone());
    setField_VehicleNo(abd.getVehicleNo());
    field_VehicleNo_OK = true;
    field_VehicleNo_ERROR.postValue(null);
    if (abd.getAppOperationId().equals(Utility.OPERATION_EDIT))
      validate_field_VehicleNo(abd.getVehicleNo(), abd.getBookingDate());

    setField_IsOpenClose(abd.getOpenClose());

    BigDecimal consAmtVal = abd.getConsumerAmt() == null ? BigDecimal.ZERO : abd.getConsumerAmt();
    BigDecimal fifAmtVal = abd.getFifAmt() == null ? BigDecimal.ZERO : abd.getFifAmt();
    BigDecimal bjAmtVal = abd.getBiroJasaAmt() == null ? BigDecimal.ZERO : abd.getBiroJasaAmt();
    setField_ConsumerAmount(consAmtVal);
    setField_FIFAmount(fifAmtVal);
    setField_BiroJasaAmount(bjAmtVal);
    field_ConsAmount_OLD = consAmtVal;
    field_FIFAmount_OLD = fifAmtVal;
    field_BiroJasaAmount_OLD = bjAmtVal;

    List<KeyValueModel<AppBookingBankAccountData,String>> kvmList = new ArrayList<>();
    KeyValueModel<AppBookingBankAccountData, String> transfer_Cons_BankId_Val = null;
    KeyValueModel<AppBookingBankAccountData, String> transfer_BJ_BankId_Val = null;
    KeyValueModel<AppBookingBankAccountData, String> transfer_FIF_BankId_Val = null;

    boolean isConsOtherBank = !Objects.equals(abd.getConsOtherBank(), null);
    boolean isConsManual =  Utility.MANUAL.equals(abd.getConsTransferType());
    isConsManual = isConsOtherBank && !isConsManual ? true : isConsManual;

    boolean isBjOtherBank = !Objects.equals(abd.getBjOtherBank(), null);
    boolean isBjManual = Utility.MANUAL.equals(abd.getBjTransferType());
    isBjManual = isBjOtherBank && !isBjManual ? true : isBjManual;

    boolean isFifOtherBank = !Objects.equals(abd.getFifOtherBank(), null);
    boolean isFifManual = Utility.MANUAL.equals(abd.getFifTransferType());
    isFifManual = isFifOtherBank && isFifManual ? true : isFifManual;

//    (!Objects.equals(altAccVal , null) && !isManualVal ? true : isManualVal)

    for (AppBookingBankAccountData ad : abd.getWfBankAccountDataList()) {
      KeyValueModel<AppBookingBankAccountData, String> kvm = new KeyValueModel<>(ad,
          ad.getBankName());
      if (ad.getId().equals(abd.getConsTransferSourceId()) &&
          (Objects.equals(ad.isManual(), isConsManual)))
        transfer_Cons_BankId_Val = kvm;
      if (ad.getId().equals(abd.getBjSourceId()) &&
          (Objects.equals(ad.isManual(), isBjManual)))
        transfer_BJ_BankId_Val = kvm;
      if (ad.getId().equals(abd.getFifSourceId()) &&
          (Objects.equals(ad.isManual(), isFifManual)))
        transfer_FIF_BankId_Val = kvm;
      kvmList.add(kvm);
    }
    setTxnBankAccountDataList(kvmList);
    setField_Wf_Txn_BankIdCons(transfer_Cons_BankId_Val);
    setField_Wf_Txn_DateCons(abd.getConsTransferDate());
    setField_Wf_Txn_IsManualCons(Utility.MANUAL.equals(abd.getConsTransferType()));

    setField_Wf_Txn_BankIdBj(transfer_BJ_BankId_Val);
    setField_Wf_Txn_DateBj(abd.getBjTransferDate());
    setField_Wf_Txn_IsManualBj(Utility.MANUAL.equals(abd.getBjTransferType()));

    setField_Wf_Txn_BankIdFIF(transfer_FIF_BankId_Val);
    setField_Wf_Txn_DateFIF(abd.getFifTransferDate());
    setField_Wf_Txn_IsManualFIF(Utility.MANUAL.equals(abd.getFifTransferType()));

    setField_BookingAmount();
    setField_FeeMatrix(abd.getMatrixFeeAmt() == null ? BigDecimal.ZERO : abd.getMatrixFeeAmt());
    HyperLog.d(TAG, "setField_FeeScheme abd.getSchemeFeeAmt()[" + abd.getSchemeFeeAmt() + "]");
    setField_FeeScheme(abd.getSchemeFeeAmt() == null ? BigDecimal.ZERO : abd.getSchemeFeeAmt());

    if (abd.getAtt_PO() != null) {
      HyperLog.d(TAG, fn + "ATT_PO isArchive[" + abd.getAtt_PO().isArchived() + "]");
      setField_Attach_PO_isArchive(abd.getAtt_PO().isArchived());

      if (!abd.getAtt_PO().isArchived())
        setAttachmentPO(abd.getAtt_PO().getId(), abd.getAtt_PO().getFileName(), abd.getPoNo(),
            abd.getPoDate(), poprIdList);
        else
      validation_field_PonoDateAttach(abd.getPoNo(), abd.getPoDate(),
          new File("fakeFileAgarTidakAdaPesanError"), poprIdList);
      //      setAttachment(AppBookingBrowseVM.ATTACHMENT_MODE_PO, abd.getAtt_PO().getId(),
      //          abd.getAtt_PO().getFileName());
    }else {
      setField_Attach_PO_isArchive(false);
    }

    if (abd.getAtt_STNK_KTP() != null) {
      HyperLog.d(TAG, fn + "Att_STNK_KTP isArchive[" + abd.getAtt_STNK_KTP().isArchived() + "]");
      setField_Attach_STNK_isArchive(abd.getAtt_STNK_KTP().isArchived());

      if (!abd.getAtt_STNK_KTP().isArchived())
        setAttachment(UtilityState.ATTCH_STNK.getId(), abd.getAtt_STNK_KTP().getId(),
            abd.getAtt_STNK_KTP().getFileName());
    } else {
      setField_Attach_STNK_isArchive(false);
    }

    if (abd.getAtt_KK() != null) {
      HyperLog.d(TAG, fn + "Att_KK isArchive[" + abd.getAtt_KK().isArchived() + "]");
      setField_Attach_KK_isArchive(abd.getAtt_KK().isArchived());

      if (!abd.getAtt_KK().isArchived())
        setAttachment(UtilityState.ATTCH_KK.getId(), abd.getAtt_KK().getId(),
            abd.getAtt_KK().getFileName());
    } else {
      setField_Attach_KK_isArchive(false);
    }

    if (abd.getAtt_KWITANSI() != null) {
      HyperLog.d(TAG, fn + "ATTCH_BPKB isArchive[" + abd.getAtt_KWITANSI().isArchived() + "]");
      setField_Attach_BPKB_isArchive(abd.getAtt_KWITANSI().isArchived());

      if (!abd.getAtt_KWITANSI().isArchived())
        setAttachment(UtilityState.ATTCH_BPKB.getId(), abd.getAtt_KWITANSI().getId(),
            abd.getAtt_KWITANSI().getFileName());
    } else {
      setField_Attach_BPKB_isArchive(false);
    }

    if (abd.getAtt_SPT() != null) {
      HyperLog.d(TAG, fn + "ATTCH_SPT isArchive[" + abd.getAtt_SPT().isArchived() + "]");
      setField_Attach_SPT_isArchive(abd.getAtt_SPT().isArchived());

      if (!abd.getAtt_SPT().isArchived())
        setAttachment(UtilityState.ATTCH_SPT.getId(), abd.getAtt_SPT().getId(),
            abd.getAtt_SPT().getFileName());
    } else {
      setField_Attach_SPT_isArchive(false);
    }

    if (abd.getAtt_SERAH_TERIMA_UANG() != null) {
      showBuktiRetur.postValue(true);
      HyperLog.d(TAG,
          fn + "ATTCH_SERAH_TERIMA isArchive[" + abd.getAtt_SERAH_TERIMA_UANG().isArchived() + "]");
      setField_Attach_SERAH_TERIMA_isArchive(abd.getAtt_SERAH_TERIMA_UANG().isArchived());

      if (!abd.getAtt_SERAH_TERIMA_UANG().isArchived())
        setAttachment(UtilityState.ATTCH_SERAHTERIMA.getId(),
            abd.getAtt_SERAH_TERIMA_UANG().getId(), abd.getAtt_SERAH_TERIMA_UANG().getFileName());
    } else {
      setField_Attach_SERAH_TERIMA_isArchive(false);
    }

    if (abd.getAtt_FISIK_MOTOR() != null){
      HyperLog.d(TAG,
          fn + "ATTCH_FISIK_MOTOR isArchive[" + abd.getAtt_FISIK_MOTOR().isArchived() + "]");
      setField_Attach_FISIK_MOTOR_isArchive(abd.getAtt_FISIK_MOTOR().isArchived());

      if (!abd.getAtt_FISIK_MOTOR().isArchived())
        setAttachment(UtilityState.ATTCH_FISIKMOTOR.getId(),
            abd.getAtt_FISIK_MOTOR().getId(), abd.getAtt_FISIK_MOTOR().getFileName());
    } else {
      setField_Attach_FISIK_MOTOR_isArchive(false);
    }

    if (abd.getAtt_HO() != null) {
      HyperLog.d(TAG, fn + "ATTCH_HO isArchive[" + abd.getAtt_HO().isArchived() + "]");
      setField_Attach_HO_isArchive(abd.getAtt_HO().isArchived());

      if (!abd.getAtt_HO().isArchived())
        setAttachment(UtilityState.ATTCH_HO.getId(), abd.getAtt_HO().getId(),
            abd.getAtt_HO().getFileName());
    } else {
      setField_Attach_HO_isArchive(false);
    }

    if (abd.getAtt_BUKTIRETUR() != null) {
      HyperLog.d(TAG,
          fn + "ATTCH_BUKTIRETUR isArchive[" + abd.getAtt_BUKTIRETUR().isArchived() + "]");
      setField_Attach_BUKTIRETUR_isArchive(abd.getAtt_BUKTIRETUR().isArchived());

      if (!abd.getAtt_BUKTIRETUR().isArchived())
        setAttachment(UtilityState.ATTCH_BUKTIRETUR.getId(),
            abd.getAtt_BUKTIRETUR().getId(), abd.getAtt_BUKTIRETUR().getFileName());
    } else {
      setField_Attach_BUKTIRETUR_isArchive(false);
    }

    setField_CancelReason(abd.getCancelReason(), true);

    //---------------------------------

    setField_ConsumerBankId(abd.getConsBankId(), abd.getConsBankName());
    setField_ConsumerBankName(abd.getConsBankName());
    setField_ConsumerOtherBank(abd.getConsOtherBank());
    setField_ConsumerAccountName(abd.getConsAccountName(), true);
    setField_TF_ConsumerNameWarn(abd.getConsAccountName(), abd.getConsumerName());
    setField_ConsumerAccountNo(abd.getConsAccountNo());
    setField_ConsValid(abd.isValidCons() == null ? false : abd.isValidCons());

    setField_FIFBankId(abd.getFifBankId(), abd.getFifBankName());
    setField_FIFBankName(abd.getFifBankName());
    setField_FIFOtherBank(abd.getFifOtherBank());
    setField_FIFAccountName(abd.getFifAccountName());
    setField_FIFAccountNo(abd.getFifAccountNo());
    setField_FIFValid(abd.isValidFIF() == null ? false : abd.isValidFIF());

    setField_BiroJasaBankId(abd.getBjBankId(), abd.getBjBankName());
    setField_BiroJasaBankName(abd.getBjBankName());
    setField_BiroJasaOtherBank(abd.getBjOtherBank());
    setField_BiroJasaAccountName(abd.getBjAccountName());
    setField_BiroJasaAccountNo(abd.getBjAccountNo());
    setField_BjValid(abd.isValidBj() == null ? false : abd.isValidBj());

    setField_TF_ConsumerNameWarn(abd.getConsAccountName(), abd.getConsumerName());
    isLoadProcess = false;

    setReadOnly_AppNoPoNoPoDatePoAttchment2(null, screenMode, abd.getAppNo(), abd.getPoNo(),
        abd.getPoDate(), abd.getAtt_PO() != null);
    setFormState(AppBookingFormState.ready(res.getMessage(), false, null, screenMode));
    setTakeActionScreenMode(screenMode, ptdList);
  }


  protected AppBookingTransferScreenMode getScreenMode(List<ProcTaskData> ptdList, String opId,
      String wfStatus) {
    AppBookingTransferScreenMode mode = null;
    if (ptdList.size() > 0) {
      for (ProcTaskData ptd : ptdList) {
        String ptActKey = ptd.getActTaskDefinitionKey();
        mode = AppBookingTransferScreenMode.getScreenModeWithTaskKey(ptActKey);
      }
    } else {
      mode = AppBookingTransferScreenMode.getScreenMode(false, opId,
          wfStatus);
    }
    return mode;
  }

  //TODO CARA SEMENTARA, UNTUK MENENTUKAN KAPAN MUNCUL TIDAKNYA  TOMBOL TAKE ACTION
  private List<String> consumerTaskVerificationNewActKey = new ArrayList<>();
  private List<String> consumerTaskVerificationEditActKey = new ArrayList<>();
  private List<String> jasaTaskVerificationNewActKey = new ArrayList<>();
  private List<String> jasaTaskVerificationEditActKey = new ArrayList<>();
  private List<String> fifTaskVerificationNewActKey = new ArrayList<>();
  private List<String> fifTaskVerificationEditActKey = new ArrayList<>();

  //TODO CARA SEMENTARA, UNTUK MENENTUKAN KAPAN MUNCUL TIDAKNYA  TOMBOL TAKE ACTION
  private void initActKeyList() {
    consumerTaskVerificationEditActKey.add("UT_AocCustomerVerification_EDIT");
    consumerTaskVerificationEditActKey.add("UT_CaptainCustomerVerification_EDIT");
    consumerTaskVerificationEditActKey.add("UT_DirectorCustomerVerification_EDIT");
    consumerTaskVerificationEditActKey.add("UT_FinanceReturnCustomerVerification_EDIT");
    consumerTaskVerificationEditActKey.add("UT_FinanceStaffCustomerVerification_EDIT");
    consumerTaskVerificationEditActKey.add("UT_FinanceSpvCustomerVerification_EDIT");

    consumerTaskVerificationNewActKey.add("UT_AocCustomerVerification_NEW");
    consumerTaskVerificationNewActKey.add("UT_CaptainCustomerVerification_NEW");
    consumerTaskVerificationNewActKey.add("UT_FinanceStaffCustomerVerification_NEW");
    consumerTaskVerificationNewActKey.add("UT_FinanceSpvCustomerVerification_NEW");

    jasaTaskVerificationEditActKey.add("UT_AocJasaVerification_EDIT");
    jasaTaskVerificationEditActKey.add("UT_CaptainJasaVerification_EDIT");
    jasaTaskVerificationEditActKey.add("UT_DirectorJasaVerification_EDIT");
    jasaTaskVerificationEditActKey.add("UT_FinanceReturnJasaVerification_EDIT");
    jasaTaskVerificationEditActKey.add("UT_FinanceStaffJasaVerification_EDIT");
    jasaTaskVerificationEditActKey.add("UT_FinanceSpvJasaVerification_EDIT");

    jasaTaskVerificationNewActKey.add("UT_AocJasaVerification_NEW");
    jasaTaskVerificationNewActKey.add("UT_CaptainJasaVerification_NEW");
    jasaTaskVerificationNewActKey.add("UT_FinanceStaffJasaVerification_NEW");
    jasaTaskVerificationNewActKey.add("UT_FinanceSpvJasaVerification_NEW");

    fifTaskVerificationEditActKey.add("UT_AocFifVerification_EDIT");
    fifTaskVerificationEditActKey.add("UT_CaptainFifVerification_EDIT");
    fifTaskVerificationEditActKey.add("UT_DirectorFifVerification_EDIT");
    fifTaskVerificationEditActKey.add("UT_FinanceReturnFifVerification_EDIT");
    fifTaskVerificationEditActKey.add("UT_FinanceStaffFifVerification_EDIT");
    fifTaskVerificationEditActKey.add("UT_FinanceSpvFifVerification_EDIT");

    fifTaskVerificationNewActKey.add("UT_AocFifVerification_NEW");
    fifTaskVerificationNewActKey.add("UT_CaptainFifVerification_NEW");
    fifTaskVerificationNewActKey.add("UT_FinanceStaffFifVerification_NEW");
    fifTaskVerificationNewActKey.add("UT_FinanceSpvFifVerification_NEW");

  }

  protected void setTakeActionScreenMode(AppBookingTransferScreenMode sm,
      List<ProcTaskData> ptdList) {

    if (sm.equals(AppBookingTransferScreenMode.TRANSACTIONS_MODE))
      initActKeyList();

    ProcTaskData consPtd = null;
    ProcTaskData bjPtd = null;
    ProcTaskData fifPtd = null;
    if (ptdList.size() > 0) {
      for (ProcTaskData ptd : ptdList) {
        String ptActKey = ptd.getActTaskDefinitionKey();
        if (consumerTaskVerificationNewActKey.contains(ptActKey)
            || consumerTaskVerificationEditActKey.contains(ptActKey)
            || "UT_KasirRevisionCustomer_EDIT".equals(ptActKey)
            || "UT_KasirRevisionCustomer_NEW".equals(ptActKey)) {
          consPtd = ptd;
          readOnly_AppNoPoNoPoDatePoAttchment.postValue(false);
        } else if (jasaTaskVerificationNewActKey.contains(ptActKey)
            || jasaTaskVerificationEditActKey.contains(ptActKey)
            || "UT_KasirRevisionJasa_EDIT".equals(ptActKey)
            || "UT_KasirRevisionJasa_NEW".equals(ptActKey)) {
          bjPtd = ptd;
          readOnly_AppNoPoNoPoDatePoAttchment.postValue(false);
        } else if (fifTaskVerificationNewActKey.contains(ptActKey)
            || fifTaskVerificationEditActKey.contains(ptActKey)
            || "UT_KasirRevisionFif_EDIT".equals(ptActKey)
            || "UT_KasirRevisionFif_NEW".equals(ptActKey)) {
          fifPtd = ptd;
          readOnly_AppNoPoNoPoDatePoAttchment.postValue(false);
        }
      }
    }
    setScreenModeForTxn(consPtd, TXN_CONSUMER);
    setScreenModeForTxn(bjPtd, TXN_BIRO_JASA);
    setScreenModeForTxn(fifPtd, TXN_FIF);
  }

  private void setScreenModeForTxn(ProcTaskData ptd, String mode) {
    String taskKey = ptd != null ? ptd.getActTaskDefinitionKey() : null;
    if (mode == TXN_CONSUMER) {
      setTxnScreenModeCons(AppBookingTransferScreenMode.getScreenModeForSubTransaction(
          taskKey));
      setPtCons(ptd);
    } else if (mode == TXN_BIRO_JASA) {
      setTxnScreenModeBj(AppBookingTransferScreenMode.getScreenModeForSubTransaction(
          taskKey));
      setPtBj(ptd);
    } else if (mode == TXN_FIF) {
      setTxnScreenModeFIF(AppBookingTransferScreenMode.getScreenModeForSubTransaction(
          taskKey));
      setPtFIF(ptd);
    }
  }

  @Override
  public void onPrepareAppBookingSuccess(Resource<NewBookingReponse> response) {
    setPrepareForm(response.getData());
    AppBookingTransferScreenMode screenMode = AppBookingTransferScreenMode.getScreenMode(true,
        Utility.OPERATION_NEW, null);
    setFormState(AppBookingFormState.ready(response.getMessage(), false, null, screenMode));
  }

  @Override
  public void processPrepareBookingForm() {
    setTransferFieldEnabled(true);
    prepareUC.prepare(true);
  }

  @Override
  public void processLoadAppBookingData(UUID id) {
    appBookingLoadUC.loadAppBookingData(id, true);
  }

  public class ValidateBankIdRes {
    private boolean showProgress;
    private boolean valid;
    private String message;
    private String errorMessage = null;

    public ValidateBankIdRes(boolean showProgress, boolean isValid, String message,
        String errorMessage) {
      this.showProgress = showProgress;
      this.valid = isValid;
      this.message = message;
      this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
      return errorMessage;
    }

    //    public void setErrorMessage(String errorMessage) {
    //      this.errorMessage = errorMessage;
    //    }

    public boolean getShowProgress() {
      return showProgress;
    }

    //    public void setShowProgress(Boolean showProgress) {
    //      this.showProgress = showProgress;
    //    }

    public boolean isValid() {
      return valid;
    }

    //    public void setValid(Boolean valid) {
    //      this.valid = valid;
    //    }

    public String getMessage() {
      return message;
    }

    //    public void setMessage(String message) {
    //      this.message = message;
    //    }

  }

  @Override
  protected void setReadOnly_AppNoPoNoPoDatePoAttchment2(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans, String appNo, String poNo, LocalDate poDate,
      boolean isAttchmentPoExist) {
    boolean doNextProcess = false;
    switch (smTrans) {
    case KASIR_SUBMITTED_NEW_APPBOOKING:
    case KASIR_SUBMITTED_EDIT_APPBOOKING:
      //    case KASIR_REVISION_NEW_APPBOOKING:
      //    case KASIR_REVISION_EDIT_APPBOOKING:
      readOnly_AppNoPoNoPoDatePoAttchment.postValue(false);
      doNextProcess = false;
      break;
    default:
      doNextProcess = true;
      break;
    }
    readOnly_AppNoPoNoPoDatePoAttchment.postValue(false);
    //    if (doNextProcess) {
    //      if (appNo != null && poNo != null && poDate != null
    //          && isAttchmentPoExist)
    //        readOnly_AppNoPoNoPoDatePoAttchment.postValue(true);
    //      else
    //        readOnly_AppNoPoNoPoDatePoAttchment.postValue(false);
    //    }
  }
}