package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.google.android.material.chip.Chip;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmds.data.common.LobShortData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.common.SoShortData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingVM;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;

public class AppBookingCashCancelFragment extends AppBookingCashNewFragment {
  private final String TAG = AppBookingCashCancelFragment.class.getSimpleName();
  private BookingBrowseViewModel mBrowseVM;

  public AppBookingCashCancelFragment() {
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    Abstract_AppBookingVM vm = new ViewModelProvider(this).get(AppBookingCashVM.class);
    AppBookingCashCancelFragmentArgs args = AppBookingCashCancelFragmentArgs
        .fromBundle(getArguments());
    mBrowseVM = new ViewModelProvider(requireActivity()).get(BookingBrowseViewModel.class);
    boolean isHasPermission =args.getIsHasCreatePermission();
    setOnCreateView(isHasPermission, inflater, container, this, TAG, vm);
    getCashVm().setField_BookingId(args.getBookingId());
    return getBinding().getRoot();
  }

  @Override
  protected void processLoadData() {
    getCashVm().processLoadAppBookingDataByBooking(getCashVm().getField_BookingId(), false, false);
  }

  @Override
  protected void setReadyFormState(AppBookingCashScreenMode smCash, AppBookingTransferScreenMode smTrans, String message,
      Animation inAnim, Animation outAnim, boolean isFromAction) {
    if (isFromAction) {
      mBrowseVM.setRefreshList(true);
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      navController.navigateUp();
      return;
    } else
      super.setReadyFormState(smCash, smTrans, message, inAnim,
          outAnim, isFromAction);
  }

  @Override
  protected void setMode_generalField(AppBookingCashScreenMode smCash, AppBookingTransferScreenMode smTransfer, String message,
      Animation inAnimation, Animation outAnimation) {
    super.setMode_generalField(smCash, smTransfer, message, inAnimation, outAnimation);
  }

  @Override
  protected void setField_Outlet() {
    outletsAdapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item);
    getBinding().spOutlet.setAdapter(outletsAdapter);
    getBinding().spOutlet.setFocusable(false);
    getCashVm().getField_outletKvm().observe(getViewLifecycleOwner(), oSelected -> {
      if (oSelected == null) {
        // TODO ?
      } else {
        List<KeyValueModel<OutletShortData, String>> kvmList = new ArrayList<>();
        kvmList.add(oSelected);
        outletsAdapter.addAll(kvmList);
        outletsAdapter.notifyDataSetChanged();
        getBinding().spOutlet.setSelection(0);
      }
    });
  }

  @Override
  protected void setField_ConsumerNo() {
    getCashVm().getField_ConsumerNo().observe(getViewLifecycleOwner(), data -> {
      getBinding().etConsumerNo.setText(data);
    });
  }

  @Override
  protected void setField_BookingDate() {
    // set data
    getCashVm().getField_BookingDate().observe(getViewLifecycleOwner(), bDate -> {
      if (bDate == null) {
        getBinding().etBookingDate.setText(null);
      } else if (!bDate.format(Formatter.DTF_dd_MM_yyyy)
          .equals(getBinding().etBookingDate.getText())) {
        getBinding().etBookingDate.setText(bDate.format(Formatter.DTF_dd_MM_yyyy));
      }
    });
  }

  @Override
  protected void setField_Lob() {
    lobAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    getBinding().spLOB.setAdapter(lobAdapter);
    getBinding().spLOB.setFocusable(false);
    getCashVm().getField_LobKvm().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        // TODO ?
      } else {
        List<KeyValueModel<LobShortData, String>> kvmList = new ArrayList<>();
        kvmList.add(data);
        lobAdapter.addAll(kvmList);
        lobAdapter.notifyDataSetChanged();
        getBinding().spLOB.setSelection(0);
      }
    });
  }

  @Override
  protected void setField_So() {
    soAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    getBinding().spSO.setAdapter(soAdapter);
    getBinding().spSO.setFocusable(false);
    getCashVm().getField_soKvm().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        // TODO ?
      } else {
        List<KeyValueModel<SoShortData, String>> kvmList = new ArrayList<>();
        kvmList.add(data);
        soAdapter.addAll(kvmList);
        soAdapter.notifyDataSetChanged();
        getBinding().spSO.setSelection(0);
      }
    });
  }

  @Override
  protected void setField_Idnpksf() {
    getCashVm().getField_Idnpksf().observe(getViewLifecycleOwner(), data -> {
      getBinding().etIdnpksf.setText(data);
    });
  }

  @Override
  protected void setField_AlasanPinjam() {
    getCashVm().getField_AlasanPinjam().observe(getViewLifecycleOwner(), data -> {
      getBinding().etReason.setText(data);
    });
  }

  @Override
  protected void setField_AlasanCash() {
    getCashVm().getField_AlasanPinjamCash().observe(getViewLifecycleOwner(), data -> {
      getBinding().etCashReason.setText(data);
    });
  }

  @Override
  protected void setField_AppNo() {
    getCashVm().getField_AppNo().observe(getViewLifecycleOwner(), data -> {
      getBinding().etAppNo.setText(data);
    });
  }

  @Override
  protected void setField_PoNo() {
    getCashVm().getField_PoNo().observe(getViewLifecycleOwner(), data -> {
      getBinding().etPoNo.setText(data);
    });
  }

  @Override
  protected void setField_PoDate() {
    getCashVm().getField_PoDate().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etPoDate.setText(null);
      } else if (!data.format(Formatter.DTF_dd_MM_yyyy)
          .equals(getBinding().etBookingDate.getText())) {
        getBinding().etPoDate.setText(data.format(Formatter.DTF_dd_MM_yyyy));
      }
    });
  }

  @Override
  protected void setPopReasonList() {
    getCashVm().getField_PoprList().observe(getViewLifecycleOwner(), dataList -> {
      getBinding().cgPendingPoReason.removeAllViews();
      int i = 0;
      for (KeyValueModel<UUID, String> data : dataList) {
        i++;
        Chip chip = (Chip) getThisInflater().inflate(R.layout.chip_filter, null);
        chip.setId(i);
        chip.setText(data.getValue());
        chip.setTag(data.getKey());
        chip.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            KeyValueModel<UUID, String> removePopr = new KeyValueModel(chip.getTag(),
                chip.getText());
            getCashVm().setField_PoprList(false, removePopr, null);
          }
        });
        getBinding().cgPendingPoReason.addView(chip);
      }
    });
  }

  @Override
  protected void setField_ConsumerName() {
    getCashVm().getField_ConsumerName().observe(getViewLifecycleOwner(), data -> {
      getBinding().etConsumerName.setText(data);
    });
  }

  @Override
  protected void setField_ConsumerAddress() {
    getCashVm().getField_ConsumerAddress().observe(getViewLifecycleOwner(), data -> {
      getBinding().etConsumerAddress.setText(data);
    });
  }

  @Override
  protected void setField_ConsumerPhone() {
    getCashVm().getField_ConsumerPhone().observe(getViewLifecycleOwner(), data -> {
      getBinding().etConsumerPhone.setText(data);
    });
  }

  @Override
  protected void setField_VehicleNo() {
    getCashVm().getField_VehicleNo().observe(getViewLifecycleOwner(), data -> {
      getBinding().etVehicleNo.setText(data);
    });
  }

  @Override
  protected void setField_OpenClose() {
    getCashVm().isField_isOpenClose().observe(getViewLifecycleOwner(), isOpen -> {
      if (isOpen == null) {
        getCashVm().setField_IsOpenClose(false);
      } else if (!isOpen.equals(getBinding().cbOpenClose.isChecked())) {
        getBinding().cbOpenClose.setChecked(isOpen);
      }
    });
  }

  @Override
  protected void setField_ConsumerAmount() {
    getCashVm().getField_ConsumerAmount().observe(getViewLifecycleOwner(), amt -> {
      getBinding().etPencairanKonsumen.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
    });
  }

  @Override
  protected void setField_FIFAmount() {
    getCashVm().getField_FIFAmount().observe(getViewLifecycleOwner(), amt -> {
      getBinding().etPencairanFIF.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
    });
  }

  @Override
  protected void setField_BiroJasaAmount() {
    getCashVm().getField_BiroJasaAmount().observe(getViewLifecycleOwner(), amt -> {
      getBinding().etBiroJasa.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
    });
  }

  @Override
  protected void setField_PencairanTOTAL() {
    getCashVm().getField_BookingAmount().observe(getViewLifecycleOwner(), amt -> {
      getBinding().etTotalPencairan.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
    });
  }

  @Override
  protected void setField_FeeMatrix() {
    getCashVm().getField_FeeMatrix().observe(getViewLifecycleOwner(), amt -> {
      getBinding().etFeeMatrix.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
    });
  }

  @Override
  protected void setField_FeeScheme() {
    getCashVm().getField_FeeScheme().observe(getViewLifecycleOwner(), amt -> {
      getBinding().etFeeScheme.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
    });
  }

  @Override
  protected void setField_CancelReason() {
    getCashVm().getField_CancelReason().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etCancelReason.setText(null);
      } else if (!data.equals(getBinding().etCancelReason.getText().toString())) {
        getBinding().etCancelReason.setText(data);
      }
    });

    // set listener
    getBinding().etCancelReason.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          getCashVm().setField_CancelReason(getBinding().etCancelReason.getText().toString(), false);
        else
          getCashVm().setField_CancelReason(null, false);
      }
    });

    // set Error message
    getCashVm().getField_CancelReason_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilCancelReason.setError(message);
    });
  }

  @Override
  protected void setStandartButtonListener(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer) {
    getBinding().btnSimpan.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getCashVm().processSaveDraftCancel(true);
      }
    });

    getBinding().btnSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getCashVm().processWfSubmitCancel(true);
      }
    });

    getBinding().btnWfHistory.setOnClickListener(v -> {
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      AppBookingCashCancelFragmentDirections.ActionWfHistory dir = AppBookingCashCancelFragmentDirections
          .actionWfHistory(WfType.APP_BOOKING,
              getCashVm().getField_AppBookingId());
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });
  }
}