package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferProcessResponse;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class TugasTransferWFGenerateOtpUC
    extends BaseObservable<TugasTransferWFGenerateOtpUC.Listener, Boolean> {
  private final String TAG = TugasTransferWfFormUC.class.getSimpleName();
  private final TugasTransferService service;
  private final Application app;
  private final AppExecutors appExecutors;
  String doneMsg;
  String failMsg;

  @Inject
  public TugasTransferWFGenerateOtpUC(Application app, TugasTransferService service,
      AppExecutors appExecutors) {
    super(app);
    this.app = app;
    this.service = service;
    this.appExecutors = appExecutors;
    doneMsg = this.app.getString(R.string.msg_request_otp);
    failMsg = this.app.getString(R.string.process_fail);
  }

  public void processGenerateOTP() {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processGenerateOTP();
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean result) {
        notifySuccess(doneMsg, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(failMsg + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<Boolean> result) {
    listener.onProcessGetOtpStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<Boolean> result) {
    listener.onProcessGetOtpSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<Boolean> result) {
    listener.onProcessGetOtpFailure(result);
  }

  public interface Listener {
    void onProcessGetOtpStarted(Resource<Boolean> loading);

    void onProcessGetOtpSuccess(Resource<Boolean> res);

    void onProcessGetOtpFailure(Resource<Boolean> res);
  }
}
