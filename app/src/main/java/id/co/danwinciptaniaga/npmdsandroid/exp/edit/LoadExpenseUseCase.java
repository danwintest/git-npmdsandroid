package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseDetailResponse;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseItemData;
import id.co.danwinciptaniaga.npmds.data.expense.NewExpenseResponse;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;

public class LoadExpenseUseCase extends
    BaseObservable<LoadExpenseUseCase.Listener, LoadExpenseUseCase.LoadExpenseResult> {
  public interface Listener {
    void onLoadExpenseStarted();

    void onLoadExpenseSuccess(Resource<LoadExpenseResult> result);

    void onLoadExpenseFailure(Resource<LoadExpenseResult> responseError);
  }

  public static class LoadExpenseResult {
    private ExpenseDetailResponse expenseDetailResponse;
    private NewExpenseResponse newExpenseResponse;
    private List<ExpenseItemData> expenseItemDataList;

    public LoadExpenseResult(
        ExpenseDetailResponse expenseDetailResponse,
        NewExpenseResponse newExpenseResponse,
        List<ExpenseItemData> expenseItemDataList) {
      this.expenseDetailResponse = expenseDetailResponse;
      this.newExpenseResponse = newExpenseResponse;
      this.expenseItemDataList = expenseItemDataList;
    }

    public ExpenseDetailResponse getExpenseDetailResponse() {
      return expenseDetailResponse;
    }

    public NewExpenseResponse getNewExpenseResponse() {
      return newExpenseResponse;
    }

    public List<ExpenseItemData> getExpenseItemDataList() {
      return expenseItemDataList;
    }
  }

  private final ExpenseService expenseService;
  private final AppExecutors appExecutors;

  @Inject
  public LoadExpenseUseCase(Application app, ExpenseService expenseService,
      AppExecutors appExecutors) {
    super(app);
    this.expenseService = expenseService;
    this.appExecutors = appExecutors;
  }

  public ListenableFuture<LoadExpenseResult> load(UUID expenseId) {
    notifyStart(null, null);

    // load info detail Expense
    ListenableFuture<ExpenseDetailResponse> edrLf = expenseService.loadExpense(expenseId);
    // data yg diperlukan form
    // TODO: nama NewExpenseReponse tidak tepat, karena isinya diperlukan untuk Form lain-lain, seperti Pengajuan Booking X
    ListenableFuture<NewExpenseResponse> nerLf = expenseService.getNewExpense();
    // daftar Expense Item yang diperlukan form
    ListenableFuture<List<ExpenseItemData>> eilLf = expenseService.getExpenseItemList();

    // kumpulkan hasil dari 3 call di atas
    ListenableFuture<LoadExpenseResult> allSuccessLf = Futures.whenAllSucceed(
        Arrays.asList(edrLf, nerLf, eilLf))
        .call(new Callable<LoadExpenseResult>() {
          @Override
          public LoadExpenseResult call() throws Exception {
            return new LoadExpenseResult(
                Futures.getDone(edrLf),
                Futures.getDone(nerLf),
                Futures.getDone(eilLf));
          }
        }, appExecutors.backgroundIO());

    Futures.addCallback(allSuccessLf, new FutureCallback<LoadExpenseResult>() {
      @Override
      public void onSuccess(@NullableDecl LoadExpenseResult result) {
        notifySuccess("Biaya Operasional berhasil dimuat", result);
      }

      @Override
      public void onFailure(Throwable t) {
        notifyFailure("Gagal memuat Biaya Operasional", null, t);
      }
    }, appExecutors.backgroundIO());
    return allSuccessLf;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<LoadExpenseResult> result) {
    listener.onLoadExpenseStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<LoadExpenseResult> result) {
    listener.onLoadExpenseSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<LoadExpenseResult> result) {
    listener.onLoadExpenseFailure(result);
  }
}
