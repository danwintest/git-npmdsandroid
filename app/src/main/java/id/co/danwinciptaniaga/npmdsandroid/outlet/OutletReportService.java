package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.util.UUID;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceListResponse;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceReportFilter;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceReportResponse;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletReportFilter;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletReportSort;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletStockByDayReportResponse;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OutletReportService {
  @GET("rest/s/outletBalance")
  ListenableFuture<OutletBalanceListResponse> getOutletBalanceList(@Query("page") Integer page,
      @Query("pageSize") Integer pageSize,
      @Query("filter") OutletReportFilter filter,
      @Query("sort") OutletReportSort sort);

  @GET("rest/s/outletBalanceReport/{id}")
  ListenableFuture<OutletBalanceReportResponse> getOutletBalanceReport(@Path("id") UUID outletId,
      @Query("filter") OutletBalanceReportFilter filter);

  @GET("rest/s/outletStockByDayReport/{id}")
  ListenableFuture<OutletStockByDayReportResponse> getOutletStockByDayReport(@Path("id") UUID outletId,
      @Query("filter") OutletBalanceReportFilter filter);
}
