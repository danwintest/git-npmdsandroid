package id.co.danwinciptaniaga.npmdsandroid.prefs;

import android.app.Application;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditViewModel;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;

public class ChangePasswordVM extends AndroidViewModel implements ChangePasswordUC.Listener {
  private static final String TAG = DroppingAdditionalDetailEditViewModel.class.getSimpleName();

  private final AppExecutors appExecutors;
  private final ChangePasswordUC ucChangePassword;

  private MutableLiveData<FormState> formStateLd = new MutableLiveData<>(null);
  private MutableLiveData<Resource> actionEvent = new SingleLiveEvent<>();

  @ViewModelInject
  public ChangePasswordVM(Application application, AppExecutors appExecutors,
      ChangePasswordUC changePasswordUC) {
    super(application);
    this.appExecutors = appExecutors;
    this.ucChangePassword = changePasswordUC;
    changePasswordUC.registerListener(this);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.ucChangePassword.unregisterListener(this);
  }

  public void setReady() {
    formStateLd.postValue(FormState.ready(null));
  }

  public LiveData<FormState> getFormStateLd() {
    return formStateLd;
  }

  public LiveData<Resource> getActionEvent() {
    return actionEvent;
  }

  @Override
  public void onChangePasswordStarted(Resource<Void> loading) {
    formStateLd.postValue(FormState.actionInProgress(loading.getMessage()));
  }

  @Override
  public void onChangePasswordSuccess(Resource<Void> response) {
    formStateLd.postValue(FormState.ready(response.getMessage()));
    // kirim SingleLiveEvent sukses
    actionEvent.postValue(Resource.Builder.success(response.getMessage(), null));
  }

  @Override
  public void onChangePasswordFailure(Resource<Void> response) {
    String message = Resource.getMessageFromError(response,
        getApplication().getString(R.string.error_contact_support));
    // form kembali ready
    formStateLd.postValue(FormState.ready(message));
    // kirim SingleLiveEvent error
    actionEvent.postValue(Resource.Builder.error(message));
  }

  public void changePassword(String currentPassword, String newPassword, String confirmPassword) {
    ucChangePassword.changePassword(currentPassword, newPassword, confirmPassword);
  }
}
