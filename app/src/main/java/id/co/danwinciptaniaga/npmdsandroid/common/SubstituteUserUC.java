package id.co.danwinciptaniaga.npmdsandroid.common;

import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.util.Log;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.ExtUserInfoData;
import retrofit2.HttpException;

public class SubstituteUserUC extends BaseObservable<SubstituteUserUC.Listener, ExtUserInfoData> {
  private static final String TAG = SubstituteUserUC.class.getSimpleName();

  public interface Listener {

    void onSubstituteUserStarted();

    void onSubstituteUserSuccess(Resource<ExtUserInfoData> response);

    void onSubstituteUserFailure(Resource<ExtUserInfoData> response);

  }

  private final CommonService commonService;

  private final AppExecutors appExecutors;

  @Inject
  public SubstituteUserUC(Application application, CommonService commonService,
      AppExecutors appExecutors) {
    super(application);
    this.commonService = commonService;
    this.appExecutors = appExecutors;
  }

  public ListenableFuture<ExtUserInfoData> substituteUser(UUID substituteUserId) {
    Log.i(TAG, "substituteUser to: " + substituteUserId);
    notifyStart(null, null);
    ListenableFuture<ExtUserInfoData> responseLf = commonService.substituteUser(substituteUserId);
    Futures.addCallback(responseLf, new FutureCallback<ExtUserInfoData>() {
      @Override
      public void onSuccess(@NullableDecl ExtUserInfoData result) {
        HyperLog.d(TAG,
            "SubstituteUser success: " + result == null && result.getId() != null ?
                null :
                result.getId().toString());
        notifySuccess("Substitusi User berhasil", result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(
              ((HttpException) t).response().errorBody());
        }
        notifyFailure(er == null ? "Substitusi User gagal" : null, er, t);
      }
    }, appExecutors.backgroundIO());
    return responseLf;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<ExtUserInfoData> result) {
    listener.onSubstituteUserStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<ExtUserInfoData> result) {
    listener.onSubstituteUserSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<ExtUserInfoData> result) {
    listener.onSubstituteUserFailure(result);
  }
}
