package id.co.danwinciptaniaga.npmdsandroid;

import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.security.ProtectedActivity;
import id.co.danwinciptaniaga.androcon.update.UpdateUtility;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity;

public abstract class BaseActivity extends ProtectedActivity {
  private final String TAG = getClass().getSimpleName();

  @Inject
  AndroconConfig androconConfig;

  private BroadcastReceiver br = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      HyperLog.i(TAG, "onReceive called: " + intent);
      if (UpdateUtility.ACTION_DOWNLOAD_COMPLETE.equals(intent.getAction())) {
        if (intent.getIntExtra(UpdateUtility.EXTRA_DOWNLOAD_STATUS, -1)
            == DownloadManager.STATUS_SUCCESSFUL) {
          UpdateUtility.triggerUpdate(getApplicationContext(), androconConfig.getApkFileName());
        } else {
          Toast.makeText(context, getString(R.string.msg_download_update_failed),
              Toast.LENGTH_LONG).show();
        }
      }
    }
  };

  @Override
  protected void onStart() {
    super.onStart();
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(UpdateUtility.ACTION_DOWNLOAD_COMPLETE);
    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
    lbm.registerReceiver(br, intentFilter);
  }

  @Override
  protected void onStop() {
    super.onStop();
    LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
    lbm.unregisterReceiver(br);
  }

  @NonNull
  @Override
  protected String getProtectionAccountType() {
    return App.ACCOUNT_TYPE;
  }

  @NonNull
  @Override
  protected String getProtectionAuthTokenType() {
    return App.AUTH_TOKEN_TYPE;
  }

  @Override
  @CallSuper
  protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Log.i(TAG, "onActivityResult: " + requestCode + " - " + resultCode);
    if (requestCode == AuthenticationActivity.REQ_GENERAL_AUTH) {
      if (resultCode == AuthenticationActivity.RESULT_OK) {
        HyperLog.i(TAG, "Relogin OK");
      } else {
        HyperLog.i(TAG, "Relogin Cancelled / Failed");
        finish();
      }
    }
  }

  /**
   * Penambahan method public logout karena proses ini dapat dipanggil jika fragment butuh untuk force logout
   * (jika proses OTP gagal dan mengharusnya apk untuk logout)
   */
  public void logout() {
    model.logout();
  }
}
