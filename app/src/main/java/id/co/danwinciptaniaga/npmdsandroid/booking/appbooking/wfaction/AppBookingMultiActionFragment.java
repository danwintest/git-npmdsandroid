package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.acra.ACRA;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypertrack.hyperlog.HyperLog;

import android.os.Bundle;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingMultipeSelectedData;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentAppBookingMultiActionBinding;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction.DroppingDailyTaskWfFragment;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfProcessParameter;

@AndroidEntryPoint
public class AppBookingMultiActionFragment extends Fragment {
  private final String TAG = AppBookingMultiActionFragment.class.getSimpleName();
  public static final String APP_BOOKING_MULTI_WF = "APP_BOOKING_MULTI_WF";
  private AppBookingMultiActionAdapter adapter;
  FragmentAppBookingMultiActionBinding binding;
  AppBookingMultiActionVM vm;
  private RecyclerView rv;

  public AppBookingMultiActionFragment() {
  }

  public static AppBookingMultiActionFragment newInstance(String param1, String param2) {
    AppBookingMultiActionFragment fragment = new AppBookingMultiActionFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentAppBookingMultiActionBinding.inflate(inflater, container, false);
    vm = new ViewModelProvider(this).get(AppBookingMultiActionVM.class);
    AppBookingMultiActionFragmentArgs args = AppBookingMultiActionFragmentArgs.fromBundle(
        getArguments());
    vm.setDataList(getDataList(args));
    vm.setDecision(args.getDecision());

    setAdapter();
    setWFButtonProcess();
    setFormStateObservable();
    return binding.getRoot();
  }

  private void setFormStateObservable() {
    vm.getFormState().observe(getViewLifecycleOwner(), state -> {
      switch (state.getState()) {
      case LOADING:
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
        break;
      case READY:
        Toast.makeText(getContext(), state.getMessage(), Toast.LENGTH_LONG).show();
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        NavController nc = NavHostFragment.findNavController(AppBookingMultiActionFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(APP_BOOKING_MULTI_WF, true);
        nc.popBackStack();
        break;
      case ERROR:
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        Toast.makeText(getContext(), state.getMessage(), Toast.LENGTH_LONG).show();
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        break;
      default:
        break;
      }
    });
  }

  private void setWFButtonProcess() {
    binding.llWorkflowButtonContainer.setVisibility(View.VISIBLE);
    List<String> dcList = new ArrayList<>();
    dcList.add(vm.getDecision());
    Utility.renderCustomWfButton(dcList, binding.llWorkflowButtonContainer,
        getLayoutInflater(), getActivity(), this::WfValidationProcess, this::handleWfAction, false);
  }

  private void setAdapter() {
    rv = binding.rvList;
    adapter = new AppBookingMultiActionAdapter(vm.getDataList());
    rv.setAdapter(adapter);
  }

  private List<AppBookingData> getDataList(AppBookingMultiActionFragmentArgs args) {
    Type dataType = new TypeToken<List<AppBookingData>>() {
    }.getType();
    String dataString = args.getDataList();
    List<AppBookingData> dataList = new Gson().fromJson(dataString, dataType);
    return dataList;
  }

  private void handleWfAction(WfProcessParameter paramObj) {

    String decision = paramObj.getDecision();
    String comment = paramObj.getComment();
    List<AppBookingData> datalist = adapter.getDataList();

    if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)) {
      vm.processMultiApproveCaptain(datalist, comment);
    } else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision)) {
      vm.processMultiReturnCaptain(datalist, comment);
    } else {
      String wf = String.format("wfOutCome[%s] ", decision);
      String msg = "Workflow" + wf + " Not Supported";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      HyperLog.w(TAG, msg);
      msg = TAG + " handleWfAction_Txn() " + msg;
      RuntimeException re = new RuntimeException(msg);
      ACRA.getErrorReporter().handleException(re);
    }

  }

  private Boolean WfValidationProcess(WfProcessParameter paramObj) {
    return true;
  }
}