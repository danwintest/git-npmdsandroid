package id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking;

import java.util.UUID;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentExpenseEditSelectBookingBinding;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseEditFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SelectBookingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
public class SelectBookingFragment extends ProtectedFragment {

  private static final String ARG_OUTLET_ID = "outletId"; // namanya sama dengan definisi di navigation

  private SelectBookingViewModel viewModel;
  private FragmentExpenseEditSelectBookingBinding binding;
  private RecyclerView recyclerView;
  private SelectBookingListAdapter adapter;

  public SelectBookingFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param outletId booking mediator valid akan difilter untuk outletId ini
   * @return A new instance of fragment SelectBookingFragment.
   */
  public static SelectBookingFragment newInstance(UUID outletId) {
    // sebetulnya saat ini tidak diketahui kapan method ini dipakai
    SelectBookingFragment fragment = new SelectBookingFragment();
    Bundle args = new Bundle();
    args.putSerializable(ARG_OUTLET_ID, outletId);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(this).get(SelectBookingViewModel.class);
    SelectBookingFragmentArgs args = SelectBookingFragmentArgs.fromBundle(getArguments());
    viewModel.setOutletId(args.getOutletId());
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    binding = FragmentExpenseEditSelectBookingBinding.inflate(inflater, container, false);

    recyclerView = (RecyclerView) binding.list;
    adapter = new SelectBookingListAdapter(
        new SelectBookingListAdapter.BookingDataRecyclerViewAdapterListener() {
          @Override
          public void onItemClicked(View view, BookingData bookingData, int position) {
            NavController navController = Navigation.findNavController(view);
            navController.getPreviousBackStackEntry()
                .getSavedStateHandle()
                .set(ExpenseEditFragment.SAVED_STATE_LD_BOOKING_DATA, bookingData);
            navController.popBackStack();
          }
        }, getActivity().getApplicationContext());
    // TODO: tampilkan status loading

    viewModel.getBookingList().observe(getViewLifecycleOwner(), pagingData -> {
      adapter.submitData(getLifecycle(), pagingData);
    });

    viewModel.getRefreshList().observe(getViewLifecycleOwner(), e -> {
      if (e != null && e) {
        adapter.refresh();
      }
    });

    binding.swipeRefresh.setOnRefreshListener(() -> {
      adapter.refresh();
      binding.swipeRefresh.setRefreshing(false);
    });

    return binding.getRoot();
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    if (status.getStatus() == Status.LOADING) {
      binding.tvStatus.setVisibility(View.VISIBLE);
      binding.tvStatus.setText(R.string.msg_please_wait);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.tvStatus.setText(status.getMessage());
      binding.tvStatus.setVisibility(View.GONE);

      recyclerView.setAdapter(adapter);
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }
}