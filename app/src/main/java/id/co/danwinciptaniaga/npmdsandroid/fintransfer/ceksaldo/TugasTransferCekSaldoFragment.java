package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.tiper.MaterialSpinner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.BankData;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentTugasTransferCekSaldoBinding;

@AndroidEntryPoint
public class TugasTransferCekSaldoFragment extends Fragment {
  private String TAG = TugasTransferCekSaldoFragment.class.getSimpleName();
  private FragmentTugasTransferCekSaldoBinding binding;
  private TugasTransferCekSaldoVM vm;
  private RecyclerView rv;
  private TugasTransferCekSaldoListAdapter adapter;
  private ArrayAdapter<KeyValueModel<UUID, String>> bankAdapter;
  private ArrayAdapter<KeyValueModel<UUID, String>> accAdapter;

  public TugasTransferCekSaldoFragment() {
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    binding = FragmentTugasTransferCekSaldoBinding.inflate(inflater, container, false);
    TugasTransferCekSaldoFragmentArgs args = TugasTransferCekSaldoFragmentArgs.fromBundle(
        getArguments());
    vm = new ViewModelProvider(this).get(TugasTransferCekSaldoVM.class);
    vm.setCompanyId(args.getCompanyId());
    vm.setCompanyCode(args.getCompanyCode());
    setFormState();
    vm.processLoadBankList();
    setBankField();
    setAccountField();
    setBtnCekSaldo();
    setErrorMsg();
    setRecylerView();
    return binding.getRoot();
  }

  private void setFormState(){
    vm.getFormState().observe(getViewLifecycleOwner(),state->{
      if (FormState.State.LOADING.equals(state.getState())) {
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
      } else if (FormState.State.READY.equals(state.getState())) {
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
      } else if (FormState.State.ERROR.equals(state.getState())) {
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), state.getMessage(), Toast.LENGTH_LONG).show();
      }
    });
  }

  private void setBankField() {
    bankAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    binding.spBank.setAdapter(bankAdapter);
    binding.spBank.setFocusable(false);
    vm.getBankList().observe(getViewLifecycleOwner(), bankList -> {
      if (bankList == null) {
        binding.spBank.setEnabled(false);
      } else {
        List<KeyValueModel<UUID, String>> kvmList = new ArrayList<>();
        for(BankData bankData : bankList){
          kvmList.add(new KeyValueModel<>(bankData.getId(), bankData.getName()));
        }
        bankAdapter.addAll(kvmList);

        bankAdapter.notifyDataSetChanged();
        if (kvmList.size() ==1 ) {
          binding.spBank.setSelection(0);
          binding.spBank.setEnabled(false);
        }
      }
    });

    // Set Listener
    binding.spBank.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
          int i, long l) {
        KeyValueModel<UUID, String> bankKvm = bankAdapter.getItem(i);
        vm.setSelectedBankId(bankKvm.getKey());
        vm.setSelectedAccId(null);
        vm.processLoadAccList();
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
       //
      }
    });

    // set error
    vm.getBankMsg().observe(getViewLifecycleOwner(), msg -> {
      binding.tilBank.setError(msg);
    });
  }

  private void setAccountField() {
    accAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    binding.spAcc.setAdapter(accAdapter);
    binding.spAcc.setFocusable(false);
    vm.getAccList().observe(getViewLifecycleOwner(), accList -> {
      List<KeyValueModel<UUID, String>> kvmList = new ArrayList<>();
      for (BankAccountData accData : accList) {
        kvmList.add(new KeyValueModel<>(accData.getId(), accData.getAccountName()));
      }
      accAdapter.addAll(kvmList);

      accAdapter.notifyDataSetChanged();
      binding.spAcc.setSelection(-1);
    });

    // Set Listener
    binding.spAcc.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
          int i, long l) {
        KeyValueModel<UUID, String> accKvm = accAdapter.getItem(i);
        vm.setSelectedAccId(accKvm.getKey());
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
        // mAbstractVM.setField_outletKVM(null);
      }
    });

    // set error
    vm.getAccountMsg().observe(getViewLifecycleOwner(), msg -> {
      binding.tilAcc.setError(msg);
    });
  }

  private void setBtnCekSaldo() {
    binding.btnCekSaldo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        vm.processLoadBalanceList(getContext());
      }
    });
  }

  private void setRecylerView() {
    rv = binding.rv;
    adapter = new TugasTransferCekSaldoListAdapter(getContext());
    rv.setAdapter(adapter);
    vm.getBadList().observe(getViewLifecycleOwner(), dataList -> {
      adapter.setDataList(dataList);
      adapter.setCompanyCode(vm.getCompanyCode());
      adapter.notifyDataSetChanged();
    });
  }

  private void setErrorMsg() {
    vm.getFailedMsg().observe(getViewLifecycleOwner(), msg -> {
      binding.tvErrorMsg.setText(msg);
      if (msg != null) {
        binding.ivErrorMsg.setVisibility(View.VISIBLE);
        binding.tvErrorMsg.setVisibility(View.VISIBLE);
      } else {
        binding.ivErrorMsg.setVisibility(View.GONE);
        binding.tvErrorMsg.setVisibility(View.GONE);
      }
    });
  }

}