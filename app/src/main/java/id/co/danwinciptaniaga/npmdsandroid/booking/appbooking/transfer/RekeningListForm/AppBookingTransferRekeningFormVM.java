package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBankListResponse;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class AppBookingTransferRekeningFormVM extends AndroidViewModel {
  private final String TAG = AppBookingTransferRekeningFormVM.class.getSimpleName();
  private MutableLiveData<ValidateBankIdRes> validateBankAccountProgress = new MutableLiveData<>();
  public final AppBookingService appBookingService;
  private UUID appBookingId = null;
  private UUID bookingId = null;
  private MutableLiveData<UUID> companyId = new MutableLiveData<>();
  private UUID bankId = null;
  private MutableLiveData<KeyValueModel<BankAccountData, String>> bankObj = new MutableLiveData<>();
  private MutableLiveData<String> otherBankName = new MutableLiveData<>();
  private MutableLiveData<String> accName = new MutableLiveData<>();
  private MutableLiveData<String> accNo = new MutableLiveData<>();
  private String mode = null;
  private UUID outletId = null;
  private MutableLiveData<String> consumerName = new MutableLiveData<>();
  private Boolean isFromBooking = false;
  private MutableLiveData<Boolean> field_isValid = new MutableLiveData<>();
  public AppExecutors appExecutors;
  private MutableLiveData<List<KeyValueModel<BankAccountData, String>>> bankObjList = new MutableLiveData<>();

  @ViewModelInject
  public AppBookingTransferRekeningFormVM(@NonNull Application application,
      AppExecutors appExecutors, AppBookingService appBookingService) {
    super(application);
    this.appExecutors = appExecutors;
    this.appBookingService = appBookingService;
  }

  public MutableLiveData<KeyValueModel<BankAccountData, String>> getBankObj() {
    return bankObj;
  }

  public void setBankObj(KeyValueModel<BankAccountData, String> value) {
    HyperLog.d(TAG,
        "setBankObj terpanggil existingBankObj[" + bankObj.getValue() + "], value[" + value
            + "] isBankObj[" + (bankObj.getValue() != value) + "]");
    if (bankObj.getValue() != value) {
      this.bankObj.postValue(value);
      HyperLog.d(TAG, "setBankObj POST OBJECTNYA");
    }
  }

  public MutableLiveData<String> getConsumerName() {
    return consumerName;
  }

  public void setConsumerName(String consumerName) {
    this.consumerName.postValue(consumerName);
  }

  public UUID getBankId() {
    return bankId;
  }

  public void setBankId(UUID bankId) {
    this.bankId = bankId;
  }

  public MutableLiveData<String> getOtherBankName() {
    return otherBankName;
  }

  public void setOtherBankName(String value) {
    if (otherBankName.getValue() != value)
      this.otherBankName.postValue(value);
  }

  public MutableLiveData<String> getAccName() {
    return accName;
  }

  public void setAccName(String value) {
    if (accName.getValue() != value)
      this.accName.postValue(value);
  }

  public MutableLiveData<String> getAccNo() {
    return accNo;
  }

  public void setAccNo(String value) {
    if (accNo.getValue() != value)
      this.accNo.postValue(value);
  }

  public void getBankList() {
    ValidateBankIdRes data = new ValidateBankIdRes(true,
        null, getApplication().getString(R.string.silahkan_tunggu), null);
    setValidateBankAccountProgress(data);
    Log.d(TAG, "getBankList: terpanggil");
    ListenableFuture<AppBookingBankListResponse> service = appBookingService.getBankList();
    Futures.addCallback(service, new FutureCallback<AppBookingBankListResponse>() {
      @Override
      public void onSuccess(@NullableDecl AppBookingBankListResponse result) {
        ValidateBankIdRes data = new ValidateBankIdRes(false, null,
            null, null);
        setBankObjList(result.getBankAccountDataList());
        setValidateBankAccountProgress(data);
        Log.d(TAG, "getBankList: [BERHASIL]");
      }

      @Override
      public void onFailure(Throwable t) {
        String errorMessage = Utility.getErrorMessageFromThrowable(TAG, t);
        ValidateBankIdRes data = new ValidateBankIdRes(true,
            null, null, errorMessage);
        setValidateBankAccountProgress(data);
        //        setGeneralErrorMessage(errorMessage);
      }
    }, appExecutors.networkIO());
  }

  public void getBankListFromValidAcc(UUID bankId) {
    ValidateBankIdRes data = new ValidateBankIdRes(true,
        null, getApplication().getString(R.string.silahkan_tunggu), null);
    setValidateBankAccountProgress(data);
    HyperLog.d(TAG, "getBankListFromValidAccno terpanggil");
    ListenableFuture<AppBookingBankListResponse> service = appBookingService.getBankList();
    Futures.addCallback(service, new FutureCallback<AppBookingBankListResponse>() {
      @Override
      public void onSuccess(@NullableDecl AppBookingBankListResponse result) {
        ValidateBankIdRes data = new ValidateBankIdRes(false, true,
            null, null);
        setBankObjListFromValidAcc(result.getBankAccountDataList(), bankId);
        setValidateBankAccountProgress(data);
        HyperLog.d(TAG, "getBankList: [BERHASIL]");
      }

      @Override
      public void onFailure(Throwable t) {
        String errorMessage = Utility.getErrorMessageFromThrowable(TAG, t);
        ValidateBankIdRes data = new ValidateBankIdRes(true,
            null, null, errorMessage);
        setValidateBankAccountProgress(data);
        //        setGeneralErrorMessage(errorMessage);
      }
    }, appExecutors.networkIO());
  }

  public MutableLiveData<List<KeyValueModel<BankAccountData, String>>> getBankObjList() {
    return bankObjList;
  }

  public void setBankObjListFromValidAcc(List<BankAccountData> badList, UUID selectedValidAcc) {
    HyperLog.d(TAG, "setBankObjListFromValidAcc terpanggil");
    List<KeyValueModel<BankAccountData, String>> kvmList = new ArrayList<>();
    for (BankAccountData bad : badList) {
      KeyValueModel<BankAccountData, String> kvmObj = new KeyValueModel(bad, bad.getBankName());
      HyperLog.d(TAG, "setBankObjListFromValidAcc badId[" + bad.getBankId() + "] selectedId["
          + selectedValidAcc + "]");
      if (bad.getBankId().equals(selectedValidAcc)) {
        setBankObj(kvmObj);
      }
      kvmList.add(kvmObj);
    }
    bankObjList.postValue(kvmList);
  }

  public void setBankObjList(List<BankAccountData> badList) {
    List<KeyValueModel<BankAccountData, String>> kvmList = new ArrayList<>();
    for (BankAccountData bad : badList) {
      KeyValueModel<BankAccountData, String> kvmObj = new KeyValueModel(bad, bad.getBankName());
      if (bad.getBankId().equals(getBankId())) {
        setBankObj(kvmObj);
      }
      kvmList.add(kvmObj);
    }
    bankObjList.postValue(kvmList);
  }

  public MutableLiveData<Boolean> getField_IsValid() {
    return field_isValid;
  }

  public void setField_IsValid(Boolean isValid) {
    field_isValid.postValue(isValid);
  }

  public void setAppBookingId(UUID appBookingId) {
    this.appBookingId = appBookingId;
  }

  public void setBookingId(UUID bookingId) {
    this.bookingId = bookingId;
  }

  public void setFromBooking(Boolean fromBooking) {
    isFromBooking = fromBooking;
  }

  public String getMode() {
    return mode;
  }

  public void setMode(String mode) {
    this.mode = mode;
  }

  public UUID getOutletId() {
    return outletId;
  }

  public void setOutletId(UUID outletId) {
    this.outletId = outletId;
  }

  public void setCompanyId(UUID companyId) {
    this.companyId.postValue(companyId);
  }

  public void validateBankAccount() {
    String logMsg =
        "VALIDATE validateBankAccount: bankId[" + getBankObj().getValue().getKey().getBankId()
            + "] companyId["
            + companyId + "] txnType[" + mode + "] accNo[" + getAccNo().getValue() + "] accName["
            + getAccName().getValue() + "]";

    ValidateBankIdRes res = new ValidateBankIdRes(true, false, null, null);
    setValidateBankAccountProgress(res);

    // process valid
    ListenableFuture<Boolean> isValid_Lf = appBookingService.validateAppBookingBankAccount(
        appBookingId, bookingId, mode, getBankObj().getValue().getKey().getBankId(), isFromBooking,
        companyId.getValue(), getAccNo().getValue(), getAccName().getValue());
    Log.d(TAG, logMsg);

    Futures.addCallback(isValid_Lf, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean result) {
        String message = " - ";
        if (result)
          message = getApplication().getString(R.string.validation_bankaccount_valid_message);
        else
          message = getApplication().getString(R.string.validation_bankaccount_invalid_message);

        ValidateBankIdRes res = new ValidateBankIdRes(false, result, message, null);
        setValidateBankAccountProgress(res);
        setValidityTxnBankAccountByTxnType(mode, result);
        Log.d(TAG, logMsg + " " + message);
      }

      @Override
      public void onFailure(Throwable t) {
        String errorMessage = Utility.getErrorMessageFromThrowable(TAG, t);
        String message = getApplication().getString(
            R.string.validation_bankaccount_invalid_message);
        ValidateBankIdRes res = new ValidateBankIdRes(false, false, message, errorMessage);
        //        ValidateBankIdRes res = new ValidateBankIdRes(false, true, message, errorMessage);
        setValidateBankAccountProgress(res);
        setValidityTxnBankAccountByTxnType(mode, false);
      }
    }, appExecutors.networkIO());
  }

  public MutableLiveData<ValidateBankIdRes> getValidateBankAccountProgress() {
    return validateBankAccountProgress;
  }

  public void setValidateBankAccountProgress(ValidateBankIdRes data) {
    validateBankAccountProgress.postValue(data);
  }

  public class ValidateBankIdRes {
    private boolean showProgress;
    private Boolean valid;
    private String message;
    private String errorMessage = null;

    public ValidateBankIdRes(boolean showProgress, Boolean isValid, String message,
        String errorMessage) {
      this.showProgress = showProgress;
      this.valid = isValid;
      this.message = message;
      this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
      return errorMessage;
    }

    public boolean getShowProgress() {
      return showProgress;
    }

    public Boolean isValid() {
      return valid;
    }

    public String getMessage() {
      return message;
    }
  }

  private void setValidityTxnBankAccountByTxnType(String txnType, boolean isValid) {
    setField_IsValid(isValid);
  }
}
