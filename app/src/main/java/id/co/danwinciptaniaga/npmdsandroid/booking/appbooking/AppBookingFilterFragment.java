package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.time.LocalDate;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentAppBookingFilterBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

@AndroidEntryPoint
public class AppBookingFilterFragment extends BottomSheetDialogFragment {
  private final String TAG = this.getClass().getSimpleName();
  public static final String FILTER_FIELD = "FILTER_FIELD";
  private FragmentAppBookingFilterBinding binding;
  private AppBookingFilterVM vm;
  private ArrayAdapter<KeyValueModel<String, String>> operationAdapter;
  private ArrayAdapter<KeyValueModel<String, String>> bookingTypeAdapter;
  private ArrayAdapter<KeyValueModel<UUID, String>> companyAdapter;
  private ArrayAdapter<KeyValueModel<String, String>> statusAdapter;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    binding = FragmentAppBookingFilterBinding.inflate(inflater);
    HyperLog.d(TAG,"onCreateView set FilterField [MULAI]");
    vm = new ViewModelProvider(this).get(AppBookingFilterVM.class);
    AppBookingFilterFragmentArgs args = AppBookingFilterFragmentArgs.fromBundle(getArguments());
    onReady();
    vm.setFilterField(args.getFilterField());
    vm.loadFilter();
    HyperLog.d(TAG,"onCreateView set FilterField [SELESAI]");
    return binding.getRoot();
  }

  @Override
  public void onResume() {
    super.onResume();
    setFilterCompany();
    setFilterDocType();
    setFilterFromDate();
    setFilterStatus(getLayoutInflater());
    setFilterDateTo();
    setFilterConsumerName();
    setFilterButton();
    setFilterOperation();
    setFilterVehicleNo();
    setFilterTrxNo();
  }

  private void onReady() {
    vm.getFormState().observe(getViewLifecycleOwner(), state -> {
      switch (state.getState()) {
      case LOADING:
        showHideProgressLoading(true, state.getMessage());
        break;
      case READY:
        showHideProgressLoading(false, state.getMessage());
        break;
      case ERROR:
        showHideProgressLoading(true, state.getMessage());
        break;
      }
    });
  }

  private void showHideProgressLoading(boolean isShow, String message) {
    Animation outAnim = new AlphaAnimation(1f, 0f);
    outAnim.setDuration(200);
    AlphaAnimation inAnim = new AlphaAnimation(0f, 1f);
    inAnim.setDuration(200);

    if (message != null)
      Toast.makeText(getContext(), message,Toast.LENGTH_LONG).show();
    if (isShow && message != null)
      binding.progressWrapper.progressText.setText(message);
    binding.progressWrapper.progressView.setAnimation(isShow ? inAnim : outAnim);
    binding.progressWrapper.progressView.setVisibility(isShow ? View.VISIBLE : View.GONE);
//    binding.pageContent.setVisibility(isShow ? View.GONE : View.VISIBLE);
    // TODO niat nya ingin enable dan disable form jika progress wrapper muncul, tapi masih gagal
//    binding.pageContent.setEnabled(!isShow);
  }

  private void setFilterCompany(){
    vm.getCompanyKvmList().observe(getViewLifecycleOwner(),cList->{
      companyAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spCompany.setAdapter(companyAdapter);
      binding.spCompany.setFocusable(false);
      companyAdapter.addAll(cList);
      companyAdapter.notifyDataSetChanged();
      int pos = companyAdapter.getPosition(vm.getCompanyKvm());
      binding.spCompany.setSelection(pos);
    });

    binding.spCompany.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view,
          int i, long l) {
        KeyValueModel<UUID, String> kvm = companyAdapter.getItem(i);
        vm.setCompanyKvm(kvm);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
        vm.setCompanyKvm(null);
      }
    });
  }

  private void setFilterDocType() {
    bookingTypeAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    binding.spBookingType.setFocusable(false);
    binding.spBookingType.setAdapter(bookingTypeAdapter);
    bookingTypeAdapter.addAll(vm.getBookingTypeKvmList());
    bookingTypeAdapter.notifyDataSetChanged();

    vm.getBookingTypeKvm().observe(getViewLifecycleOwner(), type->{
      int pos = bookingTypeAdapter.getPosition(type);
      binding.spBookingType.setSelection(pos);
    });

    binding.spBookingType.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
          @org.jetbrains.annotations.Nullable View view,
          int i, long l) {
        KeyValueModel<String, String> dtKvm = bookingTypeAdapter.getItem(i);
        vm.setBookingTypeKvm(dtKvm,null);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
      }
    });
  }

  private void setFilterOperation() {
    operationAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    binding.spOperation.setFocusable(false);
    binding.spOperation.setAdapter(operationAdapter);
    operationAdapter.addAll(vm.getOperationKvmList());
    operationAdapter.notifyDataSetChanged();

    vm.getOperationKvm().observe(getViewLifecycleOwner(), op->{
      int pos = operationAdapter.getPosition(op);
      binding.spOperation.setSelection(pos);
    });

    binding.spOperation.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
          @org.jetbrains.annotations.Nullable View view,
          int i, long l) {
        KeyValueModel<String, String> kvmObj = operationAdapter.getItem(i);
        vm.setOperationKvm(kvmObj, null);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
      }
    });
  }

  private void setFilterFromDate() {
    vm.getFromDate().observe(getViewLifecycleOwner(), date -> {
      if (date == null) {
        binding.etDateFrom.setText(null);
      } else if (!date.format(Formatter.DTF_dd_MM_yyyy)
          .equals(binding.etDateFrom.getText().toString())) {
        binding.etDateFrom.setText(date.format(Formatter.DTF_dd_MM_yyyy));
      }

      binding.etDateFrom.setOnClickListener(v -> {
        LocalDate toDate =
            date != null ? date : LocalDate.now();
        int day = toDate.getDayOfMonth();
        int month = toDate.getMonthValue() - 1;
        int year = toDate.getYear();
        DatePickerDialog dateDialog = new DatePickerDialog(getActivity(),
            new DatePickerDialog.OnDateSetListener() {
              @Override
              public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                LocalDate date = LocalDate.of(year, month + 1, dayOfMonth);
                vm.setFromDate(date);
                binding.etDateFrom.setText(date.format(Formatter.DTF_dd_MM_yyyy));
              }
            }, year, month, day);
        // TODO PENAMBAHAN FUNGSI SEMENTARA, HARAP DIPERBAIKI MENGGUNAKA CLASS DatePickerUIHelper nanti
        dateDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
          @Override
          public void onCancel(DialogInterface dialog) {
            vm.setFromDate(null);
            binding.etDateFrom.setText(null);
          }
        });
        dateDialog.show();
      });
    });
  }

  private void setFilterDateTo() {
    vm.getToDate().observe(getViewLifecycleOwner(), date -> {
      if (date == null) {
        binding.etDateTo.setText(null);
      } else if (!date.format(Formatter.DTF_dd_MM_yyyy).equals(
          binding.etDateTo.getText().toString())) {
        binding.etDateTo.setText(date.format(Formatter.DTF_dd_MM_yyyy));
      }

      binding.etDateTo.setOnClickListener(v -> {
        LocalDate toDate = date != null ? date : LocalDate.now();
        int day = toDate.getDayOfMonth();
        int month = toDate.getMonthValue() - 1;
        int year = toDate.getYear();
        DatePickerDialog dateDialog = new DatePickerDialog(getActivity(),
            new DatePickerDialog.OnDateSetListener() {
              @Override
              public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                LocalDate date = LocalDate.of(year, month + 1, dayOfMonth);
                vm.setToDate(date);
                binding.etDateTo.setText(date.format(Formatter.DTF_dd_MM_yyyy));
              }
            }, year, month, day);
        // TODO PENAMBAHAN FUNGSI SEMENTARA, HARAP DIPERBAIKI MENGGUNAKA CLASS DatePickerUIHelper nanti
        dateDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
          @Override
          public void onCancel(DialogInterface dialog) {
            vm.setToDate(null);
            binding.etDateTo.setText(null);
          }
        });
        dateDialog.show();
      });
    });
  }

  private void setFilterStatus(@NonNull LayoutInflater inflater) {
    vm.getStatusKvmList().observe(getViewLifecycleOwner(), dataList -> {
      statusAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      statusAdapter.addAll(dataList);
      binding.acStatusWf.setAdapter(statusAdapter);
      binding.acStatusWf.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
          KeyValueModel<String, String> statusKvm = (KeyValueModel<String, String>) parent.getItemAtPosition(
              position);
          vm.setSelectedStatus(true, statusKvm);
          binding.acStatusWf.setText(null);
        }
      });
    });

    vm.getSelectedStatusKvmList().observe(getViewLifecycleOwner(), dataList -> {
      binding.cgStatusWf.removeAllViews();
      for (int i = 0; i < dataList.size(); i++) {
        KeyValueModel<String, String> kvm = dataList.get(i);
        Chip chip = (Chip) inflater.inflate(R.layout.chip_filter, null);
        chip.setId(i);
        chip.setText(kvm.getValue());
        chip.setTag(kvm.getKey());
        chip.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            KeyValueModel<String, String> selectedItem = new KeyValueModel(chip.getTag(),
                chip.getText());
            vm.setSelectedStatus(false, selectedItem);
          }
        });
        binding.cgStatusWf.addView(chip);
      }
    });
  }

  private void setFilterConsumerName() {
    vm.getConsumerName().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        binding.etConsumerName.setText(null);
      } else if (!data.equals( binding.etConsumerName.getText().toString())) {
        binding.etConsumerName.setText(data);
      }
    });

    // set listener
    binding.etConsumerName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          vm.setConsumerName(s.toString());
      }
    });
  }

  private void setFilterVehicleNo() {
    vm.getVehicleNo().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        binding.etVehicleNo.setText(null);
      } else if (!data.equals( binding.etVehicleNo.getText().toString())) {
        binding.etVehicleNo.setText(data);
      }
    });

    // set listener
    binding.etVehicleNo.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          vm.setVehicleNo(s.toString());
      }
    });
  }

  private void setFilterTrxNo() {
    vm.getTrxNo().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        binding.etTrxNo.setText(null);
      } else if (!data.equals( binding.etTrxNo.getText().toString())) {
        binding.etTrxNo.setText(data);
      }
    });

    // set listener
    binding.etTrxNo.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          vm.setTrxNo(s.toString());
      }
    });
  }

  private void setFilterButton() {
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        NavController nc = NavHostFragment.findNavController(AppBookingFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(FILTER_FIELD, vm.getFilterField());
        nc.popBackStack();
      }
    });

    binding.btnClearAll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        NavController nc = NavHostFragment.findNavController(AppBookingFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(FILTER_FIELD,
            new AppBookingBrowseFilter());
        nc.popBackStack();
      }
    });
  }

}
