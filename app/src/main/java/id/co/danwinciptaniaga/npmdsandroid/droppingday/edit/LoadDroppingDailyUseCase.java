package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyDataResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.NewDroppingDailyResponse;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyService;
import retrofit2.HttpException;

public class LoadDroppingDailyUseCase extends
    BaseObservable<LoadDroppingDailyUseCase.Listener, LoadDroppingDailyUseCase.LoadDroppingDailyResult> {

  public interface Listener {
    void onLoadDroppingDailyStarted();

    void onLoadDroppingDailySuccess(Resource<LoadDroppingDailyResult> result);

    void onLoadDroppingDailyFailure(Resource<LoadDroppingDailyResult> responseError);
  }

  public static class LoadDroppingDailyResult {
    private DroppingDailyDataResponse droppingDailyDataResponse;
    private NewDroppingDailyResponse newDroppingDailyResponse;

    public LoadDroppingDailyResult(
        DroppingDailyDataResponse droppingDailyDataResponse,
        NewDroppingDailyResponse newDroppingDailyResponse) {
      this.droppingDailyDataResponse = droppingDailyDataResponse;
      this.newDroppingDailyResponse = newDroppingDailyResponse;
    }

    public DroppingDailyDataResponse getDroppingDailyDataResponse() {
      return droppingDailyDataResponse;
    }

    public NewDroppingDailyResponse getNewDroppingDailyResponse() {
      return newDroppingDailyResponse;
    }
  }

  private final DroppingDailyService droppingDailyService;
  private final AppExecutors appExecutors;

  @Inject
  public LoadDroppingDailyUseCase(Application app,
      DroppingDailyService droppingDailyService, AppExecutors appExecutors) {
    super(app);
    this.droppingDailyService = droppingDailyService;
    this.appExecutors = appExecutors;
  }

  public ListenableFuture<LoadDroppingDailyResult> load(UUID droppingId) {
    notifyStart(null, null);

    ListenableFuture<DroppingDailyDataResponse> dddrLf = droppingDailyService.loadDropping(
        droppingId);

    // data yg diperlukan form
    ListenableFuture<NewDroppingDailyResponse> nddrLf = droppingDailyService.getNewDroppingDaily();

    // kumpulkan hasil dari 2 call di atas
    ListenableFuture<LoadDroppingDailyResult> allSuccessLf = Futures.whenAllSucceed(
        Arrays.asList(dddrLf, nddrLf))
        .call(new Callable<LoadDroppingDailyResult>() {
          @Override
          public LoadDroppingDailyResult call()
              throws Exception {
            return new LoadDroppingDailyResult(
                Futures.getDone(dddrLf),
                Futures.getDone(nddrLf));
          }
        }, appExecutors.backgroundIO());

    Futures.addCallback(allSuccessLf,
        new FutureCallback<LoadDroppingDailyResult>() {
          @Override
          public void onSuccess(@NullableDecl LoadDroppingDailyResult result) {
            notifySuccess(application.getString(R.string.dropping_daily_load_success), result);
          }

          @Override
          public void onFailure(Throwable t) {
            ErrorResponse er = null;
            if (HttpException.class.isAssignableFrom(t.getClass())) {
              er = RetrofitUtility.parseErrorBody(
                  ((HttpException) t).response().errorBody());
            }
            notifyFailure(
                er == null ? application.getString(R.string.dropping_daily_load_failed) : null,
                er, t);
          }
        }, appExecutors.backgroundIO());
    return allSuccessLf;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<LoadDroppingDailyResult> result) {
    listener.onLoadDroppingDailyStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<LoadDroppingDailyResult> result) {
    listener.onLoadDroppingDailySuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<LoadDroppingDailyResult> result) {
    listener.onLoadDroppingDailyFailure(result);
  }
}
