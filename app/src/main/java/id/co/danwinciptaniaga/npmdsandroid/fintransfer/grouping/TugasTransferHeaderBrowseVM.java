package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import java.util.LinkedHashSet;
import java.util.Set;

import org.acra.ACRA;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.LoadState;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.CompanyData;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseHeaderData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseSort;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferFilterResponse;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.GetTugasTransferFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class TugasTransferHeaderBrowseVM extends AndroidViewModel implements
    GetTugasTransferFilterUC.Listener {
  private final String TAG = TugasTransferHeaderBrowseVM.class.getSimpleName();
  private final GetTugasTransferBrowseUC ucHeader;
  private final GetTugasTransferFilterUC ucFilter;
  private final Pager<Integer, TugasTransferBrowseHeaderData> pagerHeader;
  private LiveData<PagingData<TugasTransferBrowseHeaderData>> headerList;
  private MutableLiveData<TugasTransferBrowseAndroidFilter> filterField = new MutableLiveData<>();
  private Set<SortOrder> sortField = new LinkedHashSet<>();
  private MutableLiveData<Boolean> refreshHeaderList = new SingleLiveEvent<>();
  private MutableLiveData<LoadState> loadStateHeader = new MutableLiveData<>();
  private MutableLiveData<Resource<String>> actionEvent = new SingleLiveEvent<>();

  @ViewModelInject
  public TugasTransferHeaderBrowseVM(@NonNull Application app, AppExecutors appExecutors,
      GetTugasTransferBrowseUC ucHeader, GetTugasTransferFilterUC ucFilter) {
    super(app);
    this.ucHeader = ucHeader;
    this.ucFilter = ucFilter;
    this.ucFilter.registerListener(this);
    this.sortField.add(TugasTransferBrowseSort.sortBy(TugasTransferBrowseSort.Field.BANK_NAME,
        SortOrder.Direction.ASC));

    pagerHeader = new Pager<Integer, TugasTransferBrowseHeaderData>(
        new PagingConfig(Utility.PAGE_SIZE),
        () -> new TugasTransferBrowseHeaderPagingSource(this.ucHeader, appExecutors.networkIO()));
    CoroutineScope vmScope = ViewModelKt.getViewModelScope(this);
    headerList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pagerHeader), vmScope);
  }

  public LiveData<PagingData<TugasTransferBrowseHeaderData>> getHeaderList() {
    String fn = "getHeaderList() ";
    HyperLog.d(TAG, fn + "terpanggil");
    boolean processSetHeaderList = false;

    boolean isMandatoryField = false;
    if (getFilterField().getValue() != null) {
      if (getFilterField().getValue().getCompanyId() != null) {
        isMandatoryField = true;
      }
    }

    if (!isMandatoryField) {
      String msg = "defaulFilter belum ada, maka panggil service untuk mendapatkan defaultFilterCompanyId";
      HyperLog.d(TAG, fn + msg);
      ucFilter.getCompanyFilter();
      processSetHeaderList = true;
    } else {
      String msg = "defaultFilter sudah ada, maka panggil service untuk loadDataHeader";
      HyperLog.d(TAG, fn + msg);
    }

    if (processSetHeaderList) {
      CoroutineScope vmScope = ViewModelKt.getViewModelScope(this);
      return headerList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pagerHeader), vmScope);
    } else {
      return headerList;
    }
  }

  public MutableLiveData<TugasTransferBrowseAndroidFilter> getFilterField() {
    return filterField;
  }

  public void setFilterField(TugasTransferBrowseAndroidFilter filterField) {
    this.filterField.postValue(filterField);
  }

  public Set<SortOrder> getSortField() {
    return sortField;
  }

  public void setSortField(Set<SortOrder> sortField) {
    this.sortField.clear();
    if (sortField != null) {
      this.sortField.addAll(sortField);
    }
    ucHeader.setSort(sortField);
    refreshHeaderList.postValue(true);
  }

  public MutableLiveData<LoadState> getLoadStateHeader() {
    return loadStateHeader;
  }

  public void setLoadStateHeader(LoadState loadStateHeader) {
    this.loadStateHeader.postValue(loadStateHeader);
  }

  public MutableLiveData<Boolean> getRefreshHeaderList() {
    return refreshHeaderList;
  }

  public void setRefreshHeaderList(Boolean refreshHedearList) {
    this.refreshHeaderList.postValue(refreshHedearList);
  }

  public MutableLiveData<Resource<String>> getActionEvent() {
    return actionEvent;
  }

//  public void setActionEvent(Resource<String> actionEvent) {
//    this.actionEvent.postValue(actionEvent);
//  }

  public void loadHeaderListWithFilter(TugasTransferBrowseAndroidFilter filterField) {
    this.filterField.postValue(filterField);
    ucHeader.setFilter(filterField);
    refreshHeaderList.postValue(true);
  }

  @Override
  public void onProcessStarted(Resource<TugasTransferFilterResponse> loading) {
    //TODO BELUM ADA IMPLEMENTASI
  }

  @Override
  public void onProcessSuccess(Resource<TugasTransferFilterResponse> res) {
    boolean isMandatoryField = false;
    if (getFilterField().getValue() != null) {
      if (getFilterField().getValue().getCompanyId() != null) {
        isMandatoryField = true;
      }
    }

    if (isMandatoryField)
      return;
    else {
      TugasTransferBrowseAndroidFilter defaultFilterField = new TugasTransferBrowseAndroidFilter();
      CompanyData cData = res.getData().getCompanyDataList().get(0);
      if (cData == null) {
        RuntimeException re = new RuntimeException(
            "service getDefaultFilterCompanyId tidak mengembalikan hasil [NULL]");
        ACRA.getErrorReporter().handleException(re);
        return;
      }

      HyperLog.d(TAG,
          "defaultFilterCompanyId yang di dapatkan dari webService["
              + defaultFilterField.getCompanyId()
              + "]");
      HyperLog.d(TAG,
          "defaultFilterCompanyName yang di dapatkan dari webService["
              + defaultFilterField.getCompanyName()
              + "]");

      defaultFilterField.setCompanyId(cData.getId());
      defaultFilterField.setCompanyName(cData.getCompanyName());
      defaultFilterField.setCompanyCode(cData.getCompanyCode());
      defaultFilterField.setPendingTask(true);
      loadHeaderListWithFilter(defaultFilterField);
    }
  }

  @Override
  public void onProcessFailure(Resource<TugasTransferFilterResponse> res) {
    //TODO BELUM ADA IMPLEMENTASI
  }
}

