package id.co.danwinciptaniaga.npmdsandroid.exp;

import java.util.List;
import java.util.Objects;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.hypertrack.hyperlog.HyperLog;

import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.PermissionHelper;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentExpenseItemListBinding;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;
import kotlin.Unit;

/**
 * A fragment representing a list of Items.
 */
@AndroidEntryPoint
public class ExpenseBrowseFragment extends ProtectedFragment {
  public static final String TAG = ExpenseBrowseFragment.class.getSimpleName();

  public static final String SAVED_STATE_LD_REFRESH = "refresh";

  // TODO: Customize parameter argument names
  private static final String ARG_COLUMN_COUNT = "column-count";
  // TODO: Customize parameters
  private int mColumnCount = 1;

  private FragmentExpenseItemListBinding binding;
  private ExpenseBrowseViewModel expenseBrowseViewModel;
  private ExpenseListAdapter adapter;
  private ConcatAdapter concatAdapter;
  private ActionMode actionMode;
  private ActionModeCallback actionModeCallback;
  private RecyclerView recyclerView;
  private ProgressDialog pd;

  private View.OnClickListener restart = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // kita coba restart seluruh activity
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_expense_browse)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragment = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      refreshAdapter();
    }
  };

  /**
   * Mandatory empty constructor for the fragment manager to instantiate the
   * fragment (e.g. upon screen orientation changes).
   */
  public ExpenseBrowseFragment() {
  }

  // TODO: Customize parameter initialization
  @SuppressWarnings("unused")
  public static ExpenseBrowseFragment newInstance(int columnCount) {
    ExpenseBrowseFragment fragment = new ExpenseBrowseFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_COLUMN_COUNT, columnCount);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    expenseBrowseViewModel = new ViewModelProvider(requireActivity()).get(
        ExpenseBrowseViewModel.class);
    if (getArguments() != null) {
      ExpenseBrowseFragmentArgs expenseBrowseFragmentArgs = ExpenseBrowseFragmentArgs.fromBundle(
          getArguments());
      if (expenseBrowseFragmentArgs.getExpenseData() != null) {
        // PagingLibrary sepertinya tidak memungkinkan penambahan 1 entry ExpenseData secara manual ke list-nya.
        // Jadi workaround-nya, kita refresh seluruh list
        expenseBrowseViewModel.setRefreshList(true);
      }
      mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
    }
    HyperLog.d(TAG, "expenseBrowseViewModel: " + expenseBrowseViewModel);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentExpenseItemListBinding.inflate(inflater, container, false);
    View list = binding.list;
    actionModeCallback = new ActionModeCallback();

    // Set the adapter
    if (list instanceof RecyclerView) {
      Context context = list.getContext();
      recyclerView = (RecyclerView) list;
      if (mColumnCount <= 1) {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
      } else {
        recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
      }
      adapter = new ExpenseListAdapter(
          new ExpenseListAdapter.MyExpenseDataRecyclerViewAdapterListener() {
            @Override
            public void onItemClicked(View view, ExpenseData expenseData, int position) {
              if (adapter.getSelectedItemCount() > 0) {
                enableActionMode(position);
              } else {
                NavController navController = Navigation.findNavController(view);
                ExpenseBrowseFragmentDirections.ActionExpenseEditFragment dir = ExpenseBrowseFragmentDirections.actionExpenseEditFragment();
                dir.setExpenseId(expenseData.getId());
                navController.navigate(dir, NavHelper.animParentToChild().build());
                if (actionMode != null)
                  actionMode.finish();
              }
            }

            @Override
            public void onItemLongClickedListener(View v, ExpenseData expenseData, int position) {
              enableActionMode(position);
            }
          }, getActivity().getApplicationContext());

      View.OnClickListener retryCallback = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          adapter.retry();
        }

      };

      concatAdapter = adapter.withLoadStateHeaderAndFooter(
          new ExpenseListLoadStateAdapter(retryCallback),
          new ExpenseListLoadStateAdapter(retryCallback));
      adapter.addLoadStateListener((combinedLoadStates) -> {
        // listener ini mengupdate LoadState di ViewModel
        expenseBrowseViewModel.setExpenseListLoadState(combinedLoadStates.getRefresh());
        return Unit.INSTANCE;
      });
      // lalu gunakan LiveData observer untuk mengupdate view
      expenseBrowseViewModel.getExpenseListLoadState().observe(getViewLifecycleOwner(), state -> {
        if (state instanceof LoadState.Loading) {
          // sedang loading pertama kali (page pertama)
          ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
          ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.VISIBLE);
          ViewUtil.setVisibility(binding.progressWrapper.progressText, View.GONE);
          ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
        } else if (state instanceof LoadState.Error) {
          // terjadi error pada waktu pertama kali (page pertama)
          ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
          ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.GONE);
          ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.VISIBLE);
          // retry = reload semua data
          binding.progressWrapper.retryButton.setOnClickListener(reloadFragment);
          ViewUtil.setVisibility(binding.progressWrapper.progressText, View.VISIBLE);
          binding.progressWrapper.progressText.setText(
              ((LoadState.Error) state).getError().getMessage());
          // sembunyikan list, karena retry akan dihandle oleh button di atas
          ViewUtil.setVisibility(binding.swipeRefresh, View.GONE);
        } else if (state instanceof LoadState.NotLoading) {
          // sudah ok
          ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
          ViewUtil.setVisibility(binding.swipeRefresh, View.VISIBLE);
        }
      });
      recyclerView.setAdapter(concatAdapter);

      expenseBrowseViewModel.getRefreshList().observe(getViewLifecycleOwner(), e -> {
        if (e != null && e) {
          adapter.refresh();
        }
      });

      //      expenseBrowseViewModel.getFormState().observe(getViewLifecycleOwner(), formState -> {
      //        Log.d(TAG, "masuk expenseBrowseViewModel.getFormState().observe formState->"+formState+"<-");
      ////        bindProgressAndPageContent(formState);
      //      });

      expenseBrowseViewModel.getActionEvent().observe(getViewLifecycleOwner(), s -> {
        HyperLog.d(TAG, "expenseBrowseViewModel.getActionEvent().observe s->" + s + "<-");
        adapter.clearSelections();
        if (Status.SUCCESS.equals(s.getStatus())) {
          Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
          adapter.refresh();
        } else {
          String message = s.getMessage();
          if (message == null)
            message = s.getData() != null ? s.getData() : "Terjadi kesalahan";
          Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
        }
        if (pd != null)
          pd.dismiss();
      });
    }

    binding.swipeRefresh.setOnRefreshListener(() -> {
      adapter.refresh();
      binding.swipeRefresh.setRefreshing(false);
    });
    binding.fab.setOnClickListener(e -> {
      NavController navController = Navigation.findNavController(binding.getRoot());
      ExpenseBrowseFragmentDirections.ActionExpenseEditFragment dir = ExpenseBrowseFragmentDirections.actionExpenseEditFragment();
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });
    NavController navController = NavHostFragment.findNavController(this);
    LiveData<Boolean> refreshLd = navController.getCurrentBackStackEntry()
        .getSavedStateHandle()
        .getLiveData(SAVED_STATE_LD_REFRESH);
    refreshLd.observe(getViewLifecycleOwner(), refresh -> {
      HyperLog.d(TAG, "refresh signaled: " + refresh);
      if (refresh != null && refresh) {
        adapter.refresh();
      }
      navController.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(SAVED_STATE_LD_REFRESH);
    });
    return binding.getRoot();
  }

  protected void refreshAdapter() {
    // reload fragment ini = memuat ulang seluruh list
    adapter.refresh();
    // karena menggunakan progress indicator sendiri, jadi  tidak perlu yg dari swipeRefresh
    binding.swipeRefresh.setRefreshing(false);
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    if (status.getStatus() == Status.LOADING) {
      ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.VISIBLE);
      ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      binding.swipeRefresh.setVisibility(View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.GONE);
      ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.VISIBLE);
      // retry = restart fragment (karena authentication gagal)
      binding.progressWrapper.retryButton.setOnClickListener(restart);
    }
  }

  private void onReady() {
    // harusnya permission sudah ada kalau authenticationStatusUpdate SUCCESS
    Resource<List<PermissionInfo>> permissions = basemodel.getPermissionsLd().getValue();
    if (PermissionHelper.hasPermission(permissions.getData(),
        PermissionHelper.PERMISSION_TYPE_ENTITY_OP, "npmds_Expense:create")) {
      binding.fab.setVisibility(View.VISIBLE);
    } else {
      binding.fab.setVisibility(View.GONE);
    }
    expenseBrowseViewModel.getExpenseList().observe(getViewLifecycleOwner(), pagingData -> {
      adapter.submitData(getLifecycle(), pagingData);
    });
    setFilterObserver();
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.expense_browse_menu, menu);
  }

  private void enableActionMode(int position) {
    //    Toast.makeText(getActivity().getApplicationContext(), "EnableActionMode", Toast.LENGTH_SHORT).show();

    if (actionMode == null) {
      actionMode = getActivity().startActionMode(
          (android.view.ActionMode.Callback) actionModeCallback);
    }
    toggleSelection(position);
  }

  private class ActionModeCallback implements ActionMode.Callback {

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
      mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
      Utility.setActionModeMenu(mode, TAG);
      return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
      return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
      switch (item.getItemId()) {
      case R.id.action_delete:
        deleteRow(mode);
        return true;
      case R.id.action_reject:
        rejectRow(mode);
        return true;
      default:
        return false;
      }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
      adapter.clearSelections();
      actionMode = null;
      recyclerView.post(new Runnable() {
        @Override
        public void run() {
          adapter.resetAnimationIndex();
        }
      });
    }
  }

  public void deleteRow(ActionMode mode) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setCancelable(true);
    builder.setTitle("Konfirmasi Hapus");
    builder.setMessage("Apakah anda yakin untuk melakukan penghapusan terkait data ini?");
    builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        pd = ProgressDialog.show(getActivity(), "",
            getString(R.string.silahkan_tunggu), true);
        expenseBrowseViewModel.deleteSelectedExpense(adapter.getSelectedExpenseData());
        mode.finish();
        adapter.resetAnimationIndex();
      }
    });
    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        Toast.makeText(getContext(), "Cancel", Toast.LENGTH_SHORT).show();
        mode.finish();
        adapter.resetAnimationIndex();
      }
    });

    AlertDialog dialog = builder.create();
    try {
      dialog.show();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public void rejectRow(ActionMode mode) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setCancelable(true);
    builder.setTitle("Konfirmasi Reject");
    builder.setMessage("Apakah anda yakin untuk melakukan reject terkait data ini?");
    builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        pd = ProgressDialog.show(getActivity(), "",
            "Loading. Please wait...", true);
        expenseBrowseViewModel.rejectSelectedExpense(adapter.getSelectedExpenseData());
        mode.finish();
        adapter.resetAnimationIndex();
      }
    });
    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        Toast.makeText(getContext(), "Cancel", Toast.LENGTH_SHORT).show();
        adapter.resetAnimationIndex();
      }
    });

    AlertDialog dialog = builder.create();
    try {
      dialog.show();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void toggleSelection(int position) {
    adapter.toggleSelection(position);
    boolean isDeleteApproveActive = adapter.isDeleteApproveActionActive();
    boolean isRejectActive = adapter.isRejectActionActive();
    int count = adapter.getSelectedItemCount();
    actionMode.getMenu().findItem(R.id.action_delete).setVisible(isDeleteApproveActive);
    actionMode.getMenu().findItem(R.id.action_approve).setVisible(isDeleteApproveActive);
    actionMode.getMenu().findItem(R.id.action_reject).setVisible(isRejectActive);

    if (count == 0) {
      actionMode.finish();
    } else {
      actionMode.setTitle(String.valueOf(count));
      actionMode.invalidate();
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (item.getItemId() == R.id.menu_filter) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_expense_browse, nc.getCurrentDestination().getId()))
        return true;
      ExpenseBrowseFragmentDirections.ActionExpenseFilterFragment dir = ExpenseBrowseFragmentDirections.actionExpenseFilterFragment();
      dir.setFilterField(expenseBrowseViewModel.getFilterField());
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    } else if (item.getItemId() == R.id.menu_sort) {
      Fragment f = getParentFragmentManager().findFragmentByTag("Sort");
      if (!Objects.equals(f, null))
        return true;
      BottomSheetDialogFragment bsdf = new ExpenseSortFragment();
      bsdf.show(getParentFragmentManager(), "Sort");
      return true;
    }

    return false;
  }

  public void setFilterObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<ExpenseBrowseFilter> filterField = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        ExpenseFilterFragment.EXPENSE_FILTER_FIELD);
    filterField.observe(getViewLifecycleOwner(), filterObj -> {
      expenseBrowseViewModel.setFilterField(filterObj);
      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
          ExpenseFilterFragment.EXPENSE_FILTER_FIELD);
    });
  }
}