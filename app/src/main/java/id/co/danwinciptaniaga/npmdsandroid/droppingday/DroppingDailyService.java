package id.co.danwinciptaniaga.npmdsandroid.droppingday;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.ListResponse;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilterResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseSort;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyDataResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.NewDroppingDailyResponse;
import id.co.danwinciptaniaga.npmds.data.wf.WfHistoryResponse;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DroppingDailyService {
  @GET("rest/s/droppingDaily")
  ListenableFuture<ListResponse<DroppingDailyData>> getDroppingList(
      @Query("page") Integer page,
      @Query("pageSize") Integer pageSize,
      @Query("filter") DroppingBrowseFilter filter,
      @Query("sort") DroppingBrowseSort sort);

  @GET("rest/s/droppingDaily/getFilterField")
  ListenableFuture<DroppingBrowseFilterResponse> getDroppingFilterField();

  @GET("rest/s/droppingDaily/new")
  ListenableFuture<NewDroppingDailyResponse> getNewDroppingDaily();

  @GET("rest/s/droppingDaily/{id}")
  ListenableFuture<DroppingDailyDataResponse> loadDropping(@Path("id") UUID droppingId);

  @Multipart
  @POST("rest/s/droppingDaily/finStaffApprove")
  ListenableFuture<DroppingDailyData> finStaffApprove(@Query("procTaskId") UUID procTaskId,
      @Query("comment") String comment,
      @Query("id") UUID droppingId,
      @Part("transferDate") LocalDate transferDate,
      @Query("transferType") String transferType,
      @Query("sourceAccountId") UUID sourceAccountId,
//      @Field("mandatoryDateField") LocalDate mandatoryDateField,
      @Part("checkTs") Date lCheckTs);

  @FormUrlEncoded
  @POST("rest/s/droppingDaily/{id}/finSpvApprove")
  ListenableFuture<DroppingDailyData> finSpvApprove(@Field("procTaskId") UUID procTaskId,
      @Field("comment") String comment,
      @Path("id") UUID droppingId,
      @Field("otpCode") String otpCode,
      @Field("transferType") String transferType,
      @Field("checkTs") Date lCheckTs);

  @FormUrlEncoded
  @POST("rest/s/droppingDaily/{id}/generateOtp")
  ListenableFuture<DroppingDailyData> generateOtp(@Field("procTaskId") UUID procTaskId,
      @Path("id") UUID droppingId);

  @FormUrlEncoded
  @POST("rest/s/droppingDaily/{id}/captainApprove")
  ListenableFuture<DroppingDailyData> captainApprove(@Field("procTaskId") UUID procTaskId,
      @Field("comment") String comment,
      @Path("id") UUID droppingId,
      @Field("approvedAmount") long approvedAmount,
      @Field("altAccount") boolean altAccount,
      @Field("bankAccountData") BankAccountData bankAccountData,
      @Field("checkTs") Date checkTs);

  @FormUrlEncoded
  @POST("rest/s/droppingDaily/{id}/rejectOrReturn")
  ListenableFuture<DroppingDailyData> rejectOrReturn(@Field("procTaskId") UUID procTaskId,
      @Field("outcome") String outcome, @Field("comment") String comment,
      @Path("id") UUID droppingId, @Field("checkTs") Date checkTs);

  @GET("rest/s/droppingDaily/{id}/wfHistory")
  ListenableFuture<WfHistoryResponse> getWfHistory(@Path("id") UUID entityId);
}
