package id.co.danwinciptaniaga.npmdsandroid.wf;

public interface WfValidationProcess {
  Boolean call(WfProcessParameter params);
}
