package id.co.danwinciptaniaga.npmdsandroid.booking;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.booking.BookingFilterResponse;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class GetBookingFilterUC
    extends BaseObservable<GetBookingFilterUC.Listener, BookingFilterResponse> {
  private final String TAG = GetBookingFilterUC.class.getSimpleName();
  private final BookingService service;
  private final Application app;
  private final AppExecutors appExecutors;

  @Inject
  public GetBookingFilterUC(Application app, BookingService service, AppExecutors appExecutors) {
    super(app);
    this.app = app;
    this.service = service;
    this.appExecutors = appExecutors;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<BookingFilterResponse> result) {
    listener.onProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<BookingFilterResponse> result) {
    listener.onProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<BookingFilterResponse> result) {
    listener.onProcessFailure(result);
  }

  public void getAppBookingFilter() {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<BookingFilterResponse> process = service.getAppFilterField();
    Futures.addCallback(process, new FutureCallback<BookingFilterResponse>() {
      @Override
      public void onSuccess(@NullableDecl BookingFilterResponse result) {
        HyperLog.d(TAG, "getAppBookingFilter() berhasil");
        notifySuccess(null, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String msg = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("getAppBookingFilter() Gagal->" + msg, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void getBookingFilter(){
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    HyperLog.d(TAG,"MASUK PANGGIL SERVICE");
    ListenableFuture<BookingFilterResponse> process = service.getBookingFilterField();
    Futures.addCallback(process, new FutureCallback<BookingFilterResponse>() {
      @Override
      public void onSuccess(@NullableDecl BookingFilterResponse result) {
        HyperLog.d(TAG, "getBookingFilter() berhasil");
        notifySuccess(null, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String msg = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("getBookingFilter() Gagal->" + msg, null, t);
      }
    }, appExecutors.networkIO());
  }

  public interface Listener {
    void onProcessStarted(Resource<BookingFilterResponse> loading);

    void onProcessSuccess(Resource<BookingFilterResponse> res);

    void onProcessFailure(Resource<BookingFilterResponse> res);
  }
}
