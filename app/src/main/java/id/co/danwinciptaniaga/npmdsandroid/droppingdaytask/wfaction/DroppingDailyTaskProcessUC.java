package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskProcessResultData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.DroppingDailyTaskService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class DroppingDailyTaskProcessUC
    extends BaseObservable<DroppingDailyTaskProcessUC.Listener, DroppingDailyTaskProcessResultData> {
  private final String TAG = DroppingDailyTaskProcessUC.class.getSimpleName();
  private final DroppingDailyTaskService service;
  private final AppExecutors appExecutors;
  private final Application app;
  private final String doneMsg;
  private final String failMsg;

  @Inject
  public DroppingDailyTaskProcessUC(Application app, DroppingDailyTaskService service,
      AppExecutors appExecutors) {
    super(app);
    this.service = service;
    this.appExecutors = appExecutors;
    this.app = app;
    doneMsg = this.app.getString(R.string.process_success);
    failMsg = this.app.getString(R.string.process_fail);
  }

  public void processApprove(List<DroppingDailyTaskBrowseDetailData> dataList, String comment) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    String dataJson = new Gson().toJson(dataList);
//    ListenableFuture<Boolean> process = service.processApprove(dataList, comment);
    ListenableFuture<DroppingDailyTaskProcessResultData> process = service.processApprove(dataJson, comment);
    Futures.addCallback(process, new FutureCallback<DroppingDailyTaskProcessResultData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingDailyTaskProcessResultData result) {
        notifySuccess(doneMsg, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(failMsg + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processReject(List<DroppingDailyTaskBrowseDetailData> dataList, String comment) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    String dataJson = new Gson().toJson(dataList);
    //    ListenableFuture<Boolean> process = service.processApprove(dataList, comment);
    ListenableFuture<DroppingDailyTaskProcessResultData> process = service.processReject(dataJson, comment);
    Futures.addCallback(process, new FutureCallback<DroppingDailyTaskProcessResultData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingDailyTaskProcessResultData result) {
        notifySuccess(doneMsg, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(failMsg + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<DroppingDailyTaskProcessResultData> result) {
    listener.onProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<DroppingDailyTaskProcessResultData> result) {
    listener.onProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<DroppingDailyTaskProcessResultData> result) {
    listener.onProcessFailure(result);
  }

  public interface Listener {
    void onProcessStarted(Resource<DroppingDailyTaskProcessResultData> loading);

    void onProcessSuccess(Resource<DroppingDailyTaskProcessResultData> response);

    void onProcessFailure(Resource<DroppingDailyTaskProcessResultData> resource);
  }
}
