package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmds.data.common.LobShortData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.common.SoShortData;
import id.co.danwinciptaniaga.npmds.data.common.UtilityState;
import id.co.danwinciptaniaga.npmds.data.wf.ProcTaskData;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingVM;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFormState;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingLoadUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingPrepareUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingProcessUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class AppBookingCashVM extends Abstract_AppBookingVM {
  final static String TAG = AppBookingCashVM.class.getSimpleName();

  @ViewModelInject
  public AppBookingCashVM(@NonNull Application application,
      AppExecutors appExecutors,
      AppBookingPrepareUC prepareUC,
      AppBookingLoadUC appBookingLoadUC,
      AppBookingProcessUC processUC,
      UtilityService utilityService,
      AppBookingService appBookingService) {
    super(application, appExecutors, prepareUC, appBookingLoadUC, processUC, utilityService,
        appBookingService);
  }

  @Override
  public void processSaveDraftNew(boolean isSaveDraftProcess) {
    if (isSaveDraftProcess) {
      if (!validateProcessDraft(true))
        return;
    } else {
      if (!validateProcess())
        return;
    }
    UUID outletId = getField_outletKvm().getValue() != null ?
        getField_outletKvm().getValue().getKey().getOutletId() :
        null;
    processUC.cashier_SaveDraft(getField_AppBookingId(), outletId,
        getField_BookingDate().getValue(),
        getField_AppBookingTypeId().getValue(),
        getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjamCash().getValue(), getField_AlasanPinjam().getValue(),
        getField_AppNo().getValue(), getField_PoNo().getValue(), getField_PoDate().getValue(),
        getPoprIdList(), getField_ConsumerName().getValue(), getField_ConsumerAddress().getValue(),
        getField_ConsumerPhone().getValue(), getField_VehicleNo().getValue(),
        isField_isOpenClose().getValue(),
        getField_ConsumerAmount().getValue() != null ? getField_ConsumerAmount().getValue() : BigDecimal.ZERO,
        getField_FIFAmount().getValue() != null ? getField_FIFAmount().getValue() : BigDecimal.ZERO,
        getField_BiroJasaAmount().getValue(),
        getField_BookingAmount().getValue() != null ? getField_BookingAmount().getValue() : BigDecimal.ZERO,
        getField_FeeMatrix().getValue() != null ? getField_FeeMatrix().getValue() : BigDecimal.ZERO,
        getField_FeeScheme().getValue() != null ? getField_FeeScheme().getValue() : BigDecimal.ZERO,
        getField_AlasanPinjamCash().getValue(), null, att_po_id,
        (is_att_po_change ? field_Attach_Final_PO : null),
        att_stnk_id, (is_att_stnk_change ? field_Attach_Final_STNK : null), att_kk_id,
        (is_att_kk_change ? field_Attach_Final_KK : null), att_bpkb_id,
        (is_att_bpkb_change ? field_Attach_Final_BPKB : null),
        att_spt_id, (is_att_spt_change ? field_Attach_Final_SPT : null), att_serah_terima_id,
        (is_att_serah_terima_change ? field_Attach_Final_SERAH_TERIMA : null),
        att_fisik_motor_id, (is_att_fisik_motor_change ? field_Attach_Final_FISIK_MOTOR : null),
        getField_UpdateTS().getValue(), getFieldWfStatus().getValue(), getField_AppOperation(),
        isSaveDraftProcess);
  }

  @Override
  public void processSaveDraftCancel(boolean isNewDocument) {
    validation_field_CancelReason(getField_CancelReason().getValue(), false);
    if (field_Attach_Final_HO == null)
      field_Attach_Final_HO_OK.postValue(false);

    if (!field_CancelReason_OK || field_Attach_Final_HO == null)
      return;
    processUC.saveDraftCashTransferCancel(getField_AppBookingId(),
        getField_AppBookingTypeId().getValue(), getField_BookingId(),
        getField_CancelReason().getValue(), att_HO_id,
        (isFirstTimeLoadFromEditorCancel?field_Attach_Final_HO:(is_att_HO_change?field_Attach_Final_HO:null)),
        getField_UpdateTS().getValue(),
        isNewDocument);
  }

  @Override
  public void processSaveDraftEdit(boolean isSaveDraftProcess, boolean isNewDocument) {
    if (!validateProcess())
      return;
    processUC.saveDraftCashEdit(getField_AppBookingId(),
        getField_outletKvm().getValue().getKey().getOutletId(), getField_BookingDate().getValue(),
        getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AlasanPinjamCash().getValue(),
        getField_AppNo().getValue(), getField_PoNo().getValue(), getField_PoDate().getValue(),
        att_po_id, (isFirstTimeLoadFromEditorCancel?field_Attach_Final_PO:(is_att_po_change?field_Attach_Final_PO:null)),
        getPoprIdList(), getField_ConsumerName().getValue(),
        getField_ConsumerAddress().getValue(), getField_ConsumerPhone().getValue(),
        getField_VehicleNo().getValue(), isField_isOpenClose().getValue(),
        getField_ConsumerAmount().getValue(), getField_BiroJasaAmount().getValue(),
        getField_FIFAmount().getValue(), getField_BookingAmount().getValue(),
        getField_FeeMatrix().getValue(), getField_FeeScheme().getValue(),
        att_stnk_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_STNK:(is_att_stnk_change?field_Attach_Final_STNK:null)),
        att_kk_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_KK:(is_att_kk_change?field_Attach_Final_KK:null)),
        att_bpkb_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_BPKB:(is_att_bpkb_change?field_Attach_Final_BPKB:null)),
        att_spt_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_SPT:(is_att_spt_change?field_Attach_Final_SPT:null)),
        att_serah_terima_id, (isFirstTimeLoadFromEditorCancel?field_Attach_Final_SERAH_TERIMA:(is_att_serah_terima_change ? field_Attach_Final_SERAH_TERIMA : null)),
        att_bukti_retur_id, (isFirstTimeLoadFromEditorCancel?field_Attach_Final_BUKTIRETUR:(is_att_bukti_retur_change ? field_Attach_Final_BUKTIRETUR : null)),
        att_fisik_motor_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_FISIK_MOTOR:(is_att_fisik_motor_change?field_Attach_Final_FISIK_MOTOR:null)),
        getField_BookingId(), getField_UpdateTS().getValue(),
        isSaveDraftProcess, isNewDocument);
  }

  @Override
  public void processWfSubmitNew() {
    if (!validateProcess())
      return;
    processUC.cashier_SubmitNew(getField_AppBookingId(),
        getField_outletKvm().getValue().getKey().getOutletId(), getField_BookingDate().getValue(),
        getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AlasanPinjamCash().getValue(),
        getField_AppNo().getValue(), getField_PoNo().getValue(), getField_PoDate().getValue(),
        att_po_id,(is_att_po_change?field_Attach_Final_PO:null), getPoprIdList(), getField_ConsumerName().getValue(),
        getField_ConsumerAddress().getValue(), getField_ConsumerPhone().getValue(),
        getField_VehicleNo().getValue(), isField_isOpenClose().getValue(),
        getField_ConsumerAmount().getValue(), getField_BiroJasaAmount().getValue(),
        getField_FIFAmount().getValue(), getField_BookingAmount().getValue(),
//        getField_FeeMatrix().getValue(), getField_FeeScheme().getValue(), att_stnk_id,field_Attach_Final_STNK,
//        att_kk_id,field_Attach_Final_KK, att_bpkb_id,field_Attach_Final_BPKB, att_spt_id,
//        field_Attach_Final_SPT,
//        att_serah_terima_id, field_Attach_Final_SERAH_TERIMA,
//        att_fisik_motor_id, field_Attach_Final_FISIK_MOTOR,
        // ---------------------
        getField_FeeMatrix().getValue(), getField_FeeScheme().getValue(), att_stnk_id,(is_att_stnk_change?field_Attach_Final_STNK:null),
        att_kk_id,(is_att_kk_change?field_Attach_Final_KK:null), att_bpkb_id,(is_att_bpkb_change?field_Attach_Final_BPKB:null), att_spt_id,
        (is_att_spt_change?field_Attach_Final_SPT:null),
        att_serah_terima_id, (is_att_serah_terima_change ? field_Attach_Final_SERAH_TERIMA : null),
        att_fisik_motor_id, (is_att_fisik_motor_change ? field_Attach_Final_FISIK_MOTOR : null),
        getField_UpdateTS().getValue());
  }

  @Override
  public void processWfSubmitCancel(boolean isNewDocument) {
    validation_field_CancelReason(getField_CancelReason().getValue(), false);

    if (field_Attach_Final_HO == null)
      field_Attach_Final_HO_OK.postValue(false);

    if (!field_CancelReason_OK || field_Attach_Final_HO == null)
      return;

    processUC.submitCashTransfersCancel(getField_AppBookingId(),
        getField_AppBookingTypeId().getValue(), getField_BookingId(),
        getField_CancelReason().getValue(),att_HO_id,
        (isFirstTimeLoadFromEditorCancel?field_Attach_Final_HO:(is_att_HO_change?field_Attach_Final_HO:null)), getField_UpdateTS().getValue(),
        isNewDocument);
  }

  @Override
  public void processWfSubmitEdit(boolean isNewDocument) {
    if (!validateProcess())
      return;
    processUC.cashier_SubmitAppBookingCashEdit(getField_AppBookingId(),
        getField_outletKvm().getValue().getKey().getOutletId(), getField_BookingDate().getValue(),
        getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AlasanPinjamCash().getValue(),
        getField_AppNo().getValue(), getField_PoNo().getValue(), getField_PoDate().getValue(),
        att_po_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_PO:(is_att_po_change?field_Attach_Final_PO:null)),
        getPoprIdList(), getField_ConsumerName().getValue(),
        getField_ConsumerAddress().getValue(), getField_ConsumerPhone().getValue(),
        getField_VehicleNo().getValue(), isField_isOpenClose().getValue(),
        getField_ConsumerAmount().getValue(), getField_BiroJasaAmount().getValue(),
        getField_FIFAmount().getValue(), getField_BookingAmount().getValue(),
        getField_FeeMatrix().getValue(), getField_FeeScheme().getValue(),
        att_stnk_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_STNK:(is_att_stnk_change?field_Attach_Final_STNK:null)),
        att_kk_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_KK:(is_att_kk_change?field_Attach_Final_KK:null)),
        att_bpkb_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_BPKB:(is_att_bpkb_change?field_Attach_Final_BPKB:null)),
        att_spt_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_SPT:(is_att_spt_change?field_Attach_Final_SPT:null)),
        att_serah_terima_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_SERAH_TERIMA:(is_att_serah_terima_change?field_Attach_Final_SERAH_TERIMA:null)),
        att_fisik_motor_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_FISIK_MOTOR:(is_att_fisik_motor_change?field_Attach_Final_FISIK_MOTOR:null)),
        att_bukti_retur_id,(isFirstTimeLoadFromEditorCancel?field_Attach_Final_BUKTIRETUR:(is_att_bukti_retur_change?field_Attach_Final_BUKTIRETUR:null)), getField_BookingId(), getField_UpdateTS().getValue(),
        isNewDocument);
  }

  public void processWF_DOC_APPROVE_NEW(String comment) {
    processUC.processCashWF_DOC_APPROVE_NEW(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_DOC_RETURN_NEW(String comment) {
    processUC.processCashWF_DOC_RETURN_NEW(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_KASIR_REVISION_NEW(String comment) {
    if (!validateProcess())
      return;
    processUC.processCashWF_KASIR_REVISION_NEW(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_outletKvm().getValue().getKey().getOutletId(),
        getField_BookingDate().getValue(), getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AlasanPinjamCash().getValue(),
        getField_AppNo().getValue(), getField_PoNo().getValue(), getField_PoDate().getValue(),
        att_po_id,(is_att_po_change?field_Attach_Final_PO:null), getPoprIdList(), getField_ConsumerName().getValue(),
        getField_ConsumerAddress().getValue(), getField_ConsumerPhone().getValue(),
        getField_VehicleNo().getValue(), isField_isOpenClose().getValue(),
        getField_ConsumerAmount().getValue(), getField_BiroJasaAmount().getValue(),
        getField_FIFAmount().getValue(), getField_BookingAmount().getValue(),
        getField_FeeMatrix().getValue(), getField_FeeScheme().getValue(), att_stnk_id,(is_att_stnk_change?field_Attach_Final_STNK:null),
        att_kk_id,(is_att_kk_change?field_Attach_Final_KK:null), att_bpkb_id,(is_att_bpkb_change?field_Attach_Final_BPKB:null),
        att_spt_id,(is_att_spt_change?field_Attach_Final_SPT:null),
        att_serah_terima_id, (is_att_serah_terima_change ? field_Attach_Final_SERAH_TERIMA : null),
        att_fisik_motor_id, (is_att_fisik_motor_change ? field_Attach_Final_FISIK_MOTOR : null),
        getField_UpdateTS().getValue());
  }

  public void processWF_CAPTAIN_APPROVE_NEW(String comment) {
    processUC.processCashWF_CAPTAIN_APPROVE_NEW(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_CAPTAIN_RETURN_NEW(String comment) {
    processUC.processCashWF_CAPTAIN_RETURN_NEW(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_AOC_APPROVE_EDIT(String comment) {
    processUC.processCashWF_AOC_APPROVE_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_AOC_RETURN_EDIT(String comment) {
    processUC.processCashWF_AOC_RETURN_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_AOC_REJECT_EDIT(String comment) {
    processUC.processCashWF_AOC_REJECT_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_KASIR_REVISION_EDIT(String comment) {
    if (!validateProcess())
      return;
    processUC.processCashWF_KASIR_REVISION_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_outletKvm().getValue().getKey().getOutletId(),
        getField_BookingDate().getValue(), getField_LobKvm().getValue().getKey().getLobId(),
        getField_soKvm().getValue().getKey().getSoId(), getField_Idnpksf().getValue(),
        getField_AlasanPinjam().getValue(), getField_AlasanPinjamCash().getValue(),
        getField_AppNo().getValue(), getField_PoNo().getValue(), getField_PoDate().getValue(),
        att_po_id,(is_att_po_change?field_Attach_Final_PO:null), getPoprIdList(), getField_ConsumerName().getValue(),
        getField_ConsumerAddress().getValue(), getField_ConsumerPhone().getValue(),
        getField_VehicleNo().getValue(), isField_isOpenClose().getValue(),
        getField_ConsumerAmount().getValue(), getField_BiroJasaAmount().getValue(),
        getField_FIFAmount().getValue(), getField_BookingAmount().getValue(),
        getField_FeeMatrix().getValue(), getField_FeeScheme().getValue(), att_stnk_id,(is_att_stnk_change?field_Attach_Final_STNK:null),
        att_kk_id, (is_att_kk_change ? field_Attach_Final_KK : null), att_bpkb_id,
        (is_att_bpkb_change ? field_Attach_Final_BPKB : null), att_spt_id,
        (is_att_spt_change ? field_Attach_Final_SPT : null),
        att_serah_terima_id, (is_att_serah_terima_change ? field_Attach_Final_SERAH_TERIMA : null),
        att_fisik_motor_id, (is_att_fisik_motor_change ? field_Attach_Final_FISIK_MOTOR : null),
        getField_BookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_CAPTAIN_APPROVE_EDIT(String comment) {
   processUC.processCashWF_CAPTAIN_APPROVE_EDIT(getField_ProcTaskId().getValue(), comment,
       getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_CAPTAIN_RETURN_EDIT(String comment) {
    processUC.processCashWF_CAPTAIN_RETURN_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_CAPTAIN_REJECT_EDIT(String comment) {
    processUC.processCashWF_CAPTAIN_REJECT_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_DIRECTOR_APPROVE_EDIT(String comment) {
    processUC.processCashWF_DIRECTOR_APPROVE_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_DIRECTOR_RETURN_EDIT(String comment) {
    processUC.processCashWF_DIRECTOR_RETURN_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_DIRECTOR_REJECT_EDIT(String comment) {
    processUC.processCashWF_DIRECTOR_REJECT_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_FINRETURN_APPROVE_EDIT(String comment) {
    processUC.processCashWF_FINRETURN_APPROVE_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_FINRETURN_RETURN_EDIT(String comment) {
    processUC.processCashWF_FINRETURN_RETURN_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_FINRETURN_REJECT_EDIT(String comment) {
    processUC.processCashWF_FINRETURN_REJECT_EDIT(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_KASIR_REVISION_CANCEL(String comment) {
    validation_field_CancelReason(getField_CancelReason().getValue(), false);

    if (field_Attach_Final_HO == null)
      field_Attach_Final_HO_OK.postValue(false);

    if (!field_CancelReason_OK || field_Attach_Final_HO == null)
      return;

    processUC.submitRevisionCashCancel(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_AppBookingTypeId().getValue(),
        getField_BookingId(), getField_CancelReason().getValue(),
        att_HO_id, (is_att_HO_change ? field_Attach_Final_HO : null),
        getField_UpdateTS().getValue());
  }

  public void processWF_AOC_APPROVE_CANCEL(String comment) {
    processUC.processCashWF_AOC_APPROVE_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_AOC_RETURN_CANCEL(String comment) {
    processUC.processCashWF_AOC_RETURN_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_AOC_REJECT_CANCEL(String comment) {
    processUC.processCashWF_AOC_REJECT_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_CAPTAIN_APPROVE_CANCEL(String comment) {
    processUC.processCashWF_CAPTAIN_APPROVE_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_CAPTAIN_RETURN_CANCEL(String comment) {
    processUC.processCashWF_CAPTAIN_RETURN_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_CAPTAIN_REJECT_CANCEL(String comment) {
    processUC.processCashWF_CAPTAIN_REJECT_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_DIRECTOR_APPROVE_CANCEL(String comment) {
    processUC.processCashWF_DIRECTOR_APPROVE_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_DIRECTOR_RETURN_CANCEL(String comment) {
    processUC.processCashWF_DIRECTOR_RETURN_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_DIRECTOR_REJECT_CANCEL(String comment) {
    processUC.processCashWF_DIRECTOR_REJECT_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_FINRETURN_APPROVE_CANCEL(String comment) {
    processUC.processCashWF_FINRETURN_APPROVE_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_FINRETURN_RETURN_CANCEL(String comment) {
    processUC.processCashWF_FINRETURN_RETURN_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  public void processWF_FINRETURN_REJECT_CANCEL(String comment) {
    processUC.processCashWF_FINRETURN_REJECT_CANCEL(getField_ProcTaskId().getValue(), comment,
        getField_AppBookingId(), getField_UpdateTS().getValue());
  }

  @Override
  protected void setReadOnly_AppNoPoNoPoDatePoAttchment2(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans,String appNo,String poNo, LocalDate poDate,boolean isAttchmentPoExist) {
    boolean doNextProcess = false;
    switch (smCash){
    case KASIR_SUBMITTED_NEW_APPBOOKING:
    case KASIR_SUBMITTED_EDIT_APPBOOKING:
//    case KASIR_REVISION_NEW_APPBOOKING:
//    case KASIR_REVISION_EDIT_APPBOOKING:
      readOnly_AppNoPoNoPoDatePoAttchment.postValue(false);
      doNextProcess = false;
      break;
    default:
      doNextProcess=true;
      break;
    }

    readOnly_AppNoPoNoPoDatePoAttchment.postValue(false);
//    if (doNextProcess) {
//      if (appNo != null && poNo != null && poDate != null
//          && isAttchmentPoExist)
//        readOnly_AppNoPoNoPoDatePoAttchment.postValue(true);
//      else
//        readOnly_AppNoPoNoPoDatePoAttchment.postValue(false);
//    }
  }

//  @Override
//  public void setField_ConsumerAmount(BigDecimal data) {
//    super.setField_ConsumerAmount(data);
//
//    if(!Utility.OPERATION_EDIT.equals(getField_AppOperation()))
//      return;
//    if (data != null && data.compareTo(field_ConsAmount_OLD) == -1)
//      setShowBuktiTransfer(true);
//    else
//      setShowBuktiTransfer(false);
//  }

  @Override
  public void onLoadSuccess(Resource<AppBookingLoadUC.LoadResult> res) {
    String fn = "onLoadSuccess() ";
    isLoadProcess = true;
    detailResponse = res.getData().getAppBookingDetailResponse();
    prepareFormResponse = res.getData().getNewBookingReponse();
    AppBookingData abd = detailResponse.getAppBookingData();

    setField_AppBookingId(abd.getId());
    // set wf
    List<ProcTaskData> ptdList = detailResponse.getProcTaskDataList();
    ProcTaskData pd = ptdList.size() > 0 ? ptdList.get(0): null;
    setField_ProcTaskId(pd != null ? pd.getId() : null);
    if (pd != null && pd.getPossibleOutcome() != null)
      setWfTaskList(pd.getPossibleOutcome());

    // set Header
    setField_TrxNo(abd.getTrxNo());
    setHeader_PtName(abd.getOutletObj().getCompanyName());
    setFieldWfStatus(abd.getWfId());
    setHeader_Status(abd.getWfName());
    setHeader_OutletName(abd.getOutletObj().getOutletName());
    setHeader_OutletCode(abd.getOutletObj().getOutletCode());
    setHeader_JenisPengajuan(abd.getAppTypeName());
    setHeader_BookingDate(abd.getBookingDate().format(Formatter.DTF_dd_MM_yyyy));

    String pengajuanDate = abd.getPengajuanDate() != null
        ? Formatter.SDF_dd_MM_yyyy.format(abd.getPengajuanDate())
        : "-";
    setHeader_PengajuanDate(pengajuanDate);

    // set prepare form
    setPrepareForm(prepareFormResponse);

    // set Field
    for (int i = 0; i < prepareFormResponse.getAssignedOutlet().size(); i++) {
      OutletShortData osd = prepareFormResponse.getAssignedOutlet().get(i);
      if (osd.getOutletId().equals(abd.getOutletObj().getOutletId())) {
        setField_outletKvm(new KeyValueModel<>(osd, osd.getOutletName()));
      }
    }

    setField_AppBookingTypeId(abd.getBookingTypeId());
    setField_AppOperation(abd.getAppOperationId());
    setField_ConsumerNo(abd.getConsumerNo());
    setField_BookingDate(abd.getBookingDate());

    for (int i = 0; i < prepareFormResponse.getLobData().size(); i++) {
      LobShortData lsd = prepareFormResponse.getLobData().get(i);
      if (lsd.getLobId().equals(abd.getLobId())) {
        setField_LobKvm(new KeyValueModel<>(lsd, lsd.getLobName()));
      }
    }

    for (int i = 0; i < prepareFormResponse.getSoDataList().size(); i++) {
      SoShortData ssd = prepareFormResponse.getSoDataList().get(i);
      if (ssd.getSoId().equals(abd.getSobId())) {
        setField_SoKvm(new KeyValueModel<SoShortData, String>(ssd, ssd.getSoName()));
      }
    }

    setField_BookingId(abd.getBookingId());
    setField_Idnpksf(abd.getIdnpksf());
    setField_AlasanPinjam(abd.getReason());
    setField_AlasanPinjamCash(abd.getCashReason());
    setField_AppNo(abd.getAppNo());
    setField_PoNo(abd.getPoNo());
    setField_PoDate(abd.getPoDate());
    setField_UpdateTS(abd.getUpdateTs());

    List<KeyValueModel<UUID, String>> poprIdList = abd.getPoprList().stream().map(obj -> {
      KeyValueModel<UUID, String> newObj = new KeyValueModel<>(obj.getPoprId(), obj.getName());
      return newObj;
    }).collect(Collectors.toList());
    setField_PoprList(null, null, poprIdList);

    setField_ConsumerName(abd.getConsumerName());
    setField_ConsumerAddress(abd.getConsumerAddress());
    setField_ConsumerPhone(abd.getConsumerPhone());
    setField_VehicleNo(abd.getVehicleNo());
    field_VehicleNo_OK = true;
    field_VehicleNo_ERROR.postValue(null);
    if (abd.getAppOperationId().equals(Utility.OPERATION_EDIT))
      validate_field_VehicleNo(abd.getVehicleNo(), abd.getBookingDate());

    setField_IsOpenClose(abd.getOpenClose());

    BigDecimal consAmtVal = abd.getConsumerAmt() == null ? BigDecimal.ZERO : abd.getConsumerAmt();
    BigDecimal fifAmtVal = abd.getFifAmt() == null ? BigDecimal.ZERO : abd.getFifAmt();
    BigDecimal bjAmtVal = abd.getBiroJasaAmt() == null ? BigDecimal.ZERO : abd.getBiroJasaAmt();
    setField_ConsumerAmount(consAmtVal);
    setField_FIFAmount(fifAmtVal);
    setField_BiroJasaAmount(bjAmtVal);
    field_ConsAmount_OLD = consAmtVal;
    field_FIFAmount_OLD = fifAmtVal;
    field_BiroJasaAmount_OLD = bjAmtVal;
    // validation_field_PencairanFIF(abd.getOpenClose(), abd.getFifAmt()!=null.intValue());
    setField_BookingAmount();
    setField_FeeMatrix(abd.getMatrixFeeAmt() == null ? BigDecimal.ZERO : abd.getMatrixFeeAmt());
    if (abd.getAtt_PO() != null) {
      HyperLog.d(TAG, fn + "ATT_PO isArchive[" + abd.getAtt_PO().isArchived() + "]");
      setField_Attach_PO_isArchive(abd.getAtt_PO().isArchived());

      if (!abd.getAtt_PO().isArchived())
        setAttachmentPO(abd.getAtt_PO().getId(), abd.getAtt_PO().getFileName(), abd.getPoNo(),
            abd.getPoDate(), poprIdList);
      else
        validation_field_PonoDateAttach(abd.getPoNo(), abd.getPoDate(),
            new File("fakeFileAgarTidakAdaPesanError"), poprIdList);
      //      setAttachment(AppBookingBrowseVM.ATTACHMENT_MODE_PO, abd.getAtt_PO().getId(),
      //          abd.getAtt_PO().getFileName());
    } else {
      setField_Attach_PO_isArchive(false);
    }

    if (abd.getAtt_STNK_KTP() != null) {
      HyperLog.d(TAG, fn + "Att_STNK_KTP isArchive[" + abd.getAtt_STNK_KTP().isArchived() + "]");
      setField_Attach_STNK_isArchive(abd.getAtt_STNK_KTP().isArchived());

      if (!abd.getAtt_STNK_KTP().isArchived())
        setAttachment(UtilityState.ATTCH_STNK.getId(), abd.getAtt_STNK_KTP().getId(),
            abd.getAtt_STNK_KTP().getFileName());
    } else {
      setField_Attach_STNK_isArchive(false);
    }

    if (abd.getAtt_KK() != null) {
      HyperLog.d(TAG, fn + "Att_KK isArchive[" + abd.getAtt_KK().isArchived() + "]");
      setField_Attach_KK_isArchive(abd.getAtt_KK().isArchived());

      if (!abd.getAtt_KK().isArchived())
        setAttachment(UtilityState.ATTCH_KK.getId(), abd.getAtt_KK().getId(),
            abd.getAtt_KK().getFileName());
    } else {
      setField_Attach_KK_isArchive(false);
    }

    if (abd.getAtt_KWITANSI() != null) {
      HyperLog.d(TAG, fn + "ATTCH_BPKB isArchive[" + abd.getAtt_KWITANSI().isArchived() + "]");
      setField_Attach_BPKB_isArchive(abd.getAtt_KWITANSI().isArchived());

      if (!abd.getAtt_KWITANSI().isArchived())
        setAttachment(UtilityState.ATTCH_BPKB.getId(), abd.getAtt_KWITANSI().getId(),
            abd.getAtt_KWITANSI().getFileName());
    } else {
      setField_Attach_BPKB_isArchive(false);
    }

    if (abd.getAtt_SPT() != null) {
      HyperLog.d(TAG, fn + "ATTCH_SPT isArchive[" + abd.getAtt_SPT().isArchived() + "]");
      setField_Attach_SPT_isArchive(abd.getAtt_SPT().isArchived());

      if (!abd.getAtt_SPT().isArchived())
        setAttachment(UtilityState.ATTCH_SPT.getId(), abd.getAtt_SPT().getId(),
            abd.getAtt_SPT().getFileName());
    } else {
      setField_Attach_SPT_isArchive(false);
    }

    if (abd.getAtt_SERAH_TERIMA_UANG() != null) {
      HyperLog.d(TAG,
          fn + "ATTCH_SERAH_TERIMA isArchive[" + abd.getAtt_SERAH_TERIMA_UANG().isArchived() + "]");
      setField_Attach_SERAH_TERIMA_isArchive(abd.getAtt_SERAH_TERIMA_UANG().isArchived());

      if (!abd.getAtt_SERAH_TERIMA_UANG().isArchived())
        setAttachment(UtilityState.ATTCH_SERAHTERIMA.getId(),
            abd.getAtt_SERAH_TERIMA_UANG().getId(), abd.getAtt_SERAH_TERIMA_UANG().getFileName());
    } else {
      setField_Attach_SERAH_TERIMA_isArchive(false);
    }

    if (abd.getAtt_FISIK_MOTOR() != null) {
      HyperLog.d(TAG,
          fn + "ATTCH_FISIK_MOTOR isArchive[" + abd.getAtt_FISIK_MOTOR().isArchived() + "]");
      setField_Attach_FISIK_MOTOR_isArchive(abd.getAtt_FISIK_MOTOR().isArchived());

      if (!abd.getAtt_FISIK_MOTOR().isArchived())
        setAttachment(UtilityState.ATTCH_FISIKMOTOR.getId(),
            abd.getAtt_FISIK_MOTOR().getId(), abd.getAtt_FISIK_MOTOR().getFileName());
    } else {
      setField_Attach_FISIK_MOTOR_isArchive(false);
    }

    if (abd.getAtt_HO() != null) {
      HyperLog.d(TAG, fn + "ATTCH_HO isArchive[" + abd.getAtt_HO().isArchived() + "]");
      setField_Attach_HO_isArchive(abd.getAtt_HO().isArchived());

      if (!abd.getAtt_HO().isArchived())
        setAttachment(UtilityState.ATTCH_HO.getId(),
            abd.getAtt_HO().getId(), abd.getAtt_HO().getFileName());
    }else{
      setField_Attach_HO_isArchive(false);
    }

    if (abd.getAtt_BUKTIRETUR() != null) {
      showBuktiRetur.postValue(true);
      HyperLog.d(TAG,
          fn + "ATTCH_BUKTIRETUR isArchive[" + abd.getAtt_BUKTIRETUR().isArchived() + "]");
      setField_Attach_BUKTIRETUR_isArchive(abd.getAtt_BUKTIRETUR().isArchived());

      if (!abd.getAtt_BUKTIRETUR().isArchived())
        setAttachment(UtilityState.ATTCH_BUKTIRETUR.getId(),
            abd.getAtt_BUKTIRETUR().getId(), abd.getAtt_BUKTIRETUR().getFileName());
    } else {
      setField_Attach_BUKTIRETUR_isArchive(false);
    }


    HyperLog.d(TAG,
        "setField_FeeScheme abd.getSchemeFeeAmt()[" + abd.getSchemeFeeAmt() + "]");
    setField_FeeScheme(abd.getSchemeFeeAmt() == null ? BigDecimal.ZERO : abd.getSchemeFeeAmt());

    setField_CancelReason(abd.getCancelReason(), true);

    AppBookingCashScreenMode smCash = AppBookingCashScreenMode.getScreenMode(false,
        abd.getAppOperationId(), abd.getWfId(), pd != null ? pd.getActTaskDefinitionKey() : null);
    HyperLog.d(TAG, String.format("AppBooking screenMode = %s ", abd.getId(), smCash));

    isLoadProcess = false;

    setReadOnly_AppNoPoNoPoDatePoAttchment2(smCash, null, abd.getAppNo(), abd.getPoNo(),
        abd.getPoDate(), abd.getAtt_PO() != null);

    setFormState(AppBookingFormState.ready(res.getMessage(), false, smCash, null));

  }
}
