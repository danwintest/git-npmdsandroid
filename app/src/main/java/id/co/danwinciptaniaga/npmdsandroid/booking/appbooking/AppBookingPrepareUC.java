package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.booking.NewBookingReponse;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class AppBookingPrepareUC
    extends BaseObservable<AppBookingPrepareUC.Listener, NewBookingReponse> {
  private static String TAG = AppBookingPrepareUC.class.getSimpleName();
  private final AppBookingService appBookingService;
  private final AppExecutors appExecutors;

  @Inject
  public AppBookingPrepareUC(Application app, AppBookingService service,
      AppExecutors appExecutors) {
    super(app);
    this.appBookingService = service;
    this.appExecutors = appExecutors;
  }

  public void prepare(Boolean isTransfer) {
    notifyStart(null, null);
    ListenableFuture<NewBookingReponse> newAppBooking = appBookingService.getNewAppBooking(isTransfer);
    Futures.addCallback(newAppBooking, new FutureCallback<NewBookingReponse>() {
      @Override
      public void onSuccess(@NullableDecl NewBookingReponse result) {
        notifySuccess("Pengajuan Booking berhasil dimuat", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Gagal mempersiapkan data ->" + message + "<-", null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<NewBookingReponse> result) {
    listener.onPrepareAppBookingStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<NewBookingReponse> result) {
    listener.onPrepareAppBookingSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<NewBookingReponse> result) {
    listener.onPrepareAppBookingFailure(result);
  }

  public interface Listener {
    void onPrepareAppBookingStarted();

    void onPrepareAppBookingSuccess(Resource<NewBookingReponse> response);

    void onPrepareAppBookingFailure(Resource<NewBookingReponse> resource);

  }

}
