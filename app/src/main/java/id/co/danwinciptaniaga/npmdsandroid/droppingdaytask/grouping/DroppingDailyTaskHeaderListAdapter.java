package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseHeaderData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiDroppingDayTaskHeaderBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class DroppingDailyTaskHeaderListAdapter extends
    PagingDataAdapter<DroppingDailyTaskBrowseHeaderData, DroppingDailyTaskHeaderListAdapter.ViewHolder> {
  private final static String TAG = DroppingDailyTaskHeaderListAdapter.class.getSimpleName();
  private final DddRecyclerViewAdapterListener mListener;
  private List<DroppingDailyTaskBrowseHeaderData> selectedDdd = new ArrayList<>();
  private Context mContext;
  public AppExecutors appExecutors;

  @Inject
  public DroppingDailyTaskHeaderListAdapter(
      DddRecyclerViewAdapterListener listener,Context ctx,CommonService commonService, AppExecutors appExecutors) {
    super(new DiffCallBack());
    mListener = listener;
    mContext = ctx;
    this.appExecutors = appExecutors;
  }

  @Override
  public int getItemCount() {
    int test =super.getItemCount();
    return test;
  }

  public List<DroppingDailyTaskBrowseHeaderData> getSelectedDdd() {
    return selectedDdd;
  }

  @Override
  public DroppingDailyTaskHeaderListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.li_dropping_day_task_header_browse,
        parent, false);
    return new DroppingDailyTaskHeaderListAdapter.ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final DroppingDailyTaskHeaderListAdapter.ViewHolder holder,
      int position) {
    holder.bind(getItem(position), position);
  }



  interface DddRecyclerViewAdapterListener {
    void onItemClicked(View view, DroppingDailyTaskBrowseHeaderData data, int position);
  }

  private static class DiffCallBack
      extends DiffUtil.ItemCallback<DroppingDailyTaskBrowseHeaderData> {

    @Override
    public boolean areItemsTheSame(@NonNull DroppingDailyTaskBrowseHeaderData oldItem,
        @NonNull DroppingDailyTaskBrowseHeaderData newItem) {
      return oldItem.getCompanyId() == newItem.getCompanyId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull DroppingDailyTaskBrowseHeaderData oldItem,
        @NonNull DroppingDailyTaskBrowseHeaderData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiDroppingDayTaskHeaderBrowseBinding binding;

    public ViewHolder(View view) {
      super(view);
      binding = LiDroppingDayTaskHeaderBrowseBinding.bind(view);
    }

    public void bind(DroppingDailyTaskBrowseHeaderData data, int pos) {
      setObject(data);
      setObjectListener(data, pos);
    }

    private void setObjectListener(DroppingDailyTaskBrowseHeaderData data, int pos) {

      binding.clContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, data, pos);
        }
      });
    }

    private void setObject(DroppingDailyTaskBrowseHeaderData data) {
      binding.bankName.setText(data.getBankName());
      binding.totalAmount.setText(Utility.getFormattedAmt(data.getTotalAmt()));
    }

  }
}
