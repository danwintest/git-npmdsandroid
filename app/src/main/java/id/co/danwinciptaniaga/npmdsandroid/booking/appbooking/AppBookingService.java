package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBankListResponse;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingDetailResponse;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingListResponse;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingMultipleSelectedProcessResultData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingSort;
import id.co.danwinciptaniaga.npmds.data.booking.NewBookingReponse;
import id.co.danwinciptaniaga.npmds.data.common.OtpProcessResponse;
import id.co.danwinciptaniaga.npmds.data.wf.WfHistoryResponse;
import okhttp3.MultipartBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AppBookingService {

  @GET("rest/s/AppBooking")
  ListenableFuture<AppBookingListResponse> getAppBookingList(@Query("page") Integer page,
      @Query("pageSize") Integer pageSize, @Query("filter") AppBookingBrowseFilter filter,
      @Query("sort") AppBookingSort Sort);

  @GET("rest/s/AppBooking/{id}/{isTransfer}")
  ListenableFuture<AppBookingDetailResponse> loadAppBookingData(@Path("id") UUID appBookingId,
      @Path("isTransfer") Boolean isTransfer);

  @GET("rest/s/AppBookingByBookingId/{id}/{isEdit}/{isTransfer}")
  ListenableFuture<AppBookingDetailResponse> loadAppBookingDataByBookingId(
      @Path("id") UUID bookingId, @Path("isEdit") Boolean isEdit,
      @Path("isTransfer") Boolean isTransfer);

  @GET("rest/s/AppBooking/new")
  ListenableFuture<NewBookingReponse> getNewAppBooking(@Query("isTransfer") Boolean isTransfer);

  @GET("rest/s/AppBooking/validateVehicle")
  ListenableFuture<String> validateVehicleNo(@Query("appBookingId") UUID appBookingId,
      @Query("vehicleNo") String vehicleNo,@Query("appOperation") String appOperation,
      @Query("bookingDateString") String bookingDateString);


  @POST("rest/s/AppBooking/validateAppBookingBankAccount")
  ListenableFuture<Boolean> validateAppBookingBankAccount(
      @Query("appBookingId") UUID appBookingId, @Query("bookingId") UUID bookingId,
      @Query("txnTypeId") String txnTypeId, @Query("bankId") UUID bankId,
      @Query("isFromBooking") Boolean isFromBooking,
      @Query("companyId") UUID companyId, @Query("acctNo") String accountNo,
      @Query("acctName") String acctName);

  @Multipart
  @POST("rest/s/AppBooking/saveDraftCashNew")
  ListenableFuture<String> saveDraftCashNew(@Query("id") UUID appBookingID,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("bookingTypeId") String bookingTypeId, @Query("lobId") UUID lobId,
      @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("cashReason") String cashReason, @Query("reason") String reason,
      @Query("applicationNo") String appNo, @Query("poNo") String poNo,
      @Part("poDate") LocalDate poDate, @Query("poprIdList") List<UUID> poprIdList,
      @Query("consumerName") String consumerName, @Query("consumerAddress") String consumerAddress,
      @Query("consumerPhone") String consumerPhone, @Query("vehicleNo") String vehicleNo,
      @Query("closeOpen") Boolean closeOpen, @Query("consumerAmount") BigDecimal consumerAmount,
      @Query("fif") BigDecimal fif, @Query("biroJasa") BigDecimal biroJasa,
      @Query("bookingAmount") BigDecimal bookingAmount, @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee, @Query("cancelReason") String cancelReason,
      @Query("bookingId") UUID bookingId, @Part("updateTs") Date updateTs,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_serah_terima_id") UUID att_serah_terima_id,
      @Part MultipartBody.Part att_serah_terima,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Query("wfStatus") String wfStatus,
      @Query("appOperation") String appOperation,
      @Query("isSaveDraftProcess") Boolean isSaveDraftProcess);

  @Multipart
  @POST("rest/s/AppBooking/submitCashNew")
  ListenableFuture<String> submitCashNew(@Query("id") UUID appBookingID,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soid") String soId, @Query("idnksf") String idnpksf,
      @Query("reason") String reason, @Query("cashReason") String cashReason,
      @Query("applicationNo") String applicationNo, @Query("poNo") String poNo,
      @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress,
      @Query("consumerPhone") String consumerPhone, @Query("vehicleNo") String vehicleNo,
      @Query("closeOpen") Boolean closeOpen, @Query("consumerAmount") BigDecimal consumerAmt,
      @Query("biroJasaAmount") BigDecimal biroJasaAmt, @Query("fifAmt") BigDecimal fifAmt,
      @Query("bookingAmount") BigDecimal bookingAmount, @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_serah_terima_id") UUID att_serah_terima_id,
      @Part MultipartBody.Part att_serah_terima,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor, @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/submitRevisionCashNew")
  ListenableFuture<Boolean> submitRevisionCashNew(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingID,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soid") String soId, @Query("idnksf") String idnpksf,
      @Query("reason") String reason, @Query("cashReason") String cashReason,
      @Query("applicationNo") String applicationNo, @Query("poNo") String poNo,
      @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress,
      @Query("consumerPhone") String consumerPhone, @Query("vehicleNo") String vehicleNo,
      @Query("closeOpen") Boolean closeOpen, @Query("consumerAmount") BigDecimal consumerAmt,
      @Query("biroJasaAmount") BigDecimal biroJasaAmt, @Query("fifAmt") BigDecimal fifAmt,
      @Query("bookingAmount") BigDecimal bookingAmount, @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_serah_terima_id") UUID att_serah_terima_id,
      @Part MultipartBody.Part att_serah_terima,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor, @Part("updateTs") Date updateTs);


  @Multipart
  @POST("rest/s/AppBooking/saveDraftCashTransferCancel")
  ListenableFuture<String> saveDraftCashTransferCancel(
      @Query("appBookingId") UUID appBookingId, @Query("bookingTypeId") String bookingTypeId,
      @Query("bookingId") UUID bookingId, @Query("cancelReason") String cancelReason,
      @Query("att_HO_id") UUID att_HO_id,
      @Part MultipartBody.Part att_HO, @Part("updateTs") Date updateTs,
      @Query("isNewDocument") Boolean isNewDocument);

  @Multipart
  @POST("rest/s/AppBooking/submitCashTransfersCancel")
  ListenableFuture<String> submitCashTransfersCancel(
      @Query("appBookingId") UUID appBookingId, @Query("bookingTypeId") String bookingTypeId,
      @Query("bookingId") UUID bookingId, @Query("cancelReason") String cancelReason,
      @Query("att_HO_id") UUID att_HO_id,
      @Part MultipartBody.Part att_HO, @Part("updateTs") Date updateTs,
      @Query("isNewDocument")Boolean isNewDocument);

  @Multipart
  @POST("rest/s/AppBooking/submitRevisionCashCancel")
  ListenableFuture<Boolean> submitRevisionCashCancel(
      @Query("proTaskId") UUID procTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Query("bookingTypeId") String bookingTypeId,
      @Query("bookingId") UUID bookingId, @Query("cancelReason") String cancelReason,
      @Query("att_HO_id") UUID att_HO_id,
      @Part MultipartBody.Part att_HO, @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/submitRevisionTransferCancel")
  ListenableFuture<Boolean> submitRevisionTransferCancel(
      @Query("proTaskId") UUID procTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Query("bookingTypeId") String bookingTypeId,
      @Query("bookingId") UUID bookingId, @Query("cancelReason") String cancelReason,
      @Query("att_HO_id") UUID att_HO_id,
      @Part MultipartBody.Part att_HO, @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/saveDraftCashEdit")
  ListenableFuture<String> saveDraftCashEdit(@Query("id") UUID appBookingID,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("reason") String reason, @Query("cashReason") String cashReason,
      @Query("applicationNo") String appNo, @Query("poNo") String poNo,
      @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress,
      @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo, @Query("closeOpen") Boolean closeOpen,
      @Query("consumerAmount") BigDecimal consumerAmount, @Query("biroJasa") BigDecimal biroJasa,
      @Query("fif") BigDecimal fif, @Query("bookingAmount") BigDecimal bookingAmount,
      @Query("matrixFee") BigDecimal matrixFee, @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_serah_terima_id") UUID att_serah_terima_id,
      @Part MultipartBody.Part att_serah_terima,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor, @Query("bookingId") UUID bookingId,
      @Part("updateTs") Date updateTs, @Query("isSaveDraftProcess") Boolean isSaveDraftProcess,
      @Query("att_buktiretur_id") UUID att_buktiretur_id,
      @Part MultipartBody.Part att_buktiretur, @Query("isNewDocument") Boolean isNewDocument);

  @Multipart
  @POST("rest/s/AppBooking/submitCashEdit")
  ListenableFuture<String> submitCashEdit(@Query("id") UUID appBookingId,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("reason") String reason, @Query("cashReason") String cashReason,
      @Query("applicationNo") String appNo, @Query("poNo") String poNo,
      @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress, @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo, @Query("closeOpen") Boolean closeOpen,
      @Query("consumerAmount") BigDecimal consumerAmount, @Query("biroJasa") BigDecimal biroJasa,
      @Query("fif") BigDecimal fif, @Query("bookingAmount") BigDecimal bookingAmount,
      @Query("matrixFee") BigDecimal matrixFee, @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_serah_terima_id") UUID att_serah_terima_id,
      @Part MultipartBody.Part att_serah_terima,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Query("att_buktiretur_id") UUID att_buktiretur_id,
      @Part MultipartBody.Part att_buktiretur, @Query("bookingId") UUID bookingId,
      @Part("updateTs") Date updateTs, @Query("isNewDocument") Boolean isNewDocument);

  @Multipart
  @POST("rest/s/AppBooking/submitTransferEdit")
  ListenableFuture<String> submitTransferEdit(@Query("id") UUID appBookingId,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("reason") String reason,
      @Query("applicationNo") String appNo, @Query("poNo") String poNo,
      @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress,
      @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo, @Query("closeOpen") Boolean closeOpen,
      @Query("consumerAmount") BigDecimal consumerAmount,
      @Query("consumerBankId") UUID consBankId,
      @Query("consumerOtherBank") String consOtherBank,
      @Query("consumerAccountNo") String consAccountNo,
      @Query("consumerAccountName") String consAccountName,
      @Query("isValidCons") Boolean isValidCons,
      @Query("bjAmount") BigDecimal bjAmount,
      @Query("bjBankId") UUID bjBankId,
      @Query("bjOtherBank") String bjOtherBank,
      @Query("bjAccountNo") String bjAccountNo,
      @Query("bjAccountName") String bjAccountName,
      @Query("isValidBj") Boolean isValidBj,
      @Query("fifAmount") BigDecimal fifAmount,
      @Query("fifBankId") UUID fifBankId,
      @Query("fifOtherBank") String fifOtherBank,
      @Query("fifAccountNo") String fifAccountNo,
      @Query("fifAccountName") String fifAccountName,
      @Query("isValidFIF") Boolean isValidFIF,
      @Query("bookingAmount") BigDecimal bookingAmount,
      @Query("matrixFee") BigDecimal matrixFee, @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("bookingId") UUID bookingId, @Part("updateTs") Date updateTs,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Query("att_buktiretur_id") UUID att_buktiretur_id,
      @Part MultipartBody.Part att_buktiretur,
      @Part("isNewDocument") Boolean isNewDocument);

  @Multipart
  @POST("rest/s/AppBooking/submitRevisionCashEdit")
  ListenableFuture<Boolean> submitRevisionCashEdit(
      @Query("proTaskId") UUID procTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingID, @Query("outletId") UUID outletId,
      @Part("bookingDate") LocalDate bookingDate, @Query("lobId") UUID lobId,
      @Query("soId") String soId, @Query("idnpksf") String idnpksf, @Query("reason") String reason,
      @Query("cashReason") String cashReason, @Query("applicationNo") String appNo,
      @Query("poNo") String poNo, @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress, @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo, @Query("closeOpen") Boolean closeOpen,
      @Query("consumerAmount") BigDecimal consumerAmount, @Query("biroJasa") BigDecimal biroJasa,
      @Query("fif") BigDecimal fif, @Query("bookingAmount") BigDecimal bookingAmount,
      @Query("matrixFee") BigDecimal matrixFee, @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_serah_terima_id") UUID att_serah_terima_id,
      @Part MultipartBody.Part att_serah_terima,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Query("bookingId") UUID bookingId, @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/saveDraftTransferNew")
  ListenableFuture<String> saveDraftTransferNew(@Query("id") UUID appBookingId,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("reason") String reason, @Query("applicationNo") String appNo,
      @Query("poNo") String poNo, @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress, @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo, @Query("closeOpen") Boolean closeOpen,
      @Query("consumerAmount") BigDecimal consumerAmount, @Query("consumerBankId") UUID consBankId,
      @Query("consumerOtherBank") String consOtherBank,
      @Query("consumerAccountNo") String consAccountNo,
      @Query("consumerAccountName") String consAccountName, @Query("bjAmount") BigDecimal bjAmount,
      @Query("bjBankId") UUID bjBankId, @Query("bjOtherBank") String bjOtherBank,
      @Query("bjAccountNo") String bjAccountNo, @Query("bjAccountName") String bjAccountName,
      @Query("fifAmount") BigDecimal fifAmount, @Query("fifBankId") UUID fifBankId,
      @Query("fifOtherBank") String fifOtherBank, @Query("fifAccountNo") String fifAccountNo,
      @Query("fifAccountName") String fifAccountName,
      @Query("bookingAmount") BigDecimal bookingAmount, @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Part("updateTs") Date updateTs,
      @Query("wfStatus") String wfStatus,
      @Query("appOperation") String appOperation,
      @Query("isValidCons") Boolean isValidCons,
      @Query("isValidBJ") Boolean isValidBJ,
      @Query("isValidFIF") Boolean isValidFIF,
      @Query("isSaveDraftProcess")Boolean isSaveDraftProcess);

  @Multipart
  @POST("rest/s/AppBooking/saveDraftTransferEdit")
  ListenableFuture<String> saveDraftTransferEdit(@Query("id") UUID appBookingId,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("reason") String reason, @Query("applicationNo") String appNo,
      @Query("poNo") String poNo, @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress, @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo, @Query("closeOpen") Boolean closeOpen,
      @Query("consumerAmount") BigDecimal consumerAmount, @Query("consumerBankId") UUID consBankId,
      @Query("consumerOtherBank") String consOtherBank,
      @Query("consumerAccountNo") String consAccountNo,
      @Query("consumerAccountName") String consAccountName,
      @Query("isValidCons") Boolean isValidCons,
      @Query("bjAmount") BigDecimal bjAmount,
      @Query("bjBankId") UUID bjBankId, @Query("bjOtherBank") String bjOtherBank,
      @Query("bjAccountNo") String bjAccountNo, @Query("bjAccountName") String bjAccountName,
      @Query("isValidBj") Boolean isValidBj,
      @Query("fifAmount") BigDecimal fifAmount, @Query("fifBankId") UUID fifBankId,
      @Query("fifOtherBank") String fifOtherBank, @Query("fifAccountNo") String fifAccountNo,
      @Query("fifAccountName") String fifAccountName,
      @Query("isValidFIF") Boolean isValidFIF,
      @Query("bookingAmount") BigDecimal bookingAmount, @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Part("updateTs") Date updateTs,
      @Query("att_buktiretur_id") UUID att_buktiretur_id,
      @Part MultipartBody.Part att_buktiretur,
      @Query("bookingId") UUID bookingId,
      @Query("isSaveDraftProcess") Boolean isSaveDraftProcess,
      @Query("isNewDocument") Boolean isNewDocument);


  @Multipart
  @POST("rest/s/AppBooking/submitTransferNew")
  ListenableFuture<String> submitTransferNew(@Query("id") UUID appBookingID,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("reason") String reason, @Query("applicationNo") String appNo,
      @Query("poNo") String poNo, @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress, @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo, @Query("closeOpen") Boolean closeOpen,
      @Query("consumerAmount") BigDecimal consumerAmount, @Query("consumerBankId") UUID consBankId,
      @Query("consumerOtherBank") String consOtherBank,
      @Query("consumerAccountNo") String consAccountNo,
      @Query("consumerAccountName") String consAccountName, @Query("bjAmount") BigDecimal bjAmount,
      @Query("bjBankId") UUID bjBankId, @Query("bjOtherBank") String bjOtherBank,
      @Query("bjAccountNo") String bjAccountNo, @Query("bjAccountName") String bjAccountName,
      @Query("fifAmount") BigDecimal fifAmount, @Query("fifBankId") UUID fifBankId,
      @Query("fifOtherBank") String fifOtherBank, @Query("fifAccountNo") String fifAccountNo,
      @Query("fifAccountName") String fifAccountName,
      @Query("bookingAmount") BigDecimal bookingAmount, @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Part("updateTs") Date updateTs,
      @Query("isValidCons") Boolean isValidCons,
      @Query("isValidBJ") Boolean isValidBJ,
      @Query("isValidFIF") Boolean isValidFIF);

  @Multipart
  @POST("rest/s/AppBooking/submitRevisionTransferEdit")
  ListenableFuture<Boolean> submitRevisionTransferEdit(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment,
      @Query("id") UUID appBookingId,
      @Query("outletId") UUID outletId,
      @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId,
      @Query("idnpksf") String idnpksf,
      @Query("reason") String reason,
      @Query("applicationNo") String appNo,
      @Query("poNo") String poNo,
      @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList,
      @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress,
      @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo,
      @Query("closeOpen") Boolean closeOpen,
      @Query("consumerAmount") BigDecimal consumerAmount,
      @Query("consumerBankId") UUID consBankId,
      @Query("consumerOtherBank") String consOtherBank,
      @Query("consumerAccountNo") String consAccountNo,
      @Query("consumerAccountName") String consAccountName,
      @Query("isValidCons") Boolean isValidCons,
      @Query("bjAmount") BigDecimal bjAmount,
      @Query("bjBankId") UUID bjBankId,
      @Query("bjOtherBank") String bjOtherBank,
      @Query("bjAccountNo") String bjAccountNo,
      @Query("bjAccountName") String bjAccountName,
      @Query("isValidBj") Boolean isValidBj,
      @Query("fifAmount") BigDecimal fifAmount,
      @Query("fifBankId") UUID fifBankId,
      @Query("fifOtherBank") String fifOtherBank,
      @Query("fifAccountNo") String fifAccountNo,
      @Query("fifAccountName") String fifAccountName,
      @Query("isValidFIF") Boolean isValidFIF,
      @Query("bookingAmount") BigDecimal bookingAmount,
      @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Query("bookingId") UUID bookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processWfReject")
  ListenableFuture<Boolean> processWfReject(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingID,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_DOC_APPROVE_NEW")
  ListenableFuture<Boolean> processCashWF_DOC_APPROVE_NEW(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_DOC_RETURN_NEW")
  ListenableFuture<Boolean> processCashWF_DOC_RETURN_NEW(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_CAPTAIN_APPROVE_NEW")
  ListenableFuture<Boolean> processCashWF_CAPTAIN_APPROVE_NEW(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_CAPTAIN_RETURN_NEW")
  ListenableFuture<Boolean> processCashWF_CAPTAIN_RETURN_NEW(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_AOC_APPROVE_EDIT")
  ListenableFuture<Boolean> processCashWF_AOC_APPROVE_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_AOC_RETURN_EDIT")
  ListenableFuture<Boolean> processCashWF_AOC_RETURN_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_AOC_REJECT_EDIT")
  ListenableFuture<Boolean> processCashWF_AOC_REJECT_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_CAPTAIN_APPROVE_EDIT")
  ListenableFuture<Boolean> processCashWF_CAPTAIN_APPROVE_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_CAPTAIN_RETURN_EDIT")
  ListenableFuture<Boolean> processCashWF_CAPTAIN_RETURN_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_CAPTAIN_REJECT_EDIT")
  ListenableFuture<Boolean> processCashWF_CAPTAIN_REJECT_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_DIRECTOR_APPROVE_EDIT")
  ListenableFuture<Boolean> processCashWF_DIRECTOR_APPROVE_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_DIRECTOR_RETURN_EDIT")
  ListenableFuture<Boolean> processCashWF_DIRECTOR_RETURN_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_DIRECTOR_REJECT_EDIT")
  ListenableFuture<Boolean> processCashWF_DIRECTOR_REJECT_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_FINRETURN_APPROVE_EDIT")
  ListenableFuture<Boolean> processCashWF_FINRETURN_APPROVE_EDIT(
      @Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_FINRETURN_RETURN_EDIT")
  ListenableFuture<Boolean> processCashWF_FINRETURN_RETURN_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_FINRETURN_REJECT_EDIT")
  ListenableFuture<Boolean> processCashWF_FINRETURN_REJECT_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_AOC_APPROVE_CANCEL")
  ListenableFuture<Boolean> processCashWF_AOC_APPROVE_CANCEL(
      @Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_AOC_RETURN_CANCEL")
  ListenableFuture<Boolean> processCashWF_AOC_RETURN_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_AOC_REJECT_CANCEL")
  ListenableFuture<Boolean> processCashWF_AOC_REJECT_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_CAPTAIN_APPROVE_CANCEL")
  ListenableFuture<Boolean> processCashWF_CAPTAIN_APPROVE_CANCEL(
      @Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_CAPTAIN_RETURN_CANCEL")
  ListenableFuture<Boolean> processCashWF_CAPTAIN_RETURN_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_CAPTAIN_REJECT_CANCEL")
  ListenableFuture<Boolean> processCashWF_CAPTAIN_REJECT_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_DIRECTOR_APPROVE_CANCEL")
  ListenableFuture<Boolean> processCashWF_DIRECTOR_APPROVE_CANCEL(
      @Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_DIRECTOR_RETURN_CANCEL")
  ListenableFuture<Boolean> processCashWF_DIRECTOR_RETURN_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_DIRECTOR_REJECT_CANCEL")
  ListenableFuture<Boolean> processCashWF_DIRECTOR_REJECT_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_FINRETURN_APPROVE_CANCEL")
  ListenableFuture<Boolean> processCashWF_FINRETURN_APPROVE_CANCEL(
      @Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_FINRETURN_RETURN_CANCEL")
  ListenableFuture<Boolean> processCashWF_FINRETURN_RETURN_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processCashWF_FINRETURN_REJECT_CANCEL")
  ListenableFuture<Boolean> processCashWF_FINRETURN_REJECT_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_AOC_APPROVE_NEW")
  ListenableFuture<Boolean> processTransWF_AOC_APPROVE_NEW(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs, @Query("batchId") String batchId,
      @Query("opTransfer") Boolean opTransfer);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_AOC_RETURN_NEW")
  ListenableFuture<Boolean> processTransWF_AOC_RETURN_NEW(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_CAPTAIN_APPROVE_NEW")
  ListenableFuture<Boolean> processTransWF_CAPTAIN_APPROVE_NEW(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs, @Query("batchId") String batchId,
      @Query("opTransfer") Boolean opTransfer);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_CAPTAIN_RETURN_NEW")
  ListenableFuture<Boolean> processTransWF_CAPTAIN_RETURN_NEW(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_KASIR_SUBMIT_REVISION_NEW")
  ListenableFuture<Boolean> processTransWF_KASIR_SUBMIT_REVISION_NEW(
      @Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingID,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("reason") String reason, @Query("applicationNo") String appNo,
      @Query("poNo") String poNo, @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress,
      @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo, @Query("closeOpen") Boolean closeOpen,
      @Query("consumerAmount") BigDecimal consumerAmount, @Query("consumerBankId") UUID consBankId,
      @Query("consumerOtherBank") String consOtherBank,
      @Query("consumerAccountNo") String consAccountNo,
      @Query("consumerAccountName") String consAccountName,
      @Query("isValidCons") Boolean isValidCons, @Query("bjAmount") BigDecimal bjAmount,
      @Query("bjBankId") UUID bjBankId, @Query("bjOtherBank") String bjOtherBank,
      @Query("bjAccountNo") String bjAccountNo, @Query("bjAccountName") String bjAccountName,
      @Query("isValidBj") Boolean isValidBj,
      @Query("fifAmount") BigDecimal fifAmount, @Query("fifBankId") UUID fifBankId,
      @Query("fifOtherBank") String fifOtherBank, @Query("fifAccountNo") String fifAccountNo,
      @Query("fifAccountName") String fifAccountName, @Query("isValidFIF") Boolean isValidFIF,
      @Query("bookingAmount") BigDecimal bookingAmount, @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Part("updateTs") Date updateTs);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_CUS_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_CUS_RETURN_NEW(@Query("txnProcTaskId") UUID txnProcTaskId,
      @Query("comment") String comment, @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_CUS_APPROVE_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_CUS_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Part("transferDate") LocalDate transferDate, @Query("cbaId") UUID cbaId,
      @Query("isManual") Boolean isManual, @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_CUS_APPROVE_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_CUS_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_CUS_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_CUS_RETURN_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_KASIR_CUS_REVISION_NEW")
  ListenableFuture<Boolean> processTransWF_KASIR_CUS_REVISION_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId,
      @Query("comment") String comment, @Query("appBookingId") UUID appBookingId,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("reason") String reason, @Query("applicationNo") String appNo,
      @Query("poNo") String poNo, @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress,
      @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo,
      @Query("consumerAmount") BigDecimal consumerAmount, @Query("consumerBankId") UUID consBankId,
      @Query("consumerOtherBank") String consOtherBank,
      @Query("consumerAccountNo") String consAccountNo,
      @Query("consumerAccountName") String consAccountName,
      @Query("isValidCons") Boolean isValidCons,
      @Query("bookingAmount") BigDecimal bookingAmount, @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_CUS_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_CUS_RETURN_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_CUS_APPROVE_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_CUS_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @POST("rest/s/AppBooking/BankAccountByOutlet")
  ListenableFuture<AppBookingBankListResponse> getBankAccountByOutlet(@Query("page") Integer page,
      @Query("pageSize") Integer pageSize, @Query("filter") String filterString,
      @Query("outletId") UUID outletId, @Query("type") String type);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_CUS_APPROVE_NEW")
  ListenableFuture<OtpProcessResponse> processTransTxn_WF_FINSPV_CUS_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("otp") String otp, @Query("isManual") Boolean isManual);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_CUS_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_CUS_RETURN_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_CUS_REJECT_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_CUS_REJECT_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_BJ_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_BJ_RETURN_NEW(@Query("txnProcTaskId") UUID txnProcTaskId,
      @Query("comment") String comment, @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_BJ_APPROVE_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_BJ_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Part("transferDate") LocalDate transferDate, @Query("cbaId") UUID cbaId,
      @Query("isManual") Boolean isManual, @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_BJ_APPROVE_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_BJ_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_BJ_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_BJ_RETURN_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_KASIR_BJ_REVISION_NEW")
  ListenableFuture<Boolean> processTransWF_KASIR_BJ_REVISION_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("txnBjId") UUID txnBjId,
      @Query("comment") String comment, @Query("appBookingId") UUID appBookingId,
      @Query("outletId") UUID outletId, @Query("bjAmount") BigDecimal bjAmount,
      @Query("bjBankId") UUID bjBankId, @Query("bjOtherBank") String bjOtherBank,
      @Query("bjAccountNo") String bjAccountNo, @Query("bjAccountName") String bjAccountName,
      @Query("isValidBj") Boolean isValidBj, @Query("bookingAmount") BigDecimal bookingAmount,
      @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_BJ_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_BJ_RETURN_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_BJ_APPROVE_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_BJ_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_BJ_APPROVE_NEW")
  ListenableFuture<OtpProcessResponse> processTransTxn_WF_FINSPV_BJ_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("otp") String otp, @Query("isManual") Boolean isManual);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_BJ_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_BJ_RETURN_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_BJ_REJECT_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_BJ_REJECT_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_FIF_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_FIF_RETURN_NEW(@Query("txnProcTaskId") UUID txnProcTaskId,
      @Query("comment") String comment, @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_FIF_APPROVE_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_FIF_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Part("transferDate") LocalDate transferDate, @Query("cbaId") UUID cbaId,
      @Query("isManual") Boolean isManual, @Query("txnId") UUID txnId);


  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_FIF_APPROVE_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_FIF_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnFIFUpdateTs") Date txnFIFUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_FIF_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_FIF_RETURN_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnFIFUpdateTs") Date txnFIFUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_KASIR_FIF_REVISION_NEW")
  ListenableFuture<Boolean> processTransWF_KASIR_FIF_REVISION_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("txnFIFId") UUID txnFIFId,
      @Query("comment") String comment, @Query("appBookingId") UUID appBookingId,
      @Query("outletId") UUID outletId, @Query("closeOpen") Boolean closeOpen,
      @Query("fifAmount") BigDecimal fifAmount,
      @Query("fifBankId") UUID fifBankId, @Query("fifOtherBank") String fifOtherBank,
      @Query("fifAccountNo") String fifAccountNo, @Query("fifAccountName") String fifAccountName,
      @Query("isValidFif") Boolean isValidFif, @Query("bookingAmount") BigDecimal bookingAmount,
      @Part("txnFIFUpdateTs") Date txnFIFUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_FIF_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_FIF_RETURN_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnFIFUpdateTs") Date txnFIFUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_FIF_APPROVE_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_FIF_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnFIFUpdateTs") Date txnFIFUpdateTs);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_FIF_APPROVE_NEW")
  ListenableFuture<OtpProcessResponse> processTransTxn_WF_FINSPV_FIF_APPROVE_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("otp") String otp, @Query("isManual") Boolean isManual);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_FIF_RETURN_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_FIF_RETURN_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_FIF_REJECT_NEW")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_FIF_REJECT_NEW(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_AOC_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransWF_AOC_APPROVE_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs, @Query("batchId") String batchId,
      @Query("opTransfer") Boolean opTransfer);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_AOC_RETURN_EDIT")
  ListenableFuture<Boolean> processTransWF_AOC_RETURN_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_AOC_REJECT_EDIT")
  ListenableFuture<Boolean> processTransWF_AOC_REJECT_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_CAPTAIN_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransWF_CAPTAIN_APPROVE_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs, @Query("opTransfer") Boolean opTransfer);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_CAPTAIN_RETURN_EDIT")
  ListenableFuture<Boolean> processTransWF_CAPTAIN_RETURN_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_CAPTAIN_REJECT_EDIT")
  ListenableFuture<Boolean> processTransWF_CAPTAIN_REJECT_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_KASIR_SUBMIT_REVISION_EDIT")
  ListenableFuture<Boolean> processTransWF_KASIR_SUBMIT_REVISION_EDIT(
      @Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingID,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("reason") String reason, @Query("applicationNo") String appNo,
      @Query("poNo") String poNo, @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress,
      @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo, @Query("closeOpen") Boolean closeOpen,
      @Query("consumerAmount") BigDecimal consumerAmount, @Query("consumerBankId") UUID consBankId,
      @Query("consumerOtherBank") String consOtherBank,
      @Query("consumerAccountNo") String consAccountNo,
      @Query("consumerAccountName") String consAccountName,
      @Query("isValidCons") Boolean isValidCons, @Query("bjAmount") BigDecimal bjAmount,
      @Query("bjBankId") UUID bjBankId, @Query("bjOtherBank") String bjOtherBank,
      @Query("bjAccountNo") String bjAccountNo, @Query("bjAccountName") String bjAccountName,
      @Query("isValidBj") Boolean isValidBj,
      @Query("fifAmount") BigDecimal fifAmount, @Query("fifBankId") UUID fifBankId,
      @Query("fifOtherBank") String fifOtherBank, @Query("fifAccountNo") String fifAccountNo,
      @Query("fifAccountName") String fifAccountName, @Query("isValidFIF") Boolean isValidFIF,
      @Query("bookingAmount") BigDecimal bookingAmount, @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Query("bookingId") UUID bookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_DIRECTOR_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransWF_DIRECTOR_APPROVE_EDIT(
      @Query("proTaskId") UUID procTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Query("bookingId") UUID bookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_DIRECTOR_RETURN_EDIT")
  ListenableFuture<Boolean> processTransWF_DIRECTOR_RETURN_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_DIRECTOR_REJECT_EDIT")
  ListenableFuture<Boolean> processTransWF_DIRECTOR_REJECT_EDIT(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_CUS_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_CUS_RETURN_EDIT(@Query("txnProcTaskId") UUID txnProcTaskId,
      @Query("comment") String comment, @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_CUS_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_CUS_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_CUS_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_CUS_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Part("transferDate") LocalDate transferDate, @Query("cbaId") UUID cbaId,
      @Query("isManual") Boolean isManual, @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_DIRECTOR_CUS_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_DIRECTOR_CUS_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Query("bookingId") UUID bookingId,
      @Query("consAmt") BigDecimal consAmt, @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_DIRECTOR_CUS_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_DIRECTOR_CUS_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_DIRECTOR_CUS_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_DIRECTOR_CUS_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_KASIR_CUS_REVISION_EDIT")
  ListenableFuture<Boolean> processTransWF_KASIR_CUS_REVISION_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId,
      @Query("comment") String comment, @Query("appBookingId") UUID appBookingId,
      @Query("outletId") UUID outletId, @Part("bookingDate") LocalDate bookingDate,
      @Query("lobId") UUID lobId, @Query("soId") String soId, @Query("idnpksf") String idnpksf,
      @Query("reason") String reason, @Query("applicationNo") String appNo,
      @Query("poNo") String poNo, @Part("poDate") LocalDate poDate,
      @Query("att_po_id") UUID att_po_id,
      @Part MultipartBody.Part att_po,
      @Query("poprIdList") List<UUID> poprIdList, @Query("consumerName") String consumerName,
      @Query("consumerAddress") String consumerAddress,
      @Query("consumerPhone") String consumerPhone,
      @Query("vehicleNo") String vehicleNo,
      @Query("consumerAmount") BigDecimal consumerAmount, @Query("consumerBankId") UUID consBankId,
      @Query("consumerOtherBank") String consOtherBank,
      @Query("consumerAccountNo") String consAccountNo,
      @Query("consumerAccountName") String consAccountName,
      @Query("isValidCons") Boolean isValidCons,
      @Query("bookingAmount") BigDecimal bookingAmount, @Query("matrixFee") BigDecimal matrixFee,
      @Query("schemeFee") BigDecimal schemeFee,
      @Query("att_stnk_id") UUID att_stnk_id,
      @Part MultipartBody.Part att_stnk,
      @Query("att_kk_id") UUID att_kk_id,
      @Part MultipartBody.Part att_kk,
      @Query("att_bpkb_id") UUID att_bpkb_id,
      @Part MultipartBody.Part att_bpkb,
      @Query("att_spt_id") UUID att_spt_id,
      @Part MultipartBody.Part att_spt,
      @Query("att_fisik_motor_id") UUID att_fisik_motor_id,
      @Part MultipartBody.Part att_fisik_motor,
      @Query("att_buktiretur_id") UUID att_buktiretur_id,
      @Part MultipartBody.Part att_buktiretur, @Query("bookingId") UUID bookingId,
      @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_CUS_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_CUS_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_CUS_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_CUS_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_CUS_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_CUS_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_CUS_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_CUS_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_CUS_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_CUS_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_CUS_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_CUS_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnConsUpdateTs") Date txnConsUpdateTs);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_CUS_APPROVE_EDIT")
  ListenableFuture<OtpProcessResponse> processTransTxn_WF_FINSPV_CUS_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("otp") String otp, @Query("isManual") Boolean isManual);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_CUS_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_CUS_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_CUS_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_CUS_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_BJ_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_BJ_RETURN_EDIT(@Query("txnProcTaskId") UUID txnProcTaskId,
      @Query("comment") String comment, @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_BJ_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_BJ_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_BJ_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_BJ_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Part("transferDate") LocalDate transferDate, @Query("cbaId") UUID cbaId,
      @Query("isManual") Boolean isManual, @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_DIRECTOR_BJ_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_DIRECTOR_BJ_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Query("bookingId") UUID bookingId,
      @Query("bjAmt") BigDecimal bjAmt, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_DIRECTOR_BJ_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_DIRECTOR_BJ_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_DIRECTOR_BJ_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_DIRECTOR_BJ_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_KASIR_BJ_REVISION_EDIT")
  ListenableFuture<Boolean> processTransWF_KASIR_BJ_REVISION_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId,
      @Query("comment") String comment, @Query("appBookingId") UUID appBookingId,
      @Query("outletId") UUID outletId, @Query("txnBjId") UUID txnBjId,
      @Query("bjAmount") BigDecimal bjAmount, @Query("bjBankId") UUID bjBankId,
      @Query("bjOtherBank") String bjOtherBank,
      @Query("bjAccountNo") String bjAccountNo,
      @Query("bjAccountName") String bjAccountName,
      @Query("isValidBj") Boolean isValidBj,
      @Query("bookingAmount") BigDecimal bookingAmount,
      @Query("att_buktiretur_id") UUID att_buktiretur_id,
      @Part MultipartBody.Part att_buktiretur, @Query("bookingId") UUID bookingId,
      @Part("txnBjUpdateTs") Date txnBjUpdateTs);


  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_BJ_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_BJ_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_BJ_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_BJ_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_BJ_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_BJ_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_BJ_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_BJ_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_BJ_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_BJ_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_BJ_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_BJ_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnBjUpdateTs") Date txnBjUpdateTs);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_BJ_APPROVE_EDIT")
  ListenableFuture<OtpProcessResponse> processTransTxn_WF_FINSPV_BJ_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("otp") String otp, @Query("isManual") Boolean isManual);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_BJ_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_BJ_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_BJ_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_BJ_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_FIF_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_FIF_RETURN_EDIT(@Query("txnProcTaskId") UUID txnProcTaskId,
      @Query("comment") String comment, @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_FIF_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_FIF_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_FINST_FIF_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINST_FIF_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Part("transferDate") LocalDate transferDate, @Query("cbaId") UUID cbaId,
      @Query("isManual") Boolean isManual, @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_DIRECTOR_FIF_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_DIRECTOR_FIF_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Query("bookingId") UUID bookingId,
      @Query("fifAmt") BigDecimal fifAmt, @Part("txnFifUpdateTs") Date txnFifUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_DIRECTOR_FIF_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_DIRECTOR_FIF_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnFifUpdateTs") Date txnFifUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_DIRECTOR_FIF_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_DIRECTOR_FIF_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnFifUpdateTs") Date txnFifUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_KASIR_FIF_REVISION_EDIT")
  ListenableFuture<Boolean> processTransWF_KASIR_FIF_REVISION_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment, @Query("appBookingId") UUID appBookingId,
      @Query("outletId") UUID outletId, @Query("txnFifId") UUID txnFifId, @Query("closeOpen") Boolean closeOpen,
      @Query("fifAmount") BigDecimal fifAmount,
      @Query("fifBankId") UUID fifBankId, @Query("fifOtherBank") String fifOtherBank,
      @Query("fifAccountNo") String fifAccountNo, @Query("fifAccountName") String fifAccountName,
      @Query("isValidFif") Boolean isValidFif, @Query("bookingAmount") BigDecimal bookingAmount,
      @Part MultipartBody.Part att_buktiretur,@Query("bookingId") UUID bookingId,
      @Part("txnFifUpdateTs") Date txnFifUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_FIF_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_FIF_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnFifUpdateTs") Date txnFifUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_FIF_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_FIF_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnFifUpdateTs") Date txnFifUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_AOC_FIF_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_AOC_FIF_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnFifUpdateTs") Date txnFifUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_FIF_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_FIF_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnFifUpdateTs") Date txnFifUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_FIF_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_FIF_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("appBookingId") UUID appBookingId, @Part("txnFifUpdateTs") Date txnFifUpdateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransTxn_WF_CAPTAIN_FIF_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_CAPTAIN_FIF_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("id") UUID appBookingId, @Part("txnFifUpdateTs") Date txnFifUpdateTs);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_FIF_APPROVE_EDIT")
  ListenableFuture<OtpProcessResponse> processTransTxn_WF_FINSPV_FIF_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("otp") String otp, @Query("isManual") Boolean isManual);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_FIF_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_FIF_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINSPV_FIF_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINSPV_FIF_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_AOC_APPROVE_CANCEL")
  ListenableFuture<Boolean> processTransWF_AOC_APPROVE_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_AOC_RETURN_CANCEL")
  ListenableFuture<Boolean> processTransWF_AOC_RETURN_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_AOC_REJECT_CANCEL")
  ListenableFuture<Boolean> processTransWF_AOC_REJECT_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_CAPTAIN_APPROVE_CANCEL")
  ListenableFuture<Boolean> processTransWF_CAPTAIN_APPROVE_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_CAPTAIN_RETURN_CANCEL")
  ListenableFuture<Boolean> processTransWF_CAPTAIN_RETURN_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_CAPTAIN_REJECT_CANCEL")
  ListenableFuture<Boolean> processTransWF_CAPTAIN_REJECT_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_DIRECTOR_APPROVE_CANCEL")
  ListenableFuture<Boolean> processTransWF_DIRECTOR_APPROVE_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_DIRECTOR_RETURN_CANCEL")
  ListenableFuture<Boolean> processTransWF_DIRECTOR_RETURN_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_DIRECTOR_REJECT_CANCEL")
  ListenableFuture<Boolean> processTransWF_DIRECTOR_REJECT_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_FINRETUR_APPROVE_CANCEL")
  ListenableFuture<Boolean> processTransWF_FINRETUR_APPROVE_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_FINRETUR_RETURN_CANCEL")
  ListenableFuture<Boolean> processTransWF_FINRETUR_RETURN_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @Multipart
  @POST("rest/s/AppBooking/processTransWF_FINRETUR_REJECT_CANCEL")
  ListenableFuture<Boolean> processTransWF_FINRETUR_REJECT_CANCEL(@Query("proTaskId") UUID procTaskId,
      @Query("comment") String comment, @Query("id") UUID appBookingId,
      @Part("updateTs") Date updateTs);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINRETUR_FIF_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINRETUR_FIF_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINRETUR_FIF_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINRETUR_FIF_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINRETUR_FIF_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINRETUR_FIF_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINRETUR_BJ_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINRETUR_BJ_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINRETUR_BJ_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINRETUR_BJ_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINRETUR_BJ_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINRETUR_BJ_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINRETUR_CUS_APPROVE_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINRETUR_CUS_APPROVE_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINRETUR_CUS_RETURN_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINRETUR_CUS_RETURN_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processTransTxn_WF_FINRETUR_CUS_REJECT_EDIT")
  ListenableFuture<Boolean> processTransTxn_WF_FINRETUR_CUS_REJECT_EDIT(
      @Query("txnProcTaskId") UUID txnProcTaskId, @Query("comment") String comment,
      @Query("txnId") UUID txnId);

  @POST("rest/s/AppBooking/processGenerateOTP_AppBookingTransferCustomer")
  ListenableFuture<Boolean> processGenerateOTP_AppBookingTransferCustomer(
      @Query("txnCusProcTaskId") UUID txnCusProcTaskId);

  @POST("rest/s/AppBooking/processGenerateOTP_AppBookingTransferBiroJasa")
  ListenableFuture<Boolean> processGenerateOTP_AppBookingTransferBiroJasa(
      @Query("txnBjProcTaskId") UUID txnBjProcTaskId);

  @POST("rest/s/AppBooking/processGenerateOTP_AppBookingTransferFIF")
  ListenableFuture<Boolean> processGenerateOTP_AppBookingTransferFIF(
      @Query("txnFifProcTaskId") UUID txnFifProcTaskId);

  @GET("rest/s/AppBooking/BankList")
  ListenableFuture<AppBookingBankListResponse> getBankList();

  @POST("rest/s/AppBooking/deteleAppBooking")
  ListenableFuture<HashMap<String, List<String>>> deleteAppBooking(@Query("ids") List<UUID> ids);

  @GET("rest/s/AppBooking/{id}/wfHistory")
  ListenableFuture<WfHistoryResponse> getWfHistory(@Path("id") UUID appBookingId);


  @POST("rest/s/AppBooking/ProcessMultiApprovalCaptain")
  ListenableFuture<AppBookingMultipleSelectedProcessResultData> processMultiApprovalCaptain(@Query("dataList")String dataList);

  @POST("rest/s/AppBooking/ProcessMultiReturnCaptain")
  ListenableFuture<AppBookingMultipleSelectedProcessResultData> processMultiReturnCaptain(@Query("dataList")String dataList);
}
