package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.ListActionResponse;
import id.co.danwinciptaniaga.npmdsandroid.R;

public class DroppingListActionUC
    extends BaseObservable<DroppingListActionUC.Listener, ListActionResponse<String, String>> {

  private static final String TAG = DroppingListActionUC.class.getSimpleName();
  private final DroppingAdditionalService droppingAdditionalService;
  private final AppExecutors appExecutors;

  public interface Listener {
    void onDroppingActionProcessStarted(Resource<ListActionResponse<String, String>> response);

    void onDroppingActionProcessSuccess(Resource<ListActionResponse<String, String>> response);

    void onDroppingActionProcessFailure(Resource<ListActionResponse<String, String>> response);
  }

  @Inject
  public DroppingListActionUC(DroppingAdditionalService droppingAdditionalService,
      AppExecutors appExecutors,
      Application application) {
    super(application);
    this.droppingAdditionalService = droppingAdditionalService;
    this.appExecutors = appExecutors;
  }

  public void deleteDroppings(List<UUID> ids) {
    Preconditions.checkNotNull(ids);
    Preconditions.checkArgument(ids.size() > 0);
    notifyStart(null, null);
    ListenableFuture<ListActionResponse<String, String>> deleteLf = droppingAdditionalService.deleteDropping(
        ids);
    Futures.addCallback(deleteLf, new FutureCallback<ListActionResponse<String, String>>() {
      @Override
      public void onSuccess(@NullableDecl ListActionResponse<String, String> result) {
        HyperLog.d(TAG, "onSuccess: result->" + result + "<-");
        int successCount = result.getSuccess() == null ? 0 : result.getSuccess().size();
        int failedCount = result.getFailed() == null ? 0 : result.getFailed().size();
        StringBuilder msg = new StringBuilder();
        if (successCount > 0) {
          msg.append(
              application.getString(R.string.dropping_additional_delete_success,
                  String.valueOf(successCount)));
        }
        if (failedCount > 0) {
          if (msg.length() > 0)
            msg.append("\n");

          msg.append(application.getString(R.string.dropping_additional_delete_failed,
              String.valueOf(failedCount)));
          if (successCount == 0) {
            // hanya ada pesan error, tampilkan detil error
            msg.append(":\n");
            List<String> failureMessages = result.getFailed().entrySet().stream()
                .map(e -> e.getValue())
                .collect(Collectors.toList());
            msg.append(StringUtils.join(failureMessages, "\n"));
          }
        }
        notifySuccess(msg.toString(), result);
      }

      @Override
      public void onFailure(Throwable t) {
        HyperLog.exception(TAG, "onFailure: t->" + t + "<-", t);
        ErrorResponse er = RetrofitUtility.parseErrorBody(t);
        notifyFailure(ids.size() + " Dropping Tambahan gagal dihapus: ", er,
            er == null ? t : new Exception(er.getFormattedMessage(), t));
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener,
      Resource<ListActionResponse<String, String>> result) {
    HyperLog.d(TAG, "doNotifyStart");
    listener.onDroppingActionProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener,
      Resource<ListActionResponse<String, String>> result) {
    HyperLog.d(TAG, "doNotifySuccess");
    listener.onDroppingActionProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener,
      Resource<ListActionResponse<String, String>> result) {
    HyperLog.d(TAG, "doNotifyFailure");
    listener.onDroppingActionProcessFailure(result);
  }
}
