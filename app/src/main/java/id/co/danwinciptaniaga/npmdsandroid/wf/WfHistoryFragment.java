package id.co.danwinciptaniaga.npmdsandroid.wf;

import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentWfHistoryBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.AnimationUtil;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WfHistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
public class WfHistoryFragment extends Fragment {
  private static final String TAG = WfHistoryFragment.class.getSimpleName();

  private static final String ARG_WF_TYPE = "wfType";
  private static final String ARG_ENTITY_ID = "entityId";

  private WfHistoryVM viewModel;
  private FragmentWfHistoryBinding binding;
  private ProcTaskListAdapter adapter;

  public WfHistoryFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param entityId outletId
   * @return A new instance of fragment OutletBalanceReportFragment.
   */
  public static WfHistoryFragment newInstance(WfType type, UUID entityId) {
    WfHistoryFragment fragment = new WfHistoryFragment();
    Bundle args = new Bundle();
    args.putSerializable(ARG_WF_TYPE, type);
    args.putSerializable(ARG_ENTITY_ID, entityId);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(this).get(WfHistoryVM.class);
    WfHistoryFragmentArgs args = WfHistoryFragmentArgs.fromBundle(getArguments());
    viewModel.setEntity(args.getWfType(), args.getEntityId());
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentWfHistoryBinding.inflate(inflater, container, false);
    viewModel.getFormState().observe(getViewLifecycleOwner(), this::onFormStateChange);

    adapter = new ProcTaskListAdapter(getContext());
    binding.list.setAdapter(adapter);

    binding.swipeRefresh.setOnRefreshListener(() -> {
      viewModel.loadWfHistory();
      binding.swipeRefresh.setRefreshing(false);
    });

    binding.pageNsi.retryButton.setOnClickListener(v -> {
      viewModel.loadWfHistory();
      binding.swipeRefresh.setRefreshing(false);
    });

    return binding.getRoot();
  }

  private void onFormStateChange(FormState formState) {
    if (formState == null) {
      // tahap ini dilakukan 1 kali saja, yaitu sewaktu pertama kali FormState masih null
      viewModel.loadWfHistory();
      return;
    } else {
      switch (formState.getState()) {
      case READY:
        bindProgressAndPageContentReady(AnimationUtil.fadeoutAnimationMedium(),
            AnimationUtil.fadeinAnimationMedium());
        break;
      case ERROR:
        bindProgressAndPageContentError(formState, AnimationUtil.fadeoutAnimationMedium(),
            AnimationUtil.fadeinAnimationMedium());
        break;
      case LOADING:
      default:
        bindProgressAndPageContentLoading(formState, AnimationUtil.fadeoutAnimationMedium(),
            AnimationUtil.fadeinAnimationMedium());
        break;
      }
    }
  }

  private void bindProgressAndPageContentReady(Animation outAnimation, Animation inAnimation) {
    HyperLog.d(TAG, "bindProgressAndPageContentReady");
    binding.tvStartedBy.setText(
        viewModel.getWfHistoryResponse().getProcInstanceData().getStartedByUserName());
    binding.tvStartDate.setText(
        Formatter.DTF_dd_MM_yyyy_HH_mm.format(
            viewModel.getWfHistoryResponse().getProcInstanceData().getStartDate()));
    if (viewModel.getWfHistoryResponse().getProcInstanceData().getEndDate() != null) {
      binding.tvEndDate.setText(Formatter.DTF_dd_MM_yyyy_HH_mm.format(
          viewModel.getWfHistoryResponse().getProcInstanceData().getEndDate()));
    } else {
      binding.tvEndDate.setText(null);
    }
    adapter.submitList(viewModel.getWfHistoryResponse().getProcTaskDataList());

    binding.pageNsi.getRoot().setAnimation(outAnimation);
    binding.pageNsi.getRoot().setVisibility(View.GONE);
    binding.pageContent.setAnimation(inAnimation);
    binding.pageContent.setVisibility(View.VISIBLE);
  }

  private void bindProgressAndPageContentError(FormState formState, Animation outAnimation,
      Animation inAnimation) {
    HyperLog.d(TAG, "bindProgressAndPageContentError");
    if (formState.getMessage() != null) {
      binding.pageNsi.errorMsg.setText(formState.getMessage());
    }
    binding.pageNsi.progressBar.setVisibility(View.GONE);
    binding.pageNsi.getRoot().setAnimation(inAnimation);
    binding.pageNsi.getRoot().setVisibility(View.VISIBLE);
    binding.pageNsi.retryButton.setAnimation(inAnimation);
    binding.pageNsi.retryButton.setVisibility(View.VISIBLE);
    if (binding.pageContent.getVisibility() != View.GONE) {
      binding.pageContent.setAnimation(outAnimation);
      binding.pageContent.setVisibility(View.GONE);
    }
  }

  private void bindProgressAndPageContentLoading(FormState formState, Animation outAnimation,
      Animation inAnimation) {
    HyperLog.d(TAG, "bindProgressAndPageContentLoading");
    if (formState.getMessage() != null) {
      binding.pageNsi.errorMsg.setText(formState.getMessage());
    }
    binding.pageNsi.progressBar.setVisibility(View.VISIBLE);
    binding.pageNsi.getRoot().setAnimation(inAnimation);
    binding.pageNsi.getRoot().setVisibility(View.VISIBLE);
    binding.pageNsi.retryButton.setVisibility(View.GONE);
    if (binding.pageContent.getVisibility() != View.GONE) {
      binding.pageContent.setAnimation(outAnimation);
      binding.pageContent.setVisibility(View.GONE);
    }
  }
}