package id.co.danwinciptaniaga.npmdsandroid.util;

import android.view.View;
import android.view.animation.Animation;

public class ViewUtil {
  public static void setVisibility(View view, int visibility, Animation animation) {
    if (view.getVisibility() != visibility) {
      view.setAnimation(animation);
      view.setVisibility(visibility);
    }
  }

  public static void setVisibility(View view, int visibility) {
    setVisibility(view, visibility, null);
  }
}
