package id.co.danwinciptaniaga.npmdsandroid.ui.bankacct;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.BankData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.data.ConstraintViolation;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit.DroppingAdditionalDetailEditViewModel;

public class BankAcctFormVM extends AndroidViewModel implements GetBanksUseCase.Listener {
  private static final String TAG = DroppingAdditionalDetailEditViewModel.class.getSimpleName();

  private final GetBanksUseCase ucGetBanks;

  private MutableLiveData<FormState> formState = new MutableLiveData<>(null);
  private ArrayList<KeyValueModel<BankData, String>> bankKvms;
  private Integer selectedBank;

  private boolean isOthers;
  private UUID bankId;
  private BankAccountData bankAccountData;
  private String bankCode;
  private String bankName;
  private String otherBankName;
  private MutableLiveData<String> otherBankNameErrorLd = new MutableLiveData<>();
  private String accountName;
  private MutableLiveData<String> accountNameErrorLd = new MutableLiveData<>();
  private String accountNo;
  private MutableLiveData<String> accountNoErrorLd = new MutableLiveData<>();
  private MutableLiveData<Integer> accountNoLengthLd = new MutableLiveData<>();
  private ZonedDateTime lastValidated;
  private boolean validated;

  @ViewModelInject
  public BankAcctFormVM(Application application, GetBanksUseCase getBanksUseCase) {
    super(application);
    ucGetBanks = getBanksUseCase;
    ucGetBanks.registerListener(this);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    ucGetBanks.unregisterListener(this);
  }

  public MutableLiveData<FormState> getFormState() {
    return formState;
  }

  public ArrayList<KeyValueModel<BankData, String>> getBankKvms() {
    return bankKvms;
  }

  public Integer getSelectedBank() {
    return selectedBank;
  }

  public void setSelectedBank(Integer selectedBank) {
    this.selectedBank = selectedBank;
    if (selectedBank != null) {
      BankData bd = bankKvms.get(selectedBank).getKey();
      this.bankId = bd.getId();
      this.bankName = bd.getName();
      this.bankCode = bd.getCode();
      this.accountNoLengthLd.postValue(bd.getAcctNoLength());
    } else {
      this.bankId = null;
      this.bankName = null;
      this.bankCode = null;
      this.accountNoLengthLd.postValue(null);
    }
  }

  public void setBankAccountData(BankAccountData bad) {
    this.bankAccountData = bad;
  }

  public BankAccountData getBankAccountData() {
    return bankAccountData;
  }

  public boolean isOthers() {
    return isOthers;
  }

  public void setOthers(boolean others) {
    isOthers = others;
  }

  public UUID getBankId() {
    return bankId;
  }

  public void setBankId(UUID bankId) {
    this.bankId = bankId;
  }

  public String getBankCode() {
    return bankCode;
  }

  public void setBankCode(String bankCode) {
    this.bankCode = bankCode;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public String getOtherBankName() {
    return otherBankName;
  }

  public void setOtherBankName(String otherBankName) {
    try {
      if (isOthers) {
        BankAcctValidationHelper.validateOtherBank(getApplication(), otherBankName);
      }
      this.otherBankName = otherBankName;
      otherBankNameErrorLd.postValue(null);
    } catch (ConstraintViolation constraintViolation) {
      otherBankNameErrorLd.postValue(constraintViolation.getMessage());
    }
  }

  public LiveData<String> getOtherBankNameErrorLd() {
    return otherBankNameErrorLd;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    try {
      BankAcctValidationHelper.validateAccountName(getApplication(), accountName);
      this.accountName = accountName;
      accountNameErrorLd.postValue(null);
    } catch (ConstraintViolation constraintViolation) {
      accountNameErrorLd.postValue(constraintViolation.getMessage());
    }
  }

  public LiveData<String> getAccountNameErrorLd() {
    return accountNameErrorLd;
  }

  public String getAccountNo() {
    return accountNo;
  }

  public void setAccountNo(String accountNo) {
    try {
      BankAcctValidationHelper.validateAccountNo(getApplication(), accountNo);
      this.accountNo = accountNo;
      accountNoErrorLd.postValue(null);
    } catch (ConstraintViolation constraintViolation) {
      accountNoErrorLd.postValue(constraintViolation.getMessage());
    }
  }

  public LiveData<String> getAccountNoErrorLd() {
    return accountNoErrorLd;
  }

  public LiveData<Integer> getAccountNoLengthLd() {
    return accountNoLengthLd;
  }

  public ZonedDateTime getLastValidated() {
    return lastValidated;
  }

  public void setLastValidated(ZonedDateTime lastValidated) {
    this.lastValidated = lastValidated;
  }

  public boolean isValidated() {
    return validated;
  }

  public void setValidated(boolean validated) {
    this.validated = validated;
  }

  public void loadBankAccountData() {
    if (bankAccountData.getBankId() != null) {
      this.bankId = bankAccountData.getBankId();
      this.bankName = bankAccountData.getBankName();
      this.bankCode = bankAccountData.getBankCode();
    } else {
      this.otherBankName = bankAccountData.getOtherBankName();
    }
    this.accountName = bankAccountData.getAccountName();
    this.accountNo = bankAccountData.getAccountNo();
    this.lastValidated = bankAccountData.getLastValidated();
    this.validated = bankAccountData.isValidated();
    ucGetBanks.load();
  }

  public void prepareNewBankAccountData() {
    bankAccountData = new BankAccountData();
    ucGetBanks.load();
  }

  @Override
  public void onGetBanksStarted() {
    formState.postValue(FormState.loading(getApplication().getString(R.string.memuat_data)));
  }

  @Override
  public void onGetBanksSuccess(Resource<List<BankData>> result) {
    if (result.getData() != null) {
      prepareBanksList(result.getData());
    } else {
      HyperLog.w(TAG, "No Banks returned");
    }
    this.bankId = bankAccountData.getBankId();
    this.bankCode = bankAccountData.getBankCode();
    this.bankName = bankAccountData.getBankName();
    this.otherBankName = bankAccountData.getOtherBankName();
    this.accountName = bankAccountData.getAccountName();
    this.accountNo = bankAccountData.getAccountNo();
    this.accountNoLengthLd.postValue(bankAccountData.getAccNoLength());
    this.lastValidated = bankAccountData.getLastValidated();
    this.validated = bankAccountData.isValidated();

    formState.postValue(FormState.ready(result.getMessage()));
  }

  private void prepareBanksList(List<BankData> banks) {
    // tidak pakai stream, karena perlu sekalian identifikasi selected item
    bankKvms = new ArrayList<>();
    selectedBank = 0;
    for (int index = 0; index < banks.size(); index++) {
      BankData bd = banks.get(index);
      if (bankId != null && bankId.equals(bd.getId()))
        selectedBank = index;

      bankKvms.add(new KeyValueModel<BankData, String>(bd, bd.getName()));
    }
  }

  @Override
  public void onGetBanksFailure(Resource<List<BankData>> responseError) {
    String message = Resource.getMessageFromError(responseError,
        getApplication().getString(R.string.error_contact_support));
    formState.postValue(FormState.error(message));
  }

  public boolean validateForm() {
    boolean result = false;
    if (isOthers) {
      result =
          getOtherBankNameErrorLd().getValue() == null && getAccountNameErrorLd().getValue() == null
              && getAccountNoErrorLd().getValue() == null;
    } else {
      result = getAccountNameErrorLd().getValue() == null
          && getAccountNoErrorLd().getValue() == null;
    }
    return result;
  }
}
