package id.co.danwinciptaniaga.npmdsandroid.prefs;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import okhttp3.ResponseBody;

public class TestPushNotificationUseCase {
  private static final String TAG = TestPushNotificationUseCase.class.getSimpleName();

  private final CommonService commonService;
  private final AppExecutors appExecutors;

  @Inject
  public TestPushNotificationUseCase(CommonService commonService, AppExecutors appExecutors) {
    this.commonService = commonService;
    this.appExecutors = appExecutors;
  }

  public void test(String fcmToken) {
    ListenableFuture<ResponseBody> responseLf = commonService.testPushNotification(
        fcmToken);
    Futures.addCallback(responseLf, new FutureCallback<ResponseBody>() {
      @Override
      public void onSuccess(@NullableDecl ResponseBody result) {
        HyperLog.d(TAG, "TestPushNotification success: " + result);
      }

      @Override
      public void onFailure(Throwable t) {
        HyperLog.e(TAG, "TestPushNotification failed: ", t);
      }
    }, appExecutors.backgroundIO());
  }
}
