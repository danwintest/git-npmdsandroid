package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

public class BankAccountListAdapter
    extends RecyclerView.Adapter<BankAccountListAdapter.MyViewHolder> {

  private final static String TAG = BankAccountListAdapter.class.getSimpleName();
  private Context mCtx;
  private List<BankAccountData> mData;
  BankAccountListener bankAccountListener;

  public BankAccountListAdapter(Context ctx, List<BankAccountData> data,
      BankAccountListener listener) {
    mCtx = ctx;
    mData = data;
    bankAccountListener = listener;
  }

  @NonNull
  @Override
  public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(mCtx);
    View v = inflater.inflate(R.layout.fragment_bankaccount_item, parent, false);

    return new MyViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
    BankAccountData badObj = mData.get(position);

    holder.tvBankName.setText(badObj.getBankName());
    String validateDate = badObj.getLastValidated() != null ?
        Formatter.SDF_dd_MM_yyyy.format(badObj.getLastValidated()) :
        "-";
    holder.tvValidateDate.setText(validateDate);
    holder.tvAccountName.setText(badObj.getAccountName());
    holder.tvAccountNo.setText(badObj.getAccountNo());
    holder.cl.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        bankAccountListener.onItemClicked(v, badObj, position);
      }
    });
  }

  @Override
  public int getItemCount() {
    return 0;
  }

  interface BankAccountListener {
    void onItemClicked(View v, BankAccountData data, int position);
  }

  public void setObjectListener(ConstraintLayout cl, BankAccountData data, int pos) {
    cl.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        bankAccountListener.onItemClicked(v, data, pos);
      }
    });
  }

  public class MyViewHolder extends RecyclerView.ViewHolder {
    ConstraintLayout cl;
    TextView tvBankName, tvAccountName, tvAccountNo, tvValidateDate;
    ImageView ivValidate;

    public MyViewHolder(@NonNull View itemView) {
      super(itemView);
      cl = (ConstraintLayout) itemView.findViewById(R.id.clContainer);
      tvBankName = (TextView) itemView.findViewById(R.id.tvBank);
      tvAccountName = (TextView) itemView.findViewById(R.id.tvAccountName);
      tvAccountNo = (TextView) itemView.findViewById(R.id.tvAccountNo);
      tvValidateDate = (TextView) itemView.findViewById(R.id.tvValidateDate);
      ivValidate = (ImageView) itemView.findViewById(R.id.icValidate);
      itemView.setTag(this);
      //      cl.setOnClickListener(new View.OnClickListener() {
      //        @Override
      //        public void onClick(View v) {
      //          bankAccountListener.onItemClicked(v, data, pos);
      //        }
      //      });
    }
  }
}
