package id.co.danwinciptaniaga.npmdsandroid.booking;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.CompanyData;
import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.booking.BookingFilterResponse;
import id.co.danwinciptaniaga.npmds.data.common.CabangShortData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class BookingFilterVM extends AndroidViewModel implements GetBookingFilterUC.Listener {
  private final static String TAG = BookingFilterVM.class.getSimpleName();
  private BookingBrowseFilter filterField =  new BookingBrowseFilter();
  private MutableLiveData<KeyValueModel<String, String>> bookingTypeKvm = new MutableLiveData<>();
  private MutableLiveData<LocalDate> toDate = new MutableLiveData<>();
  private MutableLiveData<LocalDate> fromDate = new MutableLiveData<>();
  private MutableLiveData<String> consumerName = new MutableLiveData<>();
  private MutableLiveData<String> vehicleNo = new MutableLiveData<>();

  private final GetBookingFilterUC ucFilter;
  private KeyValueModel<UUID, String> companyKvm = null;
  private MutableLiveData<List<KeyValueModel<UUID, String>>> companyKvmList = new MutableLiveData<>();
  private KeyValueModel<UUID, String> cabangKvm = null;
  private MutableLiveData<List<KeyValueModel<UUID, String>>> cabangKvmList = new MutableLiveData<>();
  private KeyValueModel<UUID, String> outletKvm = null;
  private MutableLiveData<List<KeyValueModel<UUID, String>>> outletKvmList = new MutableLiveData<>();
  private MutableLiveData<FormState> formState = new MutableLiveData<>();

  private List<KeyValueModel<String, String>> bookingTypeKvmList = new ArrayList<>();

  @ViewModelInject
  public BookingFilterVM(@NonNull Application application, GetBookingFilterUC ucService) {
    super(application);
    this.ucFilter = ucService;
    this.ucFilter.registerListener(this);
    bookingTypeKvmList.add(new KeyValueModel<>("-", "All"));
    bookingTypeKvmList.add(new KeyValueModel<>(Utility.BOOKING_TYPE_CASH, "Cash"));
    bookingTypeKvmList.add(new KeyValueModel<>(Utility.BOOKING_TYPE_TRANSFER, "Transfer"));
  }

  public List<KeyValueModel<String,String>> getBookingTypeKvmList(){
    return this.bookingTypeKvmList;
  }

  public MutableLiveData<KeyValueModel<String, String>> getBookingTypeKvm() {
    return bookingTypeKvm;
  }

  public void setBookingTypeKvm(KeyValueModel<String, String> kvmObj, String id) {
    HyperLog.d(TAG, "setBookingTypeKvm kvmObj->" + kvmObj + "<- id->" + id + "<-");
    if (id != null) {
      for (KeyValueModel<String, String> typeKvm : bookingTypeKvmList) {
        if (typeKvm.getKey().equals(id))
          bookingTypeKvm.postValue(typeKvm);
      }
    } else {
      if (kvmObj != null) {
        if (!kvmObj.equals(bookingTypeKvm.getValue()))
          bookingTypeKvm.postValue(kvmObj);
      } else {
        bookingTypeKvm.postValue(bookingTypeKvmList.get(0));
      }
    }
  }

  public MutableLiveData<LocalDate> getToDate() {
    return toDate;
  }

  public void setToDate(LocalDate date) {
    HyperLog.d(TAG, "setToDate date->" + date + "<-");
    this.toDate.postValue(date);
  }

  public MutableLiveData<LocalDate> getFromDate() {
    return fromDate;
  }

  public void setFromDate(LocalDate date) {
    HyperLog.d(TAG, "setFromDate date->" + date + "<-");
    this.fromDate.postValue(date);
  }

  public MutableLiveData<String> getConsumerName() {
    return consumerName;
  }

  public void setConsumerName(String name) {
    HyperLog.d(TAG, "setConsumerName name->" + name + "<-");
    if (consumerName.getValue() != name)
      consumerName.postValue(name);
  }

  public MutableLiveData<String> getVehicleNo() {
    return vehicleNo;
  }

  public void setVehicleNo(String no) {
    HyperLog.d(TAG, "setVehicleNo ->" + no + "<-");
    if (vehicleNo.getValue() != no)
      vehicleNo.postValue(no);
  }

  public BookingBrowseFilter getFilterField() {
    String type = null;
    if (getBookingTypeKvm().getValue() != null) {
      if (getBookingTypeKvm().getValue().getKey() != "-") {
        type = getBookingTypeKvm().getValue().getKey();
      }
    }
    
    filterField.setType(type);
    filterField.setBookingDateTo(getToDate().getValue() != null ? getToDate().getValue() : null);
    filterField.setBookingDateFrom(
        getFromDate().getValue() != null ? getFromDate().getValue() : null);
    filterField.setConsumerName(
        getConsumerName().getValue() != null ? getConsumerName().getValue() : null);
    filterField.setCompanyId(getCompanyKvm() != null ? getCompanyKvm().getKey() : null);
    filterField.setOutletId(getOutletKvm() != null ? getOutletKvm().getKey() : null);
    filterField.setVehicleNo(getVehicleNo() != null ? getVehicleNo().getValue() : null);
    filterField.setCabangId(getCabangKvm() != null ? getCabangKvm().getKey() : null);
    return filterField;
  }

  public void setFilterField(BookingBrowseFilter filterField){
    this.filterField = filterField;
    setBookingTypeKvm(null, this.filterField.getType());
    setFromDate(this.filterField.getBookingDateFrom());
    setToDate(this.filterField.getBookingDateTo());
    setConsumerName(this.filterField.getConsumerName());
    setVehicleNo(this.filterField.getVehicleNo());
  }

  public void loadFilter() {
    this.ucFilter.getBookingFilter();
  }

  public KeyValueModel<UUID, String> getCompanyKvm() {
    return companyKvm;
  }

  public void setCompanyKvm(KeyValueModel<UUID, String> companyKvm) {
    this.companyKvm = companyKvm;
  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getCompanyKvmList() {
    return companyKvmList;
  }

  public void setCompanyKvmList(List<CompanyData> cdList) {
    List<KeyValueModel<UUID, String>> kvmCompanyList = new ArrayList<>();
    KeyValueModel<UUID, String> kvmCompanyBlank = new KeyValueModel<>(null, null);
    kvmCompanyList.add(kvmCompanyBlank);
    for (CompanyData cd : cdList) {
      HyperLog.d(TAG, "->" + cd.getId() + "-" + cd.getCompanyName());
      KeyValueModel<UUID, String> kvmCompany = new KeyValueModel<>(cd.getId(), cd.getCompanyName());
      if (cd.getId().equals(this.filterField.getCompanyId())) {
        setCompanyKvm(kvmCompany);
      }
      kvmCompanyList.add(kvmCompany);
    }
    this.companyKvmList.postValue(kvmCompanyList);
  }

  public KeyValueModel<UUID, String> getOutletKvm() {
    return outletKvm;
  }

  public void setOutletKvm(KeyValueModel<UUID, String> outletKvm) {
    this.outletKvm = outletKvm;
  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getOutletKvmList() {
    return outletKvmList;
  }

  public void setOutletKvmList(List<OutletShortData> odList) {
    List<KeyValueModel<UUID, String>> kvmOutletList = new ArrayList<>();
    KeyValueModel<UUID, String> kvmOutletBlank = new KeyValueModel<>(null, null);
    kvmOutletList.add(kvmOutletBlank);
    for (OutletShortData od : odList) {
      KeyValueModel<UUID, String> kvmOutlet = new KeyValueModel<>(od.getOutletId(),
          od.getOutletName());
      if (od.getOutletId().equals(this.filterField.getOutletId())) {
        setOutletKvm(kvmOutlet);
      }
      kvmOutletList.add(kvmOutlet);
    }
    this.outletKvmList.postValue(kvmOutletList);
  }
  //---
  public KeyValueModel<UUID, String> getCabangKvm() {
    return cabangKvm;
  }

  public void setCabangKvm(KeyValueModel<UUID, String> cabangKvm) {
    this.cabangKvm = cabangKvm;
  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getCabangKvmList() {
    return cabangKvmList;
  }

  public void setCabangKvmList(List<CabangShortData> cbdList) {
    List<KeyValueModel<UUID, String>> kvmCabangList = new ArrayList<>();
    KeyValueModel<UUID, String> kvmCabangBlank = new KeyValueModel<>(null, null);
    kvmCabangList.add(kvmCabangBlank);
    for (CabangShortData cb : cbdList) {
      KeyValueModel<UUID, String> kvmCabang = new KeyValueModel<>(cb.getId(), cb.getCabangName());
      if (cb.getId().equals(this.filterField.getCabangId())) {
        setCabangKvm(kvmCabang);
      }
      kvmCabangList.add(kvmCabang);
    }
    this.cabangKvmList.postValue(kvmCabangList);
  }

  public MutableLiveData<FormState> getFormState() {
    return formState;
  }

  @Override
  public void onProcessStarted(Resource<BookingFilterResponse> loading) {
    formState.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessSuccess(Resource<BookingFilterResponse> res) {
    List<CompanyData> cdList = res.getData().getCompanyDataList();
    List<OutletShortData> odList = res.getData().getOutletDataList();
    List<CabangShortData> cbsList = res.getData().getCabangDataList();
    setCompanyKvmList(cdList);
    setOutletKvmList(odList);
    setCabangKvmList(cbsList);
    formState.postValue(FormState.ready(res.getMessage()));
  }

  @Override
  public void onProcessFailure(Resource<BookingFilterResponse> res) {
    formState.postValue(FormState.error(res.getMessage()));
  }
}
