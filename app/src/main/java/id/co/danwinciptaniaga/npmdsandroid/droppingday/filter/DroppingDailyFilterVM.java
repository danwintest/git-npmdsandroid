package id.co.danwinciptaniaga.npmdsandroid.droppingday.filter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.constraintlayout.motion.widget.KeyTimeCycle;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilterResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyStatusData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyStatusTransferStatusData;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;

public class DroppingDailyFilterVM extends AndroidViewModel
    implements GetDroppingDailyFilterUC.Listener {
  private final static String TAG = DroppingDailyFilterVM.class.getSimpleName();
  private final GetDroppingDailyFilterUC uc;
  private DroppingBrowseFilter filterField = new DroppingBrowseFilter();
  private MutableLiveData<List<KeyValueModel<String, String>>> statusKvmList = new MutableLiveData<>();
  private MutableLiveData<List<KeyValueModel<String, String>>> selectedStatusKvmList = new MutableLiveData<>();
  private MutableLiveData<List<KeyValueModel<String, String>>> transferStatusKvmList = new MutableLiveData<>();
  private KeyValueModel<String, String> transferStatusKvm = null;
  private MutableLiveData<FormState> formState = new MutableLiveData<>();

  @ViewModelInject
  public DroppingDailyFilterVM(@NonNull Application app, GetDroppingDailyFilterUC uc) {
    super(app);
    this.uc = uc;
    this.uc.registerListener(this);
  }

  public DroppingBrowseFilter getFilterField() {
    return filterField;
  }

  public void setFilterField(DroppingBrowseFilter filterField) {
    if (filterField != null)
      this.filterField = filterField;
  }

  public void setTrxno(String trxNo) {
    this.filterField.setTransactionNo(trxNo);
  }

  public void setOutletName(String outletName) {
    this.filterField.setOutletName(outletName);
  }

  public void setDroppingDateFrom(LocalDate date) {
    this.filterField.setDroppingDateFrom(date);
  }

  public void setDroppingDateTo(LocalDate date) {
    this.filterField.setDroppingDateTo(date);
  }

  public MutableLiveData<List<KeyValueModel<String, String>>> getSelectedStatusKvmList() {
    return this.selectedStatusKvmList;
  }

  public void setSelectedStatusKvmList(boolean isAdd,
      KeyValueModel<String, String> selectedStatusKvm) {
    List<KeyValueModel<String, String>> currentSelectedData = selectedStatusKvmList.getValue();
    currentSelectedData = currentSelectedData == null ? new ArrayList<>() : currentSelectedData;
    if (isAdd) {
      if (!currentSelectedData.contains(selectedStatusKvm))
        currentSelectedData.add(selectedStatusKvm);
    } else {
      currentSelectedData.remove(selectedStatusKvm);
    }
    selectedStatusKvmList.postValue(currentSelectedData);
  }

  private void setFirstSelectedStatus(List<KeyValueModel<String,String>> selectedStatusKvmList){
    this.selectedStatusKvmList.postValue(selectedStatusKvmList);
  }

  public MutableLiveData<List<KeyValueModel<String, String>>> getStatusKvmList() {
    return this.statusKvmList;
  }

  public void setStatusKvmList(List<KeyValueModel<String, String>> statusKvmList) {
    this.statusKvmList.postValue(statusKvmList);
  }

  public void setTransferStatus(String id) {
    this.filterField.setTransferStatus(id);
  }

  public void setTransferStatusKvm(KeyValueModel<String, String> transferStatusKvm) {
    this.transferStatusKvm = transferStatusKvm;
  }

  public KeyValueModel<String, String> getTransferStatusKvm() {
    return this.transferStatusKvm;
  }

  public void setTransferStatusKvmList(List<KeyValueModel<String, String>> tsdKvmList) {
    this.transferStatusKvmList.postValue(tsdKvmList);
  }

  public MutableLiveData<List<KeyValueModel<String, String>>> getTransferStatusKvmList() {
    return this.transferStatusKvmList;
  }

  public void getFilterData() {
    uc.getFilter();
  }

  public MutableLiveData<FormState> getFormState() {
    return formState;
  }

  @Override
  public void onProcessStarted(Resource<DroppingBrowseFilterResponse> loading) {
    HyperLog.d(TAG, "onProcessStarted() memulai load list wfStatus dan transferStatus");
    formState.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessSuccess(Resource<DroppingBrowseFilterResponse> res) {
    HyperLog.d(TAG, "onProcessSuccess selesai load list wfStatus dan transfreStatus");
    List<DroppingDailyStatusData> sdList = res.getData().getStatusDataList();
    List<DroppingDailyStatusTransferStatusData> tsdList = res.getData().getStatusTransferStatusDataList();

    List<KeyValueModel<String, String>> sKvmList = new ArrayList<>();
    List<KeyValueModel<String, String>> selectedStatusKvmList = new ArrayList<>();
    for (DroppingDailyStatusData sd : sdList) {
      KeyValueModel<String, String> sKvm = new KeyValueModel<>(sd.getId(), sd.getName());
      boolean isSelected = getFilterField().getStatus() == null ?
          false :
          getFilterField().getStatus().contains(sd.getId());
      if (isSelected) {
        selectedStatusKvmList.add(sKvm);
      }
      sKvmList.add(sKvm);
    }
    setFirstSelectedStatus(selectedStatusKvmList);
    setStatusKvmList(sKvmList);

    List<KeyValueModel<String, String>> tsKvmList = new ArrayList<>();
    for (DroppingDailyStatusTransferStatusData tsd : tsdList) {
      KeyValueModel<String, String> tsKvm = new KeyValueModel<>(tsd.getId(), tsd.getName());
      if (tsd.getId().equals(getFilterField().getTransferStatus())) {
        setTransferStatusKvm(tsKvm);
      }
      tsKvmList.add(tsKvm);
    }
    setTransferStatusKvmList(tsKvmList);
    HyperLog.d(TAG, "onProcessSuccess selesai load list wfStatus dan transfreStatus");
    formState.postValue(FormState.ready(res.getMessage()));
  }

  @Override
  public void onProcessFailure(Resource<DroppingBrowseFilterResponse> res) {
    HyperLog.exception(TAG,
        "onProcessFailure gagal load list wfStatus dan transferStatus ->" + res.getMessage()
            + "<-");
    formState.postValue(FormState.error(res.getMessage()));
  }
}
