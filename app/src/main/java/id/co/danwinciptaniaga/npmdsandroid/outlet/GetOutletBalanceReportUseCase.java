package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseDetailResponse;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseItemData;
import id.co.danwinciptaniaga.npmds.data.expense.NewExpenseResponse;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceListResponse;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceReportFilter;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceReportResponse;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletReportFilter;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletReportSort;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.LoadExpenseUseCase;

public class GetOutletBalanceReportUseCase extends
    BaseObservable<GetOutletBalanceReportUseCase.Listener, OutletBalanceReportResponse> {
  private static final String TAG = GetOutletBalanceReportUseCase.class.getSimpleName();

  public interface Listener {

    void onGetBalanceReportStarted();

    void onGetBalanceReportSuccess(Resource<OutletBalanceReportResponse> result);

    void onGetBalanceReportFailure(Resource<OutletBalanceReportResponse> responseError);

  }

  private final OutletReportService outletReportService;

  private final AppExecutors appExecutors;
  private OutletBalanceReportFilter filter = new OutletBalanceReportFilter();

  @Inject
  public GetOutletBalanceReportUseCase(Application application,
      OutletReportService outletReportService, AppExecutors appExecutors) {
    super(application);
    this.outletReportService = outletReportService;
    this.appExecutors = appExecutors;
    LocalDate fromDate = LocalDate.now().withDayOfMonth(1);
    LocalDate toDate = LocalDate.now();
    filter.setStatementDateFrom(fromDate);
    filter.setStatementDateTo(toDate);
  }

  public ListenableFuture<OutletBalanceReportResponse> getOutletBalanceReport(UUID outletId) {
    notifyStart(null, null);

    ListenableFuture<OutletBalanceReportResponse> lf = outletReportService.getOutletBalanceReport(
        outletId, filter);
    Futures.addCallback(lf, new FutureCallback<OutletBalanceReportResponse>() {
      @Override
      public void onSuccess(@NullableDecl OutletBalanceReportResponse result) {
        notifySuccess("Laporan Saldo Outlet berhasil dimuat", result);
      }

      @Override
      public void onFailure(Throwable t) {
        notifyFailure("Gagal memuat Laporan Saldo Outlet", null, t);
      }
    }, appExecutors.backgroundIO());
    return lf;
  }

  public void setStatementDateFrom(LocalDate dateFrom) {
    filter.setStatementDateFrom(dateFrom);
  }

  public void setStatementDateTo(LocalDate dateTo) {
    filter.setStatementDateTo(dateTo);
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<OutletBalanceReportResponse> result) {
    listener.onGetBalanceReportStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<OutletBalanceReportResponse> result) {
    listener.onGetBalanceReportSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<OutletBalanceReportResponse> result) {
    listener.onGetBalanceReportFailure(result);
  }
}
