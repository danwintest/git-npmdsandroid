package id.co.danwinciptaniaga.npmdsandroid.security;

import android.os.Build;
import android.util.Base64;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AuthorizationInterceptor implements Interceptor {
    private String authorization;

    public AuthorizationInterceptor(String clientId, String clientSecret) {
        String s = clientId + ":" + clientSecret;
        authorization = Base64.encodeToString(s.getBytes(), Base64.NO_WRAP);
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request original = chain.request();

        Request.Builder builder = original.newBuilder()
                .header("Authorization", "Basic " + authorization);

        Request request = builder.build();
        return chain.proceed(request);
    }
}
