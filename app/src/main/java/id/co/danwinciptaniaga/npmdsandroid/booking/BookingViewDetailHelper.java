package id.co.danwinciptaniaga.npmdsandroid.booking;

import dagger.internal.Preconditions;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmds.data.booking.BookingDetailResponse;
import id.co.danwinciptaniaga.npmds.data.wf.ProcInstanceData;
import id.co.danwinciptaniaga.npmds.data.wf.ProcTaskData;

public class BookingViewDetailHelper {
  public static EditPageMode getEditPageMode(BookingDetailResponse bdr) {
    Preconditions.checkNotNull(bdr, "bookingDetailResponse must not be null");
    Preconditions.checkNotNull(bdr.getBookingData(),
        "bookingDetailReponse.bookingDate mmust not be null");

    BookingData bd = bdr.getBookingData();
    ProcInstanceData pid = bdr.getProcInstanceData();
    ProcTaskData ptd = bdr.getProcTaskData();
    return EditPageMode.VIEW_ONLY;
  }

  public static enum EditPageMode {
    VIEW_ONLY,
  }
}
