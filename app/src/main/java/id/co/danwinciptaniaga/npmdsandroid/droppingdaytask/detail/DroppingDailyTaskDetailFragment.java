package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypertrack.hyperlog.HyperLog;

import android.app.PendingIntent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.androcon.auth.PermissionHelper;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentDroppingDailyTaskDetailBinding;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.sort.DroppingDailyTaskDetailSort;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListLoadStateAdapter;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;
import kotlin.Unit;

public class DroppingDailyTaskDetailFragment extends ProtectedFragment {
  private static final String TAG = DroppingDailyTaskDetailFragment.class.getSimpleName();
  public static final String SAVED_STATE_LD_REFRESH = "refresh";
  private FragmentDroppingDailyTaskDetailBinding binding;
  private DroppingDailyTaskDetailBrowseVM vm;
  private DroppingDailyTaskDetailListAdapter adapterDetail;
  private ConcatAdapter concatAdapterDetail;
  private RecyclerView rvDetail;
  private ActionModeCallBackDetail actionModeCallBackDetail;
  boolean isPermissionMultiApproval = false;
  boolean isPermissionUpdate = false;
  private ActionMode actionMode;

  @Inject
  AppExecutors appExecutors;

  public DroppingDailyTaskDetailFragment() {
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
//    OnBackPressedCallback callback = new OnBackPressedCallback(true) {
//      @Override
//      public void handleOnBackPressed() {
//        backPressAction();
//      }
//    };
//    requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    vm = new ViewModelProvider(requireActivity()).get(DroppingDailyTaskDetailBrowseVM.class);
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.expense_browse_menu, menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//    if (item.getItemId() == R.id.home) {
//      backPressAction();
//      return true;
//    }
    if(item.getItemId() == R.id.menu_filter){
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_dropping_daily_task_detail, nc.getCurrentDestination().getId()))
        return true;
      DroppingDailyTaskDetailFragmentDirections.ActionFilter dir = DroppingDailyTaskDetailFragmentDirections.actionFilter(false);
      dir.setFilterField(vm.getFilterField());
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    }else if(item.getItemId() == R.id.menu_sort){
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_dropping_daily_task_detail, nc.getCurrentDestination().getId()))
        return true;
      List<SortOrder> soList = new ArrayList(vm.getSortFields());
      String soListJson = new Gson().toJson(soList);
      DroppingDailyTaskDetailFragmentDirections.ActionSort dir = DroppingDailyTaskDetailFragmentDirections.actionSort();
      dir.setSortField(soListJson);
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

//  private void backPressAction() {
//    NavController nc = NavHostFragment.findNavController(DroppingDailyTaskDetailFragment.this);
//    nc.getPreviousBackStackEntry().getSavedStateHandle().set(
//        ARG_DROPPING_DAILY_DETAIL_FILTER, vm.getFilterField());
//    nc.popBackStack();
//  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentDroppingDailyTaskDetailBinding.inflate(inflater, container, false);

    DroppingDailyTaskDetailFragmentArgs args = DroppingDailyTaskDetailFragmentArgs.fromBundle(
        getArguments());
    DroppingDailyTaskBrowseFilter filterField = args.getFilterData();
    vm.setTotalAmt(args.getTotalAmt());
    setRecylerAdapterAndSwipeDetail(filterField);
    setHeaderView();
    setFilterObserver();
    setSortObserver();
    return binding.getRoot();
  }

  private void setHeaderView() {
    vm.isReadyHeaderView().observe(getViewLifecycleOwner(), isReady -> {
      if (isReady) {
        binding.tvCompanyName.setText(vm.getFilterField().getCompanyName());
        binding.tvBankName.setText(vm.getFilterField().getBankName());
        binding.tvTotalAmt.setText(Utility.getFormattedAmt(vm.getTotalAmt()));
        binding.tvSelectedTotalAmt.setText(Utility.getFormattedAmt(vm.getSelectedTotalAmt()));
        if (vm.getFilterField().getRequestDate() != null)
          binding.tvDroppingDate.setText(
              Formatter.DTF_dd_MM_yyyy.format(vm.getFilterField().getRequestDate()));
        else
          binding.tvDroppingDate.setText("-");
        vm.setReadyHeaderView(false);
      }
    });
  }

  private void setRecylerAdapterAndSwipeDetail(DroppingDailyTaskBrowseFilter filterField) {
    vm.loadDetailListWithFilter(filterField);
    rvDetail = binding.listDetail;
    actionModeCallBackDetail = new ActionModeCallBackDetail();
    adapterDetail = new DroppingDailyTaskDetailListAdapter(
        new DroppingDailyTaskDetailListAdapter.RecyclerViewAdapterListener() {
          @Override
          public void onItemClicked(View view, DroppingDailyTaskBrowseDetailData data,
              int position) {
            if (adapterDetail.getSelectedItemCount() > 0) {
              enableActionModeDetail(position);
            } else {
              if (!isPermissionUpdate)
                return;

              List<DroppingDailyTaskBrowseDetailData> singleSelected = new ArrayList<>();
              singleSelected.add(data);
              openWfFragment(vm.getSelectedSingleWf(data.getProcTaskDataList()), singleSelected);
            }
          }

          @Override
          public void onItemLongClickedListener(View v, DroppingDailyTaskBrowseDetailData data,
              int position) {
            enableActionModeDetail(position);
          }
        }, getActivity().getApplicationContext(), appExecutors);
    concatAdapterDetail = adapterDetail.withLoadStateHeaderAndFooter(
        new ExpenseListLoadStateAdapter(retryCallbackDetail),
        new ExpenseListLoadStateAdapter(retryCallbackDetail));
    adapterDetail.addLoadStateListener((combinedLoadStates) -> {
      // listener ini mengupdate LoadState di ViewModel
      vm.setLoadStateDetail(combinedLoadStates.getRefresh());
      return Unit.INSTANCE;
    });
    rvDetail.setAdapter(concatAdapterDetail);

    vm.getRefreshDetailList().observe(getViewLifecycleOwner(), e -> {
      if (e != null && e) {
        adapterDetail.refresh();
      }
    });

    vm.getActionEventDetail().observe(getViewLifecycleOwner(), s -> {
      HyperLog.d(TAG, "getActionEvent: " + s + "<-");
      if (Status.SUCCESS.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
        adapterDetail.refresh();
      } else if (Status.LOADING.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
        ViewUtil.setVisibility(binding.progressWrapper.progressText, View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            s.getMessage() == null ? getString(R.string.msg_please_wait) : s.getMessage());
        ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
      } else {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
      }
    });

    NavController nc = NavHostFragment.findNavController(this);

    LiveData<Boolean> refreshLd = nc.getCurrentBackStackEntry()
        .getSavedStateHandle().getLiveData(SAVED_STATE_LD_REFRESH);
    refreshLd.observe(getViewLifecycleOwner(), refresh -> {
      HyperLog.d(TAG, "refresh signaled: " + refresh);
      // tidak bisa langsung adapter.refresh, sepertinya adapternya belum connect ke RecyclerView-nya
      // jadi kita tunda dengan sinyal melalui viewmodel
      vm.setRefreshDetailList(refresh);
      nc.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(SAVED_STATE_LD_REFRESH);
    });

    binding.swipeRefreshDetail.setOnRefreshListener(() -> {
      adapterDetail.refresh();
      binding.swipeRefreshDetail.setRefreshing(false);
    });
  }

  private void enableActionModeDetail(int position) {
    if(!isPermissionMultiApproval)
      return;

    if (!isPermissionUpdate)
      return;

    if (actionMode == null) {
      actionMode = getActivity().startActionMode(
          (ActionMode.Callback) actionModeCallBackDetail);
    }
    toggleSelectionDetail(position);
  }

  private void toggleSelectionDetail(int position) {
    adapterDetail.toggleSelection(position);
    int selectedItem = adapterDetail.getSelectedItemCount();
    showHideDetailActionMode(selectedItem);
    vm.setSelectedTotalAmt(adapterDetail.getSelectedTotalAmt());
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    Resource<List<PermissionInfo>> permissions =  basemodel.getPermissionsLd().getValue();
    isPermissionMultiApproval = PermissionHelper.hasPermission(permissions.getData(),
        PermissionHelper.PERMISSION_TYPE_SPECIFIC,
        DroppingDailyTaskDetailBrowseVM.PERMISSION_MULTIPLE_APPROVAL);

    isPermissionUpdate = PermissionHelper.hasPermission(permissions.getData(),
        PermissionHelper.PERMISSION_TYPE_ENTITY_OP,
        DroppingDailyTaskDetailBrowseVM.PERMISSION_UPDATE);

    if (status.getStatus() == Status.LOADING) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      binding.swipeRefreshDetail.setVisibility(View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.GONE);
      binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);

      binding.progressWrapper.retryButton.setOnClickListener(restartDetail);
    }
  }

  private void onReady() {
    // harusnya permission sudah ada kalau authenticationStatusUpdate SUCCESS
    Resource<List<PermissionInfo>> permissions = basemodel.getPermissionsLd().getValue();

    // lalu gunakan LiveData observer untuk mengupdate view
    vm.getLoadStateDetail().observe(getViewLifecycleOwner(), state -> {
      HyperLog.d(TAG, "droppingListLoadState changed: " + state);
      if (state instanceof LoadState.Loading) {
        // sedang loading pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
      } else if (state instanceof LoadState.Error) {
        // terjadi error pada waktu pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);

        // retry = reload semua data
        binding.progressWrapper.retryButton.setOnClickListener(reloadFragmentDetail);
        // sembunyikan list, karena retry akan dihandle oleh button di atas
        binding.swipeRefreshDetail.setVisibility(View.GONE);

        binding.progressWrapper.progressText.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            ((LoadState.Error) state).getError().getMessage());
      } else if (state instanceof LoadState.NotLoading) {
        // sudah ok
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.swipeRefreshDetail.setVisibility(View.VISIBLE);
      }
    });
    // connect data ke adapter
    // supaya tidak hit server lagi pada waktu kembali dari child fragment, hanya load data kalau
    // list masih null

    vm.getDroppingDailyDetailList().observe(getViewLifecycleOwner(), pagingData -> {
      adapterDetail.submitData(getLifecycle(), pagingData);
    });
  }

  private View.OnClickListener restartDetail = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_dropping_daily_browse)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragmentDetail = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // reload fragment ini = memuat ulang seluruh list
      adapterDetail.refresh();
      // karena menggunakan progress indicator sendiri, jadi  tidak perlu yg dari swipeRefresh
      binding.swipeRefreshDetail.setRefreshing(false);
    }
  };

  private class ActionModeCallBackDetail implements ActionMode.Callback {

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
      mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
      mode.getMenu().findItem(R.id.action_approve).setVisible(true);
      mode.getMenu().findItem(R.id.action_reject).setVisible(true);
      mode.getMenu().findItem(R.id.action_delete).setVisible(false);
      mode.getMenu().findItem(R.id.action_return).setVisible(false);
      return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
      return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
      List<String> multiSelectedDecision = new ArrayList<>();
      if(item.getItemId() == R.id.action_approve){
        multiSelectedDecision.add(WorkflowConstants.WF_OUTCOME_APPROVE);
        openWfFragment(multiSelectedDecision, adapterDetail.getSelectedData());
      }else if (item.getItemId() == R.id.action_reject){
        multiSelectedDecision.add(WorkflowConstants.WF_OUTCOME_REJECT);
        openWfFragment(multiSelectedDecision, adapterDetail.getSelectedData());
      }else{
        //TODO BELUM ADA IMPLEMENTASI
//        HyperLog.d(TAG, "actionMenu tidak diketahui actionMenu[" + item.getItemId() + "]");
//        Toast.makeText(getContext(), "Fungsi tidak diketahui", Toast.LENGTH_SHORT).show();
      }
      return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
      adapterDetail.clearSelections();
      vm.setSelectedTotalAmt(BigDecimal.ZERO);
      actionMode = null;
      rvDetail.post(new Runnable() {
        @Override
        public void run() {
          adapterDetail.resetAnimationIndex();
        }
      });
    }
  }

  private View.OnClickListener retryCallbackDetail = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // retry hanya mencoba lagi page yang gagal
      adapterDetail.retry();
    }
  };

  private void openWfFragment(List<String> decisionList,
      List<DroppingDailyTaskBrowseDetailData> dl) {
    List<DroppingDailyTaskBrowseDetailData> dlFinal = new ArrayList<>();
    for(DroppingDailyTaskBrowseDetailData dFinal : dl){
      dFinal.setProcTaskDataList(null);
      dlFinal.add(dFinal);
    }
//    String dataJson = new Gson().toJson(dl);
    String dataJson = new Gson().toJson(dlFinal);
    String decisionJson = new Gson().toJson(decisionList);
    showHideDetailActionMode(0);
    NavController nc = Navigation.findNavController(binding.getRoot());
    DroppingDailyTaskDetailFragmentDirections.ActionWf dir = DroppingDailyTaskDetailFragmentDirections.actionWf(
        dataJson, decisionJson);
    nc.navigate(dir, NavHelper.animChildToParent().build());
  }

  private void showHideDetailActionMode(int selectedItems) {
    if (selectedItems == 0) {
      if (actionMode != null)
        actionMode.finish();
    } else {
      actionMode.setTitle(String.valueOf(selectedItems));
      actionMode.invalidate();
    }
  }

  private void setFilterObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<DroppingDailyTaskBrowseFilter> filterField = nc
        .getCurrentBackStackEntry()
        .getSavedStateHandle()
        .getLiveData(DroppingDailyTaskFilterFragment.DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_DETAIL);
    filterField.observe(getViewLifecycleOwner(), filterObj -> {
      vm.loadDetailListWithFilter(filterObj);
//      nc.getCurrentBackStackEntry().getSavedStateHandle()
//          .remove(DroppingDailyTaskFilterFragment.DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_DETAIL);
    });
  }

  private void setSortObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<String> soListJsonMld = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        DroppingDailyTaskDetailSort.DROPPING_DAILY_TASK_DETAIL_SORT);

    soListJsonMld.observe(getViewLifecycleOwner(), soListJsonObj -> {
      String soListJson = soListJsonObj.replace("\\", "");
      Type dataType = new TypeToken<List<SortOrder>>() {
      }.getType();
      List<SortOrder> soList = new Gson().fromJson(soListJson, dataType);
      Set<SortOrder> sso = new HashSet<SortOrder>(soList);
      vm.setSortFields(sso);
      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
          DroppingDailyTaskDetailSort.DROPPING_DAILY_TASK_DETAIL_SORT);
    });
  }

}

