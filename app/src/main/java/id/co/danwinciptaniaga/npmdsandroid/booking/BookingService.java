package id.co.danwinciptaniaga.npmdsandroid.booking;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseSort;
import id.co.danwinciptaniaga.npmds.data.booking.BookingDetailResponse;
import id.co.danwinciptaniaga.npmds.data.booking.BookingFilterResponse;
import id.co.danwinciptaniaga.npmds.data.booking.BookingListResponse;
import id.co.danwinciptaniaga.npmds.data.booking.NewBookingReponse;
import okhttp3.MultipartBody;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BookingService {
  @GET("rest/s/booking")
  ListenableFuture<BookingListResponse> getBookingList(@Query("page") Integer page,
      @Query("pageSize") Integer pageSize, @Query("filter") BookingBrowseFilter filter,
      @Query("sort") BookingBrowseSort sort);

  @GET("rest/s/booking/{id}")
  ListenableFuture<BookingDetailResponse> loadBooking(@Path("id") UUID bookingId);

  @GET("rest/s/booking/new")
  ListenableFuture<NewBookingReponse> getNewBooking();

  @GET("rest/s/AppBooking/getFilterField")
  ListenableFuture<BookingFilterResponse> getAppFilterField();

  @GET("rest/s/Booking/getFilterField")
  ListenableFuture<BookingFilterResponse> getBookingFilterField();

  @Multipart
  @POST("rest/s/booking/save")
  ListenableFuture<Boolean> save(@Query("id") UUID bookingId, @Query("applicationNo") String appNo,
      @Query("poNo") String poNo, @Part("poDate") LocalDate poDate,
      @Query("poprIdList") List<UUID> poprIdList, @Part MultipartBody.Part[] att_po,
      @Part("updateTs") Date updateTs, @Query("isEmpty") Boolean isEmpty);
}
