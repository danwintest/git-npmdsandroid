package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import org.jetbrains.annotations.NotNull;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiAppBookingRekeningListBinding;

public class AppBookingTransferRekeningListAdapter
    extends PagingDataAdapter<BankAccountData, AppBookingTransferRekeningListAdapter.ViewHolder> {

  private final static String TAG = AppBookingTransferRekeningListAdapter.class.getSimpleName();
  private final Listener mListener;
  private Context mCtx;

  @Inject
  public AppBookingTransferRekeningListAdapter(Listener listener, Context ctx) {
    super(new AppBookingDataDiffCallBack());
    mCtx = ctx;
    mListener = listener;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_app_booking_rekening_list,
        parent, false);
    return new ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
  }

  interface Listener {
    void onItemClicked(View v, BankAccountData data, int position);
  }

  private static class AppBookingDataDiffCallBack extends DiffUtil.ItemCallback<BankAccountData> {

    @Override
    public boolean areItemsTheSame(@NonNull BankAccountData oldItem,
        @NonNull BankAccountData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull BankAccountData oldItem,
        @NonNull BankAccountData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiAppBookingRekeningListBinding binding;

    public ViewHolder(@NotNull View v) {
      super(v);
      binding = LiAppBookingRekeningListBinding.bind(v);
    }

    public void bind(BankAccountData appData, int position) {
      setObject(appData);
      setObjectListener(appData, position);
    }

    public void setObject(BankAccountData data) {
      String finalBankName =
          data.getBankName() != null ? data.getBankName() : data.getOtherBankName();
      binding.tvBankValue.setText(finalBankName);
      binding.tvAccNameValue.setText(data.getAccountName());
      binding.tvAccNoValue.setText(data.getAccountNo());
    }

    public void setObjectListener(BankAccountData data, int pos) {
      binding.clContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, data, pos);
        }
      });

    }
  }
}
