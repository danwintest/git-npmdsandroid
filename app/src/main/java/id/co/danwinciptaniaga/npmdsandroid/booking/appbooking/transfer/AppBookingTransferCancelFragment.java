package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;

public class AppBookingTransferCancelFragment extends AppBookingTransferNewFragment {
  private final String TAG = AppBookingTransferCancelFragment.class.getSimpleName();
  private BookingBrowseViewModel mBrowseVm;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    AppBookingTransferVM vm = new ViewModelProvider(this).get(AppBookingTransferVM.class);
    AppBookingTransferCancelFragmentArgs args = AppBookingTransferCancelFragmentArgs.fromBundle(
        getArguments());

    mBrowseVm = new ViewModelProvider(requireActivity()).get(BookingBrowseViewModel.class);
    boolean isHasPermission = args.getIsHasCreatePermission();
    setOnCreateView(isHasPermission, inflater, container, this, TAG, vm);
    getTransferVm().setField_BookingId(args.getBookingId());
    getTransferVm().setFromBookingStatus(true);
    return getBinding().getRoot();
  }

  @Override
  protected void processLoadData() {
    getTransferVm().processLoadAppBookingDataByBooking(getTransferVm().getField_BookingId(), false,
        true);
  }

  @Override
  protected void setReadyFormState(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans, String message,
      Animation inAnim, Animation outAnim, boolean isFromAction) {
    if (isFromAction) {
      mBrowseVm.setRefreshList(true);
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      navController.navigateUp();
      return;
    } else
      super.setReadyFormState(smCash, smTrans, message, inAnim,
          outAnim, isFromAction);
  }

  @Override
  protected void setMode_generalField(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer, String message, Animation inAnimation,
      Animation outAnimation) {
    super.setMode_generalField(smCash, smTransfer, message, inAnimation, outAnimation);
//    showElementCashOrTransfer(false, smCash, smTransfer);
  }

  @Override
  protected void setStandartButtonListener(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer) {
    getBinding().btnSimpan.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getTransferVm().processSaveDraftCancel(true);
      }
    });

    getBinding().btnSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getTransferVm().processWfSubmitCancel(true);
      }
    });

    getBinding().btnWfHistory.setOnClickListener(v -> {
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      AppBookingTransferCancelFragmentDirections.ActionWfHistory dir = AppBookingTransferCancelFragmentDirections
          .actionWfHistory(WfType.APP_BOOKING,
              getTransferVm().getField_AppBookingId());
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });
  }
}
