package id.co.danwinciptaniaga.npmdsandroid.exp;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.LoadState;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseSort;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmdsandroid.exp.edit.ExpenseFormState;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class ExpenseBrowseViewModel extends AndroidViewModel implements ExpenseListAction.Listener {
  private static final String TAG = ExpenseBrowseViewModel.class.getSimpleName();
  private final GetExpenseListUseCase getExpenseListUseCase;
  private final Pager<Integer, ExpenseData> pager;
  private final ExpenseListAction expListAction;
  private ExpenseBrowseFilter filterField = new ExpenseBrowseFilter();

  private LiveData<PagingData<ExpenseData>> expenseList;
  private MutableLiveData<Boolean> refreshList = new SingleLiveEvent<>();
  private MutableLiveData<LoadState> expenseListLoadState = new MutableLiveData<>();

  private Set<SortOrder> sortFields = new LinkedHashSet<>(); // urutan pengaruh

  private MutableLiveData<ExpenseFormState> formState = new MutableLiveData<>(
      ExpenseFormState.loading(null));
  private MutableLiveData<Resource<String>> actionEvent = new SingleLiveEvent<>();

  public LiveData<Resource<String>> getActionEvent() {
    return actionEvent;
  }

  public LiveData<ExpenseFormState> getFormState() {
    return formState;
  }

  @ViewModelInject
  public ExpenseBrowseViewModel(@NonNull Application application, AppExecutors appExecutors,
      GetExpenseListUseCase getExpenseListUseCase, ExpenseListAction expListAction) {
    super(application);
    this.getExpenseListUseCase = getExpenseListUseCase;
    this.expListAction = expListAction;
    this.expListAction.registerListener(this);

    this.sortFields.add(ExpenseBrowseSort
        .sortBy(ExpenseBrowseSort.Field.STATEMENT_DATE, SortOrder.Direction.DESC));

    pager = new Pager<Integer, ExpenseData>(
        new PagingConfig(Utility.PAGE_SIZE),
        () -> new ExpenseListPagingSource(getExpenseListUseCase, appExecutors.networkIO())
    );
  }

  public LiveData<Boolean> getRefreshList() {
    return refreshList;
  }

  public void setRefreshList(Boolean b) {
    refreshList.postValue(b);
  }

  public LiveData<PagingData<ExpenseData>> getExpenseList() {
    if (expenseList == null) {
      CoroutineScope viewModelScope = ViewModelKt.getViewModelScope(this);
      expenseList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pager), viewModelScope);
    }
    return expenseList;
  }

  public LiveData<LoadState> getExpenseListLoadState() {
    return expenseListLoadState;
  }

  public void setExpenseListLoadState(LoadState loadState) {
    this.expenseListLoadState.postValue(loadState);
  }

  public ExpenseBrowseFilter getFilterField() {
    return filterField;
  }
  public void setFilterField(ExpenseBrowseFilter newFilter){
    this.filterField = newFilter;
    getExpenseListUseCase.setFilter(newFilter);
    this.refreshList.postValue(true);
  }

  public Set<SortOrder> getSortFields() {
    return Collections.unmodifiableSet(sortFields); // kembalikan copy saja
  }

  public void setSortFields(Set<SortOrder> sortFields) {
    this.sortFields.clear();
    if (sortFields != null) {
      this.sortFields.addAll(sortFields);
    }

    getExpenseListUseCase.setSortOrders(sortFields);
    this.refreshList.postValue(true);
  }


  public void deleteSelectedExpense(List<ExpenseData> expenseList) {
    List<UUID> ids = new ArrayList<UUID>();
    for (ExpenseData data : expenseList)
      ids.add(data.getId());
    expListAction.deleteExpense(ids);
  }

  public void rejectSelectedExpense(List<ExpenseData> expenseList) {
    List<UUID> ids = new ArrayList<UUID>();
    for (ExpenseData data : expenseList)
      ids.add(data.getId());
    expListAction.rejectExpense(ids);
  }

  @Override
  public void onExpenseActionProcesseStarted(Resource<HashMap<String, List<String>>> loading) {
    HyperLog.d(TAG, "onExpenseActionProcesseStarted loading->" + loading + "<-");
    formState.postValue(ExpenseFormState.actionInProgress(loading.getMessage()));
  }

  @Override
  public void onExpenseActionProcessSuccess(Resource<HashMap<String, List<String>>> response) {
    HyperLog.d(TAG, "onExpenseActionProcessSuccess response->" + response + "<-");
    formState.postValue(ExpenseFormState.ready(response.getMessage()));
    actionEvent.postValue(Resource.Builder.success(response.getMessage(), response.getData()));
  }

  @Override
  public void onExpenseActionProcessFailure(Resource<HashMap<String, List<String>>> response) {
    HyperLog.d(TAG, "onExpenseActionProcessFailure response->" + response + "<-");
    formState.postValue(ExpenseFormState.ready(response.getMessage()));
    actionEvent.postValue(Resource.Builder.error(response.getMessage(), response.getData()));
  }
}
