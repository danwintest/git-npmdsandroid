package id.co.danwinciptaniaga.npmdsandroid;

import java.util.Date;

import javax.inject.Inject;

import com.google.firebase.FirebaseApp;
import com.hypertrack.hyperlog.HyperLog;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import dagger.hilt.android.HiltAndroidApp;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.HasAccountType;
import id.co.danwinciptaniaga.androcon.auth.AccountAuthenticator;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity;

@HiltAndroidApp
public class App extends Application implements HasAccountType {
  private static final String TAG = App.class.getSimpleName();

  public static final String ACCOUNT_TYPE = "id.co.danwinciptaniaga.npmds";
  public static final String AUTH_TOKEN_TYPE = "id.co.danwinciptaniaga.npmds.accessToken";
  public static final String SECRET_KEY_NAME = "npmdsKey";
  public static final String IMAGE_DIR = "images";

  @Inject
  AndroconConfig androconConfig;
  @Inject
  AppExecutors mAppExecutors;
  @Inject
  LoginUtil loginUtil;

  @Override
  public void onCreate() {
    Date start = new Date();
    super.onCreate();
    // HyperLog hanya bisa dipakai setelah super.onCreate dipanggil, karena AndroconConfig akan terinisialisasi di sana
    Log.i(TAG, "App created at " + start);

    // method ini menjalankan inisialisasi secara Async
    androconConfig.init(this, this.mAppExecutors);
    Date end = new Date();
    long duration = end.getTime() - start.getTime();
    Log.i(TAG, String.format("App onCreate took %s ms", duration));
  }

  public AppExecutors getAppExecutors() {
    return this.mAppExecutors;
  }

  public static void gotoLauncher(Context context) {
    HyperLog.i(TAG, "gotoLauncher called");
    Intent i = new Intent(context, LaunchActivity.class);
    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK
        | Intent.FLAG_ACTIVITY_NEW_TASK);
    context.startActivity(i);
  }

  public static void showAuthenticationPage(LoginUtil loginUtil, Activity activity) {
    Intent i = new Intent(activity, AuthenticationActivity.class);
    i.putExtra(AccountAuthenticator.ARG_PERFORM_APP_LOGIN, true);
    if (loginUtil.isLoggedIn()) {
      String username = loginUtil.getUsername();
      HyperLog.i(TAG, "ShowingAuthentication for " + username);
      i.putExtra(AccountManager.KEY_ACCOUNT_NAME, username);
    } else {
      HyperLog.i(TAG, "ShowingAuthentication without specific user");
    }
    activity.startActivityForResult(i, AuthenticationActivity.REQ_GENERAL_AUTH);
  }

  public static void showLandingPage(Context context) {
    Intent i = new Intent(context, LandingActivity.class);
    i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    context.startActivity(i);
  }

  @Override
  public String getAccountType() {
    return ACCOUNT_TYPE;
  }

  @Override
  public String getAuthTokenType() {
    return AUTH_TOKEN_TYPE;
  }
}
