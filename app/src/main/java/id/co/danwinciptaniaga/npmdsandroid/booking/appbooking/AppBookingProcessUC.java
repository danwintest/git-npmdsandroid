package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.net.MediaType;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.OtpProcessResponse;
import id.co.danwinciptaniaga.npmds.data.common.UtilityState;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import okhttp3.MultipartBody;

public class AppBookingProcessUC extends BaseObservable<AppBookingProcessUC.Listener, Boolean> {
  private final String TAG = AppBookingProcessUC.class.getSimpleName();
  private final AppBookingService service;
  private final AppExecutors appExecutors;
  private final Application app;
  private final String doneMsg;

  @Inject
  public AppBookingProcessUC(Application app, AppBookingService service,
      AppExecutors appExecutors) {
    super(app);
    this.service = service;
    this.appExecutors = appExecutors;
    this.app = app;
    doneMsg = this.app.getString(R.string.process_success);
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<Boolean> result) {
    listener.onAppBookingProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<Boolean> result) {
    listener.onAppBookingProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<Boolean> result) {
    listener.onAppBookingProcessFailure(result);
  }

  public void cashier_SaveDraft(UUID appBookingId, UUID outletId, LocalDate bookingDate,
      String bookingTypeId, UUID lobId, String soId, String idnpksf, String cashReason,
      String reason, String appNo, String poNo, LocalDate poDate, List<UUID> poprIdList,
      String consumerName, String consumerAddress, String consumerPhone, String vehicleNo,
      Boolean closeOpen, BigDecimal consumerAmount, BigDecimal fif, BigDecimal biroJasa,
      BigDecimal bookingAmount, BigDecimal matrixFee, BigDecimal schemeFee, String cancelReason,
      UUID bookingId, UUID att_po_id, File att_po, UUID att_stnk_id, File att_stnk, UUID att_kk_id,
      File att_kk, UUID att_bpkb_id, File att_bpkb, UUID att_spt_id, File att_spt,
      UUID att_serah_terima_id, File att_serah_terima, UUID att_fisik_motor_id,
      File att_fisik_motor, Date updateTs, String wfStatus,
      String appOperation, boolean isSaveDraftProcess) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_serah_terima = null;
    if (att_serah_terima != null) {
      final_att_serah_terima = RetrofitUtility.prepareFilePart("att_serah_terima", att_serah_terima,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<String> saveDraft_Lf = service.saveDraftCashNew(appBookingId, outletId,
        bookingDate, bookingTypeId, lobId, soId, idnpksf, cashReason, reason, appNo, poNo, poDate,
        poprIdList, consumerName, consumerAddress, consumerPhone, vehicleNo, closeOpen,
        consumerAmount, fif, biroJasa, bookingAmount, matrixFee, schemeFee, cancelReason, bookingId,
        updateTs, att_po_id, final_att_po, att_stnk_id, final_att_stnk, att_kk_id, final_att_kk,
        att_bpkb_id, final_att_bpkb, att_spt_id, final_att_spt,
        att_serah_terima_id, final_att_serah_terima, att_fisik_motor_id, final_att_fisik_motor,
        wfStatus, appOperation, isSaveDraftProcess);
    String notValidMsg = this.app.getString(R.string.not_valid_document);
    Futures.addCallback(saveDraft_Lf, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String result) {
        HyperLog.d(TAG, "cashier_SaveDraft code[" + result + "]");
        if (UtilityState.DOCUMENT_PROCESS_STATUS_DONE.getId().equals(result))
          notifySuccess(doneMsg, true);
        else if (UtilityState.DOCUMENT_PROCESS_STATUS_NOT_VALID.getId().equals(result))
          notifySuccess(notValidMsg, true);
        else
          notifySuccess("Status tidak diketahui", true);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Simpan Pengajuan Booking gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void cashier_SubmitNew(UUID appBookingID, UUID outletId, LocalDate bookingDate, UUID lobId,
      String soId, String idnpksf, String reason, String cashReason, String applicationNo,
      String poNo, LocalDate poDate, UUID att_po_id, File att_po, List<UUID> poprIdList,
      String consumerName, String consumerAddress, String consumerPhone, String vehicleNo,
      Boolean closeOpen, BigDecimal consumerAmt, BigDecimal biroJasaAmt, BigDecimal fifAmt,
      BigDecimal bookingAmount, BigDecimal matrixFee, BigDecimal schemeFee, UUID att_stnk_id,
      File att_stnk, UUID att_kk_id, File att_kk, UUID att_bpkb_id, File att_bpkb, UUID att_spt_id,
      File att_spt, UUID att_serah_terima_id, File att_serah_terima, UUID att_fisik_motor_id,
      File att_fisik_motor, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_serah_terima = null;
    if (att_serah_terima != null) {
      final_att_serah_terima = RetrofitUtility.prepareFilePart("att_serah_terima", att_serah_terima,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    String notValidMsg = this.app.getString(R.string.not_valid_document);
    ListenableFuture<String> processLf = service.submitCashNew(appBookingID, outletId, bookingDate,
        lobId, soId, idnpksf, reason, cashReason, applicationNo, poNo, poDate, att_po_id,
        final_att_po, poprIdList, consumerName, consumerAddress, consumerPhone, vehicleNo,
        closeOpen, consumerAmt, biroJasaAmt, fifAmt, bookingAmount, matrixFee, schemeFee,
        att_stnk_id, final_att_stnk, att_kk_id, final_att_kk, att_bpkb_id, final_att_bpkb,
        att_spt_id, final_att_spt, att_serah_terima_id, final_att_serah_terima, att_fisik_motor_id,
        final_att_fisik_motor, updateTs);
    Futures.addCallback(processLf, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String result) {
        HyperLog.d(TAG, "submitCashNew code[" + result + "]");
        if (UtilityState.DOCUMENT_PROCESS_STATUS_DONE.getId().equals(result))
          notifySuccess(doneMsg, true);
        else if (UtilityState.DOCUMENT_PROCESS_STATUS_NOT_VALID.getId().equals(result))
          notifySuccess(notValidMsg, true);
        else
          notifySuccess("Status tidak diketahui", true);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_KASIR_REVISION_NEW(UUID procTask, String comment, UUID appBookingId,
      UUID outletId, LocalDate bookingDate, UUID lobId, String soId, String idnpksf, String reason,
      String cashReason, String appNo, String poNo, LocalDate poDate, UUID att_po_id, File att_po,
      List<UUID> poprIdList, String consumerName, String consumerAddress, String consumerPhone,
      String vehicleNo, boolean closeOpen, BigDecimal consumerAmt, BigDecimal biroJasaAmt,
      BigDecimal fifAmt, BigDecimal bookingAmt, BigDecimal matrixFee, BigDecimal schemeFee,
      UUID att_stnk_id, File att_stnk, UUID att_kk_id, File att_kk, UUID att_bpkb_id, File att_bpkb,
      UUID att_spt_id, File att_spt, UUID att_serah_terima_id, File att_serah_terima,
      UUID att_fisik_motor_id, File att_fisik_motor, Date updateTs) {
    HyperLog.d(TAG, "cashier_SubmitRevision terpanggil");
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_serah_terima = null;
    if (att_serah_terima != null) {
      final_att_serah_terima = RetrofitUtility.prepareFilePart("att_serah_terima", att_serah_terima,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<Boolean> processLf = service.submitRevisionCashNew(procTask, comment,
        appBookingId, outletId, bookingDate, lobId, soId, idnpksf, reason, cashReason, appNo, poNo,
        poDate, att_po_id, final_att_po, poprIdList, consumerName, consumerAddress, consumerPhone,
        vehicleNo, closeOpen, consumerAmt, biroJasaAmt, fifAmt, bookingAmt, matrixFee, schemeFee,
        att_stnk_id, final_att_stnk, att_kk_id, final_att_kk, att_bpkb_id, final_att_bpkb,
        att_spt_id, final_att_spt, att_serah_terima_id, final_att_serah_terima,
        att_fisik_motor_id, final_att_fisik_motor, updateTs);

    Futures.addCallback(processLf, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean result) {
        HyperLog.d(TAG, "cashier_SubmitRevision berhasil ");
        notifySuccess("Submit Revision Pengajuan Booking berhasil", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Revision Pengajuan Booking gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void saveDraftCashTransferCancel(UUID appBookingId, String bookingTypeId,
      UUID bookingId, String cancelReason, UUID att_HO_id, File att_HO, Date updateTs, boolean isNewDocument) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    MultipartBody.Part final_att_HO = null;
    if (att_HO != null) {
      final_att_HO = RetrofitUtility.prepareFilePart("att_HO", att_HO,
          MediaType.ANY_IMAGE_TYPE.toString());
    }
    ListenableFuture<String> process = service.saveDraftCashTransferCancel(appBookingId,
        bookingTypeId, bookingId, cancelReason, att_HO_id, final_att_HO, updateTs, isNewDocument);
    String notValidMsg = this.app.getString(R.string.not_valid_document_from_booking);

    Futures.addCallback(process, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String result) {
        HyperLog.d(TAG, "saveDraftCashTransferCancel code[" + result + "]");
        if (UtilityState.DOCUMENT_PROCESS_STATUS_DONE.getId().equals(result))
          notifySuccess(doneMsg, true);
        else if (UtilityState.DOCUMENT_PROCESS_STATUS_NOT_VALID.getId().equals(result))
          notifySuccess(notValidMsg, true);
        else
          notifySuccess("Status tidak diketahui", true);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Simpan Pengajuan Booking Batal gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void submitCashTransfersCancel(UUID appBookingId, String bookingTypeId,
      UUID bookingId, String cancelReason, UUID att_HO_id, File att_HO, Date updateTs, boolean isNewDocument) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    MultipartBody.Part final_att_HO = null;
    if (att_HO != null) {
      final_att_HO = RetrofitUtility.prepareFilePart("att_HO", att_HO,
          MediaType.ANY_IMAGE_TYPE.toString());
    }
    ListenableFuture<String> process = service.submitCashTransfersCancel(appBookingId,
        bookingTypeId, bookingId, cancelReason, att_HO_id, final_att_HO, updateTs, isNewDocument);
    String notValidMsg = this.app.getString(R.string.not_valid_document_from_booking);
    Futures.addCallback(process, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String result) {
        HyperLog.d(TAG, "submit Pengajuan Booking berhasil");
        if (UtilityState.DOCUMENT_PROCESS_STATUS_DONE.getId().equals(result))
          notifySuccess(doneMsg, true);
        else if (UtilityState.DOCUMENT_PROCESS_STATUS_NOT_VALID.getId().equals(result))
          notifySuccess(notValidMsg, true);
        else
          notifySuccess("Status tidak diketahui", true);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Batal gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void submitRevisionCashCancel(UUID procTask, String comment, UUID appBookingId,
      String bookingTypeId, UUID bookingId, String cancelReason, UUID att_HO_id, File att_HO,
      Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    MultipartBody.Part final_att_HO = null;
    if (att_HO != null) {
      final_att_HO = RetrofitUtility.prepareFilePart("att_HO", att_HO,
          MediaType.ANY_IMAGE_TYPE.toString());
    }
    ListenableFuture<Boolean> process = service.submitRevisionCashCancel(procTask, comment,
        appBookingId, bookingTypeId, bookingId, cancelReason, att_HO_id, final_att_HO, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean result) {
        HyperLog.d(TAG, "SubmitRevision appBookingId[" + appBookingId + "] SUCCESS");
        notifySuccess("Submit Revision Pengajuan Booking Batal berhasil", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Revision Pengajuan Booking Batal gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void submitRevisionTransferCancel(UUID procTask, String comment, UUID appBookingId,
      String bookingTypeId, UUID bookingId, String cancelReason, UUID att_HO_id, File att_HO,
      Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    MultipartBody.Part final_att_HO = null;
    if (att_HO != null) {
      final_att_HO = RetrofitUtility.prepareFilePart("att_HO", att_HO,
          MediaType.ANY_IMAGE_TYPE.toString());
    }
    ListenableFuture<Boolean> process = service.submitRevisionCashCancel(procTask, comment,
        appBookingId, bookingTypeId, bookingId, cancelReason, att_HO_id, final_att_HO, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean result) {
        HyperLog.d(TAG, "SubmitRevision appBookingId[" + appBookingId + "] SUCCESS");
        notifySuccess("Submit Revision Pengajuan Booking Batal berhasil", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Revision Pengajuan Booking Batal gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void saveDraftCashEdit(UUID appBookingId, UUID outletId,
      LocalDate bookingDate, UUID lobId, String soId, String idnpksf, String reason,
      String cashReason, String appNo, String poNo, LocalDate poDate, UUID att_po_id, File att_po,
      List<UUID> poprIdList, String consumerName, String consumerAddress, String consumerPhone,
      String vehicleNo, Boolean closeOpen, BigDecimal consumerAmt, BigDecimal biroJasa,
      BigDecimal fif, BigDecimal bookingAmt, BigDecimal matrixFee, BigDecimal schemeFee,
      UUID att_stnk_id, File att_stnk, UUID att_kk_id, File att_kk, UUID att_bpkb_id, File att_bpkb,
      UUID att_spt_id, File att_spt, UUID att_serah_terima_id, File att_serah_terima,
      UUID att_buktiretur_id, File att_buktiretur, UUID att_fisik_motor_id, File att_fisik_motor,
      UUID bookingId, Date updateTs,
      boolean isSaveDraftProcess, boolean isNewDocument) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_serah_terima = null;
    if (att_serah_terima != null) {
      final_att_serah_terima = RetrofitUtility.prepareFilePart("att_serah_terima", att_serah_terima,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_buktiretur = null;
    if (att_buktiretur != null) {
      final_att_buktiretur = RetrofitUtility.prepareFilePart("att_buktiretur", att_buktiretur,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }
    ListenableFuture<String> process = service.saveDraftCashEdit(appBookingId, outletId,
        bookingDate, lobId, soId, idnpksf, reason, cashReason, appNo, poNo, poDate, att_po_id,
        final_att_po, poprIdList, consumerName, consumerAddress, consumerPhone, vehicleNo,
        closeOpen, consumerAmt, biroJasa, fif, bookingAmt, matrixFee, schemeFee, att_stnk_id,
        final_att_stnk, att_kk_id, final_att_kk, att_bpkb_id, final_att_bpkb, att_spt_id,
        final_att_spt, att_serah_terima_id, final_att_serah_terima, att_fisik_motor_id,
        final_att_fisik_motor, bookingId, updateTs, isSaveDraftProcess,
        att_buktiretur_id, final_att_buktiretur, isNewDocument);
    String notValidMsg = this.app.getString(R.string.not_valid_document_from_booking);
    Futures.addCallback(process, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String result) {
        HyperLog.d(TAG, "saveDraftCashEdit code[" + result + "]");
        if (UtilityState.DOCUMENT_PROCESS_STATUS_DONE.getId().equals(result))
          notifySuccess(doneMsg, true);
        else if (UtilityState.DOCUMENT_PROCESS_STATUS_NOT_VALID.getId().equals(result))
          notifySuccess(notValidMsg, true);
        else
          notifySuccess("Status tidak diketahui", true);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Simpan Pengajuan Booking Edit gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());

  }

  public void cashier_SubmitAppBookingCashEdit(UUID appBookingId, UUID outletId,
      LocalDate bookingDate, UUID lobId, String soId, String idnpksf, String reason,
      String cashReason, String appNo, String poNo, LocalDate poDate, UUID att_po_id, File att_po,
      List<UUID> poprIdList, String consumerName, String consumerAddress, String consumerPhone,
      String vehicleNo, Boolean closeOpen, BigDecimal consumerAmt, BigDecimal biroJasa,
      BigDecimal fif, BigDecimal bookingAmt, BigDecimal matrixFee, BigDecimal schemeFee,
      UUID att_stnk_id, File att_stnk, UUID att_kk_id, File att_kk, UUID att_bpkb_id, File att_bpkb,
      UUID att_spt_id, File att_spt, UUID att_serah_terima_id, File att_serah_terima,
      UUID att_fisik_motor_id, File att_fisik_motor, UUID att_buktiretur_id, File att_buktiretur,
      UUID bookingId, Date updateTs, boolean isNewDocument) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      att_po.getName();
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_serah_terima = null;
    if (att_serah_terima != null) {
      final_att_serah_terima = RetrofitUtility.prepareFilePart("att_serah_terima", att_serah_terima,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_buktiretur = null;
    if (att_buktiretur != null) {
      final_att_buktiretur = RetrofitUtility.prepareFilePart("att_buktiretur", att_buktiretur,
          MediaType.ANY_IMAGE_TYPE.toString());
    }
    ListenableFuture<String> process = service.submitCashEdit(appBookingId, outletId, bookingDate,
        lobId, soId, idnpksf, reason, cashReason, appNo, poNo, poDate, att_po_id,
        final_att_po, poprIdList, consumerName, consumerAddress, consumerPhone, vehicleNo,
        closeOpen, consumerAmt, biroJasa, fif, bookingAmt, matrixFee, schemeFee, att_stnk_id,
        final_att_stnk, att_kk_id, final_att_kk, att_bpkb_id, final_att_bpkb, att_spt_id,
        final_att_spt, att_serah_terima_id, final_att_serah_terima, att_fisik_motor_id,
        final_att_fisik_motor, att_buktiretur_id, final_att_buktiretur, bookingId, updateTs,
        isNewDocument);
    String notValidMsg = this.app.getString(R.string.not_valid_document_from_booking);
    Futures.addCallback(process, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String result) {
        HyperLog.d(TAG, "submitCashEdit code[" + result + "]");
        if (UtilityState.DOCUMENT_PROCESS_STATUS_DONE.getId().equals(result))
          notifySuccess(doneMsg, true);
        else if (UtilityState.DOCUMENT_PROCESS_STATUS_NOT_VALID.getId().equals(result))
          notifySuccess(notValidMsg, true);
        else
          notifySuccess("Status tidak diketahui", true);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Edit gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_KASIR_REVISION_EDIT(UUID procTask, String comment,
      UUID appBookingId, UUID outletId, LocalDate bookingDate, UUID lobId, String soId,
      String idnpksf, String reason, String cashReason, String appNo, String poNo, LocalDate poDate,
      UUID att_po_id, File att_po, List<UUID> poprIdList, String consumerName, String consumerAddress,
      String consumerPhone, String vehicleNo, Boolean closeOpen, BigDecimal consumerAmt,
      BigDecimal biroJasa, BigDecimal fif, BigDecimal bookingAmt, BigDecimal matrixFee,
      BigDecimal schemeFee, UUID att_stnk_id, File att_stnk, UUID att_kk_id, File att_kk,
      UUID att_bpkb_id, File att_bpkb, UUID att_spt_id, File att_spt, UUID att_serah_terima_id,
      File att_serah_terima, UUID att_fisik_motor_id, File att_fisik_motor, UUID bookingId,
      Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_serah_terima = null;
    if (att_serah_terima != null) {
      final_att_serah_terima = RetrofitUtility.prepareFilePart("att_serah_terima", att_serah_terima,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }
    ListenableFuture<Boolean> process = service.submitRevisionCashEdit(procTask, comment,
        appBookingId, outletId, bookingDate, lobId, soId, idnpksf, reason, cashReason, appNo, poNo,
        poDate, att_po_id, final_att_po, poprIdList, consumerName, consumerAddress, consumerPhone,
        vehicleNo, closeOpen, consumerAmt, biroJasa, fif, bookingAmt, matrixFee, schemeFee,
        att_stnk_id, final_att_stnk, att_kk_id, final_att_kk, att_bpkb_id, final_att_bpkb,
        att_spt_id, final_att_spt, att_serah_terima_id, final_att_serah_terima, att_fisik_motor_id,
        final_att_fisik_motor, bookingId, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean result) {
        HyperLog.d(TAG, "cashier_SubmitAppBookingCashEdit berhasil ");
        notifySuccess("Submit Revision Pengajuan Booking Edit berhasil", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Revision Pengajuan Booking Edit gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());

  }

  public void saveDraftTransferNew(UUID appBookingId, UUID outletId, LocalDate bookingDate,
      UUID lobId, String soId, String idnpksf, String reason, String appNo, String poNo,
      LocalDate poDate, UUID att_po_id, File att_po, List<UUID> poprIdList, String consumerName,
      String consmerAddress, String consumerPhone, String vehicleNo, Boolean closeOpen,
      BigDecimal consumerAmt, UUID consBankId, String consOtherBank, String consAccountNo,
      String consAccountName, BigDecimal bjAmt, UUID bjBankId, String bjOtherBank,
      String bjAccountNo, String bjAccountName, BigDecimal fifAmt, UUID fifBankId,
      String fifOtherBank, String fifAccountNo, String fifAccountName, BigDecimal bookingAmt,
      BigDecimal matrixFee, BigDecimal schemeFee, UUID att_stnk_id, File att_stnk, UUID att_kk_id,
      File att_kk, UUID att_bpkb_id, File att_bpkb, UUID att_spt_id, File att_spt,
      UUID att_fisik_motor_id, File att_fisik_motor, Date updateTs, String wfStatus,
      String appOperation, Boolean isValidCons, Boolean isValidBJ, Boolean isValidFIF,
      boolean isSaveDraftProcess) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    // MultipartBody.Part final_att_serah_terima = null;
    // if (att_serah_terima != null) {
    // final_att_serah_terima = RetrofitUtility.prepareFilePart("att_serah_terima",
    // att_serah_terima,
    // MediaType.ANY_IMAGE_TYPE.toString());
    // }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<String> process = service.saveDraftTransferNew(appBookingId, outletId,
        bookingDate, lobId, soId, idnpksf, reason, appNo, poNo, poDate, att_po_id, final_att_po,
        poprIdList, consumerName, consmerAddress, consumerPhone, vehicleNo, closeOpen, consumerAmt,
        consBankId, consOtherBank, consAccountNo, consAccountName, bjAmt, bjBankId, bjOtherBank,
        bjAccountNo, bjAccountName, fifAmt, fifBankId, fifOtherBank, fifAccountNo, fifAccountName,
        bookingAmt, matrixFee, schemeFee, att_stnk_id, final_att_stnk, att_kk_id, final_att_kk,
        att_bpkb_id, final_att_bpkb, att_spt_id, final_att_spt, att_fisik_motor_id,
        final_att_fisik_motor, updateTs, wfStatus, appOperation, isValidCons, isValidBJ, isValidFIF,
        isSaveDraftProcess);
    String notValidMsg = this.app.getString(R.string.not_valid_document);

    Futures.addCallback(process, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String res) {
        HyperLog.d(TAG, "saveDraftTransferNew code[" + res + "]");
        if (UtilityState.DOCUMENT_PROCESS_STATUS_DONE.getId().equals(res))
          notifySuccess(doneMsg, true);
        else if (UtilityState.DOCUMENT_PROCESS_STATUS_NOT_VALID.getId().equals(res))
          notifySuccess(notValidMsg, true);
        else
          notifySuccess("Status tidak diketahui", true);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Simpan Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void saveDraftTransferEdit(UUID appBookingId, UUID outletId, LocalDate bookingDate,
      UUID lobId, String soId, String idnpksf, String reason, String appNo, String poNo,
      LocalDate poDate, UUID att_po_id, File att_po, List<UUID> poprIdList, String consumerName,
      String consmerAddress, String consumerPhone, String vehicleNo, Boolean closeOpen,
      BigDecimal consumerAmt, UUID consBankId, String consOtherBank, String consAccountNo,
      String consAccountName, Boolean isValidCons, BigDecimal bjAmt, UUID bjBankId,
      String bjOtherBank, String bjAccountNo, String bjAccountName, Boolean isValidBj,
      BigDecimal fifAmt, UUID fifBankId, String fifOtherBank, String fifAccountNo,
      String fifAccountName, Boolean isValidFIF, BigDecimal bookingAmt, BigDecimal matrixFee,
      BigDecimal schemeFee, UUID att_stnk_id, File att_stnk, UUID att_kk_id, File att_kk,
      UUID att_bpkb_id, File att_bpkb, UUID att_spt_id, File att_spt, UUID att_fisik_motor_id,
      File att_fisik_motor, Date updateTs, UUID att_buktiretur_id, File att_buktiretur,
      UUID bookingId, boolean isSaveDraftProcess, boolean isNewDocument) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_buktiretur = null;
    if (att_buktiretur != null) {
      final_att_buktiretur = RetrofitUtility.prepareFilePart("att_buktiretur", att_buktiretur,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<String> process = service.saveDraftTransferEdit(appBookingId, outletId,
        bookingDate, lobId, soId, idnpksf, reason, appNo, poNo, poDate, att_po_id, final_att_po,
        poprIdList, consumerName, consmerAddress, consumerPhone, vehicleNo, closeOpen, consumerAmt,
        consBankId, consOtherBank, consAccountNo, consAccountName, isValidCons, bjAmt, bjBankId,
        bjOtherBank, bjAccountNo, bjAccountName, isValidBj, fifAmt, fifBankId, fifOtherBank,
        fifAccountNo, fifAccountName, isValidFIF, bookingAmt, matrixFee, schemeFee, att_stnk_id,
        final_att_stnk, att_kk_id, final_att_kk, att_bpkb_id, final_att_bpkb, att_spt_id,
        final_att_spt, att_fisik_motor_id, final_att_fisik_motor, updateTs, att_buktiretur_id,
        final_att_buktiretur, bookingId, isSaveDraftProcess, isNewDocument);
    String notValidMsg = this.app.getString(R.string.not_valid_document_from_booking);

    Futures.addCallback(process, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String res) {
        HyperLog.d(TAG, "saveDraftTransferEdit code[" + res + "]");
        if (UtilityState.DOCUMENT_PROCESS_STATUS_DONE.getId().equals(res))
          notifySuccess(doneMsg, true);
        else if (UtilityState.DOCUMENT_PROCESS_STATUS_NOT_VALID.getId().equals(res))
          notifySuccess(notValidMsg, true);
        else
          notifySuccess("Status tidak diketahui", true);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Simpan Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void submitTransferNew(UUID appBookingId, UUID outletId, LocalDate bookingDate, UUID lobId,
      String soId, String idnpksf, String reason, String appNo, String poNo, LocalDate poDate,
      UUID att_po_id, File att_po, List<UUID> poprIdList, String consumerName,
      String consmerAddress, String consumerPhone, String vehicleNo, Boolean closeOpen,
      BigDecimal consumerAmt, UUID consBankId, String consOtherBank, String consAccountNo,
      String consAccountName, BigDecimal bjAmt, UUID bjBankId, String bjOtherBank,
      String bjAccountNo, String bjAccountName, BigDecimal fifAmt, UUID fifBankId,
      String fifOtherBank, String fifAccountNo, String fifAccountName, BigDecimal bookingAmt,
      BigDecimal matrixFee, BigDecimal schemeFee, UUID att_stnk_id, File att_stnk, UUID att_kk_id,
      File att_kk, UUID att_bpkb_id, File att_bpkb, UUID att_spt_id, File att_spt,
      UUID att_fisik_motor_id, File att_fisik_motor, Date updateTs, Boolean isValidCons,
      Boolean isValidBJ, Boolean isValidFIF) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    // MultipartBody.Part final_att_serah_terima = null;
    // if (att_serah_terima != null) {
    // final_att_serah_terima = RetrofitUtility.prepareFilePart("att_serah_terima",
    // att_serah_terima,
    // MediaType.ANY_IMAGE_TYPE.toString());
    // }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<String> process = service.submitTransferNew(appBookingId, outletId,
        bookingDate, lobId, soId, idnpksf, reason, appNo, poNo, poDate, att_po_id, final_att_po,
        poprIdList, consumerName, consmerAddress, consumerPhone, vehicleNo, closeOpen, consumerAmt,
        consBankId, consOtherBank, consAccountNo, consAccountName, bjAmt, bjBankId, bjOtherBank,
        bjAccountNo, bjAccountName, fifAmt, fifBankId, fifOtherBank, fifAccountNo, fifAccountName,
        bookingAmt, matrixFee, schemeFee, att_stnk_id, final_att_stnk, att_kk_id, final_att_kk,
        att_bpkb_id, final_att_bpkb, att_spt_id, final_att_spt, att_fisik_motor_id,
        final_att_fisik_motor, updateTs, isValidCons, isValidBJ, isValidFIF);
    String notValidMsg = this.app.getString(R.string.not_valid_document);

    Futures.addCallback(process, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String result) {
        HyperLog.d(TAG, "submitTransferNew code[" + result + "]");
        if (UtilityState.DOCUMENT_PROCESS_STATUS_DONE.getId().equals(result))
          notifySuccess(doneMsg, true);
        else if (UtilityState.DOCUMENT_PROCESS_STATUS_NOT_VALID.getId().equals(result))
          notifySuccess(notValidMsg, true);
        else
          notifySuccess("Status tidak diketahui", true);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void submitTransferEdit(UUID appBookingId, UUID outletId, LocalDate bookingDate,
      UUID lobId, String soId, String idnpksf, String reason, String appNo, String poNo,
      LocalDate poDate, UUID att_po_id, File att_po, List<UUID> poprIdList, String consumerName,
      String consmerAddress, String consumerPhone, String vehicleNo, Boolean closeOpen,
      BigDecimal consumerAmt, UUID consBankId, String consOtherBank, String consAccountNo,
      String consAccountName, Boolean isValidCons, BigDecimal bjAmt, UUID bjBankId,
      String bjOtherBank, String bjAccountNo, String bjAccountName, Boolean isValidBj,
      BigDecimal fifAmt, UUID fifBankId, String fifOtherBank, String fifAccountNo,
      String fifAccountName, Boolean isValidFif, BigDecimal bookingAmt, BigDecimal matrixFee,
      BigDecimal schemeFee, UUID att_stnk_id, File att_stnk, UUID att_kk_id, File att_kk,
      UUID att_bpkb_id, File att_bpkb, UUID att_spt_id, File att_spt, UUID att_fisik_motor_id,
      File att_fisik_motor, UUID att_buktiretur_id, File att_buktiretur, UUID bookingId,
      Date updateTs, boolean isNewDocument) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_buktiretur = null;
    if (att_buktiretur != null) {
      final_att_buktiretur = RetrofitUtility.prepareFilePart("att_buktiretur", att_buktiretur,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<String> process = service.submitTransferEdit(appBookingId, outletId,
        bookingDate, lobId, soId, idnpksf, reason, appNo, poNo, poDate, att_po_id, final_att_po,
        poprIdList, consumerName, consmerAddress, consumerPhone, vehicleNo, closeOpen, consumerAmt,
        consBankId, consOtherBank, consAccountNo, consAccountName, isValidCons, bjAmt, bjBankId,
        bjOtherBank, bjAccountNo, bjAccountName, isValidBj, fifAmt, fifBankId, fifOtherBank,
        fifAccountNo, fifAccountName, isValidFif, bookingAmt, matrixFee, schemeFee, att_stnk_id,
        final_att_stnk, att_kk_id, final_att_kk, att_bpkb_id, final_att_bpkb, att_spt_id,
        final_att_spt, bookingId, updateTs, att_fisik_motor_id, final_att_fisik_motor,
        att_buktiretur_id, final_att_buktiretur, isNewDocument);
    String notValidMsg = this.app.getString(R.string.not_valid_document_from_booking);
    Futures.addCallback(process, new FutureCallback<String>() {
      @Override
      public void onSuccess(@NullableDecl String result) {
        HyperLog.d(TAG, "submitTransferEdit code[" + result + "]");
        if (UtilityState.DOCUMENT_PROCESS_STATUS_DONE.getId().equals(result))
          notifySuccess(doneMsg, true);
        else if (UtilityState.DOCUMENT_PROCESS_STATUS_NOT_VALID.getId().equals(result))
          notifySuccess(notValidMsg, true);
        else
          notifySuccess("Status tidak diketahui", true);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void submitRevisionTransferEdit(UUID proTaskId, UUID appBookingId, String comment,
      UUID outletId, LocalDate bookingDate,
      UUID lobId, String soId, String idnpksf, String reason, String appNo, String poNo,
      LocalDate poDate, UUID att_po_id, File att_po, List<UUID> poprIdList, String consumerName,
      String consmerAddress, String consumerPhone, String vehicleNo, Boolean closeOpen,
      BigDecimal consumerAmt, UUID consBankId, String consOtherBank, String consAccountNo,
      String consAccountName, Boolean isValidCons, BigDecimal bjAmt, UUID bjBankId,
      String bjOtherBank, String bjAccountNo, String bjAccountName, Boolean isValidBj,
      BigDecimal fifAmt, UUID fifBankId, String fifOtherBank, String fifAccountNo,
      String fifAccountName, Boolean isValidFif, BigDecimal bookingAmt, BigDecimal matrixFee,
      BigDecimal schemeFee, UUID att_stnk_id, File att_stnk, UUID att_kk_id, File att_kk,
      UUID att_bpkb_id, File att_bpkb, UUID att_spt_id, File att_spt, UUID att_fisik_motor_id,
      File att_fisik_motor, UUID bookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<Boolean> process = service.submitRevisionTransferEdit(proTaskId, comment,
        appBookingId, outletId, bookingDate, lobId, soId, idnpksf, reason, appNo, poNo, poDate,
        att_po_id, final_att_po, poprIdList, consumerName, consmerAddress, consumerPhone, vehicleNo,
        closeOpen, consumerAmt, consBankId, consOtherBank, consAccountNo, consAccountName, isValidCons, bjAmt,
        bjBankId, bjOtherBank, bjAccountNo, bjAccountName, isValidBj, fifAmt, fifBankId,
        fifOtherBank, fifAccountNo, fifAccountName, isValidFif, bookingAmt, matrixFee, schemeFee,
        att_stnk_id, final_att_stnk, att_kk_id, final_att_kk, att_bpkb_id, final_att_bpkb,
        att_spt_id, final_att_spt, att_fisik_motor_id, final_att_fisik_motor,
        bookingId, updateTs);

    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "submitRevisionTransferEdit berhasil");
        notifySuccess("Submit Pengajuan Booking Berhasil", res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_DOC_APPROVE_NEW(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_DOC_APPROVE_NEW(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_DOC_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_DOC_RETURN_NEW(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_DOC_RETURN_NEW(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_DOC_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_CAPTAIN_APPROVE_NEW(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_CAPTAIN_APPROVE_NEW(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_CAPTAIN_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_CAPTAIN_RETURN_NEW(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_CAPTAIN_RETURN_NEW(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_CAPTAIN_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_AOC_APPROVE_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_AOC_APPROVE_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_AOC_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_AOC_RETURN_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_AOC_RETURN_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_AOC_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_AOC_REJECT_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_AOC_REJECT_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_AOC_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_CAPTAIN_APPROVE_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_CAPTAIN_APPROVE_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_CAPTAIN_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_CAPTAIN_RETURN_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_CAPTAIN_RETURN_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_CAPTAIN_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_CAPTAIN_REJECT_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_CAPTAIN_REJECT_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_CAPTAIN_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_DIRECTOR_APPROVE_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_DIRECTOR_APPROVE_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_DIRECTOR_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_DIRECTOR_RETURN_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_DIRECTOR_RETURN_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_DIRECTOR_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_DIRECTOR_REJECT_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_DIRECTOR_REJECT_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_DIRECTOR_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_FINRETURN_APPROVE_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_FINRETURN_APPROVE_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_FINRETURN_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_FINRETURN_RETURN_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_FINRETURN_RETURN_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_FINRETURN_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_FINRETURN_REJECT_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_FINRETURN_REJECT_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_FINRETURN_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_AOC_APPROVE_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_AOC_APPROVE_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_AOC_APPROVE_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_AOC_RETURN_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_AOC_RETURN_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_AOC_RETURN_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_AOC_REJECT_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_AOC_REJECT_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_AOC_REJECT_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_CAPTAIN_APPROVE_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_CAPTAIN_APPROVE_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_CAPTAIN_APPROVE_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_CAPTAIN_RETURN_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_CAPTAIN_RETURN_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_CAPTAIN_RETURN_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_CAPTAIN_REJECT_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_CAPTAIN_REJECT_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_CAPTAIN_REJECT_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_DIRECTOR_APPROVE_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_DIRECTOR_APPROVE_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_DIRECTOR_APPROVE_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_DIRECTOR_RETURN_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_DIRECTOR_RETURN_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_DIRECTOR_RETURN_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_DIRECTOR_REJECT_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_DIRECTOR_REJECT_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_DIRECTOR_REJECT_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_FINRETURN_APPROVE_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_FINRETURN_APPROVE_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_FINRETURN_APPROVE_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_FINRETURN_RETURN_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_FINRETURN_RETURN_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_FINRETURN_RETURN_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processCashWF_FINRETURN_REJECT_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processCashWF_FINRETURN_REJECT_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processCashWF_FINRETURN_REJECT_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_KASIR_SUBMIT_REVISION_NEW(UUID procTask, String comment,
      UUID appBookingId,
      UUID outletId, LocalDate bookingDate, UUID lobId, String soId, String idnpksf, String reason,
      String appNo, String poNo, LocalDate poDate, UUID att_po_id, File att_po,
      List<UUID> poprIdList, String consumerName, String consmerAddress, String consumerPhone,
      String vehicleNo,
      Boolean closeOpen, BigDecimal consumerAmt, UUID consBankId, String consOtherBank,
      String consAccountNo, String consAccountName, Boolean isValidCons, BigDecimal bjAmt,
      UUID bjBankId, String bjOtherBank, String bjAccountNo, String bjAccountName,
      Boolean isValidBj, BigDecimal fifAmt, UUID fifBankId, String fifOtherBank,
      String fifAccountNo, String fifAccountName, Boolean isValidFIF, BigDecimal bookingAmt,
      BigDecimal matrixFee, BigDecimal schemeFee, UUID att_stnk_id, File att_stnk, UUID att_kk_id,
      File att_kk, UUID att_bpkb_id, File att_bpkb, UUID att_spt_id, File att_spt,
      UUID att_fisik_motor_id, File att_fisik_motor, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<Boolean> process = service.processTransWF_KASIR_SUBMIT_REVISION_NEW(
        procTask, comment, appBookingId, outletId, bookingDate, lobId, soId, idnpksf, reason, appNo,
        poNo, poDate, att_po_id, final_att_po, poprIdList, consumerName, consmerAddress,
        consumerPhone, vehicleNo, closeOpen, consumerAmt, consBankId, consOtherBank, consAccountNo,
        consAccountName, isValidCons, bjAmt, bjBankId, bjOtherBank, bjAccountNo, bjAccountName,
        isValidBj, fifAmt, fifBankId, fifOtherBank, fifAccountNo, fifAccountName, isValidFIF,
        bookingAmt, matrixFee, schemeFee, att_stnk_id, final_att_stnk, att_kk_id, final_att_kk,
        att_bpkb_id, final_att_bpkb, att_spt_id, final_att_spt, att_fisik_motor_id,
        final_att_fisik_motor, updateTs);

    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_KASIR_SUBMIT_REVISION_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_AOC_APPROVE_NEW(UUID procTaskId, String comment, UUID appBookingId,
      Date updateTs, String batchId, boolean opTransfer ) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_AOC_APPROVE_NEW(procTaskId, comment,
        appBookingId, updateTs, batchId, opTransfer);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_AOC_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_AOC_RETURN_NEW(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_AOC_RETURN_NEW(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_AOC_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_CAPTAIN_APPROVE_NEW(UUID procTaskId, String comment, UUID appBookingId,
      Date updateTs, String batchId, boolean opTransfer) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_CAPTAIN_APPROVE_NEW(procTaskId,
        comment, appBookingId, updateTs, batchId, opTransfer);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_CAPTAIN_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_CAPTAIN_RETURN_NEW(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_CAPTAIN_RETURN_NEW(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_CAPTAIN_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_CUS_RETURN_NEW(UUID txnProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_CUS_RETURN_NEW(txnProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_CUS_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_CUS_APPROVE_NEW(UUID txnProcTaskId, String comment,
      LocalDate transferDate, UUID cbaId, boolean isManual, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_CUS_APPROVE_NEW(txnProcTaskId,
        comment, transferDate, cbaId, isManual, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_CUS_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_CUS_APPROVE_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_CUS_APPROVE_NEW(
        txnProcTaskId, comment, appBookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_CUS_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_CUS_RETURN_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_CUS_RETURN_NEW(
        txnProcTaskId, comment, appBookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_CUS_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_KASIR_CUS_REVISION_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, UUID outletId, LocalDate bookingDate, UUID lobId, String soId,
      String idnpksf, String reason, String appNo, String poNo, LocalDate poDate, UUID att_po_id,
      File att_po, List<UUID> poprIdList, String consumerName, String consmerAddress,
      String consumerPhone, String vehicleNo, BigDecimal consumerAmt, UUID consBankId,
      String consOtherBank, String consAccountNo, String consAccountName, Boolean isValidCons,
      BigDecimal bookingAmt, BigDecimal matrixFee, BigDecimal schemeFee, UUID att_stnk_id,
      File att_stnk, UUID att_kk_id, File att_kk, UUID att_bpkb_id, File att_bpkb,
      UUID att_spt_id, File att_spt, UUID att_fisik_motor_id, File att_fisik_motor,
      Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<Boolean> process = service.processTransWF_KASIR_CUS_REVISION_NEW(
        txnProcTaskId, comment, appBookingId, outletId, bookingDate, lobId, soId, idnpksf, reason,
        appNo, poNo, poDate, att_po_id, final_att_po, poprIdList, consumerName, consmerAddress,
        consumerPhone, vehicleNo, consumerAmt, consBankId, consOtherBank, consAccountNo,
        consAccountName, isValidCons, bookingAmt, matrixFee, schemeFee, att_stnk_id, final_att_stnk,
        att_kk_id, final_att_kk, att_bpkb_id, final_att_bpkb, att_spt_id, final_att_spt,
        att_fisik_motor_id, final_att_fisik_motor, txnConsUpdateTs);

    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_KASIR_CUS_REVISION_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_CUS_RETURN_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_CUS_RETURN_NEW(
        txnProcTaskId, comment, appBookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_CUS_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_CUS_APPROVE_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_CUS_APPROVE_NEW(
        txnProcTaskId, comment, appBookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_CUS_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_CUS_APPROVE_NEW(UUID txnConsProcTaskId, String comment,
      String OTP, Boolean isManual) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<OtpProcessResponse> process = service.processTransTxn_WF_FINSPV_CUS_APPROVE_NEW(
        txnConsProcTaskId, comment, OTP, isManual);
    Futures.addCallback(process, new FutureCallback<OtpProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl OtpProcessResponse res) {
        if(res.getOtpVerifySuccess()) {
          HyperLog.d(TAG, "processTransTxn_WF_FINSPV_CUS_APPROVE_NEW berhasil");
          notifySuccess(doneMsg, res.getOtpVerifySuccess());
        }
        else{
          HyperLog.exception(TAG, "processTransTxn_WF_FINSPV_CUS_APPROVE_NEW gagal, verifyOtp gagal"
              + res.getMessage());
          notifySuccess(res.getMessage(), res.getOtpVerifySuccess());
        }
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_CUS_RETURN_NEW(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_CUS_RETURN_NEW(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_CUS_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_CUS_REJECT_NEW(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_CUS_REJECT_NEW(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_CUS_REJECT_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_BJ_RETURN_NEW(UUID txnProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_BJ_RETURN_NEW(txnProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_BJ_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_BJ_APPROVE_NEW(UUID txnProcTaskId, String comment,
      LocalDate transferDate, UUID cbaId, boolean isManual, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_BJ_APPROVE_NEW(txnProcTaskId,
        comment, transferDate, cbaId, isManual, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_BJ_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_BJ_APPROVE_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_BJ_APPROVE_NEW(
        txnProcTaskId, comment, appBookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_BJ_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_BJ_RETURN_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_BJ_RETURN_NEW(
        txnProcTaskId, comment, appBookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_BJ_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_KASIR_BJ_REVISION_NEW(UUID txnProcTaskId, UUID txnBjId, String comment,
      UUID appBookingId, UUID outletId, BigDecimal bjAmt, UUID bjBankId, String bjOtherBank,
      String bjAccountNo, String bjAccountName, Boolean isValidBj, BigDecimal bookingAmt,
      Date txnBjUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    ListenableFuture<Boolean> process = service.processTransWF_KASIR_BJ_REVISION_NEW(
        txnProcTaskId, txnBjId, comment, appBookingId, outletId, bjAmt, bjBankId, bjOtherBank,
        bjAccountNo, bjAccountName, isValidBj, bookingAmt, txnBjUpdateTs);

    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_KASIR_BJ_REVISION_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_BJ_RETURN_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnBjUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_BJ_RETURN_NEW(
        txnProcTaskId, comment, appBookingId, txnBjUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_BJ_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_BJ_APPROVE_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnBjUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_BJ_APPROVE_NEW(
        txnProcTaskId, comment, appBookingId, txnBjUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_BJ_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_BJ_APPROVE_NEW(UUID txnBjProcTaskId, String comment,
      String OTP, Boolean isManual) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<OtpProcessResponse> process = service.processTransTxn_WF_FINSPV_BJ_APPROVE_NEW(
        txnBjProcTaskId, comment, OTP, isManual);
    Futures.addCallback(process, new FutureCallback<OtpProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl OtpProcessResponse res) {
        if (res.getOtpVerifySuccess()) {
          HyperLog.d(TAG, "processTransTxn_WF_FINSPV_BJ_APPROVE_NEW berhasil");
          notifySuccess(doneMsg, res.getOtpVerifySuccess());
        } else {
          HyperLog.exception(TAG, "processTransTxn_WF_FINSPV_BJ_APPROVE_NEW gagal, verifyOtp gagal"
              + res.getMessage());
          notifySuccess(res.getMessage(), res.getOtpVerifySuccess());

        }
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_BJ_RETURN_NEW(UUID txnBjProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_BJ_RETURN_NEW(txnBjProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_BJ_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_BJ_REJECT_NEW(UUID txnBjProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_BJ_REJECT_NEW(txnBjProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_BJ_REJECT_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_FIF_RETURN_NEW(UUID txnFIFProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_FIF_RETURN_NEW(txnFIFProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_FIF_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_FIF_APPROVE_NEW(UUID txnFIFProcTaskId, String comment,
      LocalDate transferDate, UUID cbaId, boolean isManual, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_FIF_APPROVE_NEW(txnFIFProcTaskId,
        comment, transferDate, cbaId, isManual, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_FIF_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_FIF_APPROVE_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnFIFUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_FIF_APPROVE_NEW(
        txnProcTaskId, comment, appBookingId, txnFIFUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_FIF_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_FIF_RETURN_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnFIFUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_FIF_RETURN_NEW(
        txnProcTaskId, comment, appBookingId, txnFIFUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_FIF_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_KASIR_FIF_REVISION_NEW(UUID txnProcTaskId, UUID txnFIFId, String comment,
      UUID appBookingId, UUID outletId, boolean closeOpen, BigDecimal fifAmt, UUID fifBankId, String fifOtherBank,
      String fifAccountNo, String fifAccountName, Boolean isValidFif, BigDecimal bookingAmt,
      Date txnFifUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    ListenableFuture<Boolean> process = service.processTransWF_KASIR_FIF_REVISION_NEW(
        txnProcTaskId, txnFIFId, comment, appBookingId, outletId, closeOpen, fifAmt, fifBankId,
        fifOtherBank, fifAccountNo, fifAccountName, isValidFif, bookingAmt, txnFifUpdateTs);

    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_KASIR_FIF_REVISION_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_FIF_RETURN_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnFIFUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_FIF_RETURN_NEW(
        txnProcTaskId, comment, appBookingId, txnFIFUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_FIF_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_FIF_APPROVE_NEW(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnFIFUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_FIF_APPROVE_NEW(
        txnProcTaskId, comment, appBookingId, txnFIFUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_FIF_APPROVE_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_FIF_APPROVE_NEW(UUID txnFIFProcTaskId, String comment,
      String OTP, Boolean isManual) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<OtpProcessResponse> process = service.processTransTxn_WF_FINSPV_FIF_APPROVE_NEW(
        txnFIFProcTaskId, comment, OTP, isManual);
    Futures.addCallback(process, new FutureCallback<OtpProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl OtpProcessResponse res) {
        if (res.getOtpVerifySuccess()) {
          HyperLog.d(TAG, "processTransTxn_WF_FINSPV_FIF_APPROVE_NEW berhasil");
          notifySuccess(doneMsg, res.getOtpVerifySuccess());
        } else {
          HyperLog.exception(TAG, "processTransTxn_WF_FINSPV_FIF_APPROVE_NEW gagal, verifyOtp gagal"
              + res.getMessage());
          notifySuccess(res.getMessage(), res.getOtpVerifySuccess());

        }
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_FIF_RETURN_NEW(UUID txnFIFProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_FIF_RETURN_NEW(txnFIFProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_FIF_RETURN_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_FIF_REJECT_NEW(UUID txnFIFProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_FIF_REJECT_NEW(
        txnFIFProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_FIF_REJECT_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_AOC_APPROVE_EDIT(UUID procTaskId, String comment, UUID appBookingId,
      Date updateTs, String batchId, boolean opTransfer) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_AOC_APPROVE_EDIT(procTaskId, comment,
        appBookingId, updateTs, batchId, opTransfer);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_AOC_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_AOC_RETURN_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_AOC_RETURN_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_AOC_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_AOC_REJECT_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_AOC_REJECT_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_AOC_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_KASIR_SUBMIT_REVISION_EDIT(UUID procTask, String comment,
      UUID appBookingId, UUID outletId, LocalDate bookingDate, UUID lobId, String soId,
      String idnpksf, String reason, String appNo, String poNo, LocalDate poDate, UUID att_po_id,
      File att_po, List<UUID> poprIdList, String consumerName, String consmerAddress,
      String consumerPhone, String vehicleNo, Boolean closeOpen, BigDecimal consumerAmt,
      UUID consBankId, String consOtherBank, String consAccountNo, String consAccountName,
      Boolean isValidCons, BigDecimal bjAmt, UUID bjBankId, String bjOtherBank, String bjAccountNo,
      String bjAccountName, Boolean isValidBj, BigDecimal fifAmt, UUID fifBankId,
      String fifOtherBank, String fifAccountNo, String fifAccountName, Boolean isValidFIF,
      BigDecimal bookingAmt, BigDecimal matrixFee, BigDecimal schemeFee, UUID att_stnk_id,
      File att_stnk, UUID att_kk_id, File att_kk, UUID att_bpkb_id, File att_bpkb, UUID att_spt_id,
      File att_spt, UUID att_fisik_motor_id, File att_fisik_motor, UUID bookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<Boolean> process = service.processTransWF_KASIR_SUBMIT_REVISION_EDIT(
        procTask, comment, appBookingId, outletId, bookingDate, lobId, soId, idnpksf, reason, appNo,
        poNo, poDate, att_po_id, final_att_po, poprIdList, consumerName, consmerAddress,
        consumerPhone, vehicleNo, closeOpen, consumerAmt, consBankId, consOtherBank, consAccountNo,
        consAccountName, isValidCons, bjAmt, bjBankId, bjOtherBank, bjAccountNo, bjAccountName,
        isValidBj, fifAmt, fifBankId, fifOtherBank, fifAccountNo, fifAccountName, isValidFIF,
        bookingAmt, matrixFee, schemeFee, att_stnk_id, final_att_stnk, att_kk_id, final_att_kk,
        att_bpkb_id, final_att_bpkb, att_spt_id, final_att_spt, att_fisik_motor_id,
        final_att_fisik_motor, bookingId, updateTs);

    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_KASIR_SUBMIT_REVISION_NEW berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_CAPTAIN_APPROVE_EDIT(UUID procTaskId, String comment,
      UUID appBookingId, Date updateTs, boolean opTransfer) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_CAPTAIN_APPROVE_EDIT(procTaskId,
        comment, appBookingId, updateTs, opTransfer);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_CAPTAIN_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_CAPTAIN_RETURN_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_CAPTAIN_RETURN_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_CAPTAIN_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_CAPTAIN_REJECT_EDIT(UUID procTaskId, String comment, UUID appBookingId,
      Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_CAPTAIN_REJECT_EDIT(procTaskId,
        comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_CAPTAIN_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_DIRECTOR_APPROVE_EDIT(UUID procTaskId, String comment,
      UUID appBookingId, UUID bookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_DIRECTOR_APPROVE_EDIT(procTaskId,
        comment, appBookingId, bookingId, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_DIRECTOR_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_DIRECTOR_RETURN_EDIT(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_DIRECTOR_RETURN_EDIT(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_DIRECTOR_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_DIRECTOR_REJECT_EDIT(UUID procTaskId, String comment, UUID appBookingId,
      Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_DIRECTOR_REJECT_EDIT(procTaskId,
        comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_DIRECTOR_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_CUS_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_CUS_RETURN_EDIT(
        txnProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_CUS_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_CUS_REJECT_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_CUS_REJECT_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_CUS_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_CUS_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      LocalDate transferDate, UUID cbaId, boolean isManual, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_CUS_APPROVE_EDIT(
        txnProcTaskId,
        comment, transferDate, cbaId, isManual, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_CUS_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_DIRECTOR_CUS_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, UUID bookingId, BigDecimal consAmt, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_DIRECTOR_CUS_APPROVE_EDIT(
        txnProcTaskId,  comment, appBookingId, bookingId, consAmt, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_DIRECTOR_CUS_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_DIRECTOR_CUS_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_DIRECTOR_CUS_RETURN_EDIT(
        txnProcTaskId,  comment, appBookingId, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_DIRECTOR_CUS_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_DIRECTOR_CUS_REJECT_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_DIRECTOR_CUS_REJECT_EDIT(
        txnProcTaskId,  comment, appBookingId, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_DIRECTOR_CUS_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_KASIR_CUS_REVISION_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, UUID outletId, LocalDate bookingDate, UUID lobId, String soId,
      String idnpksf, String reason, String appNo, String poNo, LocalDate poDate, UUID att_po_id,
      File att_po, List<UUID> poprIdList, String consumerName, String consmerAddress,
      String consumerPhone, String vehicleNo, BigDecimal consumerAmt, UUID consBankId,
      String consOtherBank, String consAccountNo, String consAccountName, Boolean isValidCons,
      BigDecimal bookingAmt, BigDecimal matrixFee, BigDecimal schemeFee, UUID att_stnk_id,
      File att_stnk, UUID att_kk_id, File att_kk, UUID att_bpkb_id, File att_bpkb,
      UUID att_spt_id, File att_spt, UUID att_fisik_motor_id, File att_fisik_motor,
      UUID att_buktiretur_id, File att_buktiretur, UUID bookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_po = null;
    if (att_po != null) {
      final_att_po = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_stnk = null;
    if (att_stnk != null) {
      final_att_stnk = RetrofitUtility.prepareFilePart("att_stnk", att_stnk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_kk = null;
    if (att_kk != null) {
      final_att_kk = RetrofitUtility.prepareFilePart("att_kk", att_kk,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_bpkb = null;
    if (att_bpkb != null) {
      final_att_bpkb = RetrofitUtility.prepareFilePart("att_bpkb", att_bpkb,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_spt = null;
    if (att_spt != null) {
      final_att_spt = RetrofitUtility.prepareFilePart("att_spt", att_spt,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_fisik_motor = null;
    if (att_fisik_motor != null) {
      final_att_fisik_motor = RetrofitUtility.prepareFilePart("att_fisik_motor", att_fisik_motor,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    MultipartBody.Part final_att_buktiretur = null;
    if (att_buktiretur != null) {
      final_att_buktiretur = RetrofitUtility.prepareFilePart("att_buktiretur", att_buktiretur,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<Boolean> process = service.processTransWF_KASIR_CUS_REVISION_EDIT(
        txnProcTaskId, comment, appBookingId, outletId, bookingDate, lobId, soId, idnpksf, reason,
        appNo, poNo, poDate, att_po_id, final_att_po, poprIdList, consumerName, consmerAddress,
        consumerPhone, vehicleNo, consumerAmt, consBankId, consOtherBank, consAccountNo,
        consAccountName, isValidCons, bookingAmt, matrixFee, schemeFee, att_stnk_id,
        final_att_stnk, att_kk_id, final_att_kk, att_bpkb_id, final_att_bpkb,
        att_spt_id, final_att_spt, att_fisik_motor_id, final_att_fisik_motor, att_buktiretur_id,
        final_att_buktiretur, bookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_KASIR_CUS_REVISION_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_CUS_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_CUS_RETURN_NEW(
        txnProcTaskId, comment, appBookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_CUS_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_CUS_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_CUS_APPROVE_EDIT(
        txnProcTaskId, comment, appBookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_CUS_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_CUS_REJECT_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_CUS_REJECT_EDIT(
        txnProcTaskId,  comment, appBookingId, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_CUS_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_CUS_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_CUS_RETURN_EDIT(
        txnProcTaskId, comment, appBookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_CUS_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_CUS_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnConsUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_CUS_APPROVE_EDIT(
        txnProcTaskId, comment, appBookingId, txnConsUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_CUS_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_CUS_REJECT_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_CUS_REJECT_EDIT(
        txnProcTaskId,  comment, appBookingId, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_CUS_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_CUS_APPROVE_EDIT(UUID txnConsProcTaskId, String comment,
      String OTP, Boolean isManual) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<OtpProcessResponse> process = service.processTransTxn_WF_FINSPV_CUS_APPROVE_EDIT(
        txnConsProcTaskId, comment, OTP, isManual);
    Futures.addCallback(process, new FutureCallback<OtpProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl OtpProcessResponse res) {
        if(res.getOtpVerifySuccess()) {
          HyperLog.d(TAG, "processTransTxn_WF_FINSPV_CUS_APPROVE_EDIT berhasil");
          notifySuccess(doneMsg, res.getOtpVerifySuccess());
        }
        else{
          HyperLog.exception(TAG, "processTransTxn_WF_FINSPV_CUS_APPROVE_EDIT gagal, verifyOtp gagal"
              + res.getMessage());
          notifySuccess(res.getMessage(), res.getOtpVerifySuccess());
        }
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_CUS_RETURN_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_CUS_RETURN_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_CUS_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_CUS_REJECT_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_CUS_REJECT_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_CUS_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_BJ_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_BJ_RETURN_EDIT(
        txnProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_BJ_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_BJ_REJECT_EDIT(UUID txnBjProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_BJ_REJECT_EDIT(txnBjProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_BJ_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_BJ_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      LocalDate transferDate, UUID cbaId, boolean isManual, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_BJ_APPROVE_EDIT(
        txnProcTaskId,
        comment, transferDate, cbaId, isManual, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_BJ_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_DIRECTOR_BJ_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, UUID bookingId, BigDecimal bjAmt, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_DIRECTOR_BJ_APPROVE_EDIT(
        txnProcTaskId,  comment, appBookingId, bookingId, bjAmt, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_DIRECTOR_BJ_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_DIRECTOR_BJ_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_DIRECTOR_BJ_RETURN_EDIT(
        txnProcTaskId,  comment, appBookingId, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_DIRECTOR_BJ_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_DIRECTOR_BJ_REJECT_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_DIRECTOR_BJ_REJECT_EDIT(
        txnProcTaskId,  comment, appBookingId, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_DIRECTOR_BJ_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_KASIR_BJ_REVISION_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, UUID outletId, UUID txnBjId, BigDecimal bjAmt, UUID bjBankId,
      String bjOtherBank, String bjAccountNo, String bjAccountName, Boolean isValidBj,
      BigDecimal bookingAmt, UUID att_buktiretur_id, File att_buktiretur, UUID bookingId,
      Date txnBjUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_buktiretur = null;
    if (att_buktiretur != null) {
      final_att_buktiretur = RetrofitUtility.prepareFilePart("att_buktiretur", att_buktiretur,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<Boolean> process = service.processTransWF_KASIR_BJ_REVISION_EDIT(
        txnProcTaskId, comment, appBookingId, outletId, txnBjId, bjAmt, bjBankId, bjOtherBank,
        bjAccountNo, bjAccountName, isValidBj, bookingAmt, att_buktiretur_id,
        final_att_buktiretur, bookingId, txnBjUpdateTs);

    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_KASIR_BJ_REVISION_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_BJ_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnBjUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_BJ_RETURN_EDIT(
        txnProcTaskId, comment, appBookingId, txnBjUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_BJ_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_BJ_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnBjUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_BJ_APPROVE_EDIT(
        txnProcTaskId, comment, appBookingId, txnBjUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_BJ_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_BJ_REJECT_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnBjUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_BJ_REJECT_EDIT(
        txnProcTaskId,  comment, appBookingId, txnBjUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_BJ_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_BJ_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnBjUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_BJ_RETURN_EDIT(
        txnProcTaskId, comment, appBookingId, txnBjUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_BJ_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_BJ_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnBjUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_BJ_APPROVE_EDIT(
        txnProcTaskId, comment, appBookingId, txnBjUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_BJ_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_BJ_REJECT_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnBjUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_BJ_REJECT_EDIT(
        txnProcTaskId,  comment, appBookingId, txnBjUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_BJ_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_BJ_APPROVE_EDIT(UUID txnConsProcTaskId, String comment,
      String OTP, Boolean isManual) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<OtpProcessResponse> process = service.processTransTxn_WF_FINSPV_BJ_APPROVE_EDIT(txnConsProcTaskId, comment, OTP, isManual);
    Futures.addCallback(process, new FutureCallback<OtpProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl OtpProcessResponse res) {
        if(res.getOtpVerifySuccess()) {
          HyperLog.d(TAG, "processTransTxn_WF_FINSPV_BJ_APPROVE_EDIT berhasil");
          notifySuccess(doneMsg, res.getOtpVerifySuccess());
        }
        else{
          HyperLog.exception(TAG, "processTransTxn_WF_FINSPV_BJ_APPROVE_EDIT gagal, verifyOtp gagal"
              + res.getMessage());
          notifySuccess(res.getMessage(), res.getOtpVerifySuccess());
        }
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_BJ_RETURN_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_BJ_RETURN_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_BJ_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_BJ_REJECT_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_BJ_REJECT_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_BJ_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_FIF_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_FIF_RETURN_EDIT(
        txnProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_FIF_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_FIF_REJECT_EDIT(UUID txnBjProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_FIF_REJECT_EDIT(txnBjProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_FIF_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINST_FIF_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      LocalDate transferDate, UUID cbaId, boolean isManual, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINST_FIF_APPROVE_EDIT(
        txnProcTaskId,
        comment, transferDate, cbaId, isManual, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINST_FIF_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_DIRECTOR_FIF_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, UUID bookingId, BigDecimal fifAmt, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_DIRECTOR_FIF_APPROVE_EDIT(
        txnProcTaskId,  comment, appBookingId, bookingId, fifAmt, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_DIRECTOR_FIF_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_DIRECTOR_FIF_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_DIRECTOR_FIF_RETURN_EDIT(
        txnProcTaskId,  comment, appBookingId, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_DIRECTOR_FIF_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_DIRECTOR_FIF_REJECT_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_DIRECTOR_FIF_REJECT_EDIT(
        txnProcTaskId,  comment, appBookingId, updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_DIRECTOR_FIF_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_KASIR_FIF_REVISION_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, UUID outletId,UUID txnFifId, boolean closeOpen, BigDecimal fifAmt, UUID fifBankId, String fifOtherBank,
      String fifAccountNo, String fifAccountName, Boolean isValidFif, BigDecimal bookingAmt, File att_buktiretur, UUID bookingId,
      Date txnFifUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);

    MultipartBody.Part final_att_buktiretur = null;
    if (att_buktiretur != null) {
      final_att_buktiretur = RetrofitUtility.prepareFilePart("att_buktiretur", att_buktiretur,
          MediaType.ANY_IMAGE_TYPE.toString());
    }

    ListenableFuture<Boolean> process = service.processTransWF_KASIR_FIF_REVISION_EDIT(
        txnProcTaskId, comment, appBookingId, outletId, txnFifId, closeOpen, fifAmt, fifBankId,
        fifOtherBank, fifAccountNo, fifAccountName, isValidFif, bookingAmt, final_att_buktiretur,
        bookingId, txnFifUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_KASIR_FIF_REVISION_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Submit Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_FIF_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnFifUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_FIF_RETURN_EDIT(
        txnProcTaskId, comment, appBookingId, txnFifUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_FIF_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_FIF_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnFifUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_FIF_APPROVE_EDIT(
        txnProcTaskId, comment, appBookingId, txnFifUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_FIF_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_AOC_FIF_REJECT_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnFifUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_AOC_FIF_REJECT_EDIT(
        txnProcTaskId,  comment, appBookingId, txnFifUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_AOC_FIF_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }


  public void processTransTxn_WF_CAPTAIN_FIF_RETURN_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnFifUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_FIF_RETURN_EDIT(
        txnProcTaskId, comment, appBookingId, txnFifUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_FIF_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_FIF_APPROVE_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnFifUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_FIF_APPROVE_EDIT(
        txnProcTaskId, comment, appBookingId, txnFifUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_FIF_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_CAPTAIN_FIF_REJECT_EDIT(UUID txnProcTaskId, String comment,
      UUID appBookingId, Date txnFifUpdateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_CAPTAIN_FIF_REJECT_EDIT(
        txnProcTaskId,  comment, appBookingId, txnFifUpdateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_CAPTAIN_FIF_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_FIF_APPROVE_EDIT(UUID txnConsProcTaskId, String comment,
      String OTP, Boolean isManual) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<OtpProcessResponse> process = service.processTransTxn_WF_FINSPV_FIF_APPROVE_EDIT(
        txnConsProcTaskId, comment, OTP, isManual);
    Futures.addCallback(process, new FutureCallback<OtpProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl OtpProcessResponse res) {
        if (res.getOtpVerifySuccess()) {
          HyperLog.d(TAG, "processTransTxn_WF_FINSPV_FIF_APPROVE_EDIT berhasil");
          notifySuccess(doneMsg, res.getOtpVerifySuccess());
        } else {
          HyperLog.exception(TAG,
              "processTransTxn_WF_FINSPV_FIF_APPROVE_EDIT gagal, verifyOtp gagal"
                  + res.getMessage());
          notifySuccess(res.getMessage(), res.getOtpVerifySuccess());
        }
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_FIF_RETURN_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_FIF_RETURN_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_FIF_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINSPV_FIF_REJECT_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINSPV_FIF_REJECT_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINSPV_FIF_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_AOC_APPROVE_CANCEL(UUID procTaskId, String comment, UUID appBookingId,
      Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_AOC_APPROVE_CANCEL(procTaskId, comment,
        appBookingId, updateTs );
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_AOC_APPROVE_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_AOC_RETURN_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_AOC_RETURN_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_AOC_RETURN_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_AOC_REJECT_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_AOC_REJECT_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_AOC_REJECT_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_CAPTAIN_APPROVE_CANCEL(UUID procTaskId, String comment, UUID appBookingId,
      Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_CAPTAIN_APPROVE_CANCEL(procTaskId, comment,
        appBookingId, updateTs );
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_AOC_APPROVE_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_CAPTAIN_RETURN_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_CAPTAIN_RETURN_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_CAPTAIN_RETURN_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_CAPTAIN_REJECT_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_CAPTAIN_REJECT_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_CAPTAIN_REJECT_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_DIRECTOR_APPROVE_CANCEL(UUID procTaskId, String comment, UUID appBookingId,
      Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_DIRECTOR_APPROVE_CANCEL(procTaskId, comment,
        appBookingId, updateTs );
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_AOC_APPROVE_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_DIRECTOR_RETURN_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_DIRECTOR_RETURN_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_DIRECTOR_RETURN_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_DIRECTOR_REJECT_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_DIRECTOR_REJECT_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_CAPTAIN_DIRECTOR_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_FINRETUR_APPROVE_CANCEL(UUID procTaskId, String comment, UUID appBookingId,
      Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_FINRETUR_APPROVE_CANCEL(procTaskId, comment,
        appBookingId, updateTs );
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_FINRETUR_APPROVE_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_FINRETUR_RETURN_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_FINRETUR_RETURN_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_FINRETUR_RETURN_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransWF_FINRETUR_REJECT_CANCEL(UUID procTaskId, String comment, UUID appBookingId, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransWF_FINRETUR_REJECT_CANCEL(procTaskId, comment, appBookingId,
        updateTs);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransWF_FINRETUR_REJECT_CANCEL berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Return Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINRETUR_FIF_APPROVE_EDIT(UUID txnConsProcTaskId, String comment) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINRETUR_FIF_APPROVE_EDIT(txnConsProcTaskId, comment);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINRETUR_FIF_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINRETUR_FIF_RETURN_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINRETUR_FIF_RETURN_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINRETUR_FIF_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINRETUR_FIF_REJECT_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINRETUR_FIF_REJECT_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINRETUR_FIF_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINRETUR_BJ_APPROVE_EDIT(UUID txnConsProcTaskId, String comment) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINRETUR_BJ_APPROVE_EDIT(txnConsProcTaskId, comment);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINRETUR_BJ_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINRETUR_BJ_RETURN_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINRETUR_BJ_RETURN_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINRETUR_BJ_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINRETUR_BJ_REJECT_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINRETUR_BJ_REJECT_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINRETUR_BJ_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINRETUR_CUS_APPROVE_EDIT(UUID txnConsProcTaskId, String comment) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINRETUR_CUS_APPROVE_EDIT(txnConsProcTaskId, comment);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINRETUR_CUS_APPROVE_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINRETUR_CUS_RETURN_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINRETUR_CUS_RETURN_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINRETUR_CUS_RETURN_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processTransTxn_WF_FINRETUR_CUS_REJECT_EDIT(UUID txnConsProcTaskId, String comment, UUID txnId) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processTransTxn_WF_FINRETUR_CUS_REJECT_EDIT(txnConsProcTaskId, comment, txnId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processTransTxn_WF_FINRETUR_CUS_REJECT_EDIT berhasil");
        notifySuccess(doneMsg, res);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Pengajuan Booking Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processGenerateOTP_AppBookingTransferCustomer(UUID txnCusProcTaskId) {
//    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<Boolean> process = service.processGenerateOTP_AppBookingTransferCustomer(txnCusProcTaskId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processGenerateOTP_AppBookingTransferCustomer");
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Generate OTP Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processGenerateOTP_AppBookingTransferBiroJasa(UUID txnBjProcTaskId) {
    ListenableFuture<Boolean> process = service.processGenerateOTP_AppBookingTransferBiroJasa(txnBjProcTaskId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processGenerateOTP_AppBookingTransferBiroJasa");
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Generate OTP Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processGenerateOTP_AppBookingTransferFIF(UUID txnFifProcTaskId) {
    ListenableFuture<Boolean> process = service.processGenerateOTP_AppBookingTransferFIF(txnFifProcTaskId);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean res) {
        HyperLog.d(TAG, "processGenerateOTP_AppBookingTransferFIF");
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Generate OTP Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }
  //---------

  public interface Listener {
    void onAppBookingProcessStarted(Resource<Boolean> loading);

    void onAppBookingProcessSuccess(Resource<Boolean> response);

    void onAppBookingProcessFailure(Resource<Boolean> response);
  }

}
