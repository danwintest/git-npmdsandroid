package id.co.danwinciptaniaga.npmdsandroid.security;

import java.util.List;

import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.ExtUserInfoData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.common.GetSubstituteUsersUC;
import id.co.danwinciptaniaga.npmdsandroid.common.SubstituteUserUC;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;

public class SubstituteUserVM extends AndroidViewModel
    implements GetSubstituteUsersUC.Listener, SubstituteUserUC.Listener {

  private final GetSubstituteUsersUC ucGetSubstituteUsers;
  private final SubstituteUserUC ucSubstituteUser;

  private MutableLiveData<FormState> formState = new MutableLiveData<>(null);
  private MutableLiveData<Resource<ExtUserInfoData>> actionEvent = new SingleLiveEvent<>();

  private List<ExtUserInfoData> substituteUsers = null;

  @ViewModelInject
  public SubstituteUserVM(@NonNull Application application,
      GetSubstituteUsersUC getSubstituteUsersUC, SubstituteUserUC substituteUserUC) {
    super(application);
    this.ucGetSubstituteUsers = getSubstituteUsersUC;
    this.ucGetSubstituteUsers.registerListener(this);
    this.ucSubstituteUser = substituteUserUC;
    this.ucSubstituteUser.registerListener(this);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.ucGetSubstituteUsers.unregisterListener(this);
  }

  public LiveData<FormState> getFormState() {
    return formState;
  }

  public LiveData<Resource<ExtUserInfoData>> getActionEvent() {
    return actionEvent;
  }

  public void setActionEvent(
      MutableLiveData<Resource<ExtUserInfoData>> actionEvent) {
    this.actionEvent = actionEvent;
  }

  public void clearFormState() {
    this.substituteUsers = null;
    formState.setValue(null);
  }

  public void loadSubstituteUsers(boolean forceReload) {
    // skip loading kalau sudah ada isi atau bukan dipaksa clear
    if (forceReload || substituteUsers == null) {
      ucGetSubstituteUsers.getSubstituteUsers();
    }
  }

  public List<ExtUserInfoData> getSubstituteUsers() {
    return substituteUsers;
  }

  @Override
  public void onGetSubstituteUsersStarted() {
    formState.postValue(FormState.loading(getApplication().getString(R.string.memuat_data)));
  }

  @Override
  public void onGetSubstituteUsersSuccess(Resource<List<ExtUserInfoData>> result) {
    this.substituteUsers = result.getData();
    formState.postValue(FormState.ready(result.getMessage()));
  }

  @Override
  public void onSubstituteUsersFailure(Resource<List<ExtUserInfoData>> responseError) {
    String message = Resource.getMessageFromError(responseError,
        getApplication().getString(R.string.error_contact_support));
    formState.postValue(FormState.error(message));
  }

  public ListenableFuture<ExtUserInfoData> substituteUser(ExtUserInfoData userInfoData) {
    return ucSubstituteUser.substituteUser(userInfoData.getId());
  }

  @Override
  public void onSubstituteUserStarted() {
    formState.postValue(FormState.actionInProgress(null));
  }

  @Override
  public void onSubstituteUserSuccess(Resource<ExtUserInfoData> response) {
    formState.postValue(FormState.ready(response.getMessage()));
    // kirim SingleLiveEvent sukses
    actionEvent.postValue(Resource.Builder.success(response.getMessage(), response.getData()));
  }

  @Override
  public void onSubstituteUserFailure(Resource<ExtUserInfoData> response) {
    String message = Resource.getMessageFromError(response,
        getApplication().getString(R.string.error_contact_support));
    // form kembali ready
    formState.postValue(FormState.ready(message));
    // kirim SingleLiveEvent error
    actionEvent.postValue(Resource.Builder.error(message));
  }
}
