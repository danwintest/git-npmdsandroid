package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiDroppingDayTaskDetailBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class DroppingDailyTaskDetailListAdapter extends
    PagingDataAdapter<DroppingDailyTaskBrowseDetailData, DroppingDailyTaskDetailListAdapter.ViewHolder> {
  private final RecyclerViewAdapterListener mListener;
  private SparseBooleanArray selectedItems;
  private List<DroppingDailyTaskBrowseDetailData> selectedData = new ArrayList<>();
  private SparseBooleanArray animationItemsIndex;
  private Context mCtx;
  public AppExecutors appExecutors;


  @Inject
  public DroppingDailyTaskDetailListAdapter(RecyclerViewAdapterListener listener, Context ctx, AppExecutors appExecutors){
    super(new DiffCallBack());
    mListener = listener;
    selectedItems = new SparseBooleanArray();
    animationItemsIndex = new SparseBooleanArray();
    mCtx = ctx;
    this.appExecutors = appExecutors;
  }

  @Override
  public DroppingDailyTaskDetailListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.li_dropping_day_task_detail_browse, parent, false);
    return new DroppingDailyTaskDetailListAdapter.ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(final DroppingDailyTaskDetailListAdapter.ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
    holder.view.setActivated(selectedItems.get(position, false));
    holder.binding.getRoot().setActivated(selectedItems.get(position,false));
  }

  public class ViewHolder extends RecyclerView.ViewHolder{
    private final LiDroppingDayTaskDetailBrowseBinding binding;
    private final View view;

    public ViewHolder(View v){
      super(v);
      view = v;
      binding = LiDroppingDayTaskDetailBrowseBinding.bind(v);
    }

    public void bind(DroppingDailyTaskBrowseDetailData data, int pos) {
      setObject(data);
      setObjectListener(data, pos);
    }

    private void setObject(DroppingDailyTaskBrowseDetailData data){
      binding.tvTrxNo.setText(data.getTrxNo());
      binding.tvOutletAndCode.setText(data.getOutletName()+" - "+data.getOutletCode());
      binding.tvDroppingDateVal.setText(Formatter.SDF_dd_MM_yyyy.format(data.getRequestDate()));
      binding.tvPlafonVal.setText(data.getPlafonAmt().toString());
      binding.tvCurrentBalanceVal.setText(data.getCurrentBalanceAmt().toString());
      binding.tvBalanceVal.setText(Utility.getFormattedAmt(data.getBalanceAmt()));
      binding.tvDroppingAmtVal.setText(Utility.getFormattedAmt(data.getDroppingAmt()));

      binding.tvKeputusan.setText(data.getOutcome() != null ? data.getOutcome() : "");
      binding.tvAccInfo.setText(data.getAccName()+" - "+data.getAccNo());
    }

    private void setObjectListener(DroppingDailyTaskBrowseDetailData data, int pos){
      binding.clContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, data, pos);
        }
      });

      binding.clContainer.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
          mListener.onItemLongClickedListener(v, data, pos);
          return true;
        }
      });
    }

  }

  interface RecyclerViewAdapterListener {
    void onItemClicked(View view, DroppingDailyTaskBrowseDetailData data, int position);

    void onItemLongClickedListener(View v, DroppingDailyTaskBrowseDetailData data, int position);
  }

  private static class DiffCallBack
      extends DiffUtil.ItemCallback<DroppingDailyTaskBrowseDetailData> {

    @Override
    public boolean areItemsTheSame(@NonNull DroppingDailyTaskBrowseDetailData oldItem,
        @NonNull DroppingDailyTaskBrowseDetailData newItem) {
      return oldItem.getDroppingId() == newItem.getDroppingId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull DroppingDailyTaskBrowseDetailData oldItem,
        @NonNull DroppingDailyTaskBrowseDetailData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public void toggleSelection(int pos) {
    DroppingDailyTaskBrowseDetailData data = getItem(pos);
    if (selectedItems.get(pos, false)) {
      selectedItems.delete(pos);
      selectedData.remove(data);
      animationItemsIndex.delete(pos);
    } else {
      selectedData.add(data);
      selectedItems.put(pos, true);
    }
    notifyItemChanged(pos);
  }

  public int getSelectedItemCount(){
    return selectedItems.size();
  }

  public BigDecimal getSelectedTotalAmt(){
    BigDecimal totalAmt = BigDecimal.ZERO;
    for(DroppingDailyTaskBrowseDetailData data : selectedData){
      totalAmt = totalAmt.add(BigDecimal.valueOf(data.getDroppingAmt()));
    }
    return totalAmt;
  }

  public List<DroppingDailyTaskBrowseDetailData> getSelectedData(){
    return selectedData;
  }

    public void clearSelections() {
      // reverseAllAnimations = true;
      selectedItems.clear();
      selectedData.clear();
      notifyDataSetChanged();
    }

  public void resetAnimationIndex() {
    // reverseAllAnimations = false;
    animationItemsIndex.clear();
  }
}
