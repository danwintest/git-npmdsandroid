package id.co.danwinciptaniaga.npmdsandroid.prefs;

import java.util.Objects;

import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.androcon.utility.Utility;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentChangePasswordBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.AnimationUtil;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;

@AndroidEntryPoint
public class ChangePasswordFragment extends ProtectedFragment {
  private static final String TAG = ChangePasswordFragment.class.getSimpleName();

  private FragmentChangePasswordBinding binding;
  private ChangePasswordVM viewModel;

  @Inject
  LoginUtil loginUtil;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(requireActivity()).get(ChangePasswordVM.class);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentChangePasswordBinding.inflate(inflater, container, false);

    viewModel.getFormStateLd().observe(getViewLifecycleOwner(), formState -> {
      bindProgressAndPageContent(formState);
    });
    viewModel.getActionEvent().observe(getViewLifecycleOwner(), s -> {
      if (Status.SUCCESS.equals(s.getStatus())) {
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
        AccountManager am = AccountManager.get(getContext());
        Account a = loginUtil.getAccount(App.ACCOUNT_TYPE);
        if (a != null) {
          String newPassword = Utility.getTrimmedString(binding.tietNewPassword.getText());
          am.setPassword(a, newPassword);
          NavController navController = Navigation.findNavController(getView());
          navController.popBackStack();
        }
      } else {
        String message = s.getMessage();
        if (message == null)
          message = s.getData() != null ? s.getMessage() : "Terjadi kesalahan";
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
      }
    });

    binding.btnChangePassword.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        boolean currentPasswordOk = false;
        boolean newPasswordOk = false;
        boolean confirmPasswordOk = false;
        String currentPassword = Utility.getTrimmedString(binding.tietCurrentPassword.getText());
        if (currentPassword == null) {
          binding.tilCurrentPassword.setError(getString(R.string.validation_mandatory));
        } else {
          binding.tilCurrentPassword.setError(null);
          currentPasswordOk = true;
        }

        String newPassword = Utility.getTrimmedString(binding.tietNewPassword.getText());
        if (newPassword == null) {
          binding.tilNewPassword.setError(getString(R.string.validation_mandatory));
        } else {
          binding.tilNewPassword.setError(null);
          newPasswordOk = true;
        }
        String confirmPassword = Utility.getTrimmedString(binding.tietConfirmPassword.getText());
        if (!Objects.equals(newPassword, confirmPassword)) {
          binding.tilConfirmPassword.setError(
              getString(R.string.validation_confirm_pwd_must_match));
        } else {
          binding.tilConfirmPassword.setError(null);
          confirmPasswordOk = true;
        }

        if (currentPasswordOk && newPasswordOk && confirmPasswordOk) {
          viewModel.changePassword(currentPassword, newPassword, confirmPassword);
        } else {
          Toast.makeText(getContext(), R.string.form_validation_error,
              Toast.LENGTH_SHORT).show();
        }
      }
    });
    return binding.getRoot();
  }

  private void bindProgressAndPageContent(FormState formState) {
    if (formState == null) {
      return;
    }
    HyperLog.d(TAG, "bindProgressAndPageContent: " + formState.getState());
    Animation outAnimationShort = AnimationUtil.fadeoutAnimationShort();
    Animation inAnimationShort = AnimationUtil.fadeinAnimationShort();
    Animation outAnimationMedium = AnimationUtil.fadeoutAnimationMedium();
    Animation inAnimationMedium = AnimationUtil.fadeinAnimationMedium();
    switch (formState.getState()) {
    case READY:
      binding.btnChangePassword.setEnabled(true);
      ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE, outAnimationMedium);
      ViewUtil.setVisibility(binding.pageContent, View.VISIBLE, inAnimationMedium);
      break;
    case ACTION_IN_PROGRESS:
      if (formState.getMessage() != null) {
        binding.progressWrapper.progressText.setText(formState.getMessage());
      } else {
        binding.progressWrapper.progressText.setText(getString(R.string.msg_please_wait));
      }

      ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE, inAnimationShort);
      ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.VISIBLE, inAnimationShort);
      ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
      // tidak mengubah visibilitas pageContent
      break;
    case LOADING:
    default:
      if (formState.getMessage() != null) {
        binding.progressWrapper.progressText.setText(formState.getMessage());
      } else {
        binding.progressWrapper.progressText.setText(getString(R.string.msg_please_wait));
      }
      ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE, inAnimationMedium);
      ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.VISIBLE);
      ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
      ViewUtil.setVisibility(binding.pageContent, View.GONE, outAnimationMedium);
      break;
    }
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    if (status.getStatus() == Status.LOADING) {
      binding.tvStatus.setVisibility(View.VISIBLE);
      binding.tvStatus.setText(R.string.msg_please_wait);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.tvStatus.setText(status.getMessage());
      binding.tvStatus.setVisibility(View.GONE);
      viewModel.setReady();
    }
  }
}
