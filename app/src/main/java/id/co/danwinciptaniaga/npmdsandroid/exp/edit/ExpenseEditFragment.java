package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.bumptech.glide.Glide;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.ImageUtility;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.androcon.utility.Utility;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseEditHelper;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseItemData;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentExpenseEditBinding;
import id.co.danwinciptaniaga.npmdsandroid.databinding.WfdecisionBinding;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.util.AnimationUtil;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ExpenseEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
public class ExpenseEditFragment extends ProtectedFragment {
  private static final String TAG = ExpenseEditFragment.class.getSimpleName();

  public static final String ARG_EXPENSE_ID = "expenseId"; // namanya sama dengan definisi di navigation

  public static final String SAVED_STATE_LD_BOOKING_DATA = "bookingData";

  private final int PERMISSION_REQUEST_CODE = 201;

  private FragmentExpenseEditBinding binding;
  private ExpenseEditViewModel viewModel;
  private ArrayAdapter<KeyValueModel<OutletShortData, String>> outletsAdapter;
  private ArrayAdapter<KeyValueModel<ExpenseItemData, String>> expenseItemsAdapter;

  private View.OnClickListener restart = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // kita coba restart seluruh activity
      Bundle bundle = new Bundle();
      bundle.putSerializable(ARG_EXPENSE_ID, viewModel.getExpenseId());
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_expense_edit)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragment = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      onReady();
    }
  };

  public ExpenseEditFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param expenseId kalau disebutkan, Fragment akan menampilkan detil Expense-nya.
   *                  Kalau null, maka form untuk pembuatan baru.
   * @return A new instance of fragment ExpenseEditFragment.
   */
  public static ExpenseEditFragment newInstance(UUID expenseId) {
    // sebetulnya saat ini tidak diketahui kapan method ini dipakai
    ExpenseEditFragment fragment = new ExpenseEditFragment();
    Bundle args = new Bundle();
    args.putSerializable(ARG_EXPENSE_ID, expenseId);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    HyperLog.v(TAG, "onCreate");
    super.onCreate(savedInstanceState);

    // view model disiapkan di onCreate (terpanggil sekali kalau fragment dibuat)
    viewModel = new ViewModelProvider(this).get(ExpenseEditViewModel.class);
    ExpenseEditFragmentArgs args = ExpenseEditFragmentArgs.fromBundle(getArguments());
    viewModel.setExpenseId(args.getExpenseId());
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    HyperLog.v(TAG, "onCreateView");
    // Inflate the layout for this fragment
    binding = FragmentExpenseEditBinding.inflate(inflater, container, false);

    setupForm();

    return binding.getRoot();
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    if (status.getStatus() == Status.LOADING) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      binding.pageContent.setVisibility(View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.GONE);
      binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);
      // retry = restart fragment (karena authentication gagal)
      binding.progressWrapper.retryButton.setOnClickListener(restart);
    }
  }

  private void onReady() {
    // tahap ini dilakukan 1 kali saja
    if (viewModel.getExpenseId() != null) {
      viewModel.loadExpense(false);
    } else {
      viewModel.prepareNewExpense(false);
    }
  }

  private void setupForm() {
    // observer "besar"
    viewModel.getFormState().observe(getViewLifecycleOwner(), formState -> {
      bindProgressAndPageContent(formState);
    });
    viewModel.getActionEvent().observe(getViewLifecycleOwner(), s -> {
      if (Status.SUCCESS.equals(s.getStatus())) {
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
        NavController navController = Navigation.findNavController(binding.getRoot());
        navController.getPreviousBackStackEntry().getSavedStateHandle()
            .set(ExpenseBrowseFragment.SAVED_STATE_LD_REFRESH, true);
        navController.popBackStack();
      } else {
        String message = s.getMessage();
        if (message == null)
          message = s.getData() != null ? s.getMessage() : "Terjadi kesalahan";
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
      }
    });

    viewModel.getCanProcess().observe(getViewLifecycleOwner(), canProcess -> {
      if (viewModel.getEditPageMode() != null) {
        switch (viewModel.getEditPageMode()) {
        case KASIR_DRAFT:
          if (canProcess != null && canProcess) {
            binding.btnSaveDraft.setEnabled(true);
            binding.btnSubmit.setEnabled(true);
          } else {
            binding.btnSaveDraft.setEnabled(false);
            binding.btnSubmit.setEnabled(false);
          }
          break;
        case WF_KASIR_PENDING_REVISION:
        case WF_AOC_PENDING_VERIFICATION:
          int buttonCount = binding.llWorkflowButtonContainer.getChildCount();
          boolean enableButton = false;
          if (canProcess != null && canProcess) {
            enableButton = true;
          }
          HyperLog.d(TAG, "Enabling WF buttons: " + enableButton);
          for (int z = 0; z < buttonCount; z++) {
            View view = binding.llWorkflowButtonContainer.getChildAt(z);
            view.setEnabled(enableButton);
          }
          break;
        }
      }
    });

    // setup yg tidak tergantung data
    setupHeaderOutletName();
    setupHeaderCompanyName();
    setupHeaderStatus();
    setupOutletSpinner();
    setupStatementDate();
    setupExpenseDate();
    setupExpenseItemSpinner();
    setupAmount();
    setupQuantity();
    setupBooking();
    setupDescription();
    setupAttachment();
    setupButtons();
  }

  private void bindProgressAndPageContent(ExpenseFormState formState) {
    if (formState == null) {
      return;
    }
    Animation outAnimationShort = AnimationUtil.fadeoutAnimationShort();
    Animation inAnimationShort = AnimationUtil.fadeinAnimationShort();
    Animation outAnimationMedium = AnimationUtil.fadeoutAnimationMedium();
    Animation inAnimationMedium = AnimationUtil.fadeinAnimationMedium();

    switch (formState.getState()) {
    case READY:
      if (viewModel.getExpenseDetailResponse() != null) {
        if (viewModel.getExpenseDetailResponse().getExpenseData().getTransactionNo() != null) {
          ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
          actionBar.setTitle(
              viewModel.getExpenseDetailResponse().getExpenseData().getTransactionNo());
        }
        binding.pageHeader.setVisibility(View.VISIBLE);
        binding.pageHeaderSeparator.setVisibility(View.VISIBLE);
      } else {
        binding.pageHeader.setVisibility(View.GONE);
        binding.pageHeaderSeparator.setVisibility(View.GONE);
      }
      if (ExpenseEditHelper.Mode.KASIR_DRAFT.equals(viewModel.getEditPageMode())) {
        // kasir input data
        // semua field editable normal, kecuali statementDate tergantung allowBackDate
        if (!viewModel.isAllowBackdate()) {
          binding.etStatementDate.setEnabled(false);
        }
        // tidak ada action keputusan
        // ada action standar
      } else if (ExpenseEditHelper.Mode.WF_KASIR_PENDING_REVISION
          .equals(viewModel.getEditPageMode())) {
        // workflow sudah mulai, kasir bisa ubah sebagian field
        if (!viewModel.isAllowBackdate()) {
          binding.etStatementDate.setEnabled(false);
        }
        // ada action keputusan
        // tidak ada action standar
        prepareWorkflowButtons();

        binding.btnWfHistory.setVisibility(View.VISIBLE);
        binding.btnSaveDraft.setVisibility(View.GONE);
        binding.btnSubmit.setVisibility(View.GONE);
      } else if (ExpenseEditHelper.Mode.WF_AOC_PENDING_VERIFICATION
          .equals(viewModel.getEditPageMode())) {
        // semua field readonly
        // tapi ada action keputusan
        // tidak ada action standar
        binding.spOutlet.setEnabled(false);
        binding.etStatementDate.setEnabled(false);
        binding.etExpenseDate.setEnabled(false);
        HyperLog.d(TAG, "Disabling spExpenseItem");
        binding.spExpenseItem.setEnabled(false);
        binding.etAmount.setEnabled(false);
        binding.etQuantity.setEnabled(false);
        binding.etBooking.setEnabled(false);
        binding.etDescription.setEnabled(false);
        prepareWorkflowButtons();

        binding.btnWfHistory.setVisibility(View.VISIBLE);
        binding.btnSaveDraft.setVisibility(View.GONE);
        binding.btnSubmit.setVisibility(View.GONE);
      } else if (ExpenseEditHelper.Mode.VIEW.equals(viewModel.getEditPageMode())) {
        // semua field readonly
        // tidak ada action keputusan
        // tidak ada action standar
        binding.spOutlet.setEnabled(false);
        binding.etStatementDate.setEnabled(false);
        binding.etExpenseDate.setEnabled(false);
        HyperLog.d(TAG, "Disabling spExpenseItem");
        binding.spExpenseItem.setEnabled(false);
        binding.etAmount.setEnabled(false);
        binding.etQuantity.setEnabled(false);
        binding.etBooking.setEnabled(false);
        binding.etDescription.setEnabled(false);

        binding.btnWfHistory.setVisibility(View.VISIBLE);
        binding.btnSaveDraft.setVisibility(View.GONE);
        binding.btnSubmit.setVisibility(View.GONE);
      }
      binding.progressWrapper.progressView.setAnimation(outAnimationMedium);
      binding.progressWrapper.progressView.setVisibility(View.GONE);
      binding.pageContent.setAnimation(inAnimationMedium);
      binding.pageContent.setVisibility(View.VISIBLE);
      break;
    case ACTION_IN_PROGRESS:
      if (formState.getMessage() != null) {
        binding.progressWrapper.progressText.setText(formState.getMessage());
      }
      binding.progressWrapper.progressView.setAnimation(inAnimationShort);
      binding.progressWrapper.progressView.setVisibility(View.VISIBLE);
      // tidak mengubah visibilitas pageContent
      break;
    case LOADING:
    default:
      if (formState.getMessage() != null) {
        binding.progressWrapper.progressText.setText(formState.getMessage());
      }
      binding.progressWrapper.progressView.setAnimation(inAnimationShort);
      binding.progressWrapper.progressView.setVisibility(View.VISIBLE);
      binding.pageContent.setAnimation(outAnimationShort);
      binding.pageContent.setVisibility(View.GONE);
      break;
    }
  }

  private void prepareWorkflowButtons() {
    binding.svWorkflowAction.setVisibility(View.VISIBLE);
    binding.llWorkflowButtonContainer.removeAllViews();
    if (viewModel.getExpenseDetailResponse().getProcTaskData() == null)
      return;
    List<String> possibleOutcomes = viewModel.getExpenseDetailResponse().getProcTaskData().getPossibleOutcome();
    HyperLog.d(TAG, "Preparing WF buttons for " + possibleOutcomes);
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    int margin = (int) getActivity().getResources().getDimension(R.dimen.activity_vertical_margin);
    params.setMargins(0, 0, margin, 0);
    for (String outcome : possibleOutcomes) {
      Button button = (Button) getLayoutInflater().inflate(R.layout.outlined_button, null);
      button.setText(outcome); // label bisa saja dibedakan dengan "key"
      button.setTag(outcome); // tag untuk menyimpan "key"
      Boolean canProcess = viewModel.getCanProcess().getValue();
      button.setEnabled(canProcess != null && canProcess);
      button.setOnClickListener(this::handleWfAction);
      button.setLayoutParams(params);
      binding.llWorkflowButtonContainer.addView(button);
    }
  }

  private void setupHeaderStatus() {
    viewModel.getHeaderStatusLd().observe(getViewLifecycleOwner(), data -> {
      binding.tvStatus.setText(data);
    });
  }

  private void setupHeaderCompanyName() {
    viewModel.getHeaderCompanyNameLd().observe(getViewLifecycleOwner(), data -> {
      binding.tvCompanyName.setText(data);
    });
  }

  private void setupHeaderOutletName() {
    viewModel.getHeaderOutletNameLd().observe(getViewLifecycleOwner(), data -> {
      binding.tvOutlet.setText(data);
    });
  }

  private void setupOutletSpinner() {
    outletsAdapter = new ArrayAdapter(getContext(),
        android.R.layout.select_dialog_item);
    binding.spOutlet.setAdapter(outletsAdapter);
    binding.spOutlet.setFocusable(false);
    binding.spOutlet.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
          int i, long l) {
        KeyValueModel<OutletShortData, String> outletKvm = outletsAdapter.getItem(i);
        // todo sebetulnya cukup index-nya saja?
        viewModel.setOutlet(outletKvm);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
        viewModel.setOutlet(null);
      }
    });

    viewModel.getOutletsLd().observe(getViewLifecycleOwner(), osdList -> {
      if (osdList == null) {
        binding.spOutlet.setEnabled(false);
      } else {
        List<KeyValueModel<OutletShortData, String>> kvmList = osdList.stream().map(
            osd -> new KeyValueModel<OutletShortData, String>(osd, osd.getOutletName()))
            .collect(Collectors.toList());

        outletsAdapter.addAll(kvmList);
        outletsAdapter.notifyDataSetChanged();

        if (osdList.size() > 1) {
          // hanya muncul kalau ada pilihan
          binding.spOutlet.setVisibility(View.VISIBLE);
          viewModel.getOutlet().observe(getViewLifecycleOwner(), data -> {
            if (data != null && outletsAdapter.getCount() > 0) {
              int pos = outletsAdapter.getPosition(data);
              binding.spOutlet.setSelection(pos);
            }
          });
        } else {
          binding.spOutlet.setEnabled(false);
          binding.spOutlet.setVisibility(View.GONE);
          binding.spOutlet.setSelection(0);
          KeyValueModel<OutletShortData, String> outletKvm = outletsAdapter.getItem(0);
          viewModel.setOutlet(outletKvm);
        }
      }
    });
  }

  private void setupExpenseItemSpinner() {
    binding.spExpenseItem.setFocusable(false);

    binding.spExpenseItem.setOnItemSelectedListener(
        new MaterialSpinner.OnItemSelectedListener() {
          @Override
          public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
              @Nullable View view,
              int i, long l) {
            KeyValueModel<ExpenseItemData, String> expenseItemKvm = expenseItemsAdapter.getItem(
                i);
            // todo sebetulnya cukup index-nya saja?
            viewModel.setExpenseItem(expenseItemKvm);
          }

          @Override
          public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
            viewModel.setExpenseItem(null);
          }
        });

    viewModel.getExpenseItems().observe(getViewLifecycleOwner(), eidList -> {
      HyperLog.d(TAG, "getExpenseItemsLd triggered: " + eidList);
      if (eidList == null || eidList.size() == 0) {
        binding.spExpenseItem.setEnabled(false);
      } else {
        binding.spExpenseItem.setEnabled(true);
        expenseItemsAdapter = new ArrayAdapter(getContext(),
            android.R.layout.select_dialog_item);
        binding.spExpenseItem.setAdapter(expenseItemsAdapter);

        expenseItemsAdapter.addAll(eidList);
        expenseItemsAdapter.notifyDataSetChanged();
        // quickfix: kalau sudah ada data ExpenseItemId, langsung set saja
        //        KeyValueModel<ExpenseItemData, String> kvmEid = viewModel.getExpenseItemLd().getValue();
        //        if (kvmEid != null) {
        //          int pos = expenseItemsAdapter.getPosition(kvmEid);
        //          binding.spExpenseItem.setSelection(pos);
        //        }

        viewModel.getExpenseItemLd().observe(getViewLifecycleOwner(), kvmEid -> {
          if (kvmEid != null && expenseItemsAdapter.getCount() > 0) {
            int pos = expenseItemsAdapter.getPosition(kvmEid);
            binding.spExpenseItem.setSelection(pos);
            switch (viewModel.getEditPageMode()) {
            case VIEW:
            case WF_AOC_PENDING_VERIFICATION:
              binding.spExpenseItem.setEnabled(false);
              break;
            }
          }
        });
      }
    });
  }

  private void setupStatementDate() {
    binding.etStatementDate.setOnClickListener(v -> {
      LocalDate statementDate = viewModel.getStatementDateLd() != null ?
          viewModel.getStatementDateLd().getValue() :
          LocalDate.now();
      int day = statementDate.getDayOfMonth();
      int month = statementDate.getMonthValue() - 1;
      int year = statementDate.getYear();
      DatePickerDialog dpdStatementDate = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate statementDate = LocalDate.of(year, month + 1, dayOfMonth);
              viewModel.setStatementDate(statementDate);
            }
          }, year, month, day);
      dpdStatementDate.show();
    });

    viewModel.getStatementDateLd().observe(getViewLifecycleOwner(), sdLd -> {
      binding.etStatementDate.setText(sdLd.format(Formatter.DTF_dd_MM_yyyy));
      binding.tvStatementDate.setText(sdLd.format(Formatter.DTF_dd_MM_yyyy));
    });
    // error tetap harus ditampilkan baik etStatementDate read-only atau tidak
    viewModel.getStatementDateErrorLd().observe(getViewLifecycleOwner(), errorMsg -> {
      binding.tilStatementDate.setError(errorMsg);
    });
  }

  private void setupExpenseDate() {
    binding.etExpenseDate.setOnClickListener(v -> {
      LocalDate expenseDate = viewModel.getExpenseDateLd() != null ?
          viewModel.getExpenseDateLd().getValue() :
          LocalDate.now();
      int day = expenseDate.getDayOfMonth();
      int month = expenseDate.getMonthValue() - 1;
      int year = expenseDate.getYear();
      DatePickerDialog dpdExpenseDate = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate statementDate = LocalDate.of(year, month + 1, dayOfMonth);
              viewModel.setExpenseDate(statementDate);
            }
          }, year, month, day);
      dpdExpenseDate.show();
    });

    viewModel.getExpenseDateLd().observe(getViewLifecycleOwner(), edLd -> {
      binding.etExpenseDate.setText(edLd.format(Formatter.DTF_dd_MM_yyyy));
    });
    viewModel.getExpenseDateErrorLd().observe(getViewLifecycleOwner(), errorMsg -> {
      binding.tilExpenseDate.setError(errorMsg);
    });
  }

  private void setupAmount() {
    // TODO: pengecekan setiap karakter berubah membuat UI lambat
    binding.etAmount.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          Integer amt = id.co.danwinciptaniaga.npmdsandroid.util.Utility.getAmtFromFromattedString(s.toString()).intValue();
          viewModel.setAmount(amt);
        } else {
          viewModel.setAmount(null);
        }
      }
    });

    viewModel.getAmountLd().observe(getViewLifecycleOwner(), integer -> {
      if (integer == null) {
        binding.etAmount.setText(null);
      } else if (!integer.toString().equals(binding.etAmount.getText().toString())) {
        if(binding.etAmount.isEnabled()) {
          // hanya set kalau berbeda, untuk mencegah infinite loop
          binding.etAmount.setText(integer.toString());
        }else{
          String amt = id.co.danwinciptaniaga.npmdsandroid.util.Utility.getFormattedAmt(
              new BigDecimal(integer));
          binding.etAmount.setText(amt);
        }
      }
    });
    viewModel.getAmountErrorLd().observe(getViewLifecycleOwner(), s -> {
      binding.tilAmount.setError(s);
    });
  }

  private void setupQuantity() {
    binding.etQuantity.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          viewModel.setQuantity(Integer.parseInt(s.toString()));
        } else {
          viewModel.setQuantity(null);
        }
      }
    });

    viewModel.getShowQuantityLd().observe(getViewLifecycleOwner(), show -> {
      binding.tilQuantity.setVisibility(show ? View.VISIBLE : View.GONE);
    });

    viewModel.getQuantityLd().observe(getViewLifecycleOwner(), integer -> {
      if (integer == null) {
        binding.etQuantity.setText(null);
      } else if (!integer.toString().equals(binding.etQuantity.getText().toString())) {
        // hanya set kalau berbeda, untuk mencegah infinite loop
        binding.etQuantity.setText(integer.toString());
      }
    });
    viewModel.getQuantityErrorLd().observe(getViewLifecycleOwner(), s -> {
      binding.tilQuantity.setError(s);
    });
  }

  private void setupBooking() {
    binding.etBooking.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int outletSelection = binding.spOutlet.getSelection();
        OutletShortData osd = viewModel.getOutletsLd().getValue().get(outletSelection);

        // workaround supaya sewaktu kembali dari SelectBooking, ExpenseItem terpilih kembali
        binding.spExpenseItem.setSelection(-1);

        NavController navController = Navigation.findNavController(binding.getRoot());
        ExpenseEditFragmentDirections.ActionExpenseEditSelectBooking actionSelectBooking =
            ExpenseEditFragmentDirections.actionExpenseEditSelectBooking(osd.getOutletId());
        navController.navigate(actionSelectBooking, NavHelper.animParentToChild().build());
      }
    });

    viewModel.getShowBookingLd().observe(getViewLifecycleOwner(), show -> {
      binding.tilBooking.setVisibility(show ? View.VISIBLE : View.GONE);
    });

    viewModel.getBookingDataLd().observe(getViewLifecycleOwner(), bd -> {
      if (bd == null) {
        binding.etBooking.setText(null);
      } else {
        binding.etBooking.setText(
            String.format("%s - %s - %s", bd.getConsumerNo(), bd.getConsumerName(),
                bd.getVehicleNo()));
      }
    });

    // siapkan listener untuk bookingData yang dipilih dari SelectBookingFragment
    NavController navController = NavHostFragment.findNavController(this);
    MutableLiveData<BookingData> selectedBookingData = navController
        .getCurrentBackStackEntry()
        .getSavedStateHandle()
        .getLiveData(SAVED_STATE_LD_BOOKING_DATA);
    selectedBookingData.observe(getViewLifecycleOwner(), bookingData -> {
      HyperLog.d(TAG, "selectedBookingData triggered: " + bookingData);
      viewModel.setBookingData(bookingData);
      // hapus setelah selesai baca
      navController.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(SAVED_STATE_LD_BOOKING_DATA);
    });

    viewModel.getBookingErrorLd().observe(getViewLifecycleOwner(), s -> {
      binding.tilBooking.setError(s);
    });
  }

  private void setupDescription() {
    viewModel.getDescriptionLd().observe(getViewLifecycleOwner(), s -> {
      if (s == null) {
        binding.etDescription.setText(null);
      } else if (!s.equals(binding.etDescription.getText().toString())) {
        // hanya set kalau berbeda, untuk mencegah infinite loop
        binding.etDescription.setText(s);
      }
    });
    // TODO: apakah ada validasi untuk description?
    viewModel.getDescriptionErrorLd().observe(getViewLifecycleOwner(), s -> {
      binding.etDescription.setError(s);
    });
  }

  private void setupAttachment() {
    viewModel.isArchived().observe(getViewLifecycleOwner(), isArchived -> {
      HyperLog.d(TAG, "setupAttachment Image [" + isArchived + "]");
      if (true == isArchived) {
        binding.ivAttachment.setImageResource(R.drawable.ic_baseline_archive_24);
      }
    });

    binding.ivAttachment.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
//        if (viewModel.isArchived().getValue())
//          return;

        ExpenseEditHelper.Mode pageMode = viewModel.getEditPageMode();
        boolean canChangeAttachment =
            ExpenseEditHelper.Mode.KASIR_DRAFT.equals(pageMode)
                || ExpenseEditHelper.Mode.WF_KASIR_PENDING_REVISION.equals(pageMode);
        if (canChangeAttachment && viewModel.getFinalAttachment() == null) {
          // hanya bisa ubah attachment dalam mode DRAFT / REVISI
          String[] options = getResources().getStringArray(R.array.attachment_add_actions);

          AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
          builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int item) {
              if (item == 0) {
                Intent takePictureIntent = new Intent(
                    android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                  // Buat file menunjuk lokasi penyimpanan hasil dari kamera
                  File photoFile = null;
                  try {
                    photoFile = ImageUtility.createTempFile(getContext().getCacheDir(), "temp",
                        "tmpexp-", ImageUtility.EXTENSION_JPG);
                  } catch (IOException ex) {
                    // Error occurred while creating the File
                    Toast.makeText(getContext(), R.string.error_starting_camera,
                        Toast.LENGTH_SHORT).show();
                  }
                  // Lanjutkan kalau pembuatan temp file berhasil
                  if (photoFile != null) {
                    // ini hanya "pointer" ke file-nya. File baru akan "terisi" apabila Intent berhasil
                    viewModel.setTempAttachmentFile(photoFile);
                    Uri photoURI = FileProvider.getUriForFile(getContext(),
                        getContext().getApplicationContext().getPackageName() + ".provider",
                        photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent,
                        id.co.danwinciptaniaga.npmdsandroid.util.Utility.REQUEST_TAKE_PHOTO);
                  }
                }

              } else if (item == 1) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto,
                    id.co.danwinciptaniaga.npmdsandroid.util.Utility.REQUEST_PICK_PHOTO);
              } else {
                dialog.dismiss();
              }
            }
          });
          builder.show();
        } else if (viewModel.getFinalAttachment() != null) {
          // kalau sudah ada image, maka tampilkan
          Uri attachmentUri = FileProvider.getUriForFile(getContext(),
              getContext().getApplicationContext().getPackageName() + ".provider",
              viewModel.getFinalAttachment());
          HyperLog.d(TAG, "Action View: " + attachmentUri);
          Intent viewImage = new Intent(Intent.ACTION_VIEW, attachmentUri);
          viewImage.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT
              | Intent.FLAG_GRANT_READ_URI_PERMISSION);
          startActivity(viewImage);
        }
      }
    });

    binding.ivAttachment.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
//        if (viewModel.isArchived().getValue())
//          return true;

        ExpenseEditHelper.Mode pageMode = viewModel.getEditPageMode();
        boolean canChangeAttachment =
            ExpenseEditHelper.Mode.KASIR_DRAFT.equals(pageMode)
                || ExpenseEditHelper.Mode.WF_KASIR_PENDING_REVISION.equals(pageMode);
        if (canChangeAttachment) {
          // hanya bisa ubah attachment dalam mode DRAFT / REVISI
          viewModel.setAttachmentFilled(false, true);
        }
        return true;
      }
    });
    viewModel.getReducedAttachmentLd().observe(getViewLifecycleOwner(), bitmap -> {
      if (bitmap != null) {
        if (Status.SUCCESS.equals(bitmap.getStatus())) {
          Glide.with(getContext())
              .load(bitmap.getData())
              .centerCrop()
              .into(binding.ivAttachment);
        } else {
          this.binding.ivAttachment.setImageResource(R.drawable.ic_baseline_broken_image_24);
        }
      } else {
        this.binding.ivAttachment.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
      }
    });
  }

  private void setupButtons() {
    binding.btnSaveDraft.setOnClickListener(v -> {
      int outletSelection = binding.spOutlet.getSelection();
      OutletShortData osd = viewModel.getOutletsLd().getValue().get(outletSelection);

      LocalDate statementDate = LocalDate.parse(binding.etStatementDate.getText(),
          Formatter.DTF_dd_MM_yyyy);
      LocalDate expenseDate = LocalDate.parse(binding.etExpenseDate.getText(),
          Formatter.DTF_dd_MM_yyyy);

      int expenseItemSelection = binding.spExpenseItem.getSelection();
      ExpenseItemData eid = viewModel.getExpenseItems().getValue().get(
          expenseItemSelection).getKey();

      BigDecimal amount = new BigDecimal(binding.etAmount.getText().toString());
      Integer quantity = null;
      if (eid.isStock()) {
        quantity = Integer.parseInt(binding.etQuantity.getText().toString());
      }

      viewModel.saveDraft(osd.getOutletId(), statementDate, expenseDate, eid, amount, quantity,
          Utility.getTrimmedString(binding.etDescription.getText()));
    });

    binding.btnSubmit.setOnClickListener(v -> {
      int outletSelection = binding.spOutlet.getSelection();
      OutletShortData osd = viewModel.getOutletsLd().getValue().get(outletSelection);

      LocalDate statementDate = LocalDate.parse(binding.etStatementDate.getText(),
          Formatter.DTF_dd_MM_yyyy);
      LocalDate expenseDate = LocalDate.parse(binding.etExpenseDate.getText(),
          Formatter.DTF_dd_MM_yyyy);

      int expenseItemSelection = binding.spExpenseItem.getSelection();
      ExpenseItemData eid = viewModel.getExpenseItems().getValue()
          .get(expenseItemSelection).getKey();

      BigDecimal amount = new BigDecimal(binding.etAmount.getText().toString());
      Integer quantity = null;
      if (eid.isStock()) {
        quantity = Integer.parseInt(binding.etQuantity.getText().toString());
      }

      viewModel.submit(osd.getOutletId(), statementDate, expenseDate, eid, amount, quantity,
          Utility.getTrimmedString(binding.etDescription.getText()));
    });

    binding.btnWfHistory.setOnClickListener(v -> {
      NavController navController = Navigation.findNavController(binding.getRoot());
      ExpenseEditFragmentDirections.ActionExpenseWfHistory dir = ExpenseEditFragmentDirections.actionExpenseWfHistory(
          WfType.APP_EXPENSE, viewModel.getExpenseId());
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });
  }

  private void handleWfAction(View view) {
    String decision = (String) view.getTag();
    WfdecisionBinding wfdecisionBinding = WfdecisionBinding.inflate(getLayoutInflater());
    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    builder.setTitle(decision)
        .setView(wfdecisionBinding.getRoot())
        .setNegativeButton(getString(R.string.workflow_cancel),
            new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
              }
            });
    if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision)) {
      builder.setPositiveButton(getString(R.string.workflow_yes),
          new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              int outletSelection = binding.spOutlet.getSelection();
              OutletShortData osd = viewModel.getOutletsLd().getValue().get(outletSelection);

              LocalDate statementDate = LocalDate.parse(binding.etStatementDate.getText(),
                  Formatter.DTF_dd_MM_yyyy);
              LocalDate expenseDate = LocalDate.parse(binding.etExpenseDate.getText(),
                  Formatter.DTF_dd_MM_yyyy);

              int expenseItemSelection = binding.spExpenseItem.getSelection();
              ExpenseItemData eid = viewModel.getExpenseItems().getValue()
                  .get(expenseItemSelection).getKey();

              BigDecimal amount = new BigDecimal(binding.etAmount.getText().toString());
              Integer quantity = null;
              if (eid.isStock()) {
                quantity = Integer.parseInt(binding.etQuantity.getText().toString());
              }
              viewModel.submitRevision(
                  Utility.getTrimmedString(wfdecisionBinding.tietComment.getText()),
                  osd.getOutletId(), statementDate, expenseDate, eid, amount, quantity,
                  Utility.getTrimmedString(binding.etDescription.getText()));
            }
          });
      builder.show();
    } else if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)) {
      // komentar opsional
      builder.setPositiveButton(getString(R.string.workflow_yes),
          new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              viewModel.aocDecision(decision,
                  Utility.getTrimmedString(wfdecisionBinding.tietComment.getText()));
            }
          });
      builder.show();
    } else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision)
        || WorkflowConstants.WF_OUTCOME_RETURN.equals(decision)) {
      // komentar wajib
      builder.setPositiveButton(getString(R.string.workflow_yes), null);
      AlertDialog dialog = builder.show();
      // listener di set di button seperti ini supaya dialog bisa tetap terbuka kalau validasi gagal
      dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          String comment = Utility.getTrimmedString(wfdecisionBinding.tietComment.getText());
          if (TextUtils.isEmpty(comment)) {
            wfdecisionBinding.tilComment.setError(getText(R.string.validation_mandatory));
          } else {
            wfdecisionBinding.tilComment.setError(null);
            viewModel.aocDecision(decision, comment);
            dialog.dismiss();
          }
        }
      });
    } else {
      Toast.makeText(getContext(), String.format("Decision %s not supported", decision),
          Toast.LENGTH_LONG).show();
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
      requestPermission();
  }

  private void requestPermission() {
    int resultReadExternalStorage = ContextCompat.checkSelfPermission(getContext(),
        Manifest.permission.READ_EXTERNAL_STORAGE);
    int resultWriteExternalStorage = ContextCompat.checkSelfPermission(getContext(),
        Manifest.permission.WRITE_EXTERNAL_STORAGE);

    if (resultReadExternalStorage != PackageManager.PERMISSION_GRANTED
        || resultWriteExternalStorage != PackageManager.PERMISSION_GRANTED) {
      requestPermissions(new String[] {
              Manifest.permission.READ_EXTERNAL_STORAGE,
              Manifest.permission.WRITE_EXTERNAL_STORAGE },
          PERMISSION_REQUEST_CODE);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    if (requestCode == PERMISSION_REQUEST_CODE) {
      if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)
          && permissions[1].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED
            && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
          Toast.makeText(getContext(), "Permission granted", Toast.LENGTH_LONG).show();
        } else {
          Toast.makeText(getContext(),
              "Anda harus mengizinkan aplikasi ini membaca/tulis ke media penyimpanan",
              Toast.LENGTH_LONG).show();
          NavController navController = Navigation.findNavController(binding.getRoot());
          navController.popBackStack();
        }
      }
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    HyperLog.d(TAG, String.format("onActivityResult: %s, %s", requestCode, resultCode));
    if (resultCode != Activity.RESULT_CANCELED) {
      switch (requestCode) {
      case id.co.danwinciptaniaga.npmdsandroid.util.Utility.REQUEST_TAKE_PHOTO:
        if (resultCode == Activity.RESULT_OK) {
          // kamera sudah menyimpan file, hapus temp (yg masih besar)
          viewModel.setAttachmentFilled(true, true);
        } else {
          // hapus temp (yg masih besar)
          viewModel.setAttachmentFilled(false, true);
        }
        break;
      case id.co.danwinciptaniaga.npmdsandroid.util.Utility.REQUEST_PICK_PHOTO:
        if (resultCode == Activity.RESULT_OK && data != null) {
          Uri selectedImage = data.getData();
          String[] columns = {
              MediaStore.Images.Media.DATA, // file path
              MediaStore.Images.Media.ORIENTATION };
          if (selectedImage != null) {
            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                columns, null, null, null);
            if (cursor != null) {
              cursor.moveToFirst();

              int filePathColumnIndex = cursor.getColumnIndex(columns[0]);
              int orientationColumnIndex = cursor.getColumnIndex(columns[1]);
              String picturePath = cursor.getString(filePathColumnIndex);
              int orientation = cursor.getInt(orientationColumnIndex);
              cursor.close();
              viewModel.setTempAttachmentFile(new File(picturePath));
              // karena sumbernya dari luar, jangan coba hapus sumber
              viewModel.setAttachmentFilled(true, false);
            }
          } else {
            // karena sumbernya dari luar, jangan coba hapus sumber
            viewModel.setAttachmentFilled(false, false);
          }

        }
        break;
      }
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }
}