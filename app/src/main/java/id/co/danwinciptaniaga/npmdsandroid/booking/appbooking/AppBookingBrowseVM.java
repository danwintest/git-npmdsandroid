package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.LoadState;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingSort;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseSort;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class AppBookingBrowseVM extends AndroidViewModel
    implements AppBookingListAction.Listener {
  private final static String TAG = AppBookingBrowseVM.class.getSimpleName();

  //APPBOOKING OPERATION PERMISSION
  public static String PERMISSION_CREATE = "npmds_AppBooking:create";
  //  npmds_AppBookingTransfer.edit
  public static String PERMISSION_EDIT = "npmds_AppBooking:update";
  public static String PERMISSION_READ ="npmds_AppBooking:read";
  public static String PERMISSION_DELETE = "npmds_AppBooking:delete";

  private AppBookingBrowseUC getListUC;
  private LiveData<PagingData<AppBookingData>> appBookingList;
  private MutableLiveData<LoadState> appBookingLoadState = new MutableLiveData<>();
  private AppBookingListAction appBookingListAction;
  private MutableLiveData<Boolean> refreshList = new SingleLiveEvent<>();
  private MutableLiveData<AppBookingFormState> formState = new MutableLiveData<>(
      AppBookingFormState.loading(""));
  private AppBookingBrowseFilter filterField = new AppBookingBrowseFilter();
  private Set<SortOrder> sortFields = new LinkedHashSet<>(); // urutan pengaruh

  private MutableLiveData<String> snackBarMessage = new MutableLiveData<>(null);

  @ViewModelInject
  public AppBookingBrowseVM(@NonNull Application app, AppExecutors appExecutors,
      AppBookingBrowseUC getListUC, AppBookingListAction appBookingListAction) {
    super(app);
    HyperLog.d(TAG, "Construct terapanggil");
    this.getListUC = getListUC;
    this.appBookingListAction = appBookingListAction;
    this.appBookingListAction.registerListener(this);
    this.sortFields.add(AppBookingSort
        .sortBy(AppBookingSort.Field.BOOKING_DATE, SortOrder.Direction.DESC));

    CoroutineScope viewModelScope = ViewModelKt.getViewModelScope(this);
    Pager<Integer, AppBookingData> pager = new Pager<Integer, AppBookingData>(
        new PagingConfig(Utility.PAGE_SIZE),
        () -> new AppBookingListPagingSource(getListUC, appExecutors.networkIO()));

    appBookingList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pager), viewModelScope);
  }

  public LiveData<LoadState> getAppBookingLoadState(){
    return appBookingLoadState;
  }
  public void setAppBookingLoadState(LoadState loadState){
    appBookingLoadState.postValue(loadState);
  }

  public LiveData<PagingData<AppBookingData>> getAppBookingList() {
    return appBookingList;
  }

  public MutableLiveData<String> getSnackBarMessage(){
    return this.snackBarMessage;
  }

  public void clearSnackBarMessage(){
    this.snackBarMessage.postValue(null);
  }

  public MutableLiveData<Boolean> getRefreshList() {
    return this.refreshList;
  }

  public void setRefreshList(Boolean b) {
    refreshList.postValue(b);
  }

  public void deletedSelectedAppBooking(List<AppBookingData> objList) {
    List<UUID> ids = objList.stream().map(obj -> {
      return obj.getId();
    }).collect(Collectors.toList());
    appBookingListAction.processDelete(ids);
  }

  public MutableLiveData<AppBookingFormState> getFormState(){
    return this.formState;
  }

  public void clearFormState(){
    this.formState.postValue(null);
  }

  public void loadDataWithFilter(AppBookingBrowseFilter filterField) {
    this.filterField = filterField;
    getListUC.setFilter(filterField);
    refreshList.postValue(true);
  }

  public AppBookingBrowseFilter getFilterField(){
    return filterField;
  }

  public void setSortFields(Set<SortOrder> sortFields) {
    this.sortFields.clear();
    if (sortFields != null) {
      this.sortFields.addAll(sortFields);
    }
    getListUC.setSort(sortFields);
    refreshList.postValue(true);
  }

  public Set<SortOrder> getSortFields() {
    return this.sortFields;
  }


  @Override
  public void onAppBookingActionProcessStarted(Resource<HashMap<String, List<String>>> loading) {
    HyperLog.d(TAG, "onAppBookingActionProcessStarted loading->" + loading.getMessage() + "<-");
    formState.postValue(AppBookingFormState.inProgress(loading.getMessage()));
    snackBarMessage.postValue(loading.getMessage());
  }

  @Override
  public void onAppBookingActionProcessSuccess(Resource<HashMap<String, List<String>>> response) {
    HyperLog.d(TAG, "onAppBookingActionProcessSuccess response->" + response.getMessage() + "<-");
    formState.postValue(AppBookingFormState.ready(response.getMessage(), false, null, null));
    snackBarMessage.postValue(response.getMessage());
  }

  @Override
  public void onAppBookingActionProcessFailure(Resource<HashMap<String, List<String>>> response) {
    HyperLog.d(TAG, "onAppBookingActionProcessFailure response->" + response.getMessage());
    formState.postValue(AppBookingFormState.errorProcess(response.getMessage()));
    snackBarMessage.postValue(response.getMessage());
  }
}
