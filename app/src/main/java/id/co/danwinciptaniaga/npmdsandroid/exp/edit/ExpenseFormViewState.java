package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

public class ExpenseFormViewState {
  boolean isLoading = false;
  boolean isVisible = false;
  Error error;

  public static abstract class Error {
    public final class None extends Error {
    }

    public final class AuthenticationError {
    }
  }
}
