package id.co.danwinciptaniaga.npmdsandroid.util;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.view.View;

import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import id.co.danwinciptaniaga.npmdsandroid.R;

public class AnimationUtil {
  private static String TAG = AnimationUtil.class.getSimpleName();
  private static AnimatorSet leftIn, rightOut, leftOut, rightIn;

  public static final int ANIM_DURATION_SHORT = 300;
  public static final int ANIM_DURATION_MEDIUM = 600;

  /**
   * Performs flip animation on two views
   */
  public static void flipView(Context context, final View back, final View front,
      boolean showFront) {
    leftIn = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.card_flip_left_in);
    rightOut = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.card_flip_right_out);
    leftOut = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.card_flip_left_out);
    rightIn = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.card_flip_right_in);

    final AnimatorSet showFrontAnim = new AnimatorSet();
    final AnimatorSet showBackAnim = new AnimatorSet();

    leftIn.setTarget(back);
    rightOut.setTarget(front);
    showFrontAnim.playTogether(leftIn, rightOut);

    leftOut.setTarget(back);
    rightIn.setTarget(front);
    showBackAnim.playTogether(rightIn, leftOut);

    if (showFront) {
      showFrontAnim.start();
    } else {
      showBackAnim.start();
    }
  }

  public static Animation fadeoutAnimationShort() {
    AlphaAnimation fadeoutAnimationShort = new AlphaAnimation(1f, 0f);
    fadeoutAnimationShort.setDuration(ANIM_DURATION_SHORT);
    return fadeoutAnimationShort;
  }

  public static Animation fadeinAnimationShort() {
    AlphaAnimation fadeinAnimationShort = new AlphaAnimation(0f, 1f);
    fadeinAnimationShort.setDuration(ANIM_DURATION_SHORT);
    return fadeinAnimationShort;
  }

  public static Animation fadeoutAnimationMedium() {
    AlphaAnimation fadeoutAnimationMedium = new AlphaAnimation(1f, 0f);
    fadeoutAnimationMedium.setDuration(ANIM_DURATION_MEDIUM);
    return fadeoutAnimationMedium;
  }

  public static Animation fadeinAnimationMedium() {
    AlphaAnimation fadeinAnimationMedium = new AlphaAnimation(0f, 1f);
    fadeinAnimationMedium.setDuration(ANIM_DURATION_MEDIUM);
    return fadeinAnimationMedium;
  }
}
