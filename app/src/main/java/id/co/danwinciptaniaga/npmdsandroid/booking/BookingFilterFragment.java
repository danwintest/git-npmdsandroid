package id.co.danwinciptaniaga.npmdsandroid.booking;

import java.time.LocalDate;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFilterFragmentArgs;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentBookingFilterBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

@AndroidEntryPoint
public class BookingFilterFragment extends BottomSheetDialogFragment {
  private static final String TAG = BookingFilterFragment.class.getSimpleName();
  private BookingFilterVM vm;
  private FragmentBookingFilterBinding binding;
  private ArrayAdapter<KeyValueModel<String, String>> bookingTypeAdapter;
  private ArrayAdapter<KeyValueModel<UUID, String>> companyAdapter;
  private ArrayAdapter<KeyValueModel<UUID, String>> cabangAdapter;
  private ArrayAdapter<KeyValueModel<UUID, String>> outletAdapter;
  public BookingFilterFragment() {
  }

  public static BookingFilterFragment newInstance(String param1, String param2) {
    BookingFilterFragment fragment = new BookingFilterFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentBookingFilterBinding.inflate(inflater);
    HyperLog.d(TAG,"onCreateView set FilterField [MULAI]");
    vm = new ViewModelProvider(this).get(BookingFilterVM.class);
    BookingFilterFragmentArgs args = BookingFilterFragmentArgs.fromBundle(getArguments());
//    vm.setBookingTypeKvm(null, args.getFilterField().getType());
//    vm.setFromDate(args.getFilterField().getBookingDateFrom());
//    vm.setToDate(args.getFilterField().getBookingDateTo());
//    vm.setConsumerName(args.getFilterField().getConsumerName());
    onReady();
    vm.setFilterField(args.getFilterField());
    vm.loadFilter();
    HyperLog.d(TAG,"onCreateView set FilterField [SELESAI]");
    return binding.getRoot();
  }

  @Override
  public void onResume() {
    super.onResume();
    setFilterCompany();
    setFilterCabang();
    setFilterOutlet();
    setFilterBookingType();
    setFilterFromDate();
    setFilterDateTo();
    setFilterConsumerName();
    setFilterVehicle();
    setFilterButton();
  }

  private void onReady() {
    vm.getFormState().observe(getViewLifecycleOwner(), state -> {
      switch (state.getState()) {
      case LOADING:
        showHideProgressLoading(true, state.getMessage());
        break;
      case READY:
        showHideProgressLoading(false, state.getMessage());
        break;
      case ERROR:
        showHideProgressLoading(true, state.getMessage());
        break;
      }
    });
  }

  private void showHideProgressLoading(boolean isShow, String message) {
    Animation outAnim = new AlphaAnimation(1f, 0f);
    outAnim.setDuration(200);
    AlphaAnimation inAnim = new AlphaAnimation(0f, 1f);
    inAnim.setDuration(200);

    if (message != null)
      Toast.makeText(getContext(), message,Toast.LENGTH_LONG).show();
    if (isShow && message != null)
      binding.progressWrapper.progressText.setText(message);
    binding.progressWrapper.progressView.setAnimation(isShow ? inAnim : outAnim);
    binding.progressWrapper.progressView.setVisibility(isShow ? View.VISIBLE : View.GONE);
    //    binding.pageContent.setVisibility(isShow ? View.GONE : View.VISIBLE);
    // TODO niat nya ingin enable dan disable form jika progress wrapper muncul, tapi masih gagal
    //    binding.pageContent.setEnabled(!isShow);
  }

  private void setFilterCompany() {
    vm.getCompanyKvmList().observe(getViewLifecycleOwner(),cList->{
      companyAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spCompany.setAdapter(companyAdapter);
      binding.spCompany.setFocusable(false);
      companyAdapter.addAll(cList);
      companyAdapter.notifyDataSetChanged();
      int pos = companyAdapter.getPosition(vm.getCompanyKvm());
      binding.spCompany.setSelection(pos);
    });

    binding.spCompany.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view,
          int i, long l) {
        KeyValueModel<UUID, String> kvm = companyAdapter.getItem(i);
        vm.setCompanyKvm(kvm);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
        vm.setCompanyKvm(null);
      }
    });

  }

  private void setFilterCabang() {
    vm.getCabangKvmList().observe(getViewLifecycleOwner(),cbList->{
      cabangAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spCabang.setAdapter(cabangAdapter);
      binding.spCabang.setFocusable(false);
      cabangAdapter.addAll(cbList);
      cabangAdapter.notifyDataSetChanged();
      int pos = cabangAdapter.getPosition(vm.getCabangKvm());
      binding.spCabang.setSelection(pos);
    });

    binding.spCabang.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view,
          int i, long l) {
        KeyValueModel<UUID, String> kvm = cabangAdapter.getItem(i);
        vm.setCabangKvm(kvm);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
        vm.setCabangKvm(null);
      }
    });

  }

  private void setFilterOutlet() {
    vm.getOutletKvmList().observe(getViewLifecycleOwner(),odList->{
      outletAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spOutlet.setAdapter(outletAdapter);
      binding.spOutlet.setFocusable(false);
      outletAdapter.addAll(odList);
      outletAdapter.notifyDataSetChanged();
      int pos = outletAdapter.getPosition(vm.getOutletKvm());
      binding.spOutlet.setSelection(pos);
    });

    binding.spOutlet.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @org.jetbrains.annotations.Nullable View view,
          int i, long l) {
        KeyValueModel<UUID, String> kvm = outletAdapter.getItem(i);
        vm.setOutletKvm(kvm);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
        vm.setOutletKvm(null);
      }
    });

  }

  private void setFilterBookingType() {
    bookingTypeAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    binding.spBookingType.setFocusable(false);
    binding.spBookingType.setAdapter(bookingTypeAdapter);
    bookingTypeAdapter.addAll(vm.getBookingTypeKvmList());
    bookingTypeAdapter.notifyDataSetChanged();

    vm.getBookingTypeKvm().observe(getViewLifecycleOwner(), type->{
      int pos = bookingTypeAdapter.getPosition(type);
      binding.spBookingType.setSelection(pos);
    });

    binding.spBookingType.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
          @org.jetbrains.annotations.Nullable View view,
          int i, long l) {
        KeyValueModel<String, String> dtKvm = bookingTypeAdapter.getItem(i);
        vm.setBookingTypeKvm(dtKvm,null);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
      }
    });
  }

  private void setFilterFromDate() {
    vm.getFromDate().observe(getViewLifecycleOwner(), date -> {
      if (date == null) {
        binding.etDateFrom.setText(null);
      } else if (!date.format(Formatter.DTF_dd_MM_yyyy)
          .equals(binding.etDateFrom.getText().toString())) {
        binding.etDateFrom.setText(date.format(Formatter.DTF_dd_MM_yyyy));
      }

      binding.etDateFrom.setOnClickListener(v -> {
        LocalDate toDate =
            date != null ? date : LocalDate.now();
        int day = toDate.getDayOfMonth();
        int month = toDate.getMonthValue() - 1;
        int year = toDate.getYear();
        DatePickerDialog dateDialog = new DatePickerDialog(getActivity(),
            new DatePickerDialog.OnDateSetListener() {
              @Override
              public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                LocalDate date = LocalDate.of(year, month + 1, dayOfMonth);
                vm.setFromDate(date);
                binding.etDateFrom.setText(date.format(Formatter.DTF_dd_MM_yyyy));
              }
            }, year, month, day);
        // TODO PENAMBAHAN FUNGSI SEMENTARA, HARAP DIPERBAIKI MENGGUNAKA CLASS DatePickerUIHelper nanti
        dateDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
          @Override
          public void onCancel(DialogInterface dialog) {
            vm.setFromDate(null);
            binding.etDateFrom.setText(null);
          }
        });
        dateDialog.show();
      });
    });
  }

  private void setFilterDateTo() {
    vm.getToDate().observe(getViewLifecycleOwner(), date -> {
      if (date == null) {
        binding.etDateTo.setText(null);
      } else if (!date.format(Formatter.DTF_dd_MM_yyyy).equals(
          binding.etDateTo.getText().toString())) {
        binding.etDateTo.setText(date.format(Formatter.DTF_dd_MM_yyyy));
      }

      binding.etDateTo.setOnClickListener(v -> {
        LocalDate toDate = date != null ? date : LocalDate.now();
        int day = toDate.getDayOfMonth();
        int month = toDate.getMonthValue() - 1;
        int year = toDate.getYear();
        DatePickerDialog dateDialog = new DatePickerDialog(getActivity(),
            new DatePickerDialog.OnDateSetListener() {
              @Override
              public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                LocalDate date = LocalDate.of(year, month + 1, dayOfMonth);
                vm.setToDate(date);
                binding.etDateTo.setText(date.format(Formatter.DTF_dd_MM_yyyy));
              }
            }, year, month, day);
        // TODO PENAMBAHAN FUNGSI SEMENTARA, HARAP DIPERBAIKI MENGGUNAKA CLASS DatePickerUIHelper nanti
        dateDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
          @Override
          public void onCancel(DialogInterface dialog) {
            vm.setToDate(null);
            binding.etDateTo.setText(null);
          }
        });
        dateDialog.show();
      });
    });
  }

  private void setFilterConsumerName() {
    vm.getConsumerName().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        binding.etConsumerName.setText(null);
      } else if (!data.equals( binding.etConsumerName.getText().toString())) {
        binding.etConsumerName.setText(data);
      }
    });

    // set listener
    binding.etConsumerName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          vm.setConsumerName(s.toString());
      }
    });
  }

  private void setFilterVehicle() {
    vm.getVehicleNo().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        binding.etVehicleNo.setText(null);
      } else if (!data.equals( binding.etVehicleNo.getText().toString())) {
        binding.etVehicleNo.setText(data);
      }
    });

    // set listener
    binding.etVehicleNo.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          vm.setVehicleNo(s.toString());
      }
    });
  }

  private void setFilterButton() {
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        NavController nc = NavHostFragment.findNavController(BookingFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(
            AppBookingFilterFragment.FILTER_FIELD, vm.getFilterField());
        nc.popBackStack();
      }
    });

    binding.btnClearAll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        NavController nc = NavHostFragment.findNavController(BookingFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(
            AppBookingFilterFragment.FILTER_FIELD,
            new BookingBrowseFilter());
        nc.popBackStack();
      }
    });
  }

}