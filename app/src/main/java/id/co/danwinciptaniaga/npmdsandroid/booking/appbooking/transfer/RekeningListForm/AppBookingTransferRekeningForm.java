package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import java.util.List;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;

import com.google.android.material.snackbar.Snackbar;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.BankData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferVM;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentAppBookingTransferRekeningFormBinding;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;

@AndroidEntryPoint
public class AppBookingTransferRekeningForm extends Fragment {
  private static final String TAG = AppBookingTransferRekeningForm.class.getSimpleName();
  protected ArrayAdapter<KeyValueModel<BankAccountData, String>> bankAdapter;
  public static final String TXN_FORM_FIELD = "TXN_FORM_FIELD";
  private FragmentAppBookingTransferRekeningFormBinding binding;
  private AppBookingTransferRekeningFormVM vm;
  private Integer maxRekNo = 999;

  private ProgressDialog pd;

  private void initForm() {
    setRekeningListObserver();
    binding.tvDate.setText("");
    setField_ImageValidStatus();
    setField_LainLain();
    setField_Bank();
    setField_AccountNo();
    setField_AccountName();
    setButton();
    setButtonClearFrom();
    validateBankAccountProgress();
  }

  private void setField_ImageValidStatus() {
    vm.getField_IsValid().observe(getViewLifecycleOwner(), isValid -> {
      setImageStatus(isValid);
    });
  }

  private void setField_LainLain() {
    // set data
    binding.swLainLain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        processField_LainLain(isChecked);
        setAccountNoMaxLength();
      }
    });
  }

  private void processField_LainLain(boolean isChecked) {
    ConstraintLayout cl = binding.pageContent;
    ConstraintSet cs = new ConstraintSet();
    if (isChecked) {
      binding.tilBank.setVisibility(View.GONE);
      binding.tilBankManual.setVisibility(View.VISIBLE);
      vm.setBankObj(null);
      binding.spBank.setSelection(-1);
      binding.btnCheck.setEnabled(false);
      binding.btnCheck.setVisibility(View.GONE);

      cs.clone(cl);
      cs.connect(binding.tilAccoutnName.getId(), ConstraintSet.TOP, binding.tilBankManual.getId(),
          ConstraintSet.BOTTOM);
    } else {
      binding.tilBank.setVisibility(View.VISIBLE);
      binding.tilBankManual.setVisibility(View.GONE);
      vm.setOtherBankName(null);
      binding.etBankManual.setText(null);
      binding.btnCheck.setEnabled(true);
      binding.btnCheck.setVisibility(View.VISIBLE);

      cs.clone(cl);
      cs.connect(binding.tilAccoutnName.getId(), ConstraintSet.TOP, binding.tilBank.getId(),
          ConstraintSet.BOTTOM);
    }
    cs.applyTo(cl);
  }

  private void setAccountNoMaxLength() {
    boolean isLainLain = binding.swLainLain.isChecked();
    if (isLainLain) {
      // jika lain lain, set max nomor rekening unlimited
      maxRekNo = 99;
      binding.etAccountNo.setFilters(new InputFilter[] { new InputFilter.LengthFilter(maxRekNo) });
      binding.tilAccountNo.setError(null);
    } else {
      // jika bukan, set max nomor rekening berdasarkan bank yang dipilih,
      KeyValueModel<BankAccountData, String> data = (KeyValueModel<BankAccountData, String>) binding.spBank
          .getSelectedItem();
      if (data == null)
        maxRekNo = 0;
      else
        maxRekNo = data.getKey().getAccNoLength();

      binding.etAccountNo.setFilters(new InputFilter[] { new InputFilter.LengthFilter(maxRekNo) });
    }
  }

  private void setField_Bank() {
    // set data
    bankAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    binding.spBank.setAdapter(bankAdapter);
    binding.spBank.setFocusable(false);

    vm.getBankObjList().observe(getViewLifecycleOwner(), bankList -> {
      if (bankList != null) {
        bankAdapter.clear();
        bankAdapter.addAll(bankList);
        bankAdapter.notifyDataSetChanged();

        // set listener
        binding.spBank.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
          @Override
          public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
              @org.jetbrains.annotations.Nullable View view,
              int i, long l) {
            KeyValueModel<BankAccountData, String> oKvm = bankAdapter.getItem(i);
            vm.setBankObj(oKvm);
            binding.swLainLain.setChecked(false);
            setAccountNoMaxLength();
          }

          @Override
          public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
            //            vm.setBankObj(null);
          }
        });

        // set selected
        vm.getBankObj().observe(getViewLifecycleOwner(), bankObj -> {
          if (bankObj != null) {
            binding.swLainLain.setChecked(false);
            int pos = bankAdapter.getPosition(bankObj);
            binding.spBank.setSelection(pos);
          }
        });
      }
    });

    vm.getOtherBankName().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        binding.etBankManual.setText(null);
      } else if (!data.equals(binding.etBankManual.getText().toString())) {
        binding.etBankManual.setText(data);
        binding.swLainLain.setChecked(true);
      }
    });

    binding.etBankManual.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        String value = s.toString();
        vm.setOtherBankName(value.isEmpty() ? null : value);
      }
    });
  }

  private void setField_AccountName() {
    vm.getAccName().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        binding.etAccountName.setText(null);
      } else if (!data.equals(binding.etAccountName.getText().toString())) {
        binding.etAccountName.setText(data);
      }
    });
    binding.etAccountName.setInputType(
        InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
    binding.etAccountName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        vm.setAccName(s.toString());
      }
    });
  }

  private void setField_AccountNo() {
    vm.getAccNo().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        binding.etAccountNo.setText(null);
      } else if (!data.equals(binding.etAccountNo.getText().toString())) {
        binding.etAccountNo.setText(data);
      }
    });

    binding.etAccountNo.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        vm.setAccNo(s.toString());
      }
    });
  }

  private void setButton() {
    binding.btnOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        processButtonOk(v);
      }
    });

    binding.btnPilihRekening.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //        Snackbar.make(getView(),"Fungsi belum tersedia", Snackbar.LENGTH_SHORT).show();
        processButtonPilihRekening();
      }
    });

    binding.btnCheck.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        boolean isAccountNo_OK = validateAccountNo();
        boolean isBank_OK = validateBank();
        boolean isAccountName_OK = validateAccountName();
        if (!isAccountNo_OK || !isBank_OK || !isAccountName_OK)
          return;
        vm.validateBankAccount();
      }
    });
  }

  private void processButtonPilihRekening() {
    String trxType = vm.getMode();
    UUID outletId = vm.getOutletId();
    AppBookingTransferRekeningFormDirections.ActionPilihRekening dir = AppBookingTransferRekeningFormDirections.actionPilihRekening(
        outletId, trxType);
    NavController nv = Navigation.findNavController(binding.getRoot());
    nv.navigate(dir, NavHelper.animChildToParent().build());
  }

  private void processButtonOk(View v) {
    String bankName =
        vm.getBankObj().getValue() != null ?
            vm.getBankObj().getValue().getKey().getBankName() :
            "null";
    UUID bankId =
        vm.getBankObj().getValue() != null ? vm.getBankObj().getValue().getKey().getBankId() : null;
    HyperLog.d(TAG,
        "processButtonOk() isOtherBank[" + vm.getOtherBankName().getValue() + "] bankName["
            + bankName + "] bankId[" + bankId + "] accName[" + vm.getAccName().getValue()
            + "] accNo[" + vm.getAccNo().getValue() + "]");

    boolean isAccountNo_OK = validateAccountNo();
    boolean isBank_OK = validateBank();
    boolean isAccountName_OK = validateAccountName();
    if (!isAccountNo_OK || !isBank_OK || !isAccountName_OK)
      return;
    NavController nc = Navigation.findNavController(v);

    TxnForm txnObj = new TxnForm();
    txnObj.setAccountName(vm.getAccName().getValue());
    txnObj.setAccountNo(vm.getAccNo().getValue());
    txnObj.setBankId(vm.getBankObj().getValue() != null ?
        vm.getBankObj().getValue().getKey().getBankId() :
        null);
    txnObj.setBankName(vm.getBankObj().getValue() != null ?
        vm.getBankObj().getValue().getKey().getBankName() :
        null);
    txnObj.setOtherBankName(vm.getOtherBankName().getValue());
    txnObj.setValidated(vm.getField_IsValid().getValue());
    txnObj.setMode(vm.getMode());

    nc.getPreviousBackStackEntry().getSavedStateHandle().set(TXN_FORM_FIELD, txnObj);
    nc.popBackStack();
  }

  public static class TxnForm extends BankAccountData {
    private String mode;

    private List<BankData> bankDataList;

    public String getMode() {
      return mode;
    }

    public void setMode(String mode) {
      this.mode = mode;
    }

    public List<BankData> getBankDataList() {
      return bankDataList;
    }

    public void setBankDataList(
        List<BankData> bankDataList) {
      this.bankDataList = bankDataList;
    }
  }

  private boolean validateBank() {
    boolean isLainLain = binding.swLainLain.isChecked();
    if (isLainLain) {
      binding.tilBank.setError(null);
      vm.setBankObj(null);
      if (vm.getOtherBankName().getValue() != null) {
        binding.tilBankManual.setError(null);
        return true;
      } else {
        binding.tilBankManual.setError(getString(R.string.validation_mandatory));
        return false;
      }
    } else {
      binding.tilBankManual.setError(null);
      vm.setOtherBankName(null);
      if (vm.getBankObj().getValue() != null) {
        binding.tilBank.setError(null);
        return true;
      } else {
        binding.tilBank.setError(getString(R.string.validation_mandatory));
        return false;
      }
    }
  }

  private boolean validateAccountNo() {
    // accountNo tidak boleh kosong
    if (binding.etAccountNo.getText() == null) {
      binding.tilAccountNo.setError(getString(R.string.validation_mandatory));
      return false;
    }
    if (binding.etAccountNo.getText().toString().isEmpty()) {
      binding.tilAccountNo.setError(getString(R.string.validation_mandatory));
      return false;
    }

    // Toast.makeText(getActivity(), "MaxNoRek ->" + maxRekNo + "<-", Toast.LENGTH_SHORT).show();
    // accountNo tidak boleh lebih dari batas maksimal accountNo Bank yang dipilih
    if (binding.etAccountNo.getText().length() > maxRekNo) {
      binding.tilAccountNo.setError(getString(R.string.accountNo_warn));
      return false;
    } else {
      binding.tilAccountNo.setError(null);
      return true;
    }
  }

  private boolean validateAccountName() {
    if (binding.etAccountName.getText() == null) {
      binding.tilAccoutnName.setError(getString(R.string.validation_mandatory));
      return false;
    }
    if (binding.etAccountName.getText().toString().isEmpty()) {
      binding.tilAccoutnName.setError(getString(R.string.validation_mandatory));
      return false;
    }

    if (vm.getMode() != AppBookingTransferVM.TXN_CONSUMER) {
      binding.tilAccoutnName.setError(null);
      return true;
    }

    String consumerName = vm.getConsumerName().getValue();
    consumerName = consumerName != null ? consumerName.toLowerCase() : "-";
    String accountName = binding.etAccountName.getText().toString();
    accountName = accountName != null ? accountName.toLowerCase() : "-";
    if (!accountName.equals(consumerName)) {
      binding.tilAccoutnName.setError(getString(R.string.accountName_warn));
      return true;
    } else {
      binding.tilAccoutnName.setError(null);
      return true;
    }

  }

  private void setButtonClearFrom() {
    //    binding.ivClearForm.setOnClickListener(new View.OnClickListener() {
    //      @Override
    //      public void onClick(View v) {
    //        if (vm.getMode() == AppBookingTransferVM.TXN_CONSUMER) {
    ////          vm.setField_ConsumerBankId(null, null);
    ////          vm.setField_ConsumerOtherBank(null);
    ////          vm.setField_ConsumerBank_LABEL("-");
    ////          vm.setField_ConsumerAccountNo(null);
    ////          vm.setField_ConsumerAccountName(null,false);
    //        } else if (vm.getMode() == AppBookingTransferVM.TXN_FIF) {
    ////          vm.setField_FIFBankId(null, null);
    ////          vm.setField_FIFOtherBank(null);
    ////          vm.setField_FIFBank_LABEL("-");
    ////          vm.setField_FIFAccountNo(null);
    ////          vm.setField_FIFAccountName(null);
    //        } else if (vm.getMode() == AppBookingTransferVM.TXN_BIRO_JASA) {
    ////          vm.setField_BiroJasaBankId(null, null);
    ////          vm.setField_BiroJasaOtherBank(null);
    ////          vm.setField_BiroJasaBank_LABEL("-");
    ////          vm.setField_BiroJasaAccountNo(null);
    ////          vm.setField_BiroJasaAccountName(null);
    //        }
    ////        dismiss();
    //      }
    //    });
  }

  private void validateBankAccountProgress() {
    vm.getValidateBankAccountProgress().observe(getViewLifecycleOwner(), result -> {
      showHideFormProgressBar(result.getShowProgress());
      if (result.getErrorMessage() != null) {
        Toast.makeText(getContext(), result.getErrorMessage(), Toast.LENGTH_SHORT).show();
      }
      if (result.isValid() != null)
        setImageStatus(result.isValid());
    });
  }

  private void showHideFormProgressBar(boolean isShow) {
    if (true == false) {
      if (isShow) {
        pd = ProgressDialog.show(getActivity(), "",
            getString(R.string.silahkan_tunggu), true);
        pd.show();
      } else {
        if (pd != null)
          pd.dismiss();
      }
      return;
    }
    Animation outAnim = new AlphaAnimation(1f, 0f);
    outAnim.setDuration(200);
    AlphaAnimation inAnim = new AlphaAnimation(0f, 1f);
    inAnim.setDuration(200);
    String msg = getString(R.string.silahkan_tunggu);

    Snackbar.make(getView(), msg, Snackbar.LENGTH_SHORT).show();

    binding.progressWrapper.progressText.setText(msg);
    binding.progressWrapper.progressView.setAnimation(isShow ? inAnim : outAnim);
    binding.progressWrapper.progressView.setVisibility(isShow ? View.VISIBLE : View.GONE);
    binding.pageContent.setVisibility(isShow ? View.GONE : View.VISIBLE);
    // TODO niat nya ingin enable dan disable form jika progress wrapper muncul, tapi masih
    // gagal
    binding.pageContent.setEnabled(!isShow);
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    binding = FragmentAppBookingTransferRekeningFormBinding.inflate(inflater, container, false);
    vm = new ViewModelProvider(this).get(AppBookingTransferRekeningFormVM.class);
    AppBookingTransferRekeningFormArgs args = AppBookingTransferRekeningFormArgs.fromBundle(
        getArguments());
    vm.setAppBookingId(args.getAppBookingId());
    vm.setBookingId(args.getBookingId());
    vm.setCompanyId(args.getCompanyId());
    vm.setFromBooking(args.getFromBookingStatus());
    vm.setMode(args.getMode());
    vm.setOutletId(args.getOutletId());
    vm.setAccName(args.getAccName());
    vm.setAccNo(args.getAccNo());
    vm.setBankId(args.getBankId());
    vm.setField_IsValid(args.getValidStatus());
    vm.setOtherBankName(args.getOtherBankName());
    vm.setConsumerName(args.getConsumerName());
    vm.getBankList();
    //    List<BankAccountData> test = Arrays.asList(args.getBankAccountData());
    //    for(BankAccountData bad : test) {
    //      HyperLog.d(TAG, bad.getAccountName());
    //    }
    initForm();
    return binding.getRoot();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }

  private void setImageStatus(boolean isValid) {
    if (isValid) {
      binding.ivDate.setImageResource(R.drawable.ic_baseline_check_circle_outline_24);
    } else {
      binding.ivDate.setImageResource(R.drawable.ic_baseline_remove_circle_outline_24);
    }
  }

  private void setRekeningListObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<BankAccountData> selectedData = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        AppBookingTransferRekeningListFragment.SELECTED_DATA);
    selectedData.observe(getViewLifecycleOwner(), sdObj -> {
      boolean isOtherBank = sdObj.getBankId() == null;
      String selectedBankName = sdObj.getBankName();
      UUID selectedBankId = sdObj.getBankId();
      String selectedAccName = sdObj.getAccountName();
      String selectedAccNo = sdObj.getAccountNo();

      HyperLog.d(TAG, "setRekeningListObserver() isOtherBank[" + isOtherBank + "] bankName["
          + selectedBankName + "] bankId[" + selectedBankId + "] accName[" + selectedAccName
          + "] accNo[" + selectedAccNo + "]");
      //      binding.swLainLain.setOnCheckedChangeListener (null);

      vm.setAccNo(selectedAccNo);
      vm.setAccName(selectedAccName);
      if (isOtherBank) {
        binding.ivDate.setImageResource(R.drawable.ic_baseline_remove_circle_outline_24);
        binding.swLainLain.setChecked(true);
        vm.setOtherBankName(selectedBankName);
        vm.setBankId(null);
      } else {
        binding.swLainLain.setChecked(false);
        vm.getBankListFromValidAcc(selectedBankId);
        vm.setOtherBankName(null);
        vm.setBankId(selectedBankId);
      }
      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
          AppBookingTransferRekeningListFragment.SELECTED_DATA);
    });
  }

}
