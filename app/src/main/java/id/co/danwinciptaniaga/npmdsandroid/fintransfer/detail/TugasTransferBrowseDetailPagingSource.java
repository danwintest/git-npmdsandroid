package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferDetailBrowseResponse;

public class TugasTransferBrowseDetailPagingSource
    extends ListenableFuturePagingSource<Integer, TugasTransferBrowseDetailData> {
  private static final String TAG = TugasTransferBrowseDetailPagingSource.class.getSimpleName();
  private final GetTugasTransferDetailBrowseUC uc;
  private final Executor mBgExecutor;

  public TugasTransferBrowseDetailPagingSource(GetTugasTransferDetailBrowseUC uc,
      Executor mBgExecutor) {
    this.uc = uc;
    this.mBgExecutor = mBgExecutor;
  }

  @NotNull
  @Override
  public ListenableFuture<LoadResult<Integer, TugasTransferBrowseDetailData>> loadFuture(
      @NotNull LoadParams<Integer> loadParams) {
    Integer nextPageNumber = loadParams.getKey();
    if(nextPageNumber == null){
      nextPageNumber = 1;
    }
    int pageSize = loadParams.getLoadSize();
    ListenableFuture<LoadResult<Integer,TugasTransferBrowseDetailData>> pageFuture = Futures.transform(uc.getTugasTransferBrowseDetailList(nextPageNumber,pageSize),this::toLoadResult,mBgExecutor);

    return Futures.catching(pageFuture,Exception.class, input->{
      HyperLog.e(TAG, "ERROR while getting TugasTransferBrowseDetailList", input);
      ErrorResponse er = RetrofitUtility.parseErrorBody(input);
      return new LoadResult.Error(
          er != null ? new Exception(er.getFormattedMessage(), input) : input);
    }, mBgExecutor);
  }

  private LoadResult<Integer, TugasTransferBrowseDetailData> toLoadResult(
      @NonNull TugasTransferDetailBrowseResponse response) {
    return new LoadResult.Page<>(response.getDataList(), null, response.getNextPageNumber(),
        LoadResult.Page.COUNT_UNDEFINED, LoadResult.Page.COUNT_UNDEFINED);
  }
}