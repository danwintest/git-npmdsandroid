package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask;

import java.lang.annotation.Native;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseHeaderResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskFilterResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskProcessResultData;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.TugasDroppingHarianBrowseSort;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.TugasDroppingHarianDetailSort;
import okhttp3.MultipartBody;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface DroppingDailyTaskService {

  @GET("rest/s/DroppingTask/getHeaderList")
  ListenableFuture<DroppingDailyTaskBrowseHeaderResponse> getHeaderList(
      @Query("page") Integer page, @Query("pageSize") Integer pageSize,
      @Query("filter") DroppingDailyTaskBrowseFilter filterString,
      @Query("sort") TugasDroppingHarianBrowseSort sortString);

  @GET("rest/s/DroppingTask/getDetailList")
  ListenableFuture<DroppingDailyTaskBrowseDetailResponse> getDetailList(
      @Query("page") Integer page, @Query("pageSize") Integer pageSize,
      @Query("filter") DroppingDailyTaskBrowseFilter filterString,
      @Query("sort") TugasDroppingHarianDetailSort sortString);

  @GET("rest/s/DroppingTask/getCompanyListFilter")
  ListenableFuture<DroppingDailyTaskFilterResponse> getCompanyListFilter();

  @GET("rest/s/DroppingTask/getDefaultCompanyFilter")
  ListenableFuture<DroppingDailyTaskFilterResponse> getDefaultCompanyFilter();

  @POST("rest/s/DroppingTask/processApprove")
  ListenableFuture<DroppingDailyTaskProcessResultData> processApprove(
  @Query("dataList") String dataString, @Query("comment") String comment);

  @POST("rest/s/DroppingTask/processReject")
  ListenableFuture<DroppingDailyTaskProcessResultData> processReject(
      @Query("dataList") String dataString, @Query("comment") String comment);
}
