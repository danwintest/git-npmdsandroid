package id.co.danwinciptaniaga.npmdsandroid.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import org.acra.ACRA;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.appcompat.app.AlertDialog;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.databinding.WfdecisionBinding;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.DrawerViewModel;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfProcessParameter;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfValidationProcess;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class Utility {
  public static final int REQUEST_TAKE_PHOTO = 0;
  public static final int REQUEST_PICK_PHOTO = 1;

  // TODO MUNGKIN SEBAIKNYA DIPINDAHKAN MENJADI SATU CLASS YANG SAMA DAN DIGUNAKAN BERSAMA OLEH WEB DAN ANDROID?
  // WF STATUS
  public final static String WF_STATUS_DRAFT = "DR";
  public final static String WF_STATUS_PENDING_REVISIION = "PR";
  public final static String WF_STATUS_REJECTED = "RJ";
  public final static String WF_STATUS_APPROVED = "AP";
  public final static String WF_STATUS_PENDING_APPROVAL = "PA";
  public final static String AUTOMATIC="A";
  public final static String MANUAL="M";
  public final static String WF_OUTCOME_APPROVE_TRANSFER_OTOMATIS="Approve Transfer Otomatis";
  public final static String WF_OUTCOME_APPROVE_TRANSFER_MANUAL= "Approve Transfer Manual";
  public static int PAGE_SIZE = 10;

  //BOOKING TYPE
  public final static String BOOKING_TYPE_CASH = "C";
  public final static String BOOKING_TYPE_TRANSFER = "T";

  // OPERATION
  public final static String OPERATION_NEW = "N";
  public final static String OPERATION_EDIT = "E";
  public final static String OPERATION_CANCEL = "C";

  public static final String BIAYA_OPERASIONAL = "210";
  public static final String DROPPING_DAILY = "310";
  public static final String DROPPING_ADDITIONAL = "311";

  public static String getMetadata(Context context, String key) {
    try {
      ApplicationInfo ai = context.getPackageManager().getApplicationInfo(context.getPackageName(),
          PackageManager.GET_META_DATA);
      if (ai.metaData != null) {
        return ai.metaData.getString(key);
      }
    } catch (PackageManager.NameNotFoundException e) {
      // if we can't find it in the manifest, just return null
    }
    return null;
  }

  public static void setActionModeMenu(ActionMode actMode, String TAG) {
    MenuItem actDelete = actMode.getMenu().findItem(R.id.action_delete);
    MenuItem actApprove = actMode.getMenu().findItem(R.id.action_approve);
    MenuItem actReturn = actMode.getMenu().findItem(R.id.action_return);
    MenuItem actReject = actMode.getMenu().findItem(R.id.action_reject);
    if (TAG == AppBookingBrowseFragment.TAG) {

    } else if (TAG == ExpenseBrowseFragment.TAG) {
      actReturn.setVisible(false);
    }
  }

  public static String getFormattedAmt(BigDecimal amount) {
    String formattedAmt = NumberFormat.getInstance().format(amount);
    return formattedAmt;
  }

  public static String getFormattedAmt(Long amount) {
    String formattedAmt = NumberFormat.getInstance().format(amount);
    return formattedAmt;
  }

  public static BigDecimal getAmtFromFromattedString(String amtS) {
    BigDecimal amt = BigDecimal.ZERO;
    try {
      amt = new BigDecimal((Long) NumberFormat.getInstance().parse(amtS));
    } catch (Exception e) {
      e.printStackTrace();
      ACRA.getErrorReporter().handleException(e);
    }finally {
      return amt;
    }
  }

  public static Long getLongAmtFromFromattedString(String amtS) {
    Long amt = 0L;
    try {
      amt = (Long) NumberFormat.getInstance().parse(amtS);
    } catch (Exception e) {
      e.printStackTrace();
      ACRA.getErrorReporter().handleException(e);
    } finally {
      return amt;
    }
  }

  public static String getErrorMessageFromThrowable(String TAG, Throwable t) {
    ACRA.getErrorReporter().handleException(t);
    if (t instanceof HttpException) {
      ResponseBody body = ((HttpException) t).response().errorBody();
      InputStream errorObj = body.byteStream();

      BufferedReader reader = null;
      StringBuilder sb = new StringBuilder();
      reader = new BufferedReader(new InputStreamReader(errorObj));
      String line;
      try {
        while ((line = reader.readLine()) != null) {
          sb.append(line);
        }
      } catch (IOException e) {
        HyperLog.e(TAG,"Gagal saat convert pesan errror ",e);
        return "Gagal dalam convert pesan error ->" + errorObj.toString() + "<-";
      }

      String finallyError = sb.toString();
      HyperLog.e(TAG,"ERROR-> "+finallyError,t);
      return finallyError;
    }else{
      HyperLog.e(TAG,"ERROR->",t);
      return t.getMessage();
    }
  }

  //          Utility.renderCustomWfButton(getTransferVm().getPtCons().getPossibleOutcome(),
  //              getBinding().llWFButtonContainerCons, getLayoutInflater(), getActivity(), margin);
  public static void renderCustomWfButton(List<String> wfTaskList, LinearLayout buttonContainer,
      LayoutInflater layoutInflater, Activity act, WfValidationProcess preValidation, Consumer<WfProcessParameter> handleButton, boolean isOtp) {
    isOtp = false;
    buttonContainer.removeAllViews();
    int margin = (int) act.getResources().getDimension(R.dimen.activity_vertical_margin);
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    params.setMargins(0, 0, margin, 0);
    for (String wfTask : wfTaskList) {
      Button btn = (Button) layoutInflater.inflate(R.layout.outlined_button, null);
      btn.setText(wfTask);
      btn.setTag(wfTask);
      btn.setEnabled(true);
      boolean finalIsOtp = isOtp;
      btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          WfProcessParameter paramObj = new WfProcessParameter();
          paramObj.setDecision((String) v.getTag());
          Boolean res = preValidation.call(paramObj);
          if (!res)
            return;

          String decision = (String) v.getTag();
          WfdecisionBinding wfdBinding = WfdecisionBinding.inflate(layoutInflater);
          if (finalIsOtp && WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)) {
            wfdBinding.btnGetKodeVerifikasi.setVisibility(View.VISIBLE);
            wfdBinding.tilKodeVerifikasi.setVisibility(View.VISIBLE);
            wfdBinding.btnGetKodeVerifikasi.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                WfProcessParameter paramObj = new WfProcessParameter();
                paramObj.setDecision(WorkflowConstants.WF_OUTCOME_REQ_OTP);
                paramObj.setComment(null);
                paramObj.setOtp(null);
                handleButton.accept(paramObj);
              }
            });
          }

          AlertDialog.Builder builder = new AlertDialog.Builder(act);
          builder.setTitle(decision).setView(wfdBinding.getRoot())
              .setNegativeButton(act.getString(R.string.workflow_cancel),
                  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      dialog.dismiss();
                    }
                  });

          boolean isMandatoryComment = WorkflowConstants.WF_OUTCOME_RETURN.equals(decision)
              || WorkflowConstants.WF_OUTCOME_REJECT.equals(decision);
          boolean isMandatoryOTP = finalIsOtp && WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision);
          if (isMandatoryComment) {
            builder.setPositiveButton(act.getString(R.string.workflow_yes), null);
            AlertDialog dialog = builder.show();

            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
                new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                    String comment = id.co.danwinciptaniaga.androcon.utility.Utility
                        .getTrimmedString(wfdBinding.tietComment.getText());
                    if (TextUtils.isEmpty(comment)) {
                      wfdBinding.tilComment.setError(act.getText(R.string.validation_mandatory));
                    } else {
                      wfdBinding.tilComment.setError(null);
                      WfProcessParameter paramObj = new WfProcessParameter();
                      paramObj.setDecision(decision);
                      paramObj.setComment(comment);
                      paramObj.setOtp(null);
                      handleButton.accept(paramObj);
                      dialog.dismiss();
                    }
                  }
                });
          }else if(isMandatoryOTP){
            builder.setPositiveButton(act.getString(R.string.workflow_yes),null);
            AlertDialog dialog = builder.show();
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
                new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                    String otpCode = id.co.danwinciptaniaga.androcon.utility.Utility
                        .getTrimmedString(wfdBinding.etKodeVerifikasi.getText());
                    String comment = id.co.danwinciptaniaga.androcon.utility.Utility
                        .getTrimmedString(wfdBinding.tietComment.getText());
                    if(TextUtils.isEmpty(otpCode)){
                      wfdBinding.tilKodeVerifikasi.setError(act.getText(R.string.validation_mandatory));
                    }else{
                      wfdBinding.tilKodeVerifikasi.setError(null);

                      WfProcessParameter paramObj = new WfProcessParameter();
                      paramObj.setDecision(decision);
                      paramObj.setComment(comment);
                      String otpVal = id.co.danwinciptaniaga.androcon.utility.Utility
                          .getTrimmedString(wfdBinding.etKodeVerifikasi.getText());
                      paramObj.setOtp(otpVal);
                      dialog.dismiss();
                      handleButton.accept(paramObj);
                    }
                  }
                });
          } else {
            builder.setPositiveButton(act.getString(R.string.workflow_yes),
                new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    String comment = id.co.danwinciptaniaga.androcon.utility.Utility
                        .getTrimmedString(wfdBinding.tietComment.getText());

                    WfProcessParameter paramObj = new WfProcessParameter();
                    paramObj.setDecision(decision);
                    paramObj.setComment(comment);
                    paramObj.setOtp(null);
                    handleButton.accept(paramObj);
                  }
                });
            builder.show();
          }
        }
      });
      btn.setLayoutParams(params);
      buttonContainer.addView(btn);
    }
  }

  public static Date getDateFromString(String TAG,String val){
    Date date = new Date();
    try{
      date = Formatter.SDF_dd_MM_yyyy.parse(val);
    }catch (Exception e){
      HyperLog.exception(TAG, "Error getDateFromString ->", e);
      ACRA.getErrorReporter().handleException(e);
    }finally {
      return date;
    }
  }

  public static UUID getCurrentOrSubstitutedUserId(String TAG, DrawerViewModel drawerVm){
    try{
       return drawerVm.getUserInfoLd().getValue().getData().getId();
    }catch (Exception e){
      HyperLog.exception(TAG, "Error getCurrentOrSubstitutedUserId ->",e);
      ACRA.getErrorReporter().handleException(e);
      throw e;
    }
  }
}
