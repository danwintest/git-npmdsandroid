package id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilterResponse;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyService;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.filter.GetDroppingDailyFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class GetDroppingAdditionalFilterUC
    extends BaseObservable<GetDroppingAdditionalFilterUC.Listener, DroppingBrowseFilterResponse> {
  private final String TAG = GetDroppingDailyFilterUC.class.getSimpleName();
  private final DroppingAdditionalService service;
  private final Application app;
  private final AppExecutors appExecutors;

  @Inject
  public GetDroppingAdditionalFilterUC(Application app, DroppingAdditionalService service,
      AppExecutors appExecutors) {
    super(app);
    this.service = service;
    this.app = app;
    this.appExecutors = appExecutors;
  }

  @Override
  protected void doNotifyStart(GetDroppingAdditionalFilterUC.Listener listener,
      Resource<DroppingBrowseFilterResponse> result) {
    listener.onProcessStarted(result);

  }

  @Override
  protected void doNotifySuccess(GetDroppingAdditionalFilterUC.Listener listener,
      Resource<DroppingBrowseFilterResponse> result) {
    listener.onProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(GetDroppingAdditionalFilterUC.Listener listener,
      Resource<DroppingBrowseFilterResponse> result) {
    listener.onProcessFailure(result);
  }

  public void getFilter() {
    notifyStart("", null);
    ListenableFuture<DroppingBrowseFilterResponse> process = service.getDroppingFilterField();
    Futures.addCallback(process, new FutureCallback<DroppingBrowseFilterResponse>() {
      @Override
      public void onSuccess(@NullableDecl DroppingBrowseFilterResponse result) {
        HyperLog.d(TAG, "getFilter() berhasil");
        notifySuccess("-", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("getFilter() Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public interface Listener {
    void onProcessStarted(Resource<DroppingBrowseFilterResponse> loading);

    void onProcessSuccess(Resource<DroppingBrowseFilterResponse> res);

    void onProcessFailure(Resource<DroppingBrowseFilterResponse> res);
  }
}
