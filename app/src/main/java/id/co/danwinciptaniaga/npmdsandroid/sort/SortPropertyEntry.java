package id.co.danwinciptaniaga.npmdsandroid.sort;

import java.util.Objects;

import androidx.annotation.NonNull;

/**
 * Class ini merupakan model untuk di Adapter dan RecyclerView.
 */
public class SortPropertyEntry {
  private String name;
  private int stringResId;
  private boolean selected = false;
  private boolean on = true;

  public SortPropertyEntry(@NonNull String name, @NonNull int stringResId, boolean selected,
      boolean on) {
    this.name = name;
    this.stringResId = stringResId;
    this.selected = selected;
    this.on = on;
  }

  public String getName() {
    return name;
  }

  public int getStringResId() {
    return stringResId;
  }

  public boolean isSelected() {
    return selected;
  }

  public void setSelected(boolean selected) {
    this.selected = selected;
  }

  public boolean isOn() {
    return on;
  }

  public void setOn(boolean on) {
    this.on = on;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    SortPropertyEntry that = (SortPropertyEntry) o;
    return Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public String toString() {
    return "SortPropertyEntry{" +
        "name='" + name + '\'' +
        ", stringResId=" + stringResId +
        ", selected=" + selected +
        ", on=" + on +
        '}';
  }
}
