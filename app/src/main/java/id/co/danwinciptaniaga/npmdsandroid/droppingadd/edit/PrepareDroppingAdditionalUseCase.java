package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.dropping.NewDroppingAdditionalResponse;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;

public class PrepareDroppingAdditionalUseCase extends
    BaseObservable<PrepareDroppingAdditionalUseCase.Listener, NewDroppingAdditionalResponse> {
  public interface Listener {
    void onPrepareNewDroppingAdditionalStarted();

    void onPrepareNewDroppingAdditionalSuccess(Resource<NewDroppingAdditionalResponse> response);

    void onPrepareNewDroppingAdditionalFailure(
        Resource<NewDroppingAdditionalResponse> responseError);
  }

  private final DroppingAdditionalService das;
  private final AppExecutors appExecutors;

  @Inject
  public PrepareDroppingAdditionalUseCase(Application app, DroppingAdditionalService das,
      AppExecutors appExecutors) {
    super(app);
    this.das = das;
    this.appExecutors = appExecutors;
  }

  public void prepare() {
    notifyStart(null, null);

    ListenableFuture<NewDroppingAdditionalResponse> newDroppingAdditional = das.getNewDroppingAdditional();
    Futures.addCallback(newDroppingAdditional, new FutureCallback<NewDroppingAdditionalResponse>() {
      @Override
      public void onSuccess(@NullableDecl NewDroppingAdditionalResponse result) {
        notifySuccess(null, result);
      }

      @Override
      public void onFailure(Throwable t) {
        notifyFailure(null, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(PrepareDroppingAdditionalUseCase.Listener listener,
      Resource<NewDroppingAdditionalResponse> result) {
    listener.onPrepareNewDroppingAdditionalStarted();
  }

  @Override
  protected void doNotifySuccess(PrepareDroppingAdditionalUseCase.Listener listener,
      Resource<NewDroppingAdditionalResponse> result) {
    listener.onPrepareNewDroppingAdditionalSuccess(result);

  }

  @Override
  protected void doNotifyFailure(PrepareDroppingAdditionalUseCase.Listener listener,
      Resource<NewDroppingAdditionalResponse> result) {
    listener.onPrepareNewDroppingAdditionalFailure(result);

  }
}
