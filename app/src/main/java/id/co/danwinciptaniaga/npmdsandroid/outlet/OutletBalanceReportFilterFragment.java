package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;
import com.hypertrack.hyperlog.HyperLog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentExpenseBrowseFilterBinding;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentOutletBalanceReportFilterBinding;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseBrowseViewModel;
import id.co.danwinciptaniaga.npmdsandroid.ui.DatePickerUIHelper;

@AndroidEntryPoint
public class OutletBalanceReportFilterFragment extends BottomSheetDialogFragment {
  private static final String TAG = OutletBalanceReportFilterFragment.class.getSimpleName();
  private OutletBalanceReportVM viewModel;
  private FragmentOutletBalanceReportFilterBinding binding;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    HyperLog.d(TAG, "onCreate called");
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(requireActivity()).get(OutletBalanceReportVM.class);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    HyperLog.d(TAG, "onCreateView called");
    binding = FragmentOutletBalanceReportFilterBinding.inflate(inflater);
    setupStatementDateFilter(binding);
    setupButtons(binding);

    return binding.getRoot();
  }

  private void setupStatementDateFilter(FragmentOutletBalanceReportFilterBinding binding) {
    DatePickerUIHelper.setupEditTextDateField(getContext(), binding.etStatementDateFrom,
        () -> viewModel.getStatementDateFromFilter(), false);
    DatePickerUIHelper.setupEditTextDateField(getContext(), binding.etStatementDateTo,
        () -> viewModel.getStatementDateToFilter(), false);
  }

  private void setupButtons(FragmentOutletBalanceReportFilterBinding binding) {
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // pada saat tombol Apply diklik, baru baca semua dan set semua filter
        LocalDate statementDateFrom = (LocalDate) binding.etStatementDateFrom.getTag();
        LocalDate statementDateTo = (LocalDate) binding.etStatementDateTo.getTag();

        viewModel.setFilter(statementDateFrom, statementDateTo);
        // tutup filter
        dismiss();
      }
    });
    binding.btnClearAll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // hanya clear di UI saja
        LocalDate fromDate = LocalDate.now().withDayOfMonth(1);
        LocalDate toDate = LocalDate.now();

        DatePickerUIHelper.bindDate(binding.etStatementDateFrom, fromDate);
        DatePickerUIHelper.bindDate(binding.etStatementDateTo, toDate);
      }
    });
  }
}
