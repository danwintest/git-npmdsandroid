package id.co.danwinciptaniaga.npmdsandroid.auth;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import id.co.danwinciptaniaga.androcon.auth.AccountAuthenticator;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCaseSync;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.ui.login.AuthenticationActivity;

public class NpmdsAccountAuthenticator extends AccountAuthenticator {
  private static final String TAG = NpmdsAccountAuthenticator.class.getSimpleName();

  public NpmdsAccountAuthenticator(Context context, GetTokenUseCaseSync ucGetTokenSync,
      LoginUtil loginUtil) {
    super(context, ucGetTokenSync, loginUtil);
    Log.i(TAG, "NpmdsAccountAuthenticator constructor");
  }

  @Override
  protected Intent getAddAcountIntent(AccountAuthenticatorResponse response, String accountType,
      String authTokenType, Bundle options) {
    Log.i(TAG, "getAddAcountIntent called");
    Intent intent = new Intent(mContext, AuthenticationActivity.class);
    return intent;
  }

  @Override
  protected Intent getAuthenticateIntent(AccountAuthenticatorResponse response,
      Account account, String authTokenType, Bundle options) {
    Log.i(TAG,
        "getAuthenticateIntent called for %s: " + account != null ? account.name : null);
    Intent intent = new Intent(mContext, AuthenticationActivity.class);
    return intent;
  }

  @Override
  protected Intent getConfirmCredentialsIntent(AccountAuthenticatorResponse response,
      Account account, Bundle options) {
    Log.i(TAG,
        "getConfirmCredentialsIntent called for %s: " + account != null ? account.name : null);
    Intent intent = new Intent(mContext, AuthenticationActivity.class);
    intent.putExtra(AccountAuthenticator.ARG_AUTH_TOKEN_TYPE, App.AUTH_TOKEN_TYPE);
    return intent;
  }
}
