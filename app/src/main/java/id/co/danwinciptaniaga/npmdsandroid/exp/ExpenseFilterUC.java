package id.co.danwinciptaniaga.npmdsandroid.exp;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseFilterResponse;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class ExpenseFilterUC
    extends BaseObservable<ExpenseFilterUC.Listener, ExpenseFilterResponse> {
  private final String TAG = ExpenseFilterUC.class.getSimpleName();
  private final ExpenseService service;
  private final Application app;
  private final AppExecutors appExecutors;

  @Inject
  public ExpenseFilterUC(Application app, ExpenseService service, AppExecutors appExecutors) {
    super(app);
    this.app = app;
    this.appExecutors = appExecutors;
    this.service = service;
  }

  public void getStatusList() {
    String fn = "getStatusList() ";
    HyperLog.d(TAG, fn + "terpanggil");
    notifyStart(app.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<ExpenseFilterResponse> process = service.getFilterField();
    Futures.addCallback(process, new FutureCallback<ExpenseFilterResponse>() {
      @Override
      public void onSuccess(@NullableDecl ExpenseFilterResponse result) {
        HyperLog.d(TAG, fn + "berhasil res[" + result.getEsdList() + "]");
        notifySuccess("", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String msg = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(app.getString(R.string.process_fail) + msg, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<ExpenseFilterResponse> result) {
    listener.onProcessStartedGetStatusList(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<ExpenseFilterResponse> result) {
    listener.onProcessSuccessGetStatusList(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<ExpenseFilterResponse> result) {
    listener.onProcessFailureGetStatusList(result);
  }

  public interface Listener {
    void onProcessStartedGetStatusList(Resource<ExpenseFilterResponse> loading);

    void onProcessSuccessGetStatusList(Resource<ExpenseFilterResponse> res);

    void onProcessFailureGetStatusList(Resource<ExpenseFilterResponse> res);
  }
}
