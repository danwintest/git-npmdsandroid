package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import java.time.LocalDate;

import com.google.common.base.Preconditions;

import android.content.Context;
import android.text.TextUtils;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.data.ConstraintViolation;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

public class DroppingAdditionalValidationHelper {
  // kelipatan 100,000
  public static final long TOTAL_REQUEST_AMOUNT_ROUNDED_TO = 100000;

  public static void validateDroppingDate(Context context, LocalDate now, LocalDate statementDate)
      throws ConstraintViolation {
    Preconditions.checkNotNull(now, "Now cannot be null");
    if (statementDate == null)
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.dropping_date)));

    if (statementDate.isAfter(now))
      throw new ConstraintViolation(context.getString(R.string.validation_s_date_cannot_be_future,
          context.getString(R.string.statement_date)));
  }

  public static void validateDroppingDateBefore(Context context, LocalDate now, LocalDate statementDate)
      throws ConstraintViolation {
    Preconditions.checkNotNull(now, "Now cannot be null");
    if (statementDate == null)
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.dropping_date)));

    if (statementDate.isBefore(now))
      throw new ConstraintViolation(context.getString(R.string.validation_s_date_cannot_be_before_request_date,
          context.getString(R.string.statement_date)));
  }

  public static void validateDescription(Context context, String description)
      throws ConstraintViolation {
    if (TextUtils.isEmpty(description))
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.dropping_detail_remarks)));
  }

  public static void validateAmount(Context context, Long amount) throws ConstraintViolation {
    if (amount == null)
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.dropping_additional_amount)));

    if (amount <= 0)
      throw new ConstraintViolation(context.getString(R.string.validation_s_positive,
          context.getString(R.string.dropping_additional_amount)));
  }

  public static void validateTotalAmount(Context context,Long amount) throws ConstraintViolation{
    if (amount == null)
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.dropping_additional_amount)));

    if (amount <= 0)
      throw new ConstraintViolation(context.getString(R.string.validation_s_positive,
          context.getString(R.string.dropping_additional_amount)));

    if (!(amount % TOTAL_REQUEST_AMOUNT_ROUNDED_TO == 0))
      throw new ConstraintViolation(context.getString(R.string.validation_s_multiple_of_s,
          context.getString(R.string.dropping_additional_amount),
          Formatter.DF_AMOUNT_NO_DECIMAL.format(TOTAL_REQUEST_AMOUNT_ROUNDED_TO)));
  }

  public static void validateApprovedAmount(Context context, Long netAmount, Long approvedAmount)
      throws ConstraintViolation {
    Preconditions.checkNotNull(netAmount);
    if (approvedAmount == null)
      throw new ConstraintViolation(context.getString(R.string.validation_s_mandatory,
          context.getString(R.string.dropping_additional_approved_amount)));

    if (approvedAmount <= 0)
      throw new ConstraintViolation(context.getString(R.string.validation_s_positive,
          context.getString(R.string.dropping_additional_approved_amount)));

    if (approvedAmount > netAmount)
      throw new ConstraintViolation(context.getString(R.string.validation_s_more_than_s,
          context.getString(R.string.dropping_additional_approved_amount),
          context.getString(R.string.dropping_additional_rounded_amount)));
  }
}
