package id.co.danwinciptaniaga.npmdsandroid.ui;

import com.hypertrack.hyperlog.HyperLog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.NetworkStateItemBinding;

public class NetworkStateItemViewHolder extends RecyclerView.ViewHolder {
  private static final String TAG = NetworkStateItemBinding.class.getSimpleName();
  protected final NetworkStateItemBinding networkStateItemBinding;
  protected final ProgressBar progressBar;
  protected final TextView errorMsg;
  protected final Button retry;
  protected View.OnClickListener retryCallback = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // dummy do nothing
      HyperLog.d(TAG, "Dummy is called?");
    }
  };

  public NetworkStateItemViewHolder(ViewGroup parent, View.OnClickListener retryCallback) {
    super(LayoutInflater.from(parent.getContext()).inflate(R.layout.network_state_item,
        parent, false));
    networkStateItemBinding = NetworkStateItemBinding.bind(super.itemView);
    progressBar = networkStateItemBinding.progressBar;
    errorMsg = networkStateItemBinding.errorMsg;
    retry = networkStateItemBinding.retryButton;
    this.retryCallback = retryCallback;
  }

  public void bindTo(LoadState loadState) {
    HyperLog.d(TAG, "bindTo is called for " + loadState);
    progressBar.setVisibility(loadState instanceof LoadState.Loading ? View.VISIBLE : View.GONE);
    if (loadState instanceof LoadState.Error) {
      retry.setVisibility(View.VISIBLE);
      errorMsg.setVisibility(View.VISIBLE);
      LoadState.Error error = (LoadState.Error) loadState;
      errorMsg.setText(error.getError() != null ? error.getError().getMessage() : null);
      retry.setOnClickListener(this.retryCallback);
    } else {
      retry.setVisibility(View.GONE);
      errorMsg.setVisibility(View.GONE);
      errorMsg.setText(null);
    }
  }
}
