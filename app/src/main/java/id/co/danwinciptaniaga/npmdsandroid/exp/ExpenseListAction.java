package id.co.danwinciptaniaga.npmdsandroid.exp;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;

public class ExpenseListAction
    extends BaseObservable<ExpenseListAction.Listener, HashMap<String, List<String>>> {

  private static final String TAG = ExpenseListAction.class.getSimpleName();
  private final ExpenseService expenseService;
  private final AppExecutors appExecutors;

  @Inject
  public ExpenseListAction(ExpenseService expenseService, AppExecutors appExecutors,
      Application application) {
    super(application);
    this.expenseService = expenseService;
    this.appExecutors = appExecutors;
  }

  public void deleteExpense(List<UUID> ids) {
    notifyStart(null, null);
    ListenableFuture<HashMap<String, List<String>>> deleteLf = expenseService.deleteExpense(ids);
    Futures.addCallback(deleteLf, new FutureCallback<HashMap<String, List<String>>>() {
      @Override
      public void onSuccess(@NullableDecl HashMap<String, List<String>> result) {
        HyperLog.d(TAG, "onSuccess: result->" + result + "<-");
        for (java.util.Map.Entry<String, List<String>> res : result.entrySet()) {
          if (res.getKey().equals("s")) {
            notifySuccess(ids.size() + " Biaya Operasional berhasil dihapus", result);
          }else if(res.getKey().equals("r")){
            List<String> rejectedList = res.getValue();
            String message = rejectedList.size()+" Gagal terhapus : "+android.text.TextUtils.join(",", rejectedList);
            notifySuccess(message, result);
          }
        }

      }

      @Override
      public void onFailure(Throwable t) {
        HyperLog.exception(TAG, "onFailure: t->" + t + "<-", t);
        notifyFailure(ids.size() + " Biaya Operasional gagal dihapus", null, t);
      }
    }, appExecutors.networkIO());
  }

  public void rejectExpense(List<UUID> ids) {
    notifyStart(null, null);
    ListenableFuture<HashMap<String, List<String>>> deleteLf = expenseService.rejectExpense(ids);
    Futures.addCallback(deleteLf, new FutureCallback<HashMap<String, List<String>>>() {
      @Override
      public void onSuccess(@NullableDecl HashMap<String, List<String>> result) {
        HyperLog.d(TAG, "onSuccess: result->" + result + "<-");
        for (java.util.Map.Entry<String, List<String>> res : result.entrySet()) {
          if (res.getKey().equals("s")) {
            notifySuccess(ids.size() + " Biaya Operasional berhasil direject", result);
          }else if(res.getKey().equals("r")){
            List<String> rejectedList = res.getValue();
            String message = rejectedList.size()+" Gagal direject : "+android.text.TextUtils.join(",", rejectedList);
            notifySuccess(message, result);
          }
        }

      }

      @Override
      public void onFailure(Throwable t) {
        HyperLog.e(TAG, "onFailure: t->" + t + "<-", t);
        notifyFailure(ids.size() + " Biaya Operasional gagal di-reject", null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<HashMap<String, List<String>>> result) {
    HyperLog.d(TAG, "doNotifyStart() terpanggil");
    listener.onExpenseActionProcesseStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener,
      Resource<HashMap<String, List<String>>> result) {
    HyperLog.d(TAG, "doNotifySuccess: terpanggil");
    listener.onExpenseActionProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener,
      Resource<HashMap<String, List<String>>> result) {
    HyperLog.d(TAG, "doNotifyFailure: terpanggil");
    listener.onExpenseActionProcessFailure(result);
  }

  public interface Listener {
    void onExpenseActionProcesseStarted(Resource<HashMap<String, List<String>>> loading);

    void onExpenseActionProcessSuccess(Resource<HashMap<String, List<String>>> response);

    void onExpenseActionProcessFailure(Resource<HashMap<String, List<String>>> response);
  }

}
