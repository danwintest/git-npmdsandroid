package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.time.LocalDate;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceReportFilter;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletStockByDayReportResponse;

public class GetOutletStockByDayReportUC extends
    BaseObservable<GetOutletStockByDayReportUC.Listener, OutletStockByDayReportResponse> {
  private static final String TAG = GetOutletStockByDayReportUC.class.getSimpleName();

  public interface Listener {

    void onGetStockByDayReportStarted();

    void onGetStockByDayReportSuccess(Resource<OutletStockByDayReportResponse> result);

    void onGetStockByDayReportFailure(Resource<OutletStockByDayReportResponse> responseError);

  }

  private final OutletReportService outletReportService;

  private final AppExecutors appExecutors;
  private OutletBalanceReportFilter filter = new OutletBalanceReportFilter();

  @Inject
  public GetOutletStockByDayReportUC(Application application,
      OutletReportService outletReportService, AppExecutors appExecutors) {
    super(application);
    this.outletReportService = outletReportService;
    this.appExecutors = appExecutors;
    LocalDate fromDate = LocalDate.now().withDayOfMonth(1);
    LocalDate toDate = LocalDate.now();
    filter.setStatementDateFrom(fromDate);
    filter.setStatementDateTo(toDate);
  }

  public ListenableFuture<OutletStockByDayReportResponse> getOutletStockByDayReport(UUID outletId) {
    notifyStart(null, null);

    ListenableFuture<OutletStockByDayReportResponse> lf = outletReportService.getOutletStockByDayReport(
        outletId, filter);
    Futures.addCallback(lf, new FutureCallback<OutletStockByDayReportResponse>() {
      @Override
      public void onSuccess(@NullableDecl OutletStockByDayReportResponse result) {
        notifySuccess("Laporan Materai Outlet berhasil dimuat", result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = RetrofitUtility.parseErrorBody(t);
        notifyFailure(er == null ? "Gagal memuat Laporan Materai Outlet" : er.getFormattedMessage(),
            er, t);
      }
    }, appExecutors.backgroundIO());
    return lf;
  }

  public void setStatementDateFrom(LocalDate dateFrom) {
    filter.setStatementDateFrom(dateFrom);
  }

  public void setStatementDateTo(LocalDate dateTo) {
    filter.setStatementDateTo(dateTo);
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<OutletStockByDayReportResponse> result) {
    listener.onGetStockByDayReportStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener,
      Resource<OutletStockByDayReportResponse> result) {
    listener.onGetStockByDayReportSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener,
      Resource<OutletStockByDayReportResponse> result) {
    listener.onGetStockByDayReportFailure(result);
  }
}
