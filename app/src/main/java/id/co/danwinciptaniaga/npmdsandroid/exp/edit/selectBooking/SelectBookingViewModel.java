package id.co.danwinciptaniaga.npmdsandroid.exp.edit.selectBooking;

import java.util.UUID;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class SelectBookingViewModel extends AndroidViewModel {
  private static final String TAG = SelectBookingViewModel.class.getSimpleName();
  private final GetBookingMediatorListUseCase ucGetBookingMediatorList;

  private UUID outletId;
  private LiveData<PagingData<BookingData>> bookingList;
  private MutableLiveData<Boolean> refreshList = new SingleLiveEvent<>();

  @ViewModelInject
  public SelectBookingViewModel(@NonNull Application application, AppExecutors appExecutors,
      GetBookingMediatorListUseCase getBookingMediatorListUseCase) {
    super(application);

    ucGetBookingMediatorList = getBookingMediatorListUseCase;

    CoroutineScope viewModelScope = ViewModelKt.getViewModelScope(this);
    Pager<Integer, BookingData> pager = new Pager<Integer, BookingData>(
        new PagingConfig(Utility.PAGE_SIZE),
        () -> new SelectBookingListPagingSource(ucGetBookingMediatorList, appExecutors.networkIO())
    );

    bookingList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pager), viewModelScope);
  }

  public void setOutletId(UUID outletId) {
    this.outletId = outletId;
    ucGetBookingMediatorList.setOutletId(outletId);
  }

  public LiveData<PagingData<BookingData>> getBookingList() {
    return bookingList;
  }

  public LiveData<Boolean> getRefreshList() {
    return refreshList;
  }

  public void setRefreshList(boolean refreshList) {
    this.refreshList.postValue(refreshList);
  }
}
