package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.ListActionResponse;
import id.co.danwinciptaniaga.npmds.data.ListResponse;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.ListWrapper;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalDataResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilterResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseSort;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDetailDTO;
import id.co.danwinciptaniaga.npmds.data.dropping.NewDroppingAdditionalResponse;
import id.co.danwinciptaniaga.npmds.data.wf.WfHistoryResponse;
import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DroppingAdditionalService {
  public static final String PARAM_ATTACHMENT_PREFIX = "att";

  @GET("rest/s/droppingAdditional")
  ListenableFuture<ListResponse<DroppingAdditionalData>> getDroppingAdditionalList(
      @Query("page") Integer page,
      @Query("pageSize") Integer pageSize,
      @Query("filter") DroppingBrowseFilter filter,
      @Query("sort") DroppingBrowseSort sort);

  @GET("rest/s/droppingAdditional/getFilterField")
  ListenableFuture<DroppingBrowseFilterResponse> getDroppingFilterField();

  @GET("rest/s/droppingAdditional/new")
  ListenableFuture<NewDroppingAdditionalResponse> getNewDroppingAdditional();

  @Multipart
  @POST("rest/s/droppingAdditional/saveDraft")
  ListenableFuture<DroppingAdditionalData> saveDroppingAdditionalDraft(@Part("id") UUID droppingId,
      @Part("outletId") UUID outletId, @Part("requestDate") LocalDate requestDate,
      @Part("droppingDate") LocalDate droppingDate,
      @Part("droppingDetails") ListWrapper<DroppingDetailDTO> droppingDetailWrapper,
      @Part MultipartBody.Part[] attachments,
      @Part("checkTs") Date checkTs);

  @Multipart
  @POST("rest/s/droppingAdditional/submit")
  ListenableFuture<DroppingAdditionalData> submitDroppingAdditional(@Part("id") UUID droppingId,
      @Part("outletId") UUID outletId, @Part("requestDate") LocalDate requestDate,
      @Part("droppingDate") LocalDate droppingDate,
      @Part("droppingDetails") ListWrapper<DroppingDetailDTO> droppingDetailWrapper,
      @Part MultipartBody.Part[] attachments,
      @Part("checkTs") Date checkTs);

  @Multipart
  @POST("rest/s/droppingAdditional/{id}/submitRevision")
  ListenableFuture<DroppingAdditionalData> submitDroppingAdditionalRevision(
      @Part("procTaskId") UUID procTaskId, @Part("comment") String comment,
      @Path("id") UUID droppingId, @Part("outletId") UUID outletId,
      @Part("requestDate") LocalDate requestDate,
      @Part("droppingDate") LocalDate droppingDate,
      @Part("droppingDetails") ListWrapper<DroppingDetailDTO> droppingDetailsWrapper,
      @Part MultipartBody.Part[] attachmentParts, @Part("checkTs") Date checkTs);

  @GET("rest/s/droppingAdditional/{id}")
  ListenableFuture<DroppingAdditionalDataResponse> loadDropping(@Path("id") UUID droppingId);

  @FormUrlEncoded
  @POST("rest/s/droppingAdditional/{id}/docApprove")
  ListenableFuture<DroppingAdditionalData> docApprove(@Field("procTaskId") UUID procTaskId,
      @Field("comment") String comment,
      @Path("id") UUID droppingId, @Field("batchCode") String batchCode,
      @Field("approvedAmount") long approvedAmount,
      @Field("altAccount") boolean altAccount,
      @Field("bankAccountData") BankAccountData bankAccountData,
      @Field("opTransfer") Boolean opTransfer,
      @Field("checkTs") Date checkTs);

  @FormUrlEncoded
  @POST("rest/s/droppingAdditional/{id}/captainApprove")
  ListenableFuture<DroppingAdditionalData> captainApprove(@Field("procTaskId") UUID procTaskId,
      @Field("comment") String comment,
      @Path("id") UUID droppingId, @Field("batchCode") String batchCode,
      @Field("approvedAmount") long approvedAmount,
      @Field("altAccount") boolean altAccount,
      @Field("bankAccountData") BankAccountData bankAccountData,
      @Field("opTransfer") Boolean opTransfer,
      @Field("checkTs") Date checkTs);

  @Multipart
  @POST("rest/s/droppingAdditional/finStaffApprove")
  ListenableFuture<DroppingAdditionalData> finStaffApprove(@Query("procTaskId") UUID procTaskId,
      @Query("comment") String comment,
      @Query("id") UUID droppingId,
      @Part("transferDate") LocalDate transferDate,
      @Query("transferType") String transferType,
      @Query("sourceAccountId") UUID sourceAccountId,
      @Part("checkTs") Date lCheckTs);

  @FormUrlEncoded
  @POST("rest/s/droppingAdditional/{id}/finSpvApprove")
  ListenableFuture<DroppingAdditionalData> finSpvApprove(@Field("procTaskId") UUID procTaskId,
      @Field("comment") String comment,
      @Path("id") UUID droppingId,
      @Field("otpCode") String otpCode,
      @Field("transferType") String transferType,
      @Field("checkTs") Date lCheckTs);

  @FormUrlEncoded
  @POST("rest/s/droppingAdditional/{id}/generateOtp")
  ListenableFuture<DroppingAdditionalData> generateOtp(@Field("procTaskId") UUID procTaskId,
      @Path("id") UUID droppingId);

  @FormUrlEncoded
  @POST("rest/s/droppingAdditional/{id}/rejectOrReturn")
  ListenableFuture<DroppingAdditionalData> rejectOrReturn(@Field("procTaskId") UUID procTaskId,
      @Field("outcome") String outcome, @Field("comment") String comment,
      @Path("id") UUID droppingId, @Field("checkTs") Date checkTs);

  @GET("rest/s/droppingAdditional/{id}/wfHistory")
  ListenableFuture<WfHistoryResponse> getWfHistory(@Path("id") UUID entityId);

  @POST("rest/s/droppingAdditional/delete")
  ListenableFuture<ListActionResponse<String, String>> deleteDropping(@Query("ids") List<UUID> ids);
}
