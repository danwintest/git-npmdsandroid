package id.co.danwinciptaniaga.npmdsandroid.ui;

import java.time.LocalDate;
import java.util.function.Consumer;
import java.util.function.Supplier;

import android.content.Context;
import android.widget.EditText;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

public class DatePickerUIHelper {
  public static void bindDate(EditText editText, LocalDate date) {
    if (date != null) {
      editText.setText(date.format(Formatter.DTF_dd_MM_yyyy));
    } else {
      editText.setText(null);
    }
    editText.setTag(date);
  }

  public static void setupEditTextDateField(Context context, EditText editText,
      Supplier<LocalDate> dateSupplier) {
    setupEditTextDateField(context, editText, dateSupplier, true);
  }

  public static void setupEditTextDateField(Context context, EditText editText,
      Supplier<LocalDate> dateSupplier, boolean cancelClearField) {
    editText.setOnFocusChangeListener((view, isFocused) -> {
      if (view.isInTouchMode() && isFocused)
        view.performClick();
    });
    editText.setOnClickListener(new DatePickerDialogListener(context,
        dateSupplier,
        e -> DatePickerUIHelper.bindDate(editText, e), cancelClearField));
    DatePickerUIHelper.bindDate(editText, dateSupplier.get());
  }
}
