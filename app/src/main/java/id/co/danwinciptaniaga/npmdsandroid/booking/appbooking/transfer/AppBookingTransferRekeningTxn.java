package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.snackbar.Snackbar;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentAppBookingTransferRekeningTxnBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class AppBookingTransferRekeningTxn extends BottomSheetDialogFragment {
  private static final String TAG = AppBookingTransferRekeningTxn.class.getSimpleName();

  private FragmentAppBookingTransferRekeningTxnBinding binding;
  private AppBookingTransferVM vm;
  private String mode;

  @NonNull
  @Override
  public Dialog onCreateDialog(@androidx.annotation.Nullable Bundle savedInstanceState) {
    Dialog dialog = super.onCreateDialog(savedInstanceState);
    dialog.setOnShowListener(new DialogInterface.OnShowListener() {
      @Override
      public void onShow(DialogInterface dialogInterface) {
        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) dialogInterface;
        setupFullHeight(bottomSheetDialog);
      }
    });
    return dialog;
  }

  private void setupFullHeight(BottomSheetDialog bottomSheetDialog) {
    FrameLayout bottomSheet = (FrameLayout) bottomSheetDialog.findViewById(
        R.id.design_bottom_sheet);
    BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
    ViewGroup.LayoutParams layoutParams = bottomSheet.getLayoutParams();

    int windowHeight = getWindowHeight();
    if (layoutParams != null) {
      layoutParams.height = windowHeight;
    }
    bottomSheet.setLayoutParams(layoutParams);
    behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
  }

  private int getWindowHeight() {
    // Calculate window height for fullscreen use
    DisplayMetrics displayMetrics = new DisplayMetrics();
    requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    return displayMetrics.heightPixels;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentAppBookingTransferRekeningTxnBinding.inflate(inflater, container, false);

//    mode = getArguments().getString(AppBookingTransferRekeningForm.MODE);

//    vm = (AppBookingTransferVM) getArguments().getSerializable(AppBookingTransferRekeningForm.VM);
    initForm();
    return binding.getRoot();
  }

  private void initForm() {
    setTitle();
    setButtonClose();
    setScreenMode();
    setButtonProcessListener();
  }

  private void setTitle() {
    String title = "-";
    switch (mode) {
    case AppBookingTransferVM.TXN_CONSUMER:
      title = getString(R.string.title_mode_kerja_consumer);
      break;
    case AppBookingTransferVM.TXN_BIRO_JASA:
      title = getString(R.string.title_mode_kerja_biro_jasa);
      break;
    case AppBookingTransferVM.TXN_FIF:
      title = getString(R.string.title_mode_kerja_finance);
      break;
    default:
      //TODO
      break;
    }
    binding.tvTitle.setText(title);
  }

  protected void setScreenMode() {
    binding.btnSubmitRevision.setVisibility(View.GONE);
    vm.getTxnScreenMode().observe(getViewLifecycleOwner(),screenMode->{
      switch (screenMode) {
      case AOC_CUS_VERIFICATION_NEW:
        //        BTN APPRV = 1
        //        BTN RETURN = 1
        //        BTN REJECT = JIKA  OP = EDIT? 1:0
        //        BTN SUBMIT = 0
        break;
      case CAPTAIN_CUS_VERIFICATION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_STAFF_CUS_VERIFICATION_NEW:
        binding.btnApprove.setVisibility(View.VISIBLE);
        binding.btnRelease.setVisibility(View.GONE);
        binding.btnReturn.setVisibility(View.VISIBLE);
        if(vm.getField_AppOperation().equals(Utility.OPERATION_EDIT))
          binding.btnReject.setVisibility(View.VISIBLE);
        else
          binding.btnReject.setVisibility(View.GONE);
        break;
      case FIN_SPV_CUS_VERIFICATION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case KASIR_CUS_REVISION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case AOC_JASA_VERIFICATION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case CAPTAIN_JASA_VERIFICATION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_STAFF_JASA_VERIFICATION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_SPV_JASA_VERIFICATION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case KASIR_JASA_REVISION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case AOC_FIF_VERIFICATION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case CAPTAIN_FIF_VERIFICATION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_STAFF_FIF_VERIFICATION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_SPV_FIF_VERIFICATION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case KASIR_FIF_REVISION_NEW:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case AOC_CUS_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case CAPTAIN_CUS_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case DIRECTOR_CUS_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_STAFF_CUS_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_SPV_CUS_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_RETUR_CUS_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case KASIR_CUS_REVISION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case AOC_JASA_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case CAPTAIN_JASA_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case DIRECTOR_JASA_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_STAFF_JASA_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_SPV_JASA_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_RETUR_JASA_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case KASIR_JASA_REVISION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case AOC_FIF_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case CAPTAIN_FIF_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case DIRECTOR_FIF_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_STAFF_FIF_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_SPV_FIF_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case FIN_RETUR_FIF_VERIFICATION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      case KASIR_FIF_REVISION_EDIT:
        //TODO BELUM ADA IMPLEMENTASI
        break;
      default:
        readOnlyScreen();
        break;
      }
    });

  }

  private void readOnlyScreen(){
    binding.tilTransferDate.setEnabled(false);
    binding.tilSourceBank.setEnabled(false);
    binding.btnApprove.setVisibility(View.GONE);
    binding.btnReturn.setVisibility(View.GONE);
    binding.btnRelease.setVisibility(View.GONE);
    binding.btnReject.setVisibility(View.GONE);
    binding.btnSubmitRevision.setVisibility(View.GONE);
  }

  private void setButtonProcessListener() {
    binding.btnApprove.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //TODO BELUM ADA IMPLEMENTASI
      }
    });

    binding.btnReturn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //TODO BELUM ADA IMPLEMENTASI
      }
    });

    binding.btnRelease.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //TODO BELUM ADA IMPLEMENTASI
      }
    });

    binding.btnSubmitRevision.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //TODO BELUM ADA IMPLEMENTASI
      }
    });

  }

  private void setButtonClose() {
    binding.ivClose.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dismiss();
      }
    });
  }

  @Override
  public void onDismiss(@NonNull DialogInterface dialog) {
    super.onDismiss(dialog);
    switch (mode) {
    case AppBookingTransferVM.TXN_CONSUMER:
      vm.setConsumerWfDialogShow(false);
      break;
    case AppBookingTransferVM.TXN_BIRO_JASA:
      vm.setBiroJasaWfDialogShow(false);
      break;
    case AppBookingTransferVM.TXN_FIF:
      vm.setFIFWfDialogShow(false);
      break;
    default:
      Snackbar.make(getView(), "Mode tidak didukung", Snackbar.LENGTH_LONG).show();
      HyperLog.e(TAG, "onDismis mode tidak diketahui->" + mode + "<-");
      break;
    }
  }
}