package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiDroppingDayTaskWfBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class DroppingDailyTaskWFDataAdapter extends
    RecyclerView.Adapter<DroppingDailyTaskWFDataAdapter.ViewHolder> {
//    private final WfItemListener mListener;
    private List<DroppingDailyTaskBrowseDetailData> dataList = new ArrayList<>();
    private Context ctx;
    private int totalError = 0;
//    private AppExecutors appExecutors;

  @Inject
  public DroppingDailyTaskWFDataAdapter(Context ctx,
//      List<DroppingDailyTaskBrowseDetailData> dataList, WfItemListener listener) {
    List<DroppingDailyTaskBrowseDetailData> dataList) {
//      super(new DiffCallBack());
      this.dataList = dataList;
//      this.mListener = listener;
      this.ctx = ctx;
//      this.appExecutors = appExecutors;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
      private final LiDroppingDayTaskWfBinding binding;

      public ViewHolder(View v) {
        super(v);
        binding = LiDroppingDayTaskWfBinding.bind(v);
      }

      public void bind(DroppingDailyTaskBrowseDetailData data, int pos) {
        setObject(data);
        setObjectListener(data, pos);
      }

      private void setObject(DroppingDailyTaskBrowseDetailData data) {
        binding.tvTrxNo.setText(data.getTrxNo());
        binding.tvOutletAndCode.setText(data.getOutletName() + " - " + data.getOutletCode());
        binding.tvDroppingDateVal.setText(Formatter.SDF_dd_MM_yyyy.format(data.getRequestDate()));
        binding.tvPlafonVal.setText(Utility.getFormattedAmt(data.getPlafonAmt()));
        binding.tvCurrentBalanceVal.setText(Utility.getFormattedAmt(data.getCurrentBalanceAmt()));
        binding.tvBalanceVal.setText(Utility.getFormattedAmt(data.getBalanceAmt()));
        binding.etDroppingAmtVal.setText(data.getDroppingAmt().toString());

        binding.tvKeputusan.setText(data.getOutcome() != null ? data.getOutcome() : "");
        binding.tvAccInfo.setText(data.getAccName() + " - " + data.getAccNo());
      }

      private void setObjectListener(DroppingDailyTaskBrowseDetailData data, int pos) {

        binding.etDroppingAmtVal.addTextChangedListener(new TextWatcher() {
          @Override
          public void beforeTextChanged(CharSequence s, int start, int count, int after) {

          }

          @Override
          public void onTextChanged(CharSequence s, int start, int before, int count) {

          }

          @Override
          public void afterTextChanged(Editable s) {
//            mListener.onDroppingAmtChange(s.toString(), pos);
            Long amt = null;
            if (s.length() > 0)
              amt = Long.parseLong(s.toString());

            DroppingDailyTaskBrowseDetailData data = dataList.get(pos);
            data.setDroppingAmt(amt);
            dataList.set(pos, data);
          }
        });
      }
    }

    interface WfItemListener {
      void onDroppingAmtChange(String amtString, int pos);
    }

    @Override
    public DroppingDailyTaskWFDataAdapter.ViewHolder onCreateViewHolder(@Nonnull ViewGroup parent,
        int viewType) {
      View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.li_dropping_day_task_wf,parent,false);
      return new DroppingDailyTaskWFDataAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DroppingDailyTaskWFDataAdapter.ViewHolder holder,
        int position) {
      holder.bind(getItem(position), position);
    }

  @Override
  public int getItemCount() {
    return dataList.size();
  }


  public List<DroppingDailyTaskBrowseDetailData> getDataList(){
    return this.dataList;
  }

  DroppingDailyTaskBrowseDetailData getItem(int pos) {
    return dataList.get(pos);
  }

}
