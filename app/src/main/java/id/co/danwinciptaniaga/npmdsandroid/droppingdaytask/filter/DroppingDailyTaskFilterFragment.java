package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter;

import java.time.LocalDate;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentDroppingDailyTaskFilterBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

@AndroidEntryPoint
public class DroppingDailyTaskFilterFragment extends BottomSheetDialogFragment {
  private static final String TAG = DroppingDailyTaskFilterFragment.class.getSimpleName();
  public static final String DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_GROUPING = "DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_GROUPING";
  public static final String DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_DETAIL = "DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_DETAIL";
  private FragmentDroppingDailyTaskFilterBinding binding;
  protected ArrayAdapter<KeyValueModel<UUID, String>> companyAdapter;
  private DroppingDailyTaskFilterVM vm;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    HyperLog.d(TAG, "onCreateView terpanggil");
    binding = FragmentDroppingDailyTaskFilterBinding.inflate(inflater);
    DroppingDailyTaskFilterFragmentArgs args = DroppingDailyTaskFilterFragmentArgs.fromBundle(
        getArguments());
    vm = new ViewModelProvider(this).get(DroppingDailyTaskFilterVM.class);
    vm.setFilterField(args.getFilterField());
    vm.setGroup(args.getIsGrouping());
    return binding.getRoot();
  }

  @Override
  public void onResume() {
    super.onResume();
    setFormMode();
    vm.processLoadCompanyList();
    setFieldCompanyList();
    setFieldOutletName();
    setFieldRequestDate();
    setButton();
  }

  private void setFormMode() {
    binding.spCompany.setFocusable(false);
    vm.isGroup().observe(getViewLifecycleOwner(), isGroup -> {
      if (isGroup) {
        binding.tvCompany.setVisibility(View.VISIBLE);
        binding.tilCompany.setVisibility(View.VISIBLE);
        binding.spCompany.setEnabled(true);
        binding.tvOutlet.setVisibility(View.GONE);
        binding.tilOutlet.setVisibility(View.GONE);
        binding.tvDroppingDate.setVisibility(View.VISIBLE);
        binding.tilDroppingDate.setVisibility(View.VISIBLE);
      } else {
        binding.tvCompany.setVisibility(View.GONE);
        binding.tilCompany.setVisibility(View.GONE);
        binding.spCompany.setEnabled(false);
        binding.tvOutlet.setVisibility(View.VISIBLE);
        binding.tilOutlet.setVisibility(View.VISIBLE);
        binding.tvDroppingDate.setVisibility(View.GONE);
        binding.tilDroppingDate.setVisibility(View.GONE);
      }
    });
  }

  private void setFieldCompanyList() {
    vm.getCompanyKvmList().observe(getViewLifecycleOwner(), dataList -> {
      companyAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spCompany.setAdapter(companyAdapter);
      binding.spCompany.setFocusable(false);
      companyAdapter.addAll(dataList);
      companyAdapter.notifyDataSetChanged();

      int position = companyAdapter.getPosition(vm.getCompanyKvm());
      binding.spCompany.setSelection(position);

      binding.spCompany.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
            int i, long l) {
          vm.setCompanyId(companyAdapter.getItem(i).getKey(), companyAdapter.getItem(i).getValue());
        }

        @Override
        public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
          //TODO belum ada implementasi
        }
      });
    });

  }

  private void setFieldOutletName() {
    binding.etOutlet.setText(vm.getFilterField().getOutletName());
    binding.etOutlet.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        vm.setOutletName(s.toString());
      }
    });
  }

  private void setFieldRequestDate() {
    if (vm.getFilterField().getRequestDate() != null) {
      binding.etDroppingDate.setText(
          vm.getFilterField().getRequestDate().format(Formatter.DTF_dd_MM_yyyy));
    }

    binding.etDroppingDate.setOnClickListener(v -> {
      LocalDate dateVal = vm.getFilterField().getRequestDate() != null ?
          vm.getFilterField().getRequestDate() : LocalDate.now();

      int day = dateVal.getDayOfMonth();
      int month = dateVal.getMonthValue() - 1;
      int year = dateVal.getYear();

      DatePickerDialog dpd = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate dateVal = LocalDate.of(year, month + 1, dayOfMonth);
              vm.setRequestDate(dateVal);
              binding.etDroppingDate.setText(dateVal.format(Formatter.DTF_dd_MM_yyyy));
            }
          }, year, month, day);
      dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          vm.setRequestDate(null);
          binding.etDroppingDate.setText("");
        }
      });
      dpd.show();
    });
  }

  private void setButton() {
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String key = vm.isGroup().getValue() ?
            DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_GROUPING :
            DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_DETAIL;
        NavController nc = NavHostFragment.findNavController(DroppingDailyTaskFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(key, vm.getFilterField());
        nc.popBackStack();
      }
    });

    binding.btnClearAll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        NavController nc = NavHostFragment.findNavController(DroppingDailyTaskFilterFragment.this);
        DroppingDailyTaskBrowseFilter filter = vm.getFilterField();
        String key = DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_DETAIL;
        if (vm.isGroup().getValue()) {
          filter.setRequestDate(null);
          filter.setBankId(null);
          filter.setAltAccount(null);
          key = DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_GROUPING;
        }
        filter.setOutletName(null);
        //        filter.setCompanyId(vm.defaultFilterCompanyId);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(key, filter);
        nc.popBackStack();
      }
    });
  }
}