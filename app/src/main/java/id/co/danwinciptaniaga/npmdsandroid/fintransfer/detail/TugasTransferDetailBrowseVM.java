package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.LoadState;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalScreenMode;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyScreenMode;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferDetailBrowseSort;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class TugasTransferDetailBrowseVM extends AndroidViewModel {
  private final String TAG = TugasTransferDetailBrowseVM.class.getSimpleName();
  private LiveData<PagingData<TugasTransferBrowseDetailData>> detailList;
  private final GetTugasTransferDetailBrowseUC uc;
  private final Pager<Integer, TugasTransferBrowseDetailData> pagerDetail;
  private MutableLiveData<LoadState> loadStateDetail = new MutableLiveData<>();
  private MutableLiveData<Boolean> refreshDetailList = new SingleLiveEvent<>();
  private MutableLiveData<Resource<String>> actionEventDetail = new SingleLiveEvent<>();
  private MutableLiveData<TugasTransferBrowseAndroidFilter> filterField = new MutableLiveData<>();
  private Set<SortOrder> sortField = new LinkedHashSet<>();
  private final AppExecutors appExecutors;

  private MutableLiveData<Boolean> isAllowTransferAutomaticBtn = new MutableLiveData<>(false);
  private MutableLiveData<Boolean> isAllowTransferManualBtn = new MutableLiveData<>(false);
  private MutableLiveData<Boolean> isAllowReleaseBtn = new MutableLiveData<>(false);
  private MutableLiveData<Boolean> isAllowReJectBtn = new MutableLiveData<>(false);
  private MutableLiveData<Boolean> isAllowReturnBtn = new MutableLiveData<>(false);
  private List<TugasTransferBrowseDetailData> selectedCandidateData = new ArrayList<>();

  @ViewModelInject
  public TugasTransferDetailBrowseVM(
      @NonNull Application app, GetTugasTransferDetailBrowseUC uc, AppExecutors appExecutors) {
    super(app);
    this.uc = uc;
    this.appExecutors = appExecutors;
    this.sortField.add(TugasTransferDetailBrowseSort
        .sortBy(TugasTransferDetailBrowseSort.Field.TRANSFER_DATE, SortOrder.Direction.DESC));

    pagerDetail = new Pager<Integer, TugasTransferBrowseDetailData>(
        new PagingConfig(Utility.PAGE_SIZE),
        () -> new TugasTransferBrowseDetailPagingSource(this.uc,
            this.appExecutors.networkIO()));
    CoroutineScope vmScopeDetail = ViewModelKt.getViewModelScope(this);
    detailList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pagerDetail), vmScopeDetail);
  }

  public LiveData<PagingData<TugasTransferBrowseDetailData>> getDetailList() {
    HyperLog.d(TAG, "getDroppingDailyDetailList() terpanggil");
    if (detailList == null) {
      CoroutineScope vmScope = ViewModelKt.getViewModelScope(this);
      detailList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pagerDetail), vmScope);
    }
    return detailList;
  }

  public MutableLiveData<LoadState> getLoadStateDetail() {
    return loadStateDetail;
  }

  public void setLoadStateDetail(LoadState loadStateDetail) {
    this.loadStateDetail.postValue(loadStateDetail);
  }

  public LiveData<Boolean> getRefreshDetailList() {
    return refreshDetailList;
  }

  public LiveData<Resource<String>> getActionEventDetail() {
    return actionEventDetail;
  }

  public void setRefreshDetailList(Boolean refreshDetailList) {
    this.refreshDetailList.postValue(refreshDetailList);
  }

  public void setFitlerField(TugasTransferBrowseAndroidFilter filterField) {
    this.filterField.postValue(filterField);
  }

  public MutableLiveData<TugasTransferBrowseAndroidFilter> getFilterField() {
    return filterField;
  }


  public void setSelectedData(int selectedDataCount, TugasTransferBrowseDetailData data) {
    setSelectedCandidateData(data);
    if (selectedDataCount > 0) {
      setAllowTransferAutomaticManualBtn(true);
      setAllowTransferAutomaticManualBtn(false);
      setAllowReleaseBtn();
      setAllowRejectBtn();
      setAllowReturnBtn();
    } else {
      isAllowTransferAutomaticBtn.postValue(false);
      isAllowTransferManualBtn.postValue(false);
      isAllowReleaseBtn.postValue(false);
      isAllowReJectBtn.postValue(false);
      isAllowReturnBtn.postValue(false);
    }
  }

  public void clearSelectedCandidateData(){
    selectedCandidateData.clear();
  }

  public void setSelectedCandidateData(TugasTransferBrowseDetailData selectedData) {
    if(selectedCandidateData.contains(selectedData)){
      selectedCandidateData.remove(selectedData);
    }else{
      selectedCandidateData.add(selectedData);
    }

//    boolean contain = selectedCandidateData.containsKey(selectedData);
//    if (contain)
//      selectedCandidateData.remove(selectedData);
//    else
//      selectedCandidateData.put(selectedData, selectedData.getBankId());

//    Iterator i =selectedCandidateData.entrySet().iterator();
//    while (i.hasNext()){
//      Map.Entry e = (Map.Entry) i.next();
//      if(selectedData.equals(e.getKey())){
//        i.remove();
//      }
//    }
  }

  private void setAllowTransferAutomaticManualBtn(boolean isAuto) {
    boolean isAssign = true;
    boolean isBankId = true;
    boolean isEndDate = true;

    for (TugasTransferBrowseDetailData d : selectedCandidateData) {
      isAssign =
          Objects.equals(DroppingDailyScreenMode.FINANCE_STAFF_TASK_KEY, d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_CUS_TASK_KEY_NEW,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_CUS_TASK_KEY_EDIT,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_JASA_TASK_KEY_NEW,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_JASA_TASK_KEY_EDIT,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_FIF_TASK_KEY_NEW,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_FIF_TASK_KEY_EDIT,
                  d.getUserTaskId());
      if (!isAssign) {
        isAssign = false;
        break;
      }
      if (d.getEndDate() != null) {
        isEndDate = false;
        break;
      }

      if (isAuto) {
        if (d.getBankId() == null)
          isBankId = false;
        break;
      }
    }
    if (isAuto) {
      if (isAssign && isEndDate && isBankId)
        isAllowTransferAutomaticBtn.postValue(true);
      else
        isAllowTransferAutomaticBtn.postValue(false);
    }else{
      if (isAssign && isEndDate && isBankId)
        isAllowTransferManualBtn.postValue(true);
      else
        isAllowTransferManualBtn.postValue(false);
    }

  }

//  public List<String> getDecisionList(){
//    List<String> decisionList = new ArrayList<>();
//    if (isAllowTransferBtnVal) {
//      decisionList.add(Utility.WF_OUTCOME_APPROVE_TRANSFER_OTOMATIS);
//      decisionList.add(Utility.WF_OUTCOME_APPROVE_TRANSFER_MANUAL);
//    }
//    if (isAllowReleaseBtnVal)
//      decisionList.add(WorkflowConstants.WF_OUTCOME_APPROVE);
//
//    if (isAllowReJectBtnVal)
//      decisionList.add(WorkflowConstants.WF_OUTCOME_REJECT);
//    if (isAllowRerurnBtnVal)
//      decisionList.add(WorkflowConstants.WF_OUTCOME_RETURN);
//
//    return decisionList;
//  }

  public MutableLiveData<Boolean> getIsAllowTransferAutomaticBtn() {
    return isAllowTransferAutomaticBtn;
  }

  public MutableLiveData<Boolean> getIsAllowTransferManualBtn() {
    return isAllowTransferManualBtn;
  }

  private void setAllowReleaseBtn() {
    boolean isAssign = true;
    boolean isEndDate = true;
    for (TugasTransferBrowseDetailData d : selectedCandidateData) {
      isAssign =
          Objects.equals(DroppingAdditionalScreenMode.FINANCE_SPV_TASK_KEY, d.getUserTaskId()) ||
              Objects.equals(DroppingDailyScreenMode.FINANCE_SPV_TASK_KEY, d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_CUS_TASK_KEY_NEW,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_CUS_TASK_KEY_EDIT,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_JASA_TASK_KEY_NEW,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_JASA_TASK_KEY_EDIT,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_FIF_TASK_KEY_NEW,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_FIF_TASK_KEY_EDIT,
                  d.getUserTaskId());
      if (isAssign)
        break;
      if (d.getEndDate() != null) {
        isEndDate = false;
        break;
      }
    }
    isAllowReleaseBtn.postValue(isAssign && isEndDate);
  }

  public MutableLiveData<Boolean> getIsAllowReleaseBtn() {
    return isAllowReleaseBtn;
  }

  private void setAllowRejectBtn() {
    boolean isAssign = true;

    for (TugasTransferBrowseDetailData d : selectedCandidateData) {
      isAssign =
          Objects.equals(DroppingAdditionalScreenMode.FINANCE_SPV_TASK_KEY, d.getUserTaskId()) ||
              Objects.equals(DroppingDailyScreenMode.FINANCE_SPV_TASK_KEY, d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_CUS_TASK_KEY_NEW,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_CUS_TASK_KEY_EDIT,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_JASA_TASK_KEY_NEW,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_JASA_TASK_KEY_EDIT,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_FIF_TASK_KEY_NEW,
                  d.getUserTaskId()) ||
              Objects.equals(AppBookingTransferScreenMode.FIN_SPV_FIF_TASK_KEY_EDIT,
                  d.getUserTaskId());
      if(!isAssign)
        break;
    }
    isAllowReJectBtn.postValue(isAssign);
  }

  public MutableLiveData<Boolean> getIsAllowReJectBtn() {
    return isAllowReJectBtn;
  }

  private void setAllowReturnBtn() {
    boolean isAssignForFinStaff = true;
    boolean isAssignForFinSpv = true;
    for (TugasTransferBrowseDetailData d : selectedCandidateData) {

      if (!isAssignForFinSpv && !isAssignForFinStaff)
        break;

      if (isAssignForFinStaff) {
        isAssignForFinStaff =
            Objects.equals(DroppingAdditionalScreenMode.FINANCE_STAFF_TASK_KEY,
                d.getUserTaskId()) ||
                Objects.equals(DroppingDailyScreenMode.FINANCE_STAFF_TASK_KEY, d.getUserTaskId())
                ||
                Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_CUS_TASK_KEY_NEW,
                    d.getUserTaskId()) ||
                Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_CUS_TASK_KEY_EDIT,
                    d.getUserTaskId()) ||
                Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_JASA_TASK_KEY_NEW,
                    d.getUserTaskId()) ||
                Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_JASA_TASK_KEY_EDIT,
                    d.getUserTaskId()) ||
                Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_FIF_TASK_KEY_NEW,
                    d.getUserTaskId()) ||
                Objects.equals(AppBookingTransferScreenMode.FIN_STAFF_FIF_TASK_KEY_EDIT,
                    d.getUserTaskId());
      }
      if(isAssignForFinSpv) {
        isAssignForFinSpv =
            Objects.equals(DroppingAdditionalScreenMode.FINANCE_SPV_TASK_KEY, d.getUserTaskId())
                ||
                Objects.equals(DroppingDailyScreenMode.FINANCE_SPV_TASK_KEY, d.getUserTaskId())
                ||
                Objects.equals(AppBookingTransferScreenMode.FIN_SPV_CUS_TASK_KEY_NEW,
                    d.getUserTaskId()) ||
                Objects.equals(AppBookingTransferScreenMode.FIN_SPV_CUS_TASK_KEY_EDIT,
                    d.getUserTaskId()) ||
                Objects.equals(AppBookingTransferScreenMode.FIN_SPV_JASA_TASK_KEY_NEW,
                    d.getUserTaskId()) ||
                Objects.equals(AppBookingTransferScreenMode.FIN_SPV_JASA_TASK_KEY_EDIT,
                    d.getUserTaskId()) ||
                Objects.equals(AppBookingTransferScreenMode.FIN_SPV_FIF_TASK_KEY_NEW,
                    d.getUserTaskId()) ||
                Objects.equals(AppBookingTransferScreenMode.FIN_SPV_FIF_TASK_KEY_EDIT,
                    d.getUserTaskId());
      }
    }
    isAllowReturnBtn.postValue(isAssignForFinStaff || isAssignForFinSpv);
  }

  public MutableLiveData<Boolean> getIsAllowReturnBtn() {
    return isAllowReturnBtn;
  }

  public void loadDetailListWithFilter(TugasTransferBrowseAndroidFilter filterField) {
    setFitlerField(filterField);
    HyperLog.d(TAG, "loadDroppingDailyDetail() terpanggil "
        + "companyId[" + filterField.getCompanyId() + "] "
        + "bankId[" + filterField.getBankId() + "] "
        + "jenisTransfer[" + filterField.getTransferType() + "]"
        + "rekeningSumber[" + filterField.getSourceAccountId() + "]"
        + "pendingTaskStatus[" + filterField.getPendingTask() + "]"
        + "batch[" + filterField.getBatchId() + "] [" + filterField.getBatchName() + "]"
        + "transferStatus[" + filterField.getTransferStatusId() + "] ["
        + filterField.getTransferStatusName() + "]"
        + "rekeningSumber[" + filterField.getTransactionTypeId() + "] ["
        + filterField.getTransactionTypeName() + "]"
    );

    uc.setFilterField(filterField);
    refreshDetailList.postValue(true);
  }

  public void setSortFields(Set<SortOrder> sortFields) {
    this.sortField.clear();
    if (sortFields != null) {
      this.sortField.addAll(sortFields);
    }
    uc.setSort(sortFields);
    refreshDetailList.postValue(true);
  }

  public Set<SortOrder> getSortFields() {
    return this.sortField;
  }
}
