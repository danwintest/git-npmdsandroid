package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.util.Objects;

import javax.inject.Inject;

import android.content.Context;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiOutletBalanceBinding;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalListAdapter;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

public class OutletBalanceListAdapter extends
    PagingDataAdapter<OutletBalanceData, OutletBalanceListAdapter.ViewHolder> {
  private final static String TAG = DroppingAdditionalListAdapter.class.getSimpleName();
  private final OutletBalanceListAdapter.Listener mListener;

  private Context mContext;

  @Inject
  public OutletBalanceListAdapter(Context context, Listener listener) {
    super(new DiffCallBack());
    mContext = context;
    mListener = listener;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.li_outlet_balance, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder,
      int position) {
    holder.bind(getItem(position), position);
  }

  interface Listener {
    void onBalanceReportClicked(View view, OutletBalanceData outletBalanceData, int position);

    void onStampReport(View v, OutletBalanceData outletBalanceData, int position);
  }

  private static class DiffCallBack extends DiffUtil.ItemCallback<OutletBalanceData> {
    @Override
    public boolean areItemsTheSame(@NonNull OutletBalanceData oldItem,
        @NonNull OutletBalanceData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull OutletBalanceData oldItem,
        @NonNull OutletBalanceData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiOutletBalanceBinding binding;

    public ViewHolder(View view) {
      super(view);
      binding = LiOutletBalanceBinding.bind(view);
    }

    public void bind(OutletBalanceData outletBalanceData, int position) {
      setObject(outletBalanceData);
      setObjectListener(outletBalanceData, position);
    }

    private void setObjectListener(OutletBalanceData outletBalanceData, int position) {
      binding.btnBalance.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onBalanceReportClicked(v, outletBalanceData, getBindingAdapterPosition());
        }
      });

      binding.btnStamp.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onStampReport(v, outletBalanceData, getBindingAdapterPosition());
        }
      });
    }

    private void setObject(OutletBalanceData outletBalanceData) {
      binding.tvCompany.setText(outletBalanceData.getCompanyName());
      binding.tvOutletAndCode.setText(
          String.format("%s (%s)", outletBalanceData.getName(), outletBalanceData.getCode()));
      binding.tvBalance.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(outletBalanceData.getBalance()));
      binding.tvLimit.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(outletBalanceData.getBalanceLimit()));
      binding.tvStamp.setText(String.valueOf(outletBalanceData.getStampBalance()));
    }
  }
}
