package id.co.danwinciptaniaga.npmdsandroid.prefs;

import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.AsyncCallable;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.acs.data.LatestAppVersionResponse;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.update.DownloadUpdateUseCase;
import id.co.danwinciptaniaga.androcon.update.UpdateUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.DownloadHelper;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.GetLatestVersionUseCase;
import id.co.danwinciptaniaga.npmdsandroid.R;

public class PreferenceViewModel extends AndroidViewModel
    implements GetLatestVersionUseCase.Listener, DownloadUpdateUseCase.Listener {
  private static final String TAG = PreferenceViewModel.class.getSimpleName();

  private final AppExecutors appExecutors;
  private final ListeningExecutorService scheduledBgExecutor;
  private final AndroconConfig androconConfig;
  private final GetLatestVersionUseCase ucGetLatestVersion;
  private final DownloadUpdateUseCase ucDownloadUpdate;
  private final DownloadManager dm;

  private MutableLiveData<Resource<LatestAppVersionResponse>> latestAppVersionLd = new MutableLiveData<>();
  private MutableLiveData<Integer> downloadUpdateStatusLd = new MutableLiveData<>();
  private MutableLiveData<String> downloadMessageLd = new MutableLiveData<>();

  @ViewModelInject
  public PreferenceViewModel(@NonNull Application application, AppExecutors appExecutors,
      AndroconConfig androconConfig, GetLatestVersionUseCase getLatestVersionUseCase,
      DownloadUpdateUseCase downloadUpdateUseCase) {
    super(application);
    this.appExecutors = appExecutors;
    this.scheduledBgExecutor = MoreExecutors.listeningDecorator(
        Executors.newScheduledThreadPool(2));
    this.androconConfig = androconConfig;
    this.ucGetLatestVersion = getLatestVersionUseCase;
    this.ucGetLatestVersion.registerListener(this);
    this.ucDownloadUpdate = downloadUpdateUseCase;
    this.ucDownloadUpdate.registerListener(this);

    dm = (DownloadManager) getApplication().getSystemService(Context.DOWNLOAD_SERVICE);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.ucGetLatestVersion.unregisterListener(this);
    this.ucDownloadUpdate.unregisterListener(this);
  }

  public LiveData<Resource<LatestAppVersionResponse>> getLatestAppVersionLd() {
    return latestAppVersionLd;
  }

  public void checkLatestAppVersionLd() {
    ucGetLatestVersion.getLatestVersion();
  }

  @Override
  public void onGetLatestVersionStarted(Resource<LatestAppVersionResponse> loading) {
    latestAppVersionLd.postValue(loading);
  }

  @Override
  public void onGetLatestVersionSuccess(Resource<LatestAppVersionResponse> response) {
    int newVersion = Integer.parseInt(response.getData().getVersionCode());
    String msg = null;
    HyperLog.i(TAG, String.format("Comparing current vs server version: %s(%s) vs %s(%s)",
        response.getData().getVersionName(), response.getData().getVersionCode(),
        androconConfig.getAppVersionName(), androconConfig.getAppVersionCode()));
    if (newVersion > androconConfig.getAppVersionCode()) {
      msg = getApplication().getString(R.string.msg_new_version_available,
          response.getData().getVersionName());
      latestAppVersionLd.postValue(Resource.Builder.success(msg, response.getData()));
    } else {
      msg = getApplication().getString(R.string.msg_version_up_to_date,
          androconConfig.getAppVersionName());
      latestAppVersionLd.postValue(Resource.Builder.success(msg, null));
    }
  }

  @Override
  public void onGetLatestVersionFailure(Resource<LatestAppVersionResponse> response) {
    latestAppVersionLd.postValue(response);
  }

  public LiveData<Integer> getDownloadUpdateStatus() {
    return downloadUpdateStatusLd;
  }

  public LiveData<String> getDownloadMessage() {
    return downloadMessageLd;
  }

  public void downloadUpdate() {
    ucDownloadUpdate.downloadUpdate();
  }

  public void monitorDownload() {
    ucDownloadUpdate.scheduleMonitor();
  }

  @Override
  public void onDownloadUpdateStarted(
      Resource<DownloadUpdateUseCase.DownloadUpdateResult> loading) {
    DownloadUpdateUseCase.DownloadUpdateResult data = loading.getData();
    if (downloadUpdateStatusLd.getValue() != data.getStatus()) {
      downloadUpdateStatusLd.postValue(data.getStatus());
    }
    // pesan progress di sini
    if (!Objects.equals(downloadMessageLd.getValue(), loading.getMessage()))
      downloadMessageLd.postValue(loading.getMessage());
  }

  @Override
  public void onDownloadUpdateSuccess(
      Resource<DownloadUpdateUseCase.DownloadUpdateResult> response) {
    DownloadUpdateUseCase.DownloadUpdateResult data = response.getData();
    if (downloadUpdateStatusLd.getValue() != data.getStatus()) {
      downloadUpdateStatusLd.postValue(data.getStatus());
    }
    switch (data.getStatus()) {
    case DownloadManager.STATUS_PENDING:
      if (!Objects.equals(downloadMessageLd.getValue(), response.getMessage()))
        downloadMessageLd.postValue(response.getMessage());
      break;
    case DownloadManager.STATUS_SUCCESSFUL:
      if (!Objects.equals(downloadMessageLd.getValue(), response.getMessage())) {
        if (response.getData().getProgress() == 100) {
          downloadMessageLd
              .postValue(getApplication().getString(R.string.msg_click_to_install_update));
        } else {
          downloadMessageLd.postValue(response.getMessage());
        }
      }
      downloadUpdateStatusLd.postValue(data.getStatus());
      break;
    case DownloadManager.STATUS_PAUSED:
      if (!Objects.equals(downloadMessageLd.getValue(), response.getMessage()))
        downloadMessageLd.postValue(response.getMessage());
      break;
    }
  }

  @Override
  public void onDownloadUpdateFailure(
      Resource<DownloadUpdateUseCase.DownloadUpdateResult> response) {
    downloadUpdateStatusLd.postValue(DownloadManager.STATUS_FAILED);
    if (!Objects.equals(downloadMessageLd.getValue(), response.getMessage()))
      downloadMessageLd.postValue(response.getMessage());
  }
}
