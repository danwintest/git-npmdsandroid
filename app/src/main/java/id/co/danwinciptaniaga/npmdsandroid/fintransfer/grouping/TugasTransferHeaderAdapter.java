package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import java.text.NumberFormat;
import java.util.Objects;

import javax.inject.Inject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseHeaderData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiTugasTransferHeaderBrowseBinding;

public class TugasTransferHeaderAdapter extends PagingDataAdapter<TugasTransferBrowseHeaderData, TugasTransferHeaderAdapter.ViewHolder> {
  private final static String TAG = TugasTransferHeaderAdapter.class.getSimpleName();
  private final ItemListener mListener;
  private AppExecutors appExecutors;
  private Context ctx;

  @Inject
  public TugasTransferHeaderAdapter(ItemListener listener, Context ctx, AppExecutors appExecutors) {
    super(new DiffCallBack());
    mListener = listener;
    this.appExecutors = appExecutors;
    this.ctx = ctx;
  }

  //  @Override
  //  public int getItemCount() {
  //    int test =super.getItemCount();
  //    return test;
  //  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.li_tugas_transfer_header_browse, parent, false);
    return new TugasTransferHeaderAdapter.ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
  }

  private static class DiffCallBack extends DiffUtil
      .ItemCallback<TugasTransferBrowseHeaderData> {

    @Override
    public boolean areItemsTheSame(@NonNull TugasTransferBrowseHeaderData oldItem,
        @NonNull TugasTransferBrowseHeaderData newItem) {
      return oldItem.getCompanyId() == newItem.getCompanyId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull TugasTransferBrowseHeaderData oldItem,
        @NonNull TugasTransferBrowseHeaderData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  interface ItemListener {
    void onItemClicked(View v, TugasTransferBrowseHeaderData data, int pos);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiTugasTransferHeaderBrowseBinding binding;

    public ViewHolder(View v) {
      super(v);
      binding = LiTugasTransferHeaderBrowseBinding.bind(v);
    }

    public void bind(TugasTransferBrowseHeaderData data, int pos) {
      setObject(data);
      setObjectListener(data, pos);
    }

    private void setObject(TugasTransferBrowseHeaderData data) {
      binding.bankName.setText(data.getBankName());
      String formattedAmt = NumberFormat.getInstance().format(data.getTotalAmt());
      binding.totalAmount.setText(formattedAmt);
    }

    private void setObjectListener(TugasTransferBrowseHeaderData data, int pos) {
      binding.clContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, data, pos);
        }
      });
    }
  }
}
