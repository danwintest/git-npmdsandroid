package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.AppBaseConstants;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskProcessResultData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferProcessResponse;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferWfResponse;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class TugasTransferWfVM extends AndroidViewModel
    implements TugasTransferWfFormUC.Listener, TugasTransferWfProcessUC.Listener,
    TugasTransferWFGenerateOtpUC.Listener {
  private static String TAG = TugasTransferWfVM.class.getSimpleName();
  private final TugasTransferWfProcessUC ucProcess;
  private final TugasTransferWFGenerateOtpUC ucGenerateOtpUC;
  private List<TugasTransferBrowseDetailData> dataList = new ArrayList<>();
  private HashMap<Object, BigDecimal> allowenceMap = new HashMap<>();
  private String companyName;
  private UUID companyId;
  private TugasTransferWfFormUC ucForm;
  private List<String> decisionList = new ArrayList<>();
  private Boolean isShowAdditionalField = true;
  private Boolean isAuto;
  private BigDecimal allowanceRate = BigDecimal.ZERO;
  private Boolean isShowHitungPenyesuaian = false;
  private MutableLiveData<BigDecimal> totalAmt = new MutableLiveData<>();
  private MutableLiveData<Boolean> allowBackDate = new MutableLiveData<>(false);
  private UUID fieldSelectedSourceAccId = null;
  private MutableLiveData<Boolean> isFieldSourceAccIdOk = new MutableLiveData<>(false);
  private MutableLiveData<LocalDate> fieldTglTransfer = new MutableLiveData<>(LocalDate.now());
  private MutableLiveData<Boolean> isFieldTglTransferOk = new MutableLiveData<>(true);
  private MutableLiveData<List<KeyValueModel<UUID, String>>> sourceAccKvmList = new MutableLiveData<>();
  private MutableLiveData<FormState> formState = new MutableLiveData<>(
      FormState.loading("Silahkan Tunggu"));
  private MutableLiveData<String> otpVerifyFailMsg = new MutableLiveData<>();
  private MutableLiveData<FormState> formStateProcess = new MutableLiveData<>();
  private MutableLiveData<FormState> formStateGetOtp = new MutableLiveData<>();

  @ViewModelInject
  public TugasTransferWfVM(@NonNull Application application,
      TugasTransferWfFormUC ucForm, TugasTransferWfProcessUC ucProcess,
      TugasTransferWFGenerateOtpUC ucGenerateOtpUC) {
    super(application);
    this.ucForm = ucForm;
    this.ucProcess = ucProcess;
    this.ucGenerateOtpUC = ucGenerateOtpUC;
    this.ucForm.registerListener(this);
    this.ucProcess.registerListener(this);
    this.ucGenerateOtpUC.registerListener(this);
  }

  public void setAuto(Boolean auto) {
    isAuto = auto;
  }

  public void loadFormData() {
    ucForm.getWfFormResponse(companyId, isAuto);
  }

  public List<TugasTransferBrowseDetailData> getDataList() {
    return dataList;
  }

  public boolean isContainingAutomaticTask() {
    boolean isApproveDecision = getDecisionList().contains(WorkflowConstants.WF_OUTCOME_APPROVE);
    boolean isContaining = false;
    for (TugasTransferBrowseDetailData data : dataList) {
      if (Utility.AUTOMATIC.equals(data.getTransferTypeId())) {
        isContaining = true;
        break;
      }
    }
    return isApproveDecision && isContaining;
  }

  public void setDataList(List<TugasTransferBrowseDetailData> dataList) {
    if (dataList.size() > 0) {
      companyName = dataList.get(0).getCompanyName();
      companyId = dataList.get(0).getCompanyId();

      BigDecimal totalAmt = BigDecimal.ZERO;
      for (TugasTransferBrowseDetailData data : dataList)
        totalAmt = totalAmt.add(data.getAmount());

      this.totalAmt.postValue(totalAmt);
    }
    this.dataList = dataList;
    setShowDroppingTrx();
  }

  public Boolean getIsShowHitungPenyesuaian() {
    return isShowHitungPenyesuaian;
  }

  private void setShowDroppingTrx() {
    boolean isShowFinal = true;
    for (TugasTransferBrowseDetailData data : dataList) {
      boolean otherApp = (data.getTypeId().equals(AppBaseConstants.DROPPING_ADDITIONAL) ||
          data.getTypeId().equals(AppBaseConstants.PENGAJUAN_BARU_BOOKING_TRANSFER) ||
          data.getTypeId().equals(AppBaseConstants.PENGAJUAN_EDIT_BOOKING_TRANSFER));
      isShowFinal = isShowFinal ? !otherApp : isShowFinal;
    }
    isShowHitungPenyesuaian = isShowFinal;
  }

  public MutableLiveData<BigDecimal> getTotalAmt() {
    return totalAmt;
  }

  public BigDecimal getAllowanceRate() {
    return allowanceRate;
  }

  public void allowanceRateCalculate(BigDecimal allowanceRate) {
    this.allowanceRate = allowanceRate;
    List<TugasTransferBrowseDetailData> newDataList = new ArrayList<>();

    BigDecimal ONE_HUNDRED = new BigDecimal("100");
    if (allowanceRate == null) {
      return;
    } else {
      for (TugasTransferBrowseDetailData data : dataList) {
        TugasTransferBrowseDetailData newData = data;
        BigDecimal amt = data.getAmount();
        if (amt != null && amt
            .compareTo(data.getDroppingMinimumAmount()) >= 0) {
          BigDecimal realAmount = allowenceMap.get(data.getTransferInstructionId());
          BigDecimal amount;
          if (realAmount == null) {
            allowenceMap.put(data.getTransferInstructionId(), amt);
            amount = amt;
          } else {
            amount = realAmount;
          }

          BigDecimal allowanceFactorial = ONE_HUNDRED.subtract(allowanceRate)
              .divide(ONE_HUNDRED, 2, RoundingMode.HALF_UP);
          BigDecimal allowanceAmount = amount.multiply(allowanceFactorial);
          //          kve.setValue("amount", allowanceAmount);
          newData.setAmount(allowanceAmount);
          //          kve.setValue("allowanceRate", allowanceRate);
          newData.setAllowanceRate(allowanceRate);
          //          kve.setValue("allowance", allowanceAmount);
          newData.setAllowanceAmount(allowanceAmount);
        }
        newDataList.add(data);
      }
      setDataList(newDataList);
    }

  }

  public String getCompanyName() {
    return companyName;
  }

  public UUID getCompanyId() {
    return companyId;
  }

  public void setCompanyId(UUID companyId) {
    this.companyId = companyId;
  }

  public MutableLiveData<FormState> getFormState() {
    return formState;
  }

  public MutableLiveData<String> getOtpVerifyFailMsg() {
    return otpVerifyFailMsg;
  }

  public List<String> getDecisionList() {
    return decisionList;
  }

  public void setDecisionList(List<String> decisionList) {
    this.decisionList = decisionList;
    setIsShowAdditionalField(decisionList.get(0));
  }

  public MutableLiveData<Boolean> getIsFieldSourceAccIdOk() {
    return isFieldSourceAccIdOk;
  }

  public void setIsFieldSourceAccIdOk(Boolean isOk) {
    this.isFieldSourceAccIdOk.postValue(isOk);
  }

  public MutableLiveData<Boolean> getIsFieldTglTransferOk() {
    return isFieldTglTransferOk;
  }

  private void setIsFieldTglTransferOk(Boolean isOk) {
    this.isFieldTglTransferOk.postValue(isOk);
  }

  private void setIsShowAdditionalField(String decision) {
    boolean isShow =
        decision.equals(Utility.WF_OUTCOME_APPROVE_TRANSFER_OTOMATIS) || decision.equals(
            Utility.WF_OUTCOME_APPROVE_TRANSFER_MANUAL);

    if (!isShow)
      isShowHitungPenyesuaian = false;

    isShowAdditionalField = isShow;
  }

  public Boolean getIsShowAdditionalField() {
    return isShowAdditionalField;
  }

  public MutableLiveData<Boolean> getAllowBackDate() {
    return allowBackDate;
  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getSourceAccKvmList() {
    return sourceAccKvmList;
  }

  public void setFieldSelectedSourceAccId(UUID id) {
    setIsFieldSourceAccIdOk(id != null);
    fieldSelectedSourceAccId = id;
  }

  public UUID getFieldSelectedSourceAccId() {
    return fieldSelectedSourceAccId;
  }

  public void setFieldTglTransfer(LocalDate date) {
    setIsFieldTglTransferOk(date != null);
    fieldTglTransfer.postValue(date);
  }

  public MutableLiveData<LocalDate> getFieldTglTransfer() {
    return fieldTglTransfer;
  }

  public MutableLiveData<FormState> getFormStateProcess() {
    return formStateProcess;
  }

  public MutableLiveData<FormState> getFormStateGetOtp() {
    return formStateGetOtp;
  }

  public void processApproveTransferOtomatis(String comment) {
    ucProcess.processApproveTransferOtomatis(comment, fieldTglTransfer.getValue(),
        fieldSelectedSourceAccId, dataList);
  }

  public void processApproveTransferManual(String comment) {
    ucProcess.processApproveTransferManual(comment, fieldTglTransfer.getValue(),
        fieldSelectedSourceAccId, dataList);
  }

  public void processReturn(String comment) {
    ucProcess.processReturn(comment, dataList);
  }

  public void processReject(String comment) {
    ucProcess.processReject(comment, dataList);
  }

  public void processGenerateOTP() {
    ucGenerateOtpUC.processGenerateOTP();
  }

  public void processApprove(String comment, String otp) {
    ucProcess.processApprove(comment, dataList, otp, !isContainingAutomaticTask());
  }

  @Override
  public void onProcessStarted(Resource<TugasTransferWfResponse> loading) {
    formState.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessSuccess(Resource<TugasTransferWfResponse> res) {
    TugasTransferWfResponse data = res.getData();
    allowBackDate.postValue(data.getAllowBackDate());
    List<KeyValueModel<UUID, String>> kvmList = new ArrayList<>();
    for (BankAccountData bad : data.getBankAccountDataList()) {
      KeyValueModel<UUID, String> kvm = new KeyValueModel<>(bad.getId(),
          bad.getBankName() + " - " + bad.getAccountNo());
      kvmList.add(kvm);
    }

    sourceAccKvmList.postValue(kvmList);
    formState.postValue(FormState.ready(res.getMessage()));
  }

  @Override
  public void onProcessFailure(Resource<TugasTransferWfResponse> res) {
    formState.postValue(FormState.error(res.getMessage()));
  }

  @Override
  public void onProcessWFStarted(Resource<TugasTransferProcessResponse> loading) {
    formStateProcess.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessWFSuccess(Resource<TugasTransferProcessResponse> res) {
    /**
     * penambahan variable isSuccess validasi untuk mengetahui apakah proses benar benar sukses
     * atau gagal dan memberi signal apk untuk logout
     * (jika otp gagal verify dan mengharuskan apk untuk logout)
     */
    boolean isSuccess = res.getData().getOtpVerifySuccess();
    if(isSuccess) {
      HyperLog.d(TAG, res.getMessage());
      TugasTransferProcessResponse resData = res.getData();
      if (resData != null) {
        StringBuilder sbTrxNo = new StringBuilder();

        for (String trxNo : resData.getTrxNoList())
          sbTrxNo.append(trxNo + ", ");
        if (!sbTrxNo.toString().isEmpty()) {
          formStateProcess.postValue(FormState.ready(sbTrxNo.toString() + "Gagal Di Proses"));
        } else {
          formStateProcess.postValue(FormState.ready(res.getMessage()));
        }

        StringBuilder sbErrMsg = new StringBuilder();
        for (String errorMsg : resData.getErrorMsg())
          sbErrMsg.append(errorMsg + ", ");
        HyperLog.d(TAG, sbErrMsg.toString());

      } else {
        formState.postValue(FormState.ready(res.getMessage()));
      }
      otpVerifyFailMsg.postValue(null);
    }else{
      otpVerifyFailMsg.postValue(res.getData().getErrorMsg().get(0));
    }

  }

  @Override
  public void onProcessWFFailure(Resource<TugasTransferProcessResponse> res) {
    formStateProcess.postValue(FormState.error(res.getMessage()));
  }

  @Override
  public void onProcessGetOtpStarted(Resource<Boolean> loading) {
    formStateGetOtp.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessGetOtpSuccess(Resource<Boolean> res) {
    formStateGetOtp.postValue(FormState.ready(res.getMessage()));
  }

  @Override
  public void onProcessGetOtpFailure(Resource<Boolean> res) {
    formStateGetOtp.postValue(FormState.error(res.getMessage()));
  }
}
