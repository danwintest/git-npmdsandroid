package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import retrofit2.HttpException;

public class ExpenseAocDecideUseCase extends
    BaseObservable<ExpenseAocDecideUseCase.Listener, ExpenseData> {
  public interface Listener {

    void onExpenseAocDecideStarted(Resource<ExpenseData> loading);

    void onExpenseAocDecideSuccess(Resource<ExpenseData> response);

    void onExpenseAocDecideFailure(Resource<ExpenseData> response);

  }

  private final ExpenseService expenseService;

  private final AppExecutors appExecutors;

  @Inject
  public ExpenseAocDecideUseCase(Application application, ExpenseService expenseService,
      AppExecutors appExecutors) {
    super(application);
    this.expenseService = expenseService;
    this.appExecutors = appExecutors;
  }

  public void aocDecide(UUID procTaskId, UUID expenseId, String outcome, String comment,
      Date checkTs) {
    notifyStart(null, null);
    ListenableFuture<ExpenseData> saveDraftLf = expenseService.decide(procTaskId, expenseId,
        outcome, comment, checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<ExpenseData>() {
      @Override
      public void onSuccess(@NullableDecl ExpenseData result) {
        notifySuccess(outcome + " Biaya Operasional berhasil", result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(
              ((HttpException) t).response().errorBody());
        }
        notifyFailure(er == null ? outcome + " Biaya Operasional gagal" : null, er, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<ExpenseData> result) {
    listener.onExpenseAocDecideStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<ExpenseData> result) {
    listener.onExpenseAocDecideSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<ExpenseData> result) {
    listener.onExpenseAocDecideFailure(result);
  }
}
