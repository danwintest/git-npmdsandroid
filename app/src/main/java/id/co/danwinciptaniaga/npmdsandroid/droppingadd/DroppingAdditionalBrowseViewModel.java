package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.LoadState;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.ListActionResponse;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseSort;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseSort;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class DroppingAdditionalBrowseViewModel extends AndroidViewModel
    implements DroppingListActionUC.Listener {

  private final GetDroppingAdditionalListUseCase ucGetDroppingAdditionalList;
  private final DroppingListActionUC ucDroppingListAction;
  private final Pager<Integer, DroppingAdditionalData> pager;

  private Set<SortOrder> sortFields = new LinkedHashSet<>(); // urutan pengaruh
  private MutableLiveData<DroppingBrowseFilter> filterField = new MutableLiveData<>();

  private LiveData<PagingData<DroppingAdditionalData>> droppingList;
  private MutableLiveData<Boolean> refreshList = new SingleLiveEvent<>();
  private MutableLiveData<LoadState> droppingListLoadState = new MutableLiveData<>();
  private MutableLiveData<Resource<String>> actionEvent = new SingleLiveEvent<>();

  @ViewModelInject
  public DroppingAdditionalBrowseViewModel(@NonNull Application application,
      AppExecutors appExecutors,
      GetDroppingAdditionalListUseCase getDroppingAdditionalListUseCase,
      DroppingListActionUC droppingListActionUC) {
    super(application);
    this.ucGetDroppingAdditionalList = getDroppingAdditionalListUseCase;
    this.ucDroppingListAction = droppingListActionUC;
    this.ucDroppingListAction.registerListener(this);

    this.sortFields.add(DroppingBrowseSort
        .sortBy(DroppingBrowseSort.Field.REQUEST_DATE, SortOrder.Direction.DESC));

    pager = new Pager<Integer, DroppingAdditionalData>(
        new PagingConfig(Utility.PAGE_SIZE),
        () -> new DroppingAdditionalPagingSource(ucGetDroppingAdditionalList,
            appExecutors.networkIO())
    );

  }

  public LiveData<LoadState> getDroppingListLoadState() {
    return droppingListLoadState;
  }

  public void setDroppingListLoadState(LoadState loadState) {
    this.droppingListLoadState.postValue(loadState);
  }

  public LiveData<Resource<String>> getActionEvent() {
    return actionEvent;
  }

  public LiveData<PagingData<DroppingAdditionalData>> getDroppingList() {
    if (droppingList == null) {
      // hanya load data pertama kali
      CoroutineScope viewModelScope = ViewModelKt.getViewModelScope(this);
      // data akan di-load otomatis setelah setup berikut
      droppingList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pager), viewModelScope);
    }
    return droppingList;
  }

  public void setRefreshList(Boolean refreshList) {
    this.refreshList.postValue(refreshList);
  }

  public LiveData<Boolean> getRefreshList() {
    return refreshList;
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    this.ucDroppingListAction.unregisterListener(this);
  }

  public void deleteSelectedDropping(List<DroppingAdditionalData> selectedDad) {
    if (selectedDad != null) {
      List<UUID> selectedIds = selectedDad.stream().map(d -> d.getId()).collect(
          Collectors.toList());
      ucDroppingListAction.deleteDroppings(selectedIds);
    }
  }

  public Set<SortOrder> getSortFields() {
    return sortFields;
  }

  public void setSortFields(Set<SortOrder> sortFields) {
    this.sortFields.clear();
    if (sortFields != null) {
      this.sortFields.addAll(sortFields);
    }
    ucGetDroppingAdditionalList.setSort(sortFields);
    refreshList.postValue(true);
  }

  public MutableLiveData<DroppingBrowseFilter> getFilterField() {
    return filterField;
  }

  public void loadListWithFilter(DroppingBrowseFilter filterField) {
    this.filterField.postValue(filterField);
    ucGetDroppingAdditionalList.setFilter(filterField);
    refreshList.postValue(true);
  }

  @Override
  public void onDroppingActionProcessStarted(
      Resource<ListActionResponse<String, String>> response) {
    actionEvent.postValue(Resource.Builder.loading(response.getMessage(), null));
  }

  @Override
  public void onDroppingActionProcessSuccess(
      Resource<ListActionResponse<String, String>> response) {
    actionEvent.postValue(Resource.Builder.success(response.getMessage(), null));
  }

  @Override
  public void onDroppingActionProcessFailure(
      Resource<ListActionResponse<String, String>> response) {
    String message = Resource.getMessageFromError(response,
        getApplication().getString(R.string.error_contact_support));
    actionEvent.postValue(Resource.Builder.error(message));
  }
}
