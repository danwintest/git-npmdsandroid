package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseHeaderData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseHeaderResponse;

public class DroppingDailyTaskBrowseHeaderPagingSource
    extends ListenableFuturePagingSource<Integer, DroppingDailyTaskBrowseHeaderData> {
  private static final String TAG = DroppingDailyTaskBrowseHeaderPagingSource.class.getSimpleName();
  private final GetDroppingDailyTaskBrowseHeaderUC uc;
  private final Executor mBgExecutor;

  public DroppingDailyTaskBrowseHeaderPagingSource(GetDroppingDailyTaskBrowseHeaderUC uc,
      Executor mBgExecutor) {
    this.uc = uc;
    this.mBgExecutor = mBgExecutor;
  }

  @NotNull
  @Override
  public ListenableFuture<LoadResult<Integer, DroppingDailyTaskBrowseHeaderData>> loadFuture(
      @NotNull LoadParams<Integer> loadParams) {
    Integer nextPageNumber = loadParams.getKey();
    if (nextPageNumber == null) {
      nextPageNumber = 1;
    }
    int pageSize = loadParams.getPageSize();
    ListenableFuture<LoadResult<Integer, DroppingDailyTaskBrowseHeaderData>> pageFuture = Futures.transform(
        uc.getDroppingTaskBrowseHeaderList(nextPageNumber,
            pageSize), this::toLoadResult, mBgExecutor);
    return Futures.catching(pageFuture, Exception.class, input -> {
      HyperLog.e(TAG, "ERROR while getting DroppingDailyTaskBrowseHeaderList", input);
      ErrorResponse er = RetrofitUtility.parseErrorBody(input);
      return new LoadResult.Error(
          er != null ? new Exception(er.getFormattedMessage(), input) : input);
    }, mBgExecutor);
  }

  private LoadResult<Integer, DroppingDailyTaskBrowseHeaderData> toLoadResult(
      @NonNull DroppingDailyTaskBrowseHeaderResponse response) {
    return new LoadResult.Page<>(
        response.getDroppingDailyTaskBrowseHeaderData(),
        null,
        response.getNextPageNumber(),
        LoadResult.Page.COUNT_UNDEFINED,
        LoadResult.Page.COUNT_UNDEFINED);
  }
}
