package id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter;

import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferFilterResponse;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class GetTugasTransferFilterUC
    extends BaseObservable<GetTugasTransferFilterUC.Listener, TugasTransferFilterResponse> {
  private final String TAG = GetTugasTransferFilterUC.class.getSimpleName();
  private final TugasTransferService service;
  private final Application app;
  private final AppExecutors appExecutors;

  @Inject
  public GetTugasTransferFilterUC(Application app, TugasTransferService service,
      AppExecutors appExecutors) {
    super(app);
    this.app = app;
    this.service = service;
    this.appExecutors = appExecutors;
  }

  @Override
  protected void doNotifyStart(Listener listener,
      Resource<TugasTransferFilterResponse> result) {
    listener.onProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener,
      Resource<TugasTransferFilterResponse> result) {
    listener.onProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener,
      Resource<TugasTransferFilterResponse> result) {
    listener.onProcessFailure(result);
  }

  public void getCompanyFilter() {
    ListenableFuture<TugasTransferFilterResponse> process = service.getDefaultCompanyFilter();
    Futures.addCallback(process, new FutureCallback<TugasTransferFilterResponse>() {
      @Override
      public void onSuccess(@NullableDecl TugasTransferFilterResponse result) {
        HyperLog.d(TAG, "getCompanyFilter() berhasil");
        notifySuccess("-", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String msg = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("getCompanyFilter() Gagal ->" + msg, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void getFilterField(UUID companyId, Boolean isReloadSourceAcc) {
    notifyStart("Silahkan Tunggu", null);
    ListenableFuture<TugasTransferFilterResponse> process = service.getFilterField(companyId,
        isReloadSourceAcc);
    Futures.addCallback(process, new FutureCallback<TugasTransferFilterResponse>() {
      @Override
      public void onSuccess(@NullableDecl TugasTransferFilterResponse result) {
        HyperLog.d(TAG, "getFilterField() berhasil");
        notifySuccess("-", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String msg = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("getFilterField() Gagal ->" + msg, null, t);
      }
    }, appExecutors.networkIO());
  }

  public interface Listener {
    void onProcessStarted(Resource<TugasTransferFilterResponse> loading);

    void onProcessSuccess(Resource<TugasTransferFilterResponse> res);

    void onProcessFailure(Resource<TugasTransferFilterResponse> res);
  }
}
