package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import org.acra.ACRA;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.google.android.material.snackbar.Snackbar;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import id.co.danwinciptaniaga.androcon.utility.ImageUtility;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBankAccountData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmds.data.common.BatchData;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingVM;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseVM;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFormState;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningForm;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfProcessParameter;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;

public class AppBookingTransferNewFragment extends Abstract_AppBookingFragment {
  private final static String TAG = AppBookingTransferNewFragment.class.getSimpleName();
  protected ArrayAdapter<KeyValueModel<BatchData, String>> batchAdapter;
  protected ArrayAdapter<KeyValueModel<AppBookingBankAccountData, String>> wfBankAdapter;
  private AppBookingBrowseVM mBrowseVM;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {

    Abstract_AppBookingVM vm = new ViewModelProvider(this).get(AppBookingTransferVM.class);
    AppBookingTransferNewFragmentArgs args = AppBookingTransferNewFragmentArgs
        .fromBundle(getArguments());

    boolean isHasCreatePermission = args.getIsHasCreatePermission();
    setOnCreateView(isHasCreatePermission, inflater, container, this, TAG, vm);
    getTransferVm().setTxnMode(args.getTxnMode());
    getTransferVm().setField_AppBookingId(args.getAppPBookingId());
    getTransferVm().setField_AppBookingTypeId(args.getBookingType());
    if (Objects.equals(getTransferVm().getField_AppOperation(), null))
      getTransferVm().setField_AppOperation(args.getAppOperation());
    getTransferVm().setFromBookingStatus(false);

    mBrowseVM = new ViewModelProvider(requireActivity()).get(AppBookingBrowseVM.class);
    return getBinding().getRoot();
  }

  @Override
  protected void processLoadData() {
    if (getTransferVm().getField_AppBookingId() != null) {
      HyperLog.d(getThisTag(),"processLoadData processLoadAppBookingData");
      getTransferVm().processLoadAppBookingData(getTransferVm().getField_AppBookingId());
    } else {
      HyperLog.d(getThisTag(),"processLoadData processPrepareBookingForm");
      getTransferVm().processPrepareBookingForm();
    }
  }

  @Override
  protected void initForm() {
    setField_ConsumerBankName();
    setField_ConsumerAccountName();
    setField_ConsumerAccountNameWarn();
    setField_ConsumerAccountNo();
    setIvValid_Cons();
    setButtonEditRekConsumer();
    setButtonClearConsumer();

    setField_BiroJasaBankName();
    setField_BiroJasaAccountName();
    setField_BiroJasaAccountNo();
    setIvValid_BJ();
    setButtonEditRekBiroJasa();
    setButtonClearBiroJasa();

    setField_FIFBankName();
    setField_FIFAccountName();
    setField_FIFAccountNo();
    setIvValid_FIF();
    setButtonEditRekFIF();
    setButtonClearFIF();

    setField_Batch();
    setTxnFormObserver();
    setReloadFormObserver();
    super.initForm();
  }

  @Override
  protected void setField_ConsumerName() {
    getTransferVm().getField_ConsumerName().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etConsumerName.setText(null);
      } else if (!data.equals(getBinding().etConsumerName.getText().toString())) {
        getBinding().etConsumerName.setText(data);
      }
    });

    // set listener
    getBinding().etConsumerName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          //          getTransferVm().setField_ConsumerName(s.toString(),false);
          getTransferVm().setField_ConsumerName(s.toString());
      }
    });

    // set Error message
    getTransferVm().getField_ConsumerName_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilConsumerName.setError(message);
    });
  }

  @Override
  protected void setField_OpenClose() {
    getBinding().cbTFOpenClose.setOnCheckedChangeListener(
        new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            getTransferVm().setField_IsOpenClose(isChecked);
            getBinding().tilTFFIF.setEnabled(isChecked);
            getBinding().btnTFEditRekeningFIF.setEnabled(isChecked);
            getBinding().btnTFClearFIF.setEnabled(isChecked);
            if (!isChecked)
              getTransferVm().setField_FIFAmount(BigDecimal.ZERO);
          }
        });
    // set data
    getTransferVm().isField_isOpenClose().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getTransferVm().setField_IsOpenClose(false);
      } else if (!data.equals(getBinding().cbTFOpenClose.isChecked())) {
        getBinding().cbTFOpenClose.setChecked(data);
      }
    });
  }

  @Override
  protected void setField_AlasanCash() {
    return;
  }

  @Override
  protected void setField_Attachment_SERAH_TERIMA() {
    return;
  }


  @Override
  protected void setField_ConsumerAmount() {
    // set data
    getTransferVm().getField_ConsumerAmount().observe(getViewLifecycleOwner(), amt -> {
      if (amt == null) {
        getBinding().etTFConsumer.setText(null);
      } else {
        if(!getTransferVm().isTransferFieldEnabled()) {
          String formattedAmt = Utility.getFormattedAmt(amt);
          if (!formattedAmt.equals(getBinding().etTFConsumer.getText().toString())) {
            getBinding().etTFConsumer.setText(formattedAmt);
          }
        }else{
          if (!amt.toString().equals(getBinding().etTFConsumer.getText().toString()))
            getBinding().etTFConsumer.setText(amt.toString());
        }
      }
    });

    // set listener
    getBinding().etTFConsumer.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          BigDecimal amt = Utility.getAmtFromFromattedString(s.toString());
          getTransferVm().setField_ConsumerAmount(amt);
        } else {
          getTransferVm().setField_ConsumerAmount(null);
        }
      }
    });

    // set ERROR Message
    getTransferVm().getField_ConsumerAmount_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilTFConsumer.setError(message);
    });
  }

  @Override
  protected void setField_BiroJasaAmount() {
    // set data
    getTransferVm().getField_BiroJasaAmount().observe(getViewLifecycleOwner(), amt -> {
      if (amt == null) {
        getBinding().etTFBiroJasa.setText(null);
      } else {
        if (!getTransferVm().isTransferFieldEnabled()) {
          String formattedAmt = Utility.getFormattedAmt(amt);
          if (!formattedAmt.equals(getBinding().etTFBiroJasa.getText().toString())) {
            getBinding().etTFBiroJasa.setText(formattedAmt);
          }
        }else{
          if (!amt.toString().equals(getBinding().etTFBiroJasa.getText().toString())) {
            getBinding().etTFBiroJasa.setText(amt.toString());
          }
        }
      }
    });

    // set error
    getTransferVm().getField_BiroJasaAmount_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilTFBiroJasa.setError(message);
    });

    // set listener
    getBinding().etTFBiroJasa.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          BigDecimal amt = Utility.getAmtFromFromattedString(s.toString());
          getTransferVm().setField_BiroJasaAmount(amt);
        } else {
          getTransferVm().setField_BiroJasaAmount(null);
        }
      }
    });
  }

  @Override
  protected void setField_FIFAmount() {
    // set data
    getTransferVm().getField_FIFAmount().observe(getViewLifecycleOwner(), amt -> {
      String fn = "TRANSgetField_FIFAmount().observe ";
      HyperLog.d(TAG, fn + "amt[" + amt + "]");
      HyperLog.d(TAG, fn + " isPencairanFIF Enable[" + (getBinding().etPencairanFIF.isEnabled()) + "]");
      if (amt == null) {
        getBinding().etTFFIF.setText(null);
      } else {
        if (!getTransferVm().isTransferFieldEnabled()) {
          String formattedAmt = Utility.getFormattedAmt(amt);
          if (!formattedAmt.equals(getBinding().etTFFIF.getText().toString())) {
            getBinding().etTFFIF.setText(formattedAmt);
          }
        } else {
          if (!amt.toString().equals(getBinding().etTFFIF.getText().toString())) {
            getBinding().etTFFIF.setText(amt.toString());
          }
        }
      }
    });

    // set listener
    getBinding().etTFFIF.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          BigDecimal amt = Utility.getAmtFromFromattedString(s.toString());
          getTransferVm().setField_FIFAmount(amt);
        } else {
          getTransferVm().setField_FIFAmount(null);
        }
      }
    });

    // set ERROR
    getTransferVm().getField_FIFAmount_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilTFFIF.setError(message);
    });
  }

  private void setButtonClearConsumer() {
    getBinding().btnTFClearCons.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getTransferVm().setField_ConsumerBankId(null, null);
        getTransferVm().setField_ConsumerOtherBank(null);
        getTransferVm().setField_ConsumerBank_LABEL("-");
        getTransferVm().setField_ConsumerAccountNo(null);
        getTransferVm().setField_ConsumerAccountName(null, false);
        getTransferVm().setField_ConsumerAmount(null);
      }
    });
  }

  private void setButtonClearBiroJasa() {
    getBinding().btnTFClearBiroJasa.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getTransferVm().setField_BiroJasaBankId(null, null);
        getTransferVm().setField_BiroJasaOtherBank(null);
        getTransferVm().setField_BiroJasaBank_LABEL("-");
        getTransferVm().setField_BiroJasaAccountNo(null);
        getTransferVm().setField_BiroJasaAccountName(null);
        getTransferVm().setField_BiroJasaAmount(null);
      }
    });
  }

  private void setButtonClearFIF() {
    getBinding().btnTFClearFIF.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getTransferVm().setField_FIFBankId(null, null);
        getTransferVm().setField_FIFOtherBank(null);
        getTransferVm().setField_FIFBank_LABEL("-");
        getTransferVm().setField_FIFAccountNo(null);
        getTransferVm().setField_FIFAccountName(null);
        getTransferVm().setField_FIFAmount(null);
      }
    });
  }

  private void setButtonEditRekConsumer() {
    getBinding().btnTFEditRekeningConsumer.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (!getTransferVm().isConsumerAccountDialogShow())
          showEditRekeningForm(AppBookingTransferVM.TXN_CONSUMER);
      }
    });
  }

  private void setButtonEditRekBiroJasa() {
    getBinding().btnTFEditRekeningBiroJasa.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (!getTransferVm().isBiroJasaAccountDialogShow())
          showEditRekeningForm(AppBookingTransferVM.TXN_BIRO_JASA);
      }
    });
  }

  private void setButtonEditRekFIF() {
    getBinding().btnTFEditRekeningFIF.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (!getTransferVm().isFIFAccountDialogShow())
          showEditRekeningForm(AppBookingTransferVM.TXN_FIF);
      }
    });
  }

  protected void setReloadFormObserver() {
    // setup listener untuk hasil dari Fragment dengan mode TXN
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<Boolean> isFromTxnProcess = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        AppBookingTransferVM.FROM_TXN_PROCESS);
    isFromTxnProcess.observe(getViewLifecycleOwner(), isTrue -> {
      if (isTrue)
        getTransferVm().setTxnMode(null);
      processLoadData();
    });
  }

  protected void setTxnFormObserver() {
    // setup listener untuk hasil dari Fragment EditDetail
    NavController navController = NavHostFragment.findNavController(this);
    MutableLiveData<AppBookingTransferRekeningForm.TxnForm> txnFormMdl = navController
        .getCurrentBackStackEntry()
        .getSavedStateHandle()
        .getLiveData(AppBookingTransferRekeningForm.TXN_FORM_FIELD);

    txnFormMdl.observe(getViewLifecycleOwner(), txnForm -> {
      switch (txnForm.getMode()) {
      case AppBookingTransferVM.TXN_CONSUMER:
        getTransferVm().setField_ConsumerAccountName(txnForm.getAccountName(), false);
        getTransferVm().setField_ConsumerAccountNo(txnForm.getAccountNo());
        getTransferVm().setField_ConsumerBankId(txnForm.getBankId(), txnForm.getBankName());
        getTransferVm().setField_ConsumerBankName(txnForm.getBankName());
        getTransferVm().setField_ConsumerOtherBank(txnForm.getOtherBankName());
        getTransferVm().setField_ConsValid(txnForm.isValidated());
        break;
      case AppBookingTransferVM.TXN_BIRO_JASA:
        getTransferVm().setField_BiroJasaAccountName(txnForm.getAccountName());
        getTransferVm().setField_BiroJasaAccountNo(txnForm.getAccountNo());
        getTransferVm().setField_BiroJasaBankId(txnForm.getBankId(), txnForm.getBankName());
        getTransferVm().setField_BiroJasaBankName(txnForm.getBankName());
        getTransferVm().setField_BiroJasaOtherBank(txnForm.getOtherBankName());
        getTransferVm().setField_BjValid(txnForm.isValidated());
        break;
      case AppBookingTransferVM.TXN_FIF:
        getTransferVm().setField_FIFAccountName(txnForm.getAccountName());
        getTransferVm().setField_FIFAccountNo(txnForm.getAccountNo());
        getTransferVm().setField_FIFBankId(txnForm.getBankId(), txnForm.getBankName());
        getTransferVm().setField_FIFBankName(txnForm.getBankName());
        getTransferVm().setField_FIFOtherBank(txnForm.getOtherBankName());
        getTransferVm().setField_FIFValid(txnForm.isValidated());
        break;
      }
      navController.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(AppBookingTransferRekeningForm.TXN_FORM_FIELD);
    });

  }

  protected void setField_ConsumerBankName() {
    getTransferVm().getField_ConsumerBank_LABEL().observe(getViewLifecycleOwner(),
        data -> {
          String bankName = getTransferVm().getField_ConsumerBankName();
          String finalBankName = bankName != null ? bankName : (data == null ? "-" : data);
          getBinding().tvTFConsumerBankName.setText(finalBankName);
          getBinding().tvTFConsumerBankName
              .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
        });

    // set ERROR
    getTransferVm().getField_ConsumerBank_LABEL_ERROR()
        .observe(getViewLifecycleOwner(), message -> {
          if (message != null) {
            getBinding().tvTFConsumerBankName.setText(message);
            getBinding().tvTFConsumerBankName.setTextColor(
                getContext().getResources().getColor(R.color.design_default_color_error));
          }
        });
  }

  protected void setField_ConsumerAccountName() {
    getTransferVm().getField_ConsumerAccountName().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFConsumerAccountName.setText(data == null ? "-" : data);
          getBinding().tvTFConsumerAccountName
              .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
        });

    // set ERROR
    getTransferVm().getField_ConsumerAccountName_ERROR()
        .observe(getViewLifecycleOwner(), message -> {
          if (message != null) {
            getBinding().tvTFConsumerAccountName.setText(message);
            getBinding().tvTFConsumerAccountName.setTextColor(
                getContext().getResources().getColor(R.color.design_default_color_error));
          }
        });
  }

  protected void setField_ConsumerAccountNameWarn() {
    getTransferVm().getField_TF_ConsumerNameWarn().observe(getViewLifecycleOwner(), isVisible -> {
      if (isVisible) {
        getBinding().ivTFConsumerAccountNameWarn.setVisibility(View.VISIBLE);
        getBinding().tvTFConsumerAccountNameWarn.setVisibility(View.VISIBLE);
        getBinding().tvTFConsumerAccountNameWarn.setText(R.string.accountName_warn);
      } else {
        getBinding().ivTFConsumerAccountNameWarn.setVisibility(View.GONE);
        getBinding().tvTFConsumerAccountNameWarn.setVisibility(View.GONE);
      }
    });
  }

  protected void setField_ConsumerAccountNo() {
    getTransferVm().getField_ConsumerAccountNo().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFConsumerRekNo.setText(data == null ? "-" : data);
          getBinding().tvTFConsumerRekNo
              .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
        });

    // set ERROR
    getTransferVm().getField_ConsumerAccountNo_ERROR()
        .observe(getViewLifecycleOwner(), message -> {
          if (message != null) {
            getBinding().tvTFConsumerRekNo.setText(message);
            getBinding().tvTFConsumerRekNo.setTextColor(
                getContext().getResources().getColor(R.color.design_default_color_error));
          }
        });
  }

  protected void setIvValid_Cons() {
    getTransferVm().isField_ConsValid().observe(getViewLifecycleOwner(), isValid -> {
      if (isValid)
        getBinding().ivTFConsumerStatus.setImageResource(
            R.drawable.ic_baseline_check_circle_outline_24);
      else
        getBinding().ivTFConsumerStatus.setImageResource(
            R.drawable.ic_baseline_remove_circle_outline_24);
    });
  }

  protected void setField_BiroJasaBankName() {
    getTransferVm().getField_BiroJasaBank_LABEL().observe(getViewLifecycleOwner(),
        data -> {
          String bankName = getTransferVm().getField_BiroJasaBankName();
          String finalBankName = bankName != null ? bankName : (data == null ? "-" : data);
          getBinding().tvTFBiroJasaBankName.setText(finalBankName);
          getBinding().tvTFBiroJasaBankName
              .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
        });
    // set ERROR
    getTransferVm().getField_BiroJasaBank_LABEL_ERROR()
        .observe(getViewLifecycleOwner(), message -> {
          if (message != null) {
            getBinding().tvTFBiroJasaBankName.setText(message);
            getBinding().tvTFBiroJasaBankName.setTextColor(
                getContext().getResources().getColor(R.color.design_default_color_error));
          }
        });
  }

  protected void setField_BiroJasaAccountName() {
    getTransferVm().getField_BiroJasaAccountName().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFBiroJasaAccountName.setText(data == null ? "-" : data);
          getBinding().tvTFBiroJasaAccountName
              .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
        });
    // set ERROR
    getTransferVm().getField_BiroJasaAccountName_ERROR()
        .observe(getViewLifecycleOwner(), message -> {
          if (message != null) {
            getBinding().tvTFBiroJasaAccountName.setText(message);
            getBinding().tvTFBiroJasaAccountName.setTextColor(
                getContext().getResources().getColor(R.color.design_default_color_error));
          }
        });
  }

  protected void setIvValid_BJ() {
    getTransferVm().isField_BjValid().observe(getViewLifecycleOwner(), isValid -> {
      if (isValid)
        getBinding().ivTFBiroJasaStatus.setImageResource(
            R.drawable.ic_baseline_check_circle_outline_24);
      else
        getBinding().ivTFBiroJasaStatus.setImageResource(
            R.drawable.ic_baseline_remove_circle_outline_24);
    });
  }

  protected void setField_BiroJasaAccountNo() {
    getTransferVm().getField_BiroJasaAccountNo().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFBiroJasaRekNo.setText(data == null ? "-" : data);
          getBinding().tvTFBiroJasaRekNo
              .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
        });
    // set ERROR
    getTransferVm().getField_BiroJasaAccountName_ERROR()
        .observe(getViewLifecycleOwner(), message -> {
          if (message != null) {
            getBinding().tvTFBiroJasaRekNo.setText(message);
            getBinding().tvTFBiroJasaRekNo.setTextColor(
                getContext().getResources().getColor(R.color.design_default_color_error));
          }
        });
  }

  protected void setField_FIFBankName() {
    getTransferVm().getField_FIFBank_LABEL().observe(getViewLifecycleOwner(),
        data -> {
          String bankName = getTransferVm().getField_FIFBankName();
          String finalBankName = bankName != null ? bankName : (data == null ? "-" : data);
          getBinding().tvTFFIFBankName.setText(finalBankName);
          getBinding().tvTFFIFBankName.setTextColor(
              getContext().getResources().getColor(R.color.app_secondaryColor));
        });

    // set ERROR
    getTransferVm().getField_FIFBank_LABEL_ERROR()
        .observe(getViewLifecycleOwner(), message -> {
          if (message != null) {
            getBinding().tvTFFIFBankName.setText(message);
            getBinding().tvTFFIFBankName.setTextColor(
                getContext().getResources().getColor(R.color.design_default_color_error));
          }
        });
  }

  protected void setField_FIFAccountName() {
    getTransferVm().getField_FIFAccountName().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFFIFAccountName.setText(data == null ? "-" : data);
          getBinding().tvTFFIFAccountName.setTextColor(
              getContext().getResources().getColor(R.color.app_secondaryColor));
        });

    // set ERROR
    getTransferVm().getField_FIFAccountName_ERROR()
        .observe(getViewLifecycleOwner(), message -> {
          if (message != null) {
            getBinding().tvTFFIFAccountName.setText(message);
            getBinding().tvTFFIFAccountName.setTextColor(
                getContext().getResources().getColor(R.color.design_default_color_error));
          }
        });
  }

  protected void setIvValid_FIF() {
    getTransferVm().isField_FIFValid().observe(getViewLifecycleOwner(), isValid -> {
      if (isValid)
        getBinding().ivTFFIFStatus.setImageResource(
            R.drawable.ic_baseline_check_circle_outline_24);
      else
        getBinding().ivTFFIFStatus.setImageResource(
            R.drawable.ic_baseline_remove_circle_outline_24);
    });
  }

  protected void setField_FIFAccountNo() {
    getTransferVm().getField_FIFAccountNo().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFFIFRekNo.setText(data == null ? "-" : data);
          getBinding().tvTFFIFRekNo.setTextColor(
              getContext().getResources().getColor(R.color.app_secondaryColor));
        });
    // set ERROR
    getTransferVm().getField_FIFAccountNo_ERROR()
        .observe(getViewLifecycleOwner(), message -> {
          if (message != null) {
            getBinding().tvTFFIFRekNo.setText(message);
            getBinding().tvTFFIFRekNo.setTextColor(
                getContext().getResources().getColor(R.color.design_default_color_error));
          }
        });
  }

  protected void setField_Batch() {
    //    TODO batch mana yang akan terselect pertama kali?
    batchAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    getBinding().spBatch.setAdapter(batchAdapter);
    getBinding().spBatch.setFocusable(false);
    getTransferVm().getBatch_Ld().observe(getViewLifecycleOwner(), dataList -> {
      List<KeyValueModel<BatchData, String>> kvmList = new ArrayList<>();
      for (BatchData data : dataList) {
        String timeString = data.getCode().equals(AppBookingTransferVM.REAL_TIME_CODE) ?
            AppBookingTransferVM.REAL_TIME_VALUE :
            data.getEndTime().toString();
        KeyValueModel<BatchData, String> kvm = new KeyValueModel<>(data, timeString);
        kvmList.add(kvm);
      }
      batchAdapter.addAll(kvmList);
      batchAdapter.notifyDataSetChanged();
    });

    // set selected batch
    getTransferVm().getField_Batch().observe(getViewLifecycleOwner(), data -> {
      KeyValueModel<BatchData, String> selectedBatch = (KeyValueModel<BatchData, String>)
          getBinding().spBatch.getSelectedItem();

      if (selectedBatch == null || (selectedBatch.getKey().getCode() != data.getKey().getCode())) {
        int pos = batchAdapter.getPosition(data);
        getBinding().spBatch.setSelection(pos);
      }
    });

    // set listener
    getBinding().spBatch.setOnItemClickListener(new MaterialSpinner.OnItemClickListener() {
      @Override
      public void onItemClick(@NotNull MaterialSpinner materialSpinner, @Nullable View view, int i,
          long l) {
        KeyValueModel<BatchData, String> kvm = batchAdapter.getItem(i);
        getTransferVm().setField_Batch(kvm);
      }
    });
  }

  @Override
  protected void setReadyFormState(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans, String message,
      Animation inAnim, Animation outAnim, boolean isFromAction) {
    if (isFromAction) {
      mBrowseVM.setRefreshList(true);
      NavController nc = Navigation.findNavController(getBinding().getRoot());
      if (getTransferVm().getTxnMode() != null) {
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(
            AppBookingTransferVM.FROM_TXN_PROCESS, true);
        nc.popBackStack();
      } else {
        nc.navigateUp();
      }
      return;
    } else
      super.setReadyFormState(smCash, smTrans, message, inAnim,
          outAnim, isFromAction);
  }

  @Override
  protected void setMode_generalField(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer, String message,
      Animation inAnimation, Animation outAnimation) {
    setTransferOperationalField(smTransfer);
    switch (smTransfer) {
    case READ_ONLY:
    case AOC_VERIFICATION_NEW_APPBOOKING:
    case AOC_VERIFICATION_EDIT_APPBOOKING:
    case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
    case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
      setMode_FieldTrans_READONLY();
      break;
    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
      setMode_FieldTrans_READONLY();
      getBinding().tilBatch.setVisibility(View.VISIBLE);
      getBinding().tilBatch.setEnabled(false);
      break;
    case KASIR_SUBMITTED_NEW_APPBOOKING:
    case KASIR_SUBMITTED_EDIT_APPBOOKING:
    case KASIR_REVISION_NEW_APPBOOKING:
    case KASIR_REVISION_EDIT_APPBOOKING:
      //#BOX2
      //set Enable nya ada di setFieldOutlet, karena ada logic yang membuat dia readOnly atau tidak
      getBinding().tilBookingDate.setEnabled(getTransferVm().isAllowBackDate());
      getBinding().spLOB.setEnabled(true);
      getBinding().spSO.setEnabled(true);
      setField_IdnpksfEnabledDisabled(true);
      getBinding().tilReason.setEnabled(true);

      //#BOX3
      getBinding().tilConsumerName.setEnabled(true);
      getBinding().tilConsumerAddress.setEnabled(true);
      getBinding().tilConsumerPhone.setEnabled(true);
      getBinding().tilVehicleNo.setEnabled(true);

      //#BOX4
      getBinding().cbTFOpenClose.setEnabled(true);
      getBinding().tilTFConsumer.setEnabled(true);
      getBinding().btnTFEditRekeningConsumer.setEnabled(true);
      getBinding().tilTFBiroJasa.setEnabled(true);
      getBinding().btnTFEditRekeningBiroJasa.setEnabled(true);
      getTransferVm().isField_isOpenClose().observe(getViewLifecycleOwner(), data -> {
        getBinding().tilTFFIF.setEnabled(data);
        getBinding().btnTFClearFIF.setEnabled(data);
        getBinding().btnTFEditRekeningFIF.setEnabled(data);
      });

      //      getBinding().cbTFOpenClose.setOnCheckedChangeListener(
      //          new CompoundButton.OnCheckedChangeListener() {
      //            @Override
      //            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
      //              getTransferVm().setField_IsOpenClose(isChecked, false);
      //              getBinding().tilTFFIF.setEnabled(isChecked);
      //              getBinding().btnTFEditRekeningFIF.setEnabled(isChecked);
      //              getBinding().btnTFClearFIF.setEnabled(isChecked);
      //              if (!isChecked)
      //                getTransferVm().setField_FIFAmount(BigDecimal.ZERO, false);
      //            }
      //          });

      //BOX5
      getBinding().tilFeeMatrix.setEnabled(true);
      getBinding().tilFeeScheme.setEnabled(true);

      //#BOX7
      getBinding().tvLabelCancelation.setVisibility(View.GONE);
      getBinding().tilCancelReason.setVisibility(View.GONE);
      getBinding().ivAtcHO.setVisibility(View.GONE);
      getBinding().tvLabelCancelation.setEnabled(false);
      getBinding().tilCancelReason.setEnabled(false);
      getBinding().tvLabelCancelation.setEnabled(false);
      getBinding().tilCancelReason.setEnabled(false);

      getBinding().btnTFTakeActionCons.setVisibility(View.GONE);
      getBinding().btnTFTakeActionBJ.setVisibility(View.GONE);
      getBinding().btnTFTakeActionFIF.setVisibility(View.GONE);
      break;
    case KASIR_SUBMITTED_CANCEL_APPBOOKING:
    case KASIR_REVISION_CANCEL_APPBOOKING:
      setMode_FieldTrans_READONLY();
      getBinding().tvLabelCancelation.setEnabled(true);
      getBinding().tilCancelReason.setEnabled(true);
      getBinding().ivAtcHO.setEnabled(true);

      //BOX7
      getBinding().tvLabelCancelation.setVisibility(View.VISIBLE);
      getBinding().tvLabelCancelation.setEnabled(true);
      getBinding().tilCancelReason.setVisibility(View.VISIBLE);
      getBinding().tilCancelReason.setEnabled(true);
      getBinding().ivAtcHO.setVisibility(View.VISIBLE);
      getBinding().ivAtcHO.setEnabled(true);
      break;
    case AOC_VERIFICATION_CANCEL_APPBOOKING:
    case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
    case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
    case FIN_RETUR_VERIFICATION_CANCEL_APPBOOKING:
      setMode_FieldTrans_READONLY();
      getBinding().tvLabelCancelation.setVisibility(View.VISIBLE);
      getBinding().tvLabelCancelation.setEnabled(false);
      getBinding().tilCancelReason.setVisibility(View.VISIBLE);
      getBinding().tilCancelReason.setEnabled(false);
      break;
    case TRANSACTIONS_MODE:
      setMode_TxnField();
      setMode_TxnConsForm();
      setMode_TxnBjForm();
      setMode_TxnFIFForm();
      break;
    default:
      String msg = "mode atau screenMode Halaman tidak diketahui ->" + smTransfer + "<-";
      HyperLog.w(getThisTag(), msg);
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      RuntimeException re = new RuntimeException(msg);
      ACRA.getErrorReporter().handleException(re);
      break;
    }
  }

  private void setMode_TxnConsForm() {
    getTransferVm().getTxnScreenModeCons().observe(getViewLifecycleOwner(), smTxn -> {
      boolean isShowProcessButton = true;
      switch (smTxn) {
      case KASIR_CUS_REVISION_NEW:
      case AOC_CUS_VERIFICATION_NEW:
      case CAPTAIN_CUS_VERIFICATION_NEW:
      case FIN_STAFF_CUS_VERIFICATION_NEW:
      case FIN_SPV_CUS_VERIFICATION_NEW:
      case KASIR_CUS_REVISION_EDIT:
      case AOC_CUS_VERIFICATION_EDIT:
      case CAPTAIN_CUS_VERIFICATION_EDIT:
      case DIRECTOR_CUS_VERIFICATION_EDIT:
      case FIN_STAFF_CUS_VERIFICATION_EDIT:
      case FIN_SPV_CUS_VERIFICATION_EDIT:
      case FIN_RETUR_CUS_VERIFICATION_EDIT:
        isShowProcessButton = true;
        break;
      case READ_ONLY:
        isShowProcessButton = false;
        break;
      default:
        isShowProcessButton = false;
        String msg = String.format("transactionModeConsumer sm[%s] Not Supported", smTxn);
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        HyperLog.exception(getThisTag(), msg);
        RuntimeException re = new RuntimeException(msg);
        ACRA.getErrorReporter().handleException(re);
        break;
      }

      // jika user masuk ke halaman kerja consumer, maka matikan tombol take action
      if (AppBookingTransferVM.TXN_CONSUMER.equals(getTransferVm().getTxnMode()))
        isShowProcessButton = false;

      if (isShowProcessButton)
        getBinding().btnTFTakeActionCons.setVisibility(View.VISIBLE);
      else
        getBinding().btnTFTakeActionCons.setVisibility(View.GONE);

      getBinding().btnTFTakeActionCons.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          NavController nc = Navigation.findNavController(getBinding().getRoot());
          AppBookingTransferNewFragmentDirections.ActionTxnMode dir = AppBookingTransferNewFragmentDirections.actionTxnMode(
              getTransferVm().getField_AppOperation(),
              getTransferVm().getField_AppBookingTypeId().getValue(),
              AppBookingTransferVM.TXN_CONSUMER, isHasCreatePermission);

          dir.setAppPBookingId(getTransferVm().getField_AppBookingId());
          nc.navigate(dir, NavHelper.animParentToChild().build());
        }
      });
    });
  }

  private void setMode_TxnBjForm() {
    getTransferVm().getTxnScreenModeBj().observe(getViewLifecycleOwner(), smTxn -> {
      boolean isShowProcessButton = true;
      switch (smTxn) {
      case KASIR_JASA_REVISION_NEW:
      case AOC_JASA_VERIFICATION_NEW:
      case CAPTAIN_JASA_VERIFICATION_NEW:
      case FIN_STAFF_JASA_VERIFICATION_NEW:
      case FIN_SPV_JASA_VERIFICATION_NEW:
      case KASIR_JASA_REVISION_EDIT:
      case AOC_JASA_VERIFICATION_EDIT:
      case CAPTAIN_JASA_VERIFICATION_EDIT:
      case DIRECTOR_JASA_VERIFICATION_EDIT:
      case FIN_STAFF_JASA_VERIFICATION_EDIT:
      case FIN_SPV_JASA_VERIFICATION_EDIT:
      case FIN_RETUR_JASA_VERIFICATION_EDIT:
        isShowProcessButton = true;
        break;
      case READ_ONLY:
        isShowProcessButton = false;
        break;
      default:
        isShowProcessButton = false;
        String msg = String.format("transactionModeBiroJasa sm[%s] Not Supported", smTxn);
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        HyperLog.exception(getThisTag(), msg);
        RuntimeException re = new RuntimeException(msg);
        ACRA.getErrorReporter().handleException(re);
        break;
      }

      // jika user masuk ke halaman kerja BiroJasa, maka matikan tombol take action
      if (AppBookingTransferVM.TXN_BIRO_JASA.equals(getTransferVm().getTxnMode()))
        isShowProcessButton = false;

      if (isShowProcessButton)
        getBinding().btnTFTakeActionBJ.setVisibility(View.VISIBLE);
      else
        getBinding().btnTFTakeActionBJ.setVisibility(View.GONE);

      getBinding().btnTFTakeActionBJ.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          NavController nc = Navigation.findNavController(getBinding().getRoot());
          AppBookingTransferNewFragmentDirections.ActionTxnMode dir = AppBookingTransferNewFragmentDirections.actionTxnMode(
              getTransferVm().getField_AppOperation(),
              getTransferVm().getField_AppBookingTypeId().getValue(),
              AppBookingTransferVM.TXN_BIRO_JASA, isHasCreatePermission);

          dir.setAppPBookingId(getTransferVm().getField_AppBookingId());
          nc.navigate(dir, NavHelper.animParentToChild().build());
        }
      });
    });
  }

  private void setMode_TxnFIFForm() {
    getTransferVm().getTxnScreenModeFIF().observe(getViewLifecycleOwner(), smTxn -> {
      boolean isShowProcessButton = true;
      switch (smTxn) {
      case KASIR_FIF_REVISION_NEW:
      case AOC_FIF_VERIFICATION_NEW:
      case CAPTAIN_FIF_VERIFICATION_NEW:
      case FIN_STAFF_FIF_VERIFICATION_NEW:
      case FIN_SPV_FIF_VERIFICATION_NEW:
      case KASIR_FIF_REVISION_EDIT:
      case AOC_FIF_VERIFICATION_EDIT:
      case CAPTAIN_FIF_VERIFICATION_EDIT:
      case DIRECTOR_FIF_VERIFICATION_EDIT:
      case FIN_STAFF_FIF_VERIFICATION_EDIT:
      case FIN_SPV_FIF_VERIFICATION_EDIT:
      case FIN_RETUR_FIF_VERIFICATION_EDIT:
        isShowProcessButton = true;
        break;
      case READ_ONLY:
        isShowProcessButton = false;
        break;
      default:
        isShowProcessButton = false;
        String msg = String.format("transactionModeFIF sm[%s] Not Supported", smTxn);
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        HyperLog.exception(getThisTag(), msg);
        RuntimeException re = new RuntimeException(msg);
        ACRA.getErrorReporter().handleException(re);
        break;
      }

      // jika user masuk ke halaman kerja FIF, maka matikan tombol take action
      if (AppBookingTransferVM.TXN_FIF.equals(getTransferVm().getTxnMode()))
        return;

      if (isShowProcessButton)
        getBinding().btnTFTakeActionFIF.setVisibility(View.VISIBLE);
      else
        getBinding().btnTFTakeActionFIF.setVisibility(View.GONE);

      getBinding().btnTFTakeActionFIF.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          NavController nc = Navigation.findNavController(getBinding().getRoot());
          AppBookingTransferNewFragmentDirections.ActionTxnMode dir = AppBookingTransferNewFragmentDirections.actionTxnMode(
              getTransferVm().getField_AppOperation(),
              getTransferVm().getField_AppBookingTypeId().getValue(),
              AppBookingTransferVM.TXN_FIF, isHasCreatePermission);

          dir.setAppPBookingId(getTransferVm().getField_AppBookingId());
          nc.navigate(dir, NavHelper.animParentToChild().build());
        }
      });
    });
  }

  @Override
  protected void setField_POMODE(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer) {
    getTransferVm().isEnable_PO().observe(getViewLifecycleOwner(), isEnable -> {
      if (isEnable) {
        super.setField_POMODE(smCash, smTransfer);
      } else {
        setField_PopReason(false);
        getBinding().btnSimpan.setVisibility(View.GONE);
        setField_POMODE2(false);
      }
    });
  }

  protected void setMode_TxnField() {
    //    getBinding().btnTFClearCons.setVisibility(View.GONE);
    getBinding().btnTFEditRekeningConsumer.setVisibility(View.GONE);
    //      getBinding().btnTFClearBiroJasa.setVisibility(View.GONE);
    getBinding().btnTFEditRekeningBiroJasa.setVisibility(View.GONE);
    //      getBinding().btnTFClearFIF.setVisibility(View.GONE);
    getBinding().btnTFEditRekeningFIF.setVisibility(View.GONE);
    getBinding().btnTFTakeActionCons.setVisibility(View.GONE);
    getBinding().btnTFTakeActionBJ.setVisibility(View.GONE);
    getBinding().btnTFTakeActionFIF.setVisibility(View.GONE);

    if (getTransferVm().getTxnMode() == null) {
      getBinding().tilTFConsumer.setEnabled(false);
      getBinding().btnTFClearCons.setEnabled(false);
      getBinding().btnTFEditRekeningConsumer.setEnabled(false);

      getBinding().tilTFBiroJasa.setEnabled(false);
      getBinding().btnTFClearBiroJasa.setEnabled(false);
      getBinding().btnTFEditRekeningBiroJasa.setEnabled(false);

      getBinding().tilTFFIF.setEnabled(false);
      getBinding().btnTFClearFIF.setEnabled(false);
      getBinding().btnTFEditRekeningFIF.setEnabled(false);

      getBinding().spOutlet.setEnabled(false);
      getBinding().tilBookingDate.setEnabled(false);

      getBinding().spLOB.setEnabled(false);
      getBinding().spSO.setEnabled(false);
      setField_IdnpksfEnabledDisabled(false);
      getBinding().tilReason.setEnabled(false);
      getBinding().tilCashReason.setEnabled(false);

      getBinding().tilConsumerName.setEnabled(false);
      getBinding().tilConsumerAddress.setEnabled(false);
      getBinding().tilConsumerPhone.setEnabled(false);
      getBinding().tilVehicleNo.setEnabled(false);

      getBinding().cbTFOpenClose.setEnabled(false);
      getBinding().tilIdnpksf.setEnabled(false);
      getBinding().tvLabelCancelation.setEnabled(false);
      getBinding().tilCancelReason.setEnabled(false);
      getBinding().tilFeeMatrix.setEnabled(false);
      getBinding().tilFeeScheme.setEnabled(false);
      return;
    }

    boolean isCons = AppBookingTransferVM.TXN_CONSUMER.equals(getTransferVm().getTxnMode());
    boolean isBJ = AppBookingTransferVM.TXN_BIRO_JASA.equals(getTransferVm().getTxnMode());
    boolean isFIF = AppBookingTransferVM.TXN_FIF.equals(getTransferVm().getTxnMode());
    ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
    if (isCons) {
      actionBar.setTitle(getString(R.string.title_mode_kerja_consumer));
      getBinding().wfContainerBj.setVisibility(View.GONE);
      getBinding().wfActionBj.setVisibility(View.GONE);
      getBinding().wfContainerFIF.setVisibility(View.GONE);
      getBinding().wfActionFIF.setVisibility(View.GONE);

      setField_WfTransferDate_Cons();
      getTransferVm().getField_ConsumerOtherBank().observe(getViewLifecycleOwner(), altAccVal -> {
        String fn = "getTransferVm().getField_ConsumerOtherBank() ";
        HyperLog.d(TAG, fn + "->altAccVal[" + altAccVal + "]");
        boolean isManualVal = getTransferVm().getField_Wf_Txn_IsManualCons();
        HyperLog.d(TAG, fn + "isManual[" + isManualVal + "]");
        HyperLog.d(TAG, fn + "altAccVal[" + altAccVal + "]");
        isManualVal = (!Objects.equals(altAccVal , null) && !isManualVal ? true : isManualVal);
        HyperLog.d(TAG, fn + "isManualFinal[" + isManualVal + "]");
        setWFBankAdapter2(isManualVal);
        getBinding().swWFManualCons.setChecked(isManualVal);
        setField_WfManualOtomatis_Cons(altAccVal);
        setField_WfBank_Cons();
      });

      getTransferVm().getTxnScreenModeCons().observe(getViewLifecycleOwner(), smTxn -> {
        switch (smTxn) {
        case KASIR_CUS_REVISION_NEW:
        case KASIR_CUS_REVISION_EDIT:
          //#BOX2
          //set Enable nya ada di setFieldOutlet, karena ada logic yang membuat dia readOnly atau tidak
          getBinding().tilBookingDate.setEnabled(getTransferVm().isAllowBackDate());
          getBinding().tilLOB.setEnabled(true);
          getBinding().spLOB.setEnabled(true);
          getBinding().spSO.setEnabled(true);
          setField_IdnpksfEnabledDisabled(true);
          getBinding().tilReason.setEnabled(true);

          //#BOX3
          getBinding().tilConsumerName.setVisibility(View.GONE);
          getBinding().tilConsumerName.setEnabled(true);
          getBinding().tilConsumerAddress.setEnabled(true);
          getBinding().tilConsumerPhone.setEnabled(true);
          getBinding().tilVehicleNo.setEnabled(true);

          //#BOX4
          getBinding().tilTFConsumer.setEnabled(true);
          getBinding().btnTFEditRekeningConsumer.setVisibility(View.VISIBLE);
          getBinding().btnTFEditRekeningConsumer.setEnabled(true);

          getBinding().tilTFBiroJasa.setEnabled(false);
          getBinding().btnTFClearBiroJasa.setVisibility(View.GONE);
          getBinding().btnTFEditRekeningBiroJasa.setVisibility(View.GONE);
          getBinding().btnTFEditRekeningBiroJasa.setEnabled(false);

          getBinding().cbTFOpenClose.setEnabled(false);
          getBinding().tilTFFIF.setEnabled(false);
          getBinding().btnTFClearFIF.setVisibility(View.GONE);
          getBinding().btnTFEditRekeningFIF.setEnabled(false);

          //BOX5
          getBinding().tilFeeMatrix.setEnabled(true);
          getBinding().tilFeeScheme.setEnabled(true);

          //#BOX7
          getBinding().tvLabelCancelation.setVisibility(View.GONE);
          getBinding().tilCancelReason.setVisibility(View.GONE);
          getBinding().ivAtcHO.setVisibility(View.GONE);
          getBinding().tvLabelCancelation.setEnabled(false);
          getBinding().tilCancelReason.setEnabled(false);
          getBinding().tvLabelCancelation.setEnabled(false);
          getBinding().tilCancelReason.setEnabled(false);
          getBinding().wfActionCons.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtCons().getPossibleOutcome(),
              getBinding().llWFButtonContainerCons, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case FIN_SPV_CUS_VERIFICATION_NEW:
        case FIN_SPV_CUS_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().wfContainerCons.setVisibility(View.VISIBLE);
          getBinding().tilWfDateCons.setEnabled(false);
          getBinding().swWFManualCons.setEnabled(false);
          getBinding().tilWfBankCons.setEnabled(false);
          getBinding().wfActionCons.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtCons().getPossibleOutcome(),
              getBinding().llWFButtonContainerCons, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case AOC_CUS_VERIFICATION_NEW:
        case AOC_CUS_VERIFICATION_EDIT:
        case FIN_RETUR_CUS_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().wfActionCons.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtCons().getPossibleOutcome(),
              getBinding().llWFButtonContainerCons, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case FIN_STAFF_CUS_VERIFICATION_NEW:
        case FIN_STAFF_CUS_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();

          getBinding().wfContainerCons.setVisibility(View.VISIBLE);
          getBinding().wfActionCons.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtCons().getPossibleOutcome(),
              getBinding().llWFButtonContainerCons, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case CAPTAIN_CUS_VERIFICATION_NEW:
        case CAPTAIN_CUS_VERIFICATION_EDIT:
          getBinding().tilBatch.setVisibility(View.VISIBLE);
          getBinding().tilBatch.setEnabled(false);
          setMode_FieldTrans_READONLY();
          getBinding().wfContainerCons.setVisibility(View.GONE);
          getBinding().wfActionCons.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtCons().getPossibleOutcome(),
              getBinding().llWFButtonContainerCons, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case DIRECTOR_CUS_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().wfContainerCons.setVisibility(View.GONE);
          getBinding().wfActionCons.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtCons().getPossibleOutcome(),
              getBinding().llWFButtonContainerCons, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        default:
          break;
        }
      });
    } else if (isBJ) {
      actionBar.setTitle(getString(R.string.title_mode_kerja_biro_jasa));
      getBinding().wfContainerCons.setVisibility(View.GONE);
      getBinding().wfActionCons.setVisibility(View.GONE);
      getBinding().wfContainerFIF.setVisibility(View.GONE);
      getBinding().wfActionFIF.setVisibility(View.GONE);

      setField_WfTransferDate_Bj();
      getTransferVm().getField_BiroJasaOtherBank().observe(getViewLifecycleOwner(), altAccVal -> {
        String fn = "getTransferVm().getField_BiroJasaOtherBank() ";
        boolean isManualVal = getTransferVm().getField_Wf_Txn_IsManualBj();
        HyperLog.d(TAG, fn + "isManualVal[" + isManualVal + "]");
        HyperLog.d(TAG, fn + "altAccVal[" + altAccVal + "]");
        isManualVal = (!Objects.equals(altAccVal, null) && !isManualVal ? true : isManualVal);
        HyperLog.d(TAG, fn + "isManualVal(final)[" + isManualVal + "]");
        setWFBankAdapter2(isManualVal);
        getBinding().swWFManualBj.setChecked(isManualVal);
        setField_WfManualOtomatis_Bj(altAccVal);
        setField_WfBank_Bj();
      });


      getTransferVm().getTxnScreenModeBj().observe(getViewLifecycleOwner(), smTxn -> {
        switch (smTxn) {
        case KASIR_JASA_REVISION_NEW:
        case KASIR_JASA_REVISION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().tilTFConsumer.setEnabled(false);
          getBinding().btnTFEditRekeningConsumer.setVisibility(View.GONE);
          getBinding().btnTFEditRekeningConsumer.setEnabled(false);

          getBinding().tilTFBiroJasa.setEnabled(true);
          getBinding().btnTFClearBiroJasa.setVisibility(View.VISIBLE);
          getBinding().btnTFClearBiroJasa.setEnabled(true);
          getBinding().btnTFEditRekeningBiroJasa.setVisibility(View.VISIBLE);
          getBinding().btnTFEditRekeningBiroJasa.setEnabled(true);

          getBinding().cbTFOpenClose.setEnabled(false);
          getBinding().tilTFFIF.setEnabled(false);
          getBinding().btnTFClearFIF.setVisibility(View.GONE);
          getBinding().btnTFEditRekeningFIF.setEnabled(false);

          getBinding().wfActionBj.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtBj().getPossibleOutcome(),
              getBinding().llWFButtonContainerBj, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case AOC_JASA_VERIFICATION_NEW:
        case AOC_JASA_VERIFICATION_EDIT:
        case FIN_RETUR_JASA_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().wfActionBj.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtBj().getPossibleOutcome(),
              getBinding().llWFButtonContainerBj, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case FIN_SPV_JASA_VERIFICATION_NEW:
        case FIN_SPV_JASA_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().wfContainerBj.setVisibility(View.VISIBLE);
          getBinding().tilWfDateBj.setEnabled(false);
          getBinding().swWFManualBj.setEnabled(false);
          getBinding().tilWfBankBj.setEnabled(false);
          getBinding().wfActionBj.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtBj().getPossibleOutcome(),
              getBinding().llWFButtonContainerBj, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, !getTransferVm().getField_Wf_Txn_IsManualBj());
          break;
        case CAPTAIN_JASA_VERIFICATION_NEW:
        case CAPTAIN_JASA_VERIFICATION_EDIT:
          getBinding().tilBatch.setVisibility(View.VISIBLE);
          getBinding().tilBatch.setEnabled(false);
          setMode_FieldTrans_READONLY();
          getBinding().wfContainerBj.setVisibility(View.GONE);
          getBinding().wfActionBj.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtBj().getPossibleOutcome(),
              getBinding().llWFButtonContainerBj, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case DIRECTOR_JASA_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().wfContainerBj.setVisibility(View.GONE);
          getBinding().wfActionBj.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtBj().getPossibleOutcome(),
              getBinding().llWFButtonContainerBj, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case FIN_STAFF_JASA_VERIFICATION_NEW:
        case FIN_STAFF_JASA_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();

          getBinding().wfContainerBj.setVisibility(View.VISIBLE);
          getBinding().wfActionBj.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtBj().getPossibleOutcome(),
              getBinding().llWFButtonContainerBj, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        default:
          break;
        }
      });
    } else if (isFIF) {
      actionBar.setTitle(getString(R.string.title_mode_kerja_finance));
      getBinding().wfContainerCons.setVisibility(View.GONE);
      getBinding().wfActionCons.setVisibility(View.GONE);
      getBinding().wfContainerBj.setVisibility(View.GONE);
      getBinding().wfActionBj.setVisibility(View.GONE);

      setField_WfTransferDate_FIF();
      getTransferVm().getField_FIFOtherBank().observe(getViewLifecycleOwner(), altAccVal -> {
        String fn = "getTransferVm().getField_FIFOtherBank() ";
        boolean isManualVal = getTransferVm().getField_Wf_Txn_IsManualFIF();
        HyperLog.d(TAG, fn + "isManualVal[" + isManualVal + "]");
        HyperLog.d(TAG, fn + "altAccVal[" + altAccVal + "]");
        isManualVal = (!Objects.equals(altAccVal, null) && !isManualVal ? true : isManualVal);
        HyperLog.d(TAG, fn + "isManualVal(final)[" + isManualVal + "]");
        setWFBankAdapter2(isManualVal);
        getBinding().swWFManualFIF.setChecked(isManualVal);
        setField_WfManualOtomatis_FIF(altAccVal);
        setField_WfBank_FIF();
      });

      getTransferVm().getTxnScreenModeFIF().observe(getViewLifecycleOwner(), smTxn -> {
        switch (smTxn) {
        case KASIR_FIF_REVISION_NEW:
        case KASIR_FIF_REVISION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().tilTFConsumer.setEnabled(false);
          getBinding().btnTFEditRekeningConsumer.setVisibility(View.GONE);
          getBinding().btnTFEditRekeningConsumer.setEnabled(false);

          getBinding().tilTFBiroJasa.setEnabled(false);
          getBinding().btnTFClearBiroJasa.setVisibility(View.GONE);
          getBinding().btnTFClearBiroJasa.setEnabled(false);
          getBinding().btnTFEditRekeningBiroJasa.setVisibility(View.GONE);
          getBinding().btnTFEditRekeningBiroJasa.setEnabled(false);

          getBinding().cbTFOpenClose.setEnabled(true);
          getBinding().tilTFFIF.setEnabled(true);
          getBinding().etTFFIF.setEnabled(true);
          getBinding().btnTFClearFIF.setVisibility(View.VISIBLE);
          getBinding().btnTFClearFIF.setEnabled(true);
          getBinding().btnTFEditRekeningFIF.setVisibility(View.VISIBLE);
          getBinding().btnTFEditRekeningFIF.setEnabled(true);
          getBinding().wfActionFIF.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtFIF().getPossibleOutcome(),
              getBinding().llWFButtonContainerFIF, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case CAPTAIN_FIF_VERIFICATION_NEW:
        case CAPTAIN_FIF_VERIFICATION_EDIT:
          getBinding().tilBatch.setVisibility(View.VISIBLE);
          getBinding().tilBatch.setEnabled(false);
          setMode_FieldTrans_READONLY();
          getBinding().wfContainerFIF.setVisibility(View.GONE);
          getBinding().wfActionFIF.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtFIF().getPossibleOutcome(),
              getBinding().llWFButtonContainerFIF, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case DIRECTOR_FIF_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().wfContainerFIF.setVisibility(View.GONE);
          getBinding().wfActionFIF.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtFIF().getPossibleOutcome(),
              getBinding().llWFButtonContainerFIF, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case FIN_STAFF_FIF_VERIFICATION_NEW:
        case FIN_STAFF_FIF_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().wfContainerFIF.setVisibility(View.VISIBLE);
          getBinding().wfActionFIF.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtFIF().getPossibleOutcome(),
              getBinding().llWFButtonContainerFIF, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case AOC_FIF_VERIFICATION_NEW:
        case AOC_FIF_VERIFICATION_EDIT:
        case FIN_RETUR_FIF_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().wfActionFIF.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtFIF().getPossibleOutcome(),
              getBinding().llWFButtonContainerFIF, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, false);
          break;
        case FIN_SPV_FIF_VERIFICATION_NEW:
        case FIN_SPV_FIF_VERIFICATION_EDIT:
          setMode_FieldTrans_READONLY();
          getBinding().wfContainerFIF.setVisibility(View.VISIBLE);
          getBinding().tilWfDateFIF.setEnabled(false);
          getBinding().swWFManualFIF.setEnabled(false);
          getBinding().tilWfBankFIF.setEnabled(false);
          getBinding().wfActionFIF.setVisibility(View.VISIBLE);

          Utility.renderCustomWfButton(getTransferVm().getPtFIF().getPossibleOutcome(),
              getBinding().llWFButtonContainerFIF, getLayoutInflater(), getActivity(),
              this::WfValidationProcess,this::handleWfAction_Txn, !getTransferVm().getField_Wf_Txn_IsManualFIF());
          break;
        default:
          break;
        }
      });
    }
  }

  @Override
  protected void setStandartButtonListener(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer) {
    getBinding().btnSimpan.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        switch (smTransfer) {
        case KASIR_SUBMITTED_NEW_APPBOOKING:
          getTransferVm().processSaveDraftNew(true);
          break;
        case KASIR_SUBMITTED_EDIT_APPBOOKING:
          getTransferVm().processSaveDraftEdit(true, false);
          break;
        case KASIR_SUBMITTED_CANCEL_APPBOOKING:
          getTransferVm().processSaveDraftCancel(false);
          break;
        case READ_ONLY:
          if (getTransferVm().getField_AppOperation().equals(Utility.OPERATION_NEW))
            getTransferVm().processSaveDraftNew(false);
          else if (getTransferVm().getField_AppOperation().equals(Utility.OPERATION_EDIT))
            getTransferVm().processSaveDraftEdit(false, false);
          else if (getTransferVm().getField_AppOperation().equals(Utility.OPERATION_CANCEL))
            getTransferVm().processSaveDraftCancel(false);
        default:
          break;
        }
      }
    });

    getBinding().btnSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        switch (smTransfer) {
        case KASIR_SUBMITTED_NEW_APPBOOKING:
          getTransferVm().processWfSubmitNew();
          break;
        case KASIR_SUBMITTED_EDIT_APPBOOKING:
          getTransferVm().processWfSubmitEdit(false);
          break;
        case KASIR_SUBMITTED_CANCEL_APPBOOKING:
          getTransferVm().processWfSubmitCancel(false);
          break;
        default:
          break;
        }
      }
    });

    getBinding().btnWfHistory.setOnClickListener(v -> {
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      AppBookingTransferNewFragmentDirections.ActionWfHistory dir = AppBookingTransferNewFragmentDirections
          .actionWfHistory(WfType.APP_BOOKING,
              getTransferVm().getField_AppBookingId());
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });
  }

  protected void showEditRekeningForm(String mode) {
    boolean isSupportedMode = true;
    boolean validStatus = false;
    String accName = null;
    String accNo = null;
    String otherBankName = null;
    UUID bankId = null;
    switch (mode) {
    case AppBookingTransferVM.TXN_CONSUMER:
      accName = getTransferVm().getField_ConsumerAccountName().getValue();
      accNo = getTransferVm().getField_ConsumerAccountNo().getValue();
      otherBankName = getTransferVm().getField_ConsumerOtherBank().getValue();
      bankId = getTransferVm().getField_ConsumerBankId().getValue();
      validStatus = getTransferVm().isField_ConsValid().getValue() != null ?
          getTransferVm().isField_ConsValid().getValue() :
          false;
      break;
    case AppBookingTransferVM.TXN_BIRO_JASA:
      accName = getTransferVm().getField_BiroJasaAccountName().getValue();
      accNo = getTransferVm().getField_BiroJasaAccountNo().getValue();
      otherBankName = getTransferVm().getField_BiroJasaOtherBank().getValue();
      bankId = getTransferVm().getField_BiroJasaBankId().getValue();
      validStatus = getTransferVm().isField_BjValid() != null ?
          getTransferVm().isField_BjValid().getValue() :
          false;
      break;
    case AppBookingTransferVM.TXN_FIF:
      accName = getTransferVm().getField_FIFAccountName().getValue();
      accNo = getTransferVm().getField_FIFAccountNo().getValue();
      otherBankName = getTransferVm().getField_FIFOtherBank().getValue();
      bankId = getTransferVm().getField_FIFBankId().getValue();
      validStatus = getTransferVm().isField_FIFValid() != null ?
          getTransferVm().isField_FIFValid().getValue() :
          false;
      break;
    default:
      Snackbar.make(getView(), "Mode tidak didukung", Snackbar.LENGTH_LONG).show();
      HyperLog.e(getThisTag(), "onDismis mode tidak diketahui->" + mode + "<-");
      isSupportedMode = false;
      break;
    }
    if (!isSupportedMode)
      return;

    NavController navController = Navigation.findNavController(getBinding().getRoot());

    if (getTransferVm().getField_outletKvm() == null) {
      Toast.makeText(getContext(), getString(R.string.mandatory_outlet), Toast.LENGTH_SHORT).show();
      return;
    } else if (getTransferVm().getField_outletKvm().getValue() == null) {
      Toast.makeText(getContext(), getString(R.string.mandatory_outlet), Toast.LENGTH_SHORT).show();
      return;
    }

    AppBookingTransferNewFragmentDirections.ActionTrxForm dir = AppBookingTransferNewFragmentDirections.actionTrxForm(
        getTransferVm().getField_outletKvm().getValue().getKey().getOutletId(),
        mode, getTransferVm().getField_CompanyId(), getTransferVm().getFromBookingStatus(),
        getTransferVm().getField_ConsumerName().getValue(), accName, accNo, bankId, otherBankName,
        validStatus);
    navController.navigate(dir, NavHelper.animChildToParent().build());
  }

  @Override
  protected void handleDocumentWfActionTransfer(String decision, String comment,
      AppBookingTransferScreenMode smTrans) {
    boolean isNotSupportedWF = false;
    switch (smTrans) {
    case READ_ONLY:
    case KASIR_REVISION_NEW_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getTransferVm().processTransWF_KASIR_SUBMIT_REVISION_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_VERIFICATION_NEW_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransWF_AOC_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransWF_AOC_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransWF_CAPTAIN_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransWF_CAPTAIN_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_VERIFICATION_EDIT_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransWF_AOC_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransWF_AOC_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransWF_AOC_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case KASIR_REVISION_EDIT_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getTransferVm().processTransWF_KASIR_SUBMIT_REVISION_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransWF_CAPTAIN_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransWF_CAPTAIN_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransWF_CAPTAIN_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransWF_DIRECTOR_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransWF_DIRECTOR_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransWF_DIRECTOR_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case KASIR_REVISION_CANCEL_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getTransferVm().processTransWF_KASIR_REVISION_CANCEL(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_VERIFICATION_CANCEL_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransWF_AOC_APPROVE_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransWF_AOC_RETURN_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransWF_AOC_REJECT_CANCEL(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransWF_CAPTAIN_APPROVE_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransWF_CAPTAIN_RETURN_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransWF_CAPTAIN_REJECT_CANCEL(comment);
      else
        isNotSupportedWF = true;
      break;
    case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransWF_DIRECTOR_APPROVE_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransWF_DIRECTOR_RETURN_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransWF_DIRECTOR_REJECT_CANCEL(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_RETUR_VERIFICATION_CANCEL_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransWF_FINRETUR_APPROVE_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransWF_FINRETUR_RETURN_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransWF_FINRETUR_REJECT_CANCEL(comment);
      else
        isNotSupportedWF = true;
      break;
    case TRANSACTIONS_MODE:
      isNotSupportedWF = false;
      break;
    default:
      isNotSupportedWF = true;
      break;
    }

    if (isNotSupportedWF) {
      String scMode = String.format("Screen Mode[%s] ", smTrans);
      String wf = String.format("wfOutCome[%s] ", decision);
      String msg = scMode + wf + " Not Supported";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      HyperLog.exception(getThisTag(), msg);
      RuntimeException re = new RuntimeException(msg);
      ACRA.getErrorReporter().handleException(re);
    }
  }

  protected void processDeleteImageAttachmentHO() {
//    Boolean isArchive = null;
    String attchMode = getTransferVm().getAttachMode();

//    if (attchMode.equals(UtilityState.ATTCH_HO.getId()))
//      isArchive = getVm().getField_Attach_HO_isArchive().getValue();
//    else {
//      String error = "mode tidak didukung attchmentMode[" + attchMode + "]";
//      Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
//      HyperLog.exception(TAG, error);
//      RuntimeException re = new RuntimeException(error);
//      ACRA.getErrorReporter().handleException(re);
//      return;
//    }
//
//    HyperLog.d(getThisTag(), attchMode + " processSetImageAttachmentHO isArchive[" + isArchive
//        + "], jika True maka return");
//    if(isArchive){
//      return;
//    }

    AppBookingFormState fs = getTransferVm().getFormState().getValue();
    if (fs == null)
      return;
    AppBookingTransferScreenMode screenMode = fs.getScreenModeTransfer();
    switch (screenMode) {
//    case READ_ONLY:
//    case KASIR_SUBMITTED_EDIT_APPBOOKING:
//    case KASIR_SUBMITTED_NEW_APPBOOKING:
//    case AOC_VERIFICATION_CANCEL_APPBOOKING:
//    case KASIR_REVISION_NEW_APPBOOKING:
//    case KASIR_REVISION_EDIT_APPBOOKING:
//    case AOC_VERIFICATION_EDIT_APPBOOKING:
//    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
//    case TRANSACTIONS_MODE:
//      // doNoting
//      break;
    case KASIR_SUBMITTED_CANCEL_APPBOOKING:
    case KASIR_REVISION_CANCEL_APPBOOKING:
      getTransferVm().setAttachment_Filled(false, true);
      break;
    default:
//      Snackbar.make(getView(), "NotSupported Permission ->" + screenMode, Snackbar.LENGTH_SHORT)
//          .show();
      break;
    }
  }

  protected void processSetImageAttachmentHO() {
//    Boolean isArchive = null;
    String attchMode = getTransferVm().getAttachMode();

//    if (attchMode.equals(UtilityState.ATTCH_HO.getId()))
//      isArchive = getVm().getField_Attach_HO_isArchive().getValue();
//    else {
//      String error = "mode tidak didukung attchmentMode[" + attchMode + "]";
//      Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
//      HyperLog.exception(TAG, error);
//      RuntimeException re = new RuntimeException(error);
//      ACRA.getErrorReporter().handleException(re);
//      return;
//    }
//
//    HyperLog.d(getThisTag(), attchMode + " processSetImageAttachmentHO isArchive[" + isArchive
//        + "], jika True maka return");
//    if(isArchive){
//      return;
//    }

    AppBookingFormState fs = getTransferVm().getFormState().getValue();
    if (fs == null)
      return;
    AppBookingTransferScreenMode smTransfer = fs.getScreenModeTransfer();
    switch (smTransfer) {
//    case READ_ONLY:
//    case KASIR_SUBMITTED_EDIT_APPBOOKING:
//    case KASIR_SUBMITTED_NEW_APPBOOKING:
//    case KASIR_REVISION_NEW_APPBOOKING:
//    case KASIR_REVISION_EDIT_APPBOOKING:
//    case AOC_VERIFICATION_CANCEL_APPBOOKING:
//    case AOC_VERIFICATION_EDIT_APPBOOKING:
//    case AOC_CUS_VERIFICATION_EDIT:
//    case AOC_CUS_VERIFICATION_NEW:
//    case AOC_FIF_VERIFICATION_EDIT:
//    case AOC_FIF_VERIFICATION_NEW:
//    case AOC_JASA_VERIFICATION_EDIT:
//    case AOC_JASA_VERIFICATION_NEW:
//    case AOC_VERIFICATION_NEW_APPBOOKING:
//    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
//    case DIRECTOR_CUS_VERIFICATION_EDIT:
//    case DIRECTOR_FIF_VERIFICATION_EDIT:
//    case DIRECTOR_JASA_VERIFICATION_EDIT:
//    case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
//    case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
//    case CAPTAIN_CUS_VERIFICATION_EDIT:
//    case CAPTAIN_CUS_VERIFICATION_NEW:
//    case CAPTAIN_FIF_VERIFICATION_EDIT:
//    case CAPTAIN_FIF_VERIFICATION_NEW:
//    case CAPTAIN_JASA_VERIFICATION_EDIT:
//    case CAPTAIN_JASA_VERIFICATION_NEW:
//    case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
//    case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
//      processSetImageAttachment2(attchMode, smTransfer, true, false);
//      break;
    case KASIR_SUBMITTED_CANCEL_APPBOOKING:
    case KASIR_REVISION_CANCEL_APPBOOKING:
      processSetImageAttachment2(attchMode, smTransfer, false, false);
      break;
//    case TRANSACTIONS_MODE:
//      processSetImageAttachment2(attchMode, smTransfer, true, true);
//      break;
    default:
      processSetImageAttachment2(attchMode, smTransfer, true, false);
      break;
    }
  }

  protected void processSetImageAttachment2(String fileName,
      AppBookingTransferScreenMode screenMode, boolean isViewOnly, boolean notSupported) {
    if (notSupported) {
      Snackbar.make(getView(), "NotSupported Permission ->" + screenMode, Snackbar.LENGTH_SHORT)
          .show();
      return;
    }

    if (getTransferVm().getAttach_FinalFile(getTransferVm().getAttachMode()) != null) {
      // kalau sudah ada image, maka tampilkan
      Uri attachmentUri = FileProvider.getUriForFile(getContext(),
          getContext().getApplicationContext().getPackageName() + ".provider",
          getTransferVm().getAttach_FinalFile(getTransferVm().getAttachMode()));
      HyperLog.d(TAG, "Action View: " + attachmentUri);
      Intent viewImage = new Intent(Intent.ACTION_VIEW, attachmentUri);
      viewImage.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_GRANT_READ_URI_PERMISSION);
      startActivity(viewImage);
    } else {
      if (isViewOnly)
        return;

      String[] opts = getResources().getStringArray(R.array.attachment_add_actions);
      AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
      builder.setItems(opts, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int item) {
          if (item == 0) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
              File photoFile = null;
              try {
                photoFile = ImageUtility.createTempFile(getContext().getCacheDir(), "temp",
                    fileName, ImageUtility.EXTENSION_JPG);
              } catch (IOException e) {
                HyperLog.e(getThisTag(), R.string.error_starting_camera + e.getMessage());
                Snackbar.make(getView(), R.string.error_starting_camera, Snackbar.LENGTH_SHORT)
                    .show();
              }
              if (photoFile != null) {
                getTransferVm().setAttach_TempFile(photoFile, getTransferVm().getAttachMode());
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                    getContext().getApplicationContext().getPackageName() + ".provider", photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, Utility.REQUEST_TAKE_PHOTO);
              }
            }
          } else if (item == 1) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto,
                Utility.REQUEST_PICK_PHOTO);
          } else {
            dialog.dismiss();
          }
        }
      });
      builder.show();
    }
  }

  protected void processDeleteImageAttachment() {
//    Boolean isArchive = null;
    String attchMode = getTransferVm().getAttachMode();

//    if (attchMode.equals(UtilityState.ATTCH_STNK.getId()))
//      isArchive = getVm().getField_Attach_STNK_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_KK.getId()))
//      isArchive = getVm().getField_Attach_KK_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_BPKB.getId()))
//      isArchive = getVm().getField_Attach_BPKB_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_SPT.getId()))
//      isArchive = getVm().getField_Attach_SPT_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_SERAHTERIMA.getId()))
//      isArchive = getVm().getField_Attach_SERAH_TERIMA_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_FISIKMOTOR.getId()))
//      isArchive = getVm().getField_Attach_FISIK_MOTOR_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_BUKTIRETUR.getId()))
//      isArchive = getVm().getField_Attach_BUKTIRETUR_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_HO.getId()))
//      isArchive = getVm().getField_Attach_HO_isArchive().getValue();
//    else{
//      String error = "mode tidak didukung attchmentMode[" + attchMode + "]";
//      Toast.makeText(getContext(),error,Toast.LENGTH_LONG).show();
//      HyperLog.exception(TAG, error);
//      RuntimeException re = new RuntimeException(error);
//      ACRA.getErrorReporter().handleException(re);
//      return;
//    }
//
//    HyperLog.d(getThisTag(), attchMode + " processDeleteImageAttachment isArchive[" + isArchive
//        + "], jika True maka return");
//    if(isArchive){
//      return;
//    }

    getTransferVm().setAttachMode(attchMode);
    AppBookingFormState fs = getTransferVm().getFormState().getValue();
    if (fs == null)
      return;
    AppBookingTransferScreenMode screenMode = fs.getScreenModeTransfer();
    switch (screenMode) {
    case READ_ONLY:
    case KASIR_SUBMITTED_CANCEL_APPBOOKING:
    case KASIR_REVISION_CANCEL_APPBOOKING:
    case AOC_VERIFICATION_CANCEL_APPBOOKING:
    case AOC_VERIFICATION_EDIT_APPBOOKING:
    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
      //--
      break;
    case KASIR_REVISION_NEW_APPBOOKING:
    case KASIR_REVISION_EDIT_APPBOOKING:
    case KASIR_SUBMITTED_NEW_APPBOOKING:
    case KASIR_SUBMITTED_EDIT_APPBOOKING:
      getTransferVm().setAttachment_Filled(false, true);
      break;
    default:
      Snackbar.make(getView(), "NotSupported Permission ->" + screenMode, Snackbar.LENGTH_SHORT)
          .show();
      break;
    }
  }

  protected void processSetImageAttachment() {
//    Boolean isArchive = null;
    String attchMode = getTransferVm().getAttachMode();
//    if (attchMode.equals(UtilityState.ATTCH_STNK.getId()))
//      isArchive = getVm().getField_Attach_STNK_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_KK.getId()))
//      isArchive = getVm().getField_Attach_KK_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_BPKB.getId()))
//      isArchive = getVm().getField_Attach_BPKB_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_SPT.getId()))
//      isArchive = getVm().getField_Attach_SPT_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_SERAHTERIMA.getId()))
//      isArchive = getVm().getField_Attach_SERAH_TERIMA_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_FISIKMOTOR.getId()))
//      isArchive = getVm().getField_Attach_FISIK_MOTOR_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_BUKTIRETUR.getId()))
//      isArchive = getVm().getField_Attach_BUKTIRETUR_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_HO.getId()))
//      isArchive = getVm().getField_Attach_HO_isArchive().getValue();
//    else {
//      String error = "mode tidak didukung attchmentMode[" + attchMode + "]";
//      Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
//      HyperLog.exception(TAG, error);
//      RuntimeException re = new RuntimeException(error);
//      ACRA.getErrorReporter().handleException(re);
//      return;
//    }
//
//    HyperLog.d(getThisTag(), attchMode + " processSetImageAttachment isArchive[" + isArchive
//        + "], jika True maka return");
//    if(isArchive){
//      return;
//    }
    AppBookingFormState fs = getTransferVm().getFormState().getValue();
    if (fs == null)
      return;
    AppBookingTransferScreenMode smTransfer = fs.getScreenModeTransfer();
    switch (smTransfer) {
//    case READ_ONLY:
//    case KASIR_SUBMITTED_CANCEL_APPBOOKING:
//    case KASIR_REVISION_CANCEL_APPBOOKING:
//    case AOC_VERIFICATION_NEW_APPBOOKING:
//    case AOC_VERIFICATION_EDIT_APPBOOKING:
//    case AOC_VERIFICATION_CANCEL_APPBOOKING:
//    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
//    case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
//    case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
//    case CAPTAIN_CUS_VERIFICATION_EDIT:
//    case CAPTAIN_CUS_VERIFICATION_NEW:
//    case CAPTAIN_FIF_VERIFICATION_EDIT:
//    case CAPTAIN_FIF_VERIFICATION_NEW:
//    case CAPTAIN_JASA_VERIFICATION_EDIT:
//    case CAPTAIN_JASA_VERIFICATION_NEW:
//    case AOC_CUS_VERIFICATION_EDIT:
//    case AOC_CUS_VERIFICATION_NEW:
//    case AOC_FIF_VERIFICATION_EDIT:
//    case AOC_FIF_VERIFICATION_NEW:
//    case AOC_JASA_VERIFICATION_EDIT:
//    case FIN_RETUR_VERIFICATION_CANCEL_APPBOOKING:
//    case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
//    case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
//    case DIRECTOR_CUS_VERIFICATION_EDIT:
//    case DIRECTOR_FIF_VERIFICATION_EDIT:
//    case DIRECTOR_JASA_VERIFICATION_EDIT:
//      processSetImageAttachment2(attchMode, smTransfer, true, false);
//      break;
    case TRANSACTIONS_MODE:
      if (AppBookingTransferVM.TXN_CONSUMER.equals(getTransferVm().getTxnMode())) {
        getTransferVm().getTxnScreenModeCons().observe(getViewLifecycleOwner(), smTxn -> {
          switch (smTxn) {
          case KASIR_CUS_REVISION_NEW:
          case KASIR_CUS_REVISION_EDIT:
            processSetImageAttachment2(attchMode, smTxn, false, false);
            break;
          default:
            processSetImageAttachment2(attchMode, smTxn, true, false);
            break;
          }
        });
      } else if (AppBookingTransferVM.TXN_BIRO_JASA.equals(getTransferVm().getTxnMode())) {
        getTransferVm().getTxnScreenModeBj().observe(getViewLifecycleOwner(), smTxn -> {
          Toast.makeText(getContext(), "belum ada implementasi", Toast.LENGTH_SHORT).show();
        });
      } else if (AppBookingTransferVM.TXN_FIF.equals(getTransferVm().getTxnMode())) {
        getTransferVm().getTxnScreenModeFIF().observe(getViewLifecycleOwner(), smTxn -> {
          Toast.makeText(getContext(), "belum ada implementasi", Toast.LENGTH_SHORT).show();
        });
      } else {
        processSetImageAttachment2(attchMode, smTransfer, true, true);
      }
      break;
    case KASIR_REVISION_NEW_APPBOOKING:
    case KASIR_REVISION_EDIT_APPBOOKING:
    case KASIR_SUBMITTED_NEW_APPBOOKING:
    case KASIR_SUBMITTED_EDIT_APPBOOKING:
      processSetImageAttachment2(attchMode, smTransfer, false, false);
      break;
    default:
      processSetImageAttachment2(attchMode, smTransfer, true, false);
      break;
    }
  }

  private void setField_WfTransferDate_Cons() {
    getBinding().tilWfDateCons.setEnabled(getTransferVm().isAllowBackDate());
    LocalDate date = getTransferVm().getField_Wf_Txn_DateCons();
    if (date == null) {
      getBinding().etWfDateCons.setText(LocalDate.now().format(Formatter.DTF_dd_MM_yyyy));
      int day = LocalDate.now().getDayOfMonth();
      int month = LocalDate.now().getMonthValue() - 1;
      int year = LocalDate.now().getYear();
      LocalDate dateVal = LocalDate.of(year, month + 1, day);
      getTransferVm().setField_Wf_Txn_DateCons(dateVal);
    } else {
      getBinding().etWfDateCons.setText(date.format(Formatter.DTF_dd_MM_yyyy));
    }

    getBinding().etWfDateCons.setOnClickListener(v -> {
      LocalDate dateVal = getTransferVm().getField_Wf_Txn_DateCons() != null
          ? getTransferVm().getField_Wf_Txn_DateCons() : LocalDate.now();
      int day = dateVal.getDayOfMonth();
      int month = dateVal.getMonthValue() - 1;
      int year = dateVal.getYear();
      DatePickerDialog dpdDate = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate dateVal = LocalDate.of(year, month + 1, dayOfMonth);
              getTransferVm().setField_Wf_Txn_DateCons(dateVal);
              getBinding().etWfDateCons.setText(dateVal.format(Formatter.DTF_dd_MM_yyyy));
            }
          }, year, month, day);
      // TODO PENAMBAHAN FUNGSI SEMENTARA, HARAP DIPERBAIKI MENGGUNAKA CLASS DatePickerUIHelper nanti
      dpdDate.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          getTransferVm().setField_Wf_Txn_DateCons(null);
          getBinding().etWfDateCons.setText(null);
        }
      });
      dpdDate.show();
    });
  }

  private void setField_WfManualOtomatis_Cons(String altAccVal) {
    getBinding().swWFManualCons.setOnCheckedChangeListener((buttonView, isManual) -> {
      String fn = "setField_WfManualOtomatis_Cons().swWFManualCons.setOnCheckedChangeListener() ";
      boolean isForceManual = (altAccVal != null) && !isManual;
      HyperLog.d(TAG, fn + "isForceManual[" + isForceManual + "]");
      if (isForceManual) {
        Toast.makeText(getContext(), getString(R.string.manual_transfer_recomended),
            Toast.LENGTH_LONG).show();
        getTransferVm().setField_Wf_Txn_IsManualCons(true);
        getBinding().swWFManualCons.setChecked(true);
        setWFBankAdapter2(true);
        getBinding().spWfBankCons.setSelection(-1);
        getTransferVm().setField_Wf_Txn_BankIdCons(null);
        return;
      }

      getTransferVm().setField_Wf_Txn_IsManualCons(isManual);
      setWFBankAdapter2(isManual);
      getBinding().spWfBankCons.setSelection(-1);
      getTransferVm().setField_Wf_Txn_BankIdCons(null);
    });
  }

  private void setField_WfBank_Cons() {
    getBinding().spWfBankCons.setAdapter(wfBankAdapter);
    KeyValueModel<AppBookingBankAccountData, String> bankCons = getTransferVm().getField_Wf_Txn_BankIdCons();
    int pos = !Objects.equals(bankCons, null) ? wfBankAdapter.getPosition(bankCons) : -1;
    getBinding().spWfBankCons.setSelection(pos);

    getBinding().spWfBankCons.setFocusable(false);
    getBinding().spWfBankCons.setOnItemSelectedListener(
        new MaterialSpinner.OnItemSelectedListener() {
          @Override
          public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
              int i, long l) {
            KeyValueModel<AppBookingBankAccountData, String> oKvm = wfBankAdapter.getItem(i);
            getTransferVm().setField_Wf_Txn_BankIdCons(oKvm);
          }

          @Override
          public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
            getTransferVm().setField_Wf_Txn_BankIdCons(null);
          }
        });
  }

  private void setField_WfTransferDate_Bj() {
    getBinding().tilWfDateBj.setEnabled(getTransferVm().isAllowBackDate());
    LocalDate date = getTransferVm().getField_Wf_Txn_DateBj();
      if (date == null) {
        getBinding().etWfDateBj.setText(LocalDate.now().format(Formatter.DTF_dd_MM_yyyy));
        int day = LocalDate.now().getDayOfMonth();
        int month = LocalDate.now().getMonthValue() - 1;
        int year = LocalDate.now().getYear();
        LocalDate dateVal = LocalDate.of(year, month + 1, day);
        getTransferVm().setField_Wf_Txn_DateBj(dateVal);
      } else {
        getBinding().etWfDateBj.setText(date.format(Formatter.DTF_dd_MM_yyyy));
      }

    getBinding().etWfDateBj.setOnClickListener(v -> {
      LocalDate dateVal = getTransferVm().getField_Wf_Txn_DateBj() != null
          ? getTransferVm().getField_Wf_Txn_DateBj() : LocalDate.now();
      int day = dateVal.getDayOfMonth();
      int month = dateVal.getMonthValue() - 1;
      int year = dateVal.getYear();
      DatePickerDialog dpdDate = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate dateVal = LocalDate.of(year, month + 1, dayOfMonth);
              getTransferVm().setField_Wf_Txn_DateBj(dateVal);
              getBinding().etWfDateBj.setText(dateVal.format(Formatter.DTF_dd_MM_yyyy));
            }
          }, year, month, day);
      // TODO PENAMBAHAN FUNGSI SEMENTARA, HARAP DIPERBAIKI MENGGUNAKA CLASS DatePickerUIHelper nanti
      dpdDate.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          getTransferVm().setField_Wf_Txn_DateBj(null);
          getBinding().etWfDateBj.setText(null);
        }
      });
      dpdDate.show();
    });
  }

  private void setField_WfManualOtomatis_Bj(String altAccVal) {
    getBinding().swWFManualBj.setOnCheckedChangeListener((buttonView, isManual) -> {
      String fn = "setField_WfManualOtomatis_Bj.setOnCheckedChangeListener() ";
      boolean isForceManual = (altAccVal != null) && !isManual;
      HyperLog.d(TAG, fn + "isForceManual[" + isForceManual + "]");
      if (isForceManual) {
        Toast.makeText(getContext(), getString(R.string.manual_transfer_recomended),
            Toast.LENGTH_LONG).show();
        getTransferVm().setField_Wf_Txn_IsManualBj(true);
        getBinding().swWFManualBj.setChecked(true);
        setWFBankAdapter2(true);
        getBinding().spWfBankBj.setSelection(-1);
        getTransferVm().setField_Wf_Txn_BankIdBj(null);
        return;
      }

      getTransferVm().setField_Wf_Txn_IsManualBj(isManual);
      setWFBankAdapter2(isManual);
      getBinding().spWfBankBj.setSelection(-1);
      getTransferVm().setField_Wf_Txn_BankIdBj(null);
    });
  }

  private void setField_WfBank_Bj() {
    getBinding().spWfBankBj.setAdapter(wfBankAdapter);
    KeyValueModel<AppBookingBankAccountData, String> bankBj = getTransferVm().getField_Wf_Txn_BankIdBj();
    int pos = !Objects.equals(bankBj, null) ? wfBankAdapter.getPosition(bankBj) : -1;
    getBinding().spWfBankBj.setSelection(pos);

    getBinding().spWfBankBj.setFocusable(false);
    getBinding().spWfBankBj.setOnItemSelectedListener(
        new MaterialSpinner.OnItemSelectedListener() {
          @Override
          public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
              int i, long l) {
            KeyValueModel<AppBookingBankAccountData, String> oKvm = wfBankAdapter.getItem(i);
            getTransferVm().setField_Wf_Txn_BankIdBj(oKvm);
          }

          @Override
          public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
            getTransferVm().setField_Wf_Txn_BankIdBj(null);
          }
        });
  }

  private void setField_WfTransferDate_FIF() {
    getBinding().tilWfDateFIF.setEnabled(getTransferVm().isAllowBackDate());
    LocalDate date = getTransferVm().getField_Wf_Txn_DateFIF();
      if (date == null) {
        getBinding().etWfDateFIF.setText(LocalDate.now().format(Formatter.DTF_dd_MM_yyyy));
        int day = LocalDate.now().getDayOfMonth();
        int month = LocalDate.now().getMonthValue() - 1;
        int year = LocalDate.now().getYear();
        LocalDate dateVal = LocalDate.of(year, month + 1, day);
        getTransferVm().setField_Wf_Txn_DateFIF(dateVal);
      } else {
        getBinding().etWfDateFIF.setText(date.format(Formatter.DTF_dd_MM_yyyy));
      }

    getBinding().etWfDateFIF.setOnClickListener(v -> {
      LocalDate dateVal = getTransferVm().getField_Wf_Txn_DateFIF() != null
          ? getTransferVm().getField_Wf_Txn_DateFIF() : LocalDate.now();
      int day = dateVal.getDayOfMonth();
      int month = dateVal.getMonthValue() - 1;
      int year = dateVal.getYear();
      DatePickerDialog dpdDate = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate dateVal = LocalDate.of(year, month + 1, dayOfMonth);
              getTransferVm().setField_Wf_Txn_DateFIF(dateVal);
              getBinding().etWfDateFIF.setText(dateVal.format(Formatter.DTF_dd_MM_yyyy));
            }
          }, year, month, day);
      // TODO PENAMBAHAN FUNGSI SEMENTARA, HARAP DIPERBAIKI MENGGUNAKA CLASS DatePickerUIHelper nanti
      dpdDate.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          getTransferVm().setField_Wf_Txn_DateFIF(null);
          getBinding().etWfDateFIF.setText(null);
        }
      });
      dpdDate.show();
    });
  }

  private void setField_WfManualOtomatis_FIF(String altAccVal) {
      getBinding().swWFManualFIF.setOnCheckedChangeListener((buttonView, isManual) -> {
        String fn = "swWFManualFIF.setOnCheckedChangeListener() ";
        boolean isForceManual = (altAccVal != null) && !isManual;
        HyperLog.d(TAG, fn + "isForceManual[" + isForceManual + "]");
        if (isForceManual) {
          Toast.makeText(getContext(), getString(R.string.manual_transfer_recomended),
              Toast.LENGTH_LONG).show();
          getTransferVm().setField_Wf_Txn_IsManualFIF(true);
          getBinding().swWFManualFIF.setChecked(true);
          setWFBankAdapter2(true);
          getBinding().spWfBankFIF.setSelection(-1);
          getTransferVm().setField_Wf_Txn_BankIdFIF(null);
          return;
        }

        getTransferVm().setField_Wf_Txn_IsManualFIF(isManual);
        setWFBankAdapter2(isManual);
        getBinding().spWfBankFIF.setSelection(-1);
        getTransferVm().setField_Wf_Txn_BankIdFIF(null);
      });
  }

  private void setField_WfBank_FIF() {
    getBinding().spWfBankFIF.setAdapter(wfBankAdapter);
    KeyValueModel<AppBookingBankAccountData, String> bankFIF = getTransferVm().getField_Wf_Txn_BankIdFIF();
    int pos = !Objects.equals(bankFIF, null) ? wfBankAdapter.getPosition(bankFIF) : -1;
    getBinding().spWfBankFIF.setSelection(pos);

    getBinding().spWfBankFIF.setFocusable(false);
    getBinding().spWfBankFIF.setOnItemSelectedListener(
        new MaterialSpinner.OnItemSelectedListener() {
          @Override
          public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
              int i, long l) {
            KeyValueModel<AppBookingBankAccountData, String> oKvm = wfBankAdapter.getItem(i);
            getTransferVm().setField_Wf_Txn_BankIdFIF(oKvm);
          }

          @Override
          public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
            getTransferVm().setField_Wf_Txn_BankIdFIF(null);
          }
        });
  }

  private void setWFBankAdapter2(boolean isManual){
    if(wfBankAdapter == null)
      wfBankAdapter = new ArrayAdapter<>(getContext(),android.R.layout.select_dialog_item);
    wfBankAdapter.clear();
    for (KeyValueModel<AppBookingBankAccountData, String> kvm : getTransferVm().getTxnBankAccountDataList()) {
      if (isManual == kvm.getKey().isManual())
        wfBankAdapter.addAll(kvm);
    }
    wfBankAdapter.notifyDataSetChanged();
  }

  private void handleWfAction_Txn(WfProcessParameter paramObj) {
    String fn = "handleWfAction_Txn() ";
    String decision = paramObj.getDecision();
    String comment = paramObj.getComment();
    String otpCode = paramObj.getOtp();
    boolean isNotSupportedWF = false;

    AppBookingTransferScreenMode mode = getScreenMode(fn,decision);
    if(mode == null)
      return;

    switch (mode) {
    case KASIR_CUS_REVISION_NEW:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getTransferVm().processTransWF_KASIR_CUS_REVISION_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_CUS_VERIFICATION_NEW:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_CUS_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_CUS_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_CUS_VERIFICATION_NEW:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_CUS_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_CUS_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_STAFF_CUS_VERIFICATION_NEW:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_CUS_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_CUS_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_SPV_CUS_VERIFICATION_NEW:
      if (WorkflowConstants.WF_OUTCOME_REQ_OTP.equals(decision)) {
        getTransferVm().processGenerateOTP_AppBookingTransferCustomer();
        Toast.makeText(getContext(), getString(R.string.msg_request_otp), Toast.LENGTH_LONG).show();
      } else if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)) {
        getTransferVm().processTransTxn_WF_FINSPV_CUS_APPROVE_NEW(comment, otpCode);
      }else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision)) {
        getTransferVm().processTransTxn_WF_FINSPV_CUS_RETURN_NEW(comment);
      }else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision)) {
        getTransferVm().processTransTxn_WF_FINSPV_CUS_REJECT_NEW(comment);
      }else {
        isNotSupportedWF = true;
      }
      break;
    case KASIR_JASA_REVISION_NEW:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getTransferVm().processTransWF_KASIR_BJ_REVISION_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_JASA_VERIFICATION_NEW:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_BJ_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_BJ_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_JASA_VERIFICATION_NEW:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_BJ_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_BJ_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_STAFF_JASA_VERIFICATION_NEW:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_BJ_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_BJ_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_SPV_JASA_VERIFICATION_NEW:
      if(WorkflowConstants.WF_OUTCOME_REQ_OTP.equals(decision)) {
        getTransferVm().processGenerateOTP_AppBookingTransferBiroJasa();
        Toast.makeText(getContext(), getString(R.string.msg_request_otp), Toast.LENGTH_LONG).show();
      }else if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_BJ_APPROVE_NEW(comment, otpCode);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_BJ_RETURN_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_BJ_REJECT_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_STAFF_FIF_VERIFICATION_NEW:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_FIF_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_FIF_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_FIF_VERIFICATION_NEW:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_FIF_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_FIF_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case KASIR_FIF_REVISION_NEW:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getTransferVm().processTransWF_KASIR_FIF_REVISION_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_FIF_VERIFICATION_NEW:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_FIF_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_FIF_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_SPV_FIF_VERIFICATION_NEW:
      if(WorkflowConstants.WF_OUTCOME_REQ_OTP.equals(decision)){
        getTransferVm().processGenerateOTP_AppBookingTransferFIF();
        Toast.makeText(getContext(), getString(R.string.msg_request_otp), Toast.LENGTH_LONG).show();
      }else if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_FIF_APPROVE_NEW(comment, otpCode);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_FIF_RETURN_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_FIF_REJECT_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_STAFF_CUS_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_CUS_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_CUS_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_CUS_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case DIRECTOR_CUS_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_DIRECTOR_CUS_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_DIRECTOR_CUS_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_DIRECTOR_CUS_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case KASIR_CUS_REVISION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getTransferVm().processTransWF_KASIR_CUS_REVISION_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_CUS_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_CUS_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_CUS_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_CUS_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_CUS_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_CUS_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_CUS_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_CUS_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_SPV_CUS_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_REQ_OTP.equals(decision)) {
        getTransferVm().processGenerateOTP_AppBookingTransferCustomer();
        Toast.makeText(getContext(), getString(R.string.msg_request_otp), Toast.LENGTH_LONG).show();
      }else if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_CUS_APPROVE_EDIT(comment, otpCode);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_CUS_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_CUS_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_STAFF_JASA_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_BJ_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_BJ_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_BJ_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case DIRECTOR_JASA_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_DIRECTOR_BJ_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_DIRECTOR_BJ_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_DIRECTOR_BJ_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case KASIR_JASA_REVISION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getTransferVm().processTransWF_KASIR_BJ_REVISION_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_JASA_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_BJ_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_BJ_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_BJ_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_JASA_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_BJ_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_BJ_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_BJ_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_SPV_JASA_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_REQ_OTP.equals(decision)) {
        getTransferVm().processGenerateOTP_AppBookingTransferBiroJasa();
        Toast.makeText(getContext(), getString(R.string.msg_request_otp), Toast.LENGTH_LONG).show();
      }if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_BJ_APPROVE_EDIT(comment, otpCode);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_BJ_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_BJ_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_STAFF_FIF_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_FIF_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_FIF_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINST_FIF_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case DIRECTOR_FIF_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_DIRECTOR_FIF_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_DIRECTOR_FIF_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_DIRECTOR_FIF_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case KASIR_FIF_REVISION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getTransferVm().processTransWF_KASIR_FIF_REVISION_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_FIF_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_FIF_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_FIF_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_AOC_FIF_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_FIF_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_FIF_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_FIF_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_CAPTAIN_FIF_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_SPV_FIF_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_REQ_OTP.equals(decision)) {
        getTransferVm().processGenerateOTP_AppBookingTransferFIF();
        Toast.makeText(getContext(), getString(R.string.msg_request_otp), Toast.LENGTH_LONG).show();
      } else if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_FIF_APPROVE_EDIT(comment, otpCode);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_FIF_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINSPV_FIF_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_RETUR_CUS_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINRETUR_CUS_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINRETUR_CUS_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINRETUR_CUS_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_RETUR_JASA_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINRETUR_BJ_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINRETUR_BJ_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINRETUR_BJ_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case FIN_RETUR_FIF_VERIFICATION_EDIT:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getTransferVm().processTransTxn_WF_FINRETUR_FIF_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getTransferVm().processTransTxn_WF_FINRETUR_FIF_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getTransferVm().processTransTxn_WF_FINRETUR_FIF_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    default:
      isNotSupportedWF = true;
      break;
    }

    if (isNotSupportedWF) {
      String scMode = String.format("Screen Mode[%s] ", mode);
      String wf = String.format("wfOutCome[%s] ", decision);
      String msg = scMode + wf + " Not Supported";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      HyperLog.w(getThisTag(), msg);
      msg = getThisTag() + " handleWfAction_Txn() " + msg;
      RuntimeException re = new RuntimeException(msg);
      ACRA.getErrorReporter().handleException(re);
    }
  }

  private Boolean WfValidationProcess(WfProcessParameter paramObj) {
    String fn = "WfValidationProcess ";
    String decision = paramObj.getDecision();
    AppBookingTransferScreenMode mode = getScreenMode(fn, decision);
    if (mode == null)
      return false;

    switch (mode) {
    case FIN_STAFF_CUS_VERIFICATION_EDIT:
    case FIN_STAFF_CUS_VERIFICATION_NEW:
    case FIN_STAFF_FIF_VERIFICATION_EDIT:
    case FIN_STAFF_FIF_VERIFICATION_NEW:
    case FIN_STAFF_JASA_VERIFICATION_EDIT:
    case FIN_STAFF_JASA_VERIFICATION_NEW:
      boolean isAuto = !getTransferVm().getField_Wf_Txn_IsManualCons();
      LocalDate txnDate = null;
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision) && isAuto) {

        if (Objects.equals(mode, AppBookingTransferScreenMode.FIN_STAFF_CUS_VERIFICATION_EDIT) ||
            Objects.equals(mode, AppBookingTransferScreenMode.FIN_STAFF_CUS_VERIFICATION_NEW))
          txnDate = getTransferVm().getField_Wf_Txn_DateCons();
        else if (
            Objects.equals(mode, AppBookingTransferScreenMode.FIN_STAFF_FIF_VERIFICATION_EDIT) ||
                Objects.equals(mode, AppBookingTransferScreenMode.FIN_STAFF_FIF_VERIFICATION_NEW))
          txnDate = getTransferVm().getField_Wf_Txn_DateFIF();
        else if (
            Objects.equals(mode, AppBookingTransferScreenMode.FIN_STAFF_JASA_VERIFICATION_EDIT) ||
                Objects.equals(mode,
                    AppBookingTransferScreenMode.FIN_STAFF_JASA_VERIFICATION_NEW))
          txnDate = getTransferVm().getField_Wf_Txn_DateBj();

        LocalDate currentDate = LocalDate.now();
        Log.d(TAG, fn + mode + "[" + txnDate + "] currentDate[" + currentDate + "]");
        if (!Objects.equals(currentDate, txnDate)) {
          String msg = getContext().getString(R.string.msg_not_supported_backdate_automatic_trx);
          Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
          return false;
        }
      }
      return true;
//    case FIN_SPV_CUS_VERIFICATION_EDIT:
//    case FIN_SPV_CUS_VERIFICATION_NEW:
//    case FIN_SPV_FIF_VERIFICATION_EDIT:
//    case FIN_SPV_FIF_VERIFICATION_NEW:
//    case FIN_SPV_JASA_VERIFICATION_EDIT:
//    case FIN_SPV_JASA_VERIFICATION_NEW:
//      return false;
    default:
      return true;
    }

  }

  private AppBookingTransferScreenMode getScreenMode(@NonNull String parentFn, String decision) {
    String fn = "getScreenMode() ";
    boolean isCons = AppBookingTransferVM.TXN_CONSUMER.equals(getTransferVm().getTxnMode());
    boolean isBJ = AppBookingTransferVM.TXN_BIRO_JASA.equals(getTransferVm().getTxnMode());
    boolean isFIF = AppBookingTransferVM.TXN_FIF.equals(getTransferVm().getTxnMode());
    AppBookingTransferScreenMode mode = null;
    if (isCons) {
      mode = getTransferVm().getTxnScreenModeCons().getValue();
    } else if (isBJ) {
      mode = getTransferVm().getTxnScreenModeBj().getValue();
    } else if (isFIF) {
      mode = getTransferVm().getTxnScreenModeFIF().getValue();
    } else {
      String scMode = String.format("Screen Mode[%s] ", mode);
      String wf = String.format("wfOutCome[%s] ", decision);
      String msg = scMode + wf + " Not Supported";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      HyperLog.w(getThisTag(), msg);
      msg = getThisTag() + parentFn + fn + msg;
      RuntimeException re = new RuntimeException(msg);
      ACRA.getErrorReporter().handleException(re);
      return null;
    }
    return mode;
  }
}
