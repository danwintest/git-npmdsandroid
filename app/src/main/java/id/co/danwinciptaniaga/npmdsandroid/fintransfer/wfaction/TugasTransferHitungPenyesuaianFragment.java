package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import java.math.BigDecimal;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentTugasTransferHitungPenyesuaianBinding;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterFragment;

public class TugasTransferHitungPenyesuaianFragment extends BottomSheetDialogFragment {
  public static final String TUGAS_TRANSFER_PENYESUAIAN_FIELD = "TUGAS_TRANSFER_PENYESUAIAN_FIELD";
  FragmentTugasTransferHitungPenyesuaianBinding binding;

  public TugasTransferHitungPenyesuaianFragment() {
    // Required empty public constructor
  }

  public static TugasTransferHitungPenyesuaianFragment newInstance(String param1, String param2) {
    TugasTransferHitungPenyesuaianFragment fragment = new TugasTransferHitungPenyesuaianFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    TugasTransferHitungPenyesuaianFragmentArgs args = TugasTransferHitungPenyesuaianFragmentArgs.fromBundle(
        getArguments());
    binding = FragmentTugasTransferHitungPenyesuaianBinding.inflate(inflater, container, false);
    setFieldValPercentage(args.getPenyesuaianPercentage());
    setButtonHitung();
    return binding.getRoot();
  }

  private void setFieldValPercentage(BigDecimal val) {
    binding.etPenyesuaianPercentage.setText(val.toString());
  }

  private void setButtonHitung() {
    binding.btnCalculate.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String valString = binding.etPenyesuaianPercentage.getText().toString();
        BigDecimal allowanceRate = BigDecimal.ZERO;
        if (!valString.isEmpty()) {
          int allowanceRate_Int=Integer.parseInt(valString);
          allowanceRate = BigDecimal.valueOf(allowanceRate_Int);
        }

        NavController nc = NavHostFragment.findNavController(
            TugasTransferHitungPenyesuaianFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(
            TUGAS_TRANSFER_PENYESUAIAN_FIELD, allowanceRate);
        nc.popBackStack();
      }
    });
  }
}