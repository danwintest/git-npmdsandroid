package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

import org.acra.ACRA;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypertrack.hyperlog.HyperLog;

import android.os.Bundle;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentDroppingDailyTaskWfBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfProcessParameter;

@AndroidEntryPoint
public class DroppingDailyTaskWfFragment extends Fragment {
  private final static String TAG = DroppingDailyTaskWfFragment.class.getSimpleName();
  FragmentDroppingDailyTaskWfBinding binding;
  private DroppingDailyTaskWFDataAdapter adapter;
  private DroppingDailyTaskWfVM vm;
  private RecyclerView rv;

  @Inject
  AppExecutors appExecutors;

  public DroppingDailyTaskWfFragment() {
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentDroppingDailyTaskWfBinding.inflate(inflater, container, false);
    vm = new ViewModelProvider(this).get(DroppingDailyTaskWfVM.class);

    DroppingDailyTaskWfFragmentArgs args = DroppingDailyTaskWfFragmentArgs.fromBundle(
        getArguments());
    vm.setDataList(getDataList(args));
    vm.setDecisionList(getDecisionList(args));

    setAdapterData();
    setWFButtonProcess();
    setFormStateObservable();
    return binding.getRoot();
  }

  private void setFormStateObservable() {
    vm.getFormState().observe(getViewLifecycleOwner(), state -> {
      switch (state.getState()) {
      case LOADING:
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
        break;
      case READY:
        Toast.makeText(getContext(),state.getMessage(),Toast.LENGTH_LONG).show();
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        NavController nc = NavHostFragment.findNavController(DroppingDailyTaskWfFragment.this);
//        nc.getPreviousBackStackEntry().getSavedStateHandle().set(
//            ARG_DROPPING_DAILY_DETAIL_FILTER, vm.getFilterField());
        nc.popBackStack();
        break;
      case ERROR:
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        Toast.makeText(getContext(),state.getMessage(),Toast.LENGTH_LONG).show();
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        break;
      default:
        break;
      }
    });
  }

  private List<DroppingDailyTaskBrowseDetailData> getDataList(DroppingDailyTaskWfFragmentArgs args) {
    Type dataType = new TypeToken<List<DroppingDailyTaskBrowseDetailData>>() {
    }.getType();
    String dataString = args.getDataList();
    List<DroppingDailyTaskBrowseDetailData> dataList = new Gson().fromJson(dataString, dataType);
    return dataList;
  }

  private List<String> getDecisionList(DroppingDailyTaskWfFragmentArgs args){
    Type dataType = new TypeToken<List<String>>() {
    }.getType();
    String decisionString = args.getDecision();
    List<String> decisionList = new Gson().fromJson(decisionString, dataType);
    return decisionList;
  }

  private void setAdapterData() {
    rv = binding.rvList;
    adapter = new DroppingDailyTaskWFDataAdapter(getContext(), vm.getDataList());
//    adapter = new DroppingDailyTaskWFDataAdapter(getContext(), vm.getDataList(),
//        new DroppingDailyTaskWFDataAdapter.WfItemListener() {
//          @Override
//          public void onDroppingAmtChange(String amtString, int pos) {
//            DroppingDailyTaskBrowseDetailData data = vm.getDataList().get(pos);
//            data.setDroppingAmt(Long.parseLong(amtString));
//            vm.getDataList().set(pos,data);
//          }
//        });
        rv.setAdapter(adapter);
  }

  private void setWFButtonProcess() {
    binding.llWorkflowButtonContainer.setVisibility(View.VISIBLE);
    Utility.renderCustomWfButton(vm.getDecisionList(), binding.llWorkflowButtonContainer,
        getLayoutInflater(), getActivity(),this::WfValidationProcess, this::handleWfAction, false);
  }

  private boolean checkMandatoryField(List<DroppingDailyTaskBrowseDetailData> dataList){
    StringBuilder sb = new StringBuilder();
    int totalError = 0;
    for(DroppingDailyTaskBrowseDetailData data : dataList){
      if(data.getDroppingAmt()==null || (data.getDroppingAmt()!=null && data.getDroppingAmt()<=0)){
        sb.append(data.getTrxNo() + ", ");
        totalError ++;
      }
    }
    if(totalError>0) {
      String finalErrorMsg = getString(R.string.msg_nilai_disetujui_tidak_boleh_kosong);
      finalErrorMsg = finalErrorMsg + "Pada Nomor Transaksi : " + sb.toString();
      Toast.makeText(getContext(),finalErrorMsg,Toast.LENGTH_LONG).show();
      return false;
    }
    return true;

  }

  private void handleWfAction(WfProcessParameter paramObj) {

    if (!checkMandatoryField(adapter.getDataList()))
      return;

    String decision = paramObj.getDecision();
    String comment = paramObj.getComment();
    if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision)){
      vm.processApprove(adapter.getDataList(), comment);
    }else if(WorkflowConstants.WF_OUTCOME_REJECT.equals(decision)){
      vm.processReject(adapter.getDataList(), comment);
    }else{
      String wf = String.format("wfOutCome[%s] ", decision);
      String msg = "Workflow" + wf + " Not Supported";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      HyperLog.w(TAG, msg);
      msg = TAG + " handleWfAction_Txn() " + msg;
      RuntimeException re = new RuntimeException(msg);
      ACRA.getErrorReporter().handleException(re);
    }

  }
  private Boolean WfValidationProcess(WfProcessParameter paramObj){
    return true;
  }

}