package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import android.content.Context;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceData;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceReportData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiOutletBalanceBinding;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiOutletFintransactionBinding;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalListAdapter;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

public class OutletBalanceReportAdapter extends
    ListAdapter<OutletBalanceReportData, OutletBalanceReportAdapter.ViewHolder> {
  private final static String TAG = OutletBalanceReportAdapter.class.getSimpleName();

  private Context mContext;
  private HashMap<Integer, BigDecimal> balanceMap = new HashMap<>();

  @Inject
  public OutletBalanceReportAdapter(Context context) {
    super(new DiffCallBack());
    mContext = context;
  }

  @Override
  public void submitList(@Nullable List<OutletBalanceReportData> list) {
    super.submitList(list);
    balanceMap.clear();
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.li_outlet_fintransaction, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
  }

  private static class DiffCallBack extends DiffUtil.ItemCallback<OutletBalanceReportData> {
    @Override
    public boolean areItemsTheSame(@NonNull OutletBalanceReportData oldItem,
        @NonNull OutletBalanceReportData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull OutletBalanceReportData oldItem,
        @NonNull OutletBalanceReportData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiOutletFintransactionBinding binding;

    public ViewHolder(View view) {
      super(view);
      binding = LiOutletFintransactionBinding.bind(view);
    }

    public void bind(OutletBalanceReportData obrd, int position) {
      setObject(obrd, position);
    }

    private void setObject(OutletBalanceReportData outletBalanceData, int position) {
      BigDecimal balance = BigDecimal.ZERO;
      if (position == 0) {
        // data pertama
        balance = balance.add(outletBalanceData.getDepositAmount())
            .subtract(outletBalanceData.getPayAmount());
        balanceMap.put(0, balance);
      } else {
        // ada data sebelumnya
        BigDecimal prevBalance = balanceMap.get(position - 1);
        if (prevBalance == null)
          prevBalance = BigDecimal.ZERO;
        balance = prevBalance.add(outletBalanceData.getDepositAmount())
            .subtract(outletBalanceData.getPayAmount());
        balanceMap.put(position, balance);
      }
      binding.tvDate.setText(
          Formatter.DTF_dd_MM_yyyy.format(outletBalanceData.getTransactionDate()));
      binding.tvDescription.setText(outletBalanceData.getDescription());
      binding.tvTypeLabel.setText(outletBalanceData.getType());
      if ("D".equals(outletBalanceData.getTypeCode())) {
        binding.tvTypeLabel.setTextColor(
            ContextCompat.getColor(mContext, R.color.app_primaryLightColor));
        binding.tvAmount.setText(
            Formatter.DF_AMOUNT_NO_DECIMAL.format(outletBalanceData.getDepositAmount()));
        binding.tvAmount.setTextColor(
            ContextCompat.getColor(mContext, R.color.app_primaryLightColor));
      } else {
        binding.tvTypeLabel.setTextColor(ContextCompat.getColor(mContext, R.color.warn_color));
        binding.tvAmount.setText(
            Formatter.DF_AMOUNT_NO_DECIMAL.format(outletBalanceData.getPayAmount()));
        binding.tvAmount.setTextColor(ContextCompat.getColor(mContext, R.color.warn_color));
      }

      binding.tvBalance.setText(Formatter.DF_AMOUNT_NO_DECIMAL.format(balance));
    }
  }
}
