package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import java.util.Set;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseHeaderResponse;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.TugasDroppingHarianBrowseSort;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.DroppingDailyTaskService;

public class GetDroppingDailyTaskBrowseHeaderUC {
  private final DroppingDailyTaskService service;
  private DroppingDailyTaskBrowseFilter filter = new DroppingDailyTaskBrowseFilter();
  private TugasDroppingHarianBrowseSort sort = new TugasDroppingHarianBrowseSort();

  @Inject
  public GetDroppingDailyTaskBrowseHeaderUC(DroppingDailyTaskService service) {
    this.service = service;
  }

  public ListenableFuture<DroppingDailyTaskBrowseHeaderResponse> getDroppingTaskBrowseHeaderList(
      Integer page, int pageSize) {
    if (filter.getCompanyId() == null) {
      ListenableFuture<DroppingDailyTaskBrowseHeaderResponse> emptyResult = SettableFuture.create();
      return emptyResult;
    }
    filter.setOutletName(null);
    ListenableFuture<DroppingDailyTaskBrowseHeaderResponse> result = service.getHeaderList(
        page, pageSize, filter, sort);
    return result;
  }

  public void setFilter(DroppingDailyTaskBrowseFilter filter) {
    this.filter = filter;
  }

  public void setSort(Set<SortOrder> sortOrders){
    this.sort.setSortOrders(sortOrders);
  }
}
