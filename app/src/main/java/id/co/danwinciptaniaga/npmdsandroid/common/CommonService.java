package id.co.danwinciptaniaga.npmdsandroid.common;

import java.util.List;
import java.util.UUID;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.common.AppConfigData;
import id.co.danwinciptaniaga.npmds.data.common.BankData;
import id.co.danwinciptaniaga.npmds.data.common.ExtUserInfoData;
import okhttp3.ResponseBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CommonService {
  @GET("rest/s/extUserInfo")
  ListenableFuture<ExtUserInfoData> getExtUserInfo();

  @GET("rest/s/appConfig")
  ListenableFuture<AppConfigData> getAppConfig();

  @GET("rest/s/pnTest")
  ListenableFuture<ResponseBody> testPushNotification(@Query("deviceToken") String deviceToken);

  @GET("rest/s/bank")
  ListenableFuture<List<BankData>> getBanks();

  @FormUrlEncoded
  @POST("rest/s/changePassword")
  ListenableFuture<ResponseBody> changePassword(@Field("current") String currentPassword,
      @Field("new") String newPassword, @Field("confirm") String confirmPassword);

  @GET("rest/s/substituteUsers")
  ListenableFuture<List<ExtUserInfoData>> getSubstituteUsers();

  @FormUrlEncoded
  @POST("rest/s/substituteUser")
  ListenableFuture<ExtUserInfoData> substituteUser(
      @Field("userId") UUID substituteUserId);
}
