package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.BalanceInfo;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class TugasTransferCekSaldoUC
    extends BaseObservable<TugasTransferCekSaldoUC.Listener, BalanceInfo> {
  private final String TAG = TugasTransferCekSaldoUC.class.getSimpleName();
  private final TugasTransferService service;
  private final Application app;
  private final AppExecutors appExecutors;

  @Inject
  public TugasTransferCekSaldoUC(Application app, TugasTransferService service,
      AppExecutors appExecutors) {
    super(app);
    this.app = app;
    this.service = service;
    this.appExecutors = appExecutors;
  }

  public void getAccountBalanceList(UUID companyId, UUID bankId, UUID accId) {
    notifyStart("Memuat data.. ", null);
    ListenableFuture<BalanceInfo> process = service.getAccountBalanceList(
        companyId, bankId, accId);
    Futures.addCallback(process, new FutureCallback<BalanceInfo>() {
      @Override
      public void onSuccess(@NullableDecl BalanceInfo result) {
        HyperLog.d(TAG, "getCompanyFilter() berhasil");
        notifySuccess("-", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String msg = Utility.getErrorMessageFromThrowable(TAG, t);
        HyperLog.exception(TAG ," getCompanyFilter() Gagal ->", t);
        notifyFailure(msg, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<BalanceInfo> result) {
    listener.onProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener,
      Resource<BalanceInfo> result) {
    listener.onProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener,
      Resource<BalanceInfo> result) {
    listener.onProcessFailure(result);
  }

  public interface Listener {
    void onProcessStarted(Resource<BalanceInfo> loading);

    void onProcessSuccess(Resource<BalanceInfo> res);

    void onProcessFailure(Resource<BalanceInfo> res);
  }
}
