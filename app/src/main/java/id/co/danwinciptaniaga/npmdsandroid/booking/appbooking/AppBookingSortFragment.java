package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingSort;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.SortPropertyAdapter;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentAppBookingSortBinding;
import id.co.danwinciptaniaga.npmdsandroid.sort.SortPropertyEntry;

public class AppBookingSortFragment extends BottomSheetDialogFragment {
  private static final String TAG = AppBookingSortFragment.class.getSimpleName();
  public static final String APP_BOOKING_SORT_FIELDS = "APP_BOOKING_SORT_FIELDS";
  FragmentAppBookingSortBinding binding;
  private static Map<AppBookingSort.Field, Integer> labelMap = new HashMap<>();

  static {
    labelMap.put(AppBookingSort.Field.COMPANY_CODE, R.string.kode_pt);
    labelMap.put(AppBookingSort.Field.COMPANY_NAME, R.string.nama_pt);
    //    labelMap.put(AppBookingSort.Field.OUTLET_CODE, R.string.outlet_code);
    labelMap.put(AppBookingSort.Field.OUTLET_NAME, R.string.outlet_name);
    labelMap.put(AppBookingSort.Field.APP_OPERATION, R.string.jenisPengajuan);
    labelMap.put(AppBookingSort.Field.BOOKING_TYPE, R.string.jenisPencairan);
    labelMap.put(AppBookingSort.Field.TRANSACTION_NO, R.string.transactionNo);
    labelMap.put(AppBookingSort.Field.TRANSACTION_DATE, R.string.transaction_date);
    labelMap.put(AppBookingSort.Field.BOOKING_DATE, R.string.booking_date);
    labelMap.put(AppBookingSort.Field.CONSUMER_NAME, R.string.consumer_name);
    labelMap.put(AppBookingSort.Field.VEHICLE_NO, R.string.vehicleNo);
    labelMap.put(AppBookingSort.Field.WF_STATUS, R.string.status);
  }

  //  public static AppBookingSortFragment newInstance(String param1, String param2) {
  //    AppBookingSortFragment fragment = new AppBookingSortFragment();
  //    return fragment;
  //  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentAppBookingSortBinding.inflate(inflater);
    AppBookingSortFragmentArgs args = AppBookingSortFragmentArgs.fromBundle(getArguments());
    String soListString = args.getSortField().replace("\\", "");
    Type dataType = new TypeToken<List<SortOrder>>() {
    }.getType();
    List<SortOrder> soList = new Gson().fromJson(soListString, dataType);
    Set<SortOrder> sso = new HashSet<SortOrder>(soList);
    setupSortPropertyList(sso);
    setupButtons();
    return binding.getRoot();
  }

  private void setupSortPropertyList(Set<SortOrder> sso) {
    final List<SortPropertyEntry> speSet = new ArrayList<>();
    final Map<SortOrder, SortPropertyEntry> map = new HashMap<>();
    for (AppBookingSort.Field field : AppBookingSort.Field.values()) {
      Integer labelId = labelMap.get(field);
      Preconditions.checkNotNull(labelId,
          String.format("Property label for Sort not found: %s", field.getName()));
      SortOrder isPresentSo = null;
      for (SortOrder so : sso) {
        if (so.getProperty().equals(field.getName())) {
          isPresentSo = so;
          break;
        }
      }

      boolean isSelected;
      boolean isOn = false;
      if (isPresentSo != null) {
        isSelected = true;
        isOn = SortOrder.Direction.ASC.equals(isPresentSo.getDirection()); // on = ASC
      } else {
        isSelected = false;
      }

      SortPropertyEntry spe = new SortPropertyEntry(field.getName(), labelId, isSelected, isOn);
      if (isPresentSo != null) {
        map.put(isPresentSo, spe);
      }
      speSet.add(spe);
    }

    Set<SortPropertyEntry> speInOrder = sso.stream().map(
        sf -> map.get(sf)).collect(Collectors.toSet());

    SortPropertyAdapter spa = new SortPropertyAdapter(speSet, speInOrder);
    binding.fieldList.setAdapter(spa);
  }

  private void setupButtons() {
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        SortPropertyAdapter adapter = (SortPropertyAdapter) binding.fieldList.getAdapter();
        Set<SortPropertyEntry> selectedSpeSet = adapter.getPropertySelection();
        Set<SortOrder> sortOrderSet = new LinkedHashSet<>();
        selectedSpeSet.stream().forEach(spe -> {
          AppBookingSort.Field f = AppBookingSort.Field.formId(spe.getName());
          sortOrderSet.add(AppBookingSort.sortBy(f,
              spe.isOn() ? SortOrder.Direction.ASC : SortOrder.Direction.DESC));
        });

        List<SortOrder> soList = new ArrayList(sortOrderSet);
        String soListJson = new Gson().toJson(soList);

        NavController nc = NavHostFragment.findNavController(AppBookingSortFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(
            APP_BOOKING_SORT_FIELDS, soListJson);
        nc.popBackStack();
      }
    });

    binding.btnReset.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Toast.makeText(getContext(), "Belum ada implementasi", Toast.LENGTH_LONG).show();
      }
    });
  }
}