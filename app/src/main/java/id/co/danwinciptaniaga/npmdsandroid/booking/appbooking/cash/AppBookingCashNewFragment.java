package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash;

import java.util.Objects;

import org.acra.ACRA;

import com.hypertrack.hyperlog.HyperLog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingFragment;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingVM;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseVM;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;

public class AppBookingCashNewFragment extends Abstract_AppBookingFragment {
  private final String TAG = AppBookingCashNewFragment.class.getSimpleName();
  private AppBookingBrowseVM mBrowseVM;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    Abstract_AppBookingVM vm = new ViewModelProvider(this).get(AppBookingCashVM.class);

    AppBookingCashNewFragmentArgs args = AppBookingCashNewFragmentArgs.fromBundle(getArguments());
    boolean isHasPermission = args.getIsHasCreatePermission();
    setOnCreateView(isHasPermission, inflater, container, this, TAG, vm);
    getCashVm().setField_AppBookingId(args.getAppPBookingId());
    getCashVm().setField_AppBookingTypeId(args.getBookingType());
    if (Objects.equals(getCashVm().getField_AppOperation(), null))
      getCashVm().setField_AppOperation(args.getAppOperation());

    mBrowseVM = new ViewModelProvider(requireActivity()).get(AppBookingBrowseVM.class);
    return getBinding().getRoot();
  }

  @Override
  protected void processLoadData() {
    if (getCashVm().getField_AppBookingId() != null)
      getCashVm().processLoadAppBookingData(getCashVm().getField_AppBookingId());
    else
      getCashVm().processPrepareBookingForm();
  }

  @Override
  protected void setStandartButtonListener(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer) {
    getBinding().btnSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        switch (smCash) {
        case KASIR_SUBMITTED_NEW_APPBOOKING:
          getCashVm().processWfSubmitNew();
          break;
        case KASIR_SUBMITTED_EDIT_APPBOOKING:
          getCashVm().processWfSubmitEdit(false);
          break;
        case KASIR_SUBMITTED_CANCEL_APPBOOKING:
          getCashVm().processWfSubmitCancel(false);
          break;
        default:
          break;
        }
      }
    });

    getBinding().btnSimpan.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        switch (smCash) {
        case KASIR_SUBMITTED_NEW_APPBOOKING:
          getCashVm().processSaveDraftNew(true);
          break;
        case KASIR_SUBMITTED_EDIT_APPBOOKING:
          getCashVm().processSaveDraftEdit(true, false);
          break;
        case KASIR_SUBMITTED_CANCEL_APPBOOKING:
          getCashVm().processSaveDraftCancel(false);
          break;
        case READ_ONLY:
          if (getCashVm().getField_AppOperation().equals(Utility.OPERATION_NEW))
            getCashVm().processSaveDraftNew(false);
          else if (getCashVm().getField_AppOperation().equals(Utility.OPERATION_EDIT))
            getCashVm().processSaveDraftEdit(false, false);
          else if (getCashVm().getField_AppOperation().equals(Utility.OPERATION_CANCEL))
            getCashVm().processSaveDraftCancel(false);
        default:
          break;
        }
      }
    });

    getBinding().btnWfHistory.setOnClickListener(v -> {
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      AppBookingCashNewFragmentDirections.ActionWfHistory dir = AppBookingCashNewFragmentDirections
          .actionWfHistory(WfType.APP_BOOKING,
              getCashVm().getField_AppBookingId());
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });
  }

  @Override
  protected void setReadyFormState(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans, String message,
      Animation inAnim, Animation outAnim, boolean isFromAction) {
    if (isFromAction) {
      mBrowseVM.setRefreshList(true);
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      navController.navigateUp();
      return;
    } else
      super.setReadyFormState(smCash, smTrans, message, inAnim, outAnim, isFromAction);
  }

  @Override
  protected void setMode_generalField(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer, String message,
      Animation inAnimation, Animation outAnimation) {

    getBinding().cvOperasionalTransfer.setVisibility(View.GONE);

    int totalChild = 0;
    switch (smCash) {
    case READ_ONLY:
    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
    case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
    case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
    case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
    case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
    case FINRETURN_VERIFICATION_CANCEL_APPBOOKING:
    case FINRETURN_VERIFICATION_EDIT_APPBOOKING:
      setMode_FieldCash_READONLY();
      break;
    case KASIR_SUBMITTED_NEW_APPBOOKING:
    case KASIR_SUBMITTED_EDIT_APPBOOKING:
      //#BOX2
      //set Enable nya ada di setFieldOutlet, karena ada logic yang membuat dia readOnly atau tidak
      //      getBinding().spOutlet.setEnabled(true);
      getBinding().tilBookingDate.setEnabled(getCashVm().isAllowBackDate());
      getBinding().spLOB.setEnabled(true);
      getBinding().spSO.setEnabled(true);
      setField_IdnpksfEnabledDisabled(true);
      getBinding().tilReason.setEnabled(true);
      getBinding().cgPendingPoReason.setEnabled(true);
      getBinding().acPendingPoReason.setEnabled(true);
      totalChild = getBinding().cgPendingPoReason.getChildCount();
      for(int i=0; i<totalChild; i++)
        getBinding().cgPendingPoReason.getChildAt(i).setEnabled(true);
      getBinding().cgPendingPoReason.setClickable(true);

      //#BOX3
      getBinding().tilConsumerName.setEnabled(true);
      getBinding().tilConsumerAddress.setEnabled(true);
      getBinding().tilConsumerPhone.setEnabled(true);
      getBinding().tilVehicleNo.setEnabled(true);

      //#BOX4
      getBinding().cbOpenClose.setEnabled(true);
      getBinding().tilCashPencairanKonsumen.setEnabled(true);
      getVm().isField_isOpenClose().observe(getViewLifecycleOwner(), isOpen -> {
        getBinding().tilCashPencairanFIF.setEnabled(isOpen);
      });
      getBinding().tilCashBiroJasa.setEnabled(true);

      //BOX5
      getBinding().tilFeeMatrix.setEnabled(true);
      getBinding().tilFeeScheme.setEnabled(true);

      //#BOX7
      getBinding().tvLabelCancelation.setVisibility(View.GONE);
      getBinding().tilCancelReason.setVisibility(View.GONE);
      getBinding().ivAtcHO.setVisibility(View.GONE);
      getBinding().tvLabelCancelation.setEnabled(false);
      getBinding().tilCancelReason.setEnabled(false);
      getBinding().tvLabelCancelation.setEnabled(false);
      getBinding().tilCancelReason.setEnabled(false);
      break;
    case KASIR_SUBMITTED_CANCEL_APPBOOKING:
    case KASIR_REVISION_CANCEL_APPBOOKING:
    case AOC_VERIFICATION_CANCEL_APPBOOKING:
      //#BOX2
      getBinding().spOutlet.setEnabled(false);
      getBinding().tilBookingDate.setEnabled(false);
      getBinding().spLOB.setEnabled(false);
      getBinding().spSO.setEnabled(false);
      setField_IdnpksfEnabledDisabled(false);
      getBinding().tilReason.setEnabled(false);
      getBinding().tilCashReason.setEnabled(false);

      //#BOX3
      getBinding().tilConsumerName.setEnabled(false);
      getBinding().tilConsumerAddress.setEnabled(false);
      getBinding().tilConsumerPhone.setEnabled(false);
      getBinding().tilVehicleNo.setEnabled(false);

      //#BOX4
      getBinding().cbOpenClose.setEnabled(false);
      getBinding().tilCashPencairanKonsumen.setEnabled(false);
      getBinding().tilCashPencairanFIF.setEnabled(false);
      getBinding().tilCashBiroJasa.setEnabled(false);

      //BOX5
      getBinding().tilFeeMatrix.setEnabled(false);
      getBinding().tilFeeScheme.setEnabled(false);

      //BOX7
      getBinding().tvLabelCancelation.setVisibility(View.VISIBLE);
      getBinding().tvLabelCancelation.setEnabled(true);
      getBinding().tilCancelReason.setVisibility(View.VISIBLE);
      getBinding().tilCancelReason.setEnabled(true);
      getBinding().ivAtcHO.setVisibility(View.VISIBLE);
      getBinding().ivAtcHO.setEnabled(true);

      switch (smCash) {
      case AOC_VERIFICATION_CANCEL_APPBOOKING:
        //BOX7
        getBinding().tvLabelCancelation.setEnabled(false);
        getBinding().tilCancelReason.setEnabled(false);
        break;
      case KASIR_REVISION_CANCEL_APPBOOKING:
        break;
      default:
        //BOX7
        getBinding().tvLabelCancelation.setEnabled(true);
        getBinding().tilCancelReason.setEnabled(true);
        getBinding().ivAtcHO.setEnabled(true);
        break;
      }
      break;
    case KASIR_REVISION_NEW_APPBOOKING:
    case KASIR_REVISION_EDIT_APPBOOKING:
      //#BOX2
      //set Enable nya ada di setFieldOutlet, karena ada logic yang membuat dia readOnly atau tidak
      //      getBinding().spOutlet.setEnabled(true);
      getBinding().tilBookingDate.setEnabled(getCashVm().isAllowBackDate());
      getBinding().spLOB.setEnabled(true);
      getBinding().spSO.setEnabled(true);
      setField_IdnpksfEnabledDisabled(true);
      getBinding().tilReason.setEnabled(true);
      getBinding().tilCashReason.setEnabled(true);

      getBinding().cgPendingPoReason.setEnabled(true);
      getBinding().acPendingPoReason.setEnabled(true);
      totalChild = getBinding().cgPendingPoReason.getChildCount();
      for(int i=0; i<totalChild; i++)
        getBinding().cgPendingPoReason.getChildAt(i).setEnabled(true);
      getBinding().cgPendingPoReason.setClickable(true);

      //#BOX3
      getBinding().tilConsumerName.setEnabled(true);
      getBinding().tilConsumerAddress.setEnabled(true);
      getBinding().tilConsumerPhone.setEnabled(true);
      getBinding().tilVehicleNo.setEnabled(true);

      //#BOX4
      getBinding().cbOpenClose.setEnabled(true);
      getBinding().tilCashPencairanKonsumen.setEnabled(true);
      getCashVm().isField_isOpenClose().observe(getViewLifecycleOwner(), isOpen -> {
        getBinding().tilCashPencairanFIF.setEnabled(isOpen);
      });
      getBinding().tilCashBiroJasa.setEnabled(true);

      //BOX5
      getBinding().tilFeeMatrix.setEnabled(true);
      getBinding().tilFeeScheme.setEnabled(true);

      // BOX7
      getBinding().tvLabelCancelation.setVisibility(View.GONE);
      getBinding().tilCancelReason.setVisibility(View.GONE);
      getBinding().ivAtcHO.setVisibility(View.GONE);
      getBinding().tvLabelCancelation.setEnabled(false);
      getBinding().tilCancelReason.setEnabled(false);
      break;
    case DOC_VERIFICATION_NEW_APPBOOKING:
    case AOC_VERIFICATION_EDIT_APPBOOKING:
      getBinding().spOutlet.setEnabled(false);
      getBinding().tilBookingDate.setEnabled(false);
      getBinding().spLOB.setEnabled(false);
      getBinding().spSO.setEnabled(false);
      setField_IdnpksfEnabledDisabled(false);
      getBinding().tilReason.setEnabled(false);
      getBinding().tilCashReason.setEnabled(false);

      getBinding().cgPendingPoReason.setEnabled(false);
      getBinding().acPendingPoReason.setEnabled(false);
      totalChild = getBinding().cgPendingPoReason.getChildCount();
      for(int i=0; i<totalChild; i++)
        getBinding().cgPendingPoReason.getChildAt(i).setEnabled(false);
      getBinding().cgPendingPoReason.setClickable(false);

      getBinding().tilConsumerName.setEnabled(false);
      getBinding().tilConsumerAddress.setEnabled(false);
      getBinding().tilConsumerPhone.setEnabled(false);
      getBinding().tilVehicleNo.setEnabled(false);

      getBinding().cbOpenClose.setEnabled(false);
      getBinding().tilCashPencairanFIF.setEnabled(false);
      getBinding().tilCashBiroJasa.setEnabled(false);
      getBinding().tilCashPencairanKonsumen.setEnabled(false);

      getBinding().tvLabelCancelation.setVisibility(View.GONE);
      getBinding().tilCancelReason.setVisibility(View.GONE);
      getBinding().tilCancelReason.setEnabled(false);
      getBinding().ivAtcHO.setVisibility(View.GONE);
      break;
    default:
      String msg = "mode atau screenMode Halaman tidak diketahui ->" + smCash + "<-";
      HyperLog.w(getThisTag(), msg);
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      RuntimeException re = new RuntimeException(msg);
      ACRA.getErrorReporter().handleException(re);
      break;
    }
  }

  protected void handleDocumentWfActionCash(String decision, String comment,
      AppBookingCashScreenMode smCash) {
    boolean isNotSupportedWF = false;
    switch (smCash) {
    case DOC_VERIFICATION_NEW_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getCashVm().processWF_DOC_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getCashVm().processWF_DOC_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case KASIR_REVISION_NEW_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getCashVm().processWF_KASIR_REVISION_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getCashVm().processWF_CAPTAIN_APPROVE_NEW(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getCashVm().processWF_CAPTAIN_RETURN_NEW(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_VERIFICATION_EDIT_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getCashVm().processWF_AOC_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getCashVm().processWF_AOC_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getCashVm().processWF_AOC_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case KASIR_REVISION_EDIT_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getCashVm().processWF_KASIR_REVISION_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getCashVm().processWF_CAPTAIN_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getCashVm().processWF_CAPTAIN_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getCashVm().processWF_CAPTAIN_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getCashVm().processWF_DIRECTOR_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getCashVm().processWF_DIRECTOR_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getCashVm().processWF_DIRECTOR_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case FINRETURN_VERIFICATION_EDIT_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getCashVm().processWF_FINRETURN_APPROVE_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getCashVm().processWF_FINRETURN_RETURN_EDIT(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getCashVm().processWF_FINRETURN_REJECT_EDIT(comment);
      else
        isNotSupportedWF = true;
      break;
    case KASIR_REVISION_CANCEL_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_SUBMIT_REVISION.equals(decision))
        getCashVm().processWF_KASIR_REVISION_CANCEL(comment);
      else
        isNotSupportedWF = true;
      break;
    case AOC_VERIFICATION_CANCEL_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getCashVm().processWF_AOC_APPROVE_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getCashVm().processWF_AOC_RETURN_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getCashVm().processWF_AOC_REJECT_CANCEL(comment);
      else
        isNotSupportedWF = true;
      break;
    case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getCashVm().processWF_CAPTAIN_APPROVE_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getCashVm().processWF_CAPTAIN_RETURN_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getCashVm().processWF_CAPTAIN_REJECT_CANCEL(comment);
      else
        isNotSupportedWF = true;
      break;
    case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getCashVm().processWF_DIRECTOR_APPROVE_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getCashVm().processWF_DIRECTOR_RETURN_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getCashVm().processWF_DIRECTOR_REJECT_CANCEL(comment);
      else
        isNotSupportedWF = true;
      break;
    case FINRETURN_VERIFICATION_CANCEL_APPBOOKING:
      if (WorkflowConstants.WF_OUTCOME_APPROVE.equals(decision))
        getCashVm().processWF_FINRETURN_APPROVE_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_RETURN.equals(decision))
        getCashVm().processWF_FINRETURN_RETURN_CANCEL(comment);
      else if (WorkflowConstants.WF_OUTCOME_REJECT.equals(decision))
        getCashVm().processWF_FINRETURN_REJECT_CANCEL(comment);
      else
        isNotSupportedWF = true;
      break;
    default:
      //      case KASIR_SUBMITTED_NEW_APPBOOKING:
      //      case KASIR_SUBMITTED_EDIT_APPBOOKING:
      //      case KASIR_SUBMITTED_CANCEL_APPBOOKING:
      isNotSupportedWF = true;
      break;
    }
    if (isNotSupportedWF) {
      String scMode = String.format("Screen Mode[%s] ", smCash);
      String wf = String.format("wfOutCome[%s] ", decision);
      String msg = scMode + wf + " Not Supported";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG);
      HyperLog.exception(getThisTag(), msg);
      RuntimeException re = new RuntimeException(msg);
      ACRA.getErrorReporter().handleException(re);
    }
  }

}
