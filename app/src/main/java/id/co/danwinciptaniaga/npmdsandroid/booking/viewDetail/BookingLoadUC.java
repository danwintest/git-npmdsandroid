package id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.booking.BookingDetailResponse;
import id.co.danwinciptaniaga.npmds.data.booking.NewBookingReponse;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class BookingLoadUC extends BaseObservable<BookingLoadUC.Listener, BookingLoadUC.LoadBookingResult>{
  private final String TAG = BookingLoadUC.class.getSimpleName();
  private final BookingService bookingService;
  private final AppExecutors appExecutors;

  @Inject
  public BookingLoadUC(Application app, BookingService bookingService,
      AppExecutors appExecutors) {
    super(app);
    this.bookingService = bookingService;
    this.appExecutors = appExecutors;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<LoadBookingResult> result) {
    listener.onLoadBookingStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<LoadBookingResult> result) {
    listener.onLoadBookingSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<LoadBookingResult> result) {
    listener.onLoadBookingFailure(result);
  }

  public interface Listener {
    void onLoadBookingStarted();

    void onLoadBookingSuccess(Resource<LoadBookingResult> res);

    void onLoadBookingFailure(Resource<LoadBookingResult> res);
  }

  public static class LoadBookingResult {
    private BookingDetailResponse bookingDetailResponse;
    private NewBookingReponse newBookingReponse;

    public LoadBookingResult(BookingDetailResponse bookingDetailResponse,
        NewBookingReponse newBookingReponse) {
      this.bookingDetailResponse = bookingDetailResponse;
      this.newBookingReponse = newBookingReponse;
    }

    public BookingDetailResponse getBookingDetailResponse() {
      return bookingDetailResponse;
    }
    public NewBookingReponse getNewBookingReponse(){
      return newBookingReponse;
    }

  }

  public ListenableFuture<LoadBookingResult> load(UUID bookingId) {
    notifyStart(null, null);
    // load info detail booking
    ListenableFuture<BookingDetailResponse> loadBookingLf =null;
    ListenableFuture<NewBookingReponse> nbrLf = null;
    try {
      loadBookingLf = bookingService.loadBooking(bookingId);
      nbrLf = bookingService.getNewBooking();
    }catch (Exception e){
      HyperLog.exception(TAG, "load: ERROR ->",e );
      throw e;
    }
    ListenableFuture<BookingDetailResponse> finalLoadBookingLf = loadBookingLf;
    ListenableFuture<NewBookingReponse> finalNewBookingLf = nbrLf;

    ListenableFuture<LoadBookingResult> allSuccessLf = Futures
        .whenAllSucceed(Arrays.asList(loadBookingLf,nbrLf)).call(new Callable<LoadBookingResult>() {
          @Override
          public LoadBookingResult call() throws Exception {
            return new LoadBookingResult(Futures.getDone(finalLoadBookingLf),Futures.getDone(finalNewBookingLf));
          }
        }, appExecutors.backgroundIO());
    Futures.addCallback(allSuccessLf, new FutureCallback<LoadBookingResult>() {
      @Override
      public void onSuccess(@NullableDecl LoadBookingResult result) {
        notifySuccess("Booking berhasil dimuat", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG,t);
        notifyFailure("Booking gagal dimuat " + message, null, t);
      }
    }, appExecutors.backgroundIO());
    return allSuccessLf;
  }




}
