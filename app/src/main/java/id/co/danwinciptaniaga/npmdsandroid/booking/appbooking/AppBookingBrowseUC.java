package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.util.Set;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingListResponse;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingSort;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;

public class AppBookingBrowseUC {
  private static final String TAG = AppBookingBrowseUC.class.getSimpleName();

  private final AppBookingService appBookingService;
  private AppBookingBrowseFilter filter = new AppBookingBrowseFilter();
  private AppBookingSort sort = new AppBookingSort();

  @Inject
  public AppBookingBrowseUC(AppBookingService appBookingService) {
    this.appBookingService = appBookingService;
  }

  public ListenableFuture<AppBookingListResponse> getAppBookingList(Integer page,
      Integer pageSize) {
    HyperLog.d(TAG, "getAppBookingList terpanggil");
    ListenableFuture<AppBookingListResponse> res = appBookingService.getAppBookingList(page,
        pageSize, filter, sort);
    return res;
  }

  public void setFilter(AppBookingBrowseFilter filter) {
    this.filter = filter;
  }

  public void setSort(Set<SortOrder> sortOrders) {
    this.sort.setSortOrders(sortOrders);
  }

}
