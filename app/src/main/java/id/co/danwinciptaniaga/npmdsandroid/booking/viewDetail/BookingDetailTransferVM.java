package id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmds.data.booking.BookingDetailResponse;
import id.co.danwinciptaniaga.npmds.data.booking.NewBookingReponse;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.BatchData;
import id.co.danwinciptaniaga.npmds.data.common.LobShortData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.common.SoShortData;
import id.co.danwinciptaniaga.npmds.data.common.UtilityState;
import id.co.danwinciptaniaga.npmds.data.wf.ProcTaskData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingVM;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingBrowseVM;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingFormState;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingLoadUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingPrepareUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingProcessUC;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferVM;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class BookingDetailTransferVM extends AppBookingTransferVM
    implements BookingLoadUC.Listener, BookingProcessUC.Listener {
  private static String TAG = BookingDetailTransferVM.class.getSimpleName();
  private final BookingLoadUC loadUC;
  private final BookingProcessUC bProcessUC;

  private MutableLiveData<String> field_ConsumerBank_LABEL = new MutableLiveData<>();
  private MutableLiveData<String> field_ConsumerAccountNo = new MutableLiveData<>();

  private MutableLiveData<String> field_BiroJasaBank_LABEL = new MutableLiveData<>();
  private MutableLiveData<String> field_BiroJasaAccountNo = new MutableLiveData<>();
  private MutableLiveData<String> field_BiroJasaAccountName = new MutableLiveData<>();

  private MutableLiveData<String> field_FIFBank_LABEL = new MutableLiveData<>();
  private MutableLiveData<String> field_FIFAccountNo = new MutableLiveData<>();
  private MutableLiveData<String> field_FIFAccountName = new MutableLiveData<>();

  private MutableLiveData<Boolean> field_CanEditPopr = new MutableLiveData<>();

  @ViewModelInject
  public BookingDetailTransferVM(@NonNull Application application, AppExecutors appExecutors,
      AppBookingPrepareUC prepareUC, AppBookingLoadUC appBookingLoadUC,
      AppBookingProcessUC processUC, UtilityService utilityService,
      AppBookingService appBookingService, BookingLoadUC loadUC,BookingProcessUC bProcessUC) {
    super(application, appExecutors, prepareUC, appBookingLoadUC, processUC, utilityService,
        appBookingService);
    this.loadUC = loadUC;
    this.bProcessUC = bProcessUC;
    this.bProcessUC.registerListener(this);
    this.loadUC.registerListener(this);
  }

  @Override
  public void setField_ConsumerAmount(BigDecimal data) {
    field_ConsumerAmount.postValue(data);
  }

  public void setField_FIFAmount(BigDecimal data) {
    field_FIFAmount.postValue(data);
  }

  protected void setField_BookingAmount(BigDecimal data) {
    field_BookingAmount.postValue(data);
  }

  public MutableLiveData<String> getField_ConsumerBank_LABEL() {
    return field_ConsumerBank_LABEL;
  }

  public void setField_ConsumerBank_LABEL(String data) {
    field_ConsumerBank_LABEL.postValue(data);
  }

  public MutableLiveData<String> getField_ConsumerAccountNo() {
    return field_ConsumerAccountNo;
  }

  public void setField_ConsumerAccountNo(String data) {
    field_ConsumerAccountNo.postValue(data);
  }

  public MutableLiveData<String> getField_BiroJasaBank_LABEL() {
    return field_BiroJasaBank_LABEL;
  }

  public void setField_BiroJasaBank_LABEL(String data) {
    field_BiroJasaBank_LABEL.postValue(data);
  }

  public MutableLiveData<String> getField_BiroJasaAccountNo() {
    return field_BiroJasaAccountNo;
  }

  public void setField_BiroJasaAccountNo(String data) {
    field_BiroJasaAccountNo.postValue(data);
  }

  public MutableLiveData<String> getField_BiroJasaAccountName() {
    return field_BiroJasaAccountName;
  }

  public void setField_BiroJasaAccountName(String data) {
    field_BiroJasaAccountName.postValue(data);
  }

  public MutableLiveData<String> getField_FIFBank_LABEL() {
    return field_FIFBank_LABEL;
  }

  public void setField_FIFBank_LABEL(String data) {
    field_FIFBank_LABEL.postValue(data);
  }

  public MutableLiveData<String> getField_FIFAccountNo() {
    return field_FIFAccountNo;
  }

  public void setField_FIFAccountNo(String data) {
    field_FIFAccountNo.postValue(data);
  }

  public MutableLiveData<String> getField_FIFAccountName() {
    return field_FIFAccountName;
  }

  public void setField_FIFAccountName(String data) {
    field_FIFAccountName.postValue(data);
  }

  public void loadByBookingId(UUID id) {
    HyperLog.d(TAG, "loadDetailBooking byId->" + id + "<-");
    loadUC.load(id);
  }

  public MutableLiveData<Boolean> getField_CanEditPopr() {
    return field_CanEditPopr;
  }

  public void setField_CanEditPopr(Boolean data) {
    field_CanEditPopr.postValue(data);
  }

  @Override
  public boolean validateProcess() {
    if (!field_poprList_OK)
      field_poprList_ERROR.postValue(getApplication().getString(R.string.validation_mandatory));

    if (getField_PoDate().getValue() != null
        && getField_PoDate().getValue().compareTo(LocalDate.now()) > 0) {
      Toast
          .makeText(getApplication(),
              getApplication().getString(R.string.podate_validation).toString(), Toast.LENGTH_LONG)
          .show();
      return false;
    }

    boolean mandatory_field = (field_PoNo_OK && field_PoDate_OK
        && field_Attach_FINAL_PO_OK.getValue() && field_poprList_OK);
    return mandatory_field;
  }

  public void processSave() {
    if (!validateProcess())
      return;
    bProcessUC.processSave(getField_BookingId(), field_Attach_Final_PO,
        getField_AppNo().getValue(), getField_PoNo().getValue(), getField_PoDate().getValue(),
        getPoprIdList(), getField_UpdateTS().getValue());
  }

  @Override
  public void validate_field_VehicleNo(String vehicleNo, LocalDate bookingDate) {
    return;
  }


  @Override
  public void onLoadBookingStarted() {
    HyperLog.d(TAG, "Load Booking START");
    setFormState(AppBookingFormState.inProgress("Menyiapkan data"));
  }

  @Override
  public void onLoadBookingSuccess(Resource<BookingLoadUC.LoadBookingResult> res) {
    String fn = "onLoadBookingSuccess() ";
    isLoadProcess = true;
    BookingDetailResponse detail = res.getData().getBookingDetailResponse();
    BookingData bd = detail.getBookingData();

    NewBookingReponse prepareForm = res.getData().getNewBookingReponse();
    setField_BookingId(bd.getId());

//    List<BankAccountData> badList = res.getData().getNewBookingReponse().getBankAccountDataList();
//    setBankAccount_Ld(badList);
    List<BatchData> bdList = res.getData().getNewBookingReponse().getBatchDataList();
    setBatch_Ld(bdList);

//    BatchData bdObj = detail.getBatchData();
//    if(bdObj!=null) {
//      for (BatchData bd : bdList) {
//        if (bd.getCode().equals(bdObj.getCode())){
//          String timeValue = bd.getCode().equals(REAL_TIME_CODE) ?
//              REAL_TIME_VALUE :
//              bd.getEndTime().toString();
//          setField_Batch(new KeyValueModel<>(bd, timeValue));
//        }
//      }
//    }

    // set Header
    setHeader_PtName(bd.getOutletObj().getCompanyName());
    setHeader_Status(bd.getStatus());
    setHeader_StatusCode(bd.getStatusCode());
    setHeader_OutletName(bd.getOutletObj().getOutletName());
    setHeader_OutletCode(bd.getOutletObj().getOutletCode());
    setField_CancelReason(bd.getCancelReason(),true);
    // FIXME
    // setHeader_JenisPengajuan(bd.getAppTypeName());
    setHeader_BookingDate(bd.getBookingDate().format(Formatter.DTF_dd_MM_yyyy));
//     String pengajuanDate = bd.getApprovalDate() != null
//     ? Formatter.SDF_dd_MM_yyyy.format(bd.getApprovalDate())
//     : "-";
//     setHeader_PengajuanDate(pengajuanDate);

    for (int i = 0; i < prepareForm.getAssignedOutlet().size(); i++) {
      OutletShortData osd = prepareForm.getAssignedOutlet().get(i);
      if (osd.getOutletId().equals(bd.getOutletObj().getOutletId())) {
        setField_outletKvm(new KeyValueModel<>(osd, osd.getOutletName()));
        break;
      }
    }
    setField_AppBookingTypeId(bd.getBookingTypeId());

    if (bd.getBookingTypeId().equals(Utility.BOOKING_TYPE_TRANSFER)) {
      String bankName = bd.getConsBankName();
      String otherBank = bd.getConsOtherBank();

      setField_ConsumerBank_LABEL(bankName != null ? bankName : otherBank);
      setField_ConsumerAccountNo(bd.getConsAccountNo());
      setField_ConsumerAccountName(bd.getConsAccountName(),true);
      setField_TF_ConsumerNameWarn(bd.getConsAccountName(), bd.getConsumerName());

      bankName = bd.getFifBankName();
      otherBank = bd.getFifOtherBank();
      setField_FIFBank_LABEL(bankName != null ? bankName : otherBank);
      setField_FIFAccountNo(bd.getFifAccountNo());
      setField_FIFAccountName(bd.getFifAccountName());

      bankName = bd.getBjBankName();
      otherBank = bd.getBjOtherBank();
      setField_BiroJasaBank_LABEL(bankName != null ? bankName : otherBank);
      setField_BiroJasaAccountNo(bd.getBjAccountNo());
      setField_BiroJasaAccountName(bd.getBjAccountName());
    }

    // FIXME
    // setField_AppOperation(bd.getAppOperationId());
    setField_ConsumerNo(bd.getConsumerNo());
    setField_BookingDate(bd.getBookingDate());

    for (int i = 0; i < prepareForm.getLobData().size(); i++) {
      LobShortData lsd = prepareForm.getLobData().get(i);
      if (lsd.getLobId().equals(bd.getLobId())) {
        setField_LobKvm(new KeyValueModel<>(lsd, lsd.getLobName()));
      }
    }

    for (int i = 0; i < prepareForm.getSoDataList().size(); i++) {
      SoShortData ssd = prepareForm.getSoDataList().get(i);
      if (ssd.getSoId().equals(bd.getSoId())) {
        setField_SoKvm(new KeyValueModel<SoShortData, String>(ssd, ssd.getSoName()));
      }
    }

    setField_TrxNo(bd.getConsumerNo());
    setField_BookingId(bd.getId());
    setField_Idnpksf(bd.getIdnpksf());
    setField_AlasanPinjam(bd.getReason());
    setField_AlasanPinjamCash(bd.getCashReason());
    setField_AppNo(bd.getAppNo());
    setField_PoNo(bd.getPoNo());
    setField_PoDate(bd.getPoDate());
    setField_UpdateTS(bd.getUpdateTs());

    setPoPrList_Ld(prepareForm.getPoprDataList());
    List<KeyValueModel<UUID, String>> poprIdList = bd.getPopr().stream().map(obj -> {
      KeyValueModel<UUID, String> newObj = new KeyValueModel<>(obj.getPoprId(), obj.getName());
      return newObj;
    }).collect(Collectors.toList());
    setField_PoprList(null, null, poprIdList);

    setField_ConsumerName(bd.getConsumerName(), true);
    setField_ConsumerAddress(bd.getConsumerAddress());
    setField_ConsumerPhone(bd.getConsumerPhone());
    setField_VehicleNo(bd.getVehicleNo());
    field_VehicleNo_OK = true;
    field_VehicleNo_ERROR.postValue(null);
    setField_IsOpenClose(bd.isOpenClose());
    setField_ConsumerAmount(bd.getConsumerAmt());
    setField_ConsValid(bd.isValidCons() == null ? false : bd.isValidCons());
    setField_FIFAmount(bd.getFifAmt());
    setField_FIFValid(bd.isValidFIF() == null ? false : bd.isValidFIF());
    setField_BiroJasaAmount(bd.getBiroJasaAmt());
    setField_BjValid(bd.isValidBJ() == null ? false : bd.isValidBJ());
    setField_BookingAmount(bd.getTotalAmt());
    setField_FeeMatrix(bd.getMatrixFee());
    setField_FeeScheme(bd.getSchemeFee());

    if (bd.getAtt_PO() != null) {
      //      setAttachment(AppBookingBrowseVM.ATTACHMENT_MODE_PO, bd.getAtt_PO().getId(),
      //          bd.getAtt_PO().getFileName());
      HyperLog.d(TAG, fn + "ATT_PO isArchive[" + bd.getAtt_PO().isArchived() + "]");
      setField_Attach_PO_isArchive(bd.getAtt_PO().isArchived());

      if (!bd.getAtt_PO().isArchived())
        setAttachmentPO(bd.getAtt_PO().getId(), bd.getAtt_PO().getFileName(), bd.getPoNo(),
            bd.getPoDate(), poprIdList);
    } else {
      setField_Attach_PO_isArchive(false);
    }

    if (bd.getAtt_STNK_KTP() != null) {
      HyperLog.d(TAG, fn + "ATT_STNK_KTP isArchive[" + bd.getAtt_STNK_KTP().isArchived() + "]");
      setField_Attach_STNK_isArchive(bd.getAtt_STNK_KTP().isArchived());

      if (!bd.getAtt_STNK_KTP().isArchived())
        setAttachment(UtilityState.ATTCH_STNK.getId(), bd.getAtt_STNK_KTP().getId(),
            bd.getAtt_STNK_KTP().getFileName());
    } else {
      setField_Attach_STNK_isArchive(false);
    }

    if (bd.getAtt_KK() != null) {
      HyperLog.d(TAG, fn + "ATT_KK isArchive[" + bd.getAtt_KK().isArchived() + "]");
      setField_Attach_KK_isArchive(bd.getAtt_KK().isArchived());

      if (!bd.getAtt_KK().isArchived())
        setAttachment(UtilityState.ATTCH_KK.getId(), bd.getAtt_KK().getId(),
            bd.getAtt_KK().getFileName());
    } else {
      setField_Attach_KK_isArchive(false);
    }

    if (bd.getAtt_KWITANSI() != null) {
      HyperLog.d(TAG, fn + "ATT_KWITANSI isArchive[" + bd.getAtt_KWITANSI().isArchived() + "]");
      setField_Attach_BPKB_isArchive(bd.getAtt_KWITANSI().isArchived());

      if (!bd.getAtt_KWITANSI().isArchived())
        setAttachment(UtilityState.ATTCH_BPKB.getId(), bd.getAtt_KWITANSI().getId(),
            bd.getAtt_KWITANSI().getFileName());
    } else {
      setField_Attach_BPKB_isArchive(false);
    }

    if (bd.getAtt_SPT() != null) {
      HyperLog.d(TAG, fn + "ATT_SPT isArchive[" + bd.getAtt_SPT().isArchived() + "]");
      setField_Attach_SPT_isArchive(bd.getAtt_SPT().isArchived());

      if (!bd.getAtt_SPT().isArchived())
        setAttachment(UtilityState.ATTCH_SPT.getId(), bd.getAtt_SPT().getId(),
            bd.getAtt_SPT().getFileName());
    } else {
      setField_Attach_SPT_isArchive(false);
    }

    if (bd.getAtt_SERAH_TERIMA_UANG() != null) {
      HyperLog.d(TAG,
          fn + "ATT_SERAH_TERIMA isArchive[" + bd.getAtt_SERAH_TERIMA_UANG().isArchived() + "]");
      setField_Attach_SERAH_TERIMA_isArchive(bd.getAtt_SERAH_TERIMA_UANG().isArchived());

      if (!bd.getAtt_SERAH_TERIMA_UANG().isArchived())
        setAttachment(UtilityState.ATTCH_SERAHTERIMA.getId(),
            bd.getAtt_SERAH_TERIMA_UANG().getId(), bd.getAtt_SERAH_TERIMA_UANG().getFileName());
    } else {
      setField_Attach_SERAH_TERIMA_isArchive(false);
    }

    if (bd.getAtt_FISIK_MOTOR() != null) {
      HyperLog.d(TAG,
          fn + "ATT_FISIK_MOTOR isArchive[" + bd.getAtt_FISIK_MOTOR().isArchived() + "]");
      setField_Attach_FISIK_MOTOR_isArchive(bd.getAtt_FISIK_MOTOR().isArchived());

      if (!bd.getAtt_FISIK_MOTOR().isArchived())
        setAttachment(UtilityState.ATTCH_FISIKMOTOR.getId(),
            bd.getAtt_FISIK_MOTOR().getId(), bd.getAtt_FISIK_MOTOR().getFileName());
    } else {
      setField_Attach_FISIK_MOTOR_isArchive(false);
    }

    if (bd.getAtt_HO() != null) {
      HyperLog.d(TAG,
          fn + "ATT_HO isArchive[" + bd.getAtt_HO().isArchived() + "]");
      setField_Attach_HO_isArchive(bd.getAtt_HO().isArchived());

      if (!bd.getAtt_HO().isArchived())
      setAttachment(UtilityState.ATTCH_HO.getId(), bd.getAtt_HO().getId(),
          bd.getAtt_HO().getFileName());
    }else{
      setField_Attach_HO_isArchive(false);
    }

    field_BookingAmount_ERROR.postValue(null);
    setField_CanEditPopr(bd.isCanEditPoPr());
    setReadOnly_AppNoPoNoPoDatePoAttchment2(null, null, bd.getAppNo(), bd.getPoNo(), bd.getPoDate(),
        bd.getAtt_PO() != null);
//    if (bd.getAppNo() != null && bd.getPoNo() != null && bd.getPoDate() != null
//        && bd.getAtt_PO() != null)
//      setReadOnly_AppNoPoNoPoDatePoAttchment(true);
//    else
//      setReadOnly_AppNoPoNoPoDatePoAttchment(false);

    isLoadProcess = false;
    HyperLog.d(TAG, String.format("Booking mode %s EditMode = %s", bd.getId(), AppBookingCashScreenMode.READ_ONLY));
    setFormState(AppBookingFormState.ready(res.getMessage(), false, AppBookingCashScreenMode.READ_ONLY, null));
    setFormState(AppBookingFormState.ready(res.getMessage(), false, null, AppBookingTransferScreenMode.READ_ONLY));
  }

  @Override
  public void onLoadBookingFailure(Resource<BookingLoadUC.LoadBookingResult> res) {
    HyperLog.e(TAG, "Load Booking GAGAL");
    setFormState(AppBookingFormState.errorLoadData(res.getMessage()));
  }

  @Override
  public void onProcessStarted(Resource<Boolean> loading) {
    HyperLog.d(TAG, "Process saveBooking START");
    setFormState(AppBookingFormState.loading(getApplication().getString(R.string.silahkan_tunggu)));
  }

  @Override
  public void onProcessSuccess(Resource<Boolean> res) {
    HyperLog.d(TAG, "Process saveBooking Berhasil");
    setFormState(
        AppBookingFormState.ready(res.getMessage(), true, AppBookingCashScreenMode.READ_ONLY,
            null));
  }

  @Override
  public void onProcessFailure(Resource<Boolean> res) {
    HyperLog.e(TAG, "Process Save Booking GAGAL");
    setFormState(AppBookingFormState.errorProcess(res.getMessage()));
  }

  @Override
  public void processSaveDraftNew(boolean isSaveDraftProcess) {
  }

  @Override
  public void processSaveDraftCancel(boolean isNewDocument) {
 }

  @Override
  public void processSaveDraftEdit(boolean isSaveDraftProcess, boolean isNewDocument) {
  }

  @Override
  public void processWfSubmitNew() {

  }

  @Override
  public void processWfSubmitCancel(boolean isNewDocument) {

  }

  @Override
  public void processWfSubmitEdit(boolean isNewDocument) {

  }
  @Override
  protected void setReadOnly_AppNoPoNoPoDatePoAttchment2(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans, String appNo, String poNo, LocalDate poDate,
      boolean isAttchmentPoExist) {
    if (appNo != null && poNo != null && poDate != null
        && isAttchmentPoExist)
      readOnly_AppNoPoNoPoDatePoAttchment.postValue(true);
    else
      readOnly_AppNoPoNoPoDatePoAttchment.postValue(false);
  }
}
