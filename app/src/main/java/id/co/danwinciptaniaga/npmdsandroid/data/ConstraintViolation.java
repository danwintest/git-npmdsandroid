package id.co.danwinciptaniaga.npmdsandroid.data;

public class ConstraintViolation extends Exception {
  public ConstraintViolation(String message) {
    super(message);
  }
}
