package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypertrack.hyperlog.HyperLog;

import android.app.PendingIntent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentTugasTransferDetailBinding;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListLoadStateAdapter;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.sort.TugasTransferDetailBrowseSortFragment;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;
import kotlin.Unit;

@AndroidEntryPoint
public class TugasTransferDetailFragment extends ProtectedFragment {
  private static final String TAG = TugasTransferDetailFragment.class.getSimpleName();
  public static final String SAVED_STATE_LD_REFRESH = "refresh";
  //  public static final String ARG_DROPPING_DAILY_DETAIL_FILTER = "droppingDailyDetailFilter";
  private FragmentTugasTransferDetailBinding binding;
  private TugasTransferDetailBrowseVM vm;
  private TugasTransferBrowseDetailListAdapter adapter;
  private ConcatAdapter concatAdapter;
  private RecyclerView rv;
  boolean isPermissionMultiApproval = false;
  boolean isPermissionUpdate = false;
  private ActionMode actionMode;
  private ActionModeCallBack actionModeCallBack;
  @Inject
  AppExecutors appExecutors;

  public TugasTransferDetailFragment() {
    // Required empty public constructor
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    vm = new ViewModelProvider(requireActivity()).get(TugasTransferDetailBrowseVM.class);
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    Resource<List<PermissionInfo>> permissions = basemodel.getPermissionsLd().getValue();
    if (status.getStatus() == Status.LOADING) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      binding.swipeRefresh.setVisibility(View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.GONE);
      binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);

      binding.progressWrapper.retryButton.setOnClickListener(restart);
    }
  }

  private void onReady() {
    // harusnya permission sudah ada kalau authenticationStatusUpdate SUCCESS
    Resource<List<PermissionInfo>> permissions = basemodel.getPermissionsLd().getValue();

    // lalu gunakan LiveData observer untuk mengupdate view
    vm.getLoadStateDetail().observe(getViewLifecycleOwner(), state -> {
      HyperLog.d(TAG, "droppingListLoadState changed: " + state);
      if (state instanceof LoadState.Loading) {
        // sedang loading pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
      } else if (state instanceof LoadState.Error) {
        // terjadi error pada waktu pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);

        // retry = reload semua data
        binding.progressWrapper.retryButton.setOnClickListener(reloadFragmentDetail);
        // sembunyikan list, karena retry akan dihandle oleh button di atas
        binding.swipeRefresh.setVisibility(View.GONE);

        binding.progressWrapper.progressText.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            ((LoadState.Error) state).getError().getMessage());
      } else if (state instanceof LoadState.NotLoading) {
        // sudah ok
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.swipeRefresh.setVisibility(View.VISIBLE);
      }
    });
    // connect data ke adapter
    // supaya tidak hit server lagi pada waktu kembali dari child fragment, hanya load data kalau
    // list masih null

    vm.getDetailList().observe(getViewLifecycleOwner(), pagingData -> {
      adapter.submitData(getLifecycle(), pagingData);
    });
  }

  private View.OnClickListener restart = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_tugas_transfer_detail)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragmentDetail = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // reload fragment ini = memuat ulang seluruh list
      adapter.refresh();
      // karena menggunakan progress indicator sendiri, jadi  tidak perlu yg dari swipeRefresh
      binding.swipeRefresh.setRefreshing(false);
    }
  };

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.expense_browse_menu, menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (item.getItemId() == R.id.menu_filter) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_tugas_transfer_detail, nc.getCurrentDestination().getId()))
        return true;
      TugasTransferDetailFragmentDirections.ActionFilter dir = TugasTransferDetailFragmentDirections.actionFilter(
          false);
      dir.setFilterField(vm.getFilterField().getValue());
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    } else if (item.getItemId() == R.id.menu_sort) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_tugas_transfer_detail, nc.getCurrentDestination().getId()))
        return true;
      List<SortOrder> soList = new ArrayList(vm.getSortFields());
      String absJson = new Gson().toJson(soList);
      TugasTransferDetailFragmentDirections.ActionSort dir = TugasTransferDetailFragmentDirections.actionSort();
      dir.setSortField(absJson);
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    }
    return super.onContextItemSelected(item);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentTugasTransferDetailBinding.inflate(inflater, container, false);
    TugasTransferDetailFragmentArgs args = TugasTransferDetailFragmentArgs.fromBundle(
        getArguments());
    setRecyclerAdapterAndSwipe(args.getFilterData());
    setHeaderView();
    setFilterObserver();
    setSortObserver();
    return binding.getRoot();
  }

  private void setHeaderView() {
    vm.getFilterField().observe(getViewLifecycleOwner(), filterField -> {
      binding.tvCompanyName.setText(filterField.getCompanyName());
      binding.tvBankVal.setText(filterField.getBankName());
      binding.tvJenisTransfer.setText(filterField.getTransferName());
      binding.tvRekeningSumber.setText(filterField.getSourceAccountName());
      binding.tvPendingTask.setText(filterField.getPendingTask().booleanValue() ? "Ya" : "Tidak");
      binding.tvBatch.setText(filterField.getBatchName());
      binding.tvTransferStatus.setText(filterField.getTransferStatusName());
      binding.tvJenisTransaksi.setText(filterField.getTransactionTypeName());
    });
  }

  private void setRecyclerAdapterAndSwipe(TugasTransferBrowseAndroidFilter filterField) {
    vm.loadDetailListWithFilter(filterField);
    rv = binding.listDetail;
    actionModeCallBack = new ActionModeCallBack();
    adapter = new TugasTransferBrowseDetailListAdapter(
        new TugasTransferBrowseDetailListAdapter.RecyclerViewAdapterListener() {
          @Override
          public void onItemClicked(View view, TugasTransferBrowseDetailData data, int position) {
//            if (adapter.getSelectedItemCount() > 0) {
              enableActionMode(position);
              vm.setSelectedData(adapter.getSelectedItemCount(), adapter.getSelecteData(position));
//            } else {
//              List<TugasTransferBrowseDetailData> dl = new ArrayList<>();
//              dl.add(data);
//              vm.setSelectedData(1, adapter.getSelecteData(position));
//              openWfFragment(dl, vm.getDecisionList(), false);
//            }
          }

          @Override
          public void onItemLongClickedListener(View v, TugasTransferBrowseDetailData data,
              int position) {
            enableActionMode(position);
            vm.setSelectedData(adapter.getSelectedItemCount(), adapter.getSelecteData(position));
          }
        }, getActivity().getApplicationContext(), appExecutors);
    concatAdapter = adapter.withLoadStateHeaderAndFooter(
        new ExpenseListLoadStateAdapter(retryCallback),
        new ExpenseListLoadStateAdapter(retryCallback));
    adapter.addLoadStateListener((combinedLoadStates) -> {
      vm.setLoadStateDetail(combinedLoadStates.getRefresh());
      return Unit.INSTANCE;
    });
    rv.setAdapter(concatAdapter);
    vm.getRefreshDetailList().observe(getViewLifecycleOwner(), e -> {
      if (e != null && e) {
        adapter.refresh();
      }
    });

    vm.getActionEventDetail().observe(getViewLifecycleOwner(), s -> {
      HyperLog.d(TAG, "getActionEventDetail() :" + s + "<-");
      if (Status.SUCCESS.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
        adapter.refresh();
      } else if (Status.LOADING.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
        ViewUtil.setVisibility(binding.progressWrapper.progressText, View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            s.getMessage() == null ? getString(R.string.msg_please_wait) : s.getMessage());
        ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
      } else {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
      }
    });

    NavController nc = NavHostFragment.findNavController(this);

    LiveData<Boolean> refreshLd = nc.getCurrentBackStackEntry()
        .getSavedStateHandle().getLiveData(SAVED_STATE_LD_REFRESH);
    refreshLd.observe(getViewLifecycleOwner(), refresh -> {
      HyperLog.d(TAG, "refresh signaled: " + refresh);
      // tidak bisa langsung adapter.refresh, sepertinya adapternya belum connect ke RecyclerView-nya
      // jadi kita tunda dengan sinyal melalui viewmodel
      vm.setRefreshDetailList(refresh);
      nc.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(SAVED_STATE_LD_REFRESH);
    });

    binding.swipeRefresh.setOnRefreshListener(() -> {
      adapter.refresh();
      binding.swipeRefresh.setRefreshing(false);
    });
  }

  private View.OnClickListener retryCallback = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // retry hanya mencoba lagi page yang gagal
      adapter.retry();
    }
  };

  private class ActionModeCallBack implements ActionMode.Callback {

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
      mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
      vm.getIsAllowTransferAutomaticBtn().observe(getViewLifecycleOwner(), isAsign -> {
        HyperLog.d(TAG,"transferAutomaticBtn allow["+isAsign+"]");
        mode.getMenu().findItem(R.id.action_approve_transfer_otomatis).setVisible(isAsign);
//        mode.getMenu().findItem(R.id.action_approve_transfer_manual).setVisible(isAsign);
      });

      vm.getIsAllowTransferManualBtn().observe(getViewLifecycleOwner(), isAsign -> {
        HyperLog.d(TAG,"transferManualBtn allow["+isAsign+"]");
//        mode.getMenu().findItem(R.id.action_approve_transfer_otomatis).setVisible(isAsign);
        mode.getMenu().findItem(R.id.action_approve_transfer_manual).setVisible(isAsign);
      });

      mode.getMenu().findItem(R.id.action_approve).setTitle(R.string.action_release);
      vm.getIsAllowReleaseBtn().observe(getViewLifecycleOwner(), isAsign -> {
        HyperLog.d(TAG,"releaseBTn allow["+isAsign+"]");
        mode.getMenu().findItem(R.id.action_approve).setVisible(isAsign);
      });

      vm.getIsAllowReJectBtn().observe(getViewLifecycleOwner(), isAsign -> {
        HyperLog.d(TAG,"rejcetBTn allow["+isAsign+"]");
        mode.getMenu().findItem(R.id.action_reject).setVisible(isAsign);
      });

      vm.getIsAllowReturnBtn().observe(getViewLifecycleOwner(), isAsign -> {
        HyperLog.d(TAG,"returnBTn allow["+isAsign+"]");
        mode.getMenu().findItem(R.id.action_return).setVisible(isAsign);
      });
      mode.getMenu().findItem(R.id.action_delete).setVisible(false);
      return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
      return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
      if (item.getItemId() == R.id.action_approve_transfer_otomatis) {
        List<String> decisionList = new ArrayList<>();
        decisionList.add(Utility.WF_OUTCOME_APPROVE_TRANSFER_OTOMATIS);
        openWfFragment(adapter.getSelectedData(), decisionList, true);
      } else if (item.getItemId() == R.id.action_approve_transfer_manual) {
        List<String> decisionList = new ArrayList<>();
        decisionList.add(Utility.WF_OUTCOME_APPROVE_TRANSFER_MANUAL);
        openWfFragment(adapter.getSelectedData(), decisionList, false);
      } else if (item.getItemId() == R.id.action_approve) {
        List<String> decisionList = new ArrayList<>();
        decisionList.add(WorkflowConstants.WF_OUTCOME_APPROVE);
        openWfFragment(adapter.getSelectedData(), decisionList, false);
      } else if (item.getItemId() == R.id.action_return) {
        List<String> decisionList = new ArrayList<>();
        decisionList.add(WorkflowConstants.WF_OUTCOME_RETURN);
        openWfFragment(adapter.getSelectedData(), decisionList, false);
      } else if (item.getItemId() == R.id.action_reject) {
        List<String> decisionList = new ArrayList<>();
        decisionList.add(WorkflowConstants.WF_OUTCOME_REJECT);
        openWfFragment(adapter.getSelectedData(), decisionList, false);
      } else {
        // TODO BELUM ADA IMPLEMENTASI
      }
      return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
      adapter.clearSelections();
      vm.clearSelectedCandidateData();
      actionMode = null;
      rv.post(new Runnable() {
        @Override
        public void run() {
          adapter.resetAnimationIndex();
        }
      });
    }
  }

  private void setFilterObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<TugasTransferBrowseAndroidFilter> filterField = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        TugasTransferFilterFragment.TUGAS_TRANSFER_FILTER_FIELD_DETAIL);
    filterField.observe(getViewLifecycleOwner(), filterObj -> {
      vm.loadDetailListWithFilter(filterObj);
//      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
//          TugasTransferFilterFragment.TUGAS_TRANSFER_FILTER_FIELD_DETAIL);
    });
  }

  private void setSortObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<String> soListJsonMld = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        TugasTransferDetailBrowseSortFragment.TUGAS_TRANSFER_DETAIL_SORT);

    soListJsonMld.observe(getViewLifecycleOwner(), soListJsonObj -> {
      String soListJson = soListJsonObj.replace("\\", "");
      Type dataType = new TypeToken<List<SortOrder>>() {
      }.getType();
      List<SortOrder> soList = new Gson().fromJson(soListJson, dataType);
      Set<SortOrder> sso = new HashSet<SortOrder>(soList);
      vm.setSortFields(sso);
      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
          TugasTransferDetailBrowseSortFragment.TUGAS_TRANSFER_DETAIL_SORT);
    });
  }

  private void enableActionMode(int pos) {
    if (actionMode == null) {
      actionMode = getActivity().startActionMode((ActionMode.Callback) actionModeCallBack);
    }
    toggleSelection(pos);
  }

  private void toggleSelection(int pos) {
    adapter.toggleSelection(pos);
    int selectedItem = adapter.getSelectedItemCount();
    showHideActionMode(selectedItem);
  }

  private void showHideActionMode(int selectedItems) {
    if (selectedItems == 0) {
      if (actionMode != null)
        actionMode.finish();
    } else {
      actionMode.setTitle(String.valueOf(selectedItems));
      actionMode.invalidate();

    }
  }

  private void openWfFragment(List<TugasTransferBrowseDetailData> dl, List<String> decision, boolean isAuto) {
    String dataJson = new Gson().toJson(dl);
    showHideActionMode(0);
    String decisionJson = new Gson().toJson(decision);
    NavController nc = Navigation.findNavController(binding.getRoot());
    TugasTransferDetailFragmentDirections.ActionWf dir = TugasTransferDetailFragmentDirections.actionWf(
        dataJson, decisionJson, isAuto);
    nc.navigate(dir, NavHelper.animChildToParent().build());
  }
}