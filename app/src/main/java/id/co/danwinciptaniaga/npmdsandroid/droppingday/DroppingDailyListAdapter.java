package id.co.danwinciptaniaga.npmdsandroid.droppingday;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiDroppingDailyBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class DroppingDailyListAdapter extends
    PagingDataAdapter<DroppingDailyData, DroppingDailyListAdapter.ViewHolder> {
  private final static String TAG = DroppingDailyListAdapter.class.getSimpleName();
  private final DddRecyclerViewAdapterListener mListener;
  private SparseBooleanArray selectedItems;
  private List<DroppingDailyData> selectedDdd = new ArrayList<>();
  private SparseBooleanArray animationItemsIndex;
  // private boolean reverseAllAnimations = false;
  // private static int currentSelectedIndex = -1;
  private Context mContext;
  //  @Inject
  public CommonService commonService;
  //  @Inject
  public AppExecutors appExecutors;

  @Inject
  public DroppingDailyListAdapter(
      DddRecyclerViewAdapterListener listener, Context ctx,
      CommonService commonService, AppExecutors appExecutors) {
    super(new DddDiffCallBack());
    mListener = listener;
    selectedItems = new SparseBooleanArray();
    animationItemsIndex = new SparseBooleanArray();
    mContext = ctx;
    this.commonService = commonService;
    this.appExecutors = appExecutors;

  }

  public int getSelectedItemCount() {
    return selectedItems.size();
  }

  public List<DroppingDailyData> getSelectedDdd() {
    return selectedDdd;
  }

  public boolean isDeleteApproveActionActive() {
    for (DroppingDailyData data : selectedDdd) {
      if (!Utility.WF_STATUS_DRAFT.equals(data.getWfStatusCode())) {
        return false;
      }
    }
    return true;
  }

  public boolean isRejectActionActive() {
    for (DroppingDailyData data : selectedDdd) {
      if (!Utility.WF_STATUS_PENDING_APPROVAL.equals(data.getWfStatusCode())) {
        return false;
      }
    }
    return true;
  }

  public void resetAnimationIndex() {
    // reverseAllAnimations = false;
    animationItemsIndex.clear();
  }

  public void toggleSelection(int pos) {
    // currentSelectedIndex = pos;
    DroppingDailyData data = getItem(pos);
    if (selectedItems.get(pos, false)) {
      selectedItems.delete(pos);
      selectedDdd.remove(data);
      animationItemsIndex.delete(pos);
    } else {
      selectedDdd.add(data);
      selectedItems.put(pos, true);
      animationItemsIndex.put(pos, true);
    }
    notifyItemChanged(pos);
  }

  @Override
  public DroppingDailyListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.li_dropping_daily_browse,
        parent, false);
    return new DroppingDailyListAdapter.ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final DroppingDailyListAdapter.ViewHolder holder,
      int position) {
    holder.bind(getItem(position), position);
    // kalau activated, warnanya beda
    holder.binding.getRoot().setActivated(selectedItems.get(position, false));
  }

  public void clearSelections() {
    // reverseAllAnimations = true;
    selectedItems.clear();
    selectedDdd.clear();
    notifyDataSetChanged();
  }

  // @Override
  // public void onBindViewHolder(@NonNull ViewHolder holder, int position,
  // @NonNull List<Object> payloads) {
  // if (!payloads.isEmpty()) {
  // // TODO: apakah perlu bind ulang, atau update field spesifik saja
  // holder.bind(getItem(position));
  // } else {
  // onBindViewHolder(holder, position);
  // }
  // }

  interface DddRecyclerViewAdapterListener {
    void onItemClicked(View view, DroppingDailyData ddd, int position);

    void onItemLongClickedListener(View v, DroppingDailyData ddd, int position);
  }

  private static class DddDiffCallBack
      extends DiffUtil.ItemCallback<DroppingDailyData> {
    @Override
    public boolean areItemsTheSame(@NonNull DroppingDailyData oldItem,
        @NonNull DroppingDailyData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull DroppingDailyData oldItem,
        @NonNull DroppingDailyData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiDroppingDailyBrowseBinding binding;

    public ViewHolder(View view) {
      super(view);
      binding = LiDroppingDailyBrowseBinding.bind(view);
    }

    public void bind(DroppingDailyData ddd, int position) {
      setObject(ddd);
      setObjectListener(ddd, position);
    }

    private void setObjectListener(DroppingDailyData dad, int position) {
      binding.clDroppingDaily.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, dad, position);
        }
      });

      binding.clDroppingDaily.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
          mListener.onItemLongClickedListener(v, dad, position);
          v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
          return true;
        }
      });
    }

    private void setObject(DroppingDailyData ddd) {
      binding.tvTransactionNo.setText(ddd.getTransactionNo());
      binding.tvWfStatus.setText(ddd.getWfStatus());
      binding.tvTransferStatus.setText(ddd.getTransferStatus());
      binding.tvOutletAndCode.setText(
          String.format("%s (%s)", ddd.getOutletName(), ddd.getOutletCode()));
      binding.tvRequestAndDroppingDate.setText(
          String.format("%s (%s)",
              Formatter.DTF_dd_MM_yyyy.format(ddd.getRequestDate()),
              Formatter.DTF_dd_MM_yyyy.format(ddd.getDroppingDate())));
      binding.tvLimitAmount.setText(Formatter.DF_AMOUNT_NO_DECIMAL.format(ddd.getBalanceLimit()));
      binding.tvDroppingBalanceAmount.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(ddd.getBalance()));
      binding.tvRequestAmount.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(ddd.getRequestAmount()));
      binding.tvApprovedAmount.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(ddd.getApprovedAmount()));
    }
  }
}
