package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingDetailResponse;
import id.co.danwinciptaniaga.npmds.data.booking.NewBookingReponse;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class AppBookingLoadUC
    extends BaseObservable<AppBookingLoadUC.Listener, AppBookingLoadUC.LoadResult> {
  private static String TAG = AppBookingLoadUC.class.getSimpleName();
  private final AppBookingService service;
  private final AppExecutors appExecutors;

  @Inject
  public AppBookingLoadUC(Application app, AppBookingService service, AppExecutors appExecutors) {
    super(app);
    this.service = service;
    this.appExecutors = appExecutors;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<LoadResult> result) {
    listener.onLoadStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<LoadResult> result) {
    listener.onLoadSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<LoadResult> result) {
    listener.onLoadFailure(result);
  }

  public ListenableFuture<LoadResult> loadAppBookingData(UUID appBookingId, Boolean isTransfer) {
    notifyStart("Memuat data.. ", null);

    // load info detail appBooking
    ListenableFuture<AppBookingDetailResponse> appbdr = service.loadAppBookingData(appBookingId,
        isTransfer);

    // load info pendukung form
    ListenableFuture<NewBookingReponse> nappbr = service.getNewAppBooking(isTransfer);

    ListenableFuture<LoadResult> allSuccess = Futures.whenAllComplete(Arrays.asList(appbdr, nappbr))
        .call(new Callable<LoadResult>() {
          @Override
          public LoadResult call() throws Exception {
            return new LoadResult(
                Futures.getDone(appbdr),
                Futures.getDone(nappbr));
          }
        }, appExecutors.backgroundIO());
    Futures.addCallback(allSuccess, new FutureCallback<LoadResult>() {
      @Override
      public void onSuccess(@NullableDecl LoadResult result) {
        notifySuccess("Pengajuan Booking berhasil dimuat", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Gagal memuat Pengajuan Booking "+message, null, t);
      }
    }, appExecutors.backgroundIO());

    return allSuccess;
  }

  public ListenableFuture<LoadResult> loadAppBookingDataByBookingId(UUID bookingId, Boolean isEdit,
      Boolean isTransfer) {
    notifyStart(String.valueOf(R.string.memuat_data), null);
    // load info detail appBooking
    ListenableFuture<AppBookingDetailResponse> getDetail = service
        .loadAppBookingDataByBookingId(bookingId, isEdit, isTransfer);
    // load info penukung form
    ListenableFuture<NewBookingReponse> getPrepare = service.getNewAppBooking(isTransfer);

    ListenableFuture<LoadResult> allSuccess = Futures
        .whenAllComplete(Arrays.asList(getDetail, getPrepare)).call(new Callable<LoadResult>() {
          @Override
          public LoadResult call() throws Exception {
            return new LoadResult(Futures.getDone(getDetail), Futures.getDone(getPrepare));
          }
        }, appExecutors.backgroundIO());
    Futures.addCallback(allSuccess, new FutureCallback<LoadResult>() {
      @Override
      public void onSuccess(@NullableDecl LoadResult result) {
        notifySuccess("Pengajuan Booking berhasil dimuat", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Gagal memuat Pengajuan Booking "+message, null, t);
      }
    }, appExecutors.backgroundIO());
    return allSuccess;
  }

  public interface Listener {
    void onLoadStarted();

    void onLoadSuccess(Resource<LoadResult> res);

    void onLoadFailure(Resource<LoadResult> res);
  }

  public static class LoadResult {
    private AppBookingDetailResponse appBookingDetailResponse;
    private NewBookingReponse newBookingReponse;

    public LoadResult(AppBookingDetailResponse appBookingDetailResponse,
        NewBookingReponse newBookingReponse) {
      this.appBookingDetailResponse = appBookingDetailResponse;
      this.newBookingReponse = newBookingReponse;
    }

    public AppBookingDetailResponse getAppBookingDetailResponse() {
      return appBookingDetailResponse;
    }

    public NewBookingReponse getNewBookingReponse() {
      return newBookingReponse;
    }
  }

}
