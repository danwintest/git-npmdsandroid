package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class TugasTransferCekSaldoAccountListUC extends
    BaseObservable<TugasTransferCekSaldoAccountListUC.Listener, List<BankAccountData>> {
  private final String TAG = TugasTransferCekSaldoAccountListUC.class.getSimpleName();
  private final TugasTransferService service;
  private final Application app;
  private final AppExecutors appExecutors;

  @Inject
  public TugasTransferCekSaldoAccountListUC(Application app, TugasTransferService service,
      AppExecutors appExecutors) {
    super(app);
    this.app = app;
    this.service = service;
    this.appExecutors = appExecutors;
  }

  public void getAccountDataListByCompanyIdAndBankId(UUID companyId, UUID bankId) {
    notifyStart("Memuat data.. ", null);
    ListenableFuture<List<BankAccountData>> process = service.getAccountListByCompanyIdAndBankId(companyId,
        bankId);
    Futures.addCallback(process, new FutureCallback<List<BankAccountData>>() {
      @Override
      public void onSuccess(@NullableDecl List<BankAccountData> result) {
        HyperLog.d(TAG, "getAccountDataListByCompanyIdAndBankId() berhasil");
        notifySuccess("-", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String msg = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("getBankDataListByCompanyId() Gagal ->" + msg, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(
      TugasTransferCekSaldoAccountListUC.Listener listener, Resource<List<BankAccountData>> result) {
    listener.onLoadAccountStarted(result);
  }

  @Override
  protected void doNotifySuccess(TugasTransferCekSaldoAccountListUC.Listener listener,
      Resource<List<BankAccountData>> result) {
    listener.onLoadAccountSuccess(result);
  }

  @Override
  protected void doNotifyFailure(TugasTransferCekSaldoAccountListUC.Listener listener,
      Resource<List<BankAccountData>> result) {
    listener.onLoadAccountFailure(result);
  }

  public interface Listener {
    void onLoadAccountStarted(Resource<List<BankAccountData>> loading);

    void onLoadAccountSuccess(Resource<List<BankAccountData>> res);

    void onLoadAccountFailure(Resource<List<BankAccountData>> res);
  }
}
