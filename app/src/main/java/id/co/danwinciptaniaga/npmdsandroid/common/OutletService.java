package id.co.danwinciptaniaga.npmdsandroid.common;

import java.util.List;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import retrofit2.http.Field;
import retrofit2.http.GET;

public interface OutletService {
  @GET("rest/s/user/outlets")
  ListenableFuture<List<OutletShortData>> searchUserOutlets(
      @Field("keyword") String keyword,
      @Field("includeHistory") Boolean includeHistory,
      @Field("activeOnly") Boolean activeOnly);
}
