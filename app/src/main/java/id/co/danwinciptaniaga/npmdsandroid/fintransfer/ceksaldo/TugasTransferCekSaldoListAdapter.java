package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.BalanceInfoDetail;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiTugasTransferCeksaldoBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class TugasTransferCekSaldoListAdapter extends
    RecyclerView.Adapter<TugasTransferCekSaldoListAdapter.ViewHolder> {
  private List<BalanceInfoDetail> dataList = new ArrayList<>();
  private String companyCode;
  private Context mCtx;

  public TugasTransferCekSaldoListAdapter(Context ctx) {
    this.mCtx = ctx;
  }

  public void setDataList(List<BalanceInfoDetail> dataList) {
    this.dataList = dataList;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  @NonNull
  @Override
  public TugasTransferCekSaldoListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.li_tugas_transfer_ceksaldo, parent, false);
    return new TugasTransferCekSaldoListAdapter.ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
  }

  @Override
  public int getItemCount() {
    return dataList.size();
  }

  BalanceInfoDetail getItem(int pos) {
    return dataList.get(pos);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiTugasTransferCeksaldoBinding binding;
    private final View view;

    public ViewHolder(View v) {
      super(v);
      view = v;
      binding = LiTugasTransferCeksaldoBinding.bind(v);
    }

    public void bind(BalanceInfoDetail data, int pos) {
      setObject(data);
      setObjectListener(data, pos);
    }

    private void setObject(BalanceInfoDetail data) {
      binding.tvCompanyCode.setText(companyCode);
      binding.tvAccNo.setText(data.getAccountNo());
      binding.tvCurrency.setText(data.getCurrency());
      binding.tvBalance.setText(Utility.getFormattedAmt(data.getBalance()));
    }

    private void setObjectListener(BalanceInfoDetail data, int pos) {
      binding.clContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          //TODO BELUM ADA IMPLEMENTASI
        }
      });

      binding.clContainer.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
          //          TODO BELUM ADA IMPLEMENTASI
          return true;
        }
      });
    }

  }
}
