package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.jetbrains.annotations.Nullable;

import com.google.common.net.MediaType;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.ListWrapper;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDetailDTO;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import okhttp3.MultipartBody;
import retrofit2.HttpException;

public class DroppingAdditionalSaveUseCase extends
    BaseObservable<DroppingAdditionalSaveUseCase.Listener, DroppingAdditionalData> {

  public interface Listener {

    void onDropppingAdditionalSaveDraftStarted(Resource<DroppingAdditionalData> loading);

    void onDropppingAdditionalSaveDraftSuccess(Resource<DroppingAdditionalData> response);

    void onDropppingAdditionalSaveDraftFailure(Resource<DroppingAdditionalData> response);

  }

  private final DroppingAdditionalService droppingAdditionalService;

  private final AppExecutors appExecutors;

  @Inject
  public DroppingAdditionalSaveUseCase(Application application,
      DroppingAdditionalService droppingAdditionalService, AppExecutors appExecutors) {
    super(application);
    this.droppingAdditionalService = droppingAdditionalService;
    this.appExecutors = appExecutors;
  }

  public void saveDroppingAdditionalDraft(UUID droppingId, UUID outletId, LocalDate requestDate,
      LocalDate droppingDate, List<DroppingDetailDTO> droppingDetails, Date checkTs) {
    notifyStart(null, null);
    ListWrapper<DroppingDetailDTO> droppingDetailsWrapper = new ListWrapper<>();
    droppingDetailsWrapper.setList(droppingDetails);
    MultipartBody.Part[] attachmentParts = prepareAttachmentMultiparts(droppingDetails);
    ListenableFuture<DroppingAdditionalData> saveDraftLf = droppingAdditionalService
        .saveDroppingAdditionalDraft(droppingId, outletId, requestDate, droppingDate,
            droppingDetailsWrapper, attachmentParts, checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<DroppingAdditionalData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingAdditionalData result) {
        notifySuccess(application.getString(R.string.dropping_additional_savedraft_success),
            result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(
              ((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_additional_savedraft_failed) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void submitDroppingAdditionalDraft(UUID droppingId, UUID outletId, LocalDate requestDate,
      LocalDate droppingDate, List<DroppingDetailDTO> droppingDetails, Date checkTs) {
    notifyStart(null, null);
    ListWrapper<DroppingDetailDTO> droppingDetailsWrapper = new ListWrapper<>();
    droppingDetailsWrapper.setList(droppingDetails);
    MultipartBody.Part[] attachmentParts = prepareAttachmentMultiparts(droppingDetails);

    ListenableFuture<DroppingAdditionalData> saveDraftLf = droppingAdditionalService
        .submitDroppingAdditional(droppingId, outletId, requestDate, droppingDate,
            droppingDetailsWrapper, attachmentParts, checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<DroppingAdditionalData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingAdditionalData result) {
        notifySuccess(application.getString(R.string.dropping_additional_submit_success),
            result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(
              ((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_additional_submit_failed) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void submitRevisionDroppingAdditionalDraft(UUID procTaskId, String comment,
      UUID droppingId, UUID outletId, LocalDate requestDate, LocalDate droppingDate,
      List<DroppingDetailDTO> droppingDetails, Date checkTs) {
    notifyStart(null, null);
    ListWrapper<DroppingDetailDTO> droppingDetailsWrapper = new ListWrapper<>();
    droppingDetailsWrapper.setList(droppingDetails);
    MultipartBody.Part[] attachmentParts = prepareAttachmentMultiparts(droppingDetails);
    ListenableFuture<DroppingAdditionalData> saveDraftLf = droppingAdditionalService
        .submitDroppingAdditionalRevision(procTaskId, comment, droppingId, outletId, requestDate,
            droppingDate, droppingDetailsWrapper, attachmentParts, checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<DroppingAdditionalData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingAdditionalData result) {
        notifySuccess(application.getString(R.string.dropping_additional_submit_revision_success),
            result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(
              ((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_additional_submit_revision_failed) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  @Nullable
  private MultipartBody.Part[] prepareAttachmentMultiparts(
      List<DroppingDetailDTO> droppingDetails) {
    List<MultipartBody.Part> attachmentList = new ArrayList<>();
    for (int z = 0; z < droppingDetails.size(); z++) {
      DroppingDetailDTO droppingDetailDTO = droppingDetails.get(z);
      if (droppingDetailDTO.getAttachmentDTO() != null) {
        MultipartBody.Part attachmentPart = RetrofitUtility
            .prepareFilePart(
                DroppingAdditionalService.PARAM_ATTACHMENT_PREFIX + droppingDetailDTO.getDetailId(),
                droppingDetailDTO.getAttachmentDTO().getFile(),
                MediaType.ANY_IMAGE_TYPE.toString());
        attachmentList.add(attachmentPart);
      }
    }
    MultipartBody.Part[] attachmentParts = null;
    if (attachmentList.size() > 0) {
      attachmentParts = attachmentList.toArray(new MultipartBody.Part[0]);
    }
    return attachmentParts;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<DroppingAdditionalData> result) {
    listener.onDropppingAdditionalSaveDraftStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<DroppingAdditionalData> result) {
    listener.onDropppingAdditionalSaveDraftSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<DroppingAdditionalData> result) {
    listener.onDropppingAdditionalSaveDraftFailure(result);
  }
}
