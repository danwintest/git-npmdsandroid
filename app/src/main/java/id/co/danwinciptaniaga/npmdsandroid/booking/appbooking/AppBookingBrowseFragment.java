package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypertrack.hyperlog.HyperLog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.PermissionHelper;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingListLoadStateAdapter;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction.AppBookingMultiActionFragment;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentAppbookingBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;
import kotlin.Unit;

@AndroidEntryPoint
public class AppBookingBrowseFragment extends ProtectedFragment {
  public static final String TAG = AppBookingBrowseFragment.class.getSimpleName();

  private static final String ARG_COLUMN_COUNT = "column-count";
  private FragmentAppbookingBrowseBinding binding;
  private AppBookingBrowseVM mViewModel;
  private AppBookingListAdapter adapter;
  private ConcatAdapter concatAdapter;
  private RecyclerView recyclerView;
  private int mColumnCount = 1;
  private ActionMode actionMode;
  boolean isPermissionCreate = false;
  boolean isPermissionDelete = false;
  Menu optMenu = null;
  private ProgressDialog pd;
  private AppBookingBrowseFragment.ActionModeCallback actionModeCallback;

  public AppBookingBrowseFragment() {
  }

  public static AppBookingBrowseFragment newInstance(int columnCount) {
    AppBookingBrowseFragment fragment = new AppBookingBrowseFragment();
    Bundle args = new Bundle();
    args.putInt(ARG_COLUMN_COUNT, columnCount);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    mViewModel = new ViewModelProvider(requireActivity()).get(AppBookingBrowseVM.class);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentAppbookingBrowseBinding.inflate(inflater);
    actionModeCallback = new ActionModeCallback();
    setAdapterAndRecycler();
    setLoadAdapter();
    setForm();
    setFilterObserver();
    setSortObserver();
    setMultiWfObserver();
    return binding.getRoot();
  }

  private void setLoadAdapter() {
    View.OnClickListener retryCallback = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        adapter.retry();
      }
    };

    concatAdapter = adapter.withLoadStateHeaderAndFooter(
        new AppBookingLoadStateAdapter(retryCallback),
        new AppBookingLoadStateAdapter(retryCallback));

    adapter.addLoadStateListener((combinedLoadStates) -> {
      // listener ini mengupdate LoadState di ViewModel
      mViewModel.setAppBookingLoadState(combinedLoadStates.getRefresh());
      return Unit.INSTANCE;
    });
    // lalu gunakan LiveData observer untuk mengupdate view
    mViewModel.getAppBookingLoadState().observe(getViewLifecycleOwner(), state -> {
      if (state instanceof LoadState.Loading) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
        ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.VISIBLE);
        ViewUtil.setVisibility(binding.progressWrapper.progressText, View.GONE);
        ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
      } else if (state instanceof LoadState.Error) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
        ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.GONE);
        ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.VISIBLE);
        ViewUtil.setVisibility(binding.progressWrapper.progressText,View.VISIBLE);
        ViewUtil.setVisibility(binding.swipeRefresh, View.GONE);
        binding.progressWrapper.retryButton.setOnClickListener(reloadFragment);
        binding.progressWrapper.progressText.setText(
            ((LoadState.Error) state).getError().getMessage());
      } else if (state instanceof LoadState.NotLoading) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        ViewUtil.setVisibility(binding.progressWrapper.progressBar, View.GONE);
        ViewUtil.setVisibility(binding.progressWrapper.progressText, View.GONE);
        ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
        ViewUtil.setVisibility(binding.swipeRefresh,View.VISIBLE);
      }
    });
  }

  private View.OnClickListener reloadFragment = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      adapter.refresh();
      binding.swipeRefresh.setRefreshing(false);
    }
  };

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    if (status.getStatus() == Status.LOADING) {
      binding.tvStatus.setVisibility(View.VISIBLE);
      binding.tvStatus.setText(R.string.msg_please_wait);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.tvStatus.setText(status.getMessage());
      binding.tvStatus.setVisibility(View.GONE);

      recyclerView.setAdapter(concatAdapter);

      Resource<List<PermissionInfo>> permissions = basemodel.getPermissionsLd().getValue();
      isPermissionCreate = PermissionHelper.hasPermission(permissions.getData(),
          PermissionHelper.PERMISSION_TYPE_ENTITY_OP, AppBookingBrowseVM.PERMISSION_CREATE);
      //      boolean isPermissionEdit = PermissionHelper.hasPermission(permissions.getData(),
      //          PermissionHelper.PERMISSION_TYPE_ENTITY_OP,
      //          AppBookingEditHelper.PERMISSION_EDIT);
      //      boolean isPermissionRead = PermissionHelper.hasPermission(permissions.getData(),
      //          PermissionHelper.PERMISSION_TYPE_ENTITY_OP,
      //          AppBookingEditHelper.PERMISSION_READ);
      isPermissionDelete = PermissionHelper.hasPermission(permissions.getData(),
          PermissionHelper.PERMISSION_TYPE_ENTITY_OP,
          AppBookingBrowseVM.PERMISSION_DELETE);
      setPermissionCreateButton(isPermissionCreate);
    }
  }

  private void setPermissionCreateButton(boolean hasPermission) {
    binding.efabAppBookingTransfer.setEnabled(hasPermission);
    binding.efabAppBookingCash.setEnabled(hasPermission);
    binding.efabAppBookingCash.setVisibility(hasPermission ? View.VISIBLE : View.GONE);
    binding.efabAppBookingTransfer.setVisibility(hasPermission ? View.VISIBLE : View.GONE);
  }

  private void setPermissionDeleteButton(boolean hasPermission, Menu menu) {
    MenuItem register = menu.findItem(R.id.action_delete);
    register.setVisible(hasPermission);
  }

  private void setPermissionActionButton(boolean hasPermission, Menu menu) {
    MenuItem actionMenu = menu.findItem(R.id.action_menu);
    actionMenu.setVisible(!hasPermission);
  }

  private void setAdapterAndRecycler() {
    View list = binding.appBookingList;
    if (list instanceof RecyclerView) {
      Context ctx = list.getContext();
      recyclerView = (RecyclerView) list;
      if (mColumnCount <= 1) {
        recyclerView.setLayoutManager(new LinearLayoutManager(ctx));
      } else {
        recyclerView.setLayoutManager(new GridLayoutManager(ctx, mColumnCount));
      }
    }
    adapter = new AppBookingListAdapter(
        new AppBookingListAdapter.MyAppBookingDataRecyclerViewAdapterListener() {
          @Override
          public void onItemClicked(View v, AppBookingData data, int position) {
            if (adapter.getSelectedItemCout() > 0) {
              enableActionMode(position);
            } else {
              NavController navController = Navigation.findNavController(binding.getRoot());
              if (data.getBookingTypeId().equals(Utility.BOOKING_TYPE_TRANSFER)) {
                AppBookingBrowseFragmentDirections.ActionEditTransferFragment dir = AppBookingBrowseFragmentDirections
                    .actionEditTransferFragment(Utility.OPERATION_EDIT, data.getBookingTypeId(),null,
                        isPermissionCreate);
                dir.setAppPBookingId(data.getId());
                navController.navigate(dir, NavHelper.animParentToChild().build());
              } else {
                AppBookingBrowseFragmentDirections.ActionEditCashFragment dir = AppBookingBrowseFragmentDirections
                    .actionEditCashFragment(Utility.OPERATION_EDIT, data.getBookingTypeId(),
                        isPermissionCreate);
                dir.setAppPBookingId(data.getId());
                navController.navigate(dir, NavHelper.animParentToChild().build());
              }
            }
          }

          @Override
          public void onItemLongClicked(View v, AppBookingData data, int pos) {
            enableActionMode(pos);
          }
        }, getActivity().getApplicationContext());
    View.OnClickListener retryCallback = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        adapter.retry();
      }
    };

    adapter.withLoadStateHeaderAndFooter(new BookingListLoadStateAdapter(retryCallback),
        new BookingListLoadStateAdapter(retryCallback));

    recyclerView.setAdapter(adapter);
    mViewModel.getAppBookingList().observe(getViewLifecycleOwner(), objList -> {
      adapter.submitData(getLifecycle(), objList);
    });

    mViewModel.getRefreshList().observe(getViewLifecycleOwner(), e -> {
      if (e != null && e) {
        adapter.refresh();
      }
    });
  }

  private void setForm() {
    binding.efabAppBookingCash.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        NavController navController = Navigation.findNavController(binding.getRoot());

        AppBookingBrowseFragmentDirections.ActionEditCashFragment dir = AppBookingBrowseFragmentDirections
            .actionEditCashFragment(Utility.OPERATION_NEW, Utility.BOOKING_TYPE_CASH,
                isPermissionCreate);
        dir.setAppPBookingId(null);
        navController.navigate(dir, NavHelper.animParentToChild().build());
      }
    });

    binding.efabAppBookingTransfer.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        NavController navController = Navigation.findNavController(binding.getRoot());
        AppBookingBrowseFragmentDirections.ActionEditTransferFragment dir = AppBookingBrowseFragmentDirections
            .actionEditTransferFragment(Utility.OPERATION_NEW, Utility.BOOKING_TYPE_TRANSFER,null,
                isPermissionCreate);
        dir.setAppPBookingId(null);
        navController.navigate(dir, NavHelper.animParentToChild().build());
      }
    });

    binding.swipeRefresh.setOnRefreshListener(() -> {
      adapter.refresh();
      binding.swipeRefresh.setRefreshing(false);
    });

    mViewModel.getFormState().observe(getViewLifecycleOwner(), formState -> {
      if (formState == null)
        return;
      HyperLog.d(TAG, "getFormState.observer() dipanggil setelah actionList");
      HyperLog.d(TAG, "formState ->" + formState.getState() + "<-");
      switch (formState.getState()) {
      case READY:
        if (pd != null)
          pd.dismiss();
        adapter.refresh();
        mViewModel.clearFormState();
        break;
      case ACTION_IN_PROGRESS:
        // FIXME mungkin harus melakukan sesuatu?
        break;
      case LOADING:
        // FIXME mungkin harus melakukan sesuatu?
        break;
      default:
        break;
      }
    });

    mViewModel.getSnackBarMessage().observe(getViewLifecycleOwner(), message -> {
      if (message != null) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_SHORT).show();
        mViewModel.clearSnackBarMessage();
      }
    });
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.expense_browse_menu, menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {

    if (item.getItemId() == R.id.menu_filter) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_p_booking_browse, nc.getCurrentDestination().getId()))
        return true;
      AppBookingBrowseFragmentDirections.ActionFilter dir = AppBookingBrowseFragmentDirections.actionFilter();
      dir.setFilterField(mViewModel.getFilterField());
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    } else if (item.getItemId() == R.id.menu_sort) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_p_booking_browse, nc.getCurrentDestination().getId()))
        return true;
      List<SortOrder> soList = new ArrayList(mViewModel.getSortFields());
      String absJson = new Gson().toJson(soList);
      AppBookingBrowseFragmentDirections.ActionSort dir = AppBookingBrowseFragmentDirections.actionSort();
      dir.setSortField(absJson);
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    } else {
      // FIXME JIKA BUKAN DARI SALAH SATU OPTS INI, MAKA APA YANG HARUS DILAKUKAN?
      // Snackbar.make(binding.getRoot(), "Menu Tidak tersedia", Snackbar.LENGTH_LONG).show();
    }
    return false;
  }

  private void toggleSelection(int pos) {
    adapter.toggleSelection(pos);
    boolean isAllowaCaptainAction = adapter.isAllowaCaptainAction();
    actionMode.getMenu().findItem(R.id.action_approve).setVisible(isAllowaCaptainAction);
    actionMode.getMenu().findItem(R.id.action_return).setVisible(isAllowaCaptainAction);
    actionMode.getMenu().findItem(R.id.action_reject).setVisible(false);

    int count = adapter.getSelectedItemCout();
    if (count == 0) {
      actionMode.finish();
    } else {
      actionMode.setTitle(String.valueOf(count));
      actionMode.invalidate();
    }
  }

  private void enableActionMode(int pos) {
    if (actionMode == null) {
      actionMode = getActivity().startActionMode((ActionMode.Callback) actionModeCallback);
    }
    toggleSelection(pos);
  }

  public void doDeleteRow(ActionMode mode) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setCancelable(true);
    builder.setTitle("Konfirmasi Hapus");
    builder.setMessage("Apakah anda yakin untuk melakukan pengahpusan terkait data ini?");
    builder.setPositiveButton("Hapus", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        pd = ProgressDialog.show(getActivity(), "", getString(R.string.silahkan_tunggu), true);
        mViewModel.deletedSelectedAppBooking(adapter.getSelectedAppBookingData());
        mode.finish();
        adapter.resetAnimationIndex();
      }
    });
    builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        Snackbar.make(getView(), "Cancel", Snackbar.LENGTH_SHORT).show();
        mode.finish();
        adapter.resetAnimationIndex();
      }
    });

    AlertDialog dialog = builder.create();
    try {
      dialog.show();
    } catch (Exception e) {
      HyperLog.e(TAG, "ERROR saat ini menampilkan dialog delete", e);
    }
  }

  public void openWfFragment(String decision) {
    List<AppBookingData> dataList = adapter.getSelectedAppBookingData();
    String dataListJson = new Gson().toJson(dataList);
    //    showHideDetailActionMode(0);
    if (actionMode != null)
      actionMode.finish();

    NavController nc = Navigation.findNavController(binding.getRoot());
    AppBookingBrowseFragmentDirections.ActionWf dir = AppBookingBrowseFragmentDirections.actionWf(
        dataListJson, decision);
    nc.navigate(dir, NavHelper.animChildToParent().build());
  }

  private class ActionModeCallback implements ActionMode.Callback {

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
      mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
      Utility.setActionModeMenu(mode, TAG);
      setPermissionDeleteButton(isPermissionDelete, menu);
      setPermissionActionButton(isPermissionCreate, menu);
      return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
      return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
      switch (item.getItemId()) {
      case R.id.action_delete:
        doDeleteRow(mode);
        return true;
      case R.id.action_return:
        openWfFragment(WorkflowConstants.WF_OUTCOME_RETURN);
        return true;
      case R.id.action_approve:
        openWfFragment(WorkflowConstants.WF_OUTCOME_APPROVE);
        return true;
      case R.id.action_menu:
        return true;
      default:
        Toast.makeText(getContext(), "Menu tidak disupport[" + item.getItemId() + "]",
            Toast.LENGTH_LONG).show();
        return false;
      }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
      adapter.clearSelections();
      actionMode = null;
      recyclerView.post(new Runnable() {
        @Override
        public void run() {
          adapter.resetAnimationIndex();
        }
      });
    }
  }

  private void setMultiWfObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<Boolean> state = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        AppBookingMultiActionFragment.APP_BOOKING_MULTI_WF);
    state.observe(getViewLifecycleOwner(), stateVal -> {
      mViewModel.setRefreshList(true);
//      nc.getCurrentBackStackEntry().getSavedStateHandle()
//          .remove(AppBookingMultiActionFragment.APP_BOOKING_MULTI_WF);
    });
  }

  private void setFilterObserver(){
    NavController navController = NavHostFragment.findNavController(this);
    MutableLiveData<AppBookingBrowseFilter> filterField = navController
        .getCurrentBackStackEntry()
        .getSavedStateHandle()
        .getLiveData(AppBookingFilterFragment.FILTER_FIELD);


    filterField.observe(getViewLifecycleOwner(), filterFieldObj -> {
      mViewModel.loadDataWithFilter(filterFieldObj);
//      navController.getCurrentBackStackEntry().getSavedStateHandle()
//          .remove(AppBookingFilterFragment.FILTER_FIELD);
    });
  }

  private void setSortObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<String> soListJsonMld = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        AppBookingSortFragment.APP_BOOKING_SORT_FIELDS);

    soListJsonMld.observe(getViewLifecycleOwner(), soListJsonObj -> {
      String soListJson = soListJsonObj.replace("\\", "");
      Type dataType = new TypeToken<List<SortOrder>>() {
      }.getType();
      List<SortOrder> soList = new Gson().fromJson(soListJson, dataType);
      Set<SortOrder> sso = new HashSet<SortOrder>(soList);
      mViewModel.setSortFields(sso);
//      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
//          AppBookingSortFragment.APP_BOOKING_SORT_FIELDS);
    });
  }
}