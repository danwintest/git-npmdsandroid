package id.co.danwinciptaniaga.npmdsandroid.booking;

import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmds.data.booking.BookingListResponse;

public class BookingListPagingSource extends ListenableFuturePagingSource<Integer, BookingData> {
  private static final String TAG = BookingListPagingSource.class.getSimpleName();
  private final GetBookingListUseCase getBookingListUseCase;
  private final Executor mBgExecutor;

  public BookingListPagingSource(GetBookingListUseCase getBookingListUseCase,
      Executor mBgExecutor) {
    this.getBookingListUseCase = getBookingListUseCase;
    this.mBgExecutor = mBgExecutor;
  }

  @NotNull
  @Override
  public ListenableFuture<LoadResult<Integer, BookingData>> loadFuture(
      @NotNull LoadParams<Integer> loadParams) {
    Integer nextPageNumber = loadParams.getKey();
    if (nextPageNumber == null) {
      nextPageNumber = 1;
    }
    int pageSize = loadParams.getPageSize();
    ListenableFuture<LoadResult<Integer, BookingData>> pageFuture = Futures.transform(
        getBookingListUseCase.getBookingList(nextPageNumber, pageSize), this::toLoadResult,
        mBgExecutor);
    return Futures.catching(pageFuture, Exception.class, input -> {
      HyperLog.e(TAG, "Error while getting BookingList", input);
      return new LoadResult.Error(input);
    }, mBgExecutor);
  }

  private LoadResult<Integer, BookingData> toLoadResult(@NonNull BookingListResponse response) {
    return new LoadResult.Page<>(response.getBookingData(), null, response.getNextPageNumber(),
        LoadResult.Page.COUNT_UNDEFINED, LoadResult.Page.COUNT_UNDEFINED);
  }
}
