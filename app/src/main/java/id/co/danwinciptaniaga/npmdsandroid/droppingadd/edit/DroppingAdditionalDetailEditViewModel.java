package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.Callable;

import org.acra.ACRA;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.ImageUtility;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.dropping.AttachmentDTO;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.data.ConstraintViolation;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;

public class DroppingAdditionalDetailEditViewModel extends AndroidViewModel {
  private static final String TAG = DroppingAdditionalDetailEditViewModel.class.getSimpleName();

  private final AppExecutors appExecutors;

  private MutableLiveData<DroppingDetailFormData> formData = new MutableLiveData<>();
  private MutableLiveData<Resource<ExpenseData>> actionEvent = new SingleLiveEvent<>();

  private MutableLiveData<String> descriptionErrorLd = new MutableLiveData<>(null);
  private MutableLiveData<String> amountErrorLd = new MutableLiveData<>(null);
  private MutableLiveData<Bitmap> reducedAttachmentLd = new MutableLiveData<>();

  private boolean descriptionOk;
  private boolean amountOk;
  private File attachmentFile;
  private MutableLiveData<Boolean> canProcess = new MutableLiveData<>(false);
  private File tempAttachmentFile;

  @ViewModelInject
  public DroppingAdditionalDetailEditViewModel(Application application, AppExecutors appExecutors) {
    super(application);
    this.appExecutors = appExecutors;
  }

  public LiveData<DroppingDetailFormData> getFormData() {
    return formData;
  }

  public void setFormData(DroppingDetailFormData formData) {
    if (formData.getAttachmentDTO() != null && formData.getAttachmentDTO().getFile() != null) {
      attachmentFile = formData.getAttachmentDTO().getFile();
    }
    this.formData.postValue(formData);
  }

  public void setDescription(String description) {
    try {
      DroppingAdditionalValidationHelper.validateDescription(getApplication(), description);
      formData.getValue().setDescription(description);
      descriptionErrorLd.postValue(null);
      descriptionOk = true;
    } catch (ConstraintViolation constraintViolation) {
      descriptionErrorLd.postValue(constraintViolation.getMessage());
      descriptionOk = false;
    }
    checkCanProcess();
  }

  public MutableLiveData<String> getDescriptionErrorLd() {
    return descriptionErrorLd;
  }

  public void setAmount(Long amount) {
    try {
      DroppingAdditionalValidationHelper.validateAmount(getApplication(), amount);
      formData.getValue().setAmount(amount);
      amountErrorLd.postValue(null);
      amountOk = true;
    } catch (ConstraintViolation constraintViolation) {
      amountErrorLd.postValue(constraintViolation.getMessage());
      amountOk = false;
    }
    checkCanProcess();
  }

  public LiveData<String> getAmountErrorLd() {
    return amountErrorLd;
  }

  public LiveData<Bitmap> getReducedAttachmentLd() {
    return reducedAttachmentLd;
  }

  public LiveData<Boolean> getCanProcess() {
    return canProcess;
  }

  private void checkCanProcess() {
    if (descriptionOk && amountOk) {
      canProcess.postValue(true);
    } else {
      canProcess.postValue(false);
    }
  }

  public void prepareNewDroppingDetailFormData() {
    DroppingDetailFormData formData = new DroppingDetailFormData();
    setFormData(formData);
  }

  public File getAttachmentFile() {
    return attachmentFile;
  }

  public void setAttachmentFile(File attachmentFile) {
    this.attachmentFile = attachmentFile;
  }

  public void setTempAttachmentFile(File tempAttachmentFile) {
    this.tempAttachmentFile = tempAttachmentFile;
  }

  public File getTempAttachmentFile() {
    return tempAttachmentFile;
  }

  public void setAttachmentFilled(boolean isFilled, boolean deleteTemp) {
    canProcess.postValue(false);
    if (isFilled && this.tempAttachmentFile != null) {
      ListenableFuture<File> attachmentLf = Futures.submit(new Callable<File>() {
        @Override
        public File call() throws Exception {
          BitmapFactory.Options bmOptions = new BitmapFactory.Options();

          // buka gambar asli sebagai bitmap
          Bitmap photo = BitmapFactory.decodeFile(tempAttachmentFile.getAbsolutePath(), bmOptions);
          // perkecil (asumsi resolusi kamera lebih tinggi, sehingga perlu diperkecil)
          Bitmap photoRescaled = ImageUtility.getResizedBitmap(photo, 1280);
          int orientation = ImageUtility.getImageOrientation(tempAttachmentFile);
          Bitmap finalPhoto = null;
          if (orientation != 0) {
            // koreksi orientasi kalau perlu rotasi
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);
            finalPhoto = Bitmap.createBitmap(photoRescaled, 0, 0, photoRescaled.getWidth(),
                photoRescaled.getHeight(), matrix, true);
          } else {
            // tidak perlu rotasi
            finalPhoto = photoRescaled;
          }
          try {
            // kita dapatkan versi JPEG dari bitmap yg sudah diresize dan sudah dikoreksi
            // orientasinya
            attachmentFile = ImageUtility.saveBitmapToJpg(getApplication(), finalPhoto,
                getApplication().getCacheDir(), App.IMAGE_DIR, "droppingtambahan-"
                // di mana baiknya constant ini?
            );
            if (deleteTemp) {
              // versi temp (yg masih besar) bisa dihapus
              tempAttachmentFile.delete();
            }
            tempAttachmentFile = null;
            if (photo != finalPhoto) {
              photo.recycle();
            }
            AttachmentDTO attachmentDTO = new AttachmentDTO();
            attachmentDTO.setFile(attachmentFile);
            // biarkan ID kosong?
            Uri uri = Uri.fromFile(attachmentFile);
            attachmentDTO.setFileName(attachmentFile.getName());
            attachmentDTO.setExtension(MimeTypeMap.getFileExtensionFromUrl(uri.toString()));
            formData.getValue().setAttachmentDTO(attachmentDTO);
            // publish bitmap final untuk ditampilkan
            reducedAttachmentLd.postValue(finalPhoto);
          } catch (IOException e) {
            HyperLog.e(TAG, "Failed to post-process attachment", e);
            if (deleteTemp) {
              tempAttachmentFile.delete();
            }
            // kalau error, pastikan attachment jadi kosong
            tempAttachmentFile = null;
            // hapus juga versi final kalau error
            if (attachmentFile != null) {
              attachmentFile.delete();
            }
            attachmentFile = null;
            formData.getValue().setAttachmentDTO(null);
          }
          checkCanProcess();
          return attachmentFile;
        }
      }, appExecutors.backgroundIO());
      Futures.addCallback(attachmentLf, new FutureCallback<File>() {
        @Override
        public void onSuccess(@NullableDecl File result) {
          HyperLog.e(TAG, "Picked Attachment processed: " + result);
        }

        @Override
        public void onFailure(Throwable t) {
          HyperLog.e(TAG, "Problem processing picked attachment", t);
          ACRA.getErrorReporter().handleException(t);
        }
      }, appExecutors.backgroundIO());
    } else {
      if (this.tempAttachmentFile != null) {
        if (deleteTemp) {
          this.tempAttachmentFile.delete();
        }
        this.tempAttachmentFile = null;
      }
      if (this.attachmentFile != null) {
        this.attachmentFile.delete();
        this.attachmentFile = null;
      }
      // publish null untuk mereset tampilan attachment
      reducedAttachmentLd.postValue(null);
      formData.getValue().setAttachmentDTO(null);
      checkCanProcess();
    }
  }
}
