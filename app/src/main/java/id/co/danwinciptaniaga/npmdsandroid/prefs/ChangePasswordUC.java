package id.co.danwinciptaniaga.npmdsandroid.prefs;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class ChangePasswordUC extends BaseObservable<ChangePasswordUC.Listener, Void> {

  public interface Listener {

    void onChangePasswordStarted(Resource<Void> loading);

    void onChangePasswordSuccess(Resource<Void> response);

    void onChangePasswordFailure(Resource<Void> response);

  }

  private final CommonService commonService;
  private final AppExecutors appExecutors;

  @Inject
  public ChangePasswordUC(Application application, CommonService commonService,
      AppExecutors appExecutors) {
    super(application);
    this.commonService = commonService;
    this.appExecutors = appExecutors;
  }

  public void changePassword(String currentPassword, String newPassword, String confirmPassword) {
    notifyStart(null, null);
    ListenableFuture<ResponseBody> lf = commonService.changePassword(currentPassword, newPassword,
        confirmPassword);
    Futures.addCallback(lf, new FutureCallback<ResponseBody>() {
      @Override
      public void onSuccess(@NullableDecl ResponseBody result) {
        notifySuccess(application.getString(R.string.change_password_success), null);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(
              ((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.change_password_failed) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<Void> result) {
    listener.onChangePasswordStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<Void> result) {
    listener.onChangePasswordSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<Void> result) {
    listener.onChangePasswordFailure(result);
  }
}
