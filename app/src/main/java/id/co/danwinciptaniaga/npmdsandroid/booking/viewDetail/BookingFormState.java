package id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail;

public class BookingFormState {
  private String message;
  private State state;

  public enum State {
    LOADING, READY, ACTION_IN_PROGRESS, ERROR
  }

  public BookingFormState(State state, String message) {
    this.state = state;
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public static BookingFormState loading(String message) {
    return new BookingFormState(State.LOADING, message);
  }

  public static BookingFormState ready(String message) {
    return new BookingFormState(State.READY, message);
  }

  public static BookingFormState actionInProgress(String message) {
    return new BookingFormState(State.ACTION_IN_PROGRESS, message);
  }

  public static BookingFormState error(String message) {
    return new BookingFormState(State.ERROR, message);
  }
}
