package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypertrack.hyperlog.HyperLog;

import android.app.PendingIntent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseHeaderData;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentDroppingDailyTaskBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyBrowseFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.DroppingDailyTaskFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.sort.DroppingDailyTaskSort;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListLoadStateAdapter;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;
import kotlin.Unit;

@AndroidEntryPoint
public class DroppingDailyTaskBrowseFragment extends ProtectedFragment {
  private static final String TAG = DroppingDailyBrowseFragment.class.getSimpleName();

  public static final String SAVED_STATE_LD_REFRESH = "refresh";

  private static final String ARG_DROPPING_DAY_DATA = "droppingDailyData";

  private FragmentDroppingDailyTaskBrowseBinding binding;
  private DroppingDailyTaskBrowseVM vm;
  private DroppingDailyTaskHeaderListAdapter adapterHeader;
  private ConcatAdapter concatAdapterHeader;
  private RecyclerView rvHeader;

  @Inject
  CommonService commonService;
  @Inject
  AppExecutors appExecutors;

  private View.OnClickListener restartHeader = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_dropping_daily_browse)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragmentHeader = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // reload fragment ini = memuat ulang seluruh list
      adapterHeader.refresh();
      // karena menggunakan progress indicator sendiri, jadi  tidak perlu yg dari swipeRefresh
      binding.swipeRefreshHeader.setRefreshing(false);
    }
  };

  public DroppingDailyTaskBrowseFragment() {
    // Required empty public constructor
  }

  public static DroppingDailyBrowseFragment newInstance(DroppingDailyData dad) {
    DroppingDailyBrowseFragment fragment = new DroppingDailyBrowseFragment();
    Bundle args = new Bundle();
    args.putSerializable(ARG_DROPPING_DAY_DATA, dad);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    vm = new ViewModelProvider(requireActivity()).get(DroppingDailyTaskBrowseVM.class);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentDroppingDailyTaskBrowseBinding.inflate(inflater, container, false);
    setRecylerAdapterAndSwipeHeader();
    setFilterObserver();
    setSortObserver();
    setHeaderView();
    return binding.getRoot();
  }

  private void setHeaderView() {
    vm.getFilterField().observe(getViewLifecycleOwner(), data -> {
      binding.tvCompanyName.setText(data.getCompanyName());
      if (data.getRequestDate() != null)
        binding.tvDroppingDate.setText(
            Formatter.DTF_dd_MM_yyyy.format(data.getRequestDate()));
      else
        binding.tvDroppingDate.setText("-");
    });

  }

  private void setRecylerAdapterAndSwipeHeader(){
    rvHeader = binding.listHeader;
    adapterHeader = new DroppingDailyTaskHeaderListAdapter(
        new DroppingDailyTaskHeaderListAdapter.DddRecyclerViewAdapterListener() {
          @Override
          public void onItemClicked(View view, DroppingDailyTaskBrowseHeaderData data,
              int position) {
            NavController nc = Navigation.findNavController(binding.getRoot());
            DroppingDailyTaskBrowseFilter filterField =vm.getFilterField().getValue();
            if(filterField!=null) {
              filterField.setBankId(data.getBankId());
              filterField.setAltAccount(data.isOtherBank());
              filterField.setBankName(data.getBankName());
            }
            DroppingDailyTaskBrowseFragmentDirections.ActionDetail dir = DroppingDailyTaskBrowseFragmentDirections.actionDetail(
                filterField,data.getTotalAmt());
            nc.navigate(dir, NavHelper.animParentToChild().build());
          }

        }, getActivity().getApplicationContext(), commonService, appExecutors);

    View.OnClickListener retryCallbackHeader = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // retry hanya mencoba lagi page yang gagal
        adapterHeader.retry();
      }
    };

    concatAdapterHeader = adapterHeader.withLoadStateHeaderAndFooter(
        new ExpenseListLoadStateAdapter(retryCallbackHeader),
        new ExpenseListLoadStateAdapter(retryCallbackHeader));
    adapterHeader.addLoadStateListener((combinedLoadStates) -> {
      // listener ini mengupdate LoadState di ViewModel
      vm.setLoadStateHeader(combinedLoadStates.getRefresh());
      return Unit.INSTANCE;
    });
    rvHeader.setAdapter(concatAdapterHeader);

    vm.getRefreshHeaderList().observe(getViewLifecycleOwner(), e -> {
      if (e != null && e) {
        adapterHeader.refresh();
      }
    });

    vm.getActionEventHeader().observe(getViewLifecycleOwner(), s -> {
      HyperLog.d(TAG, "getActionEvent: " + s + "<-");
      if (Status.SUCCESS.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
        adapterHeader.refresh();
      } else if (Status.LOADING.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
        ViewUtil.setVisibility(binding.progressWrapper.progressText, View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            s.getMessage() == null ? getString(R.string.msg_please_wait) : s.getMessage());
        ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
      } else {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
      }
    });

    NavController nc = NavHostFragment.findNavController(this);

    LiveData<Boolean> refreshLd = nc.getCurrentBackStackEntry()
        .getSavedStateHandle().getLiveData(SAVED_STATE_LD_REFRESH);
    refreshLd.observe(getViewLifecycleOwner(), refresh -> {
      HyperLog.d(TAG, "refresh signaled: " + refresh);
      // tidak bisa langsung adapter.refresh, sepertinya adapternya belum connect ke RecyclerView-nya
      // jadi kita tunda dengan sinyal melalui viewmodel
      vm.setRefreshHeaderList(refresh);
      nc.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(SAVED_STATE_LD_REFRESH);
    });

    binding.swipeRefreshHeader.setOnRefreshListener(() -> {
      // reload fragment ini = memuat ulang seluruh list
      adapterHeader.refresh();
      // karena menggunakan progress indicator sendiri, jadi  tidak perlu yg dari swipeRefresh
      binding.swipeRefreshHeader.setRefreshing(false);
    });
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (item.getItemId() == R.id.menu_filter) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_dropping_daily_task_browse, nc.getCurrentDestination().getId()))
        return true;
      DroppingDailyTaskBrowseFragmentDirections.ActionFilter dir = DroppingDailyTaskBrowseFragmentDirections.actionFilter(
          true);
      dir.setFilterField(vm.getFilterField().getValue());
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    } else if (item.getItemId() == R.id.menu_sort) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_dropping_daily_task_browse, nc.getCurrentDestination().getId()))
        return true;
      List<SortOrder> soList = new ArrayList(vm.getSortFields());
      String absJson = new Gson().toJson(soList);
      DroppingDailyTaskBrowseFragmentDirections.ActionSort dir = DroppingDailyTaskBrowseFragmentDirections.actionSort();
      dir.setSortField(absJson);
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    }
    return false;
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.expense_browse_menu, menu);
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    if (status.getStatus() == Status.LOADING) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      binding.swipeRefreshHeader.setVisibility(View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.GONE);
      binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);

      // retry = restart fragment (karena authentication gagal)
        binding.progressWrapper.retryButton.setOnClickListener(restartHeader);
    }
  }

  private void onReady() {
    // harusnya permission sudah ada kalau authenticationStatusUpdate SUCCESS
    Resource<List<PermissionInfo>> permissions = basemodel.getPermissionsLd().getValue();

    // lalu gunakan LiveData observer untuk mengupdate view
    vm.getLoadStateHeader().observe(getViewLifecycleOwner(), state -> {
      HyperLog.d(TAG, "droppingListLoadState changed: " + state);
      if (state instanceof LoadState.Loading) {
        // sedang loading pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
      } else if (state instanceof LoadState.Error) {
        // terjadi error pada waktu pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);

        // retry = reload semua data
          binding.progressWrapper.retryButton.setOnClickListener(reloadFragmentHeader);
          // sembunyikan list, karena retry akan dihandle oleh button di atas
          binding.swipeRefreshHeader.setVisibility(View.GONE);

        binding.progressWrapper.progressText.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            ((LoadState.Error) state).getError().getMessage());
      } else if (state instanceof LoadState.NotLoading) {
        // sudah ok
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.swipeRefreshHeader.setVisibility(View.VISIBLE);
      }
    });
    // connect data ke adapter
    // supaya tidak hit server lagi pada waktu kembali dari child fragment, hanya load data kalau
    // list masih null
    vm.getHeaderList().observe(getViewLifecycleOwner(), pagingData -> {
      adapterHeader.submitData(getLifecycle(), pagingData);
    });
  }

  private void setFilterObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<DroppingDailyTaskBrowseFilter> filterField = nc
        .getCurrentBackStackEntry()
        .getSavedStateHandle()
        .getLiveData(DroppingDailyTaskFilterFragment.DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_GROUPING);
    filterField.observe(getViewLifecycleOwner(), filterObj -> {
      filterObj.setOutletName(null);
      vm.loadHeaderListWithFilter(filterObj);
//      nc.getCurrentBackStackEntry().getSavedStateHandle()
//          .remove(DroppingDailyTaskFilterFragment.DROPPING_DAILY_TASK_FILTER_FILTER_FIELD_GROUPING);
    });
  }

  private void setSortObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<String> soListJsonMld = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        DroppingDailyTaskSort.DROPPING_DAILY_TASK_SORT);

    soListJsonMld.observe(getViewLifecycleOwner(), soListJsonObj -> {
      String soListJson = soListJsonObj.replace("\\", "");
      Type dataType = new TypeToken<List<SortOrder>>() {
      }.getType();
      List<SortOrder> soList = new Gson().fromJson(soListJson, dataType);
      Set<SortOrder> sso = new HashSet<SortOrder>(soList);
      vm.setSortFields(sso);
      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
          DroppingDailyTaskSort.DROPPING_DAILY_TASK_SORT);
    });
  }
}
