package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskFilterResponse;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.DroppingDailyTaskService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class GetDroppingDailyTaskFilterUC
    extends BaseObservable<GetDroppingDailyTaskFilterUC.Listener, DroppingDailyTaskFilterResponse> {
  private final String TAG = GetDroppingDailyTaskFilterUC.class.getSimpleName();
  private final DroppingDailyTaskService service;
  private final Application app;
  private final AppExecutors appExecutors;

  @Inject
  public GetDroppingDailyTaskFilterUC(Application app, DroppingDailyTaskService service,
      AppExecutors appExecutors) {
    super(app);
    this.service = service;
    this.app = app;
    this.appExecutors = appExecutors;
  }

  @Override
  protected void doNotifyStart(Listener listener,
      Resource<DroppingDailyTaskFilterResponse> result) {
    listener.onProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener,
      Resource<DroppingDailyTaskFilterResponse> result) {
    listener.onProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener,
      Resource<DroppingDailyTaskFilterResponse> result) {
    listener.onProcessFailure(result);
  }

  public void getCompanyFilter() {
    ListenableFuture<DroppingDailyTaskFilterResponse> process = service.getCompanyListFilter();
    Futures.addCallback(process, new FutureCallback<DroppingDailyTaskFilterResponse>() {
      @Override
      public void onSuccess(@NullableDecl DroppingDailyTaskFilterResponse result) {
        HyperLog.d(TAG, "getCompanyFilter() berhasil");
        notifySuccess("-", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("getCompanyFilter() Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void getDefaultCompanyFilter() {
    String fn =  "getDefaultCompanyFilter() ";
    HyperLog.d(TAG,fn+"terpanggil ");
    ListenableFuture<DroppingDailyTaskFilterResponse> process = service.getDefaultCompanyFilter();
    Futures.addCallback(process, new FutureCallback<DroppingDailyTaskFilterResponse>() {
      @Override
      public void onSuccess(@NullableDecl DroppingDailyTaskFilterResponse result) {
        HyperLog.d(TAG, fn + "berhasil ");
        notifySuccess("-", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG + " " + fn, t);
        notifyFailure("getCompanyFilter() Gagal ->" + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public interface Listener {
    void onProcessStarted(Resource<DroppingDailyTaskFilterResponse> loading);

    void onProcessSuccess(Resource<DroppingDailyTaskFilterResponse> res);

    void onProcessFailure(Resource<DroppingDailyTaskFilterResponse> res);
  }
}
