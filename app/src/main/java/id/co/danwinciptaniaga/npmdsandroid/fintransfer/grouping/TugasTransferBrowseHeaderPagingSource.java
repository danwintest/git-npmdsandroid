package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseHeaderData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferHeaderBrowseResponse;

public class TugasTransferBrowseHeaderPagingSource
    extends ListenableFuturePagingSource<Integer, TugasTransferBrowseHeaderData> {
  private static final String TAG = TugasTransferBrowseHeaderPagingSource.class.getSimpleName();
  private final GetTugasTransferBrowseUC uc;
  private final Executor mBgExecutor;

  public TugasTransferBrowseHeaderPagingSource(GetTugasTransferBrowseUC uc, Executor mBgExecutor) {
    this.uc = uc;
    this.mBgExecutor = mBgExecutor;
  }

  @NotNull
  @Override
  public ListenableFuture<LoadResult<Integer, TugasTransferBrowseHeaderData>> loadFuture(
      @NotNull LoadParams<Integer> loadParams) {
    Integer nextPageNumber = loadParams.getKey();
    if (nextPageNumber == null) {
      nextPageNumber = 1;
    }
    int pageSize = loadParams.getPageSize();
    ListenableFuture<LoadResult<Integer, TugasTransferBrowseHeaderData>> pageFuture = Futures.transform(
        uc.getTugasTransferHeaderBrowseList(nextPageNumber, pageSize), this::toLoadResult,
        mBgExecutor);
    return Futures.catching(pageFuture, Exception.class, input -> {
      HyperLog.e(TAG, "ERROR while getting TugasTransferHeaderBrowseList", input);
      ErrorResponse er = RetrofitUtility.parseErrorBody(input);
      return new LoadResult.Error(
          er != null ? new Exception(er.getFormattedMessage(), input) : input);
    }, mBgExecutor);
  }

  private LoadResult<Integer, TugasTransferBrowseHeaderData> toLoadResult(@NonNull
      TugasTransferHeaderBrowseResponse response) {
    return new LoadResult.Page<>(
        response.getDataList(), null, response.getNextPageNumber(), LoadResult.Page.COUNT_UNDEFINED,
        LoadResult.Page.COUNT_UNDEFINED);
  }

}
