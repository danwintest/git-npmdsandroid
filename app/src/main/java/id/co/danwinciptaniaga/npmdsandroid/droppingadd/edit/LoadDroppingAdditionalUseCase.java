package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalDataResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.NewDroppingAdditionalResponse;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import retrofit2.HttpException;

public class LoadDroppingAdditionalUseCase extends
    BaseObservable<LoadDroppingAdditionalUseCase.Listener, LoadDroppingAdditionalUseCase.LoadDroppingAdditionalResult> {

  public interface Listener {
    void onLoadDroppingAdditionalStarted();

    void onLoadDroppingAdditionalSuccess(Resource<LoadDroppingAdditionalResult> result);

    void onLoadDroppingAdditionalFailure(Resource<LoadDroppingAdditionalResult> responseError);
  }

  public static class LoadDroppingAdditionalResult {
    private DroppingAdditionalDataResponse droppingAdditionalDataResponse;
    private NewDroppingAdditionalResponse newDroppingAdditionalResponse;

    public LoadDroppingAdditionalResult(
        DroppingAdditionalDataResponse droppingAdditionalDataResponse,
        NewDroppingAdditionalResponse newDroppingAdditionalResponse) {
      this.droppingAdditionalDataResponse = droppingAdditionalDataResponse;
      this.newDroppingAdditionalResponse = newDroppingAdditionalResponse;
    }

    public DroppingAdditionalDataResponse getDroppingAdditionalDataResponse() {
      return droppingAdditionalDataResponse;
    }

    public NewDroppingAdditionalResponse getNewDroppingAdditionalResponse() {
      return newDroppingAdditionalResponse;
    }
  }

  private final DroppingAdditionalService droppingAdditionalService;
  private final AppExecutors appExecutors;

  @Inject
  public LoadDroppingAdditionalUseCase(Application app,
      DroppingAdditionalService droppingAdditionalService, AppExecutors appExecutors) {
    super(app);
    this.droppingAdditionalService = droppingAdditionalService;
    this.appExecutors = appExecutors;
  }

  public ListenableFuture<LoadDroppingAdditionalResult> load(UUID droppingId) {
    notifyStart(null, null);

    ListenableFuture<DroppingAdditionalDataResponse> dadrLf = droppingAdditionalService.loadDropping(
        droppingId);
    // data yg diperlukan form
    ListenableFuture<NewDroppingAdditionalResponse> ndarLf = droppingAdditionalService.getNewDroppingAdditional();

    // kumpulkan hasil dari 2 call di atas
    ListenableFuture<LoadDroppingAdditionalResult> allSuccessLf = Futures.whenAllSucceed(
        Arrays.asList(dadrLf, ndarLf))
        .call(new Callable<LoadDroppingAdditionalResult>() {
          @Override
          public LoadDroppingAdditionalResult call() throws Exception {
            return new LoadDroppingAdditionalResult(
                Futures.getDone(dadrLf),
                Futures.getDone(ndarLf));
          }
        }, appExecutors.backgroundIO());

    Futures.addCallback(allSuccessLf, new FutureCallback<LoadDroppingAdditionalResult>() {
      @Override
      public void onSuccess(@NullableDecl LoadDroppingAdditionalResult result) {
        notifySuccess(application.getString(R.string.dropping_additional_load_success), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(
              ((HttpException) t).response().errorBody());
        }
        notifyFailure(
            er == null ? application.getString(R.string.dropping_additional_load_failed) : null,
            er, t);
      }
    }, appExecutors.backgroundIO());
    return allSuccessLf;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<LoadDroppingAdditionalResult> result) {
    listener.onLoadDroppingAdditionalStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<LoadDroppingAdditionalResult> result) {
    listener.onLoadDroppingAdditionalSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<LoadDroppingAdditionalResult> result) {
    listener.onLoadDroppingAdditionalFailure(result);
  }
}
