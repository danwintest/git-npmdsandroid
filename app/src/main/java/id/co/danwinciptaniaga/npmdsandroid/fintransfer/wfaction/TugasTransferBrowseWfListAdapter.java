package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseDetailData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiTugasTransferDetailBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiTugasTransferWfBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class TugasTransferBrowseWfListAdapter extends
    RecyclerView.Adapter<TugasTransferBrowseWfListAdapter.ViewHolder> {
  private List<TugasTransferBrowseDetailData> dataList = new ArrayList<>();
  private Context mCtx;

  public TugasTransferBrowseWfListAdapter(Context ctx) {
    this.mCtx = ctx;
  }

  public void setDataList(
      List<TugasTransferBrowseDetailData> dataList) {
    this.dataList = dataList;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.li_tugas_transfer_wf_browse, parent, false);
    return new ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiTugasTransferWfBrowseBinding binding;
    private final View view;

    public ViewHolder(View v) {
      super(v);
      view = v;
      binding = LiTugasTransferWfBrowseBinding.bind(v);
    }

    public void bind(TugasTransferBrowseDetailData data, int pos) {
      setObject(data);
      setObjectListener(data, pos);
    }

    private void setObject(TugasTransferBrowseDetailData data) {
      binding.tvTrxNo.setText(data.getTransactionNo());
//      binding.tvJenisTransferTransferDate.setText(data.getTransferInfo());
      binding.tvOutletNameOutletCode.setText(data.getOutletInfo());
      binding.tvJenisTransaksi.setText(data.getJenisTransaksiName());
//      binding.tvStatusTransfer.setText(data.getTransferStatus());
      binding.tvNamaTugas.setText(data.getNamaTugas());
//      binding.tvBatch.setText(data.getBatchName());
      binding.tvAccNameAccNoIsAlt.setText(data.getAccInfo());
      binding.tvAmountVal.setText(Utility.getFormattedAmt(data.getAmount()));
    }

    private void setObjectListener(TugasTransferBrowseDetailData data, int pos) {
      binding.clContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          //TODO BELUM ADA IMPLEMENTASI
        }
      });

      binding.clContainer.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
//          TODO BELUM ADA IMPLEMENTASI
          return true;
        }
      });
    }

  }

  interface RecyclerViewAdapterListener {
//    void onItemClicked(View view, TugasTransferBrowseDetailData data, int position);
//
//    void onItemLongClickedListener(View v, TugasTransferBrowseDetailData data, int position);
  }

  private static class DiffCallBack extends DiffUtil.ItemCallback<TugasTransferBrowseDetailData> {

    @Override
    public boolean areItemsTheSame(@NonNull TugasTransferBrowseDetailData oldItem,
        @NonNull TugasTransferBrowseDetailData newItem) {
      return oldItem.getTransferInstructionId() == newItem.getTransferInstructionId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull TugasTransferBrowseDetailData oldItem,
        @NonNull TugasTransferBrowseDetailData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public List<TugasTransferBrowseDetailData> getDataList() {
    return dataList;
  }

  @Override
  public int getItemCount() {
    return dataList.size();
  }

  TugasTransferBrowseDetailData getItem(int pos) {
    return dataList.get(pos);
  }

}
