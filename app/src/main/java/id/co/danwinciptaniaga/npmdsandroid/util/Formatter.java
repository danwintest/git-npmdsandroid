package id.co.danwinciptaniaga.npmdsandroid.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Formatter {
  public static final SimpleDateFormat SDF_dd_MM_yyyy = new SimpleDateFormat("dd-MM-yyyy");
  public static final DateTimeFormatter DTF_dd_MM_yyyy = DateTimeFormatter.ofPattern("dd-MM-yyyy")
      .withZone(ZoneId.systemDefault());
  public static final DateTimeFormatter DTF_dd_MM_yyyy_HH_mm = DateTimeFormatter
      .ofPattern("dd-MM-yyyy HH:mm").withZone(ZoneId.systemDefault());

  public static final DecimalFormat DF_AMOUNT_NO_DECIMAL = new DecimalFormat("#,###");
  public static final DecimalFormat DF_AMOUNT_2_DECIMAL = new DecimalFormat("#,##0.00");
}
