package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.util.LinkedHashSet;
import java.util.Set;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.LoadState;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceData;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletReportSort;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class OutletBalanceBrowseVM extends AndroidViewModel {

  private final GetOutletBalanceListUseCase ucGetOutletBalanceList;

  private Set<SortOrder> sortFields = new LinkedHashSet<>(); // urutan pengaruh

  private final Pager<Integer, OutletBalanceData> pager;
  private LiveData<PagingData<OutletBalanceData>> outletBalanceList;
  private MutableLiveData<Boolean> refreshList = new SingleLiveEvent<>();
  private MutableLiveData<LoadState> outletBalanceListLoadState = new MutableLiveData<>();

  @ViewModelInject
  public OutletBalanceBrowseVM(@NonNull Application application,
      AppExecutors appExecutors,
      GetOutletBalanceListUseCase getOutletBalanceListUseCase) {
    super(application);
    this.ucGetOutletBalanceList = getOutletBalanceListUseCase;

    this.sortFields.add(OutletReportSort
        .sortBy(OutletReportSort.Field.OUTLET_NAME, SortOrder.Direction.DESC));

    pager = new Pager<Integer, OutletBalanceData>(
        new PagingConfig(Utility.PAGE_SIZE),
        () -> new OutletBalancePagingSource(ucGetOutletBalanceList,
            appExecutors.networkIO())
    );
  }

  public LiveData<LoadState> getOutletBalanceListLoadState() {
    return outletBalanceListLoadState;
  }

  public void setOutletBalanceListLoadState(LoadState loadState) {
    this.outletBalanceListLoadState.postValue(loadState);
  }

  public LiveData<PagingData<OutletBalanceData>> getOutletBalanceList() {
    if (outletBalanceList == null) {
      // hanya load data pertama kali
      CoroutineScope viewModelScope = ViewModelKt.getViewModelScope(this);
      // data akan di-load otomatis setelah setup berikut
      outletBalanceList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pager),
          viewModelScope);
    }
    return outletBalanceList;
  }

  public void setRefreshList(Boolean refreshList) {
    this.refreshList.postValue(refreshList);
  }

  public LiveData<Boolean> getRefreshList() {
    return refreshList;
  }
}
