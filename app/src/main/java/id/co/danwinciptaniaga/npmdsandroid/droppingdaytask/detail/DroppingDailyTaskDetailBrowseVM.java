package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.LoadState;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingSort;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskFilterResponse;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.DroppingDailyTaskBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.tugasdropping.TugasDroppingHarianDetailSort;
import id.co.danwinciptaniaga.npmds.data.wf.ProcTaskData;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.filter.GetDroppingDailyTaskFilterUC;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.grouping.DroppingDailyTaskBrowseVM;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class DroppingDailyTaskDetailBrowseVM extends AndroidViewModel implements
    GetDroppingDailyTaskFilterUC.Listener {
  public static final String PERMISSION_MULTIPLE_APPROVAL ="npmds.dropping.daily.multipleApproval";
  public static final String PERMISSION_UPDATE = "npmds_Dropping:update";
  private final String TAG = DroppingDailyTaskBrowseVM.class.getSimpleName();
  private LiveData<PagingData<DroppingDailyTaskBrowseDetailData>> detailList;
  private final GetDroppingDailyTaskBrowseDetailUC ucDetail;
  private final Pager<Integer, DroppingDailyTaskBrowseDetailData> pagerDetail;
  private MutableLiveData<LoadState> loadStateDetail = new MutableLiveData<>();
  private MutableLiveData<Boolean> refreshDetailList = new SingleLiveEvent<>();
  private MutableLiveData<Resource<String>> actionEventDetail = new SingleLiveEvent<>();
  private DroppingDailyTaskBrowseFilter filterField = new DroppingDailyTaskBrowseFilter();
  private Set<SortOrder> sortFields = new LinkedHashSet<>();
  private BigDecimal totalAmt = BigDecimal.ZERO;
  private BigDecimal selectedTotalAmt = BigDecimal.ZERO;
  private MutableLiveData<Boolean> readyHeaderView = new MutableLiveData<>(false);
  private final AppExecutors appExecutors;

  @ViewModelInject
  public DroppingDailyTaskDetailBrowseVM(@NonNull Application app, AppExecutors appExecutors,GetDroppingDailyTaskBrowseDetailUC ucDetail){
    super(app);
    this.ucDetail = ucDetail;
    this.appExecutors = appExecutors;
    this.sortFields.add(TugasDroppingHarianDetailSort
        .sortBy(TugasDroppingHarianDetailSort.Field.DROPPING_DATE, SortOrder.Direction.DESC));

    pagerDetail = new Pager<Integer, DroppingDailyTaskBrowseDetailData>(
        new PagingConfig(Utility.PAGE_SIZE),
        () -> new DroppingDailyTaskBrowseDetailPagingSource(this.ucDetail,
            this.appExecutors.networkIO()));
    CoroutineScope vmScopeDetail = ViewModelKt.getViewModelScope(this);
    detailList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pagerDetail), vmScopeDetail);
  }

  public LiveData<PagingData<DroppingDailyTaskBrowseDetailData>> getDroppingDailyDetailList(){
    HyperLog.d(TAG,"getDroppingDailyDetailList() terpanggil");
    if(detailList == null){
      CoroutineScope vmScope = ViewModelKt.getViewModelScope(this);
      detailList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pagerDetail),vmScope);
    }
    return detailList;
  }

  public MutableLiveData<LoadState> getLoadStateDetail() {
    return loadStateDetail;
  }

  public void setLoadStateDetail(LoadState loadStateDetail){
    this.loadStateDetail.postValue(loadStateDetail);
  }

  public LiveData<Boolean> getRefreshDetailList() {
    return refreshDetailList;
  }

  public LiveData<Resource<String>> getActionEventDetail() {
    return actionEventDetail;
  }

  public void setRefreshDetailList(Boolean refreshDetailList) {
    this.refreshDetailList.postValue(refreshDetailList);
  }

  public void setFitlerField(DroppingDailyTaskBrowseFilter filterField){
    this.filterField = filterField;
  }

  public DroppingDailyTaskBrowseFilter getFilterField() {
    return filterField;
  }

  public void setSortFields(Set<SortOrder> sortFields) {
    this.sortFields.clear();
    if (sortFields != null) {
      this.sortFields.addAll(sortFields);
    }
    ucDetail.setSortField(sortFields);
    refreshDetailList.postValue(true);
  }

  public Set<SortOrder> getSortFields() {
    return this.sortFields;
  }

  public MutableLiveData<Boolean> isReadyHeaderView() {
    return readyHeaderView;
  }

  public void setReadyHeaderView(Boolean isReady) {
    this.readyHeaderView.postValue(isReady);
  }

  public BigDecimal getTotalAmt() {
    return totalAmt;
  }

  public void setTotalAmt(BigDecimal amt) {
    totalAmt = amt;
  }

  public BigDecimal getSelectedTotalAmt() {
    return selectedTotalAmt;
  }

  public void setSelectedTotalAmt(BigDecimal amt) {
    selectedTotalAmt= amt;
    setReadyHeaderView(true);
  }

  public void loadDetailListWithFilter(DroppingDailyTaskBrowseFilter filterField) {
    setFitlerField(filterField);
    setReadyHeaderView(true);
    HyperLog.d(TAG,"loadDroppingDailyDetail() terpanggil "
        + "companyId[" +filterField.getCompanyId() + "] "
        + "bankId["+ filterField.getBankId() + "] "
        + "isOtherBank[" + filterField.isAltAccount() + "]"
        +"outletName["+filterField.getOutletName()+"]");

    ucDetail.setFilterField(filterField);
    refreshDetailList.postValue(true);
  }


  @Override
  public void onProcessStarted(Resource<DroppingDailyTaskFilterResponse> loading) {
    //TODO BELUM ADA IMPLEMENTASI
    //    setLoadStateHeader(AppBookingLoadUC.LoadResult);
    HyperLog.d(TAG, "belum ada implementasi");
  }

  @Override
  public void onProcessSuccess(Resource<DroppingDailyTaskFilterResponse> res) {
    //TODO BELUM ADA IMPLEMENTASI
//    if (filterField.getCompanyId() != null)
//      return;
//    else {
//      DroppingDailyTaskBrowseFilter filterData = new DroppingDailyTaskBrowseFilter();
//      filterData.setCompanyId(res.getData().getDefaultFilterCompanyId());
//      loadHeaderListWithFilter(filterData);
//    }
  }

  @Override
  public void onProcessFailure(Resource<DroppingDailyTaskFilterResponse> res) {
    //TODO BELUM ADA IMPLEMENTASI
    HyperLog.d(TAG, "belum ada implementasi");
  }

  public List<String> getSelectedSingleWf(List<ProcTaskData> ptList) {
    List<String> wfList = new ArrayList<>();
    ProcTaskData pd = ptList.size() > 0 ? ptList.get(0) : null;
    if (pd != null && pd.getPossibleOutcome() != null)
      wfList = pd.getPossibleOutcome();

    return wfList;
  }

}
