package id.co.danwinciptaniaga.npmdsandroid.auth;

import javax.inject.Inject;

import android.util.Log;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.AccountAuthenticator;
import id.co.danwinciptaniaga.androcon.auth.AuthenticatorService;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCaseSync;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;

@AndroidEntryPoint
public class NpmdsAuthenticatorService extends AuthenticatorService {
  private static final String TAG = NpmdsAuthenticatorService.class.getSimpleName();

  @Inject
  GetTokenUseCaseSync ucGetTokenSync;
  @Inject
  LoginUtil loginUtil;

  @Override
  protected AccountAuthenticator getAuthenticator() {
    if (null == AuthenticatorService.sAccountAuthenticator) {
      AuthenticatorService.sAccountAuthenticator = new NpmdsAccountAuthenticator(this,
          ucGetTokenSync, loginUtil);
    }
    Log.i(TAG, "getAuthenticator: " + AuthenticatorService.sAccountAuthenticator);
    return AuthenticatorService.sAccountAuthenticator;

  }
}
