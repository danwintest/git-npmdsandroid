package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.wfaction;

import java.util.ArrayList;
import java.util.List;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskProcessResultData;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;

public class DroppingDailyTaskWfVM extends AndroidViewModel
    implements DroppingDailyTaskProcessUC.Listener {
  private static String TAG = DroppingDailyTaskWfVM.class.getSimpleName();
  private List<DroppingDailyTaskBrowseDetailData> dataList = new ArrayList<>();
  private List<String> decisionList = new ArrayList<>();
  private final DroppingDailyTaskProcessUC processUC;
  private MutableLiveData<FormState> formState = new MutableLiveData<>();

  @ViewModelInject
  public DroppingDailyTaskWfVM(@NonNull Application application,
      DroppingDailyTaskProcessUC processUC) {
    super(application);
    this.processUC = processUC;
    this.processUC.registerListener(this);
  }

  public List<DroppingDailyTaskBrowseDetailData> getDataList() {
    return dataList;
  }

  public void setDataList(List<DroppingDailyTaskBrowseDetailData> dataList) {
    this.dataList = dataList;
  }

  //  public MutableLiveData<PagingData<DroppingDailyTaskBrowseDetailData>> getDataList() {
  //    return dataList;
  //  }

  //  public void setDataList(
  //      List<DroppingDailyTaskBrowseDetailData> dataList) {
  //    DroppingDailyTaskBrowseDetailResponse res = new DroppingDailyTaskBrowseDetailResponse();
  //    res.setDroppingDailyTaskBrowseDetailDataList(dataList);
  //    res.setNextPageNumber(2);
  //    PagingData<DroppingDailyTaskBrowseDetailData> pd = new Pager<Integer, DroppingDailyTaskBrowseDetailData>(
  //        new PagingConfig(dataList.size()), convertListToPagingSource(res));
  //    this.dataList.postValue(pd);
  //  }

  //  private PagingSource.LoadResult<Integer, DroppingDailyTaskBrowseDetailData> convertListToPagingSource(@NonNull
  //      DroppingDailyTaskBrowseDetailResponse response) {
  //    return new PagingSource.LoadResult.Page<>(response.getDroppingDailyTaskBrowseDetailDataList(), null,
  //        response.getNextPageNumber(), PagingSource.LoadResult.Page.COUNT_UNDEFINED,
  //        PagingSource.LoadResult.Page.COUNT_UNDEFINED);
  //  }

  public List<String> getDecisionList() {
    return decisionList;
  }

  public void setDecisionList(List<String> decisionList) {
    this.decisionList = decisionList;
  }

  public MutableLiveData<FormState> getFormState() {
    return formState;
  }

  public void processApprove(List<DroppingDailyTaskBrowseDetailData> dataList, String comment) {
    List<DroppingDailyTaskBrowseDetailData> finalDataList = new ArrayList<>();
    for (DroppingDailyTaskBrowseDetailData data : dataList) {
      data.setRequestDate(null);
      finalDataList.add(data);
    }
    processUC.processApprove(finalDataList, comment);
  }

  public void processReject(List<DroppingDailyTaskBrowseDetailData> dataList, String comment) {
    processUC.processReject(dataList, comment);
  }

  @Override
  public void onProcessStarted(Resource<DroppingDailyTaskProcessResultData> loading) {
    HyperLog.d(TAG, loading.getMessage());
    formState.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessSuccess(Resource<DroppingDailyTaskProcessResultData> response) {
    HyperLog.d(TAG, response.getMessage());
    DroppingDailyTaskProcessResultData resData = response.getData();
    if (resData != null) {
      StringBuilder sbTrxNo = new StringBuilder();

      for (String trxNo : resData.getTrxNoList())
        sbTrxNo.append(trxNo + ", ");
      if(!sbTrxNo.toString().isEmpty()) {
        formState.postValue(FormState.ready(sbTrxNo.toString() + "Gagal Di Proses"));
      }else{
        formState.postValue(FormState.ready(response.getMessage()));
      }

      StringBuilder sbErrMsg = new StringBuilder();
      for(String errorMsg :resData.getErrorMsg())
        sbErrMsg.append(errorMsg+", ");
      HyperLog.d(TAG, sbErrMsg.toString());

    } else {
      formState.postValue(FormState.ready(response.getMessage()));
    }

  }

  @Override
  public void onProcessFailure(Resource<DroppingDailyTaskProcessResultData> resource) {
    HyperLog.e(TAG, resource.getMessage());
    formState.postValue(FormState.error(resource.getMessage()));
  }
}
