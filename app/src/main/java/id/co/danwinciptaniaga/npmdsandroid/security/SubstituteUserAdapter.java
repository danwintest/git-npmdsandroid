package id.co.danwinciptaniaga.npmdsandroid.security;

import java.util.Objects;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.common.ExtUserInfoData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiUserBinding;

public class SubstituteUserAdapter
    extends ListAdapter<ExtUserInfoData, SubstituteUserAdapter.ViewHolder> {
  private Context mContext;
  private Listener mListener;

  private static class DiffCallBack extends DiffUtil.ItemCallback<ExtUserInfoData> {
    @Override
    public boolean areItemsTheSame(@NonNull ExtUserInfoData oldItem,
        @NonNull ExtUserInfoData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull ExtUserInfoData oldItem,
        @NonNull ExtUserInfoData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiUserBinding binding;

    public ViewHolder(View view) {
      super(view);
      binding = LiUserBinding.bind(view);
    }

    public void bind(ExtUserInfoData userData, int position) {
      binding.tvText.setText(String.format("%s [%s]",
          StringUtils.defaultIfBlank(userData.getName(), StringUtils.EMPTY),
          userData.getLogin()));
      binding.clUser.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, userData, position);
        }
      });
    }
  }

  interface Listener {
    void onItemClicked(View v, ExtUserInfoData userInfoData, int position);
  }

  @Inject
  public SubstituteUserAdapter(Context context, Listener listener) {
    super(new SubstituteUserAdapter.DiffCallBack());
    mListener = listener;
    mContext = context;
  }

  @Override
  public SubstituteUserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.li_user, parent, false);
    return new SubstituteUserAdapter.ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final SubstituteUserAdapter.ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
  }
}
