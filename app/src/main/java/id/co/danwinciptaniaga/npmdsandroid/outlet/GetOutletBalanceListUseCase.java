package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceListResponse;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletReportFilter;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletReportSort;

public class GetOutletBalanceListUseCase {
  private static final String TAG = GetOutletBalanceListUseCase.class.getSimpleName();

  private final OutletReportService outletReportService;
  private final AppExecutors appExecutors;

  private OutletReportFilter filter = new OutletReportFilter();
  private OutletReportSort sort = new OutletReportSort();

  @Inject
  public GetOutletBalanceListUseCase(OutletReportService outletReportService,
      AppExecutors appExecutors) {
    this.outletReportService = outletReportService;
    this.appExecutors = appExecutors;
  }

  public ListenableFuture<OutletBalanceListResponse> getOutletBalanceList(Integer page,
      Integer pageSize) {
    ListenableFuture<OutletBalanceListResponse> result = outletReportService.getOutletBalanceList(
        page,
        pageSize, filter, sort);
    return result;
  }

  public void setCompanyIdFilter(UUID companyId) {
    this.filter.setCompanyId(companyId);
  }

  public void setSortOrders(Set<SortOrder> sortOrders) {
    this.sort.setSortOrders(sortOrders);
  }
}
