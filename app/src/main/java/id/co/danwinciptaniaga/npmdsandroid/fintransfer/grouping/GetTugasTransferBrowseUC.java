package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import java.util.Set;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;

import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseSort;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferHeaderBrowseResponse;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;

public class GetTugasTransferBrowseUC {
  private final TugasTransferService service;
  private TugasTransferBrowseAndroidFilter filter = new TugasTransferBrowseAndroidFilter();
  private TugasTransferBrowseSort sort = new TugasTransferBrowseSort();

  @Inject
  public GetTugasTransferBrowseUC(TugasTransferService service) {
    this.service = service;
  }

  public ListenableFuture<TugasTransferHeaderBrowseResponse> getTugasTransferHeaderBrowseList(
      Integer page, int pageSize) {
    if (filter.getCompanyId() == null) {
      ListenableFuture<TugasTransferHeaderBrowseResponse> emptyResult = SettableFuture.create();
      return emptyResult;
    }

    filter.setOutletName(null);
    ListenableFuture<TugasTransferHeaderBrowseResponse> result = service.getHeaderList(page,
        pageSize, filter, sort);
    return result;
  }

  public TugasTransferBrowseAndroidFilter getFilter() {
    return filter;
  }

  public void setFilter(TugasTransferBrowseAndroidFilter filter) {
    this.filter = filter;
  }

  public void setSort(Set<SortOrder> sortField){
    this.sort.setSortOrders(sortField);
  }
}
