package id.co.danwinciptaniaga.npmdsandroid.di;

import javax.inject.Singleton;

import android.content.Context;
import android.util.Log;
import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.components.ApplicationComponent;
import dagger.hilt.android.qualifiers.ApplicationContext;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.retrofit.WebServiceGenerator;
import id.co.danwinciptaniaga.androcon.utility.Utility;
import id.co.danwinciptaniaga.npmdsandroid.BuildConfig;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingService;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyService;
import id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.DroppingDailyTaskService;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import id.co.danwinciptaniaga.npmdsandroid.outlet.OutletReportService;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
@InstallIn(ApplicationComponent.class)
public class NpmdsModule {
  private static final String TAG = NpmdsModule.class.getSimpleName();

  @Singleton
  @Provides
  public AndroconConfig provideAndroconConfig(@ApplicationContext Context context) {
    Log.d(TAG, "provideAndroconConfig invoked");
    String androconUrl = Utility.getMetadata(context, "androcon_url");
    return new AndroconConfig.Builder()
        .setBuildConfigClass(BuildConfig.class)
        .setApkFileName("npmdsandroid.apk")
        .setAndroconUrl(androconUrl)
        .setClientId(Utility.getMetadata(context, "api_client_id"))
        .setClientSecret(Utility.getMetadata(context, "api_client_secret"))
        .setHyperlogUrl(androconUrl + "/logs")
        .setHyperLogLevel(Log.VERBOSE)
        .setAcraUrl(Utility.getMetadata(context, "acra_url"))
        .setAcraUsername(Utility.getMetadata(context, "acra_username"))
        .setAcraPassword(Utility.getMetadata(context, "acra_password"))
        .setRetrofitHttpLoggingLevel(HttpLoggingInterceptor.Level.BODY)
        .build();
  }

  @Provides
  public ExpenseService provideExpenseService(WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(ExpenseService.class);
  }

  @Provides
  public CommonService provideCommonService(WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(CommonService.class);
  }

  @Provides
  public BookingService provideBookingService(WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(BookingService.class);
  }

  @Provides
  public AppBookingService provideAppBookingService(WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(AppBookingService.class);
  }

  @Provides
  public DroppingDailyService provideDroppingDailyService(WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(DroppingDailyService.class);
  }

  @Provides
  public DroppingAdditionalService provideDroppingAdditionalService(
      WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(DroppingAdditionalService.class);
  }

  @Provides
  public OutletReportService provideOutletReportService(WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(OutletReportService.class);
  }

  @Provides
  public DroppingDailyTaskService provideDroppingDailyTaskService(
      WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(DroppingDailyTaskService.class);
  }

  @Provides
  public TugasTransferService provideTugasTransferService(
      WebServiceGenerator webServiceGenerator) {
    return webServiceGenerator.createService(TugasTransferService.class);
  }
}
