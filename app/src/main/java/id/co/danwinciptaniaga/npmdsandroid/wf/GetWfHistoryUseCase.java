package id.co.danwinciptaniaga.npmdsandroid.wf;

import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.wf.WfHistoryResponse;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.DroppingAdditionalService;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyService;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;

public class GetWfHistoryUseCase extends
    BaseObservable<GetWfHistoryUseCase.Listener, WfHistoryResponse> {
  private static final String TAG = GetWfHistoryUseCase.class.getSimpleName();

  public interface Listener {
    void onGetWfHistoryStarted();

    void onGetWfHistorySuccess(Resource<WfHistoryResponse> result);

    void onGetWfHistoryFailure(Resource<WfHistoryResponse> responseError);
  }

  private final ExpenseService expenseService;
  private final DroppingDailyService droppingDailyService;
  private final DroppingAdditionalService droppingAdditionalService;
  private final AppBookingService appBookingService;

  private final AppExecutors appExecutors;

  @Inject
  public GetWfHistoryUseCase(Application application,
      ExpenseService expenseService,
      DroppingDailyService droppingDailyService,
      DroppingAdditionalService droppingAdditionalService,
      AppBookingService appBookingService,
      AppExecutors appExecutors) {
    super(application);
    this.expenseService = expenseService;
    this.droppingDailyService = droppingDailyService;
    this.droppingAdditionalService = droppingAdditionalService;
    this.appBookingService = appBookingService;
    this.appExecutors = appExecutors;
  }

  public ListenableFuture<WfHistoryResponse> loadWfHistory(WfType wfType,
      UUID entityId) {
    notifyStart(null, null);

    ListenableFuture<WfHistoryResponse> lf = null;
    switch (wfType) {
    case APP_EXPENSE:
      lf = loadWfHistoryForExpense(entityId);
      break;
    case DROPPING_HARIAN:
      lf = loadWfHistoryForDroppingHarian(entityId);
      break;
    case DROPPING_TAMBAHAN:
      lf = loadWfHistoryForDroppingTambahan(entityId);
      break;
    case APP_BOOKING:
      lf = loadWfHistoryForBooking(entityId);
      break;
    default:
      throw new IllegalArgumentException("WfHistory for " + wfType + " not supported");
    }

    Futures.addCallback(lf, new FutureCallback<WfHistoryResponse>() {
      @Override
      public void onSuccess(@NullableDecl WfHistoryResponse result) {
        notifySuccess("Riwayat Workflow berhasil dimuat", result);
      }

      @Override
      public void onFailure(Throwable t) {
        notifyFailure("Gagal memuat Riwayat Workflow", null, t);
      }
    }, appExecutors.backgroundIO());
    return lf;
  }

  private ListenableFuture<WfHistoryResponse> loadWfHistoryForDroppingHarian(UUID entityId) {
    return droppingDailyService.getWfHistory(entityId);
  }

  private ListenableFuture<WfHistoryResponse> loadWfHistoryForDroppingTambahan(UUID entityId) {
    return droppingAdditionalService.getWfHistory(entityId);
  }

  private ListenableFuture<WfHistoryResponse> loadWfHistoryForExpense(UUID entityId) {
    return expenseService.getWfHistory(entityId);
  }

  private ListenableFuture<WfHistoryResponse> loadWfHistoryForBooking(UUID entityId) {
    return appBookingService.getWfHistory(entityId);
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<WfHistoryResponse> result) {
    listener.onGetWfHistoryStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<WfHistoryResponse> result) {
    listener.onGetWfHistorySuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<WfHistoryResponse> result) {
    listener.onGetWfHistoryFailure(result);
  }
}
