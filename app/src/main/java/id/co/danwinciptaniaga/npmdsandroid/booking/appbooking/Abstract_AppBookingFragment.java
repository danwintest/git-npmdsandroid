package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.acra.ACRA;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.bumptech.glide.Glide;
import com.google.android.material.chip.Chip;
import com.google.android.material.snackbar.Snackbar;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.ImageUtility;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmds.data.common.LobShortData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.common.SoShortData;
import id.co.danwinciptaniaga.npmds.data.common.UtilityState;
import id.co.danwinciptaniaga.npmds.data.wf.WorkflowConstants;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.BaseActivity;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.cash.AppBookingCashVM;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferVM;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentAppbookingBinding;
import id.co.danwinciptaniaga.npmdsandroid.databinding.WfdecisionBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

@AndroidEntryPoint
//public abstract class Abstract_AppBookingFragment extends ProtectedFragment {
public abstract class Abstract_AppBookingFragment extends Fragment {
  @Inject
  LoginUtil loginUtil;
  private String TAG = Abstract_AppBookingFragment.class.getSimpleName();
  private final int PERMISSION_REQUEST_CODE = 201;
  private FragmentAppbookingBinding binding;
  private Abstract_AppBookingVM mViewModel;

  //set data awal
  private LayoutInflater thisInflater;
  protected boolean isHasCreatePermission = false;
  protected ArrayAdapter<KeyValueModel<OutletShortData, String>> outletsAdapter;
  protected ArrayAdapter<KeyValueModel<SoShortData, String>> soAdapter;
  protected ArrayAdapter<KeyValueModel<LobShortData, String>> lobAdapter;
  protected ArrayAdapter<KeyValueModel<UUID, String>> poprAdapter;

  public Abstract_AppBookingFragment(){
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onResume() {
    super.onResume();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
      requestPermission();
  }

  private void requestPermission() {
    int resultReadExternalStorage = ContextCompat.checkSelfPermission(getContext(),
        Manifest.permission.READ_EXTERNAL_STORAGE);
    int resultWriteExternalStorage = ContextCompat.checkSelfPermission(getContext(),
        Manifest.permission.WRITE_EXTERNAL_STORAGE);

    if (resultReadExternalStorage != PackageManager.PERMISSION_GRANTED
        || resultWriteExternalStorage != PackageManager.PERMISSION_GRANTED) {
      requestPermissions(new String[] { Manifest.permission.READ_EXTERNAL_STORAGE,
          Manifest.permission.WRITE_EXTERNAL_STORAGE }, PERMISSION_REQUEST_CODE);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    if (requestCode == PERMISSION_REQUEST_CODE) {
      if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)
          && permissions[1].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED
            && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
          Snackbar.make(this.binding.getRoot(),
              "Permission granted",
              Snackbar.LENGTH_LONG).show();
        } else {
          Snackbar.make(this.binding.getRoot(),
              "Anda harus mengizinkan aplikasi ini membaca/tulis ke media penyimpanan",
              Snackbar.LENGTH_LONG).show();
          NavController navController = Navigation.findNavController(binding.getRoot());
          navController.popBackStack();
        }
      }
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    setOnCreateView(false, inflater, container, this, getThisTag(), null);
    return getBinding().getRoot();
  }

  protected void setOnCreateView(@NotNull boolean isHasCreatePermission,
      @NonNull @NotNull LayoutInflater lif, @NonNull @NotNull ViewGroup cont,
      @NonNull @NotNull androidx.lifecycle.ViewModelStoreOwner owner, @NonNull @NotNull String tag,
      Abstract_AppBookingVM abstactVM) {
    this.isHasCreatePermission = isHasCreatePermission;
    TAG = tag;
    thisInflater = lif;
    binding = FragmentAppbookingBinding.inflate(lif, cont, false);
    mViewModel = abstactVM;
    getVm().setHasBeenInitializedForm(false);
    setFormState();
    setOtpProcessObserver();
  }


  protected Abstract_AppBookingVM getVm() {
    return mViewModel;
  }

  protected AppBookingCashVM getCashVm() {
    return (AppBookingCashVM) getVm();
  }

  protected AppBookingTransferVM getTransferVm() {
    return (AppBookingTransferVM) getVm();
  }

//  protected void setVisibility_TEMPORARY() {
//    getBinding().tilOutletName.setVisibility(View.VISIBLE);
//    getBinding().tilConsumerNo.setVisibility(View.VISIBLE);
//    getBinding().tilBookingDate.setVisibility(View.VISIBLE);
//    getBinding().tilBookingDate.setVisibility(View.VISIBLE);
//    getBinding().tilLOB.setVisibility(View.VISIBLE);
//    getBinding().tilSO.setVisibility(View.VISIBLE);
//    getBinding().tilIdnpksf.setVisibility(View.VISIBLE);
//    getBinding().tilReason.setVisibility(View.VISIBLE);
//    getBinding().tilCashReason.setVisibility(View.VISIBLE);
//    getBinding().tilAppNo.setVisibility(View.VISIBLE);
//    getBinding().tilPoNo.setVisibility(View.VISIBLE);
//    getBinding().tilPoDate.setVisibility(View.VISIBLE);
//    getBinding().clPoAttach.setVisibility(View.VISIBLE);
    // binding.hsvPendingPoReason.setVisibility(View.VISIBLE);
//    getBinding().tvLabelKonsumen.setVisibility(View.VISIBLE);
//    getBinding().tilConsumerName.setVisibility(View.VISIBLE);
//    getBinding().tilConsumerAddress.setVisibility(View.VISIBLE);
//    getBinding().tilConsumerPhone.setVisibility(View.VISIBLE);
//    getBinding().tilVehicleNo.setVisibility(View.VISIBLE);
//    getBinding().tvLabelGambarLampiran.setVisibility(View.VISIBLE);
//    getBinding().ivAtcStnkKtp.setVisibility(View.VISIBLE);
//    getBinding().tvLabelStnkKtp.setVisibility(View.VISIBLE);
//    getBinding().ivAtcKK.setVisibility(View.VISIBLE);
//    getBinding().tvLabelKK.setVisibility(View.VISIBLE);
//    getBinding().ivAtcKWITASIBPKB.setVisibility(View.VISIBLE);
//    getBinding().tvLabelKWITANSIBPKB.setVisibility(View.VISIBLE);
//    getBinding().ivAtcSptTunai.setVisibility(View.VISIBLE);
//    getBinding().tvLabelSptTunai.setVisibility(View.VISIBLE);
//    getBinding().ivAtcSerahTerima.setVisibility(View.VISIBLE);
//    getBinding().ivAtcFisikMotor.setVisibility(View.VISIBLE);
//    getBinding().tvLabelSerahTerima.setVisibility(View.VISIBLE);
//    getBinding().tvLabelFisikMotor.setVisibility(View.VISIBLE);
//    getBinding().tvLabelPencairan.setVisibility(View.VISIBLE);
//    getBinding().cbOpenClose.setVisibility(View.VISIBLE);
//    getBinding().tilCashTotalPencairan.setVisibility(View.VISIBLE);
//    getBinding().tilCashPencairanFIF.setVisibility(View.VISIBLE);
//    getBinding().tilCashPencairanKonsumen.setVisibility(View.VISIBLE);
//    getBinding().tilCashBiroJasa.setVisibility(View.VISIBLE);
//    getBinding().tvLabelFee.setVisibility(View.VISIBLE);
//    getBinding().tilFeeMatrix.setVisibility(View.VISIBLE);
//    getBinding().tilFeeScheme.setVisibility(View.VISIBLE);
//    getBinding().acPendingPoReason.setVisibility(View.VISIBLE);
//  }

  private void setMode_transferField(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans) {
    if (smCash != null) {
      getBinding().tilCashReason.setVisibility(View.VISIBLE);

      getBinding().cbOpenClose.setVisibility(View.VISIBLE);
      getBinding().tilCashPencairanKonsumen.setVisibility(View.VISIBLE);
      getBinding().tilCashPencairanFIF.setVisibility(View.VISIBLE);
      getBinding().tilCashBiroJasa.setVisibility(View.VISIBLE);
      getBinding().tilCashTotalPencairan.setVisibility(View.VISIBLE);

      getBinding().cbTFOpenClose.setVisibility(View.GONE);
      getBinding().cvTFConsumer.setVisibility(View.GONE);
      getBinding().cvTFBiroJasa.setVisibility(View.GONE);
      getBinding().cvTFFIF.setVisibility(View.GONE);
      return;
    } else if (smTrans != null) {
      getBinding().cbOpenClose.setVisibility(View.GONE);
      getBinding().tilCashReason.setVisibility(View.GONE);
      getBinding().tvLabelSerahTerima.setText("");
      getBinding().ivAtcSerahTerima.setEnabled(false);
      getBinding().ivAtcSerahTerima
          .setBackgroundColor(getContext().getResources().getColor(R.color.transparent_color));
      getBinding().ivAtcSerahTerima.setImageResource(0);

      getBinding().tilCashPencairanKonsumen.setVisibility(View.GONE);
      getBinding().tilCashPencairanFIF.setVisibility(View.GONE);
      getBinding().tilCashBiroJasa.setVisibility(View.GONE);
      getBinding().tilCashTotalPencairan.setVisibility(View.VISIBLE);

      getBinding().cbTFOpenClose.setVisibility(View.VISIBLE);
      getBinding().cvTFConsumer.setVisibility(View.VISIBLE);
      getBinding().cvTFBiroJasa.setVisibility(View.VISIBLE);
      getBinding().cvTFFIF.setVisibility(View.VISIBLE);


      getBinding().etTFConsumer.setEnabled(getVm().isTransferFieldEnabled);
      getBinding().btnTFEditRekeningConsumer.setEnabled(getVm().isTransferFieldEnabled);
      getBinding().etTFBiroJasa.setEnabled(getVm().isTransferFieldEnabled);
      getBinding().btnTFEditRekeningBiroJasa.setEnabled(getVm().isTransferFieldEnabled);
      if(getVm().isTransferFieldEnabled()){
        getVm().isField_isOpenClose().observe(getViewLifecycleOwner(), isOpen -> {
          getBinding().tilTFFIF.setEnabled(isOpen);
          getBinding().btnTFEditRekeningFIF.setEnabled(isOpen);
        });
      }else{
        getBinding().etTFFIF.setEnabled(getVm().isTransferFieldEnabled);
        getBinding().btnTFEditRekeningFIF.setEnabled(getVm().isTransferFieldEnabled);
      }

//      switch (smTrans) {
//      case KASIR_SUBMITTED_NEW_APPBOOKING:
//      case KASIR_SUBMITTED_EDIT_APPBOOKING:
//      case KASIR_REVISION_NEW_APPBOOKING:
//      case KASIR_REVISION_EDIT_APPBOOKING:
//        getBinding().etTFConsumer.setEnabled(true);
//        getBinding().btnTFEditRekeningConsumer.setEnabled(true);
//        getBinding().etTFBiroJasa.setEnabled(true);
//        getBinding().btnTFEditRekeningBiroJasa.setEnabled(true);
//        getVm().isField_isOpenClose().observe(getViewLifecycleOwner(), isOpen -> {
//          getBinding().tilTFFIF.setEnabled(isOpen);
//          getBinding().btnTFEditRekeningFIF.setEnabled(isOpen);
//        });
//        break;
//      default:
//        getBinding().etTFConsumer.setEnabled(false);
//        getBinding().btnTFEditRekeningConsumer.setEnabled(false);
//        getBinding().etTFBiroJasa.setEnabled(false);
//        getBinding().btnTFEditRekeningBiroJasa.setEnabled(false);
//        getBinding().etTFFIF.setEnabled(false);
//        getBinding().btnTFEditRekeningFIF.setEnabled(false);
//        break;
//      }
    } else {
      String msg = "setMode_transferField() screenMode tidak di support";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      RuntimeException re = new RuntimeException(getThisTag() + msg);
      ACRA.getErrorReporter().handleException(re);
    }
  }

  private void setMode_cancelationField(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans) {
    if (Utility.OPERATION_NEW.equals(getVm().getField_AppOperation())) {
      getBinding().tvLabelCancelation.setVisibility(View.GONE);
      getBinding().tilCancelReason.setVisibility(View.GONE);
      getBinding().tvLabelHO.setVisibility(View.GONE);
      getBinding().ivAtcHO.setVisibility(View.GONE);
    } else if (Utility.OPERATION_EDIT.equals(getVm().getField_AppOperation())) {
      getBinding().tvLabelCancelation.setVisibility(View.GONE);
      getBinding().tilCancelReason.setVisibility(View.GONE);
      getBinding().tvLabelHO.setVisibility(View.GONE);
      getBinding().ivAtcHO.setVisibility(View.GONE);
    } else if (Utility.OPERATION_CANCEL.equals(getVm().getField_AppOperation())) {
      getBinding().tvLabelCancelation.setVisibility(View.VISIBLE);
      getBinding().tilCancelReason.setVisibility(View.VISIBLE);
      getBinding().tvLabelHO.setVisibility(View.VISIBLE);
      getBinding().ivAtcHO.setVisibility(View.VISIBLE);
    } else {
      getBinding().tvLabelCancelation.setVisibility(View.VISIBLE);
      getBinding().tilCancelReason.setVisibility(View.VISIBLE);
      getBinding().tvLabelHO.setVisibility(View.VISIBLE);
      getBinding().ivAtcHO.setVisibility(View.VISIBLE);
    }
  }

  protected void initForm() {
    setGeneralMessage();
//    setVisibility_TEMPORARY();
    showHideVehicleValidateProgressBar();
    setFormHeader();
    setFieldTrxNo();
    setField_Outlet();
    setField_ConsumerNo();
    setField_BookingDate();
    setField_Lob();
    setField_So();
    setField_Idnpksf();
    setField_AlasanPinjam();
    setField_AlasanCash();
    setField_AppNo();
    setField_PoNo();
    setField_PoDate();
    setField_Attachment_Po();
    setPopReasonList();
    setField_ConsumerName();
    setField_ConsumerAddress();
    setField_ConsumerPhone();
    setField_VehicleNo();
    setField_Attachment_STNK_KTP();
    setField_Attachment_KK();
    setField_Attachment_KWINTASI();
    setField_Attachment_SPT();
    setField_Attachment_SERAH_TERIMA();
    setField_Attachment_FISIK_MOTOR();
    setField_OpenClose();
    setField_ConsumerAmount();
    setField_FIFAmount();
    setField_BiroJasaAmount();
    setField_PencairanTOTAL();
    setField_FeeMatrix();
    setField_FeeScheme();
    setField_CancelReason();
    setField_Attachment_HO();
    setField_Attachment_BUKTIRETUR();
//    setFormState();
//    setOtpProcessObserver();
  }

  private void setGeneralMessage() {
    getVm().getGeneralErrorMessage().observe(getViewLifecycleOwner(), msg -> {
      if (msg != null) {
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        getVm().setGeneralErrorMessage(null);
      }
    });
  }

  protected void setFormHeader() {
    mViewModel.getHeader_PtName().observe(getViewLifecycleOwner(), data -> {
      binding.tvCompanyName.setText(data);
    });
    mViewModel.getHeader_Status().observe(getViewLifecycleOwner(), data -> {
      binding.tvStatus.setText(data);
    });
    mViewModel.getHeader_OutletName().observe(getViewLifecycleOwner(), data -> {
      binding.tvOutletName.setText(data);
    });
    mViewModel.getHeader_OutletCode().observe(getViewLifecycleOwner(), data -> {
      binding.tvOutletCode.setText(data);
    });
    mViewModel.getHeader_JenisPengajuan().observe(getViewLifecycleOwner(), data -> {
      binding.tvJenisPengajuan.setText(data);
    });
    mViewModel.getHeader_BookingDate().observe(getViewLifecycleOwner(), data -> {
      binding.tvBookingDate.setText(data);
    });
    mViewModel.getHeader_PengajuanDate().observe(getViewLifecycleOwner(), data -> {
      binding.tvPengajuanDate.setText(data);
    });
  }

  protected void setFieldTrxNo(){
    mViewModel.getField_TrxNo().observe(getViewLifecycleOwner(),data->{
      getBinding().tvTrxNo.setText(data);
    });
  }

  protected void setField_Outlet() {
    // Set data Spinner
    outletsAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    getBinding().spOutlet.setAdapter(outletsAdapter);
    getBinding().spOutlet.setFocusable(false);
    getVm().getOutletList_Ld().observe(getViewLifecycleOwner(), oList -> {
      if (oList == null) {
        getBinding().spOutlet.setEnabled(false);
      } else {
        List<KeyValueModel<OutletShortData, String>> kvmList = oList.stream()
            .map(o -> new KeyValueModel<>(o, o.getOutletName())).collect(Collectors.toList());
        outletsAdapter.addAll(kvmList);
        outletsAdapter.notifyDataSetChanged();
        if (kvmList.size() ==1 ) {
          getBinding().spOutlet.setSelection(0);
          getBinding().spOutlet.setEnabled(false);
        }
        // set selected outlet
        mViewModel.getField_outletKvm().observe(getViewLifecycleOwner(), oObj -> {
          if (oObj != null && outletsAdapter.getCount() > 0) {
            int pos = outletsAdapter.getPosition(oObj);
            binding.spOutlet.setSelection(pos);
          }
        });
      }
    });

    // Set Listener
    binding.spOutlet.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
      @Override
      public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
          int i, long l) {
        KeyValueModel<OutletShortData, String> oKvm = outletsAdapter.getItem(i);
        mViewModel.setField_outletKvm(oKvm);
      }

      @Override
      public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
        // mAbstractVM.setField_outletKVM(null);
      }
    });

    // set error
    mViewModel.getField_outletKvm_ERROR().observe(getViewLifecycleOwner(), obj -> {
      binding.tilOutletName.setError(obj);
    });
  }

  protected void setField_ConsumerNo(){
    mViewModel.getField_ConsumerNo().observe(getViewLifecycleOwner(),data->{
      binding.etConsumerNo.setText(data);
    });

  }

  protected void setField_BookingDate() {

    // set data
    mViewModel.getField_BookingDate().observe(getViewLifecycleOwner(), bDate -> {
      if (bDate == null) {
        binding.etBookingDate.setText(null);
      } else if (!bDate.format(Formatter.DTF_dd_MM_yyyy).equals(binding.etBookingDate.getText())) {
        binding.etBookingDate.setText(bDate.format(Formatter.DTF_dd_MM_yyyy));
      }
    });

    // set listener
    binding.etBookingDate.setOnClickListener(v -> {
      LocalDate bookingDate = mViewModel.getField_BookingDate().getValue() != null
          ? mViewModel.getField_BookingDate().getValue()
          : LocalDate.now();
      int day = bookingDate.getDayOfMonth();
      int month = bookingDate.getMonthValue() - 1;
      int year = bookingDate.getYear();
      DatePickerDialog dpdBookingDate = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate bookingDate = LocalDate.of(year, month + 1, dayOfMonth);
              mViewModel.setField_BookingDate(bookingDate);
            }
          }, year, month, day);
      dpdBookingDate.show();
    });
  }

  protected void setField_Lob() {
    // Set Data
    lobAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    getBinding().spLOB.setAdapter(lobAdapter);
    getBinding().spLOB.setFocusable(false);
    mViewModel.getLobList_Ld().observe(getViewLifecycleOwner(), lobList -> {
      List<KeyValueModel<LobShortData, String>> kvmList = lobList.stream()
          .map(lobObj -> new KeyValueModel<>(lobObj, lobObj.getLobName()))
          .collect(Collectors.toList());
      lobAdapter.addAll(kvmList);
      lobAdapter.notifyDataSetChanged();

      // set selected lob
      mViewModel.getField_LobKvm().observe(getViewLifecycleOwner(), lobKvm -> {
        if(lobKvm!=null && lobAdapter.getCount()>0){
          int pos = lobAdapter.getPosition(lobKvm);
          binding.spLOB.setSelection(pos);
        }
//        if(true==false) {
//          KeyValueModel<LobShortData, String> selectedLob = (KeyValueModel<LobShortData, String>) getBinding().spLOB
//              .getSelectedItem();
//          if (selectedLob == null || (selectedLob.getKey().getLobId()
//              != lobKvm.getKey().getLobId())) {
//            int pos = lobAdapter.getPosition(lobKvm);
//            binding.spLOB.setSelection(pos);
//          }
//        }
      });

      // Set Listener
      binding.spLOB.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
            int i, long l) {
          KeyValueModel<LobShortData, String> lobObj = lobAdapter.getItem(i);
          mViewModel.setField_LobKvm(lobObj);
        }

        @Override
        public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
          mViewModel.setField_LobKvm(null);
        }
      });
    });

    // set error
    mViewModel.getField_LobKvm_ERROR().observe(getViewLifecycleOwner(), obj -> {
      binding.tilLOB.setError(obj);
    });
  }

  protected void setField_So() {
    // Set Data
    soAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
    getBinding().spSO.setAdapter(soAdapter);
    getBinding().spSO.setFocusable(false);
    mViewModel.getSoList_Ld().observe(getViewLifecycleOwner(), soList -> {
      List<KeyValueModel<SoShortData, String>> kvmList = soList.stream()
          .map(soObj -> new KeyValueModel<>(soObj, soObj.getSoName())).collect(Collectors.toList());
      soAdapter.addAll(kvmList);
      soAdapter.notifyDataSetChanged();

      // Set Listener
      getBinding().spSO.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(@NotNull MaterialSpinner materialSpinner, @Nullable View view,
            int i, long l) {
          KeyValueModel<SoShortData, String> soObj = soAdapter.getItem(i);
          mViewModel.setField_SoKvm(soObj);
        }

        @Override
        public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
          mViewModel.setField_SoKvm(null);
        }
      });
    });


    mViewModel.getField_SoKvm_ERROR().observe(getViewLifecycleOwner(), obj -> {
      getBinding().tilSO.setError(obj);
    });
  }

  protected void setField_Idnpksf() {
    // set data
    mViewModel.getField_Idnpksf().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etIdnpksf.setText(null);
      } else if (!data.equals(getBinding().etIdnpksf.getText().toString())) {
        getBinding().etIdnpksf.setText(data);
      }
    });
    // set listener
    getBinding().etIdnpksf.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          mViewModel.setField_Idnpksf(s.toString());
        else
          mViewModel.setField_Idnpksf(null);
      }
    });

    // set Error
    mViewModel.getField_Idnpksf_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilIdnpksf.setError(message);
    });
  }

  protected void setField_AlasanPinjam() {
    // set data
    mViewModel.getField_AlasanPinjam().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etReason.setText(null);
      } else if (!data.equals(binding.etReason.getText().toString())) {
        getBinding().etReason.setText(data);
      }
    });
    // set listener
    getBinding().etReason.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          mViewModel.setField_AlasanPinjam(s.toString());
        else
          mViewModel.setField_AlasanPinjam(null);
      }
    });

    // set Error
    mViewModel.getField_AlasanPinjam_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilReason.setError(message);
    });
  }

  protected void setField_AlasanCash() {
    // set data
    mViewModel.getField_AlasanPinjamCash().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etCashReason.setText(null);
      } else if (!data.equals(binding.etCashReason.getText().toString())) {
        getBinding().etCashReason.setText(data);
      }
    });

    // set Listener
    getBinding().etCashReason.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          mViewModel.setField_AlasanPinjamCash(getBinding().etCashReason.getText().toString());
        else
          mViewModel.setField_AlasanPinjamCash(null);
      }
    });

    // set Error message
    mViewModel.getField_AlasanPinjamCash_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilCashReason.setError(message);
    });
  }

  protected void setField_AppNo() {
    // set data
    mViewModel.getField_AppNo().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etAppNo.setText(null);
      } else if (!data.equals(getBinding().etAppNo.getText().toString())) {
        getBinding().etAppNo.setText(data);
      }
    });

    // set listener
    getBinding().etAppNo.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          mViewModel.setField_AppNo(s.toString());
      }
    });
  }

  protected void setField_PoNo() {
    // set data
    mViewModel.getField_PoNo().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etPoNo.setText(null);
      } else if (!data.equals(binding.etPoNo.getText().toString())) {
        getBinding().etPoNo.setText(data);
      }
    });

    // set listener
    getBinding().etPoNo.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          mViewModel.setField_PoNo(s.toString());
        else
          mViewModel.setField_PoNo(null);
      }
    });

    // set ERROR
    mViewModel.getField_PoNo_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilPoNo.setError(message);
    });
  }

  protected void setField_PoDate() {
    // set data
    mViewModel.getField_PoDate().observe(getViewLifecycleOwner(), bDate -> {
      if (bDate == null) {
        getBinding().etPoDate.setText(null);
      } else if (!bDate.format(Formatter.DTF_dd_MM_yyyy)
          .equals(getBinding().etPoDate.getText().toString())) {
        getBinding().etPoDate.setText(bDate.format(Formatter.DTF_dd_MM_yyyy));
      }
    });

    // set listener
    getBinding().etPoDate.setOnClickListener(v -> {
      LocalDate bookingDate = mViewModel.getField_PoDate().getValue() != null
          ? mViewModel.getField_PoDate().getValue()
          : LocalDate.now();
      int day = bookingDate.getDayOfMonth();
      int month = bookingDate.getMonthValue() - 1;
      int year = bookingDate.getYear();
      DatePickerDialog dpdBookingDate = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate poDate = LocalDate.of(year, month + 1, dayOfMonth);
              mViewModel.setField_PoDate(poDate);
            }
          }, year, month, day);
      // TODO PENAMBAHAN FUNGSI SEMENTARA, HARAP DIPERBAIKI MENGGUNAKA CLASS DatePickerUIHelper nanti
      dpdBookingDate.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          mViewModel.setField_PoDate(null);
        }
      });
      dpdBookingDate.show();
    });

    mViewModel.getField_PoDate_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilPoDate.setError(message);
    });
  }

  protected void setField_Attachment_Po() {
    // Set Data
    getVm().getField_Attach_PO_isArchive().observe(getViewLifecycleOwner(), isArchive -> {
      HyperLog.d(TAG, "PO setArchive Image [" + isArchive + "]");
      if (true == isArchive) {
        this.getBinding().ivAtPo.setImageResource(R.drawable.ic_baseline_archive_24);
      }
    });

    getVm().getField_Attach_ReducedFinal_PO().observe(getViewLifecycleOwner(), bitmap -> {
      if (bitmap != null) {
        if (bitmap.getStatus() == Status.SUCCESS) {
          HyperLog.d(TAG, "PO setGlide->" + bitmap.getData() + "<-");
          Glide.with(getContext()).load(bitmap.getData()).centerCrop().into(binding.ivAtPo);
        } else {
          HyperLog.d(TAG, "PO setBrokenImg");
          this.getBinding().ivAtPo.setImageResource(R.drawable.ic_baseline_broken_image_24);
        }
      } else {
        this.getBinding().ivAtPo.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
      }
    });

    // set ERRROR
    getVm().getField_Attach_FINAL_PO_OK().observe(getViewLifecycleOwner(), isOk -> {
      if (isOk) {
        getBinding().ivAtPo
            .setBackgroundColor(getContext().getResources().getColor(R.color.design_default_color_background));
        getBinding().tvPoAttach
            .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
      } else {
        getBinding().tvPoAttach
            .setTextColor(getContext().getResources().getColor(R.color.design_default_color_error));
      }

    });
  }

  protected void setPopReasonList() {
    // set list AutoComplete dan listenernya
    getVm().getPoprList_Ld().observe(getViewLifecycleOwner(), popLdList -> {
      List<KeyValueModel<UUID, String>> popKvmlist = popLdList.stream().map(popLd -> {
        KeyValueModel<UUID, String> popKvm = new KeyValueModel<>(popLd.getPoprId(),
            popLd.getName());
        return popKvm;
      }).collect(Collectors.toList());
      poprAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      poprAdapter.addAll(popKvmlist);
      getBinding().acPendingPoReason.setAdapter(poprAdapter);
      getBinding().acPendingPoReason.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
          KeyValueModel<UUID, String> popr = (KeyValueModel<UUID, String>) parent
              .getItemAtPosition(pos);
          getVm().setField_PoprList(true, popr, null);
          getBinding().acPendingPoReason.setText(null);
        }
      });
    });

    // set Error
    getVm().getField_PoprList_ERROR().observe(getViewLifecycleOwner(), message -> {
      if (message == null)
        getBinding().tvLabelPendingPOReason
            .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
      else
        getBinding().tvLabelPendingPOReason.setTextColor(
            getContext().getResources().getColor(R.color.design_default_color_error));
    });
    // set WARN
    getVm().getField_PoprList_WARN().observe(getViewLifecycleOwner(), message -> {
      if (message != null) {
        getBinding().tvLabelPendingPOReason.setText(message);
        getBinding().tvLabelPendingPOReason
            .setTextColor(getContext().getResources().getColor(R.color.warn_color));
      } else {
        String label = getActivity().getString(R.string.pendingPoReason);
        getBinding().tvLabelPendingPOReason.setText(label);
        getBinding().tvLabelPendingPOReason
            .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
      }
    });
  }

  protected void setField_PopReason(boolean isEnable){
    // set selected poprList
    getVm().getField_PoprList().observe(getViewLifecycleOwner(), poprList -> {
      Log.d(TAG, "setField_PopReason: TERPANGGIL ->" + poprList + "<-");
      getBinding().cgPendingPoReason.removeAllViews();
      int i = 0;
      for(KeyValueModel<UUID,String> popr : poprList){
        i++;
        Chip chip =(Chip) thisInflater.inflate(R.layout.chip_filter,null);
        chip.setId(i);
        chip.setText(popr.getValue());
        chip.setTag(popr.getKey());
        chip.setEnabled(isEnable);
        chip.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            KeyValueModel<UUID,String> removePopr = new KeyValueModel(chip.getTag(),chip.getText());
            mViewModel.setField_PoprList(false,removePopr,null);
          }
        });
        getBinding().cgPendingPoReason.addView(chip);
      }

    });
  }

  protected void setField_ConsumerName() {
    // set data
    mViewModel.getField_ConsumerName().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etConsumerName.setText(null);
      } else if (!data.equals(binding.etConsumerName.getText().toString())) {
        getBinding().etConsumerName.setText(data);
      }
    });

    // set listener
    getBinding().etConsumerName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
          mViewModel.setField_ConsumerName(s.toString());
      }
    });

    // set Error message
    mViewModel.getField_ConsumerName_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilConsumerName.setError(message);
    });
  }

  protected void setField_ConsumerAddress() {
    // set data
    mViewModel.getField_ConsumerAddress().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etConsumerAddress.setText(null);
      } else if (!data.equals(binding.etConsumerAddress.getText().toString())) {
        getBinding().etConsumerAddress.setText(data);
      }
    });

    // set listener
    getBinding().etConsumerAddress.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          mViewModel.setField_ConsumerAddress(s.toString());
      }
    });

    // set Error message
    mViewModel.getField_ConsumerAddress_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilConsumerAddress.setError(message);
    });
  }

  protected void setField_ConsumerPhone() {
    // set data
    mViewModel.getField_ConsumerPhone().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etConsumerPhone.setText(null);
      } else if (!data.equals(binding.etConsumerPhone.getText().toString())) {
        getBinding().etConsumerPhone.setText(data);
      }
    });

    // set Listener
    getBinding().etConsumerPhone.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          mViewModel.setField_ConsumerPhone(s.toString());
        }
      }
    });

    // set error
    mViewModel.getField_ConsumerPhone_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilConsumerPhone.setError(message);
    });
  }

  protected void setField_VehicleNo() {
    // set data
    mViewModel.getField_VehicleNo().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        getBinding().etVehicleNo.setText(null);
      } else if (!data.equals(binding.etVehicleNo.getText().toString())) {
        getBinding().etVehicleNo.setText(data);
      }
    });

    // set listener
    getBinding().etVehicleNo.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          mViewModel.setField_VehicleNo(s.toString());
      }
    });

    getBinding().etVehicleNo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
      @Override
      public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
          String vehicleNo = getBinding().etVehicleNo.getText() != null
              ? getBinding().etVehicleNo.getText().toString()
              : "";
          mViewModel.validate_field_VehicleNo(vehicleNo, null);
        }
      }
    });

    // set error message
    mViewModel.getErrorMessage_VehicleNo().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilVehicleNo.setError(message);
    });
  }

  protected void setField_Attachment_STNK_KTP() {
    // Set Data
    getVm().getField_Attach_STNK_isArchive().observe(getViewLifecycleOwner(), isArchive -> {
      HyperLog.d(TAG, "PO setArchive Image [" + isArchive + "]");
      if (true == isArchive) {
        this.getBinding().ivAtcStnkKtp.setImageResource(R.drawable.ic_baseline_archive_24);
      }
    });

    getVm().getField_Attach_ReducedFinal_STNK().observe(getViewLifecycleOwner(), bitmap -> {
      if (bitmap != null) {
        if (Status.SUCCESS.equals(bitmap.getStatus())) {
          HyperLog.d(TAG, "STNKKTP setGlide->" + bitmap.getData() + "<-");
          Glide.with(getContext()).load(bitmap.getData()).centerCrop().into(binding.ivAtcStnkKtp);
        } else {
          HyperLog.d(TAG, "STNKKTP setBrokenImg");
          this.getBinding().ivAtcStnkKtp.setImageResource(R.drawable.ic_baseline_broken_image_24);
        }
      } else {
        if (true == getVm().getField_Attach_STNK_isArchive().getValue())
          this.getBinding().ivAtcStnkKtp.setImageResource(R.drawable.ic_baseline_archive_24);
        else
          this.getBinding().ivAtcStnkKtp.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
      }
    });

    // Set Listener
    binding.ivAtcStnkKtp.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_STNK.getId());
        processSetImageAttachment();
      }
    });

    binding.ivAtcStnkKtp.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_STNK.getId());
        processDeleteImageAttachment();
        return true;
      }
    });

    // set Error message
    mViewModel.getField_Attach_Final_STNK_OK().observe(getViewLifecycleOwner(), isDone -> {
      if (isDone) {
        getBinding().tvLabelStnkKtp
            .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
      } else {
        getBinding().tvLabelStnkKtp
            .setTextColor(getContext().getResources().getColor(R.color.design_default_color_error));
      }
    });
  }

  protected void setField_Attachment_KK() {
    // Set Data
    getVm().getField_Attach_KK_isArchive().observe(getViewLifecycleOwner(), isArchive -> {
      HyperLog.d(TAG, "PO setArchive Image [" + isArchive + "]");
      if (true == isArchive) {
        this.getBinding().ivAtcKK.setImageResource(R.drawable.ic_baseline_archive_24);
      }
    });

    mViewModel.getField_Attach_ReducedFinal_KK().observe(getViewLifecycleOwner(), bitmap -> {
      if (bitmap != null) {
        if (Status.SUCCESS.equals(bitmap.getStatus())) {
          HyperLog.d(TAG, "DANWIN KK setGlide->" + bitmap.getData() + "<-");
          Glide.with(getContext()).load(bitmap.getData()).centerCrop().into(binding.ivAtcKK);
        } else {
          HyperLog.d(TAG, "DANWIN KK setBrokenImg");
          this.getBinding().ivAtcKK.setImageResource(R.drawable.ic_baseline_broken_image_24);
        }
      } else {
        if (true == getVm().getField_Attach_KK_isArchive().getValue())
          this.getBinding().ivAtcKK.setImageResource(R.drawable.ic_baseline_archive_24);
        else
          this.getBinding().ivAtcKK.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
      }
    });

    // Set Listener
    getBinding().ivAtcKK.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_KK.getId());
        processSetImageAttachment();
      }
    });

    getBinding().ivAtcKK.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_KK.getId());
        processDeleteImageAttachment();
        return true;
      }
    });

    // set Error message
    mViewModel.getField_Attach_Final_KK_OK().observe(getViewLifecycleOwner(), isDone -> {
      if (isDone) {
        getBinding().tvLabelKK
            .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
      } else {
        getBinding().tvLabelKK
            .setTextColor(getContext().getResources().getColor(R.color.design_default_color_error));
      }
    });
  }

  protected void setField_Attachment_KWINTASI() {
    // Set Data
    getVm().getField_Attach_BPKB_isArchive().observe(getViewLifecycleOwner(), isArchive -> {
      HyperLog.d(TAG, "PO setArchive Image [" + isArchive + "]");
      if (true == isArchive) {
        this.getBinding().ivAtcKWITASIBPKB.setImageResource(R.drawable.ic_baseline_archive_24);
      }
    });

    getVm().getField_Attach_ReducedFinal_BPKB().observe(getViewLifecycleOwner(), bitmap -> {
      if (bitmap != null) {
        if (Status.SUCCESS.equals(bitmap.getStatus())) {
          HyperLog.d(TAG, "DANWIN KWINTANSI setGlide->" + bitmap.getData() + "<-");
          Glide.with(getContext()).load(bitmap.getData()).centerCrop()
              .into(binding.ivAtcKWITASIBPKB);
        } else {
          HyperLog.d(TAG, "DANWIN KWINTANSI setBrokenImg");
          getBinding().ivAtcKWITASIBPKB.setImageResource(R.drawable.ic_baseline_broken_image_24);
        }
      } else {
        if (true == getVm().getField_Attach_BPKB_isArchive().getValue())
          this.getBinding().ivAtcKWITASIBPKB.setImageResource(R.drawable.ic_baseline_archive_24);
        else
          getBinding().ivAtcKWITASIBPKB.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
      }
    });

    // Set Listener
    binding.ivAtcKWITASIBPKB.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_BPKB.getId());
        processSetImageAttachment();
      }
    });

    getBinding().ivAtcKWITASIBPKB.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_BPKB.getId());
        processDeleteImageAttachment();
        return true;
      }
    });

    // set Error message
    mViewModel.getField_Attach_Final_BPKB_OK().observe(getViewLifecycleOwner(), isDone -> {
      if (isDone) {
        getBinding().tvLabelKWITANSIBPKB
            .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
      } else {
        getBinding().tvLabelKWITANSIBPKB
            .setTextColor(getContext().getResources().getColor(R.color.design_default_color_error));
      }
    });
  }

  protected void setField_Attachment_SPT() {
    // Set Data
    getVm().getField_Attach_SPT_isArchive().observe(getViewLifecycleOwner(), isArchive -> {
      HyperLog.d(TAG, "PO setArchive Image [" + isArchive + "]");
      if (true == isArchive) {
        this.getBinding().ivAtcSptTunai.setImageResource(R.drawable.ic_baseline_archive_24);
      }
    });

    mViewModel.getField_Attach_ReducedFinal_SPT().observe(getViewLifecycleOwner(), bitmap -> {
      if (bitmap != null) {
        if (Status.SUCCESS.equals(bitmap.getStatus())) {
          HyperLog.d(TAG, "DANWIN SPT setGlide->" + bitmap.getData() + "<-");
          Glide.with(getContext()).load(bitmap.getData()).centerCrop().into(binding.ivAtcSptTunai);
        } else {
          HyperLog.d(TAG, "DANWIN SPT setBrokenImg");
          getBinding().ivAtcSptTunai.setImageResource(R.drawable.ic_baseline_broken_image_24);
        }
      } else {
        if (true == getVm().getField_Attach_SPT_isArchive().getValue())
          this.getBinding().ivAtcSptTunai.setImageResource(R.drawable.ic_baseline_archive_24);
        else
          getBinding().ivAtcSptTunai.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
      }
    });

    // Set Listener
    binding.ivAtcSptTunai.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_SPT.getId());
        processSetImageAttachment();
      }
    });

    getBinding().ivAtcSptTunai.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_SPT.getId());
        processDeleteImageAttachment();
        return true;
      }
    });

    // set Error message
    mViewModel.getField_Attach_Final_SPT_OK().observe(getViewLifecycleOwner(), isDone -> {
      if (isDone) {
        getBinding().tvLabelSptTunai
            .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
      } else {
        getBinding().tvLabelSptTunai
            .setTextColor(getContext().getResources().getColor(R.color.design_default_color_error));
      }
    });
  }

  protected void setField_Attachment_SERAH_TERIMA() {
    // Set Data
    getVm().getField_Attach_SERAH_TERIMA_isArchive().observe(getViewLifecycleOwner(), isArchive -> {
      HyperLog.d(TAG, "PO setArchive Image [" + isArchive + "]");
      if (true == isArchive) {
        this.getBinding().ivAtcSerahTerima.setImageResource(R.drawable.ic_baseline_archive_24);
      }
    });

    mViewModel.getField_Attach_ReducedFinal_SERAH_TERIMA().observe(getViewLifecycleOwner(),
        bitmap -> {
          if (bitmap != null) {
            if (Status.SUCCESS.equals(bitmap.getStatus())) {
              HyperLog.d(TAG, "DANWIN SERAHTERIMA setGlide->" + bitmap.getData() + "<-");
              Glide.with(getContext()).load(bitmap.getData()).centerCrop()
                  .into(binding.ivAtcSerahTerima);
            } else {
              HyperLog.d(TAG, "DANWIN SERAHTERIMA setBrokenImg");
              this.getBinding().ivAtcSerahTerima.setImageResource(
                  R.drawable.ic_baseline_broken_image_24);
            }
          } else {
            if (true == getVm().getField_Attach_SERAH_TERIMA_isArchive().getValue())
              this.getBinding().ivAtcSerahTerima.setImageResource(
                  R.drawable.ic_baseline_archive_24);
            else
              this.getBinding().ivAtcSerahTerima.setImageResource(
                  R.drawable.ic_baseline_add_a_photo_24);
          }
        });

    // Set Listener
    getBinding().ivAtcSerahTerima.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_SERAHTERIMA.getId());
        processSetImageAttachment();
      }
    });

    binding.ivAtcSerahTerima.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_SERAHTERIMA.getId());
        processDeleteImageAttachment();
        return true;
      }
    });

    // set Error message
    mViewModel.getField_Attach_Final_SERAH_TERIMA_OK().observe(getViewLifecycleOwner(), isDone -> {
      if (isDone) {
        getBinding().tvLabelSerahTerima
            .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
      } else {
        getBinding().tvLabelSerahTerima
            .setTextColor(getContext().getResources().getColor(R.color.design_default_color_error));
      }
    });
  }

  protected void setField_Attachment_FISIK_MOTOR() {
    // Set Data
    getVm().getField_Attach_FISIK_MOTOR_isArchive().observe(getViewLifecycleOwner(), isArchive -> {
      HyperLog.d(TAG, "PO setArchive Image [" + isArchive + "]");
      if (true == isArchive) {
        this.getBinding().ivAtcFisikMotor.setImageResource(R.drawable.ic_baseline_archive_24);
      }
    });

    mViewModel.getField_Attach_ReducedFinal_FISIK_MOTOR().observe(getViewLifecycleOwner(),
        bitmap -> {
          if (bitmap != null) {
            if (Status.SUCCESS.equals(bitmap.getStatus())) {
              HyperLog.d(TAG, "DANWIN FISIKI_MOTOR setGlide->" + bitmap.getData() + "<-");
              Glide.with(getContext()).load(bitmap.getData()).centerCrop()
                  .into(binding.ivAtcFisikMotor);
            } else {
              HyperLog.d(TAG, "DANWIN FISIKI_MOTOR setBrokenImg");
              getBinding().ivAtcFisikMotor.setImageResource(R.drawable.ic_baseline_broken_image_24);
            }
          } else {
            if (true == getVm().getField_Attach_FISIK_MOTOR_isArchive().getValue())
              this.getBinding().ivAtcFisikMotor.setImageResource(R.drawable.ic_baseline_archive_24);
            else
              getBinding().ivAtcFisikMotor.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
          }
        });

    // Set Listener
    getBinding().ivAtcFisikMotor.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_FISIKMOTOR.getId());
        processSetImageAttachment();
      }
    });

    getBinding().ivAtcFisikMotor.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_FISIKMOTOR.getId());
        processDeleteImageAttachment();
        return true;
      }
    });
  }

  protected void setField_OpenClose() {
    // set data
    mViewModel.isField_isOpenClose().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
        mViewModel.setField_IsOpenClose(false);
      } else if (!data.equals(getBinding().cbOpenClose.isChecked())) {
        getBinding().cbOpenClose.setChecked(data);
      }
    });

    // set listener
    getBinding().cbOpenClose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mViewModel.setField_IsOpenClose(isChecked);
//        binding.tilCashPencairanFIF.setEnabled(isChecked);
        if (!isChecked)
          mViewModel.setField_FIFAmount(BigDecimal.ZERO);
      }
    });
  }

  protected void setField_ConsumerAmount() {
    // set data
    mViewModel.getField_ConsumerAmount().observe(getViewLifecycleOwner(), amt -> {
      if (amt == null) {
        getBinding().etPencairanKonsumen.setText(null);
      } else {
        if (!getBinding().tilCashPencairanKonsumen.isEnabled()) {
          String formattedAmt = Utility.getFormattedAmt(amt);
          if (!formattedAmt.equals(binding.etPencairanKonsumen.getText().toString())) {
            getBinding().etPencairanKonsumen.setText(formattedAmt);
          }
        } else {
          if (!amt.toString().equals(binding.etPencairanKonsumen.getText().toString())) {
            getBinding().etPencairanKonsumen.setText(amt.toString());
          }
        }
      }
    });

    // set listener
    getBinding().etPencairanKonsumen.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          BigDecimal amt = Utility.getAmtFromFromattedString(s.toString());
          mViewModel.setField_ConsumerAmount(amt);
        } else {
          mViewModel.setField_ConsumerAmount(null);
        }
      }
    });

    // set ERROR Message
    mViewModel.getField_ConsumerAmount_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilCashPencairanKonsumen.setError(message);
    });
  }

  protected void setField_FIFAmount() {
    // set data
    mViewModel.getField_FIFAmount().observe(getViewLifecycleOwner(), amt -> {
      String fn = "getField_FIFAmount().observe ";
      HyperLog.d(TAG, fn + "amt[" + amt + "]");
      HyperLog.d(TAG, fn + " isPencairanFIF Enable[" + (getBinding().etPencairanFIF.isEnabled()) + "]");
      if (amt == null) {
        getBinding().etPencairanFIF.setText(null);
      } else {
        if (!getBinding().tilCashPencairanFIF.isEnabled()) {
          String formattedAmt = Utility.getFormattedAmt(amt);
          if (!formattedAmt.equals(binding.etPencairanFIF.getText().toString())) {
            getBinding().etPencairanFIF.setText(formattedAmt);
          }
        } else {
          if (!amt.toString().equals(binding.etPencairanFIF.getText().toString())) {
            getBinding().etPencairanFIF.setText(amt.toString());
          }
        }
      }
    });

    // set listener
    getBinding().etPencairanFIF.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          BigDecimal amt = Utility.getAmtFromFromattedString(s.toString());
          mViewModel.setField_FIFAmount(amt);
        } else {
          mViewModel.setField_FIFAmount(BigDecimal.ZERO);
        }
      }
    });

    // set ERROR
    mViewModel.getField_FIFAmount_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilCashPencairanFIF.setError(message);
    });
  }

  protected void setField_BiroJasaAmount() {

    // set data
    mViewModel.getField_BiroJasaAmount().observe(getViewLifecycleOwner(), amt -> {
      if (amt == null) {
        getBinding().etBiroJasa.setText(null);
      } else {
       if(!getBinding().tilCashBiroJasa.isEnabled()){
         String formattedAmt = Utility.getFormattedAmt(amt);
         if (!formattedAmt.equals(binding.etBiroJasa.getText().toString())) {
           getBinding().etBiroJasa.setText(formattedAmt);
         }
       }else{
         if (!amt.toString().equals(binding.etBiroJasa.getText().toString())) {
           getBinding().etBiroJasa.setText(amt.toString());
         }
       }
      }
    });

    // set listener
    getBinding().etBiroJasa.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          BigDecimal amt = Utility.getAmtFromFromattedString(s.toString());
          mViewModel.setField_BiroJasaAmount(amt);
        } else {
          mViewModel.setField_BiroJasaAmount(null);
        }
      }
    });

  }

  protected void setField_PencairanTOTAL() {
    // set data
    mViewModel.getField_BookingAmount().observe(getViewLifecycleOwner(), amt -> {
      getBinding().etTotalPencairan.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
    });

    // set error
    mViewModel.getField_BookingAmount_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilCashTotalPencairan.setError(message);
    });
  }

  protected void setField_FeeMatrix() {
    // set data
    mViewModel.getField_FeeMatrix().observe(getViewLifecycleOwner(), amt -> {
      if (amt == null) {
        getBinding().etFeeMatrix.setText(null);
      } else {
        if (!getBinding().etFeeMatrix.isEnabled()) {
          String formattedAmt = Utility.getFormattedAmt(amt);
          if (!formattedAmt.equals(binding.etFeeMatrix.getText().toString())) {
            getBinding().etFeeMatrix.setText(formattedAmt);
          }
        } else {
          if (!amt.toString().equals(binding.etFeeMatrix.getText().toString())) {
            getBinding().etFeeMatrix.setText(amt.toString());
          }
        }
      }
    });

    // set listener
    getBinding().etFeeMatrix.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          BigDecimal amt = Utility.getAmtFromFromattedString(s.toString());
          mViewModel.setField_FeeMatrix(amt);
        } else
          mViewModel.setField_FeeMatrix(null);
      }
    });

    // set ERROR
    mViewModel.getField_FeeMatrix_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilFeeMatrix.setError(message);
    });

    // set EditAble
    mViewModel.getField_FeeMatrix_ISEDITABLE().observe(getViewLifecycleOwner(), isEditAble -> {
      getBinding().tilFeeMatrix.setEnabled(isEditAble);
    });
  }

  protected void setField_FeeScheme() {
    // set data
    mViewModel.getField_FeeScheme().observe(getViewLifecycleOwner(), amt -> {
      HyperLog.d(TAG, "setField_FeeScheme value amt[" + amt + "]");
      if (amt == null) {
        binding.etFeeScheme.setText(null);
      } else {
        if (!binding.etFeeScheme.isEnabled()) {
          String formattedAmt = Utility.getFormattedAmt(amt);
          if (!formattedAmt.equals(binding.etFeeScheme.getText().toString())) {
            binding.etFeeScheme.setText(formattedAmt);
          }
        } else {
          if (!amt.toString().equals(binding.etFeeScheme.getText().toString())) {
            binding.etFeeScheme.setText(amt.toString());
          }
        }
      }
    });
    // set listener
    getBinding().etFeeScheme.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          BigDecimal amt = Utility.getAmtFromFromattedString(s.toString());
          mViewModel.setField_FeeScheme(amt);
        } else
          mViewModel.setField_FeeScheme(BigDecimal.ZERO);
      }
    });

    // set WARN
    mViewModel.getField_FeeScheme_WARN().observe(getViewLifecycleOwner(), message -> {
      if (message != null) {
        String label = getActivity().getString(R.string.pendingPoReason);
        getBinding().etFeeScheme.setHint(message + " " + label);
      } else {
        getBinding().etFeeScheme.setHint(message);
      }
    });

    // set Visibiliy
    mViewModel.getField_FeeScheme_ISVISIBLE().observe(getViewLifecycleOwner(), isVisible -> {
      getBinding().tilFeeScheme.setVisibility(isVisible);
    });

    mViewModel.getField_FeeScheme_ISEDITABLE().observe(getViewLifecycleOwner(), isEdit -> {
      getBinding().tilFeeScheme.setEnabled(isEdit);
    });

  }

  protected void setField_CancelReason(){
    mViewModel.getField_CancelReason().observe(getViewLifecycleOwner(),data->{
      if (data == null) {
        getBinding().etCancelReason.setText(null);
      } else if (!data.equals(binding.etCancelReason.getText().toString())) {
        getBinding().etCancelReason.setText(data);
      }
    });

    // set listener
    getBinding().etCancelReason.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0)
          mViewModel.setField_CancelReason(getBinding().etCancelReason.getText().toString(),false);
        else
          mViewModel.setField_CancelReason(null,false);
      }
    });

    // set Error message
    mViewModel.getField_CancelReason_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilCancelReason.setError(message);
    });
  }

  protected void setField_Attachment_HO() {
    // Set Data
    getVm().getField_Attach_HO_isArchive().observe(getViewLifecycleOwner(), isArchive -> {
      HyperLog.d(TAG, "PO setArchive Image [" + isArchive + "]");
      if (true == isArchive) {
        this.getBinding().ivAtcHO.setImageResource(R.drawable.ic_baseline_archive_24);
      }
    });

    mViewModel.getField_Attach_ReducedFinal_HO().observe(getViewLifecycleOwner(), bitmap -> {
      if (bitmap != null) {
        if (Status.SUCCESS.equals(bitmap.getStatus())) {
          HyperLog.d(TAG, "DANWIN HO setGlide->" + bitmap.getData() + "<-");
          Glide.with(getContext()).load(bitmap.getData()).centerCrop().into(binding.ivAtcHO);
        } else {
          HyperLog.d(TAG, "DANWIN HO setBrokenImg");
          getBinding().ivAtcHO.setImageResource(R.drawable.ic_baseline_broken_image_24);
        }
      } else {
        if (true == getVm().getField_Attach_HO_isArchive().getValue())
          this.getBinding().ivAtcHO.setImageResource(R.drawable.ic_baseline_archive_24);
        else
          getBinding().ivAtcHO.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
      }
    });

    // Set Listener
    getBinding().ivAtcHO.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_HO.getId());
        processSetImageAttachmentHO();
      }
    });

    getBinding().ivAtcHO.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_HO.getId());
        processDeleteImageAttachmentHO();
        return true;
      }
    });

    // set Error message
    mViewModel.getField_Attach_Final_HO_OK().observe(getViewLifecycleOwner(), isDone -> {
      if (isDone) {
        getBinding().tvLabelHO
            .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
      } else {
        getBinding().tvLabelHO
            .setTextColor(getContext().getResources().getColor(R.color.design_default_color_error));
      }
    });
  }

  protected void setField_Attachment_BUKTIRETUR() {
    getVm().getField_Attach_BUKTIRETUR_isArchive().observe(getViewLifecycleOwner(), isArchive -> {
      HyperLog.d(TAG, "PO setArchive Image [" + isArchive + "]");
      if (true == isArchive) {
        this.getBinding().ivAtcBUKTIRETUR.setImageResource(R.drawable.ic_baseline_archive_24);
      }
    });

    getVm().isShowBuktiRetur().observe(getViewLifecycleOwner(),isShow->{
      if(isShow){
        getBinding().tvLabelBUKTIRETUR.setVisibility(View.VISIBLE);
        getBinding().ivAtcBUKTIRETUR.setVisibility(View.VISIBLE);
      }else{
        getBinding().tvLabelBUKTIRETUR.setVisibility(View.GONE);
        getBinding().ivAtcBUKTIRETUR.setVisibility(View.GONE);
      }
    });

    // Set Data
    mViewModel.getField_Attach_ReducedFinal_BUKTIRETUR().observe(getViewLifecycleOwner(), bitmap -> {
      if (bitmap != null) {
        getBinding().tvLabelBUKTIRETUR.setVisibility(View.VISIBLE);
        getBinding().ivAtcBUKTIRETUR.setVisibility(View.VISIBLE);
        if (Status.SUCCESS.equals(bitmap.getStatus())) {
          HyperLog.d(TAG, "DANWIN BUKTITRANSFER setGlide->" + bitmap.getData() + "<-");
          Glide.with(getContext()).load(bitmap.getData()).centerCrop().into(binding.ivAtcBUKTIRETUR);
        } else {
          HyperLog.d(TAG, "DANWIN BUKTITRANSFER setBrokenImg");
          getBinding().ivAtcBUKTIRETUR.setImageResource(R.drawable.ic_baseline_broken_image_24);
        }
      } else {
        if (getVm().getField_Attach_BUKTIRETUR_isArchive().getValue() != null
            && true == getVm().getField_Attach_BUKTIRETUR_isArchive().getValue())
          this.getBinding().ivAtcBUKTIRETUR.setImageResource(R.drawable.ic_baseline_archive_24);
        else
          getBinding().ivAtcBUKTIRETUR.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
      }
    });


    // Set Listener
    getBinding().ivAtcBUKTIRETUR.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getVm().setAttachMode(UtilityState.ATTCH_BUKTIRETUR.getId());
        processSetImageAttachment();
      }
    });

    getBinding().ivAtcBUKTIRETUR.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        // NOTE: JIKA SUDAH ADA BUKTI RETUR, MAKA TIDAK BISA DIHAPUS, UNTUK MENGHAPUSNYA, USER HARUS INPUT DATA LEBIH BESAR
//        getVm().setAttachMode(UtilityState.ATTCH_BUKTIRETUR.getId());
//        processDeleteImageAttachment(getVm().getAttachMode());
        return true;
      }
    });

    // set Error message
    mViewModel.getField_Attach_Final_BUKTIRETUR_OK().observe(getViewLifecycleOwner(), isDone -> {
      if (isDone) {
        getBinding().tvLabelBUKTIRETUR
            .setTextColor(getContext().getResources().getColor(R.color.app_secondaryColor));
      } else {
        getBinding().tvLabelBUKTIRETUR
            .setTextColor(getContext().getResources().getColor(R.color.design_default_color_error));
      }
    });
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode,
      @androidx.annotation.Nullable Intent data) {
    if (resultCode != Activity.RESULT_CANCELED) {
      switch (requestCode) {
      case Utility.REQUEST_TAKE_PHOTO:
        if (resultCode == Activity.RESULT_OK) {
          mViewModel.setAttachment_Filled(true, true);
        } else {
          mViewModel.setAttachment_Filled(false, true);
        }
        break;
      case Utility.REQUEST_PICK_PHOTO:
        if (resultCode == Activity.RESULT_OK && data != null) {
          Uri selectedImage = data.getData();
          String[] columns = { MediaStore.Images.Media.DATA, MediaStore.Images.Media.ORIENTATION };
          if (selectedImage != null) {
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, columns, null,
                null, null);
            if (cursor != null) {
              cursor.moveToFirst();

              int filePathColumnIndex = cursor.getColumnIndex(columns[0]);
              int orientationColumnIndex = cursor.getColumnIndex(columns[1]);
              String picturePath = cursor.getString(filePathColumnIndex);
              int orientation = cursor.getInt(orientationColumnIndex);
              cursor.close();
              mViewModel.setAttach_TempFile(new File(picturePath), getVm().getAttachMode());
              // karena sumbernya dari luar, jangan coba hapus sumber
              mViewModel.setAttachment_Filled(true, false);
            }
          }
        }
        break;
      }
    }
  }

  protected void processDeleteImageAttachmentHO() {
//    Boolean isArchive = null;
    String attchMode = getVm().getAttachMode();

//    if (attchMode.equals(UtilityState.ATTCH_HO.getId()))
//      isArchive = getVm().getField_Attach_HO_isArchive().getValue();
//    else {
//      String error = "mode tidak didukung attchmentMode[" + attchMode + "]";
//      Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
//      HyperLog.exception(TAG, error);
//      RuntimeException re = new RuntimeException(error);
//      ACRA.getErrorReporter().handleException(re);
//      return;
//    }
//
//    HyperLog.d(getThisTag(), attchMode + " processSetImageAttachmentHO isArchive[" + isArchive
//        + "], jika True maka return");
//    if(isArchive){
//      return;
//    }

    AppBookingFormState fs = getVm().getFormState().getValue();
    if (fs == null)
      return;
    AppBookingCashScreenMode screenMode = fs.getScreenModeCash();
    switch (screenMode) {
//    case READ_ONLY:
//    case KASIR_SUBMITTED_EDIT_APPBOOKING:
//    case KASIR_SUBMITTED_NEW_APPBOOKING:
//    case AOC_VERIFICATION_CANCEL_APPBOOKING:
//    case KASIR_REVISION_NEW_APPBOOKING:
//    case KASIR_REVISION_EDIT_APPBOOKING:
//    case DOC_VERIFICATION_NEW_APPBOOKING:
//    case AOC_VERIFICATION_EDIT_APPBOOKING:
//    case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
//    case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
//      // doNoting
//      break;
    case KASIR_SUBMITTED_CANCEL_APPBOOKING:
    case KASIR_REVISION_CANCEL_APPBOOKING:
      mViewModel.setAttachment_Filled(false, true);
      break;
    default:
//      String msg = "processDeleteImageAttachmentHO() NotSupported Permission ->" + screenMode+"<-";
//      Toast.makeText(getContext(),msg, Toast.LENGTH_LONG).show();
//      RuntimeException re = new RuntimeException(msg);
//      ACRA.getErrorReporter().handleException(re);
      break;
    }
  }

  protected void processSetImageAttachmentHO() {
//    Boolean isArchive = null;
    String attchMode = getVm().getAttachMode();

//    if (attchMode.equals(UtilityState.ATTCH_HO.getId()))
//      isArchive = getVm().getField_Attach_HO_isArchive().getValue();
//    else {
//      String error = "mode tidak didukung attchmentMode[" + attchMode + "]";
//      Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
//      HyperLog.exception(TAG, error);
//      RuntimeException re = new RuntimeException(error);
//      ACRA.getErrorReporter().handleException(re);
//      return;
//    }

//    HyperLog.d(getThisTag(), attchMode + " processSetImageAttachmentHO isArchive[" + isArchive
//        + "], jika True maka return");
//    if(isArchive){
//      return;
//    }

    AppBookingFormState fs = getVm().getFormState().getValue();
    if (fs == null)
      return;
    AppBookingCashScreenMode screenMode = fs.getScreenModeCash();
    boolean isViewOnly = true;
    boolean notSupported = false;
    switch (screenMode) {
//    case READ_ONLY:
//    case KASIR_SUBMITTED_EDIT_APPBOOKING:
//    case KASIR_SUBMITTED_NEW_APPBOOKING:
//    case AOC_VERIFICATION_CANCEL_APPBOOKING:
//    case AOC_VERIFICATION_EDIT_APPBOOKING:
//    case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
//    case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
//    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
//    case FINRETURN_VERIFICATION_CANCEL_APPBOOKING:
//    case KASIR_REVISION_NEW_APPBOOKING:
//    case KASIR_REVISION_EDIT_APPBOOKING:
//    case DOC_VERIFICATION_NEW_APPBOOKING:
//    case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
//    case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
//      isViewOnly = true;
//      break;
    case KASIR_SUBMITTED_CANCEL_APPBOOKING:
    case KASIR_REVISION_CANCEL_APPBOOKING:
      isViewOnly = false;
      break;
    default:
//      String msg = "processSetImageAttachmentHO() NotSupported Permission ->" + screenMode+"<-";
//      Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();
//      RuntimeException re = new RuntimeException(msg);
//      ACRA.getErrorReporter().handleException(re);
//      notSupported = true;
      isViewOnly = true;
      break;
    }

    if(notSupported)
      return;

    if (getVm().getAttach_FinalFile(getVm().getAttachMode()) != null) {
      // kalau sudah ada image, maka tampilkan
      Uri attachmentUri = FileProvider.getUriForFile(getContext(),
          getContext().getApplicationContext().getPackageName() + ".provider",
          getVm().getAttach_FinalFile(getVm().getAttachMode()));
      HyperLog.d(TAG, "Action View: " + attachmentUri);
      Intent viewImage = new Intent(Intent.ACTION_VIEW, attachmentUri);
      viewImage.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_GRANT_READ_URI_PERMISSION);
      startActivity(viewImage);
    } else {
      if (isViewOnly)
        return;

      String[] opts = getResources().getStringArray(R.array.attachment_add_actions);
      AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
      builder.setItems(opts, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int item) {
          if (item == 0) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
              File photoFile = null;
              try {
                photoFile = ImageUtility.createTempFile(getContext().getCacheDir(), "temp",
                    attchMode, ImageUtility.EXTENSION_JPG);
              } catch (IOException e) {
                HyperLog.e(getThisTag(), R.string.error_starting_camera + e.getMessage());
                Snackbar.make(getView(), R.string.error_starting_camera, Snackbar.LENGTH_SHORT)
                    .show();
              }
              if (photoFile != null) {
                mViewModel.setAttach_TempFile(photoFile, getVm().getAttachMode());
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                    getContext().getApplicationContext().getPackageName() + ".provider", photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, Utility.REQUEST_TAKE_PHOTO);
              }
            }
          } else if (item == 1) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto,
                id.co.danwinciptaniaga.npmdsandroid.util.Utility.REQUEST_PICK_PHOTO);
          } else {
            dialog.dismiss();
          }
        }
      });
      builder.show();
    }
  }

  protected void processDeleteImageAttachment() {
//    Boolean isArchive = null;
//    String attchMode = getVm().getAttachMode();

//    if (attchMode.equals(UtilityState.ATTCH_STNK.getId()))
//      isArchive = getVm().getField_Attach_STNK_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_KK.getId()))
//      isArchive = getVm().getField_Attach_KK_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_BPKB.getId()))
//      isArchive = getVm().getField_Attach_BPKB_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_SPT.getId()))
//      isArchive = getVm().getField_Attach_SPT_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_SERAHTERIMA.getId()))
//      isArchive = getVm().getField_Attach_SERAH_TERIMA_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_FISIKMOTOR.getId()))
//      isArchive = getVm().getField_Attach_FISIK_MOTOR_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_BUKTIRETUR.getId()))
//      isArchive = getVm().getField_Attach_BUKTIRETUR_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_HO.getId()))
//      isArchive = getVm().getField_Attach_HO_isArchive().getValue();
//    else{
//      String error = "mode tidak didukung attchmentMode[" + attchMode + "]";
//      Toast.makeText(getContext(),error,Toast.LENGTH_LONG).show();
//      HyperLog.exception(TAG, error);
//      RuntimeException re = new RuntimeException(error);
//      ACRA.getErrorReporter().handleException(re);
//      return;
//    }
//
//    HyperLog.d(getThisTag(), attchMode + " processDeleteImageAttachment isArchive[" + isArchive
//        + "], jika True maka return");
//    if(isArchive){
//      return;
//    }

    AppBookingFormState fs = getVm().getFormState().getValue();
    if(fs==null)
      return;
    AppBookingCashScreenMode screenMode = fs.getScreenModeCash();
    switch (screenMode){
//    case READ_ONLY:
//    case KASIR_SUBMITTED_CANCEL_APPBOOKING:
//    case KASIR_REVISION_CANCEL_APPBOOKING:
//    case AOC_VERIFICATION_CANCEL_APPBOOKING:
//    case DOC_VERIFICATION_NEW_APPBOOKING:
//    case AOC_VERIFICATION_EDIT_APPBOOKING:
//      //--
//      break;
    case KASIR_REVISION_NEW_APPBOOKING:
    case KASIR_REVISION_EDIT_APPBOOKING:
    case KASIR_SUBMITTED_NEW_APPBOOKING:
    case KASIR_SUBMITTED_EDIT_APPBOOKING:
      mViewModel.setAttachment_Filled(false, true);
      break;
    default:

//      Snackbar.make(getView(), "NotSupported Permission ->" + screenMode, Snackbar.LENGTH_SHORT)
//          .show();
      break;
    }
  }

  protected void processSetImageAttachment() {
//    Boolean isArchive = null;
    String attchMode = getVm().getAttachMode();
//    if (attchMode.equals(UtilityState.ATTCH_STNK.getId()))
//      isArchive = getVm().getField_Attach_STNK_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_KK.getId()))
//      isArchive = getVm().getField_Attach_KK_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_BPKB.getId()))
//      isArchive = getVm().getField_Attach_BPKB_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_SPT.getId()))
//      isArchive = getVm().getField_Attach_SPT_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_SERAHTERIMA.getId()))
//      isArchive = getVm().getField_Attach_SERAH_TERIMA_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_FISIKMOTOR.getId()))
//      isArchive = getVm().getField_Attach_FISIK_MOTOR_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_BUKTIRETUR.getId()))
//      isArchive = getVm().getField_Attach_BUKTIRETUR_isArchive().getValue();
//    else if (attchMode.equals(UtilityState.ATTCH_HO.getId()))
//      isArchive = getVm().getField_Attach_HO_isArchive().getValue();
//    else {
//      String error = "mode tidak didukung attchmentMode[" + attchMode + "]";
//      Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
//      HyperLog.exception(TAG, error);
//      RuntimeException re = new RuntimeException(error);
//      ACRA.getErrorReporter().handleException(re);
//      return;
//    }

//    HyperLog.d(getThisTag(), attchMode + " processSetImageAttachment isArchive[" + isArchive
//        + "], jika True maka return");
//    if(isArchive){
//        return;
//    }
    AppBookingFormState fs = getVm().getFormState().getValue();
    if (fs == null)
      return;
    AppBookingCashScreenMode smCash = fs.getScreenModeCash();
    AppBookingTransferScreenMode smTransfer = fs.getScreenModeTransfer();
    boolean isViewOnly = true;
    boolean notSupported = false;
    switch (smCash) {
//    case READ_ONLY:
//    case KASIR_SUBMITTED_CANCEL_APPBOOKING:
//    case KASIR_REVISION_CANCEL_APPBOOKING:
//    case AOC_VERIFICATION_CANCEL_APPBOOKING:
//    case AOC_VERIFICATION_EDIT_APPBOOKING:
//    case DOC_VERIFICATION_NEW_APPBOOKING:
//    case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
//    case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
//    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
//    case FINRETURN_VERIFICATION_CANCEL_APPBOOKING:
//    case FINRETURN_VERIFICATION_EDIT_APPBOOKING:
//    case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
//    case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
//      isViewOnly = true;
//      break;
    case KASIR_REVISION_NEW_APPBOOKING:
    case KASIR_REVISION_EDIT_APPBOOKING:
    case KASIR_SUBMITTED_NEW_APPBOOKING:
    case KASIR_SUBMITTED_EDIT_APPBOOKING:
      isViewOnly = false;
      break;
    default:
//      Snackbar.make(getView(), "NotSupported Permission ->" + smCash, Snackbar.LENGTH_SHORT)
//          .show();
//      notSupported = true;
      isViewOnly = true;
      break;
    }
    if (notSupported)
      return;

    if (getVm().getAttach_FinalFile(attchMode) != null) {
      // kalau sudah ada image, maka tampilkan
      Uri attachmentUri = FileProvider.getUriForFile(getContext(),
          getContext().getApplicationContext().getPackageName() + ".provider",
          getVm().getAttach_FinalFile(attchMode));
      HyperLog.d(TAG, "Action View: " + attachmentUri);
      Intent viewImage = new Intent(Intent.ACTION_VIEW, attachmentUri);
      viewImage.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_GRANT_READ_URI_PERMISSION);
      startActivity(viewImage);
    } else {
      if (isViewOnly)
        return;

      String[] opts = getResources().getStringArray(R.array.attachment_add_actions);
      AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
      builder.setItems(opts, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int item) {
          if (item == 0) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
              File photoFile = null;
              try {
                photoFile = ImageUtility.createTempFile(getContext().getCacheDir(), "temp",
                    getVm().getAttachMode(), ImageUtility.EXTENSION_JPG);
              } catch (IOException e) {
                HyperLog.e(getThisTag(), R.string.error_starting_camera + e.getMessage());
                Snackbar.make(getView(), R.string.error_starting_camera, Snackbar.LENGTH_SHORT)
                    .show();
              }
              if (photoFile != null) {
                mViewModel.setAttach_TempFile(photoFile, getVm().getAttachMode());
                Uri photoURI = FileProvider.getUriForFile(getContext(),
                    getContext().getApplicationContext().getPackageName() + ".provider", photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, Utility.REQUEST_TAKE_PHOTO);
              }
            }
          } else if (item == 1) {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto,
                id.co.danwinciptaniaga.npmdsandroid.util.Utility.REQUEST_PICK_PHOTO);
          } else {
            dialog.dismiss();
          }
        }
      });
      builder.show();
    }
  }

  protected String getThisTag() {
    return this.TAG;
  }

  protected void setStandartButtonListener(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer) {
    setOverrideMsg("setStandartButtonListener()");
  }

  private void handleWfAction(String decision, String comment, AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer) {
    boolean decisionWithComment = WorkflowConstants.WF_OUTCOME_RETURN.equals(decision)
        || WorkflowConstants.WF_OUTCOME_REJECT.equals(decision);
    if (decisionWithComment && TextUtils.isEmpty(comment)) {
      Snackbar.make(getView(), "Komentar " + getString(R.string.validation_mandatory),
          Snackbar.LENGTH_SHORT).show();
      return;
    }

    if (smCash != null) {
      handleDocumentWfActionCash(decision, comment, smCash);
    } else if (smTransfer != null) {
      handleDocumentWfActionTransfer(decision, comment, smTransfer);
    } else {
      String msg = "handleWfAction() screenMode tidak di support set standartButton gone";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      RuntimeException re = new RuntimeException(getThisTag() + msg);
      ACRA.getErrorReporter().handleException(re);
    }
  }

  protected void handleDocumentWfActionCash(String decision, String comment,
      AppBookingCashScreenMode smCash) {
    setOverrideMsg("handleWfActionCash() ");
  }

  protected void handleDocumentWfActionTransfer(String decision, String comment,
      AppBookingTransferScreenMode smTrans) {
    setOverrideMsg("handleWfActionTransfer() ");
  }

  protected FragmentAppbookingBinding getBinding(){
    return this.binding;
  }

  protected LayoutInflater getThisInflater(){
    return thisInflater;
  }

  protected void processLoadData(){
    setOverrideMsg("processLoadData()");
  }

  private void setOtpProcessObserver(){
    getVm().getOtpVerifyFailMsg().observe(getViewLifecycleOwner(), msg -> {
      if (msg != null) {
        BaseActivity act = ((BaseActivity) getActivity());
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        AccountManager am = AccountManager.get(getContext());
        Account a = loginUtil.getAccount(App.ACCOUNT_TYPE);
        if (a != null) {
          am.invalidateAuthToken(App.ACCOUNT_TYPE, loginUtil.getSavedToken());
        }

        act.logout();
        App.gotoLauncher(getContext());
        act.finish();
      }
    });
  }

  private void setFormState() {
    getBinding().tilConsumerNo.setVisibility(View.VISIBLE);
    getBinding().etConsumerNo.setText(" ");
    getBinding().tilConsumerNo.setEnabled(false);
    Animation outAnimation = new AlphaAnimation(1f, 0f);
    outAnimation.setDuration(200);
    AlphaAnimation inAnimation = new AlphaAnimation(0f, 1f);
    inAnimation.setDuration(200);

    getVm().getFormState().observe(getViewLifecycleOwner(), formState -> {

      if(formState == null){
        processLoadData();
        return;
      }

      AppBookingFormState.State state = formState.getState();
      AppBookingCashScreenMode smCash = formState.getScreenModeCash();
      AppBookingTransferScreenMode smTrans = formState.getScreenModeTransfer();
      String message = formState.getMessage();
      boolean isFromAction = formState.isFromAction();
      HyperLog.d(TAG, "setFormState state[" + state + "]");
      switch (state) {
      case READY:
        HyperLog.d(TAG, "masuk state ->READY<- ");
        if (!getVm().isHasBeenInitializedForm())
          initForm();
        setReadyFormState(smCash, smTrans, message, inAnimation, outAnimation, isFromAction);
        showHideFormProgressBar(false, message);
        getVm().setHasBeenInitializedForm(true);
        break;
      case ACTION_IN_PROGRESS:
        showHideFormProgressBar(true,  message);
        break;
      case ERROR_PROCESS:
        showHideFormProgressBar(false, message);
        break;
      case ERROR_LOAD_DATA:
        showHideFormProgressBar(false, message);
        binding.pageContent.setVisibility(View.GONE);
        break;
      case LOADING:
        showHideFormProgressBar(true, null);
        break;
      default:
        break;
      }
    });
  }

  private void showHideVehicleValidateProgressBar(){
    getVm().getValidateVehilceProgress().observe(getViewLifecycleOwner(),isShow->{
      showHideFormProgressBar(isShow, null);
    });

  }

  private void showHideFormProgressBar(boolean isShow, String message) {
    Animation outAnim = new AlphaAnimation(1f, 0f);
    outAnim.setDuration(200);
    AlphaAnimation inAnim = new AlphaAnimation(0f, 1f);
    inAnim.setDuration(200);

    if (message != null)
      Toast.makeText(getContext(), message,Toast.LENGTH_LONG).show();
    if (isShow && message != null)
      getBinding().progressWrapper.progressText.setText(message);
    getBinding().progressWrapper.progressView.setAnimation(isShow ? inAnim : outAnim);
    getBinding().progressWrapper.progressView.setVisibility(isShow ? View.VISIBLE : View.GONE);
    getBinding().pageContent.setVisibility(isShow ? View.GONE : View.VISIBLE);
    // TODO niat nya ingin enable dan disable form jika progress wrapper muncul, tapi masih gagal
    getBinding().pageContent.setEnabled(!isShow);
  }

  protected void setReadyFormState(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans, String message,
      Animation inAnim, Animation outAnim, boolean isFromAction) {
    setMode_generalField(smCash, smTrans, message, inAnim, outAnim);
    setMode_transferField(smCash, smTrans);
    setMode_cancelationField(smCash, smTrans);
    setMode_StandartButton(smCash, smTrans);
    setStandartButtonListener(smCash, smTrans);
    setMode_WfHistoryButton(smCash, smTrans);
    setMode_WfButton(smCash, smTrans);
    setField_POMODE(smCash, smTrans);
  }

  protected void setMode_generalField(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer, String message,
      Animation inAnimation, Animation outAnimation) {
    setOverrideMsg("setMode_Field()");
  }

  protected void setMode_StandartButton(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans) {
    String msg = "setMode_StandartButton() ";
    if (smCash != null) {
      msg = msg + " masuk smCash[" + smCash + "] ";
      HyperLog.d(getThisTag(), msg);
      switch (smCash) {
      case READ_ONLY:
      case KASIR_REVISION_NEW_APPBOOKING:
      case KASIR_REVISION_EDIT_APPBOOKING:
      case KASIR_REVISION_CANCEL_APPBOOKING:
      case AOC_VERIFICATION_EDIT_APPBOOKING:
      case AOC_VERIFICATION_CANCEL_APPBOOKING:
      case DOC_VERIFICATION_NEW_APPBOOKING:
      case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
      case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
      case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
      case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
      case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
      case FINRETURN_VERIFICATION_EDIT_APPBOOKING:
      case FINRETURN_VERIFICATION_CANCEL_APPBOOKING:
        HyperLog.d(getThisTag(), msg + " standardButton[Gone]");
        getBinding().btnSimpan.setVisibility(View.GONE);
        getBinding().btnSubmit.setVisibility(View.GONE);
        break;
      case KASIR_SUBMITTED_NEW_APPBOOKING:
      case KASIR_SUBMITTED_EDIT_APPBOOKING:
      case KASIR_SUBMITTED_CANCEL_APPBOOKING:
        HyperLog.d(getThisTag(), msg + " standardButton[Visible]");
        getBinding().btnSimpan.setVisibility(View.VISIBLE);
        getBinding().btnSubmit.setVisibility(View.VISIBLE);
        break;
      default:
        getBinding().btnSimpan.setVisibility(View.GONE);
        getBinding().btnSubmit.setVisibility(View.GONE);
        msg = msg + " screenMode tidak di support set standartButton gone";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        RuntimeException re = new RuntimeException(getThisTag() + msg);
        ACRA.getErrorReporter().handleException(re);
        break;
      }

    } else if (smTrans != null) {
      msg = msg+ "masuk smTrans[" + smTrans + "] ";
      switch (smTrans) {
      case READ_ONLY:
      case KASIR_REVISION_NEW_APPBOOKING:
      case KASIR_REVISION_EDIT_APPBOOKING:
      case KASIR_REVISION_CANCEL_APPBOOKING:
      case KASIR_CUS_REVISION_NEW:
      case KASIR_CUS_REVISION_EDIT:
      case KASIR_JASA_REVISION_NEW:
      case KASIR_JASA_REVISION_EDIT:
      case KASIR_FIF_REVISION_NEW:
      case KASIR_FIF_REVISION_EDIT:
      case AOC_VERIFICATION_NEW_APPBOOKING:
      case AOC_VERIFICATION_EDIT_APPBOOKING:
      case AOC_VERIFICATION_CANCEL_APPBOOKING:
      case AOC_CUS_VERIFICATION_NEW:
      case AOC_CUS_VERIFICATION_EDIT:
      case AOC_JASA_VERIFICATION_NEW:
      case AOC_JASA_VERIFICATION_EDIT:
      case AOC_FIF_VERIFICATION_NEW:
      case AOC_FIF_VERIFICATION_EDIT:
      case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
      case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
      case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
      case CAPTAIN_CUS_VERIFICATION_NEW:
      case CAPTAIN_CUS_VERIFICATION_EDIT:
      case CAPTAIN_JASA_VERIFICATION_NEW:
      case CAPTAIN_JASA_VERIFICATION_EDIT:
      case CAPTAIN_FIF_VERIFICATION_NEW:
      case CAPTAIN_FIF_VERIFICATION_EDIT:
      case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
      case DIRECTOR_CUS_VERIFICATION_EDIT:
      case DIRECTOR_JASA_VERIFICATION_EDIT:
      case DIRECTOR_FIF_VERIFICATION_EDIT:
      case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
      case FIN_RETUR_VERIFICATION_CANCEL_APPBOOKING:
      case FIN_STAFF_CUS_VERIFICATION_NEW:
      case FIN_STAFF_CUS_VERIFICATION_EDIT:
      case FIN_STAFF_JASA_VERIFICATION_NEW:
      case FIN_STAFF_JASA_VERIFICATION_EDIT:
      case FIN_STAFF_FIF_VERIFICATION_NEW:
      case FIN_STAFF_FIF_VERIFICATION_EDIT:
      case FIN_SPV_CUS_VERIFICATION_NEW:
      case FIN_SPV_CUS_VERIFICATION_EDIT:
      case FIN_SPV_JASA_VERIFICATION_NEW:
      case FIN_SPV_JASA_VERIFICATION_EDIT:
      case FIN_SPV_FIF_VERIFICATION_NEW:
      case FIN_SPV_FIF_VERIFICATION_EDIT:
      case FIN_RETUR_CUS_VERIFICATION_EDIT:
      case FIN_RETUR_JASA_VERIFICATION_EDIT:
      case FIN_RETUR_FIF_VERIFICATION_EDIT:
      case TRANSACTIONS_MODE:
        HyperLog.d(getThisTag(), msg + " standardButton[Gone]");
        getBinding().btnSimpan.setVisibility(View.GONE);
        getBinding().btnSubmit.setVisibility(View.GONE);
        break;
      case KASIR_SUBMITTED_NEW_APPBOOKING:
      case KASIR_SUBMITTED_EDIT_APPBOOKING:
      case KASIR_SUBMITTED_CANCEL_APPBOOKING:
        HyperLog.d(getThisTag(), msg + " standardButton[Visible]");
        getBinding().btnSimpan.setVisibility(View.VISIBLE);
        getBinding().btnSubmit.setVisibility(View.VISIBLE);
        break;
      default:
        getBinding().btnSimpan.setVisibility(View.GONE);
        getBinding().btnSubmit.setVisibility(View.GONE);
        msg = msg + " screenMode tidak di support set standartButton gone";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        RuntimeException re = new RuntimeException(getThisTag() + msg);
        HyperLog.exception(getThisTag(), re);
        ACRA.getErrorReporter().handleException(re);
        break;
      }
    } else {
      msg = msg + "screenMode tidak ada";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      RuntimeException re = new RuntimeException(getThisTag() + msg);
      HyperLog.exception(getThisTag(), re);
      ACRA.getErrorReporter().handleException(re);
    }
  }

  protected void setMode_WfHistoryButton(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans) {
    String msg = "setMode_WfHistoryButton() ";
    if (smCash != null) {
      msg = msg+ "masuk smCash[" + smCash + "] ";
      HyperLog.d(getThisTag(), msg);
      switch (smCash) {
      case READ_ONLY:
      case KASIR_REVISION_NEW_APPBOOKING:
      case KASIR_REVISION_EDIT_APPBOOKING:
      case KASIR_REVISION_CANCEL_APPBOOKING:
      case AOC_VERIFICATION_EDIT_APPBOOKING:
      case AOC_VERIFICATION_CANCEL_APPBOOKING:
      case DOC_VERIFICATION_NEW_APPBOOKING:
      case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
      case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
      case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
      case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
      case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
      case FINRETURN_VERIFICATION_EDIT_APPBOOKING:
      case FINRETURN_VERIFICATION_CANCEL_APPBOOKING:
        getBinding().btnWfHistory.setVisibility(View.VISIBLE);
        break;
      case KASIR_SUBMITTED_NEW_APPBOOKING:
      case KASIR_SUBMITTED_EDIT_APPBOOKING:
      case KASIR_SUBMITTED_CANCEL_APPBOOKING:
        getBinding().btnWfHistory.setVisibility(View.GONE);
        break;
      default:
        getBinding().btnWfHistory.setVisibility(View.GONE);
        msg = msg + " screenMode tidak di support set wfHisotry[gone]";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        RuntimeException re = new RuntimeException(getThisTag() + msg);
        ACRA.getErrorReporter().handleException(re);
        break;
      }
    } else if (smTrans != null) {

      msg = msg+ "masuk smTrans[" + smTrans + "] ";
      switch (smTrans){
      case READ_ONLY:
      case KASIR_REVISION_NEW_APPBOOKING:
      case KASIR_REVISION_EDIT_APPBOOKING:
      case KASIR_REVISION_CANCEL_APPBOOKING:
      case AOC_VERIFICATION_NEW_APPBOOKING:
      case AOC_VERIFICATION_EDIT_APPBOOKING:
      case AOC_VERIFICATION_CANCEL_APPBOOKING:
      case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
      case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
      case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
      case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
      case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
      case FIN_RETUR_VERIFICATION_CANCEL_APPBOOKING:
      case TRANSACTIONS_MODE:
        HyperLog.d(getThisTag(), msg + " setMode_WfHistoryButton[Gone]");
        getBinding().btnWfHistory.setVisibility(View.VISIBLE);
        break;
      case KASIR_SUBMITTED_NEW_APPBOOKING:
      case KASIR_SUBMITTED_EDIT_APPBOOKING:
      case KASIR_SUBMITTED_CANCEL_APPBOOKING:
        HyperLog.d(getThisTag(), msg + " setMode_WfHistoryButton[Visible]");
        getBinding().btnWfHistory.setVisibility(View.GONE);
        break;
      default:
        getBinding().btnWfHistory.setVisibility(View.VISIBLE);
        msg = msg + " screenMode tidak di support set wfHistoryButton gone";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        RuntimeException re = new RuntimeException(getThisTag() + msg);
        HyperLog.exception(getThisTag(), re);
        ACRA.getErrorReporter().handleException(re);
        break;
      }
    } else {
      msg = " setMode_WfHistoryButton() screenMode tidak ada";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      RuntimeException re = new RuntimeException(getThisTag() + msg);
      HyperLog.exception(getThisTag(), re);
      ACRA.getErrorReporter().handleException(re);
    }
  }

  protected void setMode_WfButton(AppBookingCashScreenMode smCash, AppBookingTransferScreenMode smTransfer) {
    boolean doObserve = false;
    String msg = " setMode_WfButton() ";
    if (smCash != null) {
      msg = msg + " smCash[" + smCash + "] ";
      switch (smCash) {
      case READ_ONLY:
      case KASIR_SUBMITTED_NEW_APPBOOKING:
      case KASIR_SUBMITTED_EDIT_APPBOOKING:
      case KASIR_SUBMITTED_CANCEL_APPBOOKING:
        HyperLog.d(getThisTag(), msg + " doObserve[true]");
        doObserve = false;
        break;
      case KASIR_REVISION_NEW_APPBOOKING:
      case KASIR_REVISION_EDIT_APPBOOKING:
      case KASIR_REVISION_CANCEL_APPBOOKING:
      case AOC_VERIFICATION_EDIT_APPBOOKING:
      case AOC_VERIFICATION_CANCEL_APPBOOKING:
      case DOC_VERIFICATION_NEW_APPBOOKING:
      case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
      case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
      case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
      case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
      case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
      case FINRETURN_VERIFICATION_EDIT_APPBOOKING:
      case FINRETURN_VERIFICATION_CANCEL_APPBOOKING:
        HyperLog.d(getThisTag(), msg + " doObserve[false]");
        doObserve = true;
        break;
      default:
        msg = msg + " screenMode tidak didukung ";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        RuntimeException re = new RuntimeException(getThisTag() + msg);
        HyperLog.exception(getThisTag(), re);
        ACRA.getErrorReporter().handleException(re);
        break;
      }
    } else if (smTransfer != null) {
      msg = msg + " smTransfer[" + smTransfer + "] ";
      switch (smTransfer) {
      case READ_ONLY:
      case KASIR_SUBMITTED_NEW_APPBOOKING:
      case KASIR_SUBMITTED_EDIT_APPBOOKING:
      case KASIR_SUBMITTED_CANCEL_APPBOOKING:
      case KASIR_CUS_REVISION_NEW:
      case KASIR_CUS_REVISION_EDIT:
      case KASIR_JASA_REVISION_NEW:
      case KASIR_JASA_REVISION_EDIT:
      case KASIR_FIF_REVISION_NEW:
      case KASIR_FIF_REVISION_EDIT:
      case AOC_CUS_VERIFICATION_NEW:
      case AOC_CUS_VERIFICATION_EDIT:
      case AOC_JASA_VERIFICATION_NEW:
      case AOC_JASA_VERIFICATION_EDIT:
      case AOC_FIF_VERIFICATION_NEW:
      case AOC_FIF_VERIFICATION_EDIT:
      case CAPTAIN_CUS_VERIFICATION_NEW:
      case CAPTAIN_CUS_VERIFICATION_EDIT:
      case CAPTAIN_JASA_VERIFICATION_NEW:
      case CAPTAIN_JASA_VERIFICATION_EDIT:
      case CAPTAIN_FIF_VERIFICATION_NEW:
      case CAPTAIN_FIF_VERIFICATION_EDIT:
      case DIRECTOR_CUS_VERIFICATION_EDIT:
      case DIRECTOR_JASA_VERIFICATION_EDIT:
      case DIRECTOR_FIF_VERIFICATION_EDIT:
      case FIN_RETUR_CUS_VERIFICATION_EDIT:
      case FIN_RETUR_JASA_VERIFICATION_EDIT:
      case FIN_RETUR_FIF_VERIFICATION_EDIT:
      case FIN_STAFF_CUS_VERIFICATION_NEW:
      case FIN_STAFF_JASA_VERIFICATION_NEW:
      case FIN_STAFF_FIF_VERIFICATION_NEW:
      case FIN_STAFF_CUS_VERIFICATION_EDIT:
      case FIN_STAFF_FIF_VERIFICATION_EDIT:
      case FIN_STAFF_JASA_VERIFICATION_EDIT:
      case FIN_SPV_CUS_VERIFICATION_NEW:
      case FIN_SPV_CUS_VERIFICATION_EDIT:
      case FIN_SPV_FIF_VERIFICATION_NEW:
      case FIN_SPV_FIF_VERIFICATION_EDIT:
      case FIN_SPV_JASA_VERIFICATION_NEW:
      case FIN_SPV_JASA_VERIFICATION_EDIT:
      case TRANSACTIONS_MODE:
        doObserve = false;
        HyperLog.d(getThisTag(), msg + " doObserve[" + doObserve + "]");
        break;
      case KASIR_REVISION_NEW_APPBOOKING:
      case KASIR_REVISION_EDIT_APPBOOKING:
      case KASIR_REVISION_CANCEL_APPBOOKING:
      case AOC_VERIFICATION_NEW_APPBOOKING:
      case AOC_VERIFICATION_EDIT_APPBOOKING:
      case AOC_VERIFICATION_CANCEL_APPBOOKING:
      case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
      case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
      case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
      case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
      case DIRECTOR_VERIFICATION_CANCEL_APPBOOKING:
      case FIN_RETUR_VERIFICATION_CANCEL_APPBOOKING:
        doObserve = true;
        HyperLog.d(getThisTag(), msg + " doObserve[" + doObserve + "]");
        break;
      default:
        msg = msg + "screenMode tidak didukung ";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        RuntimeException re = new RuntimeException(getThisTag() + msg);
        HyperLog.exception(getThisTag(), re);
        ACRA.getErrorReporter().handleException(re);
        break;
      }
    } else {
      msg = msg + "screenMode tidak ada ";
      Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
      RuntimeException re = new RuntimeException(getThisTag() + msg);
      HyperLog.exception(getThisTag(), re);
      ACRA.getErrorReporter().handleException(re);
    }

    if (!doObserve)
      return;

    mViewModel.getWfTaskList().observe(getViewLifecycleOwner(),wfTaskList->{
      getBinding().svWorkflowAction.setVisibility(View.VISIBLE);
      getBinding().llWorkflowButtonContainer.removeAllViews();
      LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
          LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
      int margin = (int) getActivity().getResources().getDimension(R.dimen.activity_vertical_margin);
      params.setMargins(0, 0, margin, 0);
      for (String wfTask : wfTaskList) {
        Button btn = (Button) getLayoutInflater().inflate(R.layout.outlined_button, null);
        btn.setText(wfTask);
        btn.setTag(wfTask);
        btn.setEnabled(true);
        btn.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            String decision = (String) v.getTag();
            WfdecisionBinding wfdBinding = WfdecisionBinding.inflate(getLayoutInflater());
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(decision).setView(wfdBinding.getRoot())
                .setNegativeButton(getString(R.string.workflow_cancel), new DialogInterface.OnClickListener() {
                  @Override
                  public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                  }
                });


            boolean decisionWithComment = WorkflowConstants.WF_OUTCOME_RETURN.equals(decision)
                || WorkflowConstants.WF_OUTCOME_REJECT.equals(decision);
            if(decisionWithComment){
              builder.setPositiveButton(getString(R.string.workflow_yes), null);
              AlertDialog dialog = builder.show();

              dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
                  new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                      String comment = id.co.danwinciptaniaga.androcon.utility.Utility
                          .getTrimmedString(wfdBinding.tietComment.getText());
                      if(TextUtils.isEmpty(comment)){
                        wfdBinding.tilComment.setError(getText(R.string.validation_mandatory));
                      }else{
                        wfdBinding.tilComment.setError(null);
                        handleWfAction(decision, comment, smCash, smTransfer);
                        dialog.dismiss();
                      }
                    }
                  });
            }else{
              builder.setPositiveButton(getString(R.string.workflow_yes),
                  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      String comment = id.co.danwinciptaniaga.androcon.utility.Utility
                          .getTrimmedString(wfdBinding.tietComment.getText());
                      handleWfAction(decision, comment, smCash, smTransfer);
                    }
                  });
              builder.show();
            }
          }
        });
        btn.setLayoutParams(params);
        getBinding().llWorkflowButtonContainer.addView(btn);
      }
    });
  }

  protected void setMode_FieldCash_READONLY(){
    int totalChild = 0;
    getBinding().spOutlet.setEnabled(false);
    getBinding().tilBookingDate.setEnabled(false);
    getBinding().spLOB.setEnabled(false);
    getBinding().spSO.setEnabled(false);
    setField_IdnpksfEnabledDisabled(false);
    getBinding().tilReason.setEnabled(false);
    getBinding().tilCashReason.setEnabled(false);

    getBinding().cgPendingPoReason.setEnabled(false);
    getBinding().acPendingPoReason.setEnabled(false);
    totalChild = getBinding().cgPendingPoReason.getChildCount();
    for(int i=0; i<totalChild; i++)
      getBinding().cgPendingPoReason.getChildAt(i).setEnabled(false);

    getBinding().cgPendingPoReason.setClickable(false);
    getBinding().tilConsumerName.setEnabled(false);
    getBinding().tilConsumerAddress.setEnabled(false);
    getBinding().tilConsumerPhone.setEnabled(false);
    getBinding().tilVehicleNo.setEnabled(false);
    getBinding().cbOpenClose.setEnabled(false);
    getBinding().tilCashPencairanFIF.setEnabled(false);
    getBinding().tilCashPencairanKonsumen.setEnabled(false);
    getBinding().tilCashBiroJasa.setEnabled(false);
    getBinding().tvLabelCancelation.setEnabled(false);
    getBinding().tilCancelReason.setEnabled(false);
    getBinding().tilFeeMatrix.setEnabled(false);
    getBinding().tilFeeScheme.setEnabled(false);
  }


  protected void setTransferOperationalField(AppBookingTransferScreenMode screenMode){

    getTransferVm().getField_OperationalTransfer().observe(getViewLifecycleOwner(), isOp -> {
      getBinding().cbOperasionalTransfer.setChecked(isOp);
    });

    getBinding().cbOperasionalTransfer.setOnCheckedChangeListener(
        new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            getTransferVm().setField_OperationalTransfer(isChecked);
          }
        });


    switch (screenMode){
    case READ_ONLY:
    case TRANSACTIONS_MODE:
    case DIRECTOR_VERIFICATION_EDIT_APPBOOKING:
      getBinding().cbOperasionalTransfer.setEnabled(false);
      break;
    case AOC_VERIFICATION_NEW_APPBOOKING:
    case AOC_VERIFICATION_EDIT_APPBOOKING:
    case AOC_VERIFICATION_CANCEL_APPBOOKING:
    case CAPTAIN_VERIFICATION_NEW_APPBOOKING:
    case CAPTAIN_VERIFICATION_EDIT_APPBOOKING:
    case CAPTAIN_VERIFICATION_CANCEL_APPBOOKING:
      getBinding().cbOperasionalTransfer.setEnabled(true);
      //    case AOC_CUS_VERIFICATION_NEW:
      //    case AOC_CUS_VERIFICATION_EDIT:
      //    case AOC_FIF_VERIFICATION_NEW:
      //    case AOC_FIF_VERIFICATION_EDIT:
      //    case AOC_JASA_VERIFICATION_NEW:
      //    case AOC_JASA_VERIFICATION_EDIT:
      break;
    default :
      getBinding().cvOperasionalTransfer.setVisibility(View.GONE);
    }
  }

  protected void setMode_FieldTrans_READONLY(){
    getBinding().spOutlet.setEnabled(false);
    getBinding().tilBookingDate.setEnabled(false);
    getBinding().spLOB.setEnabled(false);
    getBinding().spSO.setEnabled(false);
    setField_IdnpksfEnabledDisabled(false);
    getBinding().tilReason.setEnabled(false);
    getBinding().tilCashReason.setEnabled(false);

    getBinding().tilConsumerName.setEnabled(false);
    getBinding().tilConsumerAddress.setEnabled(false);
    getBinding().tilConsumerPhone.setEnabled(false);
    getBinding().tilVehicleNo.setEnabled(false);

    getBinding().cbTFOpenClose.setEnabled(false);
    getBinding().tilTFConsumer.setEnabled(false);
    getBinding().btnTFClearCons.setEnabled(false);
    getBinding().btnTFEditRekeningConsumer.setEnabled(false);
    getBinding().tilTFBiroJasa.setEnabled(false);
    getBinding().btnTFClearBiroJasa.setEnabled(false);
    getBinding().btnTFEditRekeningBiroJasa.setEnabled(false);
    getBinding().tilTFFIF.setEnabled(false);
    getBinding().btnTFClearFIF.setEnabled(false);
    getBinding().btnTFEditRekeningFIF.setEnabled(false);

    getBinding().tilIdnpksf.setEnabled(false);
    getBinding().tvLabelCancelation.setEnabled(false);
    getBinding().tilCancelReason.setEnabled(false);
    getBinding().tilFeeMatrix.setEnabled(false);
    getBinding().tilFeeScheme.setEnabled(false);

    getBinding().btnTFTakeActionCons.setVisibility(View.GONE);
    getBinding().btnTFClearCons.setVisibility(View.GONE);
    getBinding().btnTFEditRekeningConsumer.setVisibility(View.GONE);

    getBinding().btnTFTakeActionBJ.setVisibility(View.GONE);
    getBinding().btnTFClearBiroJasa.setVisibility(View.GONE);
    getBinding().btnTFEditRekeningBiroJasa.setVisibility(View.GONE);

    getBinding().btnTFTakeActionFIF.setVisibility(View.GONE);
    getBinding().btnTFClearFIF.setVisibility(View.GONE);
    getBinding().btnTFEditRekeningFIF.setVisibility(View.GONE);
  }

  //TODO fungsi ini masih berantakan, silahkan perbaiki nanti
  protected void setField_POMODE(AppBookingCashScreenMode smCash, AppBookingTransferScreenMode smTransfer) {
    // jika tidak punya create permission, semua menjadi readOnly
//    boolean test = getVm().getField_AppOperation().equals(AppBookingBrowseVM.OPERATION_CANCEL);
//    if (!isHasCreatePermission || getVm().getField_AppOperation().equals(AppBookingBrowseVM.OPERATION_CANCEL)) {
    if (!isHasCreatePermission) {
//      getBinding().acPendingPoReason.setEnabled(false);
      setField_PopReason(false);
      getBinding().btnSimpan.setVisibility(View.GONE);
      setField_POMODE2(false);
      return;
    }
    if(getVm().getField_AppOperation().equals(Utility.OPERATION_CANCEL)){
      setField_PopReason(false);
      setField_POMODE2(false);
      if (smCash != null) {
        if (smCash.equals(AppBookingCashScreenMode.KASIR_REVISION_NEW_APPBOOKING)
            || smCash.equals(AppBookingCashScreenMode.KASIR_REVISION_EDIT_APPBOOKING)
            || smCash.equals(AppBookingCashScreenMode.KASIR_REVISION_CANCEL_APPBOOKING))
          getBinding().btnSimpan.setVisibility(View.GONE);
        else
          getBinding().btnSimpan.setVisibility(View.VISIBLE);
      } else if (smTransfer != null) {
        if (smTransfer.equals(AppBookingTransferScreenMode.KASIR_REVISION_NEW_APPBOOKING)
            || smTransfer.equals(AppBookingTransferScreenMode.KASIR_REVISION_EDIT_APPBOOKING)
            || smTransfer.equals(AppBookingTransferScreenMode.KASIR_REVISION_CANCEL_APPBOOKING))
          getBinding().btnSimpan.setVisibility(View.GONE);
        else
          getBinding().btnSimpan.setVisibility(View.VISIBLE);
      }
      return;
    }

    getVm().getFieldWfStatus().observe(getViewLifecycleOwner(), status -> {
      switch (status) {
      case Utility.WF_STATUS_APPROVED:
      case Utility.WF_STATUS_REJECTED:
        setField_POMODE2(false);
        setField_PopReason(false);
        getBinding().btnSimpan.setVisibility(View.GONE);
        break;
      default:
        // jika AppNo && PoNo && PoDate && PoAttchment ada isinya, maka readOnly
        getVm().isReadOnly_AppNoPoNoPoDatePoAttchment().observe(getViewLifecycleOwner(),
            isReadOnly -> {
              setField_PopReason(isReadOnly);
              setField_POMODE2(!isReadOnly);
              getBinding().acPendingPoReason.setEnabled(true);
              setField_PopReason(true);
            });
        break;
      }
    });

    if (smCash != null) {
      if (smCash.equals(AppBookingCashScreenMode.KASIR_SUBMITTED_NEW_APPBOOKING)
          || smCash.equals(AppBookingCashScreenMode.KASIR_SUBMITTED_EDIT_APPBOOKING)
          || smCash.equals(AppBookingCashScreenMode.READ_ONLY)) {
        setField_POMODE2(true);
        setField_PopReason(true);
        getBinding().btnSimpan.setVisibility(View.VISIBLE);
        return;
      } else {
        setField_POMODE2(false);
        setField_PopReason(false);
        return;
      }
    } else if (smTransfer != null) {
      if (smTransfer.equals(AppBookingTransferScreenMode.KASIR_SUBMITTED_NEW_APPBOOKING)
          || smTransfer.equals(AppBookingTransferScreenMode.KASIR_SUBMITTED_EDIT_APPBOOKING)
          || smTransfer.equals(AppBookingTransferScreenMode.READ_ONLY)) {
        setField_POMODE2(true);
        setField_PopReason(true);
        getBinding().btnSimpan.setVisibility(View.VISIBLE);
        return;
      } else {
        setField_POMODE2(false);
        setField_PopReason(false);
        return;
      }
    }

  }

  protected void setField_POMODE2(boolean isEnable) {
    getBinding().acPendingPoReason.setEnabled(isEnable);
    getBinding().tilAppNo.setEnabled(isEnable);
    getBinding().tilPoNo.setEnabled(isEnable);
    getBinding().tilPoDate.setEnabled(isEnable);

    // Set Listener
    getBinding().ivAtPo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
//        Boolean isArchive = getVm().getField_Attach_PO_isArchive().getValue();
//        HyperLog.d(getThisTag(),
//            "attachPo diClick isArchive[" + isArchive + "], jika archive maka return");
//        if (getVm().getField_Attach_PO_isArchive().getValue().booleanValue())
//          return;

        getVm().setAttachMode(UtilityState.ATTCH_PO.getId());

        if (getVm().getAttach_FinalFile(getVm().getAttachMode()) != null) {
          // kalau sudah ada image, maka tampilkan
          Uri attachmentUri = FileProvider.getUriForFile(getContext(),
              getContext().getApplicationContext().getPackageName() + ".provider",
              getVm().getAttach_FinalFile(getVm().getAttachMode()));
          HyperLog.d(TAG, "Action View: " + attachmentUri);
          Intent viewImage = new Intent(Intent.ACTION_VIEW, attachmentUri);
          viewImage.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_GRANT_READ_URI_PERMISSION);
          startActivity(viewImage);
        } else {
          if (!isEnable)
            return;

          String[] opts = getResources().getStringArray(R.array.attachment_add_actions);
          AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
          builder.setItems(opts, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
              if (item == 0) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                  File photoFile = null;
                  try {
                    photoFile = ImageUtility.createTempFile(getContext().getCacheDir(), "temp",
                        getVm().getAttachMode(), ImageUtility.EXTENSION_JPG);
                  } catch (IOException e) {
                    HyperLog.e(getThisTag(), R.string.error_starting_camera + e.getMessage());
                    Snackbar.make(getView(), R.string.error_starting_camera, Snackbar.LENGTH_SHORT)
                        .show();
                  }
                  if (photoFile != null) {
                    mViewModel.setAttach_TempFile(photoFile, getVm().getAttachMode());
                    Uri photoURI = FileProvider.getUriForFile(getContext(),
                        getContext().getApplicationContext().getPackageName() + ".provider", photoFile);

                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, Utility.REQUEST_TAKE_PHOTO);
                  }
                }
              } else if (item == 1) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto,
                    id.co.danwinciptaniaga.npmdsandroid.util.Utility.REQUEST_PICK_PHOTO);
              } else {
                dialog.dismiss();
              }
            }
          });
          builder.show();
        }
      }
    });

    getBinding().ivAtPo.setOnLongClickListener(new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
//        Boolean isArchive = getVm().getField_Attach_PO_isArchive().getValue();
//        HyperLog.d(getThisTag(),
//            "attachPo onLongClickListener isArchive[" + isArchive + "], jika archive maka return");
//        if (getVm().getField_Attach_PO_isArchive().getValue().booleanValue())
//          return true;

        getVm().setAttachMode(UtilityState.ATTCH_PO.getId());
        if (isEnable)
          mViewModel.setAttachment_Filled(false, true);
        return true;
      }
    });
  }

  protected void setField_IdnpksfEnabledDisabled(boolean enabled) {
    // set selected so
    mViewModel.getField_soKvm().observe(getViewLifecycleOwner(), soKvm -> {
//      if(soKvm!=null && soAdapter.getCount()>0){
//        int pos =soAdapter.getPosition(soKvm);
//        getBinding().spSO.setSelection(pos);
//      }
//      if (soKvm != null) {
      int test = soAdapter.getCount();
      if (soKvm != null && soAdapter.getCount() > 0) {
        //        KeyValueModel<SoShortData, String> selectedSo = (KeyValueModel<SoShortData, String>) binding.spSO
        //            .getSelectedItem();
        //        if (selectedSo == null || (selectedSo.getKey().getSoId() != soKvm.getKey().getSoId())) {
        //          int pos = soAdapter.getPosition(soKvm);
        //          getBinding().spSO.setSelection(pos);
        //        }
        int pos = soAdapter.getPosition(soKvm);
        getBinding().spSO.setSelection(pos);

        if (enabled) {
          if (soKvm.getKey().isIdSalesForce()||soKvm.getKey().isNonKios())
            getBinding().tilIdnpksf.setEnabled(true);
          else {
            getBinding().tilIdnpksf.setEnabled(false);
            getVm().setField_Idnpksf(null);
          }
        } else {
          getBinding().tilIdnpksf.setEnabled(false);
        }
      }
    });
  }

  private void setOverrideMsg(String fn) {
    String msg = fn + " Fungsi ini harus di override pada masing masing extend class";
    Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    HyperLog.e(getThisTag(), msg);
  }
}
