package id.co.danwinciptaniaga.npmdsandroid.fintransfer.detail;

import java.util.Set;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferDetailBrowseResponse;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferDetailBrowseSort;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;

public class GetTugasTransferDetailBrowseUC {
  private final TugasTransferService service;
  private final AppExecutors appExecutors;
  private TugasTransferBrowseAndroidFilter filterField = new TugasTransferBrowseAndroidFilter();
  private TugasTransferDetailBrowseSort sort = new TugasTransferDetailBrowseSort();

  @Inject
  public GetTugasTransferDetailBrowseUC(TugasTransferService service, AppExecutors appExecutors) {
    this.service = service;
    this.appExecutors = appExecutors;
  }
  public void setFilterField(TugasTransferBrowseAndroidFilter filterField){
    this.filterField = filterField;
  }

  public void setSort(Set<SortOrder> sortField) {
    this.sort.setSortOrders(sortField);
  }

  public ListenableFuture<TugasTransferDetailBrowseResponse> getTugasTransferBrowseDetailList(Integer page,int pageSize){
    ListenableFuture<TugasTransferDetailBrowseResponse> res = service.getDetailList(page,pageSize,filterField,sort);
    return res;
  }
}
