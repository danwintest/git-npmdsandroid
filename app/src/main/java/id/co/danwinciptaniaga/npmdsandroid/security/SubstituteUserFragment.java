package id.co.danwinciptaniaga.npmdsandroid.security;

import org.apache.commons.lang3.StringUtils;

import com.hypertrack.hyperlog.HyperLog;

import android.app.PendingIntent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Toast;
import androidx.annotation.UiThread;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDeepLinkBuilder;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.common.ExtUserInfoData;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentSubstituteUserBinding;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.util.AnimationUtil;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SubstituteUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
public class SubstituteUserFragment extends ProtectedFragment {
  public static final String TAG = SubstituteUserFragment.class.getSimpleName();

  private SubstituteUserVM viewModel;
  private FragmentSubstituteUserBinding binding;
  private SubstituteUserAdapter adapter;

  private View.OnClickListener restart = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // kita coba restart seluruh activity
      Bundle bundle = new Bundle();
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_substitute_user)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragment = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      viewModel.loadSubstituteUsers(true);
      binding.swipeRefresh.setRefreshing(false);
    }
  };

  public SubstituteUserFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment SubstituteUserFragment.
   */
  public static SubstituteUserFragment newInstance() {
    SubstituteUserFragment fragment = new SubstituteUserFragment();
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    viewModel = new ViewModelProvider(requireActivity()).get(SubstituteUserVM.class);
    viewModel.clearFormState();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentSubstituteUserBinding.inflate(inflater, container, false);
    viewModel.getFormState().observe(getViewLifecycleOwner(), this::onFormStateChange);
    viewModel.getActionEvent().observe(getViewLifecycleOwner(), this::onFormActionEvent);

    adapter = new SubstituteUserAdapter(getContext(),
        new SubstituteUserAdapter.Listener() {
          @Override
          public void onItemClicked(View v, ExtUserInfoData userInfoData, int position) {
            Resource<ExtUserInfoData> value = ((LandingActivity) getActivity()).getDrawerViewModel().getUserInfoLd().getValue();
            if (value != null && value.getData() != null && value.getData().getId() != null
                && value.getData().getId().equals(userInfoData.getId())) {
              // abaikan switch user, karena user yang sama
            } else {
              viewModel.substituteUser(userInfoData);
            }
          }
        });
    binding.list.setAdapter(adapter);

    binding.swipeRefresh.setOnRefreshListener(() -> {
      viewModel.loadSubstituteUsers(true);
      binding.swipeRefresh.setRefreshing(true);
    });

    return binding.getRoot();
  }

  private void onFormStateChange(FormState formState) {
    HyperLog.d(TAG, "onFormStateChange called: " + formState);
    if (formState == null)
      return;
    switch (formState.getState()) {
    case READY:
      bindProgressAndPageContentReady(AnimationUtil.fadeoutAnimationMedium(),
          AnimationUtil.fadeinAnimationMedium());
      break;
    case ERROR:
      bindProgressAndPageContentError(formState, AnimationUtil.fadeoutAnimationMedium(),
          AnimationUtil.fadeinAnimationMedium());
      break;
    case LOADING:
    default:
      bindProgressAndPageContentLoading(formState, AnimationUtil.fadeoutAnimationMedium(),
          AnimationUtil.fadeinAnimationMedium());
      break;
    }
  }

  private void onFormActionEvent(Resource<ExtUserInfoData> s) {
    if (Status.SUCCESS.equals(s.getStatus())) {
      Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
      App.showLandingPage(getContext());
    } else {
      String message = s.getMessage();
      if (message == null)
        message =
            s.getData() != null ? s.getMessage() : getString(R.string.error_contact_support);
      Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }
  }

  private void bindProgressAndPageContentReady(Animation outAnimation, Animation inAnimation) {
    HyperLog.d(TAG, "bindProgressAndPageContentReady");
    adapter.submitList(viewModel.getSubstituteUsers());

    binding.progressWrapper.getRoot().setAnimation(outAnimation);
    binding.progressWrapper.getRoot().setVisibility(View.GONE);
    binding.pageContent.setAnimation(inAnimation);
    binding.pageContent.setVisibility(View.VISIBLE);
  }

  private void bindProgressAndPageContentError(FormState formState, Animation outAnimation,
      Animation inAnimation) {
    HyperLog.d(TAG, "bindProgressAndPageContentError");
    if (formState.getMessage() != null) {
      binding.progressWrapper.progressText.setText(formState.getMessage());
    } else {
      binding.progressWrapper.progressText.setText(getString(R.string.error_contact_support));
    }
    binding.progressWrapper.progressBar.setVisibility(View.GONE);
    binding.progressWrapper.getRoot().setAnimation(inAnimation);
    binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
    binding.progressWrapper.retryButton.setAnimation(inAnimation);
    binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);
    if (binding.pageContent.getVisibility() != View.GONE) {
      binding.pageContent.setAnimation(outAnimation);
      binding.pageContent.setVisibility(View.GONE);
    }
  }

  private void bindProgressAndPageContentLoading(FormState formState, Animation outAnimation,
      Animation inAnimation) {
    HyperLog.d(TAG, "bindProgressAndPageContentLoading");
    if (formState.getMessage() != null) {
      binding.progressWrapper.progressText.setText(formState.getMessage());
    } else {
      binding.progressWrapper.progressText.setText(getString(R.string.msg_please_wait));
    }
    binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
    binding.progressWrapper.retryButton.setVisibility(View.GONE);
    binding.progressWrapper.retryButton.setOnClickListener(reloadFragment);
    binding.progressWrapper.getRoot().setAnimation(inAnimation);
    binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
    if (binding.pageContent.getVisibility() != View.GONE) {
      binding.pageContent.setAnimation(outAnimation);
      binding.pageContent.setVisibility(View.GONE);
    }
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    if (status.getStatus() == Status.LOADING) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      ExtUserInfoData euid = ((LandingActivity) getActivity()).getDrawerViewModel().getUserInfoLd().getValue().getData();
      if (euid != null) {
        binding.tvUser.setText(String.format(String.format("%s [%s]",
            StringUtils.defaultIfBlank(euid.getName(), StringUtils.EMPTY),
            euid.getLogin())));
      } else {
        HyperLog.w(TAG, "Why no current ExtUserInfoData here?");
      }

      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      binding.swipeRefresh.setVisibility(View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.GONE);
      binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);
      // retry = restart fragment (karena authentication gagal)
      binding.progressWrapper.retryButton.setOnClickListener(restart);
    }
  }

  @UiThread
  private void onReady() {
    // trigger loading pertama kali setelah authentication Success
    viewModel.loadSubstituteUsers(false);
  }
}