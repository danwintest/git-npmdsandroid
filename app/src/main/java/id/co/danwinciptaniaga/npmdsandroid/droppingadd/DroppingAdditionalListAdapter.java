package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiDroppingAdditionalBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class DroppingAdditionalListAdapter extends
    PagingDataAdapter<DroppingAdditionalData, DroppingAdditionalListAdapter.ViewHolder> {
  private final static String TAG = DroppingAdditionalListAdapter.class.getSimpleName();
  private final DadRecyclerViewAdapterListener mListener;
  private SparseBooleanArray selectedItems;
  private List<DroppingAdditionalData> selectedDad = new ArrayList<>();
  private SparseBooleanArray animationItemsIndex;
  // private boolean reverseAllAnimations = false;
  // private static int currentSelectedIndex = -1;
  private Context mContext;
  //  @Inject
  public CommonService commonService;
  //  @Inject
  public AppExecutors appExecutors;

  @Inject
  public DroppingAdditionalListAdapter(
      DadRecyclerViewAdapterListener listener, Context ctx,
      CommonService commonService, AppExecutors appExecutors) {
    super(new DadDiffCallBack());
    mListener = listener;
    selectedItems = new SparseBooleanArray();
    animationItemsIndex = new SparseBooleanArray();
    mContext = ctx;
    this.commonService = commonService;
    this.appExecutors = appExecutors;

  }

  public int getSelectedItemCount() {
    return selectedItems.size();
  }

  public List<DroppingAdditionalData> getSelectedDad() {
    return selectedDad;
  }

  public boolean isDeleteApproveActionActive() {
    for (DroppingAdditionalData data : selectedDad) {
      if (!Utility.WF_STATUS_DRAFT.equals(data.getWfStatusCode())) {
        return false;
      }
    }
    return true;
  }

  public boolean isRejectActionActive() {
    for (DroppingAdditionalData data : selectedDad) {
      if (!Utility.WF_STATUS_PENDING_APPROVAL.equals(data.getWfStatusCode())) {
        return false;
      }
    }
    return true;
  }

  public void resetAnimationIndex() {
    // reverseAllAnimations = false;
    animationItemsIndex.clear();
  }

  public void toggleSelection(int pos) {
    // currentSelectedIndex = pos;
    DroppingAdditionalData data = getItem(pos);
    if (selectedItems.get(pos, false)) {
      selectedItems.delete(pos);
      selectedDad.remove(data);
      animationItemsIndex.delete(pos);
    } else {
      selectedDad.add(data);
      selectedItems.put(pos, true);
      animationItemsIndex.put(pos, true);
    }
    notifyItemChanged(pos);
  }

  @Override
  public DroppingAdditionalListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.li_dropping_additional_browse,
        parent, false);
    return new DroppingAdditionalListAdapter.ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final DroppingAdditionalListAdapter.ViewHolder holder,
      int position) {
    holder.bind(getItem(position), position);
    // kalau activated, warnanya beda
    holder.binding.getRoot().setActivated(selectedItems.get(position, false));
  }

  public void clearSelections() {
    // reverseAllAnimations = true;
    selectedItems.clear();
    selectedDad.clear();
    notifyDataSetChanged();
  }

  // @Override
  // public void onBindViewHolder(@NonNull ViewHolder holder, int position,
  // @NonNull List<Object> payloads) {
  // if (!payloads.isEmpty()) {
  // // TODO: apakah perlu bind ulang, atau update field spesifik saja
  // holder.bind(getItem(position));
  // } else {
  // onBindViewHolder(holder, position);
  // }
  // }

  interface DadRecyclerViewAdapterListener {
    void onItemClicked(View view, DroppingAdditionalData dad, int position);

    void onItemLongClickedListener(View v, DroppingAdditionalData dad, int position);
  }

  private static class DadDiffCallBack
      extends DiffUtil.ItemCallback<DroppingAdditionalData> {
    @Override
    public boolean areItemsTheSame(@NonNull DroppingAdditionalData oldItem,
        @NonNull DroppingAdditionalData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull DroppingAdditionalData oldItem,
        @NonNull DroppingAdditionalData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiDroppingAdditionalBrowseBinding binding;

    public ViewHolder(View view) {
      super(view);
      binding = LiDroppingAdditionalBrowseBinding.bind(view);
    }

    public void bind(DroppingAdditionalData dad, int position) {
      setObject(dad);
      setObjectListener(dad, position);
    }

    private void setObjectListener(DroppingAdditionalData dad, int position) {
      binding.clDroppingAdditional.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, dad, position);
        }
      });

      binding.clDroppingAdditional.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
          mListener.onItemLongClickedListener(v, dad, position);
          v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
          return true;
        }
      });
    }

    private void setObject(DroppingAdditionalData dad) {
      binding.tvTransactionNo.setText(dad.getTransactionNo());
      binding.tvWfStatus.setText(dad.getWfStatus());
      binding.tvTransferStatus.setText(dad.getTransferStatus());
      binding.tvOutletAndCode.setText(
          String.format("%s (%s)", dad.getOutletName(), dad.getOutletCode()));
      binding.tvRequestAndDroppingDate.setText(
          String.format("%s (%s)",
              Formatter.DTF_dd_MM_yyyy.format(dad.getRequestDate()),
              Formatter.DTF_dd_MM_yyyy.format(dad.getDroppingDate())));
      binding.tvRequestAmount.setText(
          Formatter.DF_AMOUNT_NO_DECIMAL.format(dad.getRequestAmount()));
    }

    //    private void setImage(ExpenseData expenseData){
    //      List<ExpenseAttachmentData> attachmentDataList = expenseData.getAttachments();
    //      if (attachmentDataList != null && attachmentDataList.size() > 0) {
    //        ExpenseAttachmentData ead = attachmentDataList.get(0);
    //        ListenableFuture<ResponseBody> rbLf = commonService.getFile(ead.getId());
    //        Futures.addCallback(rbLf, new FutureCallback<ResponseBody>() {
    //          @Override
    //          public void onSuccess(@NullableDecl ResponseBody result) {
    //            InputStream is = result.byteStream();
    //            FileOutputStream fos = null;
    //            try {
    //              File output = new File(
    //                  ExpenseListAdapter.this.mContext.getCacheDir() + File.separator + App.IMAGE_DIR,
    //                  ead.getFilename());
    //              fos = new FileOutputStream(output);
    //              byte buffer[] = new byte[1024];
    //              int read = -1;
    //              while ((read = is.read(buffer)) != -1) {
    //                fos.write(buffer, 0, read);
    //              }
    //              fos.flush();
    //
    //              Glide.with(ExpenseListAdapter.this.mContext).asBitmap().load(output)
    //                  .placeholder(R.drawable.ic_spinner_drawable).into(new CustomTarget<Bitmap>() {
    //                @Override
    //                public void onResourceReady(@NonNull Bitmap resource,
    //                    @Nullable Transition<? super Bitmap> transition) {
    //                  //                      ExpenseListAdapter.this..postValue(resource);
    //                  Glide.with(ExpenseListAdapter.this.mContext)
    //                      .load(resource)
    //                      .centerCrop()
    //                      .into(ViewHolder.this.mImage);
    ////                  ViewHolder.this.mImage.setImageResource(resource);
    //                }
    //
    //                @Override
    //                public void onLoadCleared(@Nullable Drawable placeholder) {
    //
    //                }
    //              });
    //            } catch (Exception e) {
    //              // TODO: show error message
    //              HyperLog.w(TAG,
    //                  String.format("Failed to save Expense Attachment %s locally", ead.getId()), e);
    //            } finally {
    //              if (fos != null) {
    //                try {
    //                  fos.close();
    //                } catch (IOException e) {
    //                  e.printStackTrace();
    //                }
    //              }
    //            }
    //          }
    //
    //          @Override
    //          public void onFailure(Throwable t) {
    //            // TODO: show error message
    //            HyperLog.w(TAG, String.format("Failed to download Expense Attachment %s", ead.getId()),
    //                t);
    //          }
    //        }, appExecutors.networkIO());
    //      }
    //    }
  }

}
