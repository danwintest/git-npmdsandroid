package id.co.danwinciptaniaga.npmdsandroid.common;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.hypertrack.hyperlog.HyperLog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmdsandroid.databinding.SortEntryBinding;
import id.co.danwinciptaniaga.npmdsandroid.sort.SortPropertyEntry;

public class SortPropertyAdapter extends RecyclerView.Adapter<SortPropertyAdapter.ViewHolder> {
  private static final String TAG = SortPropertyAdapter.class.getSimpleName();
  private List<SortPropertyEntry> items = null;
  private LinkedHashSet<SortPropertyEntry> currentSort = new LinkedHashSet<>(); // sesuai urutan

  public SortPropertyAdapter(@NonNull List<SortPropertyEntry> speList,
      Set<SortPropertyEntry> selectedSpeList) {
    items = speList;
    if (selectedSpeList != null) {
      currentSort.addAll(selectedSpeList);
    }
  }

  @NonNull
  @Override
  public SortPropertyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    SortEntryBinding seb = SortEntryBinding.inflate(LayoutInflater.from(parent.getContext()),
        parent, false);
    return new ViewHolder(seb.getRoot());
  }

  @Override
  public void onBindViewHolder(@NonNull SortPropertyAdapter.ViewHolder holder, int position) {
    SortPropertyEntry item = items.get(position);
    bindVHState(holder, item);
    holder.getBinding().cbSortProperty.setOnCheckedChangeListener((buttonView, isChecked) -> {
      SortPropertyEntry taggedItem = (SortPropertyEntry) buttonView.getTag();
      HyperLog.d(TAG, String.format("%s isChecked: %s", taggedItem, isChecked));
      taggedItem.setSelected(isChecked);
      bindVHState(holder, taggedItem);
    });
    holder.getBinding().swSortDirection.setOnToggledListener((toggleableView, isOn) -> {
      SortPropertyEntry taggedItem = (SortPropertyEntry) toggleableView.getTag();
      taggedItem.setOn(isOn);
      bindVHState(holder, taggedItem);
    });
  }

  private void bindVHState(@NonNull ViewHolder holder, SortPropertyEntry item) {
    holder.getBinding().cbSortProperty.setText(item.getStringResId());
    holder.getBinding().cbSortProperty.setTag(item);

    holder.getBinding().cbSortProperty.setChecked(item.isSelected());
    if (item.isSelected()) {
      holder.getBinding().swSortDirection.setEnabled(true);
      if (!currentSort.contains(item)) {
        // kalau dicentang, dan belum ada --> set
        currentSort.add(item);
      }
    } else {
      holder.getBinding().swSortDirection.setEnabled(false);
      if (currentSort.contains(item)) {
        // kalau tidak dicentang, dan ada --> hapus
        currentSort.remove(item);
      }
    }
    HyperLog.d(TAG, String.format("selection: %s", currentSort));
    holder.getBinding().swSortDirection.setOn(item.isOn());
    holder.getBinding().swSortDirection.setTag(item);
  }

  @Override
  public int getItemCount() {
    return items.size();
  }

  public Set<SortPropertyEntry> getPropertySelection() {
    HyperLog.d(TAG, String.format("getPropertySelection: %s", currentSort));
    return Collections.unmodifiableSet(currentSort);
  }

  public static class ViewHolder extends RecyclerView.ViewHolder {
    private final SortEntryBinding binding;

    public ViewHolder(@NonNull View itemView) {
      super(itemView);
      binding = SortEntryBinding.bind(itemView);
    }

    public SortEntryBinding getBinding() {
      return binding;
    }
  }
}
