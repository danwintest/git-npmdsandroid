package id.co.danwinciptaniaga.npmdsandroid.booking;

import org.jetbrains.annotations.NotNull;

import com.hypertrack.hyperlog.HyperLog;

import android.view.View;
import android.view.ViewGroup;
import androidx.paging.LoadState;
import androidx.paging.LoadStateAdapter;
import id.co.danwinciptaniaga.npmdsandroid.ui.NetworkStateItemViewHolder;

public class BookingListLoadStateAdapter extends LoadStateAdapter<NetworkStateItemViewHolder> {
  private static final String TAG = BookingListLoadStateAdapter.class.getSimpleName();
  private final View.OnClickListener retryCallback;

  public BookingListLoadStateAdapter(View.OnClickListener retryCallback) {
    this.retryCallback = retryCallback;
  }

  @Override
  public void onBindViewHolder(@NotNull NetworkStateItemViewHolder viewHolder,
      @NotNull LoadState loadState) {
    HyperLog.d(TAG, "calling NetworkStateItemViewHolder.bindTo: " + viewHolder + " - " + loadState);
    viewHolder.bindTo(loadState);
  }

  @NotNull
  @Override
  public NetworkStateItemViewHolder onCreateViewHolder(@NotNull ViewGroup parent,
      @NotNull LoadState loadState) {
    return new NetworkStateItemViewHolder(parent,retryCallback);
  }
}
