package id.co.danwinciptaniaga.npmdsandroid.prefs;

import java.nio.charset.Charset;
import java.util.Objects;
import java.util.concurrent.Executor;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.app.DownloadManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreferenceCompat;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.acs.data.LatestAppVersionResponse;
import id.co.danwinciptaniaga.androcon.AndroconConfig;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.update.UpdateUtility;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.SecurityUtil;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.BuildConfig;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;

@AndroidEntryPoint
public class PreferenceFragment extends PreferenceFragmentCompat {
  private static final String TAG = PreferenceFragment.class.getSimpleName();

  @Inject
  AndroconConfig androconConfig;
  @Inject
  LoginUtil loginUtil;
  @Inject
  TestPushNotificationUseCase ucTestPushNotification;
  @Inject
  UploadLogFileUseCase ucUploadLogFile;

  private Executor executor;
  private PreferenceViewModel model;

  private Preference prefCheckUpdate;
  private Preference prefDownloadUpdate;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    model = new ViewModelProvider(requireActivity()).get(PreferenceViewModel.class);
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    prefCheckUpdate = findPreference("check_update");
    prefDownloadUpdate = findPreference("download_update");

    prepareObserverAppUpdate();

    model.getDownloadUpdateStatus().observe(getViewLifecycleOwner(), downloadStatus -> {
      if (downloadStatus != null) {
        prefDownloadUpdate.setVisible(true);
        switch (downloadStatus) {
        case DownloadManager.STATUS_SUCCESSFUL:
          prefDownloadUpdate.setEnabled(true);
          prefDownloadUpdate.setTitle(getString(R.string.install_update_pref_title));
          break;
        case DownloadManager.STATUS_RUNNING:
          prefDownloadUpdate.setTitle(getString(R.string.download_update_pref_title));
          prefDownloadUpdate.setEnabled(false);
          break;
        case DownloadManager.STATUS_PENDING:
          prefDownloadUpdate.setTitle(getString(R.string.download_update_pref_title));
          prefDownloadUpdate.setEnabled(false);
          break;
        case DownloadManager.STATUS_FAILED:
          prefDownloadUpdate.setTitle(getString(R.string.download_update_pref_title));
          prefDownloadUpdate.setEnabled(true);
          break;
        case DownloadManager.STATUS_PAUSED:
          prefDownloadUpdate.setTitle(getString(R.string.download_update_pref_title));
          prefDownloadUpdate.setEnabled(false);
          break;
        }
      }
    });

    model.getDownloadMessage().observe(getViewLifecycleOwner(), message -> {
      if (message != null) {
        prefDownloadUpdate.setSummary(message);
      }
    });
  }

  @Override
  public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
    setPreferencesFromResource(R.xml.app_preferences, rootKey);

    setupPreferenceBiometric();

    Preference prefChangePassword = findPreference("change_password");
    prefChangePassword.setOnPreferenceClickListener(
        new Preference.OnPreferenceClickListener() {
          @Override
          public boolean onPreferenceClick(Preference preference) {
            NavController navController = Navigation.findNavController(getView());
            navController.navigate(R.id.nav_change_password, null,
                NavHelper.animParentToChild().build());
            return true;
          }
        }
    );

    Preference prefTestPushNotification = findPreference("test_push_notification");
    prefTestPushNotification.setOnPreferenceClickListener(
        new Preference.OnPreferenceClickListener() {
          @Override
          public boolean onPreferenceClick(Preference preference) {
            if (androconConfig.getFcmToken() != null) {
              ucTestPushNotification.test(androconConfig.getFcmToken());
            } else {
              // TODO: trigger get FCM Token?
              Toast.makeText(getContext(), R.string.push_notification_token_not_available,
                  Toast.LENGTH_SHORT).show();
            }
            return true;
          }
        });

    Preference prefUploadLog = findPreference("upload_log_file");
    prefUploadLog.setOnPreferenceClickListener(
        new Preference.OnPreferenceClickListener() {
          @Override
          public boolean onPreferenceClick(Preference preference) {
            ucUploadLogFile.upload();
            return true;
          }
        });

    setupPreferenceCheckUpdate();

    setupPreferenceDownloadUpdate();
  }

  @Override
  public void onResume() {
    super.onResume();
    long downloadId = UpdateUtility.getDownloadId(getContext());
    if (downloadId != -1) {
      model.monitorDownload();
    } else {
      model.checkLatestAppVersionLd();
    }
  }

  private void setupPreferenceBiometric() {
    SwitchPreferenceCompat prefEnableBiometric = findPreference("enable_biometric");
    String bioMatricUsername = loginUtil.getBiometricUsername();
    if (Objects.equals(bioMatricUsername, null)) {
      HyperLog.i(TAG, "bioMatricUsername[" + bioMatricUsername + "] setSwitch Biomatric OFF");
      prefEnableBiometric.setChecked(false);
    }
    if (loginUtil.isLoggedIn() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      BiometricManager biometricManager = BiometricManager.from(getContext());
      executor = ContextCompat.getMainExecutor(getContext());
      int canAuthenticate = biometricManager.canAuthenticate();
      boolean isVisible = false;
      switch (canAuthenticate) {
      case BiometricManager.BIOMETRIC_SUCCESS:
        prefEnableBiometric.setEnabled(true);
        isVisible = true;
        break;
      case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
        prefEnableBiometric.setEnabled(false);
        prefEnableBiometric.setSummary(
            getString(R.string.biometric_signin_activation_pref_summary_none_enrolled));
        isVisible = true;
        break;
      default:
        HyperLog.w(TAG, "Disabling Biometric Signin: " + canAuthenticate);
        isVisible = false;
      }
      prefEnableBiometric.setVisible(isVisible);
      if (isVisible) {
        setupBiometric(prefEnableBiometric);
      }
    } else {
      prefEnableBiometric.setVisible(false);
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  private void setupBiometric(SwitchPreferenceCompat prefEnableBiometric) {
    executor = ContextCompat.getMainExecutor(getContext());

    prefEnableBiometric.setOnPreferenceClickListener(
        new Preference.OnPreferenceClickListener() {
          @Override
          public boolean onPreferenceClick(Preference preference) {
            if (!prefEnableBiometric.isChecked()) {
              // matikan Biometric Signin
              HyperLog.d(TAG, "Biometric Signin OFF");
              loginUtil.clearBiometricUser();
            } else {
              HyperLog.d(TAG, "Biometric Signin ON");
              // aktifkan Biometric Signin
              String username = loginUtil.getUsername();
              try {
                SecretKey secretKey = null;
//                try {
//                  secretKey = SecurityUtil.getSecretKey(App.SECRET_KEY_NAME);
//                } catch (Exception e) {
//                  HyperLog.w(TAG, "Problem retrieving Secret Key", e);
//                }
//                if (secretKey == null) {
//                  HyperLog.i(TAG, "Generating Secret Key");
                  SecurityUtil.generateSecretKey(App.SECRET_KEY_NAME);
                  secretKey = SecurityUtil.getSecretKey(App.SECRET_KEY_NAME);
//                } else {
//                  HyperLog.i(TAG, "Secret Key exists");
//                }
                Cipher finalCipher = SecurityUtil.getCipher();
                // harusnya kalau sampai di sini, sudah ready
                finalCipher.init(Cipher.ENCRYPT_MODE, secretKey);

                // confirm password untuk currentUser dulu
                BiometricPrompt biometricPrompt = new BiometricPrompt(
                    PreferenceFragment.this.getActivity(), executor,
                    new BiometricPrompt.AuthenticationCallback() {
                      @Override
                      public void onAuthenticationError(int errorCode,
                          @NonNull CharSequence errString) {
                        HyperLog.d(TAG,
                            String.format("onAuthenticationError: %s-%s", errorCode,
                                errString));
                        if (errorCode == BiometricPrompt.ERROR_CANCELED
                            || errorCode == BiometricPrompt.ERROR_TIMEOUT
                            || errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                          // tidak perlu tampilkan pesan
                        } else {
                          Toast.makeText(getContext(),
                              getString(R.string.biometric_signin_activation_fail_reason,
                                  username, errString),
                              Toast.LENGTH_SHORT).show();
                        }
                        prefEnableBiometric.setChecked(false);
                      }

                      @Override
                      public void onAuthenticationSucceeded(
                          @NonNull BiometricPrompt.AuthenticationResult result) {
                        HyperLog.d(TAG, String.format("onAuthenticationSucceeded: %s", result));
                        try {
                          byte[] encryptedUsername = result.getCryptoObject().getCipher().doFinal(
                              username.getBytes(Charset.defaultCharset()));

                          IvParameterSpec ivParams = finalCipher.getParameters().getParameterSpec(
                              IvParameterSpec.class);
                          loginUtil.setBiometricUser(ivParams.getIV(), username, encryptedUsername);

                          prefEnableBiometric.setChecked(true);
                          Toast.makeText(getContext(),
                              getString(R.string.biometric_signin_activation_success, username),
                              Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                          HyperLog.e(TAG,
                              "Failed to encrypt data after successful Biometric Authentication",
                              e);
                          Toast.makeText(getContext(),
                              getString(R.string.biometric_signin_activation_fail_reason,
                                  username, e.getMessage()),
                              Toast.LENGTH_SHORT).show();
                          prefEnableBiometric.setChecked(false);
                        }
                      }

                      @Override
                      public void onAuthenticationFailed() {
                        HyperLog.d(TAG, String.format("onAuthenticationFailed"));
                        Toast.makeText(getContext(),
                            getString(R.string.biometric_signin_activation_fail, username),
                            Toast.LENGTH_SHORT).show();
                        prefEnableBiometric.setChecked(false);
                      }
                    });

                BiometricPrompt.PromptInfo promptInfo = new BiometricPrompt.PromptInfo.Builder()
                    .setTitle(
                        getString(R.string.biometric_signin_activation_prompt_title, username))
                    .setSubtitle(getString(R.string.biometric_signin_activation_prompt_subtitle))
                    .setDeviceCredentialAllowed(false)
                    .setNegativeButtonText(getString(R.string.biometric_signin_cancel))
                    .build();

                biometricPrompt.authenticate(promptInfo,
                    new BiometricPrompt.CryptoObject(finalCipher));
              } catch (Exception e) {
                HyperLog.w(TAG, "Problem prompting Biometric Authentication", e);
                prefEnableBiometric.setChecked(false);
                Toast.makeText(getContext(),
                    getString(R.string.generic_error, e.getMessage()),
                    Toast.LENGTH_LONG).show();
              }
            }
            return true;
          }
        });
  }

  private void setupPreferenceCheckUpdate() {
    Preference prefCheckUpdate = findPreference("check_update");
    prefCheckUpdate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
      @Override
      public boolean onPreferenceClick(Preference preference) {
        model.checkLatestAppVersionLd();
        return true;
      }
    });
  }

  private void setupPreferenceDownloadUpdate() {
    Preference prefDownloadUpdate = findPreference("download_update");
    prefDownloadUpdate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
      @Override
      public boolean onPreferenceClick(Preference preference) {
        int installerStatus = UpdateUtility.getInstallerStatus(getContext());
        if (installerStatus == UpdateUtility.INSTALLER_STATUS_READY) {
          UpdateUtility.triggerUpdate(getContext(), androconConfig.getApkFileName());
        } else {
          model.downloadUpdate();
        }
        return true;
      }
    });
  }

  private void prepareObserverAppUpdate() {
    model.getLatestAppVersionLd().observe(getViewLifecycleOwner(),
        new Observer<Resource<LatestAppVersionResponse>>() {
          @Override
          public void onChanged(Resource<LatestAppVersionResponse> lavrr) {
            switch (lavrr.getStatus()) {
            case LOADING:
              prefCheckUpdate.setSummary(lavrr.getMessage());
              prefCheckUpdate.setEnabled(false);
              prefDownloadUpdate.setVisible(false);
              break;
            case SUCCESS:
              prefCheckUpdate.setSummary(lavrr.getMessage());
              prefCheckUpdate.setEnabled(true);
              if (lavrr.getData() != null || BuildConfig.DEBUG) {
                prefDownloadUpdate.setVisible(true);
              } else {
                prefDownloadUpdate.setVisible(false);
              }
              break;
            case ERROR:
              String msg = String.format("%s: %s", lavrr.getMessage(),
                  lavrr.getThrowable().getMessage());
              prefCheckUpdate.setSummary(msg);
              prefCheckUpdate.setEnabled(true);
              if (BuildConfig.DEBUG) {
                prefDownloadUpdate.setVisible(true);
              } else {
                prefDownloadUpdate.setVisible(false);
              }
              break;
            }
          }
        });
  }
}