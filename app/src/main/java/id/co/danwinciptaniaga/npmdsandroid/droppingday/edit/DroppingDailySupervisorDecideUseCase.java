package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.droppingday.DroppingDailyService;
import retrofit2.HttpException;

public class DroppingDailySupervisorDecideUseCase extends
    BaseObservable<DroppingDailySupervisorDecideUseCase.Listener, DroppingDailyData> {
  public interface Listener {

    void onDdDecideStarted(Resource<DroppingDailyData> loading);

    void onDdDecideSuccess(Resource<DroppingDailyData> response);

    void onDdDecideFailure(Resource<DroppingDailyData> response);

  }

  private final DroppingDailyService ddService;

  private final AppExecutors appExecutors;

  @Inject
  public DroppingDailySupervisorDecideUseCase(Application application,
      DroppingDailyService ddService,
      AppExecutors appExecutors) {
    super(application);
    this.ddService = ddService;
    this.appExecutors = appExecutors;
  }

  public void finStaffApprove(UUID procTaskId, String comment, UUID droppingId,
      LocalDate transferDate, String transferType, UUID sourceAccountId, Date checkTs) {
//    LocalDate mandatoryDateField = LocalDate.now();
    notifyStart(null, null);
    ListenableFuture<DroppingDailyData> process = ddService.finStaffApprove(procTaskId,
        comment, droppingId, transferDate, transferType, sourceAccountId,
//        mandatoryDateField,
        checkTs);

    Futures.addCallback(process, new FutureCallback<DroppingDailyData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingDailyData result) {
        notifySuccess(application.getString(R.string.dropping_daily_decide_success,
            application.getString(R.string.action_approve)), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_daily_decide_failed,
                application.getString(R.string.action_reject)) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void finSpvApprove(UUID procTaskId, String comment, UUID droppingId, String otpCode,
      String transferType, Date checkTs) {
    notifyStart(null, null);
    ListenableFuture<DroppingDailyData> process = ddService.finSpvApprove(procTaskId,
        comment, droppingId, otpCode, transferType, checkTs);
    Futures.addCallback(process, new FutureCallback<DroppingDailyData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingDailyData result) {
        notifySuccess(application.getString(R.string.dropping_daily_decide_success,
            application.getString(R.string.action_approve)), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_daily_decide_failed,
                application.getString(R.string.action_reject)) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void processGenerateOTP(UUID procTaskId, UUID droppingId) {
    notifyStart(null, null);
    ListenableFuture<DroppingDailyData> process = ddService.generateOtp(procTaskId, droppingId);
    Futures.addCallback(process, new FutureCallback<DroppingDailyData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingDailyData result) {
//        notifySuccess(application.getString(R.string.msg_request_otp), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.request_otp_failed) : null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void captainApprove(UUID procTaskId, String comment, UUID droppingId, long approvedAmount,
      boolean altAccount, BankAccountData bankAccountData, Date checkTs) {
    notifyStart(null, null);
    ListenableFuture<DroppingDailyData> saveDraftLf = ddService.captainApprove(procTaskId,
        comment,
        droppingId, approvedAmount, altAccount, bankAccountData, checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<DroppingDailyData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingDailyData result) {
        notifySuccess(application.getString(R.string.dropping_daily_decide_success,
            application.getString(R.string.action_approve)), result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_daily_decide_failed,
                application.getString(R.string.action_reject)) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void rejectOrReturn(UUID procTaskId, UUID droppingId, String outcome, String comment,
      Date checkTs) {
    notifyStart(null, null);
    ListenableFuture<DroppingDailyData> saveDraftLf = ddService.rejectOrReturn(procTaskId,
        outcome,
        comment, droppingId, checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<DroppingDailyData>() {
      @Override
      public void onSuccess(@NullableDecl DroppingDailyData result) {
        notifySuccess(application.getString(R.string.dropping_daily_decide_success, outcome),
            result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(((HttpException) t).response().errorBody());
        }
        // kalau tidak ada ER, berikan pesan error standar
        notifyFailure(er == null ?
            application.getString(R.string.dropping_daily_decide_failed, outcome) :
            null, er, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<DroppingDailyData> result) {
    listener.onDdDecideStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<DroppingDailyData> result) {
    listener.onDdDecideSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<DroppingDailyData> result) {
    listener.onDdDecideFailure(result);
  }
}
