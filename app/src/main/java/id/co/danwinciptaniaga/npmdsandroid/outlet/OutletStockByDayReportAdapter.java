package id.co.danwinciptaniaga.npmdsandroid.outlet;

import java.util.List;
import java.util.Objects;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletStockByDayReportData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.LiOutletStockByDayBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

public class OutletStockByDayReportAdapter extends
    ListAdapter<OutletStockByDayReportData, OutletStockByDayReportAdapter.ViewHolder> {
  private final static String TAG = OutletStockByDayReportAdapter.class.getSimpleName();

  public OutletStockByDayReportAdapter() {
    super(new DiffCallBack());
  }

  @Override
  public void submitList(@Nullable List<OutletStockByDayReportData> list) {
    super.submitList(list);
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.li_outlet_stock_by_day, parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
  }

  private static class DiffCallBack extends DiffUtil.ItemCallback<OutletStockByDayReportData> {
    @Override
    public boolean areItemsTheSame(@NonNull OutletStockByDayReportData oldItem,
        @NonNull OutletStockByDayReportData newItem) {
      return oldItem.getTransactionDate() == newItem.getTransactionDate();
    }

    @Override
    public boolean areContentsTheSame(@NonNull OutletStockByDayReportData oldItem,
        @NonNull OutletStockByDayReportData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    private final LiOutletStockByDayBinding binding;

    public ViewHolder(View view) {
      super(view);
      binding = LiOutletStockByDayBinding.bind(view);
    }

    public void bind(OutletStockByDayReportData obrd, int position) {
      setObject(obrd, position);
    }

    private void setObject(OutletStockByDayReportData outletStockData, int position) {
      binding.tvDate.setText(Formatter.DTF_dd_MM_yyyy.format(outletStockData.getTransactionDate()));
      binding.tvQtyIn.setText(Formatter.DF_AMOUNT_NO_DECIMAL.format(outletStockData.getQtyIn()));
      binding.tvQtyOut.setText(Formatter.DF_AMOUNT_NO_DECIMAL.format(outletStockData.getQtyOut()));
    }
  }
}
