package id.co.danwinciptaniaga.npmdsandroid.exp;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseSort;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseListResponse;

public class GetExpenseListUseCase {
  private static final String TAG = GetExpenseListUseCase.class.getSimpleName();

  private final ExpenseService expenseService;
  private ExpenseBrowseFilter filter = new ExpenseBrowseFilter();
  private ExpenseBrowseSort sort = new ExpenseBrowseSort();

  @Inject
  public GetExpenseListUseCase(ExpenseService expenseService) {
    this.expenseService = expenseService;
  }

  public ListenableFuture<ExpenseListResponse> getExpenseList(Integer page, Integer pageSize) {
    ListenableFuture<ExpenseListResponse> result = expenseService.getExpenseList(page, pageSize,
        filter, sort);
    return result;
  }

  public void setSortOrders(Set<SortOrder> sortOrders) {
    this.sort.setSortOrders(sortOrders);
  }
  public void setFilter(ExpenseBrowseFilter newFilter) {
    this.filter = newFilter;
  }
}
