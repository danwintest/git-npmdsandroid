package id.co.danwinciptaniaga.npmdsandroid.ui;

import java.io.File;
import java.io.IOException;

import com.hypertrack.hyperlog.HyperLog;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.widget.Toast;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import id.co.danwinciptaniaga.androcon.utility.ImageUtility;
import id.co.danwinciptaniaga.npmdsandroid.R;

public class ImageUIHelper {
  private static final String TAG = ImageUIHelper.class.getSimpleName();

  public static File startImageCaptureIntent(Fragment fragment, int requestCode) {
    Intent takePictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
    File photoFile = null;
    if (takePictureIntent.resolveActivity(fragment.getActivity().getPackageManager()) != null) {
      // Buat file menunjuk lokasi penyimpanan hasil dari kamera
      photoFile = null;
      try {
        photoFile = ImageUtility.createTempFile(fragment.getContext().getCacheDir(), "temp",
            "tmpexp-", ImageUtility.EXTENSION_JPG);
      } catch (IOException ex) {
        HyperLog.exception(TAG, "Failed to create temporary file for storing camera result", ex);
        // Error occurred while creating the File
        Toast.makeText(fragment.getContext(), R.string.error_starting_camera,
            Toast.LENGTH_SHORT).show();
      }
      // Lanjutkan kalau pembuatan temp file berhasil
      if (photoFile != null) {
        // ini hanya "pointer" ke file-nya. File baru akan "terisi" apabila Intent berhasil
        Uri photoURI = FileProvider.getUriForFile(fragment.getContext(),
            fragment.getContext().getApplicationContext().getPackageName() + ".provider",
            photoFile);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        fragment.startActivityForResult(takePictureIntent, requestCode);
      }
    }
    return photoFile;
  }

  public static void startImagePickIntent(Fragment fragment, int requestCode) {
    Intent pickImageIntent = new Intent(Intent.ACTION_PICK,
        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
    fragment.startActivityForResult(pickImageIntent, requestCode);
  }

  public static void viewImageUsingIntent(Context context, File imageFile) {
    Uri attachmentUri = FileProvider.getUriForFile(context,
        context.getApplicationContext().getPackageName() + ".provider", imageFile);
    HyperLog.d(TAG, "Action View: " + attachmentUri);
    Intent viewImage = new Intent(Intent.ACTION_VIEW, attachmentUri);
    viewImage.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT | Intent.FLAG_GRANT_READ_URI_PERMISSION);
    context.startActivity(viewImage);
  }
}
