package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer;

import java.util.UUID;

import com.google.android.material.snackbar.Snackbar;
import com.hypertrack.hyperlog.HyperLog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Toast;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm.AppBookingTransferRekeningForm;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.wf.WfType;

public class AppBookingTransferEditFragment extends AppBookingTransferNewFragment {
  private final String TAG = AppBookingTransferEditFragment.class.getSimpleName();
  private BookingBrowseViewModel mBrowseVm;

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    AppBookingTransferVM vm = new ViewModelProvider(this).get(AppBookingTransferVM.class);
    AppBookingTransferEditFragmentArgs args = AppBookingTransferEditFragmentArgs.fromBundle(
        getArguments());

    mBrowseVm = new ViewModelProvider(requireActivity()).get(BookingBrowseViewModel.class);
    boolean isHasPermission = args.getIsHasCreatePermission();
    setOnCreateView(isHasPermission, inflater, container, this, TAG, vm);

    getTransferVm().setField_BookingId(args.getBookingId());
    getTransferVm().setFromBookingStatus(true);
    return getBinding().getRoot();
  }

  @Override
  protected void processLoadData() {
    getTransferVm().processLoadAppBookingDataByBooking(getTransferVm().getField_BookingId(), true,
        true);
  }

  @Override
  protected void setReadyFormState(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans, String message,
      Animation inAnim, Animation outAnim, boolean isFromAction) {
    if (isFromAction) {
      mBrowseVm.setRefreshList(true);
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      navController.navigateUp();
      return;
    } else
      super.setReadyFormState(smCash, smTrans, message, inAnim,
          outAnim, isFromAction);
  }

  @Override
  protected void setMode_generalField(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer, String message, Animation inAnimation,
      Animation outAnimation) {
    super.setMode_generalField(smCash, smTransfer, message, inAnimation, outAnimation);
  }

  @Override
  protected void setStandartButtonListener(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer) {
    getBinding().btnSimpan.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getTransferVm().processSaveDraftEdit(true, true);
      }
    });

    getBinding().btnSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getTransferVm().processWfSubmitEdit(true);
      }
    });

    getBinding().btnWfHistory.setOnClickListener(v -> {
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      AppBookingTransferEditFragmentDirections.ActionWfHistory dir = AppBookingTransferEditFragmentDirections
          .actionWfHistory(WfType.APP_BOOKING,
              getTransferVm().getField_AppBookingId());
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });
  }

  @Override
  protected void showEditRekeningForm(String mode) {
    boolean isSupportedMode = true;
    boolean validStatus = false;
    String accName = null;
    String accNo = null;
    String otherBankName = null;
    UUID bankId = null;
    switch (mode) {
    case AppBookingTransferVM.TXN_CONSUMER:
      accName = getTransferVm().getField_ConsumerAccountName().getValue();
      accNo = getTransferVm().getField_ConsumerAccountNo().getValue();
      otherBankName = getTransferVm().getField_ConsumerOtherBank().getValue();
      bankId = getTransferVm().getField_ConsumerBankId().getValue();
      validStatus = getTransferVm().isField_ConsValid().getValue() != null ?
          getTransferVm().isField_ConsValid().getValue() :
          false;
      break;
    case AppBookingTransferVM.TXN_BIRO_JASA:
      accName = getTransferVm().getField_BiroJasaAccountName().getValue();
      accNo = getTransferVm().getField_BiroJasaAccountNo().getValue();
      otherBankName = getTransferVm().getField_BiroJasaOtherBank().getValue();
      bankId = getTransferVm().getField_BiroJasaBankId().getValue();
      validStatus = getTransferVm().isField_BjValid() != null ?
          getTransferVm().isField_BjValid().getValue() :
          false;
      break;
    case AppBookingTransferVM.TXN_FIF:
      accName = getTransferVm().getField_FIFAccountName().getValue();
      accNo = getTransferVm().getField_FIFAccountNo().getValue();
      otherBankName = getTransferVm().getField_FIFOtherBank().getValue();
      bankId = getTransferVm().getField_FIFBankId().getValue();
      validStatus = getTransferVm().isField_FIFValid() != null ?
          getTransferVm().isField_FIFValid().getValue() :
          false;
      break;
    default:
      Snackbar.make(getView(), "Mode tidak didukung", Snackbar.LENGTH_LONG).show();
      HyperLog.e(getThisTag(), "onDismis mode tidak diketahui->" + mode + "<-");
      isSupportedMode = false;
      break;
    }
    if (!isSupportedMode)
      return;

    NavController navController = Navigation.findNavController(getBinding().getRoot());

    if (getTransferVm().getField_outletKvm() == null) {
      Toast.makeText(getContext(), getString(R.string.mandatory_outlet), Toast.LENGTH_SHORT).show();
      return;
    } else if (getTransferVm().getField_outletKvm().getValue() == null) {
      Toast.makeText(getContext(), getString(R.string.mandatory_outlet), Toast.LENGTH_SHORT).show();
      return;
    }

    AppBookingTransferEditFragmentDirections.ActionTrxForm dir = AppBookingTransferEditFragmentDirections.actionTrxForm(
        getTransferVm().getField_outletKvm().getValue().getKey().getOutletId(),
        mode, getTransferVm().getField_CompanyId(), getTransferVm().getFromBookingStatus(),
        getTransferVm().getField_ConsumerName().getValue(), accName, accNo, bankId, otherBankName,
        validStatus);
    navController.navigate(dir, NavHelper.animChildToParent().build());
  }
}
