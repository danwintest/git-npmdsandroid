package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.base.Preconditions;
import com.google.common.net.MediaType;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import okhttp3.MultipartBody;
import retrofit2.HttpException;

public class ExpenseSaveDraftUseCase
    extends BaseObservable<ExpenseSaveDraftUseCase.Listener, ExpenseData> {

  public static final String PARAM_EXISTING_ATTACHMENT_IDS = "existingAttachmentIds";
  public static final String PARAM_NEW_ATTACHMENTS = "newAttachments";

  public interface Listener {
    void onExpenseSaveDraftStarted(Resource<ExpenseData> loading);

    void onExpenseSaveDraftSuccess(Resource<ExpenseData> response);

    void onExpenseSaveDraftFailure(Resource<ExpenseData> response);
  }

  private final ExpenseService expenseService;

  private final AppExecutors appExecutors;

  @Inject
  public ExpenseSaveDraftUseCase(Application application, ExpenseService expenseService,
      AppExecutors appExecutors) {
    super(application);
    this.expenseService = expenseService;
    this.appExecutors = appExecutors;
  }

  public void saveExpenseDraft(UUID expenseId, UUID outletId, LocalDate statementDate,
      LocalDate expenseDate, UUID expenseItemId, UUID bookingId, BigDecimal amount,
      Integer quantity, String description, UUID attachmentId, File attachment, Date checkTs) {
    notifyStart(null, null);
    Preconditions.checkArgument(!(attachmentId != null && attachment != null),
        "Existing attachmentId and attachment cannot be set together");
    MultipartBody.Part[] attachmentIds = null;
    if (attachmentId != null) {
      MultipartBody.Part aIdPart = MultipartBody.Part.createFormData(PARAM_EXISTING_ATTACHMENT_IDS,
          attachmentId.toString());
      attachmentIds = new MultipartBody.Part[] { aIdPart };
    }
    MultipartBody.Part[] attachmentParts = null;
    if (attachment != null) {
      MultipartBody.Part attachmentPart = RetrofitUtility.prepareFilePart(PARAM_NEW_ATTACHMENTS,
          attachment, MediaType.ANY_IMAGE_TYPE.toString());
      attachmentParts = new MultipartBody.Part[] { attachmentPart };
    }

    ListenableFuture<ExpenseData> saveDraftLf = expenseService.saveExpenseDraft(expenseId, outletId,
        statementDate, expenseDate, expenseItemId, bookingId, amount, quantity, description,
        attachmentIds, attachmentParts, checkTs);
    Futures.addCallback(saveDraftLf, new FutureCallback<ExpenseData>() {
      @Override
      public void onSuccess(@NullableDecl ExpenseData result) {
        notifySuccess("Simpan Biaya Operasional berhasil", result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(
              ((HttpException) t).response().errorBody());
        }
        notifyFailure(er == null ? "Simpan Biaya Operasional gagal" : null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void submitExpense(UUID expenseId, UUID outletId, LocalDate statementDate,
      LocalDate expenseDate, UUID expenseItemId, UUID bookingId, BigDecimal amount,
      Integer quantity, String description, UUID attachmentId, File attachment, Date checkTs) {
    notifyStart(null, null);
    MultipartBody.Part[] attachmentIds = null;
    if (attachmentId != null) {
      MultipartBody.Part aIdPart = MultipartBody.Part.createFormData(PARAM_EXISTING_ATTACHMENT_IDS,
          attachmentId.toString());
      attachmentIds = new MultipartBody.Part[] { aIdPart };
    }
    MultipartBody.Part[] attachmentParts = null;
    if (attachment != null) {
      MultipartBody.Part attachmentPart = RetrofitUtility.prepareFilePart(PARAM_NEW_ATTACHMENTS,
          attachment, MediaType.ANY_IMAGE_TYPE.toString());
      attachmentParts = new MultipartBody.Part[] { attachmentPart };
    }
    ListenableFuture<ExpenseData> submitLf = expenseService.submitExpense(expenseId, outletId,
        statementDate, expenseDate, expenseItemId, bookingId, amount, quantity, description,
        attachmentIds, attachmentParts, checkTs);
    Futures.addCallback(submitLf, new FutureCallback<ExpenseData>() {
      @Override
      public void onSuccess(@NullableDecl ExpenseData result) {
        notifySuccess("Submit Biaya Operasional berhasil", result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(
              ((HttpException) t).response().errorBody());
        }
        notifyFailure(er == null ? "Submit biaya Operasional gagal" : null, er, t);
      }
    }, appExecutors.networkIO());
  }

  public void submitExpenseRevision(UUID expenseId, UUID procTaskId, String comment, UUID outletId,
      LocalDate statementDate, LocalDate expenseDate, UUID expenseItemId, UUID bookingId,
      BigDecimal amount, Integer quantity, String description, UUID attachmentId, File attachment,
      Date checkTs) {
    notifyStart(null, null);
    MultipartBody.Part[] attachmentIds = null;
    if (attachmentId != null) {
      MultipartBody.Part aIdPart = MultipartBody.Part.createFormData(PARAM_EXISTING_ATTACHMENT_IDS,
          attachmentId.toString());
      attachmentIds = new MultipartBody.Part[] { aIdPart };
    }
    MultipartBody.Part[] attachmentParts = null;
    if (attachment != null) {
      MultipartBody.Part attachmentPart = RetrofitUtility.prepareFilePart(PARAM_NEW_ATTACHMENTS,
          attachment, MediaType.ANY_IMAGE_TYPE.toString());
      attachmentParts = new MultipartBody.Part[] { attachmentPart };
    }
    ListenableFuture<ExpenseData> submitLf = expenseService.submitExpenseRevision(expenseId,
        procTaskId, comment, outletId, statementDate, expenseDate, expenseItemId, bookingId, amount,
        quantity,
        description, attachmentIds, attachmentParts, checkTs);
    Futures.addCallback(submitLf, new FutureCallback<ExpenseData>() {
      @Override
      public void onSuccess(@NullableDecl ExpenseData result) {
        notifySuccess("Submit Revisi Biaya Operasional berhasil", result);
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = null;
        if (HttpException.class.isAssignableFrom(t.getClass())) {
          er = RetrofitUtility.parseErrorBody(
              ((HttpException) t).response().errorBody());
        }
        notifyFailure(er == null ? "Submit Revisi Biaya Operasional gagal" : null, er, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<ExpenseData> result) {
    listener.onExpenseSaveDraftStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<ExpenseData> result) {
    listener.onExpenseSaveDraftSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<ExpenseData> result) {
    listener.onExpenseSaveDraftFailure(result);
  }
}
