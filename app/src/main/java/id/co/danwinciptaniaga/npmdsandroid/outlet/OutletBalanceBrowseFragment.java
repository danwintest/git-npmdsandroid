package id.co.danwinciptaniaga.npmdsandroid.outlet;

import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.app.PendingIntent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.UiThread;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.navigation.Navigation;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.outlet.OutletBalanceData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentOutletBalanceBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListLoadStateAdapter;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import kotlin.Unit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OutletBalanceBrowseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OutletBalanceBrowseFragment extends ProtectedFragment {
  public static final String TAG = OutletBalanceBrowseFragment.class.getSimpleName();

  private FragmentOutletBalanceBrowseBinding binding;
  private OutletBalanceBrowseVM viewModel;
  private OutletBalanceListAdapter adapter;
  private ConcatAdapter concatAdapter;
  private RecyclerView recyclerView;

  @Inject
  AppExecutors appExecutors;

  private View.OnClickListener restart = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // kita coba restart seluruh activity
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_outlet_list)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragment = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      refreshAdapter();
    }
  };

  private View.OnClickListener retryCallback = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // retry hanya mencoba lagi page yang gagal
      adapter.retry();
    }
  };

  public OutletBalanceBrowseFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment OutletInfoFragment.
   */
  public static OutletBalanceBrowseFragment newInstance() {
    OutletBalanceBrowseFragment fragment = new OutletBalanceBrowseFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    HyperLog.d(TAG, "onCreate called");
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    viewModel = new ViewModelProvider(requireActivity()).get(OutletBalanceBrowseVM.class);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    HyperLog.d(TAG, "onCreateView called");
    binding = FragmentOutletBalanceBrowseBinding.inflate(inflater, container, false);
    recyclerView = binding.list;
    adapter = new OutletBalanceListAdapter(getContext(), new OutletBalanceListAdapter.Listener() {
      @Override
      public void onBalanceReportClicked(View view, OutletBalanceData outletBalanceData,
          int position) {
        NavController navController = Navigation.findNavController(binding.getRoot());
        OutletBalanceBrowseFragmentDirections.ActionBalanceReportFragment action = OutletBalanceBrowseFragmentDirections.actionBalanceReportFragment(
            outletBalanceData.getId());
        navController.navigate(action, NavHelper.animParentToChild().build());
      }

      @Override
      public void onStampReport(View v, OutletBalanceData outletBalanceData, int position) {
        NavController navController = Navigation.findNavController(binding.getRoot());
        OutletBalanceBrowseFragmentDirections.ActionStockByDayReportFragment action = OutletBalanceBrowseFragmentDirections.actionStockByDayReportFragment(
            outletBalanceData.getId());
        navController.navigate(action, NavHelper.animParentToChild().build());
      }
    });

    // LoadState pada waktu paging di handle oleh header dan footer pada concateAdapter
    concatAdapter = adapter.withLoadStateHeaderAndFooter(
        new ExpenseListLoadStateAdapter(retryCallback),
        new ExpenseListLoadStateAdapter(retryCallback));
    // LoadState pertama kali (page pertama), tetap dihandle oleh adapter utama
    adapter.addLoadStateListener((combinedLoadStates) -> {
      // listener ini mengupdate LoadState di ViewModel
      viewModel.setOutletBalanceListLoadState(combinedLoadStates.getRefresh());
      return Unit.INSTANCE;
    });
    recyclerView.setAdapter(concatAdapter);

    viewModel.getRefreshList().observe(getViewLifecycleOwner(), e -> {
      if (e != null && e) {
        adapter.refresh();
      }
    });

    binding.swipeRefresh.setOnRefreshListener(() -> {
      refreshAdapter();
    });
    return binding.getRoot();
  }

  protected void refreshAdapter() {
    // reload fragment ini = memuat ulang seluruh list
    adapter.refresh();
    // karena menggunakan progress indicator sendiri, jadi  tidak perlu yg dari swipeRefresh
    binding.swipeRefresh.setRefreshing(false);
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    if (status.getStatus() == Status.LOADING) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      binding.swipeRefresh.setVisibility(View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.GONE);
      binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);
      // retry = restart fragment (karena authentication gagal)
      binding.progressWrapper.retryButton.setOnClickListener(restart);
    }
  }

  /**
   * Method mempersiapkan tampilan data utama setelah authentication Success.
   * Method ini akan terpanggil setiap kali onResume, mis: kembali dari halaman subreport.
   */
  @UiThread
  private void onReady() {
    HyperLog.d(TAG, "onReady called");
    // lalu gunakan LiveData observer untuk mengupdate view
    viewModel.getOutletBalanceListLoadState().observe(getViewLifecycleOwner(), state -> {
      HyperLog.d(TAG, "outletBalanceListLoadState updated: " + state);
      if (state instanceof LoadState.Loading) {
        // sedang loading pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
      } else if (state instanceof LoadState.Error) {
        // terjadi error pada waktu pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);
        // retry = reload semua data
        binding.progressWrapper.retryButton.setOnClickListener(reloadFragment);
        binding.progressWrapper.progressText.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            ((LoadState.Error) state).getError().getMessage());
        // sembunyikan list, karena retry akan dihandle oleh button di atas
        binding.swipeRefresh.setVisibility(View.GONE);
      } else if (state instanceof LoadState.NotLoading) {
        // sudah ok
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.swipeRefresh.setVisibility(View.VISIBLE);
      }
    });
    // connect data ke adapter
    // supaya tidak hit server lagi pada waktu kembali dari subreport, getOutletBalanceList hanya load data kalau
    // outletBalanceList masih null
    viewModel.getOutletBalanceList().observe(getViewLifecycleOwner(), pagingData -> {
      adapter.submitData(getLifecycle(), pagingData);
    });
  }
}