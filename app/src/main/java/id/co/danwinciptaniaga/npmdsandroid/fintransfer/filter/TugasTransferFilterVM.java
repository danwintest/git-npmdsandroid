package id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.CompanyData;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBatchListFilterData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferFilterResponse;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferJenisTransaksiFilterData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferTransferStatusFilterData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferTransferTyperListFilterData;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;

public class TugasTransferFilterVM extends AndroidViewModel
    implements GetTugasTransferFilterUC.Listener {
  private final static String TAG = TugasTransferFilterVM.class.getSimpleName();
  private final GetTugasTransferFilterUC uc;
  private MutableLiveData<FormState> formState = new MutableLiveData<>();
  private TugasTransferBrowseAndroidFilter filterField = new TugasTransferBrowseAndroidFilter();
  private KeyValueModel<UUID, String> companyKvm = null;
  private MutableLiveData<List<KeyValueModel<UUID, String>>> companyKvmList = new MutableLiveData<>();
  private KeyValueModel<String, String> tranfeTyperKvm = null;
  private MutableLiveData<List<KeyValueModel<String, String>>> transferTypeKvmList = new MutableLiveData<>();
  private KeyValueModel<UUID, String> rekeningSumberKvm = null;
  private MutableLiveData<List<KeyValueModel<UUID, String>>> rekenginSumberKvmList = new MutableLiveData<>();
  private KeyValueModel<String, String> batchKvm = null;
  private MutableLiveData<List<KeyValueModel<String, String>>> batchKvmList = new MutableLiveData<>();
  private KeyValueModel<String, String> transferStatusKvm = null;
  private MutableLiveData<List<KeyValueModel<String, String>>> transferStatusKvmList = new MutableLiveData<>();
  private KeyValueModel<String, String> jenisTransaksiKvm = null;
  private MutableLiveData<List<KeyValueModel<String, String>>> jenisTransaksiKvmList = new MutableLiveData<>();
  private Boolean group = false;

  @ViewModelInject
  public TugasTransferFilterVM(@NonNull Application app, GetTugasTransferFilterUC uc) {
    super(app);
    this.uc = uc;
    this.uc.registerListener(this);
  }

  public MutableLiveData<FormState> getFormState() {
    return formState;
  }

  public TugasTransferBrowseAndroidFilter getFilterField() {
    return filterField;
  }

  public void setFilterField(
      TugasTransferBrowseAndroidFilter filterField) {
    this.filterField = filterField;
  }

  public Boolean isGroup() {
    return group;
  }

  public void setGroup(Boolean group) {
    this.group = group;
  }

  public KeyValueModel<UUID, String> getCompanyKvm() {
    return companyKvm;
  }

  public void setCompanyKvm(
      KeyValueModel<UUID, String> companyKvm) {
    this.companyKvm = companyKvm;
  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getCompanyKvmList() {
    return companyKvmList;
  }

  public void setCompanyKvmList(List<CompanyData> cdList) {
    List<KeyValueModel<UUID, String>> kvmCompanyList = new ArrayList<>();
    for (CompanyData cd : cdList) {
      KeyValueModel<UUID, String> kvmCompany = new KeyValueModel<>(cd.getId(), cd.getCompanyName());
      if (cd.getId().equals(getFilterField().getCompanyId())) {
        setCompanyKvm(kvmCompany);
      }
      kvmCompanyList.add(kvmCompany);
    }
    this.companyKvmList.postValue(kvmCompanyList);
  }

  public MutableLiveData<List<KeyValueModel<String, String>>> getTransferTypeKvmList() {
    return transferTypeKvmList;
  }

  public void setTransferTypeKvmList(List<TugasTransferTransferTyperListFilterData> tTypeList) {
    List<KeyValueModel<String, String>> kvmTTypeList = new ArrayList<>();
    for (TugasTransferTransferTyperListFilterData tType : tTypeList) {
      KeyValueModel<String, String> kvmTType = new KeyValueModel<>(tType.getId(), tType.getName());
      if (tType.getId().equals(getFilterField().getTransferType())) {
        setTranfeTyperKvm(kvmTType);
      }
      kvmTTypeList.add(kvmTType);
    }
    this.transferTypeKvmList.postValue(kvmTTypeList);
  }

  public KeyValueModel<String, String> getTranfeTyperKvm() {
    return tranfeTyperKvm;
  }

  public void setTranfeTyperKvm(KeyValueModel<String, String> tranfeTyperKvm) {
    this.tranfeTyperKvm = tranfeTyperKvm;
  }

  public KeyValueModel<UUID, String> getRekeningSumberKvm() {
    return rekeningSumberKvm;
  }

  public void setRekeningSumberKvm(KeyValueModel<UUID, String> rekeningSumberKvm) {
    this.rekeningSumberKvm = rekeningSumberKvm;
  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getRekenginSumberKvmList() {
    return rekenginSumberKvmList;
  }

  public void setRekenginSumberKvmList(List<BankAccountData> badList) {
    List<KeyValueModel<UUID, String>> kvmList = new ArrayList<>();
    for (BankAccountData bad : badList) {
      String bankName = bad.getBankName() != null ? bad.getBankName() : bad.getOtherBankName();
      String accNo = bad.getAccountNo();
      String val = bankName + " - " + accNo;
      KeyValueModel<UUID, String> kvm = new KeyValueModel(bad.getId(), val);
      if (bad.getId().equals(filterField.getSourceAccountId()))
        setRekeningSumberKvm(kvm);
      kvmList.add(kvm);
    }
    this.rekenginSumberKvmList.postValue(kvmList);
  }

  public KeyValueModel<String, String> getBatchKvm() {
    return batchKvm;
  }

  public void setBatchKvm(KeyValueModel<String, String> batchKvm) {
    this.batchKvm = batchKvm;
  }

  public MutableLiveData<List<KeyValueModel<String, String>>> getBatchKvmList() {
    return batchKvmList;
  }

  public void setBatchKvmList(List<TugasTransferBatchListFilterData> batchList) {
    List<KeyValueModel<String, String>> kvmList = new ArrayList<>();
    KeyValueModel<String, String> kvmBlank = new KeyValueModel<>(null, null);
    kvmList.add(kvmBlank);
    for (TugasTransferBatchListFilterData batch : batchList) {
      KeyValueModel<String, String> kvm = new KeyValueModel<>(batch.getId(), batch.getValue());
      if (batch.getId().equals(filterField.getBatchId()))
        setBatchKvm(kvm);
      kvmList.add(kvm);
    }
    this.batchKvmList.postValue(kvmList);
  }

  public KeyValueModel<String, String> getTransferStatusKvm() {
    return transferStatusKvm;
  }

  public void setTransferStatusKvm(KeyValueModel<String, String> transferStatusKvm) {
    this.transferStatusKvm = transferStatusKvm;
  }

  public MutableLiveData<List<KeyValueModel<String, String>>> getTransferStatusKvmList() {
    return transferStatusKvmList;
  }

  public void setTransferStatusKvmList(List<TugasTransferTransferStatusFilterData> dataList) {
    List<KeyValueModel<String, String>> kvmList = new ArrayList<>();
    KeyValueModel<String, String> kvmBlank = new KeyValueModel<>(null, null);
    kvmList.add(kvmBlank);
    for (TugasTransferTransferStatusFilterData data : dataList) {
      KeyValueModel<String, String> kvm = new KeyValueModel<>(data.getId(), data.getName());
      if (data.getId().equals(filterField.getTransferStatusId()))
        setTransferStatusKvm(kvm);
      kvmList.add(kvm);
    }
    this.transferStatusKvmList.postValue(kvmList);
  }

  public KeyValueModel<String, String> getJenisTransaksiKvm() {
    return jenisTransaksiKvm;
  }

  public void setJenisTransaksiKvm(KeyValueModel<String, String> jenisTransaksiKvm) {
    this.jenisTransaksiKvm = jenisTransaksiKvm;
  }

  public MutableLiveData<List<KeyValueModel<String, String>>> getJenisTransaksiKvmList() {
    return jenisTransaksiKvmList;
  }

  public void setJenisTransaksiKvmList(List<TugasTransferJenisTransaksiFilterData> dataList) {
    List<KeyValueModel<String, String>> kvmList = new ArrayList<>();
    KeyValueModel<String, String> kvmBlank = new KeyValueModel<>(null, null);
    kvmList.add(kvmBlank);
    for (TugasTransferJenisTransaksiFilterData data : dataList) {
      KeyValueModel<String, String> kvm = new KeyValueModel<>(data.getId(), data.getName());
      if (data.getId().equals(filterField.getTransactionTypeId()))
        setJenisTransaksiKvm(kvm);
      kvmList.add(kvm);
    }
    this.jenisTransaksiKvmList.postValue(kvmList);
  }

  public void setCompanyFilter(UUID companyId, String companyName) {
    filterField.setCompanyId(companyId);
    filterField.setCompanyName(companyName);
    uc.getFilterField(filterField.getCompanyId(), true);
  }

  public void setJenisTransferFilter(String transferType, String transferTypeName) {
    filterField.setTransferType(transferType);
    filterField.setTransferName(transferTypeName);
  }

  public void setRekeningSumberFilter(UUID id, String bankName) {
    filterField.setSourceAccountId(id);
    filterField.setSourceAccountName(bankName);
  }

  public void setNoTrx(String noTrx){
    filterField.setTransactionNo(noTrx);
  }

  public void setPendingTaskStatusFilter(boolean isTrue) {
    filterField.setPendingTask(isTrue);
  }

  public void setBatchFilter(String id, String batch) {
    filterField.setBatchId(id);
    filterField.setBatchName(batch);
  }

  public void setTransferStatusFilter(String id, String name) {
    filterField.setTransferStatusId(id);
    filterField.setTransferStatusName(name);
  }

  public void setJenisTransaksiFilter(String id, String name) {
    filterField.setTransactionTypeId(id);
    filterField.setTransactionTypeName(name);
  }

  public void setEndDate(Date date) {
    HyperLog.d(TAG, "setEndDate date->" + date + "<-");
    filterField.setStartDateTo(date);
  }

  public void setStartDate(Date date) {
    HyperLog.d(TAG, "setStartDate date->" + date + "<-");
    filterField.setStartDateFrom(date);
  }

  public void setOutletFilter(String name) {
    filterField.setOutletName(name);
  }

  public void processLoadFilterData() {
    if (isGroup())
      uc.getFilterField(filterField.getCompanyId(), false);
  }

  @Override
  public void onProcessStarted(Resource<TugasTransferFilterResponse> loading) {
    formState.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessSuccess(Resource<TugasTransferFilterResponse> res) {
    boolean isReloadSourceAcc = res.getData().getReloadSourceAcc();
    if (!isReloadSourceAcc) {
      // companyList
      List<CompanyData> cdList = res.getData().getCompanyDataList();
      setCompanyKvmList(cdList);

      // transferTypelist
      List<TugasTransferTransferTyperListFilterData> tTypeList = res.getData().getTransferTypeList();
      setTransferTypeKvmList(tTypeList);

      // batchList
      List<TugasTransferBatchListFilterData> batchList = res.getData().getBatchList();
      setBatchKvmList(batchList);

      // transferStatus
      List<TugasTransferTransferStatusFilterData> ttsList = res.getData().getTransferStatusList();
      setTransferStatusKvmList(ttsList);

      // jenisTransaksi
      List<TugasTransferJenisTransaksiFilterData> jtList = res.getData().getJenisTransaksiList();
      setJenisTransaksiKvmList(jtList);
    }
    // rekening sumber
    List<BankAccountData> badList = res.getData().getSourceAccList();
    setRekenginSumberKvmList(badList);

    formState.postValue(FormState.ready(res.getMessage()));
  }

  @Override
  public void onProcessFailure(Resource<TugasTransferFilterResponse> res) {
    formState.postValue(FormState.error(res.getMessage()));
  }
}
