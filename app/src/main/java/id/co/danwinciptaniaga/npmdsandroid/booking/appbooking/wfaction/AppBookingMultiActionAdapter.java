package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import com.hypertrack.hyperlog.HyperLog;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class AppBookingMultiActionAdapter
    extends RecyclerView.Adapter<AppBookingMultiActionAdapter.ViewHolder> {
  private final String TAG = AppBookingMultiActionAdapter.class.getSimpleName();
  private List<AppBookingData> dataList = new ArrayList<>();

  @Inject
  public AppBookingMultiActionAdapter(List<AppBookingData> dataList) {
    this.dataList = dataList;
  }

  @NonNull
  @Override
  public AppBookingMultiActionAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
      int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(
        R.layout.fragment_appbooking_multi_action_item, parent, false);
    return new AppBookingMultiActionAdapter.ViewHolder(v);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
  }

  AppBookingData getItem(int pos) {
    return dataList.get(pos);
  }

  @Override
  public int getItemCount() {
    return dataList.size();
  }

  public List<AppBookingData> getDataList() {
    return this.dataList;
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    //    public final FragmentAppbookingMultiActionItemBinding binding;

    public final View mView;
    public final ConstraintLayout mContainer;
    public final TextView mTrxNo;
    public final TextView mStatus;
    public final TextView mOutlet;
    public final TextView mBookingInfo;
    public final TextView mConsInfo;
    public final TextView mPengajuan;
    public final CheckBox mCbOpTransfer;
    public AppBookingData mItem;

    public ViewHolder(View v) {
      super(v);
      mView = v;
      mContainer = (ConstraintLayout) v.findViewById(R.id.clAppBookingContainer);
      mTrxNo = (TextView) v.findViewById(R.id.tvAppBookingTrxNo);
      mStatus = (TextView) v.findViewById(R.id.tvAppBookingStatus);
      mOutlet = (TextView) v.findViewById(R.id.tvAppBookingOutlet);
      mBookingInfo = (TextView) v.findViewById(R.id.tvAppBookingBdatePencairanType);
      mConsInfo = (TextView) v.findViewById(R.id.tvAppBookingConsData);
      mPengajuan = (TextView) v.findViewById(R.id.tvAppBookingPengajuan);
      mCbOpTransfer = (CheckBox) v.findViewById(R.id.cbOperasionalTransfer);
    }

    public void bind(AppBookingData data, int pos) {
      setObject(data);
      setObjectListener(pos);
    }

    private void setObjectListener(int pos) {
      mCbOpTransfer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          AppBookingData data = dataList.get(pos);
          data.setOperationalTransfer(isChecked);
          dataList.set(pos,data);
        }
      });

    }

    private void setObject(AppBookingData data) {
      boolean isCash = Objects.equals(data.getBookingTypeId(), Utility.BOOKING_TYPE_CASH);
      HyperLog.d(TAG, "isCash[" + isCash + "] isOpt[" + data.getOperationalTransfer() + "]");
      if (isCash)
        mCbOpTransfer.setVisibility(View.GONE);

      String trxNo = data.getTrxNo() != null ? data.getTrxNo() : "-";
      String status = data.getWfName() != null ? data.getWfName() : "-";

      String outletNameTypeCode = data.getOutletObj().getOutletName();

      String bookingDatePencairan = data.getBookingDate() != null
          ?
          data.getBookingDate().format(Formatter.DTF_dd_MM_yyyy) + " (" + data.getBookingTypeName()
              + ")" :
          "-";
      String consumerNo = data.getConsumerNo() != null ? data.getConsumerNo() : "-";
      String consumerName = data.getConsumerName() != null ? data.getConsumerName() : "-";
      String vehicleNo = (data.getVehicleNo() != null && !data.getVehicleNo().isEmpty() ?
          " - " + data.getVehicleNo() : "");
      String consumerInfo = consumerNo + " - " + consumerName + vehicleNo;

      String pengajuanInfo = (data.getPengajuanDate() != null ?
          Formatter.SDF_dd_MM_yyyy.format(data.getPengajuanDate()) :
          " - ") + " (" + ((data.getAppTypeName() != null ? data.getAppTypeName() : " - ")) + ")";

      mTrxNo.setText(trxNo);
      mStatus.setText(status);
      mOutlet.setText(outletNameTypeCode);
      mBookingInfo.setText(bookingDatePencairan);
      mConsInfo.setText(consumerInfo);
      mPengajuan.setText(pengajuanInfo);
      mCbOpTransfer.setChecked(data.getOperationalTransfer());
    }
  }
}
