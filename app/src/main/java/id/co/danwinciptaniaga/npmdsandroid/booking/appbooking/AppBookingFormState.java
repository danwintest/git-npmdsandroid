package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;

public class AppBookingFormState {
  private String message;
  private State state;
  private boolean isFromAction;
  private AppBookingCashScreenMode screenModeCash;
  private AppBookingTransferScreenMode screenModeTransfer;

  public AppBookingFormState(State state, String message, boolean isFromAction,AppBookingCashScreenMode screenModeCash, AppBookingTransferScreenMode screenModeTransfer) {
    this.state = state;
    this.message = message;
    this.isFromAction = isFromAction;
    this.screenModeCash = screenModeCash;
    this.screenModeTransfer = screenModeTransfer;
  }

  public static AppBookingFormState loading(String message) {
    return new AppBookingFormState(State.LOADING, message, false, null, null);
  }

  /**
   *
   * @param message
   *          Pesan yang akan ditampilkan pada saat ready
   * @param isFromAction
   *          jika TRUE: maka pada saat status READY, maka halaman akan di navigateUp dan reload.
   *          jika FALSE: maka akan stay di halaman ini.
   * @return
   */
  public static AppBookingFormState ready(String message, boolean isFromAction,
      AppBookingCashScreenMode screenModeCash, AppBookingTransferScreenMode screenModeTransfer) {
    return new AppBookingFormState(State.READY, message, isFromAction, screenModeCash,
        screenModeTransfer);
  }

  public static AppBookingFormState inProgress(String message) {
    return new AppBookingFormState(State.ACTION_IN_PROGRESS, message, false, null, null);
  }

  public static AppBookingFormState errorProcess(String message) {
    return new AppBookingFormState(State.ERROR_PROCESS, message, false, null, null);
  }

  public static AppBookingFormState errorLoadData(String message) {
    return new AppBookingFormState(State.ERROR_LOAD_DATA, message, false,null, null);
  }

  public String getMessage() {
    return message;
  }

  public State getState() {
    return state;
  }

  public boolean isFromAction() {
    return this.isFromAction;
  }

  public AppBookingCashScreenMode getScreenModeCash(){
    return screenModeCash;
  }

  public AppBookingTransferScreenMode getScreenModeTransfer() {
    return screenModeTransfer;
  }

  public enum State {
    LOADING, READY, ACTION_IN_PROGRESS, ERROR_PROCESS, ERROR_LOAD_DATA
  }
}
