package id.co.danwinciptaniaga.npmdsandroid.droppingday.filter;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.chip.Chip;
import com.hypertrack.hyperlog.HyperLog;
import com.tiper.MaterialSpinner;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentDroppingDailyFilterBinding;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;

@AndroidEntryPoint
public class DroppingDailyFilterFragment extends BottomSheetDialogFragment {
  private static final String TAG = DroppingDailyFilterFragment.class.getSimpleName();
  public static final String DROPPING_DAILY_FILTER = "DROPPING_DAILY_FILTER";
  private FragmentDroppingDailyFilterBinding binding;
  private DroppingDailyFilterVM vm;
  protected ArrayAdapter<KeyValueModel<String, String>> transferStatusAdapter;
  private ArrayAdapter<KeyValueModel<String, String>> statusAdapter;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    HyperLog.d(TAG, "onCreateView terpanggil");
    binding = FragmentDroppingDailyFilterBinding.inflate(inflater);
    DroppingDailyFilterFragmentArgs args = DroppingDailyFilterFragmentArgs.fromBundle(
        getArguments());
    vm = new ViewModelProvider(this).get(DroppingDailyFilterVM.class);
    vm.setFilterField(args.getFilterField());
    setFormState();
    vm.getFilterData();
    setFieldNoTrx();
    setFieldOutlet();
    setFieldDroppingDateFrom();
    setFieldDroppingDateTo();
    setFieldStatus(inflater);
    setFieldTransferField();
    setButton();
    return binding.getRoot();
  }

  public void setFormState() {
    vm.getFormState().observe(getViewLifecycleOwner(), state -> {
      switch (state.getState()) {
      case LOADING:
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
        break;
      case READY:
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
        break;
      case ERROR:
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
        Toast.makeText(getContext(), state.getMessage(), Toast.LENGTH_LONG).show();
        break;
      default:
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(state.getMessage());
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
        break;
      }
    });
  }

  public void setFieldNoTrx() {
    binding.etTrxNo.setText(
        vm.getFilterField() != null ? vm.getFilterField().getTransactionNo() : null);
    binding.etTrxNo.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        vm.setTrxno(s.toString());
      }
    });
  }

  public void setFieldOutlet() {
    binding.etOutletName.setText(
        vm.getFilterField() != null ? vm.getFilterField().getOutletName() : null);
    binding.etOutletName.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        vm.setOutletName(s.toString());
      }
    });
  }

  public void setFieldDroppingDateFrom() {
    if (vm.getFilterField().getDroppingDateFrom() != null) {
      binding.etDroppingDateFrom.setText(
          vm.getFilterField().getDroppingDateFrom().format(Formatter.DTF_dd_MM_yyyy));
    }

    binding.etDroppingDateFrom.setOnClickListener(v -> {
      LocalDate dateVal = vm.getFilterField().getDroppingDateFrom() != null ?
          vm.getFilterField().getDroppingDateFrom() :
          LocalDate.now();
      int day = dateVal.getDayOfMonth();
      int month = dateVal.getMonthValue() - 1;
      int year = dateVal.getYear();

      DatePickerDialog dpd = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate dateVal = LocalDate.of(year, month + 1, dayOfMonth);
              vm.setDroppingDateFrom(dateVal);
              binding.etDroppingDateFrom.setText(dateVal.format(Formatter.DTF_dd_MM_yyyy));
            }
          }, year, month, day);
      dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          vm.setDroppingDateFrom(null);
          binding.etDroppingDateFrom.setText("");
        }
      });
      dpd.show();
    });

  }

  public void setFieldDroppingDateTo() {
    if (vm.getFilterField().getDroppingDateTo() != null) {
      binding.etDroppingDateTo.setText(
          vm.getFilterField().getDroppingDateTo().format(Formatter.DTF_dd_MM_yyyy));
    }

    binding.etDroppingDateTo.setOnClickListener(v -> {
      LocalDate dateVal = vm.getFilterField().getDroppingDateTo() != null ?
          vm.getFilterField().getDroppingDateTo() :
          LocalDate.now();
      int day = dateVal.getDayOfMonth();
      int month = dateVal.getMonthValue() - 1;
      int year = dateVal.getYear();

      DatePickerDialog dpd = new DatePickerDialog(getActivity(),
          new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
              LocalDate dateVal = LocalDate.of(year, month + 1, dayOfMonth);
              vm.setDroppingDateTo(dateVal);
              binding.etDroppingDateTo.setText(dateVal.format(Formatter.DTF_dd_MM_yyyy));
            }
          }, year, month, day);
      dpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
        @Override
        public void onCancel(DialogInterface dialog) {
          vm.setDroppingDateTo(null);
          binding.etDroppingDateTo.setText("");
        }
      });
      dpd.show();
    });
  }

  public void setFieldStatus(@NonNull LayoutInflater inflater) {
    vm.getStatusKvmList().observe(getViewLifecycleOwner(), dataList -> {
      statusAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      statusAdapter.addAll(dataList);
      binding.acStatusWf.setAdapter(statusAdapter);
      binding.acStatusWf.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
          KeyValueModel<String, String> statusKvm = (KeyValueModel<String, String>) parent.getItemAtPosition(
              position);
          vm.setSelectedStatusKvmList(true, statusKvm);
          binding.acStatusWf.setText(null);
        }
      });
    });

    vm.getSelectedStatusKvmList().observe(getViewLifecycleOwner(), dataList -> {
      binding.cgStatusWf.removeAllViews();
      for (int i = 0; i < dataList.size(); i++) {
        KeyValueModel<String, String> kvm = dataList.get(i);
        Chip chip = (Chip) inflater.inflate(R.layout.chip_filter, null);
        chip.setId(i);
        chip.setText(kvm.getValue());
        chip.setTag(kvm.getKey());
        chip.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            KeyValueModel<String, String> selectedItem = new KeyValueModel(chip.getTag(),
                chip.getText());
            vm.setSelectedStatusKvmList(false, selectedItem);
          }
        });
        binding.cgStatusWf.addView(chip);
      }
    });
  }

  public void setFieldTransferField() {
    vm.getTransferStatusKvmList().observe(getViewLifecycleOwner(), dataList -> {
      transferStatusAdapter = new ArrayAdapter(getContext(), android.R.layout.select_dialog_item);
      binding.spTransferStatus.setAdapter(transferStatusAdapter);
      binding.spTransferStatus.setFocusable(false);
      transferStatusAdapter.addAll(dataList);
      transferStatusAdapter.notifyDataSetChanged();

      int position = transferStatusAdapter.getPosition(vm.getTransferStatusKvm());
      binding.spTransferStatus.setSelection(position);

      binding.spTransferStatus.setOnItemSelectedListener(
          new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(@NotNull MaterialSpinner materialSpinner,
                @Nullable View view,
                int i, long l) {
              KeyValueModel<String, String> dtKvm = transferStatusAdapter.getItem(i);
              vm.setTransferStatusKvm(dtKvm);
              vm.setTransferStatus(transferStatusAdapter.getItem(i).getKey());
            }

            @Override
            public void onNothingSelected(@NotNull MaterialSpinner materialSpinner) {
              vm.setTransferStatus(null);
            }
          });
    });

  }

  public void setButton() {
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Set<String> selectedStatus1 = new HashSet<>();
        if (vm.getSelectedStatusKvmList().getValue() != null) {
          for (KeyValueModel<String, String> selectedStatus2 : vm.getSelectedStatusKvmList().getValue()) {
            selectedStatus1.add(selectedStatus2.getKey());
          }
        }
        DroppingBrowseFilter filterField = vm.getFilterField();
        filterField.setStatus(selectedStatus1);
        NavController nc = NavHostFragment.findNavController(DroppingDailyFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(DROPPING_DAILY_FILTER,
            filterField);
        nc.popBackStack();
      }
    });

    binding.btnClearAll.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        NavController nc = NavHostFragment.findNavController(DroppingDailyFilterFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(DROPPING_DAILY_FILTER,
            new DroppingBrowseFilter());
        nc.popBackStack();
      }
    });
  }
}
