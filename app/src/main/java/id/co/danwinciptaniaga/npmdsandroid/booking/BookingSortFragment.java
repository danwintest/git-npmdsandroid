package id.co.danwinciptaniaga.npmdsandroid.booking;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingSort;
import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseSort;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingSortFragment;
import id.co.danwinciptaniaga.npmdsandroid.common.SortPropertyAdapter;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentBookingSortBinding;
import id.co.danwinciptaniaga.npmdsandroid.sort.SortPropertyEntry;

public class BookingSortFragment extends BottomSheetDialogFragment {
  private static final String TAG = BookingSortFragment.class.getSimpleName();
  public static final String BOOKING_SORT_FIELDS = "BOOKING_SORT_FIELDS";
  private static Map<BookingBrowseSort.Field, Integer> labelMap = new HashMap<>();
  FragmentBookingSortBinding binding;

  static {
    labelMap.put(BookingBrowseSort.Field.CONSUMER_NO, R.string.consumer_no);
    labelMap.put(BookingBrowseSort.Field.CONSUMER_NAME, R.string.consumer_name);
    labelMap.put(BookingBrowseSort.Field.BOOKINGDATE, R.string.booking_date);
    labelMap.put(BookingBrowseSort.Field.APPROVAL_DATE, R.string.approval_date);
    labelMap.put(BookingBrowseSort.Field.SCHEME_FEE, R.string.feeScheme);
    labelMap.put(BookingBrowseSort.Field.UPDATED, R.string.update_ts);
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentBookingSortBinding.inflate(inflater, container, false);
    BookingSortFragmentArgs args = BookingSortFragmentArgs.fromBundle(getArguments());
    String soListString = args.getSortField().replace("\\","");
    Type dataType = new TypeToken<List<SortOrder>>() {
    }.getType();
    List<SortOrder> soList = new Gson().fromJson(soListString, dataType);
    Set<SortOrder> sso = new HashSet<SortOrder>(soList);
    setupSortPropertyList(sso);
    setupButtons();
    return binding.getRoot();
  }

  private void setupSortPropertyList(Set<SortOrder> sso) {
    final List<SortPropertyEntry> speSet = new ArrayList<>();
    final Map<SortOrder, SortPropertyEntry> map = new HashMap<>();
    for (BookingBrowseSort.Field field : BookingBrowseSort.Field.values()) {
      Integer labelId = labelMap.get(field);

      if(field.getName().equals(BookingBrowseSort.Field.UPDATED.getName()))
        continue;

      if(field.getName().equals(BookingBrowseSort.Field.SCHEME_FEE.getName()))
        continue;

      Preconditions.checkNotNull(labelId,
          String.format("Property label for Sort not found: %s", field.getName()));
      SortOrder isPresentSo = null;
      for (SortOrder so : sso) {
        if (so.getProperty().equals(field.getName())) {
          isPresentSo = so;
          break;
        }
      }
      boolean isSelected;
      boolean isOn = false;
      if (isPresentSo != null) {
        isSelected = true;
        isOn = SortOrder.Direction.ASC.equals(isPresentSo.getDirection()); // on = ASC
      } else {
        isSelected = false;
      }

      SortPropertyEntry spe = new SortPropertyEntry(field.getName(), labelId, isSelected, isOn);
      if (isPresentSo != null) {
        map.put(isPresentSo, spe);
      }
      speSet.add(spe);
    }
    Set<SortPropertyEntry> speInOrder = sso.stream().map(
        sf -> map.get(sf)).collect(Collectors.toSet());

    SortPropertyAdapter spa = new SortPropertyAdapter(speSet, speInOrder);
    binding.fieldList.setAdapter(spa);
  }

  private void setupButtons(){
    binding.btnApply.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        SortPropertyAdapter adapter = (SortPropertyAdapter) binding.fieldList.getAdapter();
        Set<SortPropertyEntry> selectedSpeSet = adapter.getPropertySelection();
        Set<SortOrder> sortOrderSet = new LinkedHashSet<>();
        selectedSpeSet.stream().forEach(spe -> {
          BookingBrowseSort.Field f = BookingBrowseSort.Field.fromId(spe.getName());
          sortOrderSet.add(BookingBrowseSort.sortBy(f,
              spe.isOn() ? SortOrder.Direction.ASC : SortOrder.Direction.DESC));
        });
        List<SortOrder> soList = new ArrayList(sortOrderSet);
        String soListJson = new Gson().toJson(soList);

        NavController nc = NavHostFragment.findNavController(BookingSortFragment.this);
        nc.getPreviousBackStackEntry().getSavedStateHandle().set(
            BOOKING_SORT_FIELDS, soListJson);
        nc.popBackStack();
      }
    });
  }
}