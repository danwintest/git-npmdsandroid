package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import java.util.List;
import java.util.concurrent.Future;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingMultipeSelectedData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingMultipleSelectedProcessResultData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.AppBookingService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class AppBookingMultiActionUC extends
    BaseObservable<AppBookingMultiActionUC.Listener, AppBookingMultipleSelectedProcessResultData> {
  private final String TAG = AppBookingMultiActionUC.class.getSimpleName();
  private final AppBookingService service;
  private final AppExecutors appExecutors;
  private final Application app;
  private final String doneMsg;
  private final String failMsg;

  @Override
  protected void doNotifyStart(Listener listener,
      Resource<AppBookingMultipleSelectedProcessResultData> result) {
    listener.onProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener,
      Resource<AppBookingMultipleSelectedProcessResultData> result) {
    listener.onProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener,
      Resource<AppBookingMultipleSelectedProcessResultData> result) {
    listener.onProcessFailure(result);
  }

  public interface Listener {
    void onProcessStarted(Resource<AppBookingMultipleSelectedProcessResultData> loading);

    void onProcessSuccess(Resource<AppBookingMultipleSelectedProcessResultData> response);

    void onProcessFailure(Resource<AppBookingMultipleSelectedProcessResultData> response);
  }

  @Inject
  public AppBookingMultiActionUC(Application app, AppBookingService service,
      AppExecutors appExecutors) {
    super(app);
    this.service = service;
    this.app = app;
    this.appExecutors = appExecutors;
    doneMsg = this.app.getString(R.string.process_success);
    failMsg = this.app.getString(R.string.process_fail);
  }

  public void processMultiApprove(List<AppBookingMultipeSelectedData> dataList) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    String dataJson = new Gson().toJson(dataList);
    ListenableFuture<AppBookingMultipleSelectedProcessResultData> process = service.processMultiApprovalCaptain(
        dataJson);
    Futures.addCallback(process, new FutureCallback<AppBookingMultipleSelectedProcessResultData>() {
      @Override
      public void onSuccess(@NullableDecl AppBookingMultipleSelectedProcessResultData result) {
        notifySuccess(doneMsg, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(failMsg + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processMultiReturn(List<AppBookingMultipeSelectedData> dataList) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    String dataJson = new Gson().toJson(dataList);
    ListenableFuture<AppBookingMultipleSelectedProcessResultData> process = service.processMultiReturnCaptain(
        dataJson);
    Futures.addCallback(process, new FutureCallback<AppBookingMultipleSelectedProcessResultData>() {
      @Override
      public void onSuccess(@NullableDecl AppBookingMultipleSelectedProcessResultData result) {
        notifySuccess(doneMsg, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(failMsg + message, null, t);
      }
    }, appExecutors.networkIO());
  }
}
