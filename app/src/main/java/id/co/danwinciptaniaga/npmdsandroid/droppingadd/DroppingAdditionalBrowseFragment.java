package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypertrack.hyperlog.HyperLog;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.PermissionHelper;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentDroppingAdditionalBrowseBinding;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.filter.DroppingAdditionalFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.droppingadd.sort.DroppingAdditionalSortFragment;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListLoadStateAdapter;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;
import kotlin.Unit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DroppingAdditionalBrowseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
public class DroppingAdditionalBrowseFragment extends ProtectedFragment {
  private static final String TAG = DroppingAdditionalBrowseFragment.class.getSimpleName();

  public static final String SAVED_STATE_LD_REFRESH = "refresh";

  private static final String ARG_DROPPING_ADD_DATA = "droppingAdditionalData";

  private FragmentDroppingAdditionalBrowseBinding binding;
  private DroppingAdditionalBrowseViewModel viewModel;
  private DroppingAdditionalListAdapter adapter;
  private ConcatAdapter concatAdapter;
  private RecyclerView recyclerView;
  private ActionMode actionMode;
  private ActionModeCallback actionModeCallback;
  boolean isPermissionCreate = false;

  @Inject
  CommonService commonService;
  @Inject
  AppExecutors appExecutors;

  private View.OnClickListener restart = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // kita coba restart seluruh activity
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_dropping_additional_browse)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragment = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      refreshAdapter();
    }
  };

  private View.OnClickListener retryCallback = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // retry hanya mencoba lagi page yang gagal
      adapter.retry();
    }
  };

  public DroppingAdditionalBrowseFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param dad
   * @return A new instance of fragment DroppingAdditionalBrowseFragment.
   */
  public static DroppingAdditionalBrowseFragment newInstance(DroppingAdditionalData dad) {
    // sebetulnya saat ini tidak diketahui kapan method ini dipakai
    DroppingAdditionalBrowseFragment fragment = new DroppingAdditionalBrowseFragment();
    Bundle args = new Bundle();
    args.putSerializable(ARG_DROPPING_ADD_DATA, dad);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);

    viewModel = new ViewModelProvider(requireActivity())
        .get(DroppingAdditionalBrowseViewModel.class);
    if (getArguments() != null) {
      DroppingAdditionalBrowseFragmentArgs args = DroppingAdditionalBrowseFragmentArgs.fromBundle(
          getArguments());
      if (args.getDroppingAdditionalData() != null) {
        // PagingLibrary sepertinya tidak memungkinkan penambahan 1 entry Data secara manual ke list-nya.
        // Jadi workaround-nya, kita refresh seluruh list
        viewModel.setRefreshList(true);
      }
    }
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.expense_browse_menu, menu);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentDroppingAdditionalBrowseBinding.inflate(inflater, container, false);
    recyclerView = binding.list;
    actionModeCallback = new ActionModeCallback();
    adapter = new DroppingAdditionalListAdapter(
        new DroppingAdditionalListAdapter.DadRecyclerViewAdapterListener() {
          @Override
          public void onItemClicked(View view, DroppingAdditionalData dad, int position) {
            if (adapter.getSelectedItemCount() > 0) {
              enableActionMode(position);
            } else {
              NavController navController = Navigation.findNavController(view);
              DroppingAdditionalBrowseFragmentDirections.ActionDroppingAdditionalEditFragment dir =
                  DroppingAdditionalBrowseFragmentDirections.actionDroppingAdditionalEditFragment();
              dir.setDroppingId(dad.getId());
              navController.navigate(dir, NavHelper.animParentToChild().build());
              if (actionMode != null)
                actionMode.finish();
            }
          }

          @Override
          public void onItemLongClickedListener(View v, DroppingAdditionalData dad, int position) {
            enableActionMode(position);
          }
        }, getActivity().getApplicationContext(), commonService, appExecutors);

    concatAdapter = adapter.withLoadStateHeaderAndFooter(
        new ExpenseListLoadStateAdapter(retryCallback),
        new ExpenseListLoadStateAdapter(retryCallback));
    adapter.addLoadStateListener((combinedLoadStates) -> {
      // listener ini mengupdate LoadState di ViewModel
      viewModel.setDroppingListLoadState(combinedLoadStates.getRefresh());
      return Unit.INSTANCE;
    });
    recyclerView.setAdapter(concatAdapter);

    viewModel.getRefreshList().observe(getViewLifecycleOwner(), e -> {
      if (e != null && e) {
        adapter.refresh();
      }
    });

    binding.swipeRefresh.setOnRefreshListener(() -> {
      refreshAdapter();
    });

    viewModel.getActionEvent().observe(getViewLifecycleOwner(), s -> {
      HyperLog.d(TAG, "getActionEvent: " + s + "<-");
      adapter.clearSelections();
      if (Status.SUCCESS.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
        adapter.refresh();
      } else if (Status.LOADING.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
        ViewUtil.setVisibility(binding.progressWrapper.progressText, View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            s.getMessage() == null ? getString(R.string.msg_please_wait) : s.getMessage());
        ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
      } else {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
      }
    });

    binding.fab.setOnClickListener(e -> {
      NavController navController = Navigation.findNavController(binding.getRoot());
      DroppingAdditionalBrowseFragmentDirections.ActionDroppingAdditionalEditFragment dir =
          DroppingAdditionalBrowseFragmentDirections.actionDroppingAdditionalEditFragment();
      navController.navigate(dir, NavHelper.animParentToChild().build());
    });

    NavController navController = NavHostFragment.findNavController(this);
    LiveData<Boolean> refreshLd = navController.getCurrentBackStackEntry()
        .getSavedStateHandle().getLiveData(SAVED_STATE_LD_REFRESH);
    refreshLd.observe(getViewLifecycleOwner(), refresh -> {
      HyperLog.d(TAG, "refresh signaled: " + refresh);
      // tidak bisa langsung adapter.refresh, sepertinya adapternya belum connect ke RecyclerView-nya
      // jadi kita tunda dengan sinyal melalui viewmodel
      viewModel.setRefreshList(refresh);
      navController.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(SAVED_STATE_LD_REFRESH);
    });
    return binding.getRoot();
  }

  protected void refreshAdapter() {
    // reload fragment ini = memuat ulang seluruh list
    adapter.refresh();
    // karena menggunakan progress indicator sendiri, jadi  tidak perlu yg dari swipeRefresh
    binding.swipeRefresh.setRefreshing(false);
  }

  private void enableActionMode(int position) {
    if (actionMode == null) {
      actionMode = getActivity().startActionMode(
          (android.view.ActionMode.Callback) actionModeCallback);
    }
    toggleSelection(position);
  }

  private void toggleSelection(int position) {
    adapter.toggleSelection(position);
    boolean isDeleteApproveActive = adapter.isDeleteApproveActionActive();
    boolean isRejectActive = adapter.isRejectActionActive();
    int count = adapter.getSelectedItemCount();
    actionMode.getMenu().findItem(R.id.action_delete).setVisible(isDeleteApproveActive);
    actionMode.getMenu().findItem(R.id.action_approve).setVisible(isDeleteApproveActive);
    actionMode.getMenu().findItem(R.id.action_reject).setVisible(isRejectActive);

    if (count == 0) {
      actionMode.finish();
    } else {
      actionMode.setTitle(String.valueOf(count));
      actionMode.invalidate();
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (item.getItemId() == R.id.menu_filter) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_dropping_additional_browse, nc.getCurrentDestination().getId()))
        return true;
      DroppingAdditionalBrowseFragmentDirections.ActionFilterFragment dir = DroppingAdditionalBrowseFragmentDirections.actionFilterFragment();
      dir.setFilterField(viewModel.getFilterField().getValue());
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    } else if (item.getItemId() == R.id.menu_sort) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_dropping_additional_browse, nc.getCurrentDestination().getId()))
        return true;
      List<SortOrder> soList = new ArrayList(viewModel.getSortFields());
      String soJson = new Gson().toJson(soList);
      DroppingAdditionalBrowseFragmentDirections.ActionSortFragment dir = DroppingAdditionalBrowseFragmentDirections.actionSortFragment();
      dir.setSortField(soJson);
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    }
    return false;
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    if (status.getStatus() == Status.LOADING) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      binding.swipeRefresh.setVisibility(View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.GONE);
      binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);
      // retry = restart fragment (karena authentication gagal)
      binding.progressWrapper.retryButton.setOnClickListener(restart);
    }
  }

  private void onReady() {
    // harusnya permission sudah ada kalau authenticationStatusUpdate SUCCESS
    Resource<List<PermissionInfo>> permissions = basemodel.getPermissionsLd().getValue();
    isPermissionCreate = PermissionHelper.hasPermission(permissions.getData(),
        PermissionHelper.PERMISSION_TYPE_ENTITY_OP, "npmds_Dropping:create");
    if (isPermissionCreate) {
      binding.fab.setVisibility(View.VISIBLE);
    } else {
      binding.fab.setVisibility(View.GONE);
    }
    // lalu gunakan LiveData observer untuk mengupdate view
    viewModel.getDroppingListLoadState().observe(getViewLifecycleOwner(), state -> {
      HyperLog.d(TAG, "droppingListLoadState changed: " + state);
      if (state instanceof LoadState.Loading) {
        // sedang loading pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
      } else if (state instanceof LoadState.Error) {
        // terjadi error pada waktu pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);
        // retry = reload semua data
        binding.progressWrapper.retryButton.setOnClickListener(reloadFragment);
        binding.progressWrapper.progressText.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            ((LoadState.Error) state).getError().getMessage());
        // sembunyikan list, karena retry akan dihandle oleh button di atas
        binding.swipeRefresh.setVisibility(View.GONE);
      } else if (state instanceof LoadState.NotLoading) {
        // sudah ok
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.swipeRefresh.setVisibility(View.VISIBLE);
      }
    });
    // connect data ke adapter
    // supaya tidak hit server lagi pada waktu kembali dari child fragment, hanya load data kalau
    // list masih null
    viewModel.getDroppingList().observe(getViewLifecycleOwner(), pagingData -> {
      adapter.submitData(getLifecycle(), pagingData);
    });
    setFilterObserver();
    setSortObserver();
  }

  private class ActionModeCallback implements ActionMode.Callback {

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
      mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
      Utility.setActionModeMenu(mode, TAG);
      setPermissionActionButton(isPermissionCreate, menu);
      return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
      return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
      switch (item.getItemId()) {
      case R.id.action_delete:
        deleteRow(mode);
        return true;
      case R.id.action_reject:
        //        rejectRow(mode);
        return true;
      default:
        return false;
      }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
      adapter.clearSelections();
      actionMode = null;
      recyclerView.post(new Runnable() {
        @Override
        public void run() {
          adapter.resetAnimationIndex();
        }
      });
    }
  }

  private void setPermissionActionButton(boolean hasPermission, Menu menu) {
    MenuItem actionMenu = menu.findItem(R.id.action_menu);
    actionMenu.setVisible(!hasPermission);
  }

  public void deleteRow(ActionMode mode) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    builder.setCancelable(true);
    builder.setTitle(R.string.confirm_delete_title);
    builder.setMessage(R.string.confirm_delete_prompt);
    builder.setPositiveButton(R.string.button_hapus, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        viewModel.deleteSelectedDropping(adapter.getSelectedDad());
        mode.finish();
        adapter.resetAnimationIndex();
      }
    });
    builder.setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        mode.finish();
        adapter.resetAnimationIndex();
      }
    });

    AlertDialog dialog = builder.create();
    dialog.show();
  }

  public void setSortObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<String> soListJdonMld = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        DroppingAdditionalSortFragment.DROPPING_ADDITIONAL_SORT);

    soListJdonMld.observe(getViewLifecycleOwner(), soListJdon -> {
      String soListJson = soListJdon.replace("\\", "");
      Type dataType = new TypeToken<List<SortOrder>>() {
      }.getType();
      List<SortOrder> soList = new Gson().fromJson(soListJson, dataType);
      Set<SortOrder> sso = new HashSet<SortOrder>(soList);
      viewModel.setSortFields(sso);
      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
          DroppingAdditionalSortFragment.DROPPING_ADDITIONAL_SORT);
    });
  }

  public void setFilterObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<DroppingBrowseFilter> filterField = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        DroppingAdditionalFilterFragment.DROPPING_ADDITIONAL_FILTER);
    filterField.observe(getViewLifecycleOwner(), filterObj -> {
      viewModel.loadListWithFilter(filterObj);
      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
          DroppingAdditionalFilterFragment.DROPPING_ADDITIONAL_FILTER);
    });
  }
}