package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.wfaction;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingMultipeSelectedData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingMultipleSelectedProcessResultData;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;

public class AppBookingMultiActionVM extends AndroidViewModel
    implements AppBookingMultiActionUC.Listener {
  private static String TAG = AppBookingMultiActionVM.class.getSimpleName();
  private List<AppBookingData> dataList = new ArrayList<>();
  private AppBookingMultiActionUC processUC;
  private String decision = null;
  private MutableLiveData<FormState> formState = new MutableLiveData<>();

  @ViewModelInject
  public AppBookingMultiActionVM(@NonNull Application application,
      AppBookingMultiActionUC processUC) {
    super(application);
    this.processUC = processUC;
    this.processUC.registerListener(this);
  }

  public List<AppBookingData> getDataList() {
    return dataList;
  }

  public void setDataList(List<AppBookingData> dataList) {
    this.dataList = dataList;
  }

  public String getDecision() {
    return this.decision;
  }

  public void setDecision(String decision) {
    this.decision = decision;
  }

  public void processMultiApproveCaptain(List<AppBookingData> dataList, String comment) {
    List<AppBookingMultipeSelectedData> finalDataList = populateMultipleSelectedData(dataList,
        comment);
    processUC.processMultiApprove(finalDataList);
  }

  public void processMultiReturnCaptain(List<AppBookingData> dataList, String comment) {
    List<AppBookingMultipeSelectedData> finalDataList = populateMultipleSelectedData(dataList,
        comment);
    processUC.processMultiReturn(finalDataList);
  }

  public MutableLiveData<FormState> getFormState() {
    return formState;
  }

  private List<AppBookingMultipeSelectedData> populateMultipleSelectedData(
      List<AppBookingData> dataList, String comment) {
    List<AppBookingMultipeSelectedData> finalDataList = new ArrayList<>();
    for (AppBookingData data : dataList) {
      AppBookingMultipeSelectedData finalData = new AppBookingMultipeSelectedData();
      finalData.setAppBookingId(data.getId());
      finalData.setComment(comment);
      finalData.setProcTaskId(data.getProcTaskId());
      finalData.setOperationalTransfer(data.getOperationalTransfer());
      finalData.setUpdateTsString(data.getUpdateTsString());

      finalDataList.add(finalData);
    }
    return finalDataList;
  }

  @Override
  public void onProcessStarted(Resource<AppBookingMultipleSelectedProcessResultData> loading) {
    HyperLog.d(TAG, loading.getMessage());
    formState.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessSuccess(Resource<AppBookingMultipleSelectedProcessResultData> response) {
    HyperLog.d(TAG, response.getMessage());
    AppBookingMultipleSelectedProcessResultData resData = response.getData();
    if (!Objects.equals(resData, null)) {
      StringBuilder sbTrxNo = new StringBuilder();
      for (String trxNo : resData.getTrxNoList())
        sbTrxNo.append(trxNo + ",");

      if (!sbTrxNo.toString().isEmpty()) {
        formState.postValue(FormState.ready(sbTrxNo.toString() + "Gagal di Proses"));
      } else {
        formState.postValue(FormState.ready(response.getMessage()));
      }
      StringBuilder sbErrMsg = new StringBuilder();
      for (String errorMsg : resData.getErrorMsg())
        sbErrMsg.append(errorMsg + ", ");
      HyperLog.d(TAG, sbErrMsg.toString());
    } else {
      formState.postValue(FormState.ready(response.getMessage()));
    }
  }

  @Override
  public void onProcessFailure(Resource<AppBookingMultipleSelectedProcessResultData> response) {
    HyperLog.e(TAG, response.getMessage());
    formState.postValue(FormState.error(response.getMessage()));
  }
}
