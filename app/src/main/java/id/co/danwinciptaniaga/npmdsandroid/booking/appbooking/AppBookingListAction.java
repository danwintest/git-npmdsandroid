package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class AppBookingListAction
    extends BaseObservable<AppBookingListAction.Listener, HashMap<String, List<String>>> {
  private static final String TAG = AppBookingListAction.class.getSimpleName();
  private final AppBookingService service;
  private final AppExecutors appExecutors;
  private final Application application;

  @Inject
  public AppBookingListAction(AppBookingService service, AppExecutors appExecutors,
      Application app) {
    super(app);
    this.application = app;
    this.service = service;
    this.appExecutors = appExecutors;
  }

  public void processDelete(List<UUID> ids) {
    notifyStart(application.getString(R.string.silahkan_tunggu), null);
    ListenableFuture<HashMap<String, List<String>>> deleteLf = service.deleteAppBooking(ids);
    Futures.addCallback(deleteLf, new FutureCallback<HashMap<String, List<String>>>() {
      @Override
      public void onSuccess(@NullableDecl HashMap<String, List<String>> result) {
        for (java.util.Map.Entry<String, List<String>> res : result.entrySet()) {
          String message = "";
          if (res.getKey().equals("s")) {
            message = ids.size() + " Pengajuan Booking berhasil dihapus";
            notifySuccess(message, result);
          } else if (res.getKey().equals("r")) {
            List<String> rejectedList = res.getValue();
            message = rejectedList.size() + "Gagal terhapus"
                + android.text.TextUtils.join(",", rejectedList);
            notifySuccess(message, result);
          }
          HyperLog.d(TAG, "onSuccess processDelete msg->" + message + "<-");
        }
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG,t);
        notifyFailure(ids.size() + "Pengajuan Booking gagal dihapus "+message, null, t);
      }
    }, appExecutors.networkIO());

  }

  public void processApprove(List<UUID> ids) {

  }

  public void processReturn(List<UUID> ids) {

  }

  public void processReject(List<UUID> ids) {

  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<HashMap<String, List<String>>> result) {
    listener.onAppBookingActionProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener,
      Resource<HashMap<String, List<String>>> result) {
    listener.onAppBookingActionProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener,
      Resource<HashMap<String, List<String>>> result) {
    listener.onAppBookingActionProcessFailure(result);
  }

  public interface Listener {
    void onAppBookingActionProcessStarted(Resource<HashMap<String, List<String>>> loading);

    void onAppBookingActionProcessSuccess(Resource<HashMap<String, List<String>>> response);

    void onAppBookingActionProcessFailure(Resource<HashMap<String, List<String>>> response);
  }
}
