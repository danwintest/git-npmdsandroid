package id.co.danwinciptaniaga.npmdsandroid.ui.landing;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.google.android.gms.common.util.CollectionUtils;
import com.google.android.material.navigation.NavigationView;
import com.hypertrack.hyperlog.HyperLog;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewParent;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.customview.widget.Openable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.auth.PermissionHelper;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.common.ExtUserInfoData;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.BaseActivity;
import id.co.danwinciptaniaga.npmdsandroid.BuildConfig;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.ActivityLandingBinding;

@AndroidEntryPoint
public class LandingActivity extends BaseActivity {
  private static String TAG = LandingActivity.class.getSimpleName();

  @Inject
  LoginUtil loginUtil;
  TextView tvUserName, tvVersion;
  private AppBarConfiguration mAppBarConfiguration;
  private DrawerViewModel drawerViewModel;
  private Toolbar toolbar;
  private DrawerLayout drawer;
  private NavigationView navigationView;
  private NavController navController;
  private ActivityLandingBinding binding;

  private Map<String, Integer> menuPermissionMap = new HashMap<>();

  private void setObject() {
    toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    drawer = findViewById(R.id.drawer_layout);
    navigationView = findViewById(R.id.nav_view);
    navController = Navigation.findNavController(this, R.id.nav_host_fragment);
    tvUserName = navigationView.getHeaderView(0).findViewById(R.id.tvUsername);
    tvVersion = navigationView.getHeaderView(0).findViewById(R.id.tvVersion);
    tvVersion.setText(BuildConfig.VERSION_NAME);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    binding = ActivityLandingBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());
    setObject();

    initMenuPermissionMap();

    // Passing each menu ID as a set of Ids because each
    // menu should be considered as top level destinations.
    mAppBarConfiguration = new AppBarConfiguration.Builder(
        R.id.nav_outlet_list,
        R.id.nav_expense_browse,
        R.id.nav_p_booking_browse,
        R.id.nav_booking_browse,
        R.id.nav_dropping_daily_browse,
        R.id.nav_dropping_additional_browse,
        R.id.nav_dropping_daily_task_browse,
        R.id.nav_tugas_transfer_browse
    ).setOpenableLayout(drawer).build();

    NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
    navigationView.setNavigationItemSelectedListener(
        new NavigationView.OnNavigationItemSelectedListener() {
          @Override
          public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            // lebih dulu handle menu selain navigation
            boolean handled = LandingActivity.this.handleNavigationMenu(item);
            if (handled)
              return true;
            handled = NavigationUI.onNavDestinationSelected(item, navController);
            if (handled) {
              ViewParent parent = navigationView.getParent();
              if (parent instanceof Openable) {
                ((Openable) parent).close();
              }
            }
            return handled;
          }
        });

    drawerViewModel = new ViewModelProvider(this).get(DrawerViewModel.class);
    drawerViewModel.getErrorMessage().observe(this, s -> {
      Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    });
  }

  private void initMenuPermissionMap() {
    if (menuPermissionMap.isEmpty()) {
      menuPermissionMap.put("npmds_Expense.browse", R.id.nav_expense_browse);
      menuPermissionMap.put("npmds_AppBooking.browse", R.id.nav_p_booking_browse);
      menuPermissionMap.put("npmds_Booking.browse", R.id.nav_booking_browse);
      menuPermissionMap.put("npmds_DroppingDaily.browse", R.id.nav_dropping_daily_browse);
      menuPermissionMap.put("npmds_DroppingAdditional.browse", R.id.nav_dropping_additional_browse);
      menuPermissionMap.put("npmds_DroppingTask.browse", R.id.nav_dropping_daily_task_browse);
      menuPermissionMap.put("npmds_TugasTransfer.browse", R.id.nav_tugas_transfer_browse);
    }
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    if (status.getStatus() == Status.SUCCESS) {
      drawerViewModel.getUserInfoLd().observe(this, userInfoResource -> {
        if (userInfoResource != null) {
          switch (userInfoResource.getStatus()) {
          case LOADING:
            tvUserName.setText(getString(R.string.msg_please_wait));
            break;
          case SUCCESS:
            ExtUserInfoData euid = userInfoResource.getData();
            // kena race condition untuk visibilitas fungsi Substitusi User.
            // Kalau User login, permissionsLd lebih dulu terpanggil baru ke sini.
            // Jadi di kedua tempat ld, cek apakah substitusi harus visible
            boolean canSubstitute = false;
            if (euid.isSubstitute() != null && euid.isSubstitute()) {
              // kalau adalah user substitusi, menu Substitusi visible
              tvUserName.setText(String.format("[%s]",
                  StringUtils.defaultIfBlank(euid.getName(), euid.getLogin())));
              canSubstitute = true;
            } else {
              // kalau permission sudah ada, cek apakah substitusi harus visible
              if (model.getPermissionsLd().getValue() != null && !CollectionUtils.isEmpty(
                  model.getPermissionsLd().getValue().getData())) {
                Optional<PermissionInfo> opt = model.getPermissionsLd().getValue().getData().stream().filter(
                    e -> {
                      return PermissionHelper.PERMISSION_TYPE_SPECIFIC.equals(e.getType())
                          && "npmds.user.substituteUser".equals(e.getTarget())
                          && PermissionHelper.PERMISSION_VALUE_ALLOW.equals(e.getValue());
                    }).findFirst();
                if (opt.isPresent()) {
                  canSubstitute = true;
                }
              }
              tvUserName.setText(euid.getName());
            }
            navigationView.getMenu().findItem(R.id.nav_substitute_user).setVisible(canSubstitute);
            break;
          case ERROR:
            tvUserName.setText(getString(R.string.get_ext_user_info_failed));
            Toast.makeText(this, userInfoResource.getMessage(), Toast.LENGTH_SHORT).show();
            break;
          }
        }
      });
      model.getPermissionsLd().observe(this, permissions -> {
        if (permissions == null) {
          // matikan menu yang tergantung permission
          // note: setelah refactoring ke ProtectedActivityViewModel,
          // sepertinya permissions == null tidak pernah akan terpanggil di sini
          for (Map.Entry<String, Integer> mp : menuPermissionMap.entrySet()) {
            navigationView.getMenu().findItem(mp.getValue()).setVisible(false);
          }
        } else {
          switch (permissions.getStatus()) {
          case SUCCESS:
            // matikan menu yang tergantung permission
            for (Map.Entry<String, Integer> mp : menuPermissionMap.entrySet()) {
              navigationView.getMenu().findItem(mp.getValue()).setVisible(false);
            }
            boolean canSubstitute = false;
            // kena race condition untuk visibilitas fungsi Substitusi User
            Resource<ExtUserInfoData> euid = drawerViewModel.getUserInfoLd().getValue();
            if (euid != null) {
              // kalau sekarang sudah jadi Substitute User, pasti boleh substitusi lagi
              canSubstitute = euid.getData() != null && euid.getData().isSubstitute() != null
                  && euid.getData().isSubstitute();
            }
            for (PermissionInfo pi : permissions.getData()) {
              // hanya cek yang tipe SCREEN dan ALLOW
              if ("SCREEN".equals(pi.getType()) && pi.getIntValue() == 1) {
                Integer menu = menuPermissionMap.get(pi.getTarget());
                if (menu != null) {
                  navigationView.getMenu().findItem(menu).setVisible(true);
                }
              }
              if (!canSubstitute &&
                  PermissionHelper.PERMISSION_TYPE_SPECIFIC.equals(pi.getType())
                  && "npmds.user.substituteUser".equals(pi.getTarget())
                  && PermissionHelper.PERMISSION_VALUE_ALLOW.equals(pi.getValue())) {
                canSubstitute = true;
              }
            }
            // menu substitusi visible sesuai evaluasi canSubstitute
            navigationView.getMenu().findItem(R.id.nav_substitute_user).setVisible(canSubstitute);
            break;
          case ERROR:
            // todo: ijinkan retry?
            Resource.getMessageFromError(permissions,
                getString(R.string.get_user_permissions_failed));
            Toast.makeText(this, permissions.getMessage(), Toast.LENGTH_SHORT).show();
            break;
          }
        }
      });
      drawerViewModel.loadUserInfo();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.landing, menu);
    return true;
  }

  @Override
  public boolean onSupportNavigateUp() {
    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
    return NavigationUI.navigateUp(navController, mAppBarConfiguration)
        || super.onSupportNavigateUp();
  }

  private boolean handleNavigationMenu(MenuItem item) {
    if (item.getItemId() == R.id.nav_logout) {
      // logout melakukan hal-berikut:
      // 1. invalidate acct di device, supaya harus dapatkan token baru
      AccountManager am = AccountManager.get(getApplicationContext());
      Account a = loginUtil.getAccount(getProtectionAccountType());
      if (a != null) {
        am.invalidateAuthToken(getProtectionAccountType(), loginUtil.getSavedToken());
      }
      HyperLog.i(TAG,"User logs out");
      // 2. app logout (logout dari app android device, dan revoke token di server)
      super.logout();
      App.gotoLauncher(this);
      finish();
      return true;
    }
    return false;
  }

  public DrawerViewModel getDrawerViewModel() {
    return drawerViewModel;
  }
}