package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

public class ExpenseFormState {
  public enum State {
    LOADING, READY, ACTION_IN_PROGRESS, ERROR
  }

  private String message;
  private State state;

  public ExpenseFormState(State state, String message) {
    this.state = state;
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public State getState() {
    return state;
  }

  public static ExpenseFormState loading(String message) {
    return new ExpenseFormState(State.LOADING, message);
  }

  public static ExpenseFormState ready(String message) {
    return new ExpenseFormState(State.READY, message);
  }

  public static ExpenseFormState actionInProgress(String message) {
    return new ExpenseFormState(State.ACTION_IN_PROGRESS, message);
  }

  public static ExpenseFormState error(String message) {
    return new ExpenseFormState(State.ERROR, message);
  }
}
