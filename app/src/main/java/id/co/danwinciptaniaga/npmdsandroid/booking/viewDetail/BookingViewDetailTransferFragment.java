package id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.hypertrack.hyperlog.HyperLog;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingCashScreenMode;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingTransferScreenMode;
import id.co.danwinciptaniaga.npmds.data.common.LobShortData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.common.SoShortData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingBrowseViewModel;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.Abstract_AppBookingVM;
import id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.AppBookingTransferNewFragment;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class BookingViewDetailTransferFragment extends AppBookingTransferNewFragment {
  private final String TAG = BookingViewDetailTransferFragment.class.getSimpleName();
  private BookingBrowseViewModel mBrowseVM;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    Abstract_AppBookingVM vm = new ViewModelProvider(this).get(BookingDetailTransferVM.class);
    BookingViewDetailFragmentArgs args = BookingViewDetailFragmentArgs.fromBundle(getArguments());

    boolean isHasCareatePermission = args.getIsHasCreatePermission();
    setOnCreateView(isHasCareatePermission, inflater, container, this, TAG, vm);
    getDetailTransferVm().setField_BookingId(args.getBookingId());
    mBrowseVM = new ViewModelProvider(requireActivity()).get(BookingBrowseViewModel.class);
    return getBinding().getRoot();
  }

  @Override
  protected void processLoadData() {
    getDetailTransferVm().loadByBookingId(getDetailTransferVm().getField_BookingId());
  }

  private BookingDetailTransferVM getDetailTransferVm() {
    return ((BookingDetailTransferVM) getTransferVm());
  }


  @Override
  protected void initForm() {
    super.initForm();
    setField_ConsumerBankName();
    setField_ConsumerAccountName();
    setField_ConsumerAccountNo();
    setField_BiroJasaBankName();
    setField_BiroJasaAccountName();
    setField_BiroJasaAccountNo();
    setField_FIFBankName();
    setField_FIFAccountName();
    setField_FIFAccountNo();
    getBinding().btnWfHistory.setVisibility(View.GONE);
  }

  @Override
  protected void setIvValid_Cons() {
    getDetailTransferVm().isField_ConsValid().observe(getViewLifecycleOwner(), isValid -> {
      if (isValid)
        getBinding().ivTFConsumerStatus.setImageResource(
            R.drawable.ic_baseline_check_circle_outline_24);
      else
        getBinding().ivTFConsumerStatus.setImageResource(
            R.drawable.ic_baseline_remove_circle_outline_24);
    });
  }

  @Override
  protected void setIvValid_BJ() {
    getDetailTransferVm().isField_BjValid().observe(getViewLifecycleOwner(), isValid -> {
      if (isValid)
        getBinding().ivTFBiroJasaStatus.setImageResource(
            R.drawable.ic_baseline_check_circle_outline_24);
      else
        getBinding().ivTFBiroJasaStatus.setImageResource(
            R.drawable.ic_baseline_remove_circle_outline_24);
    });
  }

  @Override
  protected void setIvValid_FIF() {
    getDetailTransferVm().isField_FIFValid().observe(getViewLifecycleOwner(), isValid -> {
      if (isValid)
        getBinding().ivTFFIFStatus.setImageResource(
            R.drawable.ic_baseline_check_circle_outline_24);
      else
        getBinding().ivTFFIFStatus.setImageResource(
            R.drawable.ic_baseline_remove_circle_outline_24);
    });
  }

  @Override
  protected void setField_Outlet() {
    outletsAdapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item);
    getBinding().spOutlet.setAdapter(outletsAdapter);
    getVm().getField_outletKvm().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {

      } else {
        List<KeyValueModel<OutletShortData, String>> kvmList = new ArrayList<>();
        kvmList.add(data);
        outletsAdapter.addAll(kvmList);
        outletsAdapter.notifyDataSetChanged();
        getBinding().spOutlet.setSelection(0);
      }
    });
  }

  @Override
  protected void setField_Lob() {
    lobAdapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item);
    getBinding().spLOB.setAdapter(lobAdapter);
    getVm().getField_LobKvm().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
      } else {
        List<KeyValueModel<LobShortData, String>> kvmList = new ArrayList<>();
        kvmList.add(data);
        lobAdapter.addAll(kvmList);
        lobAdapter.notifyDataSetChanged();
        getBinding().spLOB.setSelection(0);
      }
    });
  }

  @Override
  protected void setField_So() {
    soAdapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item);
    getBinding().spSO.setAdapter(soAdapter);
    getVm().getField_soKvm().observe(getViewLifecycleOwner(), data -> {
      if (data == null) {
      } else {
        List<KeyValueModel<SoShortData, String>> kvmList = new ArrayList<>();
        kvmList.add(data);
        soAdapter.addAll(kvmList);
        soAdapter.notifyDataSetChanged();
        getBinding().spSO.setSelection(0);
      }
    });
  }

  @Override
  protected void setMode_generalField(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer, String message,
      Animation inAnimation, Animation outAnimation) {
    super.setMode_generalField(smCash, smTransfer, message, inAnimation, outAnimation);
    getBinding().cvOperasionalTransfer.setVisibility(View.GONE);
    getVm().getField_AppBookingTypeId().observe(getViewLifecycleOwner(), data -> {
      if (data.equals(Utility.BOOKING_TYPE_CASH)) {
        setMode_FieldCash_READONLY();
      } else {
        setMode_FieldTrans_READONLY();
      }
      //      getBinding().btnWfHistory.setVisibility(View.GONE);
      // TODO set enabled(false) untuk feeScheme dan feeMatrix pada super.setScreenMode tidak bekerja
      getBinding().tilFeeScheme.setEnabled(false);
      getBinding().tilFeeMatrix.setEnabled(false);
    });
  }

  @Override
  protected void setField_ConsumerAmount() {
    getVm().getField_AppBookingTypeId().observe(getViewLifecycleOwner(), data -> {
      getVm().getField_ConsumerAmount().observe(getViewLifecycleOwner(), amt -> {
        if (data.equals(Utility.BOOKING_TYPE_CASH)) {
          getBinding().etPencairanKonsumen.setText(
              amt != null ? Utility.getFormattedAmt(amt) : null);
        } else {
          getBinding().etTFConsumer.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
        }
      });
    });
  }

  @Override
  protected void setField_FIFAmount() {
    getVm().getField_AppBookingTypeId().observe(getViewLifecycleOwner(), data -> {
      getVm().getField_FIFAmount().observe(getViewLifecycleOwner(), amt -> {
        if (data.equals(Utility.BOOKING_TYPE_CASH)) {
          getBinding().etPencairanFIF.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
        } else {
          getBinding().etTFFIF.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
        }
      });
    });
  }

  @Override
  protected void setField_BiroJasaAmount() {
    getVm().getField_AppBookingTypeId().observe(getViewLifecycleOwner(), data -> {
      getVm().getField_BiroJasaAmount().observe(getViewLifecycleOwner(), amt -> {
        if (data.equals(Utility.BOOKING_TYPE_CASH)) {
          getBinding().etBiroJasa.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
        } else {
          getBinding().etTFBiroJasa.setText(amt != null ? Utility.getFormattedAmt(amt) : null);
        }
      });
    });
  }

  protected void setField_ConsumerBankName() {
    getDetailTransferVm().getField_ConsumerBank_LABEL().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFConsumerBankName.setText(data);
        });
  }

  protected void setField_ConsumerAccountName() {
    getDetailTransferVm().getField_ConsumerAccountName().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFConsumerAccountName.setText(data);
        });
  }

  @Override
  protected void setField_Attachment_HO() {
    getDetailTransferVm().getHeader_StatusCode().observe(getViewLifecycleOwner(),status->{
      if (Objects.equals(status, Utility.OPERATION_CANCEL)) {
        getBinding().tvLabelCancelation.setVisibility(View.VISIBLE);
        getBinding().tilCancelReason.setVisibility(View.VISIBLE);
        getBinding().tvLabelHO.setVisibility(View.VISIBLE);
        getBinding().ivAtcHO.setVisibility(View.VISIBLE);
      }else{
        getBinding().tvLabelCancelation.setVisibility(View.GONE);
        getBinding().tilCancelReason.setVisibility(View.GONE);
        getBinding().tvLabelHO.setVisibility(View.GONE);
        getBinding().ivAtcHO.setVisibility(View.GONE);
      }
    });
    super.setField_Attachment_HO();
  }

  protected void setField_ConsumerAccountNo() {
    getDetailTransferVm().getField_ConsumerAccountNo().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFConsumerRekNo.setText(data);
        });
  }

  protected void setField_BiroJasaBankName() {
    getDetailTransferVm().getField_BiroJasaBank_LABEL().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFBiroJasaBankName.setText(data == null ? "-" : data);
        });
  }

  protected void setField_BiroJasaAccountName() {
    getDetailTransferVm().getField_BiroJasaAccountName().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFBiroJasaAccountName.setText(data == null ? "-" : data);
        });
  }

  protected void setField_BiroJasaAccountNo() {
    getDetailTransferVm().getField_BiroJasaAccountNo().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFBiroJasaRekNo.setText(data);
        });
  }

  protected void setField_FIFBankName() {
    getDetailTransferVm().getField_FIFBank_LABEL().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFFIFBankName.setText(data == null ? "-" : data);
        });
  }

  protected void setField_FIFAccountName() {
    getDetailTransferVm().getField_FIFAccountName().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFFIFAccountName.setText(data == null ? "-" : data);
        });
  }

  protected void setField_FIFAccountNo() {
    getDetailTransferVm().getField_FIFAccountNo().observe(getViewLifecycleOwner(),
        data -> {
          getBinding().tvTFFIFRekNo.setText(data == null ? "-" : data);
        });
  }

  @Override
  protected void setStandartButtonListener(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer) {
    getBinding().btnSimpan.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getDetailTransferVm().processSave();
      }
    });
  }

  @Override
  protected void setField_Batch() {
  }

  @Override
  protected void setField_FeeMatrix() {
    // set data
    ((BookingDetailTransferVM) getVm()).getField_FeeMatrix().observe(getViewLifecycleOwner(), amt -> {
      if (amt == null) {
        getBinding().etFeeMatrix.setText(null);
      } else {
        if (!getBinding().etFeeMatrix.isEnabled()) {
          String formattedAmt = Utility.getFormattedAmt(amt);
          if (!formattedAmt.equals(getBinding().etFeeMatrix.getText().toString())) {
            getBinding().etFeeMatrix.setText(formattedAmt);
          }
        } else {
          if (!amt.toString().equals(getBinding().etFeeMatrix.getText().toString())) {
            getBinding().etFeeMatrix.setText(amt.toString());
          }
        }
      }
    });

    // set listener
    getBinding().etFeeMatrix.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          BigDecimal amt = Utility.getAmtFromFromattedString(s.toString());
          ((BookingDetailVM) getVm()).setField_FeeMatrix(amt);
        } else
          ((BookingDetailVM) getVm()).setField_FeeMatrix(null);
      }
    });

    // set ERROR
    ((BookingDetailTransferVM) getVm()).getField_FeeMatrix_ERROR().observe(getViewLifecycleOwner(), message -> {
      getBinding().tilFeeMatrix.setError(message);
    });

    // set EditAble
    getBinding().tilFeeMatrix.setEnabled(false);
  }


  @Override
  protected void setField_POMODE(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTransfer) {
    if (!isHasCreatePermission) {
      setField_PopReason(false);
      getBinding().btnSimpan.setVisibility(View.GONE);
      setField_POMODE2(false);
      return;
    } else {
      getBinding().btnSimpan.setVisibility(View.VISIBLE);
      getDetailTransferVm().getField_CanEditPopr().observe(getViewLifecycleOwner(), data -> {
        setField_POMODE2(true);
        getBinding().acPendingPoReason.setEnabled(data);
        setField_PopReason(data);
      });
      getVm().isReadOnly_AppNoPoNoPoDatePoAttchment().observe(getViewLifecycleOwner(),
          isReadOnly -> {
            //            setField_POMODE2(!isReadOnly);
            getBinding().tilAppNo.setEnabled(!isReadOnly);
            getBinding().tilPoNo.setEnabled(!isReadOnly);
            getBinding().tilPoDate.setEnabled(!isReadOnly);
          });
    }
  }

  @Override
  protected void setReadyFormState(AppBookingCashScreenMode smCash,
      AppBookingTransferScreenMode smTrans, String message,
      Animation inAnim, Animation outAnim, boolean isFromAction) {
    if (isFromAction) {
      mBrowseVM.setRefreshList(true);
      NavController navController = Navigation.findNavController(getBinding().getRoot());
      navController.navigateUp();
      return;
    } else
      super.setReadyFormState(smCash, smTrans, message, inAnim, outAnim, isFromAction);
  }
}