package id.co.danwinciptaniaga.npmdsandroid.ui;

import androidx.navigation.NavOptions;
import id.co.danwinciptaniaga.npmdsandroid.R;

public class NavHelper {
  public static NavOptions.Builder animParentToChild() {
    return new NavOptions.Builder()
        .setEnterAnim(R.anim.slide_in_right)
        .setExitAnim(R.anim.slide_out_left)
        .setPopEnterAnim(R.anim.slide_in_left)
        .setPopExitAnim(R.anim.slide_out_right);
  }

  public static NavOptions.Builder animChildToParent() {
    return new NavOptions.Builder()
        .setEnterAnim(R.anim.slide_in_left)
        .setExitAnim(R.anim.slide_out_right)
        .setPopEnterAnim(R.anim.slide_in_right)
        .setPopExitAnim(R.anim.slide_out_left);
  }
}
