package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import java.io.File;

import com.bumptech.glide.Glide;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDetailDTO;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentDroppingAdditionalDetailEditBinding;
import id.co.danwinciptaniaga.npmdsandroid.ui.ImageUIHelper;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DroppingAdditionalDetailEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
public class DroppingAdditionalDetailEditFragment extends Fragment {
  private static final String TAG = DroppingAdditionalDetailEditFragment.class.getSimpleName();

  // TODO: Rename parameter arguments, choose names that match
  // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
  private static final String ARG_PARAM1 = "param1";
  private static final String ARG_PARAM2 = "param2";

  // TODO: Rename and change types of parameters
  private String mParam1;
  private String mParam2;
  private FragmentDroppingAdditionalDetailEditBinding binding;
  private DroppingAdditionalDetailEditViewModel viewModel;

  public DroppingAdditionalDetailEditFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @param param1 Parameter 1.
   * @param param2 Parameter 2.
   * @return A new instance of fragment DroppingAdditionalDetailEditFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static DroppingAdditionalDetailEditFragment newInstance(String param1, String param2) {
    DroppingAdditionalDetailEditFragment fragment = new DroppingAdditionalDetailEditFragment();
    Bundle args = new Bundle();
    args.putString(ARG_PARAM1, param1);
    args.putString(ARG_PARAM2, param2);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (getArguments() != null) {
      mParam1 = getArguments().getString(ARG_PARAM1);
      mParam2 = getArguments().getString(ARG_PARAM2);
    }
    viewModel = new ViewModelProvider(this).get(DroppingAdditionalDetailEditViewModel.class);

    DroppingAdditionalDetailEditFragmentArgs args = DroppingAdditionalDetailEditFragmentArgs
        .fromBundle(getArguments());
    DroppingDetailFormData droppingDetailData = args.getDroppingDetailData();
    if (droppingDetailData != null) {
      viewModel.setFormData(droppingDetailData);
    } else {
      viewModel.prepareNewDroppingDetailFormData();
    }
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    binding = FragmentDroppingAdditionalDetailEditBinding
        .inflate(inflater, container, false);

    viewModel.getFormData().observe(getViewLifecycleOwner(), droppingDetailFormData -> {
      binding.etRemarks.setText(droppingDetailFormData.getDescription());
      binding.etAmount.setText(
          droppingDetailFormData.getAmount() == null ?
              null :
              String.valueOf(droppingDetailFormData.getAmount()));
      if (droppingDetailFormData.getAttachmentDTO() != null
          && droppingDetailFormData.getAttachmentDTO().getFile() != null) {
        Glide.with(getContext())
            .load(droppingDetailFormData.getAttachmentDTO().getFile())
            .placeholder(R.drawable.ic_baseline_image_24)
            .into(binding.ivAttachment);
      }
    });

    binding.etRemarks.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        viewModel.setDescription(s.toString());
      }
    });
    viewModel.getDescriptionErrorLd().observe(getViewLifecycleOwner(), s -> {
      binding.tilRemarks.setError(s);
    });

    binding.etAmount.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {
        if (s.length() > 0) {
          viewModel.setAmount(Long.parseLong(s.toString()));
        } else {
          viewModel.setAmount(null);
        }
      }
    });
    viewModel.getAmountErrorLd().observe(getViewLifecycleOwner(), s -> {
      binding.tilAmount.setError(s);
    });

    viewModel.getReducedAttachmentLd().observe(getViewLifecycleOwner(), bitmap -> {
      if (bitmap != null) {
        Glide.with(getContext())
            .load(bitmap)
            .centerCrop()
            .into(binding.ivAttachment);
      } else {
        this.binding.ivAttachment.setImageResource(R.drawable.ic_baseline_add_a_photo_24);
      }
    });
    binding.ivAttachment.setOnClickListener(this::onIvAttachmentClick);
    binding.ivAttachment.setOnLongClickListener(this::onIvAttachmentLongClick);

    viewModel.getCanProcess().observe(getViewLifecycleOwner(), canProcess -> {
      if (canProcess != null && canProcess) {
        binding.btnSaveDraft.setEnabled(true);
      } else {
        binding.btnSaveDraft.setEnabled(false);
      }
    });

    binding.btnSaveDraft.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        DroppingDetailDTO dd = viewModel.getFormData().getValue();
        dd.setArchived(false);
        NavController navController = Navigation.findNavController(v);
        navController.getPreviousBackStackEntry().getSavedStateHandle()
            .set(DroppingAdditionalEditFragment.SAVED_STATE_LD_DROPPING_DETAIL_DATA, dd);
        navController.popBackStack();
      }
    });

    return binding.getRoot();
  }

  private void onIvAttachmentClick(View view) {
    if (viewModel.getAttachmentFile() == null) {
      // kalau belum ada attachment
      String[] options = getResources().getStringArray(R.array.attachment_add_actions);

      AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
      builder.setItems(options, new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int item) {
          if (item == 0) {
            // kamera
            File tempFile = ImageUIHelper.startImageCaptureIntent(
                DroppingAdditionalDetailEditFragment.this, Utility.REQUEST_TAKE_PHOTO);
            viewModel.setTempAttachmentFile(tempFile);
          } else if (item == 1) {
            // pilih gambar
            ImageUIHelper.startImagePickIntent(DroppingAdditionalDetailEditFragment.this,
                Utility.REQUEST_PICK_PHOTO);
          } else {
            dialog.dismiss();
          }
        }
      });
      builder.show();
    } else if (viewModel.getAttachmentFile() != null) {
      // kalau sudah ada attachment, maka tampilkan
      ImageUIHelper.viewImageUsingIntent(getContext(), viewModel.getAttachmentFile());
    }
  }

  private boolean onIvAttachmentLongClick(View view) {
    viewModel.setAttachmentFilled(false, true);
    return true;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    HyperLog.d(TAG, String.format("onActivityResult: %s, %s", requestCode, resultCode));
    if (resultCode != Activity.RESULT_CANCELED) {
      switch (requestCode) {
      case id.co.danwinciptaniaga.npmdsandroid.util.Utility.REQUEST_TAKE_PHOTO:
        if (resultCode == Activity.RESULT_OK) {
          // kamera sudah menyimpan file, hapus temp (yg masih besar)
          viewModel.setAttachmentFilled(true, true);
        } else {
          // hapus temp (yg masih besar)
          viewModel.setAttachmentFilled(false, true);
        }
        break;
      case id.co.danwinciptaniaga.npmdsandroid.util.Utility.REQUEST_PICK_PHOTO:
        if (resultCode == Activity.RESULT_OK && data != null) {
          Uri selectedImage = data.getData();
          String[] columns = {
              MediaStore.Images.Media.DATA, // file path
              MediaStore.Images.Media.ORIENTATION };
          if (selectedImage != null) {
            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                columns, null, null, null);
            if (cursor != null) {
              cursor.moveToFirst();

              int filePathColumnIndex = cursor.getColumnIndex(columns[0]);
              int orientationColumnIndex = cursor.getColumnIndex(columns[1]);
              String picturePath = cursor.getString(filePathColumnIndex);
              int orientation = cursor.getInt(orientationColumnIndex);
              cursor.close();
              viewModel.setTempAttachmentFile(new File(picturePath));
              // karena sumbernya dari luar, jangan coba hapus sumber
              viewModel.setAttachmentFilled(true, false);
            }
          } else {
            // karena sumbernya dari luar, jangan coba hapus sumber
            viewModel.setAttachmentFilled(false, false);
          }
        }
        break;
      }
    }
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }
}