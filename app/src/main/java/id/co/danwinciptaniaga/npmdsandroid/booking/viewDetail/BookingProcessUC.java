package id.co.danwinciptaniaga.npmdsandroid.booking.viewDetail;

import java.io.File;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.function.ToDoubleBiFunction;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.net.MediaType;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.booking.BookingService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import okhttp3.MultipartBody;

public class BookingProcessUC extends BaseObservable<BookingProcessUC.Listener, Boolean> {
  private final String TAG = BookingProcessUC.class.getSimpleName();
  private  final BookingService service;
  private final AppExecutors appExecutors;
  private final Application app;

  @Inject
  public BookingProcessUC(Application app, BookingService service, AppExecutors appExecutors) {
    super(app);
    this.service = service;
    this.app = app;
    this.appExecutors = appExecutors;
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<Boolean> result) {
    listener.onProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<Boolean> result) {
    listener.onProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<Boolean> result) {
    listener.onProcessFailure(result);
  }


  public void processSave(UUID bookingId, File att_po, String appNo, String poNo, LocalDate poDate,
      List<UUID> poprIdList, Date updateTs) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
//    if (true)
//      return;
    MultipartBody.Part[] final_att_po = null;
    if (att_po != null) {
      MultipartBody.Part final_att_po_part = RetrofitUtility.prepareFilePart("att_po", att_po,
          MediaType.ANY_IMAGE_TYPE.toString());
      final_att_po = new MultipartBody.Part[] { final_att_po_part };
    }

    // TODO perbaiki LocalDate.parse("2021-01-01") (DITAMBAHNKAN VARIABLE INI, untuk mengecek apakah
    //  poDate null atau tidak), harap perbaiki jika sudah ketemu caranya
    ListenableFuture<Boolean> process = service.save(bookingId, appNo, poNo,
        poDate == null ? LocalDate.parse("2021-01-01") : poDate, poprIdList, final_att_po, updateTs,
        poDate == null ? true : false);
    Futures.addCallback(process, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean result) {
        HyperLog.d(TAG, "Save Booking berhasil");
        notifySuccess("Simpan Booking Berhasil", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("Simpan Booking gagal ->" +message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public interface Listener {
    void onProcessStarted(Resource<Boolean> loading);

    void onProcessSuccess(Resource<Boolean> res);

    void onProcessFailure(Resource<Boolean> res);
  }




}
