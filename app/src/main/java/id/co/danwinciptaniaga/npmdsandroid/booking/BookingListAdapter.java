package id.co.danwinciptaniaga.npmdsandroid.booking;

import java.util.Objects;

import com.hypertrack.hyperlog.HyperLog;

import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class BookingListAdapter
    extends PagingDataAdapter<BookingData, BookingListAdapter.ViewHolder> {
  private final static String TAG = BookingListAdapter.class.getSimpleName();
  private final MyBookingDataRecyclerViewAdapterListener mListener;
  private SparseBooleanArray selectedItems;
  private boolean isPermissionCreate;
  private final Context ctx;

  public BookingListAdapter(MyBookingDataRecyclerViewAdapterListener listener, Context ctx, boolean isPermissionCreate) {
    super(new BookingDataDiffCallBack());
    mListener = listener;
    this.isPermissionCreate = isPermissionCreate;
    this.ctx = ctx;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_booking_item,
        parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
    // holder.mView.setActivated();
  }

  interface MyBookingDataRecyclerViewAdapterListener {
//    void onItemMenuClicked(View v, BookingData bookingData, int position);

    void onItemCliked(View v, BookingData bookingData, int posistion);

    void onItemMenuEdit(View v, BookingData bookingData, int position);

    void onItemMenuDelete(View v, BookingData bookingData, int position);

  }


  private static class BookingDataDiffCallBack extends DiffUtil.ItemCallback<BookingData> {

    @Override
    public boolean areItemsTheSame(@NonNull BookingData oldItem, @NonNull BookingData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull BookingData oldItem, @NonNull BookingData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final ConstraintLayout mContainer;
    public final TextView mConsumerNo;
    public final TextView mStatus;
    public final TextView mBookingDate;
    public final TextView mOutletNameAndCode;
    public final TextView mConsumerInfo;
    public final ImageView mMenu;
    public BookingData mitem;

    public ViewHolder(@NonNull View v) {
      super(v);
      mView = v;
      mContainer = (ConstraintLayout) v.findViewById(R.id.clBkContainer);
      mConsumerNo = (TextView) v.findViewById(R.id.tvBkConsumerNo);
      mStatus = (TextView) v.findViewById(R.id.tvBkStatus);
      mBookingDate = (TextView) v.findViewById(R.id.tvBkBookingDate);
      mOutletNameAndCode = (TextView) v.findViewById(R.id.tvBkOutletAndCode);
      mConsumerInfo = (TextView) v.findViewById(R.id.tvBkConsumerInfo);
      mMenu = (ImageView) v.findViewById(R.id.ivBkMenu);
    }

    public void bind(BookingData bookingData, int position) {
      this.mitem = bookingData;
      boolean isAvailableData = bookingData != null;
      setObject(bookingData, isAvailableData);
      setObjectListener(bookingData, position);
    }

    private void setObject(BookingData bookingData, boolean isAvailableData) {
      String consumerNo = !isAvailableData ? "Loading" : bookingData.getConsumerNo();
      String status = !isAvailableData ? "Loading" : bookingData.getStatus();
      String outletNameTypeCode = bookingData.getOutletObj().getOutletName() == null ? "-"
          : bookingData.getOutletObj().getOutletName();
      String consumerName = !isAvailableData ? "Loading" : bookingData.getConsumerName();
      String vehicleNo = !isAvailableData ? "Loading"
          : (bookingData.getVehicleNo() != null && !bookingData.getVehicleNo().isEmpty()
              ? " - " + bookingData.getVehicleNo()
              : "");
      String consumerInfo = consumerNo + " - " + consumerName + vehicleNo;
      String bookingDatePencairan = !isAvailableData ? "Loading"
          : bookingData.getBookingDate().format(Formatter.DTF_dd_MM_yyyy) + " ("
              + bookingData.getBookingTypeName() + ")";

      this.mConsumerNo.setText(consumerNo);
      this.mStatus.setText(status);
      this.mOutletNameAndCode.setText(outletNameTypeCode);

      if(!isPermissionCreate){
        this.mMenu.setVisibility(View.GONE);
      }else {
        if(Utility.OPERATION_CANCEL.equals(bookingData.getStatusCode())){
          this.mMenu.setVisibility(View.GONE);
        }else{
          if (!bookingData.isActiveWfBooking())
            this.mMenu.setVisibility(View.VISIBLE);
          else
            this.mMenu.setVisibility(View.GONE);
        }
      }

      this.mConsumerInfo.setText(consumerInfo);
      this.mBookingDate.setText(bookingDatePencairan);
    }

    private void setObjectListener(BookingData bookingData, int position) {
      this.mContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemCliked(v, bookingData, position);
        }
      });
      this.mMenu.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if (!bookingData.isActiveWfBooking())
            showPopup(v, bookingData, position);
//          mListener.onItemMenuClicked(v, bookingData, position);
        }
      });
    }

    private void showPopup(View v, BookingData bookingData, int position) {
      PopupMenu popupMenu = new PopupMenu(ctx, v);
      popupMenu.inflate(R.menu.booking_browse_popup);
      popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
          switch (item.getItemId()) {
          case R.id.action_popup_edit:
            HyperLog.d(TAG, "edit diClick bookingId[" + bookingData.getId() + "]");
            mListener.onItemMenuEdit(v, bookingData, position);
            return true;
          case R.id.action_popup_delete:
            mListener.onItemMenuDelete(v, bookingData, position);
            return true;
          default:
            return false;
          }
        }
      });
      popupMenu.show();
    }
  }
}
