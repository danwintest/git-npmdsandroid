package id.co.danwinciptaniaga.npmdsandroid.ui.bankacct;

import java.util.List;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.BankData;
import id.co.danwinciptaniaga.npmdsandroid.common.CommonService;

public class GetBanksUseCase extends BaseObservable<GetBanksUseCase.Listener, List<BankData>> {
  public interface Listener {

    void onGetBanksStarted();

    void onGetBanksSuccess(Resource<List<BankData>> response);

    void onGetBanksFailure(Resource<List<BankData>> responseError);

  }

  private final CommonService commonService;

  private final AppExecutors appExecutors;

  @Inject
  public GetBanksUseCase(Application app, CommonService commonService,
      AppExecutors appExecutors) {
    super(app);
    this.commonService = commonService;
    this.appExecutors = appExecutors;
  }

  public void load() {
    notifyStart(null, null);

    ListenableFuture<List<BankData>> newExpense = commonService.getBanks();
    Futures.addCallback(newExpense, new FutureCallback<List<BankData>>() {
      @Override
      public void onSuccess(@NullableDecl List<BankData> result) {
        notifySuccess(null, result);
      }

      @Override
      public void onFailure(Throwable t) {
        notifyFailure(null, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<List<BankData>> result) {
    listener.onGetBanksStarted();
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<List<BankData>> result) {
    listener.onGetBanksSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<List<BankData>> result) {
    listener.onGetBanksFailure(result);
  }
}
