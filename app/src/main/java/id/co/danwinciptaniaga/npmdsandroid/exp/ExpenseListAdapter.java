package id.co.danwinciptaniaga.npmdsandroid.exp;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;

import com.bumptech.glide.Glide;
import com.hypertrack.hyperlog.HyperLog;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.paging.PagingDataAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseAttachmentData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.util.Formatter;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

/**
 * {@link RecyclerView.Adapter} that can display a {@link ExpenseData}
 */
public class ExpenseListAdapter
    extends PagingDataAdapter<ExpenseData, ExpenseListAdapter.ViewHolder> {
  private final static String TAG = ExpenseListAdapter.class.getSimpleName();

  private final MyExpenseDataRecyclerViewAdapterListener mListener;
  private SparseBooleanArray selectedItems;
  private List<ExpenseData> selectedExpenseData = new ArrayList<ExpenseData>();
  private SparseBooleanArray animationItemsIndex;
  // private boolean reverseAllAnimations = false;
  // private static int currentSelectedIndex = -1;
  private Context mContext;

  @Inject
  public ExpenseListAdapter(MyExpenseDataRecyclerViewAdapterListener listener, Context ctx) {
    super(new ExpenseDataDiffCallBack());
    mListener = listener;
    selectedItems = new SparseBooleanArray();
    animationItemsIndex = new SparseBooleanArray();
    mContext = ctx;
  }

  public int getSelectedItemCount() {
    return selectedItems.size();
  }

  public List<ExpenseData> getSelectedExpenseData() {
    return selectedExpenseData;
  }

  public boolean isDeleteApproveActionActive() {
    for (ExpenseData data : selectedExpenseData) {
      if (!data.getStatusCode().equals(Utility.WF_STATUS_DRAFT)) {
        return false;
      }
    }
    return true;
  }

  public boolean isRejectActionActive() {
    for (ExpenseData data : selectedExpenseData) {
      if (!data.getStatusCode().equals(Utility.WF_STATUS_PENDING_APPROVAL)) {
        return false;
      }
    }
    return true;
  }

  public void resetAnimationIndex() {
    // reverseAllAnimations = false;
    animationItemsIndex.clear();
  }

  public void toggleSelection(int pos) {
    // currentSelectedIndex = pos;
    ExpenseData data = getItem(pos);
    if (selectedItems.get(pos, false)) {
      selectedItems.delete(pos);
      selectedExpenseData.remove(data);
      animationItemsIndex.delete(pos);
    } else {
      selectedExpenseData.add(data);
      selectedItems.put(pos, true);
      animationItemsIndex.put(pos, true);
    }
    notifyItemChanged(pos);
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_expense_item,
        parent, false);
    return new ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final ViewHolder holder, int position) {
    holder.bind(getItem(position), position);
    holder.mView.setActivated(selectedItems.get(position, false));
  }

  public void clearSelections() {
    // reverseAllAnimations = true;
    selectedItems.clear();
    selectedExpenseData.clear();
    notifyDataSetChanged();
  }

  interface MyExpenseDataRecyclerViewAdapterListener {
    void onItemClicked(View view, ExpenseData expenseData, int position);

    void onItemLongClickedListener(View v, ExpenseData expenseData, int position);
  }

  private static class ExpenseDataDiffCallBack extends DiffUtil.ItemCallback<ExpenseData> {
    @Override
    public boolean areItemsTheSame(@NonNull ExpenseData oldItem, @NonNull ExpenseData newItem) {
      return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull ExpenseData oldItem, @NonNull ExpenseData newItem) {
      return Objects.equals(oldItem, newItem);
    }
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    public final View mView;
    public final ConstraintLayout mContainer;
    public final TextView mTrxNo;
    public final TextView mStatus;
    public final TextView mOutletNameAndCode;
    public final TextView mTrxAndActDate;
    public final TextView mExpType;
    public final TextView mAmt;
    public final TextView mAdditionalInfo;
    public final ImageView mImage;

    public ExpenseData mItem;

    public ViewHolder(View view) {
      super(view);
      mView = view;
      mContainer = (ConstraintLayout) view.findViewById(R.id.clExpContainer);
      mTrxNo = (TextView) view.findViewById(R.id.tvExpItemNoTrx);
      mStatus = (TextView) view.findViewById(R.id.tvExpWfStatus);
      mOutletNameAndCode = (TextView) view.findViewById(R.id.tvExpOutletAndCode);
      mTrxAndActDate = (TextView) view.findViewById(R.id.tvExpTrxAndActDate);
      mExpType = (TextView) view.findViewById(R.id.tvExpType);
      mAmt = (TextView) view.findViewById(R.id.tvExpAmount);
      mAdditionalInfo = (TextView) view.findViewById(R.id.tvExpAdditionalInfo);
      mImage = (ImageView) view.findViewById(R.id.ivExpImg);

    }

    public void bind(ExpenseData expenseData, int position) {
      this.mItem = expenseData;
      boolean isAvailableData = expenseData != null;
      setObject(expenseData, isAvailableData);
      setObjectListener(expenseData, position);
    }

    private void setObjectListener(ExpenseData expenseData, int position) {
      this.mContainer.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          mListener.onItemClicked(v, expenseData, position);
        }
      });

      this.mContainer.setOnLongClickListener(new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
          mListener.onItemLongClickedListener(v, expenseData, position);
          v.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
          return true;
        }
      });
    }

    private void setObject(ExpenseData expenseData, boolean isAvailableData) {
      String trxNo = !isAvailableData ? "Loading"
          : StringUtils.defaultString(expenseData.getTransactionNo(), StringUtils.EMPTY);
      HyperLog.d(TAG, "setObject: trxNo ->" + trxNo);

      String status = !isAvailableData ? "Loading" : expenseData.getStatus();

      String trxDate = !isAvailableData ? "Loading"
          : expenseData.getExpenseDate().format(Formatter.DTF_dd_MM_yyyy);
      String actualDate = !isAvailableData ? "Loading "
          : expenseData.getStatementDate().format(Formatter.DTF_dd_MM_yyyy);

      String outletName = !isAvailableData ? "Loading" : expenseData.getOutletName();
      String expType = !isAvailableData ? "Loading" : expenseData.getItemName();
      // String consumenNo = !isAvailableData ? "Loading" : expenseData.get

      String expTypeId = !isAvailableData ? "Loading" : expenseData.getItemId().toString();
      String qty = "";
      if (isAvailableData) {
        boolean isItemStock = expenseData.isItemStock();
        String expQty = Formatter.DF_AMOUNT_NO_DECIMAL.format(expenseData.getQuantity());
        if (isItemStock) {
          qty = " (" + expQty + ")";
        } else {
          qty = "";
        }

      } else {
        qty = "Loading";
      }

      String amount = !isAvailableData ? "Loading"
          : Formatter.DF_AMOUNT_NO_DECIMAL.format(expenseData.getAmount());

      this.mTrxNo.setText(trxNo);
      this.mStatus.setText(status);
      this.mOutletNameAndCode.setText(outletName);
      this.mTrxAndActDate.setText(trxDate + " (" + actualDate + ")");
      this.mExpType.setText(expType + qty);
      this.mAmt.setText(amount);
      if (expenseData.getBookingData() != null) {
        BookingData bd = expenseData.getBookingData();
        this.mAdditionalInfo.setText(
            String.format("%s - %s - %s", bd.getConsumerNo(), bd.getConsumerName(),
                bd.getVehicleNo()));
      }
      // jika jenis biaya adalah Biaya Mediator, maka tampikan additional info
      if (expenseData.isItemBooking())
        this.mAdditionalInfo.setVisibility(View.VISIBLE);
      else
        this.mAdditionalInfo.setVisibility(View.GONE);

      if (expenseData.getAttachments() != null && expenseData.getAttachments().size() > 0) {
        ExpenseAttachmentData ead = expenseData.getAttachments().get(0);
        if (ead.isArchived()) {
          this.mImage.setImageResource(R.drawable.ic_baseline_archive_24);
          return;
        }
        Glide.with(mContext).load(ead.getId())
            .placeholder(R.drawable.ic_baseline_image_24)
            .error(R.drawable.ic_baseline_broken_image_24)
            .into(this.mImage);
      } else {
        Glide.with(mContext).clear(this.mImage);
        this.mImage.setImageResource(R.drawable.ic_baseline_image_24);
      }
    }
  }
}
