package id.co.danwinciptaniaga.npmdsandroid.notif;

import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hypertrack.hyperlog.HyperLog;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.preference.PreferenceManager;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.registration.RegisterDeviceUseCaseSync;
import id.co.danwinciptaniaga.androcon.utility.Utility;
import id.co.danwinciptaniaga.npmds.data.AppBaseConstants;
import id.co.danwinciptaniaga.npmds.data.notif.PnData;
import id.co.danwinciptaniaga.npmdsandroid.BuildConfig;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;

@AndroidEntryPoint
public class NpmdsNotificationService extends FirebaseMessagingService {
  private static final String TAG = NpmdsNotificationService.class.getSimpleName();
  private static final String CHANNEL_ID_MISC = "npmds-misc";
  private static final String SP_NOTIFICATION_ID = "SP_NOTIFICATION_ID";

  @Inject
  RegisterDeviceUseCaseSync ucRegisterDeviceSync;

  @Override
  public void onNewToken(@NonNull String token) {
    Log.d(TAG, "Refreshed token: " + token);
    super.onNewToken(token);
  }

  @Override
  public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
    StringBuilder msg = new StringBuilder();
    RemoteMessage.Notification notification = null;
    if (remoteMessage.getNotification() != null) {
      // app tidak pakai notification, tapi test PN pakai
      notification = remoteMessage.getNotification();
    } else {
      // msg untuk fallback, tapi harusnya server selalu mengirimkan message dan title untuk ditampilkan
      msg.append("[messageId-collapseKey]=[")
          .append(remoteMessage.getMessageId())
          .append(" - ")
          .append(remoteMessage.getCollapseKey())
          .append("]");
    }
    Log.d(TAG, "onMessageReceived: " + msg);

    NotificationManagerCompat notificationManager =
        NotificationManagerCompat.from(getApplication());
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      NotificationChannel testNc = notificationManager.getNotificationChannel(CHANNEL_ID_MISC);
      if (testNc == null) {
        testNc = new NotificationChannel(CHANNEL_ID_MISC, getString(R.string.channel_misc),
            NotificationManager.IMPORTANCE_HIGH);
        notificationManager.createNotificationChannel(testNc);
      }
    }

    PendingIntent pi = null;
    String title = null;
    String message = null;
    if (remoteMessage.getData() != null) {
      if (Utility.isDebug(BuildConfig.class)) {
        HyperLog.d(TAG, "Message contains data");
        for (String key : remoteMessage.getData().keySet()) {
          HyperLog.d(TAG, "Data: " + key + " = " + remoteMessage.getData().get(key));
        }
      }

      Map<String, String> data = remoteMessage.getData();
      title = data.get(PnData.TITLE);
      message = data.get(PnData.MESSAGE);
      String entityName = data.get(PnData.ENTITY_NAME);
      String entityId = data.get(PnData.APP_BASE_ID);
      String appType = data.get(PnData.APP_TYPE);
      String appOperation = data.get(PnData.APP_OPERATION);
      Boolean createdPermissionAllowed = Boolean.getBoolean(
          data.get(PnData.APP_CREATE_PERMISSION_ALLOWED));

      HyperLog.d(TAG,
          "data-> entityName[" + entityName + "] entityId[" + entityId + "] appType[" + appType
              + "] appOperation[" + appOperation + "] createdPermissionAllowed["
              + createdPermissionAllowed + "]");

      if (AppBaseConstants.BIAYA_OPERASIONAL.equals(appType)) {
        pi = getExpensePendingIntent(UUID.fromString(entityId));
      } else if (AppBaseConstants.PENGAJUAN_BARU_BOOKING_CASH.equals(appType)) {
        pi = getPengajuanBaruBookingCash(UUID.fromString(entityId), appOperation,
            createdPermissionAllowed);
      } else if (AppBaseConstants.PENGAJUAN_EDIT_BOOKING_CASH.equals(appType)) {
        pi = getPengajuanBaruBookingCash(UUID.fromString(entityId), appOperation,
            createdPermissionAllowed);
      } else if (AppBaseConstants.PENGAJUAN_BATAL_BOOKING_CASH.equals(appType)) {
//        pi = getPengajuanBaruBookingCash(UUID.fromString(entityId), appOperation,
//            createdPermissionAllowed);
      } else if (AppBaseConstants.PENGAJUAN_BARU_BOOKING_TRANSFER.equals(appType)) {
        pi = getPengajuanBaruBookingTransfer(UUID.fromString(entityId), appOperation,
            createdPermissionAllowed);
      } else if (AppBaseConstants.PENGAJUAN_EDIT_BOOKING_TRANSFER.equals(appType)) {
        pi = getPengajuanBaruBookingTransfer(UUID.fromString(entityId), appOperation,
            createdPermissionAllowed);
      } else if (AppBaseConstants.PENGAJUAN_BATAL_BOOKING_TRANSFER.equals(appType)) {
//        pi = getPengajuanBaruBookingTransfer(UUID.fromString(entityId), appOperation,
//            createdPermissionAllowed);
      } else if (AppBaseConstants.DROPPING_DAILY.equals(appType)) {
        pi = getDroppingDaily(UUID.fromString(entityId));
      } else if (AppBaseConstants.DROPPING_ADDITIONAL.equals(appType)) {
        pi = getDroppingAdditional(UUID.fromString(entityId));
      } else {
        HyperLog.w(TAG, "Determine PendingIntent cannot handle AppType yet: " + appType);
      }
    } else {
      HyperLog.d(TAG, "Message does not contain data");
    }

    if (pi == null) {
      // Create an Intent for the activity you want to start
      Intent resultIntent = new Intent(this, LandingActivity.class);
      // Create the TaskStackBuilder and add the intent, which inflates the back stack
      TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
      stackBuilder.addNextIntentWithParentStack(resultIntent);
      // Get the PendingIntent containing the entire back stack
      pi = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID_MISC);
    builder.setContentIntent(pi);
    builder.setPriority(NotificationCompat.PRIORITY_MAX);
    builder.setSmallIcon(R.drawable.logo);
    builder.setCategory(NotificationCompat.CATEGORY_MESSAGE);
    if (remoteMessage.getData() != null && notification != null) {
      // notifikasi hanya akan ditampilkan kalau tidak ada data (data lebih diutamakan)
      builder.setContentText(notification.getBody());
      builder.setContentTitle(notification.getTitle());
    } else {
      if (title != null) {
        builder.setContentTitle(title);
      }
      if (message != null) {
        builder.setContentText(message);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        String GROUP_KEY_WORK_EMAIL = "com.android.example.WORK_EMAIL";
        builder.setGroup(GROUP_KEY_WORK_EMAIL);
        builder.setGroupSummary(true);
      }
    }
    long[] pattern = { 0, 100, 200, 300 };
    builder.setVibrate(pattern);
    builder.setAutoCancel(true);

    Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    builder.setSound(alarmSound);


    notificationManager.notify(getCurrentNotifId(), builder.build());
    setNextNotifId();
    Log.d(TAG, "displayed Notification: " + msg);
  }

  private PendingIntent getExpensePendingIntent(UUID expenseId) {
    // buat PI untuk langsung buka ExpenseEditFragment
    // mengikuti graph, maka kalau diback, akan kembali ke ExpenseBrowseFragment
    Bundle b = new Bundle();
    b.putSerializable("expenseId", expenseId);
    PendingIntent pi = new NavDeepLinkBuilder(getApplicationContext())
        .setComponentName(LandingActivity.class)
        .setGraph(R.navigation.mobile_navigation)
        .setDestination(R.id.nav_expense_edit)
        .setArguments(b)
        .createPendingIntent();
    return pi;
  }

  private PendingIntent getPengajuanBaruBookingCash(UUID appBookingId, String appOperation,
      boolean createdPermissionAllowed) {
    Bundle b = new Bundle();
    b.putSerializable("appPBookingId", appBookingId);
    b.putSerializable("appOperation", appOperation);
    b.putSerializable("bookingType",
        id.co.danwinciptaniaga.npmdsandroid.util.Utility.BOOKING_TYPE_CASH);
    b.putSerializable("isHasCreatePermission", createdPermissionAllowed);
    PendingIntent pi = createPendingIntent(b, R.id.nav_p_booking_new);
    return pi;
  }

  private PendingIntent getPengajuanBaruBookingTransfer(UUID appBookingId, String appOperation,
      boolean createdPermissionAllowed) {
    Bundle b = new Bundle();
    b.putSerializable("appPBookingId", appBookingId);
    b.putSerializable("appOperation", appOperation);
    b.putSerializable("bookingType",
        id.co.danwinciptaniaga.npmdsandroid.util.Utility.BOOKING_TYPE_TRANSFER);
    b.putSerializable("txnMode", null);
    b.putSerializable("isHasCreatePermission", createdPermissionAllowed);
    PendingIntent pi = createPendingIntent(b, R.id.nav_p_booking_transfer_new);
    return pi;
  }

  private PendingIntent getDroppingDaily(UUID droppingId) {
    Bundle b = new Bundle();
    b.putSerializable("droppingId", droppingId);
    PendingIntent pi = createPendingIntent(b, R.id.nav_dropping_daily_edit);
    return pi;
  }

  private PendingIntent getDroppingAdditional(UUID droppingId) {
    Bundle b = new Bundle();
    b.putSerializable("droppingId", droppingId);
    PendingIntent pi = createPendingIntent(b, R.id.nav_dropping_additional_edit);
    return pi;
  }

  private PendingIntent createPendingIntent(Bundle b, int destination) {
    PendingIntent pi = new NavDeepLinkBuilder(getApplicationContext())
        .setComponentName(LandingActivity.class)
        .setGraph(R.navigation.mobile_navigation)
        .setDestination(destination)
        .setArguments(b)
        .createPendingIntent();
    return pi;
  }

  private int getCurrentNotifId() {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    int curId = sp.getInt(SP_NOTIFICATION_ID, 0);
    HyperLog.d(TAG, "currentNotifId[" + curId + "]");
    return curId;
  }

  private void setNextNotifId() {
    int nextId = getCurrentNotifId() + 1;
    if (nextId > 1000)
      nextId = 1;
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    sp.edit().putInt(SP_NOTIFICATION_ID, nextId).commit();
    HyperLog.d(TAG, "nextNotifId[" + nextId + "]");
  }
}
