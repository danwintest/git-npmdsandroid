package id.co.danwinciptaniaga.npmdsandroid.exp;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.widget.ArrayAdapter;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.CompanyData;
import id.co.danwinciptaniaga.npmds.data.common.WfStatusData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseFilterResponse;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseItemData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseStatusData;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;

public class ExpenseFilterVM extends AndroidViewModel implements ExpenseFilterUC.Listener {
  private final static String TAG = ExpenseFilterVM.class.getSimpleName();
  private MutableLiveData<FormState> formState = new MutableLiveData<>();
  private MutableLiveData<List<KeyValueModel<String, String>>> statusKvmList = new MutableLiveData<>();
  private MutableLiveData<List<KeyValueModel<String, String>>> selectedStatusKvmList = new MutableLiveData<>();
  private MutableLiveData<List<KeyValueModel<UUID,String>>> jenisBiayaKvmList = new MutableLiveData<>();
  private KeyValueModel<UUID, String> jenisBiayaKvm = null;
  private ExpenseBrowseFilter filterField = new ExpenseBrowseFilter();
  private KeyValueModel<UUID, String> companyKvm = null;
  private MutableLiveData<List<KeyValueModel<UUID, String>>> companyKvmList = new MutableLiveData<>();
  private final ExpenseFilterUC uc;

  @ViewModelInject
  public ExpenseFilterVM(@NonNull Application app, ExpenseFilterUC uc) {
    super(app);
    this.uc = uc;
    this.uc.registerListener(this);
  }

  public KeyValueModel<UUID, String> getCompanyKvm() {
    return companyKvm;
  }

  public void setCompanyKvm(KeyValueModel<UUID, String> companyKvm) {
    this.companyKvm = companyKvm;
  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getCompanyKvmList() {
    return companyKvmList;
  }

  public void setCompanyKvmList(List<CompanyData> cdList) {
    List<KeyValueModel<UUID, String>> kvmCompanyList = new ArrayList<>();
    KeyValueModel<UUID, String> kvmCompanyBlank = new KeyValueModel<>(null, null);
    kvmCompanyList.add(kvmCompanyBlank);
    for (CompanyData cd : cdList) {
      KeyValueModel<UUID, String> kvmCompany = new KeyValueModel<>(cd.getId(), cd.getCompanyName());
      HyperLog.d(TAG,"current["+this.filterField.getCompanyId()+"] - opt["+cd.getId()+"]");
      if (cd.getId().equals(this.filterField.getCompanyId())) {
        setCompanyKvm(kvmCompany);
      }
      kvmCompanyList.add(kvmCompany);
    }
    this.companyKvmList.postValue(kvmCompanyList);
  }

  public void setStatusKvm(List<WfStatusData> wsdList) {
    List<KeyValueModel<String, String>> kvmStatusList = new ArrayList<>();
    List<KeyValueModel<String, String>> kvmSelecedStatusList = new ArrayList<>();
    for (WfStatusData wsd : wsdList) {
      KeyValueModel<String, String> kvmStatus = new KeyValueModel<>(wsd.getId(), wsd.getName());
      boolean isSelected = this.filterField.getStatus() == null ?
          false :
          this.filterField.getStatus().contains(wsd.getId());
      if (isSelected) {
        kvmSelecedStatusList.add(kvmStatus);
      }
      kvmStatusList.add(kvmStatus);
    }
    setFirstSelectedStatus(kvmSelecedStatusList);
    setStatusKvmList(kvmStatusList);
  }

  public MutableLiveData<FormState> getFormState() {
    return formState;
  }

  public void setFormState(FormState formState) {
    this.formState.postValue(formState);
  }

  public MutableLiveData<List<KeyValueModel<String, String>>> getStatusKvmList() {
    return statusKvmList;
  }

  public void setStatusKvmList(List<KeyValueModel<String, String>> statusKvmList) {
    this.statusKvmList.postValue(statusKvmList);
  }

  public MutableLiveData<List<KeyValueModel<String, String>>> getSelectedStatusKvmList() {
    return selectedStatusKvmList;
  }

  public void setSelectedStatusKvmList(boolean isAdd,
      KeyValueModel<String, String> selectedStatusKvm) {
    List<KeyValueModel<String, String>> currentSelectedData = selectedStatusKvmList.getValue();
    currentSelectedData = currentSelectedData == null ? new ArrayList<>() : currentSelectedData;
    if(isAdd){
      if (!currentSelectedData.contains(selectedStatusKvm))
        currentSelectedData.add(selectedStatusKvm);
    }else{
      currentSelectedData.remove(selectedStatusKvm);
    }
    selectedStatusKvmList.postValue(currentSelectedData);
  }

  private void setFirstSelectedStatus(List<KeyValueModel<String,String>> selectedStatusKvmList){
    this.selectedStatusKvmList.postValue(selectedStatusKvmList);
  }

  public MutableLiveData<List<KeyValueModel<UUID, String>>> getJenisBiayaKvmList() {
    return jenisBiayaKvmList;
  }

  public void setJenisBiayaKvmList(List<KeyValueModel<UUID, String>> jenisBiayaKvmList) {
    this.jenisBiayaKvmList.postValue(jenisBiayaKvmList);
  }

  public KeyValueModel<UUID, String> getJenisBiayaKvm() {
    return jenisBiayaKvm;
  }

  public void setJenisBiayaKvm(KeyValueModel<UUID, String> jenisBiayaKvm) {
    this.jenisBiayaKvm = jenisBiayaKvm;
  }

  public ExpenseBrowseFilter getFilterField() {
    return filterField;
  }

  public void setFilterField(ExpenseBrowseFilter filterObj) {
    this.filterField = filterObj;
  }

  public void getStatusList() {
    uc.getStatusList();
  }

  @Override
  public void onProcessStartedGetStatusList(Resource<ExpenseFilterResponse> loading) {
    setFormState(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessSuccessGetStatusList(Resource<ExpenseFilterResponse> res) {
    List<ExpenseStatusData> esdList = res.getData().getEsdList();
    List<ExpenseItemData> eidList = res.getData().getEidList();
    List<KeyValueModel<String, String>> statusKvmList = new ArrayList<>();
    List<KeyValueModel<String, String>> selectedStatusKvmList = new ArrayList<>();
    List<CompanyData> cdList = res.getData().getCompanyDataList();
    setCompanyKvmList(cdList);

    // set status
    for (ExpenseStatusData esd : esdList) {
      KeyValueModel<String, String> statusKvm = new KeyValueModel<>(esd.getId(), esd.getName());

      boolean isSelected = getFilterField().getStatus() == null ?
          false :getFilterField().getStatus().contains(esd.getId());
      if(isSelected){
        selectedStatusKvmList.add(statusKvm);
      }
      setFirstSelectedStatus(selectedStatusKvmList);
      statusKvmList.add(statusKvm);

    }
    setStatusKvmList(statusKvmList);

    // set jenis biaya
    List<KeyValueModel<UUID, String>> jenisBiayaKvmList = new ArrayList<>();
    for (ExpenseItemData eid : eidList) {
      KeyValueModel<UUID, String> jenisBiayaKvm = new KeyValueModel<>(eid.getId(), eid.getName());
      if (eid.getId().equals(filterField.getExpenseItemId()))
        setJenisBiayaKvm(jenisBiayaKvm);

      jenisBiayaKvmList.add(jenisBiayaKvm);
    }
    setJenisBiayaKvmList(jenisBiayaKvmList);

    setFormState(FormState.ready(res.getMessage()));
  }

  @Override
  public void onProcessFailureGetStatusList(Resource<ExpenseFilterResponse> res) {
    setFormState(FormState.error(res.getMessage()));
  }
}
