package id.co.danwinciptaniaga.npmdsandroid.fintransfer.grouping;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import javax.inject.Inject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hypertrack.hyperlog.HyperLog;

import android.app.PendingIntent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.paging.LoadState;
import androidx.recyclerview.widget.ConcatAdapter;
import androidx.recyclerview.widget.RecyclerView;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.PermissionInfo;
import id.co.danwinciptaniaga.androcon.security.ProtectedFragment;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseAndroidFilter;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseHeaderData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.FragmentTugasTransferBrowseHeaderBinding;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseListLoadStateAdapter;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterFragment;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.sort.TugasTransferBrowseSortFragment;
import id.co.danwinciptaniaga.npmdsandroid.ui.NavHelper;
import id.co.danwinciptaniaga.npmdsandroid.ui.landing.LandingActivity;
import id.co.danwinciptaniaga.npmdsandroid.util.ViewUtil;
import kotlin.Unit;

@AndroidEntryPoint
public class TugasTransferBrowseHeaderFragment extends ProtectedFragment {
  private static final String TAG = TugasTransferBrowseHeaderFragment.class.getSimpleName();
  public static final String SAVED_STATE_LD_REFRESH = "refresh";
  private static final String ARG_TUGAS_TRANSFER_DATA = "tugasTransferData";

  private FragmentTugasTransferBrowseHeaderBinding binding;
  private TugasTransferHeaderAdapter adapter;
  private ConcatAdapter concatAdapter;
  private RecyclerView rv;
  private TugasTransferHeaderBrowseVM vm;

  @Inject
  AppExecutors appExecutors;

  public TugasTransferBrowseHeaderFragment() {
    // Required empty public constructor
  }

  // TODO: Rename and change types and number of parameters
  //  public static TugasTransferBrowseHeaderFragment newInstance(String param1, String param2) {
  //    TugasTransferBrowseHeaderFragment fragment = new TugasTransferBrowseHeaderFragment();
  //    Bundle args = new Bundle();
  //    args.putSerializable(ARG_TUGAS_TRANSFER_DATA,dad);
  //    return fragment;
  //  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    vm = new ViewModelProvider(requireActivity()).get(TugasTransferHeaderBrowseVM.class);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    binding = FragmentTugasTransferBrowseHeaderBinding.inflate(inflater, container, false);
    setHeaderView();
    setCekSaldoBtn();
    setRecyclerAdapterAndSwipeHeader();
    setFilterObserver();
    setSortObserver();
    return binding.getRoot();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    binding = null;
  }

  @Override
  protected void authenticationStatusUpdate(Resource<String> status) {
    HyperLog.d(TAG, "authenticationStatusUpdate called: " + status);
    if (status.getStatus() == Status.LOADING) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
      binding.progressWrapper.retryButton.setVisibility(View.GONE);
    } else if (status.getStatus() == Status.SUCCESS) {
      binding.progressWrapper.getRoot().setVisibility(View.GONE);
      binding.swipeRefresh.setVisibility(View.VISIBLE);
      onReady();
    } else if (status.getStatus() == Status.ERROR) {
      binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
      binding.progressWrapper.progressText.setText(status.getMessage());
      binding.progressWrapper.progressBar.setVisibility(View.GONE);
      binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);

      // retry = restart fragment (karena authentication gagal)
      binding.progressWrapper.retryButton.setOnClickListener(restartHeader);
    }
  }

  @Override
  public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
    inflater.inflate(R.menu.expense_browse_menu, menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    if (item.getItemId() == R.id.menu_filter) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_tugas_transfer_browse, nc.getCurrentDestination().getId()))
        return true;
      TugasTransferBrowseHeaderFragmentDirections.ActionFilter dir = TugasTransferBrowseHeaderFragmentDirections.actionFilter(
          true);
      dir.setFilterField(vm.getFilterField().getValue());
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    } else if (item.getItemId() == R.id.menu_sort) {
      NavController nc = Navigation.findNavController(binding.getRoot());
      if (!Objects.equals(R.id.nav_tugas_transfer_browse, nc.getCurrentDestination().getId()))
        return true;
      List<SortOrder> soList = new ArrayList(vm.getSortField());
      String soListJson = new Gson().toJson(soList);
      TugasTransferBrowseHeaderFragmentDirections.ActionSort dir = TugasTransferBrowseHeaderFragmentDirections.actionSort();
      dir.setSortField(soListJson);
      nc.navigate(dir, NavHelper.animChildToParent().build());
      return true;
    }
    return false;
  }

  private void onReady() {
    // harusnya permission sudah ada kalau authenticationStatusUpdate SUCCESS
    Resource<List<PermissionInfo>> permissions = basemodel.getPermissionsLd().getValue();

    // lalu gunakan LiveData observer untuk mengupdate view
    vm.getLoadStateHeader().observe(getViewLifecycleOwner(), state -> {
      HyperLog.d(TAG, "TugasTransferGroupList changed: " + state);
      if (state instanceof LoadState.Loading) {
        // sedang loading pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.GONE);
      } else if (state instanceof LoadState.Error) {
        // terjadi error pada waktu pertama kali (page pertama)
        binding.progressWrapper.getRoot().setVisibility(View.VISIBLE);
        binding.progressWrapper.progressBar.setVisibility(View.GONE);
        binding.progressWrapper.retryButton.setVisibility(View.VISIBLE);

        // retry = reload semua data
        binding.progressWrapper.retryButton.setOnClickListener(reloadFragmentHeader);
        // sembunyikan list, karena retry akan dihandle oleh button di atas
        binding.swipeRefresh.setVisibility(View.GONE);

        binding.progressWrapper.progressText.setVisibility(View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            ((LoadState.Error) state).getError().getMessage());
      } else if (state instanceof LoadState.NotLoading) {
        // sudah ok
        binding.progressWrapper.getRoot().setVisibility(View.GONE);
        binding.swipeRefresh.setVisibility(View.VISIBLE);
      }
    });
    // connect data ke adapter
    // supaya tidak hit server lagi pada waktu kembali dari child fragment, hanya load data kalau
    // list masih null
    vm.getHeaderList().observe(getViewLifecycleOwner(), pagingData -> {
      adapter.submitData(getLifecycle(), pagingData);
    });
  }

  private void setHeaderView() {
    vm.getFilterField().observe(getViewLifecycleOwner(), data -> {
      binding.tvCompanyName.setText(data.getCompanyName() != null ? data.getCompanyName() : "-");
      binding.tvPendingTask.setText(data.getPendingTask() != null ?
          (data.getPendingTask().booleanValue() == true ? "Ya" : "Tidak") :
          "-");
      binding.tvJenisTransfer.setText(
          data.getTransferName() != null ? data.getTransferName() : "-");
      binding.tvRekeningSumber.setText(
          data.getSourceAccountName() != null ? data.getSourceAccountName() : "-");
      binding.tvBatch.setText(data.getBatchName() != null ? data.getBatchName() : "-");
      binding.tvTransferStatus.setText(
          data.getTransferStatusName() != null ? data.getTransferStatusName() : "-");
      binding.tvJenisTransaksi.setText(
          data.getTransactionTypeName() != null ? data.getTransactionTypeName() : "-");

    });
  }

  private void setCekSaldoBtn() {
    binding.btnCekSaldoPT.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String companyName = vm.getFilterField().getValue().getCompanyName();
        UUID companyId = vm.getFilterField().getValue().getCompanyId();
        String companyCode = vm.getFilterField().getValue().getCompanyCode();
        NavController nc = Navigation.findNavController(binding.getRoot());
        TugasTransferBrowseHeaderFragmentDirections.ActionCekSaldo dir = TugasTransferBrowseHeaderFragmentDirections.actionCekSaldo(
            companyId, companyName, companyCode);
        nc.navigate(dir, NavHelper.animChildToParent().build());
      }
    });
  }

  private void setRecyclerAdapterAndSwipeHeader() {
    rv = binding.listHeader;
    adapter = new TugasTransferHeaderAdapter(new TugasTransferHeaderAdapter.ItemListener() {
      @Override
      public void onItemClicked(View v, TugasTransferBrowseHeaderData data, int pos) {
        NavController nc = Navigation.findNavController(binding.getRoot());
        TugasTransferBrowseAndroidFilter filterField = vm.getFilterField().getValue();
        if (filterField != null) {
          filterField.setBankId(data.getBankId());
          filterField.setBankName(data.getBankName());
        }
        TugasTransferBrowseHeaderFragmentDirections.ActionDetail dir = TugasTransferBrowseHeaderFragmentDirections.actionDetail(
            filterField);
        nc.navigate(dir, NavHelper.animParentToChild().build());
      }
    }, getActivity().getApplicationContext(), appExecutors);
    concatAdapter = adapter.withLoadStateHeaderAndFooter(
        new ExpenseListLoadStateAdapter(retryCallBack),
        new ExpenseListLoadStateAdapter(retryCallBack));
    adapter.addLoadStateListener((combinedLoadStates) -> {
      vm.setLoadStateHeader(combinedLoadStates.getRefresh());
      return Unit.INSTANCE;
    });

    rv.setAdapter(concatAdapter);

    vm.getRefreshHeaderList().observe(getViewLifecycleOwner(), e -> {
      if (e != null && e) {
        adapter.refresh();
      }
    });

    vm.getActionEvent().observe(getViewLifecycleOwner(), s -> {
      HyperLog.d(TAG, "getActionEvent[" + s + "]");
      if (Status.SUCCESS.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
        adapter.refresh();
      } else if (Status.LOADING.equals(s.getStatus())) {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.VISIBLE);
        ViewUtil.setVisibility(binding.progressWrapper.progressText, View.VISIBLE);
        binding.progressWrapper.progressText.setText(
            s.getMessage() == null ? getString(R.string.msg_please_wait) : s.getMessage());
        ViewUtil.setVisibility(binding.progressWrapper.retryButton, View.GONE);
      } else {
        ViewUtil.setVisibility(binding.progressWrapper.getRoot(), View.GONE);
        Toast.makeText(getContext(), s.getMessage(), Toast.LENGTH_LONG).show();
      }
    });

    NavController nc = NavHostFragment.findNavController(this);

    LiveData<Boolean> refreshLd = nc.getCurrentBackStackEntry()
        .getSavedStateHandle().getLiveData(SAVED_STATE_LD_REFRESH);
    refreshLd.observe(getViewLifecycleOwner(), refresh -> {
      HyperLog.d(TAG, "refresh signaled: " + refresh);
      // tidak bisa langsung adapter.refresh, sepertinya adapternya belum connect ke RecyclerView-nya
      // jadi kita tunda dengan sinyal melalui viewmodel
      vm.setRefreshHeaderList(refresh);
      nc.getCurrentBackStackEntry().getSavedStateHandle()
          .remove(SAVED_STATE_LD_REFRESH);
    });

    binding.swipeRefresh.setOnRefreshListener(() -> {
      adapter.refresh();
      binding.swipeRefresh.setRefreshing(false);
    });
  }

  private View.OnClickListener retryCallBack = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      adapter.retry();
    }
  };

  private View.OnClickListener restartHeader = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      PendingIntent pendingIntent = new NavDeepLinkBuilder(getActivity().getApplicationContext())
          .setComponentName(LandingActivity.class)
          .setDestination(R.id.nav_tugas_transfer_browse)
          .setGraph(R.navigation.mobile_navigation)
          .createPendingIntent();
      try {
        pendingIntent.send();
      } catch (PendingIntent.CanceledException e) {
        HyperLog.exception(TAG, e);
      }
    }
  };

  private View.OnClickListener reloadFragmentHeader = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
      // reload fragment ini = memuat ulang seluruh list
      adapter.refresh();
      // karena menggunakan progress indicator sendiri, jadi  tidak perlu yg dari swipeRefresh
      binding.swipeRefresh.setRefreshing(false);
    }
  };

  private void setFilterObserver() {
    HyperLog.d(TAG, "setFilterObserver()");
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<TugasTransferBrowseAndroidFilter> filterField = nc
        .getCurrentBackStackEntry()
        .getSavedStateHandle()
        .getLiveData(TugasTransferFilterFragment.TUGAS_TRANSFER_FILTER_FIELD_GROUPING);
    filterField.observe(getViewLifecycleOwner(), filterObj -> {
      HyperLog.d(TAG, "filterField.observe");
      vm.loadHeaderListWithFilter(filterObj);
//      nc.getCurrentBackStackEntry().getSavedStateHandle()
//          .remove(TugasTransferFilterFragment.TUGAS_TRANSFER_FILTER_FIELD_GROUPING);
    });
  }

  private void setSortObserver() {
    NavController nc = NavHostFragment.findNavController(this);
    MutableLiveData<String> soListJsonMld = nc.getCurrentBackStackEntry().getSavedStateHandle().getLiveData(
        TugasTransferBrowseSortFragment.TUGAS_TRANSFER_HEADER_SORT);

    soListJsonMld.observe(getViewLifecycleOwner(), soListJsonObj -> {
      String soListJson = soListJsonObj.replace("\\", "");
      Type dataType = new TypeToken<List<SortOrder>>() {
      }.getType();
      List<SortOrder> soList = new Gson().fromJson(soListJson, dataType);
      Set<SortOrder> sso = new HashSet<SortOrder>(soList);
      vm.setSortField(sso);
      nc.getCurrentBackStackEntry().getSavedStateHandle().remove(
          TugasTransferBrowseSortFragment.TUGAS_TRANSFER_HEADER_SORT);
    });
  }
}