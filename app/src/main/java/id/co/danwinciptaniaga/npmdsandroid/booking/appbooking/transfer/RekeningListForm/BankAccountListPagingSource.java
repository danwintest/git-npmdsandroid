package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking.transfer.RekeningListForm;

import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBankListResponse;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingListResponse;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;

public class BankAccountListPagingSource extends ListenableFuturePagingSource<Integer, BankAccountData> {
  private static final String TAG = BankAccountListPagingSource.class.getSimpleName();
  private final BankAccountBrowseUC bankAccountBrowseUC;
  private final Executor mBgExecutor;

  public BankAccountListPagingSource(BankAccountBrowseUC bankAccountBrowseUC, Executor mBgExecutor) {
    this.bankAccountBrowseUC = bankAccountBrowseUC;
    this.mBgExecutor = mBgExecutor;
  }

  @NotNull
  @Override
  public ListenableFuture<LoadResult<Integer, BankAccountData>> loadFuture(
      @NotNull LoadParams<Integer> loadParams) {
    Integer nextPageNumber = loadParams.getKey();
    if (nextPageNumber == null) {
      nextPageNumber = 1;
    }
    int pageSize = loadParams.getPageSize();
    ListenableFuture<LoadResult<Integer, BankAccountData>> pageFuture = Futures.transform(
        bankAccountBrowseUC.getBankAccountDataList(nextPageNumber, pageSize), this::toLoadResult,
        mBgExecutor);
    return Futures.catching(pageFuture, Exception.class, input -> {
      HyperLog.e(TAG, "Error while getting appBookingList", input);
      return new LoadResult.Error(input);
    }, mBgExecutor);
  }

  private LoadResult<Integer, BankAccountData> toLoadResult(
      @NonNull AppBookingBankListResponse response) {
    return new LoadResult.Page<>(response.getBankAccountDataList(), null, response.getNextPageNumber(),
        LoadResult.Page.COUNT_UNDEFINED, LoadResult.Page.COUNT_UNDEFINED);
  }
}


