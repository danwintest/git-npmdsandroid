package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferProcessResponse;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class TugasTransferWfProcessUC
    extends BaseObservable<TugasTransferWfProcessUC.Listener, TugasTransferProcessResponse> {
  private final String TAG = TugasTransferWfFormUC.class.getSimpleName();
  private final TugasTransferService service;
  private final Application app;
  private final AppExecutors appExecutors;
  String doneMsg;
  String failMsg;

  @Inject
  public TugasTransferWfProcessUC(Application app, TugasTransferService service,
      AppExecutors appExecutors) {
    super(app);
    this.app = app;
    this.service = service;
    this.appExecutors = appExecutors;
    doneMsg = this.app.getString(R.string.process_success);
    failMsg = this.app.getString(R.string.process_fail);
  }

  public void processApproveTransferOtomatis(String comment, LocalDate transferDate,
      UUID sourceAccId, List<TugasTransferBrowseDetailData> dataList) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    List<TugasTransferBrowseDetailData> clearDataList = new ArrayList<>();
    for (TugasTransferBrowseDetailData clearData : dataList) {
      clearData.setStartDate(null);
      clearData.setEndDate(null);
      clearData.setTransactionDate(null);
      clearDataList.add(clearData);
    }
    String dataJson = new Gson().toJson(clearDataList);
    ListenableFuture<TugasTransferProcessResponse> process = service.processApproveTransferOtomatis(
        dataJson, comment, transferDate, sourceAccId);
    Futures.addCallback(process, new FutureCallback<TugasTransferProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl TugasTransferProcessResponse result) {
        notifySuccess(doneMsg, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(failMsg + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processApproveTransferManual(String comment, LocalDate transferDate,
      UUID sourceAccId, List<TugasTransferBrowseDetailData> dataList) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    List<TugasTransferBrowseDetailData> clearDataList = new ArrayList<>();
    for (TugasTransferBrowseDetailData clearData : dataList) {
      clearData.setStartDate(null);
      clearData.setEndDate(null);
      clearData.setTransactionDate(null);
      clearDataList.add(clearData);
    }
    String dataJson = new Gson().toJson(clearDataList);
    ListenableFuture<TugasTransferProcessResponse> process = service.processApproveTransferManual(
        dataJson, comment, transferDate, sourceAccId);
    Futures.addCallback(process, new FutureCallback<TugasTransferProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl TugasTransferProcessResponse result) {
        notifySuccess(doneMsg, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(failMsg + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processReturn(String comment, List<TugasTransferBrowseDetailData> dataList) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    List<TugasTransferBrowseDetailData> clearDataList = new ArrayList<>();
    for (TugasTransferBrowseDetailData clearData : dataList) {
      clearData.setStartDate(null);
      clearData.setEndDate(null);
      clearData.setTransactionDate(null);
      clearDataList.add(clearData);
    }
    String dataJson = new Gson().toJson(clearDataList);
    ListenableFuture<TugasTransferProcessResponse> process = service.processReturn(
        dataJson, comment);
    Futures.addCallback(process, new FutureCallback<TugasTransferProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl TugasTransferProcessResponse result) {
        notifySuccess(doneMsg, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(failMsg + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processReject(String comment, List<TugasTransferBrowseDetailData> dataList) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    List<TugasTransferBrowseDetailData> clearDataList = new ArrayList<>();
    for (TugasTransferBrowseDetailData clearData : dataList) {
      clearData.setStartDate(null);
      clearData.setEndDate(null);
      clearData.setTransactionDate(null);
      clearDataList.add(clearData);
    }
    String dataJson = new Gson().toJson(clearDataList);
    ListenableFuture<TugasTransferProcessResponse> process = service.processReject(
        dataJson, comment);
    Futures.addCallback(process, new FutureCallback<TugasTransferProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl TugasTransferProcessResponse result) {
        notifySuccess(doneMsg, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(failMsg + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  public void processApprove(String comment, List<TugasTransferBrowseDetailData> dataList, String otp, boolean isManual) {
    notifyStart(this.app.getString(R.string.silahkan_tunggu), null);
    List<TugasTransferBrowseDetailData> clearDataList = new ArrayList<>();
    for (TugasTransferBrowseDetailData clearData : dataList) {
      clearData.setStartDate(null);
      clearData.setEndDate(null);
      clearData.setTransactionDate(null);
      clearDataList.add(clearData);
    }
    String dataJson = new Gson().toJson(clearDataList);
    ListenableFuture<TugasTransferProcessResponse> process = service.processApprove(
        dataJson, comment, otp, isManual);
    Futures.addCallback(process, new FutureCallback<TugasTransferProcessResponse>() {
      @Override
      public void onSuccess(@NullableDecl TugasTransferProcessResponse result) {
        notifySuccess(doneMsg, result);
      }

      @Override
      public void onFailure(Throwable t) {
        String message = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure(failMsg + message, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<TugasTransferProcessResponse> result) {
    listener.onProcessWFStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<TugasTransferProcessResponse> result) {
    listener.onProcessWFSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<TugasTransferProcessResponse> result) {
    listener.onProcessWFFailure(result);
  }

  public interface Listener {
    void onProcessWFStarted(Resource<TugasTransferProcessResponse> loading);

    void onProcessWFSuccess(Resource<TugasTransferProcessResponse> res);

    void onProcessWFFailure(Resource<TugasTransferProcessResponse> res);
  }
}
