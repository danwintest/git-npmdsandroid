package id.co.danwinciptaniaga.npmdsandroid.droppingdaytask.detail;

import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyTaskBrowseDetailResponse;

public class DroppingDailyTaskBrowseDetailPagingSource
    extends ListenableFuturePagingSource<Integer, DroppingDailyTaskBrowseDetailData> {
  private static final String TAG = DroppingDailyTaskBrowseDetailPagingSource.class.getSimpleName();
  private final GetDroppingDailyTaskBrowseDetailUC uc;
  private final Executor mBgExecutor;

  public DroppingDailyTaskBrowseDetailPagingSource(GetDroppingDailyTaskBrowseDetailUC uc,
      Executor mBgExecutor) {
    this.uc = uc;
    this.mBgExecutor = mBgExecutor;
  }

  @NotNull
  @Override
  public ListenableFuture<LoadResult<Integer, DroppingDailyTaskBrowseDetailData>> loadFuture(
      @NotNull LoadParams<Integer> loadParams) {
    Integer nextPageNumber = loadParams.getKey();
    if (nextPageNumber == null) {
      nextPageNumber = 1;
    }
    int pageSize = loadParams.getPageSize();
    ListenableFuture<LoadResult<Integer, DroppingDailyTaskBrowseDetailData>> pageFuture = Futures.transform(
        uc.getDroppingTaskBrowseDetailList(nextPageNumber, pageSize), this::toLoadResult,
        mBgExecutor);
    return Futures.catching(pageFuture, Exception.class, input -> {
      HyperLog.e(TAG, "ERROR while getting DroppingDailyTaskBrowseDetailList", input);
      ErrorResponse er = RetrofitUtility.parseErrorBody(input);
      return new LoadResult.Error(
          er != null ? new Exception(er.getFormattedMessage(), input) : input);
    }, mBgExecutor);
  }

  private LoadResult<Integer, DroppingDailyTaskBrowseDetailData> toLoadResult(@NonNull
      DroppingDailyTaskBrowseDetailResponse response) {
    return new LoadResult.Page<>(response.getDroppingDailyTaskBrowseDetailDataList(), null,
        response.getNextPageNumber(), LoadResult.Page.COUNT_UNDEFINED,
        LoadResult.Page.COUNT_UNDEFINED);
  }
}
