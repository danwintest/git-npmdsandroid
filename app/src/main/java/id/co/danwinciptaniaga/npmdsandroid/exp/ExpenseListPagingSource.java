package id.co.danwinciptaniaga.npmdsandroid.exp;

import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseListResponse;

public class ExpenseListPagingSource extends ListenableFuturePagingSource<Integer, ExpenseData> {
  private static final String TAG = ExpenseListPagingSource.class.getSimpleName();
  private final GetExpenseListUseCase expenseListUseCase;
  private final Executor mBgExecutor;

  public ExpenseListPagingSource(GetExpenseListUseCase expenseListUseCase, Executor mBgExecutor) {
    this.expenseListUseCase = expenseListUseCase;
    this.mBgExecutor = mBgExecutor;
  }

  @NotNull
  @Override
  public ListenableFuture<LoadResult<Integer, ExpenseData>> loadFuture(
      @NotNull LoadParams<Integer> loadParams) {
    // Start refresh at page 1 if undefined.
    Integer nextPageNumber = loadParams.getKey();
    if (nextPageNumber == null) {
      nextPageNumber = 1;
    }
    // TODO: cek apakah perlu fix?
    // kalau pakai LoadSize, pertama kali (nextPageNumber=1) = 3 * PageSize
    // tapi ini mengakibatkan pada nextPageNumber=2, data yang terbaca adalah data yg sudah didapatkan pada pertama kali
    // sehingga data menjadi duplikat
    int pageSize = loadParams.getPageSize();

    ListenableFuture<LoadResult<Integer, ExpenseData>> pageFuture = Futures.transform(
        expenseListUseCase.getExpenseList(nextPageNumber, pageSize),
        this::toLoadResult, mBgExecutor);
    return Futures.catching(
        pageFuture, Exception.class,
        input -> {
          HyperLog.e(TAG, "Error while getting ExpenseList", input);
          ErrorResponse er = RetrofitUtility.parseErrorBody(input);
          return new LoadResult.Error(
              er != null ? new Exception(er.getFormattedMessage(), input) : input);
        }, mBgExecutor);
  }

  private LoadResult<Integer, ExpenseData> toLoadResult(
      @NonNull ExpenseListResponse response) {
    return new LoadResult.Page<>(
        response.getExpenseData(),
        null, // Only paging forward.
        response.getNextPageNumber(),
        LoadResult.Page.COUNT_UNDEFINED,
        LoadResult.Page.COUNT_UNDEFINED);
  }
}
