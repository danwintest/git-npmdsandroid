package id.co.danwinciptaniaga.npmdsandroid.droppingadd;

import java.util.Set;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.ListResponse;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseSort;

public class GetDroppingAdditionalListUseCase {

  private final DroppingAdditionalService droppingService;

  private DroppingBrowseSort sort = new DroppingBrowseSort();
  private DroppingBrowseFilter filter = new DroppingBrowseFilter();

  @Inject
  public GetDroppingAdditionalListUseCase(DroppingAdditionalService droppingService) {
    this.droppingService = droppingService;
  }

  public ListenableFuture<ListResponse<DroppingAdditionalData>> getDroppingAdditionalList(
      Integer page, int pageSize) {
    ListenableFuture<ListResponse<DroppingAdditionalData>> result = droppingService.getDroppingAdditionalList(
        page, pageSize, filter, sort);
    return result;
  }

  public void setFilter(DroppingBrowseFilter filter) {
    this.filter = filter;
  }

  public void setSort(Set<SortOrder> sortOrders) {
    this.sort.setSortOrders(sortOrders);
  }
}
