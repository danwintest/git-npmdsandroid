package id.co.danwinciptaniaga.npmdsandroid.exp.edit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.ImageUtility;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseAttachmentData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseData;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseDetailResponse;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseEditHelper;
import id.co.danwinciptaniaga.npmds.data.expense.ExpenseItemData;
import id.co.danwinciptaniaga.npmds.data.expense.NewExpenseResponse;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.data.ConstraintViolation;
import id.co.danwinciptaniaga.npmdsandroid.exp.ExpenseService;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import okhttp3.ResponseBody;

public class ExpenseEditViewModel extends AndroidViewModel
    implements PrepareNewExpenseUseCase.Listener, ExpenseSaveDraftUseCase.Listener,
    LoadExpenseUseCase.Listener, ExpenseAocDecideUseCase.Listener {
  private static final String TAG = ExpenseEditViewModel.class.getSimpleName();
  private final ExpenseService expenseService;
  private final UtilityService utilityService;
  private final LoginUtil loginUtil;
  private final AppExecutors appExecutors;
  private final PrepareNewExpenseUseCase ucPrepareNewExpense;
  private final ExpenseSaveDraftUseCase ucExpenseSaveDraft;
  private final ExpenseAocDecideUseCase ucExpenseAocDecideUseCase;
  private final LoadExpenseUseCase ucLoadExpense;
  private final ListeningExecutorService bgExecutor;

  // data awal dan LOV
  private boolean initialized = false;
  private UUID expenseId;
  private boolean outletIdOk = false;
  private boolean statementDateOk = false;
  private boolean expenseDateOk = false;
  private boolean expenseItemIdOk = false;
  private boolean amountOk = false;
  private boolean quantityOk = false;
  private boolean bookingOk = false;

  private NewExpenseResponse newExpenseResponse = null;
  private ExpenseDetailResponse expenseDetailResponse = null;
  private ExpenseEditHelper.Mode editPageMode = null;
  private boolean allowBackdate = false;

  private MutableLiveData<Boolean> archived = new MutableLiveData<>();
  private MutableLiveData<ExpenseFormState> formState = new MutableLiveData<>(null);
  private MutableLiveData<Resource<ExpenseData>> actionEvent = new SingleLiveEvent<>();

  private MutableLiveData<List<OutletShortData>> outletsLd = new MutableLiveData<>();
  // kita perlu data lengkap ExpenseItemData dan disimpan dalam bentuk KVM, yang dapat langsung
  // digunakan pada adapter-nya
  private MutableLiveData<List<KeyValueModel<ExpenseItemData, String>>> expenseItems =
      new MutableLiveData<>(null);

  private MutableLiveData<KeyValueModel<OutletShortData, String>> outletLd = new MutableLiveData<>();
  private MutableLiveData<String> outletErrorLd = new MutableLiveData<>();
  private MutableLiveData<LocalDate> statementDateLd = new MutableLiveData<>();
  private MutableLiveData<String> statementDateErrorLd = new MutableLiveData<>();
  private MutableLiveData<LocalDate> expenseDateLd = new MutableLiveData<>();
  private MutableLiveData<String> expenseDateErrorLd = new MutableLiveData<>();
  // isi ExpenseItem harus ada index-nya
  private MutableLiveData<KeyValueModel<ExpenseItemData, String>> expenseItemLd = new MutableLiveData<>();
  private MutableLiveData<String> expenseItemErrorLd = new MutableLiveData<>();
  private MutableLiveData<Integer> amountLd = new MutableLiveData<>();
  private MutableLiveData<String> amountErrorLd = new MutableLiveData<>();
  private MutableLiveData<Boolean> showQuantityLd = new MutableLiveData<>(false);
  private Boolean showQtyVal = false;
  private MutableLiveData<Integer> quantityLd = new MutableLiveData<>();
  private MutableLiveData<String> quantityErrorLd = new MutableLiveData<>();
  private MutableLiveData<Boolean> showBookingLd = new MutableLiveData<>(false);
  private Boolean showBookingVal = false;
  private MutableLiveData<BookingData> bookingDataLd = new MutableLiveData<>();
  private MutableLiveData<String> bookingErrorLd = new MutableLiveData<>();
  private MutableLiveData<String> descriptionLd = new MutableLiveData<>();
  private MutableLiveData<String> descriptionErrorLd = new MutableLiveData<>();
  private File tempAttachment = null;
  private File attachmentFinal = null;
  private ExpenseAttachmentData expenseAttachmentData = null;
  private MutableLiveData<Resource<Bitmap>> reducedAttachmentLd = new MutableLiveData<>();
  private MutableLiveData<Boolean> canProcess = new MutableLiveData<>(false);
  private MutableLiveData<String> headerStatusLd = new MutableLiveData<>();
  private MutableLiveData<String> headerCompanyNameLd = new MutableLiveData<>();
  private MutableLiveData<String> headerOutletNameLd = new MutableLiveData<>();

  @ViewModelInject
  public ExpenseEditViewModel(@NonNull Application application,
      PrepareNewExpenseUseCase ucPrepareNewExpense, ExpenseSaveDraftUseCase ucExpenseSaveDraft,
      LoadExpenseUseCase ucLoadExpense, ExpenseAocDecideUseCase expenseAocDecideUseCase,
      ExpenseService expenseService, UtilityService utilityService, LoginUtil loginUtil,
      AppExecutors appExecutors) {
    super(application);
    this.ucPrepareNewExpense = ucPrepareNewExpense;
    this.ucExpenseSaveDraft = ucExpenseSaveDraft;
    this.ucLoadExpense = ucLoadExpense;
    this.ucExpenseAocDecideUseCase = expenseAocDecideUseCase;

    this.ucPrepareNewExpense.registerListener(this);
    this.ucExpenseSaveDraft.registerListener(this);
    this.ucLoadExpense.registerListener(this);
    this.ucExpenseAocDecideUseCase.registerListener(this);

    this.expenseService = expenseService;
    this.utilityService = utilityService;
    this.loginUtil = loginUtil;
    this.appExecutors = appExecutors;
    bgExecutor = MoreExecutors.listeningDecorator((ExecutorService) appExecutors.backgroundIO());

  }

  public void prepareNewExpense(boolean force) {
    if (force || !initialized) {
      this.ucPrepareNewExpense.prepare();

      // todo cek aman untuk menggunakan ListenableFuture dari Retrofit di ViewModel?
      ListenableFuture<List<ExpenseItemData>> expenseItemList = expenseService.getExpenseItemList();

      Futures.addCallback(expenseItemList, new FutureCallback<List<ExpenseItemData>>() {
            @Override
            public void onSuccess(@NullableDecl List<ExpenseItemData> result) {
              List<KeyValueModel<ExpenseItemData, String>> kvmList = result.stream().map(
                  eid -> new KeyValueModel<ExpenseItemData, String>(eid,
                      eid.getName())).collect(Collectors.toList());
              expenseItems.postValue(kvmList);
            }

            @Override
            public void onFailure(Throwable t) {
              HyperLog.e(TAG, "Problem geting list of Expense Items", t);
              // todo handle error
            }
          },
          // gunakan backgroundIO ketika callback dipanggil, karena bukan lagi networkIO
          appExecutors.backgroundIO());
    }
  }

  public MutableLiveData<Boolean> isArchived() {
    return archived;
  }

  public void setArchived(Boolean archived) {
    this.archived.postValue(archived);
  }

  public void setExpenseId(UUID expenseId) {
    this.expenseId = expenseId;
  }

  public UUID getExpenseId() {
    return expenseId;
  }

  @Override
  protected void onCleared() {
    if (this.reducedAttachmentLd.getValue() != null
        && this.reducedAttachmentLd.getValue().getData() != null) {
      this.reducedAttachmentLd.getValue().getData().recycle();
    }
    super.onCleared();
    this.ucPrepareNewExpense.unregisterListener(this);
    this.ucExpenseSaveDraft.unregisterListener(this);
    this.ucLoadExpense.unregisterListener(this);
    this.ucExpenseAocDecideUseCase.unregisterListener(this);
  }

  @Override
  public void onPrepareNewExpenseStarted() {
    formState.postValue(ExpenseFormState.loading(
        getApplication().getString(R.string.mempersiapkan_form)));
  }

  @Override
  public void onPrepareNewExpenseSuccess(Resource<NewExpenseResponse> response) {
    newExpenseResponse = response.getData();

    editPageMode = ExpenseEditHelper.Mode.KASIR_DRAFT;
    allowBackdate = newExpenseResponse.isBackdateAllowed();
    outletsLd.postValue(newExpenseResponse.getAssignedOutlets());
    setStatementDate(newExpenseResponse.getToday());
    setExpenseDate(newExpenseResponse.getToday());
    setArchived(false);

    initialized = true;
    formState.postValue(ExpenseFormState.ready(null));
  }

  @Override
  public void onPrepareNewExpenseFailure(Resource<NewExpenseResponse> responseError) {
    newExpenseResponse = null;
    // todo terjemahkan jenis-jenis error
    String message = Resource.getMessageFromError(responseError, "Terjadi kesalahan");
    formState.postValue(ExpenseFormState.error(message));
  }

  public NewExpenseResponse getNewExpenseResponse() {
    return newExpenseResponse;
  }

  public ExpenseDetailResponse getExpenseDetailResponse() {
    return expenseDetailResponse;
  }

  public ExpenseEditHelper.Mode getEditPageMode() {
    return editPageMode;
  }

  public LiveData<Resource<ExpenseData>> getActionEvent() {
    return actionEvent;
  }

  public LiveData<ExpenseFormState> getFormState() {
    return formState;
  }

  public LiveData<List<KeyValueModel<ExpenseItemData, String>>> getExpenseItems() {
    return expenseItems;
  }

  public boolean isAllowBackdate() {
    return allowBackdate;
  }

  public LiveData<List<OutletShortData>> getOutletsLd() {
    return outletsLd;
  }

  public LiveData<LocalDate> getStatementDateLd() {
    return statementDateLd;
  }

  public LiveData<KeyValueModel<ExpenseItemData, String>> getExpenseItemLd() {
    return expenseItemLd;
  }

  public LiveData<String> getStatementDateErrorLd() {
    return statementDateErrorLd;
  }

  public LiveData<LocalDate> getExpenseDateLd() {
    return expenseDateLd;
  }

  public LiveData<String> getExpenseDateErrorLd() {
    return expenseDateErrorLd;
  }

  public LiveData<Integer> getAmountLd() {
    return amountLd;
  }

  public LiveData<String> getAmountErrorLd() {
    return amountErrorLd;
  }

  public LiveData<Boolean> getShowQuantityLd() {
    return showQuantityLd;
  }

  public LiveData<Integer> getQuantityLd() {
    return quantityLd;
  }

  public LiveData<String> getQuantityErrorLd() {
    return quantityErrorLd;
  }

  public LiveData<Boolean> getShowBookingLd() {
    return showBookingLd;
  }

  public LiveData<String> getBookingErrorLd() {
    return bookingErrorLd;
  }

  public LiveData<BookingData> getBookingDataLd() {
    return bookingDataLd;
  }

  public void setBookingData(BookingData bookingData) {
    HyperLog.d(TAG, "setBookingData: " + bookingData);
    try {
//      if (showBookingLd.getValue()) {
      if (showBookingVal) {
        // hanya kalau ditampilkan, maka dilakukan validasi
        ExpenseValidationHelper.validateBooking(getApplication(), bookingData);
      }
      if (bookingData != bookingDataLd.getValue()) {
        this.bookingDataLd.postValue(bookingData);
      }
      bookingErrorLd.postValue(null);
      bookingOk = true;
    } catch (ConstraintViolation constraintViolation) {
      bookingErrorLd.postValue(constraintViolation.getMessage());
      bookingOk = false;
    }
    checkCanProcess();
  }

  public LiveData<String> getDescriptionLd() {
    return descriptionLd;
  }

  public LiveData<String> getDescriptionErrorLd() {
    return descriptionErrorLd;
  }

  public File getFinalAttachment() {
    return attachmentFinal;
  }

  public LiveData<Resource<Bitmap>> getReducedAttachmentLd() {
    return reducedAttachmentLd;
  }

  public LiveData<Boolean> getCanProcess() {
    return canProcess;
  }

  public void setStatementDate(LocalDate statementDate) {
    LocalDate today = newExpenseResponse.getToday();
    try {
      HyperLog.d(TAG, "validating statement date: " + statementDate);
      ExpenseValidationHelper.validateStatementDate(getApplication(), today, statementDate);
      statementDateLd.postValue(statementDate);
      statementDateErrorLd.postValue(null);
      statementDateOk = true;
    } catch (ConstraintViolation constraintViolation) {
      HyperLog.w(TAG, "Validation failed for Expense Date", constraintViolation);
      statementDateErrorLd.postValue(constraintViolation.getMessage());
      statementDateOk = false;
    }
    checkCanProcess();
  }

  public void setExpenseDate(LocalDate expenseDate) {
    LocalDate today = newExpenseResponse.getToday();
    try {
      HyperLog.d(TAG, "validating expense date: " + expenseDate);
      ExpenseValidationHelper.validateExpenseDate(getApplication(), today, expenseDate);
      expenseDateLd.postValue(expenseDate);
      expenseDateErrorLd.postValue(null);
      expenseDateOk = true;
    } catch (ConstraintViolation constraintViolation) {
      HyperLog.w(TAG, "Validation failed for Expense Date", constraintViolation);
      expenseDateErrorLd.postValue(constraintViolation.getMessage());
      expenseDateOk = false;
    }
    checkCanProcess();
  }

  private void checkCanProcess() {
    // contoh pengecekan secara async
    // untuk kasus ini, karena logic pengecekannya cepat, sebetulnya bisa secara sync saja
    ListenableFuture<Boolean> checkCanProcess = bgExecutor.submit(new Callable<Boolean>() {
      @Override
      public Boolean call() throws Exception {
        if (outletIdOk && statementDateOk && expenseDateOk && expenseItemIdOk && bookingOk
            && amountOk && quantityOk) {
          return true;
        } else {
          return false;
        }
      }
    });

    Futures.addCallback(checkCanProcess, new FutureCallback<Boolean>() {
      @Override
      public void onSuccess(@NullableDecl Boolean result) {
        if (result != canProcess.getValue())
          canProcess.postValue(result);
      }

      @Override
      public void onFailure(Throwable t) {
        HyperLog.w(TAG, "Why fail on validation?", t);
      }
    }, appExecutors.backgroundIO());
  }

  public void setOutlet(KeyValueModel<OutletShortData, String> outletData) {
    if (outletData == null) {
      outletErrorLd.postValue(getApplication().getString(R.string.validation_mandatory));
      outletIdOk = false;
    } else if (outletData != outletLd.getValue()) {
      outletLd.postValue(outletData);
      outletErrorLd.postValue(null);
      outletIdOk = true;

    }
    checkCanProcess();
  }

  public MutableLiveData<KeyValueModel<OutletShortData, String>> getOutlet(){
    return outletLd;
  }

  public void setExpenseItem(KeyValueModel<ExpenseItemData, String> expenseItemData) {
    if (expenseItemData == null) {
      expenseItemErrorLd.postValue(getApplication().getString(R.string.validation_mandatory));
      expenseItemIdOk = false;
    } else if (expenseItemData != expenseItemLd.getValue()) {
      expenseItemLd.postValue(expenseItemData);
      expenseItemErrorLd.postValue(null);
      expenseItemIdOk = true;

      ExpenseItemData eid = expenseItemData.getKey();
      if (eid.isStock()) {
        // tampilkan quantity
        showQuantityLd.postValue(true);
        showQtyVal = true;
        setQuantity(quantityLd.getValue());
      } else {
        // sembunyikan quantity, dan clear value-nya
        showQuantityLd.postValue(false);
        showQtyVal = false;
        setQuantity(null);
      }
      if (eid.isBooking()) {
        showBookingLd.postValue(true);
        showBookingVal = true;
        setBookingData(bookingDataLd.getValue());
      } else {
        showBookingLd.postValue(false);
        showBookingVal = false;
        setBookingData(null);
      }
    }
    checkCanProcess();
  }

  public void setAmount(Integer amount) {
    if (Objects.equals(amount,amountLd.getValue()))
      return;
    try {
      amountLd.postValue(amount);
      ExpenseValidationHelper.validateAmount(getApplication(), amount);
      amountErrorLd.postValue(null);
      amountOk = true;
    } catch (ConstraintViolation constraintViolation) {
      amountErrorLd.postValue(constraintViolation.getMessage());
      amountOk = false;
    }
    checkCanProcess();
  }

  public void setQuantity(Integer quantity) {
    try {
//      if (showQuantityLd.getValue()) {
      if (showQtyVal) {
        // hanya kalau ditampilkan, maka dilakukan validasi
        ExpenseValidationHelper.validateQuantity(getApplication(), quantity);
      }
      if (!Objects.equals(quantity, quantityLd.getValue())) {
        quantityLd.postValue(quantity);
      }

      quantityErrorLd.postValue(null);
      quantityOk = true;
    } catch (ConstraintViolation constraintViolation) {
      quantityErrorLd.postValue(constraintViolation.getMessage());
      quantityOk = false;
    }
    checkCanProcess();
  }

  public void setDescription(String description) {
    if (description == descriptionLd.getValue())
      return;
    descriptionLd.postValue(description);
    checkCanProcess();
  }

  public LiveData<String> getHeaderStatusLd() {
    return headerStatusLd;
  }

  public void setHeaderStatusLd(String data) {
    headerStatusLd.postValue(data);
  }

  public LiveData<String> getHeaderCompanyNameLd() {
    return headerCompanyNameLd;
  }

  public void setHeaderCompanyNameLd(String data) {
    headerCompanyNameLd.postValue(data);
  }

  public LiveData<String> getHeaderOutletNameLd() {
    return headerOutletNameLd;
  }

  public void setHeaderOutletNameLd(String data) {
    headerOutletNameLd.postValue(data);
  }

  public void saveDraft(UUID outletId, LocalDate statementDate, LocalDate expenseDate,
      ExpenseItemData expenseItemData, BigDecimal amount, Integer quantity, String description) {
    // save draft bisa dari prepareNew atau dari load
    ucExpenseSaveDraft.saveExpenseDraft(
        expenseDetailResponse != null ?
            expenseDetailResponse.getExpenseData().getId() :
            newExpenseResponse.getId(),
        outletId, statementDate, expenseDate,
        expenseItemData.getId(),
        bookingDataLd.getValue() != null ? bookingDataLd.getValue().getId() : null,
        amount, quantity, description,
        expenseAttachmentData != null ? expenseAttachmentData.getId() : null,
        expenseAttachmentData != null ? null : this.attachmentFinal,
        expenseDetailResponse != null ?
            expenseDetailResponse.getExpenseData().getUpdateTs() :
            null);
  }

  public void submit(UUID outletId, LocalDate statementDate, LocalDate expenseDate,
      ExpenseItemData expenseItemData, BigDecimal amount, Integer quantity, String description) {
    // submit bisa dari prepareNew atau dari load
    ucExpenseSaveDraft.submitExpense(
        expenseDetailResponse != null ?
            expenseDetailResponse.getExpenseData().getId() :
            newExpenseResponse.getId(),
        outletId, statementDate, expenseDate,
        expenseItemData.getId(),
        bookingDataLd.getValue() != null ? bookingDataLd.getValue().getId() : null,
        amount, quantity, description,
        expenseAttachmentData != null ? expenseAttachmentData.getId() : null,
        expenseAttachmentData != null ? null : this.attachmentFinal,
        expenseDetailResponse != null ?
            expenseDetailResponse.getExpenseData().getUpdateTs() :
            null);
  }

  public void submitRevision(String comment, UUID outletId, LocalDate statementDate,
      LocalDate expenseDate, ExpenseItemData expenseItemData, BigDecimal amount, Integer quantity,
      String description) {
    ucExpenseSaveDraft.submitExpenseRevision(expenseId,
        expenseDetailResponse.getProcTaskData().getId(), comment, outletId, statementDate,
        expenseDate, expenseItemData.getId(),
        bookingDataLd.getValue() != null ? bookingDataLd.getValue().getId() : null,
        amount, quantity, description,
        expenseAttachmentData != null ? expenseAttachmentData.getId() : null,
        expenseAttachmentData != null ? null : this.attachmentFinal,
        expenseDetailResponse != null ?
            expenseDetailResponse.getExpenseData().getUpdateTs() :
            null);
  }

  public void aocDecision(String outcome, String comment) {
    ucExpenseAocDecideUseCase.aocDecide(expenseDetailResponse.getProcTaskData().getId(), expenseId,
        outcome, comment,
        expenseDetailResponse != null ?
            expenseDetailResponse.getExpenseData().getUpdateTs() :
            null);
  }

  @Override
  public void onExpenseSaveDraftStarted(Resource<ExpenseData> loading) {
    formState.postValue(ExpenseFormState.actionInProgress(loading.getMessage()));
  }

  @Override
  public void onExpenseSaveDraftSuccess(Resource<ExpenseData> response) {
    // form kembali ready
    formState.postValue(ExpenseFormState.ready(response.getMessage()));
    // kirim SingleLiveEvent sukses
    actionEvent.postValue(Resource.Builder.success(response.getMessage(), response.getData()));
  }

  @Override
  public void onExpenseSaveDraftFailure(Resource<ExpenseData> responseError) {
    String message = Resource.getMessageFromError(responseError, "Error");
    // form kembali ready
    formState.postValue(ExpenseFormState.ready(message));
    // kirim SingleLiveEvent error
    actionEvent.postValue(Resource.Builder.error(message));
  }

  /**
   * Dipanggil untuk menyimpan pointer ke File temporary sebelum menggunakan kamera
   *
   * @param tempAttachmentFile
   */
  public void setTempAttachmentFile(File tempAttachmentFile) {
    this.tempAttachment = tempAttachmentFile;
  }

  /**
   * Dipanggil untuk mengindikasikan file sudah berhasil didapat atau belum.
   *
   * @param isFilled <code>true</code> sudah didapatkan; <code>false</code> tidak didapatkan
   */
  public void setAttachmentFilled(boolean isFilled, boolean deleteTemp) {
    if (isFilled && this.tempAttachment != null) {
      ListenableFuture<File> attachmentLf = Futures.submit(new Callable<File>() {
        @Override
        public File call() throws Exception {
          BitmapFactory.Options bmOptions = new BitmapFactory.Options();

          // buka gambar asli sebagai bitmap
          Bitmap photo = BitmapFactory.decodeFile(tempAttachment.getAbsolutePath(), bmOptions);
          // perkecil (asumsi resolusi kamera lebih tinggi, sehingga perlu diperkecil)
          Bitmap photoRescaled = ImageUtility.getResizedBitmap(photo, 1280);
          // photoRescaled akan sama dengan photo kalau tidak perlu resize
          int orientation = ImageUtility.getImageOrientation(tempAttachment);
          Bitmap finalPhoto = null;
          if (orientation != 0) {
            // koreksi orientasi kalau perlu rotasi
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);
            finalPhoto = Bitmap.createBitmap(photoRescaled, 0, 0, photoRescaled.getWidth(),
                photoRescaled.getHeight(), matrix, true);
          } else {
            // tidak perlu rotasi
            finalPhoto = photoRescaled;
          }
          try {
            // kita dapatkan versi JPEG dari bitmap yg sudah diresize dan sudah dikoreksi
            // orientasinya
            attachmentFinal = ImageUtility.saveBitmapToJpg(getApplication(), finalPhoto,
                getApplication().getCacheDir(), App.IMAGE_DIR, "expense-"
                // di mana baiknya constant ini?
            );
            if (deleteTemp) {
              // versi temp (yg masih besar) bisa dihapus
              tempAttachment.delete();
            }
            tempAttachment = null;
            if (photo != finalPhoto) {
              // hanya boleh recycle kalau photo beda dengan finalPhoto
              photo.recycle();
            }
            // publish bitmap final untuk ditampilkan
            ExpenseEditViewModel.this.reducedAttachmentLd.postValue(
                Resource.Builder.success(null, finalPhoto));
          } catch (IOException e) {
            HyperLog.e(TAG, "Failed to post-process attachment", e);
            if (deleteTemp) {
              tempAttachment.delete();
            }
            // kalau error, pastikan attachment jadi kosong
            tempAttachment = null;
            // hapus juga versi final kalau error
            if (attachmentFinal != null) {
              attachmentFinal.delete();
            }
            attachmentFinal = null;
          }
          expenseAttachmentData = null;
          return tempAttachment;
        }
      }, bgExecutor);
    } else {
      if (this.tempAttachment != null) {
        if (deleteTemp) {
          this.tempAttachment.delete();
        }
        this.tempAttachment = null;
      }
      if (this.attachmentFinal != null) {
        this.attachmentFinal.delete();
        this.attachmentFinal = null;
      }
      // publish null untuk mereset tampilan attachment
      reducedAttachmentLd.postValue(null);
      this.expenseAttachmentData = null;
    }
  }

  public void loadExpense(boolean force) {
    if (force || !initialized) {
      ucLoadExpense.load(expenseId);
    }
  }

  @Override
  public void onLoadExpenseStarted() {
    formState.postValue(ExpenseFormState.loading(getApplication().getString(R.string.memuat_data)));
  }

  @Override
  public void onLoadExpenseSuccess(Resource<LoadExpenseUseCase.LoadExpenseResult> result) {
    String fn = "onLoadExpenseSuccess() ";
    LoadExpenseUseCase.LoadExpenseResult ler = result.getData();

    // data awal form tetap diset
    newExpenseResponse = ler.getNewExpenseResponse();
    expenseDetailResponse = ler.getExpenseDetailResponse();
    editPageMode = ExpenseEditHelper.getEditPageMode(false,
        expenseDetailResponse.getExpenseData().getStatusCode(),
        expenseDetailResponse.getProcTaskData(), expenseDetailResponse.getCurrentUserId());
    HyperLog.i(TAG, String.format("Expense %s EditMode = %s", expenseId, editPageMode));

    allowBackdate = newExpenseResponse.isBackdateAllowed();

    outletsLd.postValue(newExpenseResponse.getAssignedOutlets());

    List<KeyValueModel<ExpenseItemData, String>> kvmList = ler.getExpenseItemDataList().stream()
        .map(eid -> new KeyValueModel<ExpenseItemData, String>(eid, eid.getName()))
        .collect(Collectors.toList());
    expenseItems.postValue(kvmList);

    boolean useToday = false;
    switch (editPageMode) {
    case KASIR_DRAFT:
      if (!newExpenseResponse.isBackdateAllowed()) {
        // normalnya tidak boleh backdate, jadi statementDate di set ke hari ini
        useToday = true;
      } else {
        // boleh backdate, jadi statementDate sesuai yg sudah tersimpan
        useToday = false;
      }
      // tidak break, lanjut
    case WF_KASIR_PENDING_REVISION:
    case WF_AOC_PENDING_VERIFICATION:
    case VIEW:
      // load isi field lainnya
      if (useToday) {
        setStatementDate(newExpenseResponse.getToday());
      } else {
        setStatementDate(expenseDetailResponse.getExpenseData().getStatementDate());
      }
      setExpenseDate(expenseDetailResponse.getExpenseData().getExpenseDate());

      // outlet
      for (int z = 0; z < newExpenseResponse.getAssignedOutlets().size(); z++) {
        OutletShortData osd = newExpenseResponse.getAssignedOutlets().get(z);
        if (expenseDetailResponse.getExpenseData().getOutletId().equals(osd.getOutletId())) {
          setOutlet(new KeyValueModel<>(osd, osd.getOutletName()));
          break;
        }
      }
      ExpenseItemData eid = null;
      for (int z = 0; z < ler.getExpenseItemDataList().size(); z++) {
        eid = ler.getExpenseItemDataList().get(z);
        if (expenseDetailResponse.getExpenseData().getItemId().equals(eid.getId())) {
          setExpenseItem(new KeyValueModel<>(eid, eid.getName())); // hanya z yang dibutuhkan
          break;
        }
      }
      setAmount(expenseDetailResponse.getExpenseData().getAmount().intValue());
      setQuantity(expenseDetailResponse.getExpenseData().getQuantity());
      setBookingData(expenseDetailResponse.getExpenseData().getBookingData());
      setDescription(expenseDetailResponse.getExpenseData().getDescription());
      expenseDetailResponse.getExpenseData().getStatus();

      formState.postValue(ExpenseFormState.ready(result.getMessage()));
      setHeaderStatusLd(expenseDetailResponse.getExpenseData().getStatus());
      setHeaderCompanyNameLd(expenseDetailResponse.getExpenseData().getCompanyName());
      setHeaderOutletNameLd(expenseDetailResponse.getExpenseData().getOutletName());

      // secara async, proses attachment
      List<ExpenseAttachmentData> attachmentDataList = expenseDetailResponse.getExpenseData()
          .getAttachments();
      if (attachmentDataList != null && attachmentDataList.size() > 0) {
        // saat ini hanya support 1 attachment
        ExpenseAttachmentData ead = attachmentDataList.get(0);
        HyperLog.d(TAG,
            fn + "expenseId[" + getExpenseId() + "] expenseAttacmentId[" + ead.getId()
                + "] isArchived[" + ead.isArchived() + "]");
        setArchived(ead.isArchived());

        // jika file ini adalah archive, maka abaikan proses download, dan image akan di set sebagai icon archive
        if (ead.isArchived())
          return;

        ListenableFuture<ResponseBody> rbLf = utilityService.getFile(ead.getId());
        Futures.addCallback(rbLf, new FutureCallback<ResponseBody>() {
          @Override
          public void onSuccess(@NullableDecl ResponseBody result) {
            InputStream is = result.byteStream();
            FileOutputStream fos = null;
            try {
              File dir = new File(getApplication().getCacheDir() + File.separator + App.IMAGE_DIR);
              boolean dirExists = false;
              if (!dir.exists()) {
                dirExists = dir.mkdirs();
              } else {
                dirExists = true;
              }
              if (dirExists) {
                File output = new File(
                    getApplication().getCacheDir() + File.separator + App.IMAGE_DIR,
                    ead.getFilename());
                fos = new FileOutputStream(output);
                byte buffer[] = new byte[1024];
                int read = -1;
                while ((read = is.read(buffer)) != -1) {
                  fos.write(buffer, 0, read);
                }
                fos.flush();

                // set ke viewmodel
                attachmentFinal = output;
                expenseAttachmentData = ead;
                Glide.with(getApplication()).asBitmap().load(attachmentFinal)
                    .placeholder(R.drawable.ic_spinner_drawable).into(new CustomTarget<Bitmap>() {
                  @Override
                  public void onResourceReady(@NonNull Bitmap resource,
                      @Nullable Transition<? super Bitmap> transition) {
                    reducedAttachmentLd.postValue(Resource.Builder.success(null, resource));
                  }

                  @Override
                  public void onLoadCleared(@Nullable Drawable placeholder) {

                  }
                });
              } else {
                reducedAttachmentLd.postValue(Resource.Builder.error(
                    getApplication().getString(R.string.failed_to_save_local_file)));
              }
            } catch (Exception e) {
              // TODO: show error message
              HyperLog.w(TAG,
                  String.format("Failed to save Expense Attachment %s locally", ead.getId()), e);
              reducedAttachmentLd.postValue(Resource.Builder.error(
                  getApplication().getString(R.string.failed_to_save_local_file)));
            } finally {
              if (fos != null) {
                try {
                  fos.close();
                } catch (IOException e) {
                  e.printStackTrace();
                }
              }
            }
          }

          @Override
          public void onFailure(Throwable t) {
            // TODO: show error message
            HyperLog.w(TAG, String.format("Failed to download Expense Attachment %s", ead.getId()),
                t);
          }
        }, appExecutors.networkIO());
      }else{
        HyperLog.d(TAG, fn + "expenseId[" + getExpenseId()+
            "] expenseAttacmentId[TIDAK ADA ATTACHEMNT] isArchived[FALSE]");
        setArchived(false);
      }
    }
    initialized = true;
    checkCanProcess();
  }

  @Override
  public void onLoadExpenseFailure(Resource<LoadExpenseUseCase.LoadExpenseResult> responseError) {
    // todo terjemahkan jenis-jenis error
    String message = Resource.getMessageFromError(responseError, "Terjadi kesalahan");
    formState.postValue(ExpenseFormState.error(message));
  }

  @Override
  public void onExpenseAocDecideStarted(Resource<ExpenseData> loading) {
    formState.postValue(ExpenseFormState.actionInProgress(loading.getMessage()));
  }

  @Override
  public void onExpenseAocDecideSuccess(Resource<ExpenseData> response) {
    // form kembali ready
    formState.postValue(ExpenseFormState.ready(response.getMessage()));
    // kirim SingleLiveEvent sukses
    actionEvent.postValue(Resource.Builder.success(response.getMessage(), response.getData()));
  }

  @Override
  public void onExpenseAocDecideFailure(Resource<ExpenseData> response) {
    String message = Resource.getMessageFromError(response, "Error");
    // form kembali ready
    formState.postValue(ExpenseFormState.ready(message));
    // kirim SingleLiveEvent error
    actionEvent.postValue(Resource.Builder.error(message));
  }
}
