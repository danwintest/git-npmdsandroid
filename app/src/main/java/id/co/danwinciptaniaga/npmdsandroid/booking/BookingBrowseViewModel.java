package id.co.danwinciptaniaga.npmdsandroid.booking;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelKt;
import androidx.paging.LoadState;
import androidx.paging.Pager;
import androidx.paging.PagingConfig;
import androidx.paging.PagingData;
import androidx.paging.PagingLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseSort;
import id.co.danwinciptaniaga.npmds.data.booking.BookingData;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import kotlinx.coroutines.CoroutineScope;

public class BookingBrowseViewModel extends AndroidViewModel {
  private final String TAG = BookingBrowseViewModel.class.getSimpleName();
  private LiveData<PagingData<BookingData>> bookingList;
  private GetBookingListUseCase getListUC;
  private MutableLiveData<Boolean> refreshList = new SingleLiveEvent<>();
  private Set<SortOrder> sortFields = new LinkedHashSet<>();
  private MutableLiveData<LoadState> bookingLoadState = new MutableLiveData<>();
  private BookingBrowseFilter filterField = new BookingBrowseFilter();

  @ViewModelInject
  public BookingBrowseViewModel(@NonNull Application application, AppExecutors appExecutors,
      GetBookingListUseCase getListUC) {
    super(application);
    this.getListUC = getListUC;
    this.sortFields.add(
        BookingBrowseSort.sortBy(BookingBrowseSort.Field.BOOKINGDATE, SortOrder.Direction.DESC));

    HyperLog.d(TAG, "Construct terapanggil");
    CoroutineScope viewModelScope = ViewModelKt.getViewModelScope(this);
    Pager<Integer, BookingData> pager = new Pager<Integer, BookingData>(new PagingConfig(Utility.PAGE_SIZE),
        () -> new BookingListPagingSource(getListUC, appExecutors.networkIO()));

    bookingList = PagingLiveData.cachedIn(PagingLiveData.getLiveData(pager), viewModelScope);
  }

  public void setRefreshList(Boolean b){
    refreshList.postValue(b);
  }
  public LiveData<Boolean> getRefreshList() {
    return refreshList;
  }

  public LiveData<PagingData<BookingData>> getBookingList() {
    return bookingList;
  }

  public void loadDataWithFilter(BookingBrowseFilter filter) {
    this.filterField = filter;
    getListUC.setFilter(filter);
    refreshList.postValue(true);
  }

  public BookingBrowseFilter getFilterField(){
    return filterField;
  }

  public Set<SortOrder> getSortFields() {
    return sortFields;
  }

  public void setSortFields(Set<SortOrder> sortFields) {
    this.sortFields.clear();
    if(sortFields!=null){
      this.sortFields.addAll(sortFields);
    }
    getListUC.setSort(sortFields);
    refreshList.postValue(true);
  }

  public LiveData<LoadState> getBookingLoadState(){
    return bookingLoadState;
  }
  public void setBookingLoadState(LoadState loadState){
    bookingLoadState.postValue(loadState);
  }
}
