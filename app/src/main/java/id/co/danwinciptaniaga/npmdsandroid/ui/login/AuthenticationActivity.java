package id.co.danwinciptaniaga.npmdsandroid.ui.login;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import javax.crypto.Cipher;
import javax.inject.Inject;

import org.acra.ACRA;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.android.material.textfield.TextInputLayout;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import dagger.hilt.android.AndroidEntryPoint;
import id.co.danwinciptaniaga.androcon.auth.AccountAuthenticator;
import id.co.danwinciptaniaga.androcon.auth.AccountManagerUtil;
import id.co.danwinciptaniaga.androcon.auth.AuthService;
import id.co.danwinciptaniaga.androcon.auth.AuthorizationHeaderProvider;
import id.co.danwinciptaniaga.androcon.auth.GetTokenUseCaseSync;
import id.co.danwinciptaniaga.androcon.auth.LoginUtil;
import id.co.danwinciptaniaga.androcon.auth.TokenResponse;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.SecurityUtil;
import id.co.danwinciptaniaga.androcon.utility.Status;
import id.co.danwinciptaniaga.androcon.utility.Utility;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.databinding.ActivityAuthBinding;
import okhttp3.ResponseBody;

/**
 * AccountAuthenticatorActivity tidak mendukung ViewModel.
 */
@AndroidEntryPoint
public class AuthenticationActivity extends AppCompatActivity {
  private static final String TAG = AuthenticationActivity.class.getSimpleName();

  private enum Mode {
    SIGNIN_NEW_ACCOUNT,
    SIGNIN_EXISTING_ACCOUNT,
    RELOGIN_ACCOUNT_PASSWORD,
    RELOGIN_ACCOUNT_BIOMETRIC
  }

  private Mode mode;

  private AccountAuthenticatorResponse mAccountAuthenticatorResponse = null;
  private Bundle mResultBundle = null;

  public static final String PARAM_USER_PASSWORD = "password";
  public static final String PARAM_EXPIRES_IN = "expiresIn";
  public static final String PARAM_REFRESH_TOKEN = "refreshToken";

  public static final int REQ_GENERAL_AUTH = 101; // App perlu User untuk login
  public static final int REQ_CHOOSE_ACCT = 102; // User memutuskan untuk menggunakan Account yang ada

  private boolean isGeneralLogin;
  private boolean isAddingAccount;
  private boolean performAppLogin;
  private boolean isUsernameFixed;

  @Inject
  LoginUtil loginUtil;
  @Inject
  AuthService authService;
  @Inject
  AuthorizationHeaderProvider ahp;
  @Inject
  AppExecutors executors;
  @Inject
  GetTokenUseCaseSync ucGetTokenSync;

  private ActivityAuthBinding binding;
  private AccountManager mAccountManager;
  private String username;
  private String accountType;
  private String tokenType;
  private Cipher cipher;
  private BiometricPrompt biometricPrompt;
  private BiometricPrompt.PromptInfo promptInfo;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    /**
     * Retrieves the AccountAuthenticatorResponse from either the intent of the savedInstanceState,
     * if the savedInstanceState is non-zero.
     */
    mAccountAuthenticatorResponse =
        getIntent().getParcelableExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE);

    if (mAccountAuthenticatorResponse != null) {
      mAccountAuthenticatorResponse.onRequestContinued();
    }

    binding = ActivityAuthBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    // true kalau dipanggil untuk login secara pertama kali login
    isGeneralLogin = getIntent().getBooleanExtra(AccountAuthenticator.ARG_GENERAL_LOGIN, false);
    // true kalau dipanggil oleh AccountManager utk Add Account (mis: dari Settings Android,
    // User pilih Add Account pada popup)
    isAddingAccount = getIntent().getBooleanExtra(AccountAuthenticator.ARG_IS_ADDING_NEW_ACCOUNT,
        false);
    // true kalau sumber pemanggilannya dari dalam App, false kalau dari Settings Android
    performAppLogin = getIntent().getBooleanExtra(AccountAuthenticator.ARG_PERFORM_APP_LOGIN,
        false);
    // kalau ada account, berarti sudah dipilih
    username = getIntent().getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
    accountType = getIntent().getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);
    // tidak terisi pada skenario confirm credentials
    tokenType = getIntent().getStringExtra(AccountAuthenticator.ARG_AUTH_TOKEN_TYPE);

    isUsernameFixed = username != null;
    // true kalau dipanggil untuk konfirmasi credentials, username pasti ada
    boolean isConfirmCredentials = getIntent().getBooleanExtra(
        AccountAuthenticator.ARG_IS_CONFIRMING_CREDENTIALS, false);

    mAccountManager = AccountManager.get(this);
    Account[] existingAccounts = mAccountManager.getAccountsByType(accountType);
    int noOfExistingAccounts = existingAccounts == null ? 0 : existingAccounts.length;

    mode = null;
    if (isUsernameFixed) {
      Account account = loginUtil.getAccount(username, accountType);
      String token =
          account != null && tokenType != null ?
              mAccountManager.peekAuthToken(account, tokenType) :
              null;
      HyperLog.d(TAG, String.format("peeking token for Account %s with tokenType %s = %s",
          account != null ? account.name : null, tokenType, token));

      if (token != null && loginUtil.isBiometricEnabled() && username.equals(
          loginUtil.getBiometricUsername())) {
        // token harus ada di Account, baru bisa biometric signin
        BiometricManager bm = BiometricManager.from(this);
        int canAuthenticate = bm.canAuthenticate();
        if (canAuthenticate == BiometricManager.BIOMETRIC_SUCCESS) {
          mode = Mode.RELOGIN_ACCOUNT_BIOMETRIC;
          HyperLog.d(TAG, "MODE: " + Mode.RELOGIN_ACCOUNT_BIOMETRIC);
        } else {
          HyperLog.d(TAG, "MODE: Biometric fallback to " + Mode.RELOGIN_ACCOUNT_PASSWORD);
          mode = Mode.RELOGIN_ACCOUNT_PASSWORD;
        }
      } else {
        mode = Mode.RELOGIN_ACCOUNT_PASSWORD;
        HyperLog.d(TAG, "MODE: " + Mode.RELOGIN_ACCOUNT_PASSWORD);
      }
    } else {
      if (isGeneralLogin) {
        if (noOfExistingAccounts > 0) {
          mode = Mode.SIGNIN_EXISTING_ACCOUNT;
          HyperLog.d(TAG, "MODE: General Login " + Mode.SIGNIN_EXISTING_ACCOUNT);
        } else {
          mode = Mode.SIGNIN_NEW_ACCOUNT;
          HyperLog.d(TAG, "MODE: General Login " + Mode.SIGNIN_NEW_ACCOUNT);
        }
      } else {
        if (isAddingAccount) {
          mode = Mode.SIGNIN_NEW_ACCOUNT;
          HyperLog.d(TAG, "MODE: " + Mode.SIGNIN_NEW_ACCOUNT);
        } else if (noOfExistingAccounts > 0) {
          mode = Mode.SIGNIN_EXISTING_ACCOUNT;
          HyperLog.d(TAG, "MODE: " + Mode.SIGNIN_EXISTING_ACCOUNT);
        } else {
          mode = Mode.SIGNIN_NEW_ACCOUNT;
          HyperLog.d(TAG, "MODE: fallback " + Mode.SIGNIN_NEW_ACCOUNT);
        }
      }
    }

    prepareFormByMode();

    Log.i(TAG, String.format(
        "created with options: username=%s, isAddingAccount=%s, performAppLogin=%s, noOfExistingAccounts=%s, accountType=%s, tokenType=%s",
        username, isAddingAccount, performAppLogin, noOfExistingAccounts, accountType, tokenType));

    binding.tietPassword.setOnFocusChangeListener((v, hasFocus) -> {
      if (hasFocus) {
        binding.tietPassword.setText(null);
      }
    });
    binding.btnLoginSavedAccount.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        handleLoginUsingExistingAccounts(existingAccounts);
      }
    });

    binding.btnLogin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // kalau sampai di sini, berarti kemungkinannya:
        // - User sedang menambah akun dari dalam aplikasi
        // - User menambah account dari Settings Android
        handleLoginFormSubmit();
      }
    });
    binding.btnResetPassword.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        handleResetFormPrompt();
      }
    });

    binding.btnBiometricLogin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        biometricPrompt.authenticate(promptInfo, new BiometricPrompt.CryptoObject(cipher));
      }
    });

    binding.btnLogout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        AccountManager am = AccountManager.get(getApplication());
        Account account = loginUtil.getAccount(accountType);
        if (account != null) {
          HyperLog.i(TAG, "Account is still registered: " + account.name);
          String token = am.peekAuthToken(account, tokenType);
          if (token != null) {
            HyperLog.i(TAG,
                "Logging out and amToken exists, attempting to revoke it on server: " + token);
            am.invalidateAuthToken(accountType, token);
            ListenableFuture<ResponseBody> lf = authService.revokeToken(
                ahp.clientAuthorizationHeader(), token, null);
            Futures.addCallback(lf, new FutureCallback<ResponseBody>() {
              @Override
              public void onSuccess(@NullableDecl ResponseBody result) {
                HyperLog.i(TAG, "Revoke token response: " + result);
              }

              @Override
              public void onFailure(Throwable t) {
                HyperLog.w(TAG, "Failed to revoke token response", t);
              }
            }, executors.networkIO());
          } else {
            HyperLog.i(TAG,
                "Logging out but savedToken does not exists, NOT attempting to revoke token on server");
          }
        } else {
          HyperLog.i(TAG, "Account is no longer registered");
        }
        loginUtil.logout();
        App.gotoLauncher(getApplicationContext());
        Intent res = new Intent();
        res.putExtra(AccountManager.KEY_BOOLEAN_RESULT, false);
        setAccountAuthenticatorResult(res.getExtras());
        setResult(AccountAuthenticatorActivity.RESULT_CANCELED, res);
        finish();
      }
    });

    if (mode == Mode.RELOGIN_ACCOUNT_PASSWORD || mode == Mode.RELOGIN_ACCOUNT_BIOMETRIC) {
      if (mode == Mode.RELOGIN_ACCOUNT_BIOMETRIC) {
        // boleh biometric login
        setupBiometric();
        // langsung tampilkan prompt
        biometricPrompt.authenticate(promptInfo, new BiometricPrompt.CryptoObject(cipher));
      }
    }
  }

  private void prepareFormByMode() {
    switch (mode) {
    case SIGNIN_NEW_ACCOUNT:
      prepareFormSigninNewAccount();
      break;
    case SIGNIN_EXISTING_ACCOUNT:
      prepareFormSigninExistingAccount();
      break;
    case RELOGIN_ACCOUNT_PASSWORD:
      prepareFormReloginAccountPassword();
      break;
    case RELOGIN_ACCOUNT_BIOMETRIC:
      prepareFormReloginAccountBiometric();
      break;
    }
  }

  private void prepareFormSigninNewAccount() {
    binding.btnLoginSavedAccount.setVisibility(View.GONE);
    binding.grpLoginForm.setVisibility(View.VISIBLE);
    binding.btnLogout.setVisibility(View.GONE);
    binding.btnBiometricLogin.setVisibility(View.GONE);
  }

  private void prepareFormSigninExistingAccount() {
    binding.btnLoginSavedAccount.setVisibility(View.VISIBLE);
    binding.grpLoginForm.setVisibility(View.GONE);
    binding.btnLogout.setVisibility(View.GONE);
    binding.btnBiometricLogin.setVisibility(View.GONE);
  }

  private void prepareFormReloginAccountPassword() {
    binding.btnLoginSavedAccount.setVisibility(View.GONE);
    binding.grpLoginForm.setVisibility(View.VISIBLE);
    binding.tietUsername.setText(username);
    binding.tietUsername.setEnabled(false);
    binding.btnLogout.setVisibility(View.VISIBLE);
    binding.btnBiometricLogin.setVisibility(View.GONE);
  }

  private void forcePrepareFormReloginAccountPassword(Exception e){
    ACRA.getErrorReporter().handleException(e);
    loginUtil.clearBiometricUser();
    mode = Mode.RELOGIN_ACCOUNT_PASSWORD;
    prepareFormReloginAccountPassword();
    HyperLog.exception(TAG, e);
  }

  private void prepareFormReloginAccountBiometric() {
    try {
      byte[] encryptionIv = loginUtil.getEncryptionIv();
      cipher = SecurityUtil.getInitializedCipherForDecrypt(App.SECRET_KEY_NAME, encryptionIv);
    } catch (InvalidKeyException ike) {
      forcePrepareFormReloginAccountPassword(ike);
      HyperLog.w(TAG,getString(R.string.biometric_has_changed)+ " error["+ ike.getMessage() + "]");
      Toast.makeText(getApplicationContext(), getString(R.string.biometric_has_changed),Toast.LENGTH_LONG).show();
      return;
    } catch (Exception e) {
      HyperLog.w(TAG, "Problem initializing Cipher for decryption", e);
      forcePrepareFormReloginAccountPassword(e);
      return;
    }
    binding.btnLoginSavedAccount.setVisibility(View.GONE);
    binding.grpLoginForm.setVisibility(View.VISIBLE);
    binding.tietUsername.setText(username);
    binding.tietUsername.setEnabled(false);

    binding.btnLogout.setVisibility(View.VISIBLE);
    binding.btnBiometricLogin.setVisibility(View.VISIBLE);
  }

  private void handleLoginUsingExistingAccounts(Account[] existingAccounts) {
    Intent intent = null;
    // Kalau sampai ke sini, berarti tandai ARG_PERFORM_APP_LOGIN = true
    // Kalau sampai ke sini, berarti tandai ARG_IS_ADDING_NEW_ACCOUNT = true kalau addAccount di-launch oleh picker
    Bundle bundle = new Bundle();
    bundle.putBoolean(AccountAuthenticator.ARG_PERFORM_APP_LOGIN, true);
    bundle.putBoolean(AccountAuthenticator.ARG_IS_ADDING_NEW_ACCOUNT, true);
    ArrayList<Account> filteredAccounts = new ArrayList<>();

    for (Account ea : existingAccounts) {
      if (loginUtil.isLoggedIn()) {
        // kalau ada User terlogin, maka hanya dia yang bisa relogin
        if (ea.name.equals(loginUtil.getUsername())) {
          filteredAccounts.add(ea);
          break;
        }
      } else {
        // kalau tidak ada user login, semua bisa
        filteredAccounts.add(ea);
      }
    }
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      intent = mAccountManager.newChooseAccountIntent(null, filteredAccounts,
          new String[] { accountType }, null,
          tokenType, null, bundle);
    } else {
      intent = mAccountManager.newChooseAccountIntent(null, filteredAccounts,
          new String[] { accountType }, false, null,
          tokenType, null, bundle);
    }
    // ketika intent dipanggil, maka User bisa memilih dari:
    // - Account yang ada: langsung kembali ke onActivityResult
    // - atau buat Baru: AuthenticationActivity baru akan dimulai, tapi kita tandai ARG_PERFORM_APP_LOGIN
    AuthenticationActivity.this.startActivityForResult(intent, REQ_CHOOSE_ACCT);
  }

  private void handleLoginFormSubmit() {
    if (!validateUsernamePassword())
      return;
    showLoading(true);
    ListenableFuture<String> loginLf =
        Futures.submit(new Callable<String>() {
          @Override
          public String call() {
            String errorMsg = null;
            String username = binding.tietUsername.getText().toString();
            String password = binding.tietPassword.getText().toString();
            Resource<TokenResponse> ar = ucGetTokenSync.getToken(username, password);

            if (Status.SUCCESS.equals(ar.getStatus())) {
              boolean isRootDetected = Utility.isRootDetected(getApplicationContext());
              boolean byPassRootDetection = Utility.byPassRootDetection(getApplicationContext());
              if(isRootDetected && !byPassRootDetection){
                // jika terdeteksi root dan tidak diijinkan lewat, maka tampilkan pesan khusus root detected
                return getString(R.string.root_detected_message);
              }
              TokenResponse tr = ar.getData();
              String authToken = tr.getAccessToken();
              String refreshToken = tr.getRefreshToken();

              Account account = loginUtil.getAccount(username, accountType);
              HyperLog.d(TAG,
                  String.format("account with name %s exists: %s", username, account != null));

              boolean success = false;
              final Intent res = new Intent();
              if ((isAddingAccount && account == null) || account == null) {
                Log.d(TAG, String.format(
                    "isAddingAccount=%s, existingAccount=%s, going to add new account %s",
                    isAddingAccount, account, username));
                // Creating the account on the device and setting the auth token we got
                // (Not setting the auth token will cause another call to the server to authenticate the user)
                account = new Account(username, accountType);
                boolean addAccountResult = mAccountManager.addAccountExplicitly(account, password,
                    null);
                Log.d(TAG, String.format("addAccount %s result=%s", username, addAccountResult));
                if (addAccountResult) {
                  Log.d(TAG, "authToken and refreshToken set to AccountManager");
                  AccountManagerUtil.setAccountAuthToken(mAccountManager, account,
                      tokenType, authToken, refreshToken, System.currentTimeMillis(),
                      tr.getExpiresIn());
                  success = true;
                } else {
                  Log.d(TAG, "authToken and refreshToken not set to AccountManager");
                }
              } else {
                Log.d(TAG, String.format(
                    "(isAddingAccount=%s OR existingAccount=%s) update new password",
                    isAddingAccount, username));
                mAccountManager.setPassword(account, password);
                Log.d(TAG, "authToken and refreshToken set to AccountManager");
                AccountManagerUtil.setAccountAuthToken(mAccountManager, account,
                    tokenType, authToken, refreshToken, System.currentTimeMillis(),
                    tr.getExpiresIn());
                success = true;
              }

              if (success) {
                if (performAppLogin) {
                  // authentication berhasil, ingat User sudah log in kalau performAppLogin = true
                  Log.i(TAG, "Perform App Login for " + username);
                  loginUtil.setLoggedIn(username, authToken, tr.getExpiresIn());
                  loginUtil.clearLastPausedTime();

                  // NOTE: info ini tidak akan dipakai, kalau login adalah performAppLogin
                  res.putExtra(AccountManager.KEY_ACCOUNT_NAME, username);
                  res.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                  res.putExtra(AccountManager.KEY_AUTHTOKEN, authToken);
                  res.putExtra(AccountManager.KEY_BOOLEAN_RESULT, true);
                  res.putExtra(PARAM_EXPIRES_IN, tr.getExpiresIn());
                  res.putExtra(PARAM_REFRESH_TOKEN, refreshToken);
                  res.putExtra(PARAM_USER_PASSWORD, password);
                  setAccountAuthenticatorResult(res.getExtras());
                  setResult(AccountAuthenticatorActivity.RESULT_OK, res);
                } else {
                  // authentication berhasil, tapi bukan trigger dari App
                  Log.i(TAG, "Not performing App Login for " + username);
                  Bundle bundle = new Bundle();
                  bundle.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, true);
                  bundle.putString(AccountManager.KEY_AUTHTOKEN, authToken);
                  setAccountAuthenticatorResult(bundle);
                  setResult(AccountAuthenticatorActivity.RESULT_OK);
                }
                Log.i(TAG, "Done getting token from server");
              } else {
                Log.w(TAG, "Authentication process failed (add Account not successful)");
                errorMsg = getString(R.string.login_failed);
              }
            } else if (Status.ERROR.equals(ar.getStatus())) {
              Log.e(TAG, "Problem getting token: " + ar);
              TokenResponse o = ar.getData();
              Log.e(TAG, "Problem2 getting token: " + o);
              ErrorResponse er = ar.getErrorResponse();
              if (er != null) {
                errorMsg = String.format("%s: %s", er.getError(), er.getErrorDescription());
              } else {
                errorMsg = getString(R.string.login_failed);
              }
            } else {
              Log.e(TAG, "Problem getting token: " + ar);
              errorMsg = getString(R.string.login_failed);
            }
            return errorMsg;
          }
        }, executors.backgroundIO());
    Futures.whenAllSucceed(loginLf)
        .run(new Runnable() {
          @Override
          public void run() {
            try {
              String errorMsg = Futures.getDone(loginLf);
              if (errorMsg != null) {
                showLoginFailed(errorMsg);
                showLoading(false);
                // pengecekan jika pesan adalah khusus root, maka finish
                if (getString(R.string.root_detected_message).equals(errorMsg))
                  finish();
              } else {
                Log.i(TAG, "loginSuccess");
                finish();
              }
            } catch (ExecutionException e) {
              HyperLog.w(TAG, "Problem with authentication, updating screen", e);
              showLoginFailed(getString(R.string.login_failed) + ": " + e.getMessage());
              showLoading(false);
            }
          }
        }, executors.mainThread());
  }

  private boolean validateUsernamePassword() {
    boolean result = true;
    String username = Utility.getTrimmedString(binding.tietUsername.getText());
    if (username == null || username.length() == 0) {
      result = false;
      binding.tilUsername.setError(getString(R.string.validation_mandatory));
    } else {
      result = true;
      binding.tilUsername.setError(null);
    }
    String password = Utility.getTrimmedString(binding.tietPassword.getText());
    if (password == null || password.length() == 0) {
      result = false;
      binding.tilPassword.setError(getString(R.string.validation_mandatory));
    } else {
      result = true;
      binding.tilPassword.setError(null);
    }
    return result;
  }

  private void handleResetFormPrompt() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setCancelable(true);
    builder.setTitle(R.string.title_reset_password);
    builder.setMessage(R.string.reset_password_message);
    builder.setView(R.layout.dialog_reset_password);
    builder.setPositiveButton(getString(R.string.action_reset), null);
    builder.setNegativeButton(getString(R.string.button_cancel), null);

    AlertDialog dialog = builder.create();
    dialog.show();
    final EditText etUsername = dialog.findViewById(R.id.etUsername);
    final TextInputLayout tilUsername = dialog.findViewById(R.id.tilUsername);
    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String username = Utility.getTrimmedString(etUsername.getText());
        if (username == null || username.length() == 0) {
          tilUsername.setError(getString(R.string.validation_mandatory));
        } else {
          tilUsername.setError(null);
          handleResetPasswordSubmit(dialog, username);
        }
      }
    });

  }

  private void handleResetPasswordSubmit(AlertDialog dialog, String username) {
    ListenableFuture<ResponseBody> lf = authService.resetPassword(ahp.clientAuthorizationHeader(),
        username);
    Futures.addCallback(lf, new FutureCallback<ResponseBody>() {
      @Override
      public void onSuccess(@NullableDecl ResponseBody result) {
        AuthenticationActivity.this.runOnUiThread(
            new Runnable() {
              @Override
              public void run() {
                Toast.makeText(getApplication(),
                    R.string.reset_password_success_message, Toast.LENGTH_LONG)
                    .show();
              }
            });

        if (dialog.isShowing()) {
          dialog.dismiss();
        }
      }

      @Override
      public void onFailure(Throwable t) {
        ErrorResponse er = RetrofitUtility.parseErrorBody(t);
        AuthenticationActivity.this.runOnUiThread(
            new Runnable() {
              @Override
              public void run() {
                Toast.makeText(getApplication(),
                    er == null ?
                        getString(R.string.reset_password_failed_unknown, t.getMessage()) :
                        er.getErrorDescription(),
                    Toast.LENGTH_LONG).show();
              }
            });
      }
    }, executors.networkIO());
  }

  private void showLoading(boolean isLoading) {
    if (isLoading) {
      binding.progressWrapper.progressView.setVisibility(View.VISIBLE);
      binding.btnBiometricLogin.setVisibility(View.GONE);
      binding.grpLoginForm.setVisibility(View.GONE);
      binding.btnLogout.setVisibility(View.GONE);
    } else {
      binding.progressWrapper.progressView.setVisibility(View.GONE);
      prepareFormByMode();
    }
  }

  private void showLoginFailed(String errorString) {
    Toast.makeText(this, errorString, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onBackPressed() {
    setResult(AccountAuthenticatorActivity.RESULT_CANCELED);
    super.onBackPressed();
  }

  public final void setAccountAuthenticatorResult(Bundle result) {
    mResultBundle = result;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    HyperLog.d(TAG,
        String.format("onActivityResult: RequestCode = %s, ResultCode = %s", requestCode,
            resultCode));
    if (requestCode == REQ_CHOOSE_ACCT) {
      // hasil dari User memilih Existing Account
      if (resultCode == AuthenticationActivity.RESULT_OK) {
        handleSuccessfulAccountSelectionOrCreate(data);
      }
    }
  }

  /**
   * Kembali dari login menggunakan existing account (chooseAccountIntent)
   * dengan skenario hasil:
   * - User memilih account yang ada
   * - User sudah login dengan account baru
   *
   * @param data
   */
  private void handleSuccessfulAccountSelectionOrCreate(Intent data) {
    String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
    String accountType = data.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);
    HyperLog.i(TAG, String.format("User selected/login using account: %s", accountName));
    Account account = loginUtil.getAccount(accountName, accountType);
    if (account != null) {
      // harusnya dapat account
      // tetap gunakan AccountManager untuk mendapatkan token
      ListenableFuture<Bundle> getAuthTokenLf = AccountManagerUtil.getAuthTokenLf(
          mAccountManager, account, tokenType, null, executors.backgroundIO());
      Futures.addCallback(getAuthTokenLf, new FutureCallback<Bundle>() {
        @Override
        public void onSuccess(@NullableDecl Bundle bundle) {
          final Intent intent = (Intent) bundle.get(AccountManager.KEY_INTENT);
          if (null != intent) {
            // umumnya kalau sudah pilih acct, getAuthToken akan dapat langsung mengembalikan token
            // kecuali: token invalidated, tidak ada refresh token, dan gagal login ke server menggunakan password
            // maka intent akan dikembalikan, dan kita launch untuk login lagi
            Log.i(TAG, String.format("calling intent from getAuthToken callback: %s", intent));
            // TODO: cek kapan blok ini digunakan
            startActivityForResult(intent, REQ_GENERAL_AUTH);
          } else {
            // kalau tidak ada Intent, berarti AccountManager mengembalikan data saja
            // bisa saja data lama, atau baru saja dari halaman login?
            final String authToken = bundle.getString(AccountManager.KEY_AUTHTOKEN);
            final String accountType = bundle.getString(AccountManager.KEY_ACCOUNT_TYPE);
            final String accountName = bundle.getString(AccountManager.KEY_ACCOUNT_NAME);
            final String refreshToken = bundle.getString(PARAM_REFRESH_TOKEN);
            final int expiresIn = bundle.getInt(PARAM_EXPIRES_IN);

            Log.d(TAG, "Retrieved auth token: " + authToken);
            Log.d(TAG, "Saved account name: " + accountName);

            // Save session username & auth token
            if (performAppLogin) {
              Log.i(TAG, "Perform App Login for " + accountName);
              // mark user is logged in
              loginUtil.setLoggedIn(accountName, authToken, expiresIn);
            } else {
              Log.i(TAG, "Not performing App Login for " + accountName);
            }

            // If the logged account didn't exist, we need to create it on the device
            Account account = loginUtil.getAccount(accountName, accountType);
            if (null == account) {
              // harusnya tidak pernah masuk blok ini bukan?
              HyperLog.i(TAG, "Creating account: " + accountName);
              account = new Account(accountName, accountType);
              boolean addAccountResult = mAccountManager.addAccountExplicitly(account,
                  bundle.getString(AuthenticationActivity.PARAM_USER_PASSWORD), null);
              Log.d(TAG, String.format("addAccount %s result=%s", accountName, addAccountResult));
              AccountManagerUtil.setAccountAuthToken(mAccountManager, account, tokenType,
                  authToken, refreshToken, System.currentTimeMillis(), expiresIn);
            } else {
              HyperLog.i(TAG, "Account exists: " + accountName + ", finishing");
              // account already exists, proceed
            }
            //            Bundle result = new Bundle();
            //            result.putString(AccountManager.KEY_ACCOUNT_NAME, accountName);
            //            result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, true);
            setAccountAuthenticatorResult(bundle);
            setResult(Activity.RESULT_OK);
            finish();
          }
        }

        @Override
        public void onFailure(Throwable t) {
          HyperLog.e(TAG, "Problem processing Authentication ActivityResult", t);
          showLoginFailed(t.getMessage());
        }
      }, executors.mainThread());

    } else {
      HyperLog.w(TAG, "After account is selected, but not found?");
      showLoginFailed(getString(R.string.login_with_saved_account_failed));
    }
  }

  private void setupBiometric() {
    HyperLog.d(TAG, "Biometric canAuthenticate= success");

    biometricPrompt = new BiometricPrompt(
        this, executors.mainThread(),
        new BiometricPrompt.AuthenticationCallback() {
          @Override
          public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
            HyperLog.d(TAG,
                String.format("onAuthenticationError: %s-%s", errorCode,
                    errString));
            if (errorCode == BiometricPrompt.ERROR_CANCELED
                || errorCode == BiometricPrompt.ERROR_TIMEOUT
                || errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
              // show normal login form
            } else {
              Toast.makeText(AuthenticationActivity.this,
                  getString(R.string.biometric_signin_fail_reason, errString),
                  Toast.LENGTH_LONG).show();
            }
          }

          @Override
          public void onAuthenticationSucceeded(
              @NonNull BiometricPrompt.AuthenticationResult result) {
            HyperLog.d(TAG, String.format("onAuthenticationSucceeded: %s", result));
            try {
              byte[] encryptedUsername = loginUtil.getEncryptedData();
              byte[] decryptedUsernameBytes = result.getCryptoObject().getCipher().doFinal(
                  encryptedUsername);

              String decryptedUserName = new String(decryptedUsernameBytes,
                  Charset.defaultCharset());
              if (Objects.equals(username, decryptedUserName)) {
                // Login OK
                if (performAppLogin) {
                  Account account = loginUtil.getAccount(username, accountType);
                  String authToken = mAccountManager.peekAuthToken(account, tokenType);
                  // authentication berhasil, ingat User sudah log in kalau performAppLogin = true
                  Log.i(TAG,
                      "Perform App Login for " + username + " with authToken = " + authToken
                          + ", tokenType = " + tokenType);
                  loginUtil.saveToken(authToken, tokenType);
                  loginUtil.clearLastPausedTime();
                  mAccountManager.setAuthToken(account, tokenType, authToken);

                  // NOTE: info ini tidak akan dipakai, kalau login adalah performAppLogin
                  final Intent res = new Intent();
                  res.putExtra(AccountManager.KEY_ACCOUNT_NAME, username);
                  res.putExtra(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                  res.putExtra(AccountManager.KEY_AUTHTOKEN, authToken);
                  res.putExtra(AccountManager.KEY_BOOLEAN_RESULT, Boolean.TRUE);
                  setAccountAuthenticatorResult(res.getExtras());
                  setResult(AccountAuthenticatorActivity.RESULT_OK, res);
                } else {
                  // authentication berhasil, tapi bukan trigger dari App
                  Log.i(TAG, "Not performing App Login for " + username);
                  Bundle bundle = new Bundle();
                  bundle.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, true);
                  setAccountAuthenticatorResult(bundle);
                  setResult(AccountAuthenticatorActivity.RESULT_OK);
                }
                finish();
              } else {
                // login untuk account berbeda
                Toast.makeText(AuthenticationActivity.this,
                    getString(R.string.biometric_signin_account_no_match),
                    Toast.LENGTH_SHORT).show();
              }
            } catch (Exception e) {
              HyperLog.exception(TAG,
                  "Failed to decrypt data after successful Biometric Authentication", e);
              Toast.makeText(AuthenticationActivity.this,
                  getString(R.string.biometric_signin_fail_reason, e.getMessage()),
                  Toast.LENGTH_SHORT).show();
            }
          }

          @Override
          public void onAuthenticationFailed() {
            HyperLog.d(TAG, String.format("onAuthenticationFailed"));
            Toast.makeText(AuthenticationActivity.this,
                getString(R.string.biometric_signin_fail),
                Toast.LENGTH_SHORT).show();
          }
        });

    promptInfo = new BiometricPrompt.PromptInfo.Builder()
        .setTitle(getString(R.string.biometric_signin_prompt_title, username))
        .setSubtitle(getString(R.string.biometric_signin_prompt_subtitle))
        .setDeviceCredentialAllowed(false)
        .setNegativeButtonText(getString(R.string.biometric_signin_cancel))
        .build();
  }

  @Override
  public void finish() {
    if (mAccountAuthenticatorResponse != null) {
      // send the result bundle back if set, otherwise send an error.
      if (mResultBundle != null) {
        mAccountAuthenticatorResponse.onResult(mResultBundle);
      } else {
        mAccountAuthenticatorResponse.onError(AccountManager.ERROR_CODE_CANCELED,
            "canceled");
      }
      mAccountAuthenticatorResponse = null;
    }
    super.finish();
  }
}
