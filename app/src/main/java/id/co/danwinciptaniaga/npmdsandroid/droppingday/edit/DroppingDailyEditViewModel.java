package id.co.danwinciptaniaga.npmdsandroid.droppingday.edit;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.acra.ACRA;

import com.google.android.gms.common.util.CollectionUtils;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.BatchData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyDataResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyEditHelper;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyScreenMode;
import id.co.danwinciptaniaga.npmds.data.dropping.NewDroppingDailyResponse;
import id.co.danwinciptaniaga.npmds.data.wf.ProcTaskData;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.data.ConstraintViolation;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class DroppingDailyEditViewModel extends AndroidViewModel
    implements LoadDroppingDailyUseCase.Listener, DroppingDailySupervisorDecideUseCase.Listener {

  private static final String TAG = DroppingDailyEditViewModel.class.getSimpleName();

  private final LoadDroppingDailyUseCase ucLoadDroppingDaily;
  private final DroppingDailySupervisorDecideUseCase ucSupervisorDecide;
  private final UtilityService utilityService;
  private final AppExecutors appExecutors;

  private boolean initialized = false;
  private UUID droppingId;
  private UUID currentOrSubstitutedUserId;
  private NewDroppingDailyResponse newDdResponse;
  private DroppingDailyDataResponse ddDataResponse;
  private DroppingDailyData ddData;
  private DroppingDailyScreenMode editPageMode;
  private boolean allowBackdate = false;
  private List<KeyValueModel<OutletShortData, String>> outlets = null;
  private List<KeyValueModel<BatchData, String>> batches = null;
  private Integer selectedOutlet = null;
  private Integer selectedBatch = null;
  private LocalDate requestDate;
  private LocalDate droppingDate;
  private long requestAmount = 0;
  private Long approvedAmount = null; // awalnya belum diisi
  private boolean altAccount;
  private BankAccountData bankAccountData;

  private MutableLiveData<FormState> formStateLd = new MutableLiveData<>();
  private MutableLiveData<Resource<DroppingDailyData>> actionEvent = new SingleLiveEvent<>();
  private MutableLiveData<String> requestDateErrorLd = new MutableLiveData<>();
  private MutableLiveData<String> requestAmountErrorLd = new MutableLiveData<>();
  private List<KeyValueModel<UUID, String>> kvmBadAutoList = new ArrayList<>();
  private KeyValueModel<UUID, String> kvmSelectedBad = null;
  private List<KeyValueModel<UUID, String>> kvmBadManualList = new ArrayList<>();
  private MutableLiveData<String> transferMode = new MutableLiveData<>();
  private MutableLiveData<LocalDate> transferDate = new MutableLiveData<>();
  private UUID selectedAccountId = null;

  @ViewModelInject
  public DroppingDailyEditViewModel(@NonNull Application application,
      LoadDroppingDailyUseCase loadDroppingDailyUseCase,
      DroppingDailySupervisorDecideUseCase droppingDailySupervisorDecideUseCase,
      UtilityService utilityService, AppExecutors appExecutors) {
    super(application);
    ucLoadDroppingDaily = loadDroppingDailyUseCase;
    ucSupervisorDecide = droppingDailySupervisorDecideUseCase;
    this.utilityService = utilityService;
    this.appExecutors = appExecutors;
    ucLoadDroppingDaily.registerListener(this);
    ucSupervisorDecide.registerListener(this);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    ucLoadDroppingDaily.unregisterListener(this);
    ucSupervisorDecide.unregisterListener(this);
  }

  public void loadDropping(boolean force) {
    if (force || !initialized) {
      ucLoadDroppingDaily.load(droppingId);
    }
  }

  public UUID getDroppingId() {
    return droppingId;
  }

  public void setDroppingId(UUID droppingId) {
    this.droppingId = droppingId;
  }

  public void setCurrentOrSubstitutedUserId(UUID currentOrSubstitutedUserId){
    this.currentOrSubstitutedUserId = currentOrSubstitutedUserId;
  }

  public LiveData<FormState> getFormStateLd() {
    return formStateLd;
  }

  public LiveData<Resource<DroppingDailyData>> getActionEvent() {
    return actionEvent;
  }

  public LiveData<String> getRequestDateErrorLd() {
    return requestDateErrorLd;
  }

  public LiveData<String> getRequestAmountErrorLd() {
    return requestAmountErrorLd;
  }

  public boolean isAllowBackdate() {
    return allowBackdate;
  }

  public List<KeyValueModel<OutletShortData, String>> getOutlets() {
    return outlets;
  }

  public List<KeyValueModel<BatchData, String>> getBatches() {
    return batches;
  }

  public Integer getSelectedOutlet() {
    return selectedOutlet;
  }

  public void setSelectedOutlet(int index) {
    selectedOutlet = index;
    if (!altAccount) {
      // kalau bukan alt acount, maka copy dari selected Outlet
      OutletShortData osd = outlets.get(index).getKey();
      if (osd.getBankAccountData() != null) {
        copyBankData(osd.getBankAccountData(), this.bankAccountData);
      }
    }
  }

  public void setSelectedBatch(int index) {
    selectedBatch = index;
  }

  public Integer getSelectedBatch() {
    return selectedBatch;
  }

  public LocalDate getRequestDate() {
    return requestDate;
  }

  public LocalDate getDroppingDate() {
    return droppingDate;
  }

  public void setDroppingDate(LocalDate droppingDate) {
    this.droppingDate = droppingDate;
//    LocalDate today = newDdResponse.getToday();
//    try {
//      HyperLog.d(TAG, "validating dropping date: " + requestDate);
//      DroppingDailyValidationHelper.validateDroppingDate(getApplication(), today,
//          requestDate);
//      this.requestDate = requestDate;
//      requestDateErrorLd.postValue(null);
//    } catch (ConstraintViolation constraintViolation) {
//      HyperLog.w(TAG, "Validation failed for Dropping Date", constraintViolation);
//      requestDateErrorLd.postValue(constraintViolation.getMessage());
//    }
  }

  public long getRequestAmount() {
    return requestAmount;
  }

  public DroppingDailyData getDdData() {
    return ddData;
  }

  public DroppingDailyScreenMode getEditPageMode() {
    return editPageMode;
  }

  public void setRequestDate(LocalDate requestDate) {
    LocalDate today = newDdResponse.getToday();
    try {
      HyperLog.d(TAG, "validating dropping date: " + requestDate);
      DroppingDailyValidationHelper.validateDroppingDate(getApplication(), today,
          requestDate);
      this.requestDate = requestDate;
      requestDateErrorLd.postValue(null);
    } catch (ConstraintViolation constraintViolation) {
      HyperLog.w(TAG, "Validation failed for Dropping Date", constraintViolation);
      requestDateErrorLd.postValue(constraintViolation.getMessage());
    }
  }

  public boolean isAltAccount() {
    return altAccount;
  }

  public void setAltAccount(boolean altAccount) {
    if (altAccount != this.altAccount) {
      this.altAccount = altAccount;
      if (!altAccount) {
        // kalau diubah jadi bukan altAccount, reset ke rekening outlet
        OutletShortData osd = outlets.get(selectedOutlet).getKey();
        if (osd != null && osd.getBankAccountData() != null) {
          BankAccountData bad = osd.getBankAccountData();
          copyBankData(bad, this.bankAccountData);
        }
      } else {
        // kalau diubah jadi altAccount, kosongkan
        BankAccountData bad = new BankAccountData();
        if (bankAccountData != null) {
          // pertahankan Id-nya
          bad.setId(bankAccountData.getId());
        }
        copyBankData(bad, this.bankAccountData);
      }
    }
  }

  public BankAccountData getBankAccountData() {
    return bankAccountData;
  }

  public void setBankAccountData(BankAccountData bankAccountData) {
    this.bankAccountData = bankAccountData;
  }

  public List<KeyValueModel<UUID, String>> getKvmBadAutoList() {
    return kvmBadAutoList;
  }

  public void setKvmBadAutoList(List<KeyValueModel<UUID, String>> kvmBadAutoList) {
    this.kvmBadAutoList = kvmBadAutoList;
  }

  public List<KeyValueModel<UUID, String>> getKvmBadManualList() {
    return kvmBadManualList;
  }

  public void setKvmBadManualList(List<KeyValueModel<UUID, String>> kvmBadManualList) {
    this.kvmBadManualList = kvmBadManualList;
  }

  public KeyValueModel<UUID, String> getKvmSelectedBad() {
    return kvmSelectedBad;
  }

  public void setKvmSelectedBad(KeyValueModel<UUID, String> kvmSelectedBad) {
    this.kvmSelectedBad = kvmSelectedBad;
  }

  public MutableLiveData<String> getTransferMode() {
    return transferMode;
  }

  public void setTransferMode(String transferMode) {
    if (initialized) {
      if (this.transferMode.getValue() != transferMode)
        this.transferMode.postValue(transferMode);
    } else {
      this.transferMode.postValue(transferMode);
    }
  }

  public MutableLiveData<LocalDate> getTransferDate() {
    return transferDate;
  }

  public UUID getSelectedAccountId() {
    return selectedAccountId;
  }

  public void setSelectedAccountId(UUID selectedAccountId) {
    this.selectedAccountId = selectedAccountId;
  }

  public void setTransferDate(LocalDate transferDate) {
    if (initialized) {
      if (this.transferDate.getValue() != transferDate)
        this.transferDate.postValue(transferDate);
    } else {
      this.transferDate.postValue(transferDate);
    }
  }

  // --- UNTUK LOAD --- START
  public ProcTaskData getProcTaskData() {
    return ddDataResponse.getProcTaskData();
  }
  // --- UNTUK LOAD --- END


  @Override
  public void onLoadDroppingDailyStarted() {
    formStateLd.postValue(FormState.loading(getApplication().getString(R.string.memuat_data)));
  }

  @Override
  public void onLoadDroppingDailySuccess(
      Resource<LoadDroppingDailyUseCase.LoadDroppingDailyResult> result) {
    LoadDroppingDailyUseCase.LoadDroppingDailyResult lddr = result.getData();
    ddDataResponse = lddr.getDroppingDailyDataResponse();
    ddData = ddDataResponse.getDroppingData();
    newDdResponse = lddr.getNewDroppingDailyResponse();

    if (newDdResponse.getAssignedOutlets() != null) {
      // tidak pakai stream, karena perlu sekalian identifikasi selected item
      outlets = new ArrayList<>();
      selectedOutlet = 0;
      for (int index = 0; index < newDdResponse.getAssignedOutlets().size(); index++) {
        OutletShortData osd = newDdResponse.getAssignedOutlets().get(index);
        if (ddData.getOutletId().equals(osd.getOutletId()))
          selectedOutlet = index;

        outlets.add(new KeyValueModel<OutletShortData, String>(osd, osd.getOutletName()));
      }
    }
    boolean canSeeAll = ddData.isCanSeeAll();
    String taskDefinitionKey = ddDataResponse.getProcTaskData() != null ?
        ddDataResponse.getProcTaskData().getActTaskDefinitionKey() : null;
    UUID taskUserId = ddDataResponse.getProcTaskData() != null ?
        ddDataResponse.getProcTaskData().getUserId() : null;
    try {
      editPageMode = new DroppingDailyEditHelper().getEditPageMode(false, true, canSeeAll,
          CollectionUtils.isEmpty(outlets), ddData.getWfStatusCode(), taskDefinitionKey, taskUserId,
          currentOrSubstitutedUserId);
    } catch (Exception e) {
      HyperLog.exception(TAG, e);
      ACRA.getErrorReporter().handleException(e);
      editPageMode= null;
    }

//    editPageMode = DroppingDailyScreenMode.getScreenMode(ddDataResponse.getProcTaskData() != null ?
//        ddDataResponse.getProcTaskData().getActTaskDefinitionKey() : null);
    allowBackdate = newDdResponse.isBackdateAllowed();

    this.altAccount = ddData.isAltAccount();
    this.bankAccountData = ddData.getDroppingAccountData();
    boolean useToday = false;
    switch (editPageMode) {
    case CAPTAIN_VERIFICATION: // kalau revisi, droppingDate akan perlu direset
      if (!newDdResponse.isBackdateAllowed()) {
        // normalnya tidak boleh backdate, jadi droppingDate di set ke hari ini
        useToday = true;
      } else {
        // boleh backdate, jadi droppingDate sesuai yg sudah tersimpan
        useToday = false;
      }
    }
    // load isi field lainnya
    if (useToday) {
      requestDate = newDdResponse.getToday();
      droppingDate = newDdResponse.getToday();
    } else {
      requestDate = ddData.getRequestDate();
      droppingDate = ddData.getDroppingDate();
    }

    // --set TransferInstruction--
    setSelectedAccountId(ddData.getTransferInstruction_SourceAccountId());
    setTransferDate(ddData.getTransferInstruction_TransferDate());

    List<KeyValueModel<UUID, String>> kvmBadAutoList = new ArrayList<>();
    for (BankAccountData bad : ddDataResponse.getBadAutoList()) {
      KeyValueModel<UUID, String> kvmBadAuto = new KeyValueModel<>(bad.getId(), bad.getBankName());
      if (Utility.AUTOMATIC.equals(ddData.getTransferInstruction_TransferType())) {
        if (bad.getId().equals(getSelectedAccountId()))
          setKvmSelectedBad(kvmBadAuto);
      }
      kvmBadAutoList.add(kvmBadAuto);
    }
    setKvmBadAutoList(kvmBadAutoList);

    List<KeyValueModel<UUID, String>> kvmBadManualList = new ArrayList<>();
    for (BankAccountData bad : ddDataResponse.getBadManualList()) {
      KeyValueModel<UUID, String> kvmBadManual = new KeyValueModel<>(bad.getId(),
          bad.getBankName());
      if (Utility.MANUAL.equals(ddData.getTransferInstruction_TransferType())) {
        if (bad.getId().equals(getSelectedAccountId()))
          setKvmSelectedBad(kvmBadManual);
      }
      kvmBadManualList.add(kvmBadManual);
    }
    setKvmBadManualList(kvmBadManualList);
    
    setTransferMode(ddData.getTransferInstruction_TransferType() != null ?
        ddData.getTransferInstruction_TransferType() : Utility.AUTOMATIC);
    // #--set TransferInstruction--
    initialized = true;
    formStateLd.postValue(FormState.ready(result.getMessage()));
  }

  @Override
  public void onLoadDroppingDailyFailure(
      Resource<LoadDroppingDailyUseCase.LoadDroppingDailyResult> response) {
    String message = Resource.getMessageFromError(response,
        getApplication().getString(R.string.error_contact_support));
    // form kembali ready
    formStateLd.postValue(FormState.error(message));
  }

  public void setApprovedAmount(Long amount) {
    this.approvedAmount = amount;
  }

  protected static void copyBankData(BankAccountData source, BankAccountData target) {
    target.setId(source.getId());
    target.setOtherBankName(source.getOtherBankName());
    target.setBankId(source.getBankId());
    target.setBankCode(source.getBankCode());
    target.setBankName(source.getBankName());
    target.setAccountNo(source.getAccountNo());
    target.setAccountName(source.getAccountName());
    target.setValidated(source.isValidated());
    target.setLastValidated(source.getLastValidated());
  }

  public void captainApprove(String comment) {
    ucSupervisorDecide.captainApprove(ddDataResponse.getProcTaskData().getId(), comment,
        droppingId, approvedAmount, altAccount, bankAccountData,
        ddData.getUpdateTs());
  }

  public boolean isOkFinStaffApprove() {
    HyperLog.d(TAG,"isOkFinStaffApprove()"+
        "getEditPageMode[" + getEditPageMode() + "]\n"
        + "procTaskId [" + ddDataResponse.getProcTaskData().getId() + "]\n "
        + "droppingId[" + droppingId + "]\n "
        + "transferDate[" + getTransferDate().getValue() + "]\n "
        + "transferMode[" + getTransferMode().getValue() + "]\n"
        + " selectedAccId[" + getSelectedAccountId() + "]\n "
        + "updateTs[" + ddData.getUpdateTs() + "]");

    return getSelectedAccountId() != null && getTransferMode().getValue() != null
        && getTransferDate().getValue() != null;
  }

  public void finStaffApprove(String comment) {
    ucSupervisorDecide.finStaffApprove(ddDataResponse.getProcTaskData().getId(),
        comment, droppingId, getTransferDate().getValue(), getTransferMode().getValue(),
        getSelectedAccountId(), ddData.getUpdateTs());
  }

  public void finSpvApprove(String comment, String otpCode) {
    ucSupervisorDecide.finSpvApprove(ddDataResponse.getProcTaskData().getId(),
        comment, droppingId, otpCode, getTransferMode().getValue(), ddData.getUpdateTs());
  }

  public void processGenerateOTP() {
    ucSupervisorDecide.processGenerateOTP(ddDataResponse.getProcTaskData().getId(),
        getDroppingId());
  }

  public void rejectOrReturn(String decision, String comment) {
    ucSupervisorDecide.rejectOrReturn(ddDataResponse.getProcTaskData().getId(), droppingId,
        decision, comment, ddData.getUpdateTs());
  }

  @Override
  public void onDdDecideStarted(Resource<DroppingDailyData> response) {
    formStateLd.postValue(FormState.actionInProgress(response.getMessage()));
  }

  @Override
  public void onDdDecideSuccess(Resource<DroppingDailyData> response) {
    // form kembali ready
    formStateLd.postValue(FormState.ready(response.getMessage()));
    // kirim SingleLiveEvent sukses
    actionEvent.postValue(Resource.Builder.success(response.getMessage(), response.getData()));
  }

  @Override
  public void onDdDecideFailure(Resource<DroppingDailyData> response) {
    String message = Resource.getMessageFromError(response, "Error");
    // form kembali ready
    formStateLd.postValue(FormState.ready(message));
    // kirim SingleLiveEvent error
    actionEvent.postValue(Resource.Builder.error(message));
  }
}
