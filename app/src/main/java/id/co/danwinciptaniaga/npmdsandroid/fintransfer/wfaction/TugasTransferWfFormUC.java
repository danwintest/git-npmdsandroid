package id.co.danwinciptaniaga.npmdsandroid.fintransfer.wfaction;

import java.util.UUID;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.BaseObservable;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.TugasTransferWfResponse;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.TugasTransferService;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;

public class TugasTransferWfFormUC extends
    BaseObservable<TugasTransferWfFormUC.Listener, TugasTransferWfResponse> {
  private final String TAG = TugasTransferWfFormUC.class.getSimpleName();
  private final TugasTransferService service;
  private final Application app;
  private final AppExecutors appExecutors;

  @Inject
  public TugasTransferWfFormUC(Application app, TugasTransferService service,
      AppExecutors appExecutors) {
    super(app);
    this.app = app;
    this.service = service;
    this.appExecutors = appExecutors;
  }

  public void getWfFormResponse(UUID companyId, Boolean isAuto) {
    notifyStart("Silahkan Tunggu", null);
    ListenableFuture<TugasTransferWfResponse> process = service.getWfFormResponse(companyId,
        isAuto);
    Futures.addCallback(process, new FutureCallback<TugasTransferWfResponse>() {
      @Override
      public void onSuccess(@NullableDecl TugasTransferWfResponse result) {
        HyperLog.d(TAG, "getWfFormResponse() berhasil");
        notifySuccess("-", result);
      }

      @Override
      public void onFailure(Throwable t) {
        String msg = Utility.getErrorMessageFromThrowable(TAG, t);
        notifyFailure("getWfFormResponse() Gagal ->" + msg, null, t);
      }
    }, appExecutors.networkIO());
  }

  @Override
  protected void doNotifyStart(Listener listener, Resource<TugasTransferWfResponse> result) {
    listener.onProcessStarted(result);
  }

  @Override
  protected void doNotifySuccess(Listener listener, Resource<TugasTransferWfResponse> result) {
    listener.onProcessSuccess(result);
  }

  @Override
  protected void doNotifyFailure(Listener listener, Resource<TugasTransferWfResponse> result) {
    listener.onProcessFailure(result);
  }

  public interface Listener {
    void onProcessStarted(Resource<TugasTransferWfResponse> loading);

    void onProcessSuccess(Resource<TugasTransferWfResponse> res);

    void onProcessFailure(Resource<TugasTransferWfResponse> res);
  }
}
