package id.co.danwinciptaniaga.npmdsandroid.booking.appbooking;

import java.util.concurrent.Executor;

import org.jetbrains.annotations.NotNull;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import androidx.annotation.NonNull;
import androidx.paging.ListenableFuturePagingSource;
import id.co.danwinciptaniaga.androcon.retrofit.RetrofitUtility;
import id.co.danwinciptaniaga.androcon.utility.ErrorResponse;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingData;
import id.co.danwinciptaniaga.npmds.data.AppBooking.AppBookingListResponse;

public class AppBookingListPagingSource
    extends ListenableFuturePagingSource<Integer, AppBookingData> {
  private static final String TAG = AppBookingListPagingSource.class.getSimpleName();
  private final AppBookingBrowseUC appBookingBrowseUC;
  private final Executor mBgExecutor;

  public AppBookingListPagingSource(AppBookingBrowseUC appBookingBrowseUC, Executor mBgExecutor) {
    this.appBookingBrowseUC = appBookingBrowseUC;
    this.mBgExecutor = mBgExecutor;
  }

  @NotNull
  @Override
  public ListenableFuture<LoadResult<Integer, AppBookingData>> loadFuture(
      @NotNull LoadParams<Integer> loadParams) {
    Integer nextPageNumber = loadParams.getKey();
    if (nextPageNumber == null) {
      nextPageNumber = 1;
    }
    int pageSize = loadParams.getPageSize();
    ListenableFuture<LoadResult<Integer, AppBookingData>> pageFuture = Futures.transform(
        appBookingBrowseUC.getAppBookingList(nextPageNumber, pageSize), this::toLoadResult,
        mBgExecutor);
    return Futures.catching(pageFuture, Exception.class, input -> {
      HyperLog.e(TAG, "Error while getting appBookingList", input);
      ErrorResponse er = RetrofitUtility.parseErrorBody(input);
      return new LoadResult.Error(
          er != null ? new Exception(er.getFormattedMessage(), input) : input);
    }, mBgExecutor);
  }

  private LoadResult<Integer, AppBookingData> toLoadResult(
      @NonNull AppBookingListResponse response) {
    return new LoadResult.Page<>(response.getAppBookingData(), null, response.getNextPageNumber(),
        LoadResult.Page.COUNT_UNDEFINED, LoadResult.Page.COUNT_UNDEFINED);
  }
}
