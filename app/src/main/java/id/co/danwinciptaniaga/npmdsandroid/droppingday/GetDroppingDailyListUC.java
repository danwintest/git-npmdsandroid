package id.co.danwinciptaniaga.npmdsandroid.droppingday;

import java.util.Set;

import javax.inject.Inject;

import com.google.common.util.concurrent.ListenableFuture;

import id.co.danwinciptaniaga.npmds.data.ListResponse;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingBrowseSort;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDailyData;

public class GetDroppingDailyListUC {

  private final DroppingDailyService droppingService;

  private DroppingBrowseFilter filter = new DroppingBrowseFilter();
  private DroppingBrowseSort sort = new DroppingBrowseSort();

  @Inject
  public GetDroppingDailyListUC(DroppingDailyService droppingService) {
    this.droppingService = droppingService;
  }

  public ListenableFuture<ListResponse<DroppingDailyData>> getDroppingDailyList(Integer page,
      int pageSize) {
    ListenableFuture<ListResponse<DroppingDailyData>> result =
        droppingService.getDroppingList(page, pageSize, filter, sort);
    return result;
  }

  public void setFilter(DroppingBrowseFilter filter) {
    this.filter = filter;
  }

  public void setSort(Set<SortOrder> sortOrders) {
    this.sort.setSortOrders(sortOrders);
  }
}
