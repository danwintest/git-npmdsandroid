package id.co.danwinciptaniaga.npmdsandroid.fintransfer.ceksaldo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import android.app.Application;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.BankData;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.BalanceInfo;
import id.co.danwinciptaniaga.npmds.data.tugastransfer.BalanceInfoDetail;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.fintransfer.filter.TugasTransferFilterVM;

public class TugasTransferCekSaldoVM extends AndroidViewModel
    implements TugasTransferCekSaldoUC.Listener, TugasTransferCekSaldoBankListUC.Listener, TugasTransferCekSaldoAccountListUC.Listener {
  private final static String TAG = TugasTransferFilterVM.class.getSimpleName();
  private final TugasTransferCekSaldoUC uc;
  private final TugasTransferCekSaldoBankListUC bankListUC;
  private final TugasTransferCekSaldoAccountListUC accountListUC;
  private MutableLiveData<FormState> formState = new MutableLiveData<>();
  private UUID companyId;
  private String companyCode;
  private MutableLiveData<List<BalanceInfoDetail>> badList = new MutableLiveData<>();
  private MutableLiveData<String> failedMsg = new MutableLiveData<>();
  private MutableLiveData<String> bankMsg = new MutableLiveData<>();
  private MutableLiveData<String> accountMsg = new MutableLiveData<>();
  private MutableLiveData<List<BankData>> bankList = new MutableLiveData<>();
  private UUID selectedBankId;
  private MutableLiveData<List<BankAccountData>> accList = new MutableLiveData<>();
  private UUID selectedAccId;

  @ViewModelInject
  public TugasTransferCekSaldoVM(@NonNull Application application, TugasTransferCekSaldoUC uc, TugasTransferCekSaldoBankListUC bankListUC, TugasTransferCekSaldoAccountListUC accountListUC) {
    super(application);
    this.uc = uc;
    this.bankListUC = bankListUC;
    this.accountListUC = accountListUC;
    this.uc.registerListener(this);
    this.bankListUC.registerListener(this);
    this.accountListUC.registerListener(this);
  }

  public MutableLiveData<FormState> getFormState() {
    return formState;
  }

  public void setFormState(
      MutableLiveData<FormState> formState) {
    this.formState = formState;
  }

  public MutableLiveData<List<BankData>> getBankList() {
    return bankList;
  }

  public void setBankList(List<BankData> bankList) {
    this.bankList.postValue(bankList);
  }

  public MutableLiveData<List<BankAccountData>> getAccList() {
    return accList;
  }

  public void setAccList(List<BankAccountData> accList) {
    this.accList.postValue(accList);
  }

  public void setCompanyId(UUID companyId) {
    this.companyId = companyId;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String getCompanyCode(){
    return this.companyCode;
  }

  public MutableLiveData<List<BalanceInfoDetail>> getBadList() {
    return badList;
  }

//  public void setBadList(
//      List<BalanceAccountData> badList) {
//    this.badList.postValue(badList);
//  }

  public MutableLiveData<String> getFailedMsg() {
    return failedMsg;
  }

  public MutableLiveData<String> getBankMsg(){
    return bankMsg;
  }

  public MutableLiveData<String> getAccountMsg() {
    return accountMsg;
  }

//  public void setFailedMsg(MutableLiveData<String> failedMsg) {
//    this.failedMsg = failedMsg;
//  }

  public void setSelectedBankId(UUID bankId){
    this.selectedBankId = bankId;
    clearBalanceList();
  }

  public void setSelectedAccId(UUID accId){
    this.selectedAccId = accId;
    clearBalanceList();
  }

  public void processLoadBankList() {
    bankListUC.getBankDataListByCompanyId(companyId);
  }

  public void processLoadAccList(){
    accountListUC.getAccountDataListByCompanyIdAndBankId(companyId, selectedBankId);
  }

  private void clearBalanceList(){
    badList.postValue(new ArrayList<>());
    failedMsg.postValue(null);
  }

  public void processLoadBalanceList(Context ctx){
    boolean isValidSelectedBank = true;
    boolean isValidSelectedAcc = true;
    if(Objects.equals(selectedBankId,null)){
      bankMsg.postValue(ctx.getString(R.string.validation_bank_constaint_message));
      isValidSelectedBank = false;
    }else{
      bankMsg.postValue(null);
    }

//    if(Objects.equals(selectedAccId,null)){
//      accountMsg.postValue(ctx.getString(R.string.validation_bankaccount_constraint_message));
//      isValidSelectedAcc = false;
//    }else{
//      accountMsg.postValue(null);
//    }
    if (isValidSelectedBank)
      uc.getAccountBalanceList(companyId, selectedBankId, selectedAccId);
  }

  @Override
  public void onProcessStarted(Resource<BalanceInfo> loading) {
    formState.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onProcessSuccess(Resource<BalanceInfo> res) {
    BalanceInfo data = res.getData();
    badList.postValue(data.getBalanceInfoDetails());
    failedMsg.postValue(data.getFailedMsg());
    formState.postValue(FormState.ready(res.getMessage()));
  }

  @Override
  public void onProcessFailure(Resource<BalanceInfo> res) {
    formState.postValue(FormState.error(res.getMessage()));
    clearBalanceList();
  }

  @Override
  public void onLoadBankStarted(Resource<List<BankData>> loading) {
    formState.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onLoadBankSuccess(Resource<List<BankData>> res) {
    setBankList(res.getData());
    formState.postValue(FormState.ready(res.getMessage()));
  }

  @Override
  public void onLoadBankFailure(Resource<List<BankData>> res) {
    formState.postValue(FormState.error(res.getMessage()));
  }

  @Override
  public void onLoadAccountStarted(Resource<List<BankAccountData>> loading) {
    formState.postValue(FormState.loading(loading.getMessage()));
  }

  @Override
  public void onLoadAccountSuccess(Resource<List<BankAccountData>> res) {
    setAccList(res.getData());
    formState.postValue(FormState.ready(res.getMessage()));
  }

  @Override
  public void onLoadAccountFailure(Resource<List<BankAccountData>> res) {
    formState.postValue(FormState.error(res.getMessage()));
  }
}
