package id.co.danwinciptaniaga.npmdsandroid.droppingadd.edit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import org.acra.ACRA;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;

import com.google.android.gms.common.util.CollectionUtils;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import android.app.Application;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.hilt.lifecycle.ViewModelInject;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import id.co.danwinciptaniaga.androcon.utility.AppExecutors;
import id.co.danwinciptaniaga.androcon.utility.KeyValueModel;
import id.co.danwinciptaniaga.androcon.utility.Resource;
import id.co.danwinciptaniaga.androcon.utility.UtilityService;
import id.co.danwinciptaniaga.npmds.data.common.BankAccountData;
import id.co.danwinciptaniaga.npmds.data.common.BatchData;
import id.co.danwinciptaniaga.npmds.data.common.OutletShortData;
import id.co.danwinciptaniaga.npmds.data.dropping.AttachmentDTO;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalDataResponse;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalEditHelper;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalScreenMode;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingAdditionalData;
import id.co.danwinciptaniaga.npmds.data.dropping.DroppingDetailDTO;
import id.co.danwinciptaniaga.npmds.data.dropping.NewDroppingAdditionalResponse;
import id.co.danwinciptaniaga.npmds.data.wf.ProcTaskData;
import id.co.danwinciptaniaga.npmdsandroid.App;
import id.co.danwinciptaniaga.npmdsandroid.R;
import id.co.danwinciptaniaga.npmdsandroid.common.FormState;
import id.co.danwinciptaniaga.npmdsandroid.data.ConstraintViolation;
import id.co.danwinciptaniaga.npmdsandroid.util.SingleLiveEvent;
import id.co.danwinciptaniaga.npmdsandroid.util.Utility;
import okhttp3.ResponseBody;

public class DroppingAdditionalEditViewModel extends AndroidViewModel
    implements PrepareDroppingAdditionalUseCase.Listener, DroppingAdditionalSaveUseCase.Listener,
    LoadDroppingAdditionalUseCase.Listener, DroppingAdditionalSupervisorDecideUseCase.Listener {

  private static final String TAG = DroppingAdditionalEditViewModel.class.getSimpleName();

  private final PrepareDroppingAdditionalUseCase ucPrepareDroppingAdditional;
  private final DroppingAdditionalSaveUseCase ucDroppingAdditionalSave;
  private final LoadDroppingAdditionalUseCase ucLoadDroppingAdditional;
  private final DroppingAdditionalSupervisorDecideUseCase ucSupervisorDecide;
  private final UtilityService utilityService;
  private final AppExecutors appExecutors;

  private boolean initialized = false;
  private UUID droppingId;
  private NewDroppingAdditionalResponse newDaResponse;
  private DroppingAdditionalDataResponse daDataResponse;
  private DroppingAdditionalData daData;
  private DroppingAdditionalScreenMode editPageMode;
  private boolean allowBackdate = false;
  private List<KeyValueModel<OutletShortData, String>> outlets = null;
  private List<KeyValueModel<BatchData, String>> batches = null;
  private Integer selectedOutlet = null;
  private MutableLiveData<String> selectedOutletErrorLd = new MutableLiveData<>();
  private Integer selectedBatch = null;
  private LocalDate requestDate;
  private LocalDate droppingDate;
  private MutableLiveData<String> droppingDateErrorLd = new MutableLiveData<>();
  private long requestAmount = 0;
  private Long approvedAmount = null; // awalnya belum diisi
  private boolean altAccount;
  private BankAccountData bankAccountData;
  private MutableLiveData<List<DroppingDetailDTO>> droppingDetailList = new MutableLiveData<>(
      new ArrayList<>());
  private SingleLiveEvent<Integer> changedRequestDetail = new SingleLiveEvent<>();

  private MutableLiveData<FormState> formStateLd = new MutableLiveData<>();
  private MutableLiveData<Resource<DroppingAdditionalData>> actionEvent = new SingleLiveEvent<>();
  private MutableLiveData<String> requestDateErrorLd = new MutableLiveData<>();
  private MutableLiveData<String> requestAmountErrorLd = new MutableLiveData<>();
  private List<KeyValueModel<UUID, String>> kvmBadAutoList = new ArrayList<>();
//  private MutableLiveData<KeyValueModel<UUID, String>> kvmSelectedBad = new MutableLiveData<>();
  private KeyValueModel<UUID, String> kvmSelectedBad = null;
  private List<KeyValueModel<UUID, String>> kvmBadManualList = new ArrayList<>();
  private MutableLiveData<String> transferMode = new MutableLiveData<>();
  private MutableLiveData<LocalDate> transferDate = new MutableLiveData<>();
  private UUID selectedAccountId = null;
  private MutableLiveData<Boolean> operationalTransfer = new MutableLiveData<>(false);
  private UUID currentOrSubstitutedUserId;

  @ViewModelInject
  public DroppingAdditionalEditViewModel(@NonNull Application application,
      PrepareDroppingAdditionalUseCase prepareDroppingAdditionalUseCase,
      DroppingAdditionalSaveUseCase droppingAdditionalSaveUseCase,
      LoadDroppingAdditionalUseCase loadDroppingAdditionalUseCase,
      DroppingAdditionalSupervisorDecideUseCase droppingAdditionalSupervisorDecideUseCase,
      UtilityService utilityService, AppExecutors appExecutors) {
    super(application);
    ucPrepareDroppingAdditional = prepareDroppingAdditionalUseCase;
    ucDroppingAdditionalSave = droppingAdditionalSaveUseCase;
    ucLoadDroppingAdditional = loadDroppingAdditionalUseCase;
    this.ucSupervisorDecide = droppingAdditionalSupervisorDecideUseCase;
    this.utilityService = utilityService;
    this.appExecutors = appExecutors;
    ucPrepareDroppingAdditional.registerListener(this);
    ucDroppingAdditionalSave.registerListener(this);
    ucLoadDroppingAdditional.registerListener(this);
    ucSupervisorDecide.registerListener(this);
  }

  @Override
  protected void onCleared() {
    super.onCleared();
    ucPrepareDroppingAdditional.unregisterListener(this);
    ucDroppingAdditionalSave.unregisterListener(this);
    ucLoadDroppingAdditional.unregisterListener(this);
    ucSupervisorDecide.unregisterListener(this);
  }

  public void loadDropping(boolean force) {
    if (force || !initialized) {
      ucLoadDroppingAdditional.load(droppingId);
    }
  }

  public UUID getDroppingId() {
    return droppingId;
  }

  public void setDroppingId(UUID droppingId) {
    this.droppingId = droppingId;
  }

  public void prepareNewDropping(boolean force) {
    if (force || !initialized) {
      this.ucPrepareDroppingAdditional.prepare();
    }
  }

  public LiveData<FormState> getFormStateLd() {
    return formStateLd;
  }

  public LiveData<Resource<DroppingAdditionalData>> getActionEvent() {
    return actionEvent;
  }

  public LiveData<List<DroppingDetailDTO>> getDroppingDetailList() {
    return droppingDetailList;
  }

  public LiveData<Integer> getChangedRequestDetail() {
    return changedRequestDetail;
  }

  public LiveData<String> getRequestDateErrorLd() {
    return requestDateErrorLd;
  }

  public LiveData<String> getRequestAmountErrorLd() {
    return requestAmountErrorLd;
  }

  public MutableLiveData<Boolean> getOperationalTransfer() {
    return operationalTransfer;
  }

  public void setOperationalTransfer(Boolean operationalTransfer) {
    if (!Objects.equals(this.operationalTransfer.getValue(), operationalTransfer))
      this.operationalTransfer.postValue(operationalTransfer);
  }

  public void updateDroppingDetail(DroppingDetailFormData droppingDetailFormData) {
    List<DroppingDetailDTO> list = getDroppingDetailList().getValue();
    DroppingDetailDTO dd = null;
    if (droppingDetailFormData.getIndex() == null) {
      // item baru
      dd = new DroppingDetailDTO();
      dd.setDetailId(UUID.randomUUID());
      list.add(dd);
    } else {
      // existing item
      dd = list.get(droppingDetailFormData.getIndex());
    }
    dd.setDescription(droppingDetailFormData.getDescription());
    dd.setAmount(droppingDetailFormData.getAmount());
    dd.setAttachmentDTO(droppingDetailFormData.getAttachmentDTO());
    dd.setArchived(droppingDetailFormData.isArchived());
    recalculateRequestAmount(list);
    droppingDetailList.postValue(list);
  }

  private void recalculateRequestAmount(List<DroppingDetailDTO> requestDetails) {
    long total = 0;
    if (requestDetails != null) {
      for (DroppingDetailDTO dd : requestDetails) {
        total += dd.getAmount();
      }
    }
    requestAmount = total;
  }

  public void removeDroppingDetail(DroppingDetailDTO dd) {
    List<DroppingDetailDTO> list = getDroppingDetailList().getValue();
    ListIterator<DroppingDetailDTO> iterator = list.listIterator();
    while (iterator.hasNext()) {
      if (dd == iterator.next()) {
        iterator.remove();
        break;
      }
    }
    recalculateRequestAmount(list);
    droppingDetailList.postValue(list);

  }

  @Override
  public void onPrepareNewDroppingAdditionalStarted() {
    formStateLd.postValue(
        FormState.loading(getApplication().getString(R.string.mempersiapkan_form)));
  }

  @Override
  public void onPrepareNewDroppingAdditionalSuccess(
      Resource<NewDroppingAdditionalResponse> response) {
    newDaResponse = response.getData();

    editPageMode = DroppingAdditionalScreenMode.KASIR_SUBMITTED;
    droppingId = newDaResponse.getId();
    allowBackdate = newDaResponse.isBackdateAllowed();

    requestDate = newDaResponse.getToday();
    droppingDate = newDaResponse.getToday();

    if (newDaResponse.getAssignedOutlets() != null) {
      outlets = newDaResponse.getAssignedOutlets().stream()
          .map(ao -> new KeyValueModel<OutletShortData, String>(ao, ao.getOutletName()))
          .collect(Collectors.toList());
      if (newDaResponse.getAssignedOutlets().size() == 1) {
        // auto select
        selectedOutlet = 0;
      }
    }
    if (newDaResponse.getBatchDataList() != null) {
      batches = newDaResponse.getBatchDataList().stream()
          .map(ao -> new KeyValueModel<BatchData, String>(ao, ao.getName()))
          .collect(Collectors.toList());
      // auto select
      selectedBatch = 0;
    }
    this.bankAccountData = new BankAccountData();

    initialized = true;
    formStateLd.postValue(FormState.ready(null));
  }

  @Override
  public void onPrepareNewDroppingAdditionalFailure(
      Resource<NewDroppingAdditionalResponse> responseError) {
    // todo terjemahkan jenis-jenis error
    String message = Resource.getMessageFromError(responseError,
        getApplication().getString(R.string.error_contact_support));
    formStateLd.postValue(FormState.error(message));
  }

  public void setCurrentOrSubstitutedUserId(UUID currentOrSubstitutedUserId){
    this.currentOrSubstitutedUserId = currentOrSubstitutedUserId;
  }

  public boolean isAllowBackdate() {
    return allowBackdate;
  }

  public List<KeyValueModel<OutletShortData, String>> getOutlets() {
    return outlets;
  }

  public List<KeyValueModel<BatchData, String>> getBatches() {
    return batches;
  }

  public Integer getSelectedOutlet() {
    return selectedOutlet;
  }

  public void setSelectedOutlet(int index) {
    selectedOutlet = index;
    if (!altAccount) {
      // kalau bukan alt acount, maka copy dari selected Outlet
      OutletShortData osd = outlets.get(index).getKey();
      if (osd.getBankAccountData() != null) {
        copyBankData(osd.getBankAccountData(), this.bankAccountData);
      }
    }
    if (!Objects.equals(index, 0))
      selectedOutletErrorLd.postValue(null);
  }

  public void setSelectedBatch(int index) {
    selectedBatch = index;
  }

  public Integer getSelectedBatch() {
    return selectedBatch;
  }

  public LocalDate getRequestDate() {
    return requestDate;
  }

  public LocalDate getDroppingDate() {
    return droppingDate;
  }

  public void setDroppingDate(LocalDate droppingDate) {
    LocalDate today = newDaResponse.getToday();
    this.droppingDate = droppingDate;
    try {
      HyperLog.d(TAG, "validating dropping date: " + droppingDate);
      DroppingAdditionalValidationHelper.validateDroppingDateBefore(getApplication(), today,
          droppingDate);
      droppingDateErrorLd.postValue(null);
    } catch (ConstraintViolation constraintViolation) {
      HyperLog.w(TAG, "Validation failed for Dropping Date", constraintViolation);
      droppingDateErrorLd.postValue(constraintViolation.getMessage());
    }
  }

  public LiveData<String> getDroppingDateErrorLd() {
    return droppingDateErrorLd;
  }

  public LiveData<String> getSelectedOutletErrorLd() {
    return selectedOutletErrorLd;
  }

  public long getRequestAmount() {
    return requestAmount;
  }

  public DroppingAdditionalData getDaData() {
    return daData;
  }

  public DroppingAdditionalScreenMode getEditPageMode() {
    return editPageMode;
  }

  public void setRequestDate(LocalDate requestDate) {
//    LocalDate today = newDaResponse.getToday();
//    try {
      HyperLog.d(TAG, "validating Request date: " + requestDate);
//      DroppingAdditionalValidationHelper.validateDroppingDate(getApplication(), today,
//          requestDate);
      this.requestDate = requestDate;
      requestDateErrorLd.postValue(null);
//    } catch (ConstraintViolation constraintViolation) {
//      HyperLog.w(TAG, "Validation failed for Request Date", constraintViolation);
//      requestDateErrorLd.postValue(constraintViolation.getMessage());
//    }
  }

  public boolean isAltAccount() {
    return altAccount;
  }

  public void setAltAccount(boolean altAccount) {
    if (altAccount != this.altAccount) {
      this.altAccount = altAccount;
      if (!altAccount) {
        // kalau diubah jadi bukan altAccount, reset ke rekening outlet
        OutletShortData osd = outlets.get(selectedOutlet).getKey();
        if (osd != null && osd.getBankAccountData() != null) {
          BankAccountData bad = osd.getBankAccountData();
          copyBankData(bad, this.bankAccountData);
        }
      } else {
        // kalau diubah jadi altAccount, kosongkan
        BankAccountData bad = new BankAccountData();
        if (bankAccountData != null) {
          // pertahankan Id-nya
          bad.setId(bankAccountData.getId());
        }
        copyBankData(bad, this.bankAccountData);
      }
    }
  }

  public BankAccountData getBankAccountData() {
    return bankAccountData;
  }

  public void setBankAccountData(BankAccountData bankAccountData) {
    this.bankAccountData = bankAccountData;
  }

  public UUID getSelectedAccountId() {
    return selectedAccountId;
  }

  public void setSelectedAccountId(UUID selectedAccountId) {
    this.selectedAccountId = selectedAccountId;
  }

  public List<KeyValueModel<UUID, String>> getKvmBadAutoList() {
    return kvmBadAutoList;
  }

  public void setKvmBadAutoList(List<KeyValueModel<UUID, String>> kvmBadAutoList) {
    this.kvmBadAutoList = kvmBadAutoList;
  }

  public List<KeyValueModel<UUID, String>> getKvmBadManualList() {
    return kvmBadManualList;
  }

  public void setKvmBadManualList(List<KeyValueModel<UUID, String>> kvmBadManualList) {
    this.kvmBadManualList = kvmBadManualList;
  }

    public KeyValueModel<UUID, String> getKvmSelectedBad() {
    return kvmSelectedBad;
  }

  public void setKvmSelectedBad(KeyValueModel<UUID, String> kvmSelectedBad) {
    this.kvmSelectedBad = kvmSelectedBad;
  }

  public MutableLiveData<LocalDate> getTransferDate() {
    return transferDate;
  }

  public MutableLiveData<String> getTransferMode() {
    return transferMode;
  }

  public void setTransferMode(String transferMode) {
    if (initialized) {
      if (this.transferMode.getValue() != transferMode)
        this.transferMode.postValue(transferMode);
    } else {
      this.transferMode.postValue(transferMode);
    }
  }

  public void setTransferDate(LocalDate transferDate) {
    if (initialized) {
      if (this.transferDate.getValue() != transferDate)
        this.transferDate.postValue(transferDate);
    } else {
      this.transferDate.postValue(transferDate);
    }
  }

  // --- UNTUK LOAD --- START
  public ProcTaskData getProcTaskData() {
    return daDataResponse.getProcTaskData();
  }
  // --- UNTUK LOAD --- END

  public void saveDraft() {
    if (!isOkSaveSubmit())
      return;

      OutletShortData osd = outlets.get(selectedOutlet).getKey();
      ucDroppingAdditionalSave.saveDroppingAdditionalDraft(droppingId, osd.getOutletId(),
          requestDate, droppingDate, droppingDetailList.getValue(),
          daData != null ? daData.getUpdateTs() : null);
  }

  public void submitDraft() {
    if (!isOkSaveSubmit())
      return;

      OutletShortData osd = outlets.get(selectedOutlet).getKey();
      ucDroppingAdditionalSave.submitDroppingAdditionalDraft(droppingId, osd.getOutletId(),
          requestDate, droppingDate, droppingDetailList.getValue(),
          daData != null ? daData.getUpdateTs() : null);
  }


  public boolean isOkSaveSubmit(){
    boolean isOk = true;
    if (Objects.equals(selectedOutlet, null) || (Objects.equals(selectedOutlet, 0)
        && Objects.equals(outlets, null))) {
      HyperLog.w(TAG, "Validation failed for selectedOutlet" + getApplication().getString(
          R.string.mandatory_outlet));
      selectedOutletErrorLd.postValue(getApplication().getString(R.string.mandatory_outlet));
      isOk = false;
    } else {
      selectedOutletErrorLd.postValue(null);
    }

    try {
      DroppingAdditionalValidationHelper.validateDroppingDateBefore(getApplication(), requestDate,
          droppingDate);
    } catch (Exception e) {
      HyperLog.w(TAG, "Validation failed for Dropping Date", e);
      droppingDateErrorLd.postValue(e.getMessage());
      isOk = false;
    }

    return isOk;
  }

  public boolean isOkFinStaffApprove() {
    HyperLog.d(TAG,"isOkFinStaffApprove()"+
        "getEditPageMode[" + getEditPageMode() + "]\n"
        + "procTaskId [" + daDataResponse.getProcTaskData().getId() + "]\n "
        + "droppingId[" + droppingId + "]\n "
        + "transferDate[" + getTransferDate().getValue() + "]\n "
        + "transferMode[" + getTransferMode().getValue() + "]\n"
        + " selectedAccId[" + getSelectedAccountId() + "]\n "
        + "updateTs[" + daData.getUpdateTs() + "]");

    return getSelectedAccountId() != null && getTransferMode().getValue() != null
        && getTransferDate().getValue() != null;
  }

  public void submitRevision(String comment) {
    try {
      DroppingAdditionalValidationHelper.validateDroppingDateBefore(getApplication(), requestDate,
          droppingDate);

      OutletShortData osd = outlets.get(selectedOutlet).getKey();
      ucDroppingAdditionalSave.submitRevisionDroppingAdditionalDraft(
          daDataResponse.getProcTaskData().getId(), comment, droppingId, osd.getOutletId(),
          requestDate, droppingDate, droppingDetailList.getValue(), daData.getUpdateTs());
    } catch (Exception e) {
      HyperLog.w(TAG, "Validation failed for Dropping Date", e);
      droppingDateErrorLd.postValue(e.getMessage());
    }
  }

  public void docApprove(String comment) {
    BatchData bd = batches.get(selectedBatch).getKey();
    ucSupervisorDecide.docApprove(daDataResponse.getProcTaskData().getId(), comment,
        droppingId, bd.getCode(), approvedAmount, altAccount, bankAccountData,
        operationalTransfer.getValue(), daData.getUpdateTs());
  }

  public void captainApprove(String comment) {
    BatchData bd = batches.get(selectedBatch).getKey();
    ucSupervisorDecide.captainApprove(daDataResponse.getProcTaskData().getId(), comment,
        droppingId, bd.getCode(), approvedAmount, altAccount, bankAccountData,
        operationalTransfer.getValue(), daData.getUpdateTs());
  }

  public void finStaffApprove(String comment) {
    ucSupervisorDecide.finStaffApprove(daDataResponse.getProcTaskData().getId(), comment,
        droppingId, getTransferDate().getValue(), getTransferMode().getValue(),
        getSelectedAccountId(), daData.getUpdateTs());
  }

  public void finSpvApprove(String comment, String otpCode) {
    ucSupervisorDecide.finSpvApprove(daDataResponse.getProcTaskData().getId(),
        comment, droppingId, otpCode, getTransferMode().getValue(), daData.getUpdateTs());
  }

  public void processGenerateOTP() {
    ucSupervisorDecide.processGenerateOTP(daDataResponse.getProcTaskData().getId(), droppingId);
  }

  public void rejectOrReturn(String decision, String comment) {
    ucSupervisorDecide.rejectOrReturn(daDataResponse.getProcTaskData().getId(), droppingId,
        decision, comment, daData.getUpdateTs());
  }

  @Override
  public void onDropppingAdditionalSaveDraftStarted(Resource<DroppingAdditionalData> loading) {
    formStateLd.postValue(FormState.actionInProgress(loading.getMessage()));
  }

  @Override
  public void onDropppingAdditionalSaveDraftSuccess(Resource<DroppingAdditionalData> response) {
    formStateLd.postValue(FormState.ready(response.getMessage()));
    // kirim SingleLiveEvent sukses
    actionEvent.postValue(Resource.Builder.success(response.getMessage(), response.getData()));
  }

  @Override
  public void onDropppingAdditionalSaveDraftFailure(Resource<DroppingAdditionalData> response) {
    String message = Resource.getMessageFromError(response,
        getApplication().getString(R.string.error_contact_support));
    // form kembali ready
    formStateLd.postValue(FormState.ready(message));
    // kirim SingleLiveEvent error
    actionEvent.postValue(Resource.Builder.error(message));
  }

  @Override
  public void onLoadDroppingAdditionalStarted() {
    formStateLd.postValue(FormState.loading(getApplication().getString(R.string.memuat_data)));
  }

  @Override
  public void onLoadDroppingAdditionalSuccess(
      Resource<LoadDroppingAdditionalUseCase.LoadDroppingAdditionalResult> result) {
    LoadDroppingAdditionalUseCase.LoadDroppingAdditionalResult ldar = result.getData();
    daDataResponse = ldar.getDroppingAdditionalDataResponse();
    daData = daDataResponse.getDroppingData();
    newDaResponse = ldar.getNewDroppingAdditionalResponse();


    allowBackdate = newDaResponse.isBackdateAllowed();

    setOperationalTransfer(daData.getOperationalTransfer());

    if (newDaResponse.getAssignedOutlets() != null) {
      // tidak pakai stream, karena perlu sekalian identifikasi selected item
      outlets = new ArrayList<>();
      selectedOutlet = 0;
      for (int index = 0; index < newDaResponse.getAssignedOutlets().size(); index++) {
        OutletShortData osd = newDaResponse.getAssignedOutlets().get(index);
        if (daData.getOutletId().equals(osd.getOutletId()))
          selectedOutlet = index;

        outlets.add(new KeyValueModel<OutletShortData, String>(osd, osd.getOutletName()));
      }
    }

    String taskDefinitionKey = daDataResponse.getProcTaskData() != null ?
        daDataResponse.getProcTaskData().getActTaskDefinitionKey() : null;
    UUID taskUserId = daDataResponse.getProcTaskData() != null ?
        daDataResponse.getProcTaskData().getUserId() : null;
    try {
      editPageMode = new DroppingAdditionalEditHelper().getEditPageMode(daData.isCanCreate(),
          true, daData.isCanSeeAll(), CollectionUtils.isEmpty(outlets), daData.getWfStatusCode(),
          taskDefinitionKey, taskUserId, currentOrSubstitutedUserId);
    } catch (Exception e) {
      HyperLog.exception(TAG, e);
      ACRA.getErrorReporter().handleException(e);
      editPageMode = null;
    }

    if (newDaResponse.getBatchDataList() != null) {
      // tidak pakai stream, karena perlu sekalian identifikasi selected item
      batches = new ArrayList<>();
      selectedBatch = 0;
      for (int index = 0; index < newDaResponse.getBatchDataList().size(); index++) {
        BatchData bd = newDaResponse.getBatchDataList().get(index);
        if (daData.getBatchCode().equals(bd.getCode()))
          selectedBatch = index;

        batches.add(new KeyValueModel<BatchData, String>(bd, bd.getName()));
      }
    }

    this.altAccount = daData.isAltAccount();
    this.bankAccountData = daData.getDroppingAccountData();
    boolean useToday = false;
    switch (editPageMode) {
    case KASIR_SUBMITTED:
    case KASIR_REVISION: // kalau revisi, droppingDate akan perlu direset
      if (!newDaResponse.isBackdateAllowed()) {
        // normalnya tidak boleh backdate, jadi droppingDate di set ke hari ini
        useToday = true;
      } else {
        // boleh backdate, jadi droppingDate sesuai yg sudah tersimpan
        useToday = false;
      }
    }
    // load isi field lainnya
    if (useToday) {
      requestDate = newDaResponse.getToday();
      droppingDate = newDaResponse.getToday();
    } else {
      requestDate = daData.getRequestDate();
      droppingDate = daData.getDroppingDate();
    }
    recalculateRequestAmount(daData.getRequestDetails());
    droppingDetailList.postValue(daData.getRequestDetails());

    // --set TransferInstruction--
    setSelectedAccountId(daData.getTransferInstruction_SourceAccountId());
    Log.d(TAG,"viewModel.getKvmSelectedBad() [1]setAccId[" + getSelectedAccountId() + "]");
    setTransferDate(daData.getTransferInstruction_TransferDate());

    List<KeyValueModel<UUID, String>> kvmBadAutoList = new ArrayList<>();
    for (BankAccountData bad : daDataResponse.getBadAutoList()) {
      KeyValueModel<UUID, String> kvmBadAuto = new KeyValueModel<>(bad.getId(), bad.getBankName());
      if (Utility.AUTOMATIC.equals(daData.getTransferInstruction_TransferType())) {
        if (bad.getId().equals(getSelectedAccountId()))
          setKvmSelectedBad(kvmBadAuto);
      }
      kvmBadAutoList.add(kvmBadAuto);
    }
    setKvmBadAutoList(kvmBadAutoList);

    List<KeyValueModel<UUID, String>> kvmBadManualList = new ArrayList<>();
    for (BankAccountData bad : daDataResponse.getBadManualList()) {
      KeyValueModel<UUID, String> kvmBadManual = new KeyValueModel<>(bad.getId(),
          bad.getBankName());
      if (Utility.MANUAL.equals(daData.getTransferInstruction_TransferType())) {
        if (bad.getId().equals(getSelectedAccountId()))
          setKvmSelectedBad(kvmBadManual);
      }
      kvmBadManualList.add(kvmBadManual);
    }
    setKvmBadManualList(kvmBadManualList);

    setTransferMode(daData.getTransferInstruction_TransferType() != null ?
        daData.getTransferInstruction_TransferType() :
        Utility.AUTOMATIC);
    // #--set TransferInstruction--
    initialized = true;
    formStateLd.postValue(FormState.ready(result.getMessage()));
  }

  @Override
  public void onLoadDroppingAdditionalFailure(
      Resource<LoadDroppingAdditionalUseCase.LoadDroppingAdditionalResult> response) {
    String message = Resource.getMessageFromError(response,
        getApplication().getString(R.string.error_contact_support));
    // form kembali ready
    formStateLd.postValue(FormState.error(message));
  }

  public void prepareAttachment(AttachmentDTO attachmentDTO, int position, boolean isArchived) {
    // kalau method ini dipanggil, artinya attachmentDTO tidak berisi file
    // jadi harus coba cek di cache, atau download dulu
    // jika attachment ini adalah sudah di arsipkan, maka tidak perlu melakukan proses selanjutnya, dan image akan di set dengan icon archived
    if (isArchived)
      return;

    File targetDir = new File(getApplication().getCacheDir() + File.separator + App.IMAGE_DIR);
    File file = new File(targetDir, attachmentDTO.getFileName());
    if (!file.exists()) {
      HyperLog.d(TAG, "File " + file + " does not exists yet, downloading");
      if (!targetDir.exists()) {
        boolean mkdirResult = targetDir.mkdir();
        HyperLog.d(TAG, "Creating directory " + targetDir + " result: " + mkdirResult);
      }
      // downlad dulu
      final File downloadDestination = file;
      final UUID fileId = attachmentDTO.getFileId();
      ListenableFuture<ResponseBody> fileDownloadLf = utilityService.getFile(fileId);
      Futures.addCallback(fileDownloadLf, new FutureCallback<ResponseBody>() {
        @Override
        public void onSuccess(@NullableDecl ResponseBody result) {
          InputStream is = result.byteStream();
          FileOutputStream fos = null;
          try {
            fos = new FileOutputStream(downloadDestination);
            byte buffer[] = new byte[1024];
            int read = -1;
            while ((read = is.read(buffer)) != -1) {
              fos.write(buffer, 0, read);
            }
            fos.flush();
            // ini diperlukan untuk view via intent
            attachmentDTO.setFile(downloadDestination);
            // tidak perlu post lagi, Glide sudah load ke ImageView-nya
            //            droppingDetailList.postValue(getDroppingDetailList().getValue());
          } catch (Exception e) {
            HyperLog.w(TAG, "Failed to save downloaded file: " + fileId, e);
          } finally {
            try {
              fos.close();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        }

        @Override
        public void onFailure(Throwable t) {
          HyperLog.w(TAG, "Failed to download file: " + fileId, t);
        }
      }, appExecutors.networkIO());
    } else {
      // sudah ada, tinggal tampilkan
      HyperLog.d(TAG, "File " + file + " exists, displaying");
      // ini diperlukan untuk view via intent
      attachmentDTO.setFile(file);
      // tidak perlu post lagi, Glide sudah load ke ImageView-nya
      //      droppingDetailList.postValue(getDroppingDetailList().getValue());
    }
  }

  public void setApprovedAmount(Long amount) {
    this.approvedAmount = amount;
  }

  @Override
  public void onDaDecideStarted(Resource<DroppingAdditionalData> response) {
    formStateLd.postValue(FormState.actionInProgress(response.getMessage()));
  }

  @Override
  public void onDaDecideSuccess(Resource<DroppingAdditionalData> response) {
    // form kembali ready
    formStateLd.postValue(FormState.ready(response.getMessage()));
    // kirim SingleLiveEvent sukses
    actionEvent.postValue(Resource.Builder.success(response.getMessage(), response.getData()));
  }

  @Override
  public void onDaDecideFailure(Resource<DroppingAdditionalData> response) {
    String message = Resource.getMessageFromError(response, "Error");
    // form kembali ready
    formStateLd.postValue(FormState.ready(message));
    // kirim SingleLiveEvent error
    actionEvent.postValue(Resource.Builder.error(message));
  }

  protected static void copyBankData(BankAccountData source, BankAccountData target) {
    target.setId(source.getId());
    target.setOtherBankName(source.getOtherBankName());
    target.setBankId(source.getBankId());
    target.setBankCode(source.getBankCode());
    target.setBankName(source.getBankName());
    target.setAccountNo(source.getAccountNo());
    target.setAccountName(source.getAccountName());
    target.setValidated(source.isValidated());
    target.setLastValidated(source.getLastValidated());
  }
}