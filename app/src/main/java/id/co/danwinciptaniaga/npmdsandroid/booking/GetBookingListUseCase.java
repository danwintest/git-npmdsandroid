package id.co.danwinciptaniaga.npmdsandroid.booking;

import java.time.LocalDate;
import java.util.Set;

import javax.inject.Inject;

import com.google.common.collect.Sets;
import com.google.common.util.concurrent.ListenableFuture;
import com.hypertrack.hyperlog.HyperLog;

import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseFilter;
import id.co.danwinciptaniaga.npmds.data.booking.BookingBrowseSort;
import id.co.danwinciptaniaga.npmds.data.booking.BookingListResponse;
import id.co.danwinciptaniaga.npmds.data.common.SortOrder;

public class GetBookingListUseCase {
  private static final String TAG = GetBookingListUseCase.class.getSimpleName();

  private final BookingService bookingService;
  private BookingBrowseSort sort = new BookingBrowseSort();
  private BookingBrowseFilter filter = new BookingBrowseFilter();

  @Inject
  public GetBookingListUseCase(BookingService bookingService) {
    this.bookingService = bookingService;
  }

  public ListenableFuture<BookingListResponse> getBookingList(Integer page, Integer pageSize) {
    HyperLog.d(TAG, "getBookingList terpanggil");
    ListenableFuture<BookingListResponse> result = bookingService.getBookingList(page, pageSize,
        filter, sort);
    return result;
  }

  public void setSort(Set<SortOrder> sortOrders) {
    this.sort.setSortOrders(sortOrders);
  }

  public void setFilter(BookingBrowseFilter filter){
    this.filter = filter;
  }
}
